<%-- 
    Document   : menuTest
    Created on : 3 mai 2021, 10:20:17
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Menu test</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>


        <style>
            @media (max-width: 768px) {
                .lblTxtAssuj{text-align: justify;margin-top: 3px}
                .divTableBien,.divTableArticle{overflow: auto}
            }

            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }
        </style>
    </head>
    <body>

        <div class="btn-group" style="text-align:left; height:55px"><span  style="" class="label label-warning"><i class="fa fa-list"></i></span>'
            <span class="label label-warning" data-toggle="dropdown">
                <span class="caret"></span><span class="sr-only">Toggle Dropdown</span></span>
            <ul class="dropdown-menu" role="menu">
                <li style="font-size:10.5px"><a href="#" onclick="openDetailCustomer(\'' + tempDetailCmdCarte[j].numeroCarte + '\')">Voir plus d\'infos client</a></li>'
                <li><a href="#" onclick="">Voir les emprunts</a></li>'
                <li><a href="#" onclick="">Voir les prets</a></li>'
            </ul>
        </div>

        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-list"></i> About Us
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="about-us">
                <li><a href="#">Our Story</a></li>
                <li><a href="#">Our Team</a></li>
                <li><a href="#">Contact Us</a></li>
            </ul>
        </div>

        <div class="btn-group" class="label label-warning">
            <button class="btn btn-warning">Left</button>
            <button class="btn btn-danger">Middle</button>
            <button class="btn btn-add-fonction">Right</button>
        </div>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>

        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script  type="text/javascript" src="assets/js/uploading.js"></script>
    </body>
</html>
