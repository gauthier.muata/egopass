package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(IcmParam.class)
public class IcmParam_ { 

    public static volatile SingularAttribute<IcmParam, String> tarif;
    public static volatile SingularAttribute<IcmParam, String> unite;
    public static volatile SingularAttribute<IcmParam, BigDecimal> taux;
    public static volatile SingularAttribute<IcmParam, Integer> id;
    public static volatile SingularAttribute<IcmParam, Integer> annee;
    public static volatile SingularAttribute<IcmParam, Integer> etat;
    public static volatile SingularAttribute<IcmParam, String> devise;

}