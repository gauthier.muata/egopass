package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Identite.class)
public class Identite_ { 

    public static volatile SingularAttribute<Identite, String> nationalite;
    public static volatile SingularAttribute<Identite, String> agentMaj;
    public static volatile SingularAttribute<Identite, Date> dateCreat;
    public static volatile SingularAttribute<Identite, String> carteType;
    public static volatile SingularAttribute<Identite, String> carteId;
    public static volatile SingularAttribute<Identite, Short> type;
    public static volatile SingularAttribute<Identite, String> nom;
    public static volatile SingularAttribute<Identite, Short> etat;
    public static volatile SingularAttribute<Identite, Date> dateMaj;
    public static volatile SingularAttribute<Identite, String> prenoms;
    public static volatile SingularAttribute<Identite, String> adresse;
    public static volatile SingularAttribute<Identite, Integer> id;
    public static volatile SingularAttribute<Identite, String> agentCreat;

}