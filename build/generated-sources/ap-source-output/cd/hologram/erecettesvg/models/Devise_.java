package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.DetailsAssignationBudgetaire;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Devise.class)
public class Devise_ { 

    public static volatile SingularAttribute<Devise, String> agentMaj;
    public static volatile SingularAttribute<Devise, String> code;
    public static volatile SingularAttribute<Devise, Date> dateCreat;
    public static volatile SingularAttribute<Devise, String> agentCreat;
    public static volatile SingularAttribute<Devise, String> intitule;
    public static volatile SingularAttribute<Devise, Short> parDefaut;
    public static volatile SingularAttribute<Devise, Short> etat;
    public static volatile SingularAttribute<Devise, Date> dateMaj;
    public static volatile ListAttribute<Devise, DetailsAssignationBudgetaire> detailsAssignationBudgetaireList;

}