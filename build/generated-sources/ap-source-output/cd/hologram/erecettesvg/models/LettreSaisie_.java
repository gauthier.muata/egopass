package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Commandement;
import cd.hologram.erecettesvg.models.Personne;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(LettreSaisie.class)
public class LettreSaisie_ { 

    public static volatile SingularAttribute<LettreSaisie, Personne> fkPersonne;
    public static volatile SingularAttribute<LettreSaisie, Date> dateCreat;
    public static volatile SingularAttribute<LettreSaisie, Commandement> fkCommandement;
    public static volatile SingularAttribute<LettreSaisie, BigDecimal> fraisPoursuite;
    public static volatile SingularAttribute<LettreSaisie, Date> dateEcheance;
    public static volatile SingularAttribute<LettreSaisie, String> id;
    public static volatile SingularAttribute<LettreSaisie, Agent> agentCreat;
    public static volatile SingularAttribute<LettreSaisie, Integer> etat;
    public static volatile SingularAttribute<LettreSaisie, BigDecimal> fraisPoursuiteCmd;
    public static volatile SingularAttribute<LettreSaisie, Date> dateReception;

}