package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Site;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AgentSite.class)
public class AgentSite_ { 

    public static volatile SingularAttribute<AgentSite, Agent> codeagent;
    public static volatile SingularAttribute<AgentSite, String> code;
    public static volatile SingularAttribute<AgentSite, Site> codesite;
    public static volatile SingularAttribute<AgentSite, Boolean> etat;

}