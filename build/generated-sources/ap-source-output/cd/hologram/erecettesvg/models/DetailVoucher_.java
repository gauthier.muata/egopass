package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Voucher;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailVoucher.class)
public class DetailVoucher_ { 

    public static volatile SingularAttribute<DetailVoucher, Voucher> fkVaucher;
    public static volatile SingularAttribute<DetailVoucher, Date> dateGeneration;
    public static volatile SingularAttribute<DetailVoucher, Integer> id;
    public static volatile SingularAttribute<DetailVoucher, String> codeVoucher;
    public static volatile SingularAttribute<DetailVoucher, Integer> etat;
    public static volatile SingularAttribute<DetailVoucher, Date> dateUtilisation;

}