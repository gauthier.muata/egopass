    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author gauthier.muata
 */
@Entity
public class GetTauxNonFiscal implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BORNE_INFERIEURE")
    private Float borneInferieure;
    @Column(name = "BORNE_SUPERIEURE")
    private Float borneSuperieure;
    @Column(name = "MULTIPLIER_VALEUR_BASE")
    private Boolean multiplierValeurBase;
    @Column(name = "TAUX")
    private Float taux;

    @Column(name = "ARTICLE_BUDGETAIRE")
    @Id
    String articleBudgetaire;
    @Column(name = "TYPE_TAUX")
    String typeTaux;
    @Column(name = "DEVISE")
    String devise;

    public GetTauxNonFiscal() {
        articleBudgetaire = "";
        taux = (float) 0.0;
        typeTaux = "";
        multiplierValeurBase = false;
        devise = "";
        borneSuperieure = (float) 0.0;
        borneInferieure = (float) 0.0;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }


    public String getTypeTaux() {
        return typeTaux;
    }

    public void setTypeTaux(String typeTaux) {
        this.typeTaux = typeTaux;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public float getBorneSuperieure() {
        return borneSuperieure;
    }

    public void setBorneSuperieure(float borneSuperieure) {
        this.borneSuperieure = borneSuperieure;
    }

    public float getBorneInferieure() {
        return borneInferieure;
    }

    public void setBorneInferieure(float borneInferieure) {
        this.borneInferieure = borneInferieure;
    }

    public void setBorneInferieure(Float borneInferieure) {
        this.borneInferieure = borneInferieure;
    }

//    public Float getBorneSuperieure() {
//        return borneSuperieure;
//    }

    public void setBorneSuperieure(Float borneSuperieure) {
        this.borneSuperieure = borneSuperieure;
    }

    public Boolean getMultiplierValeurBase() {
        return multiplierValeurBase;
    }

    public void setMultiplierValeurBase(Boolean multiplierValeurBase) {
        this.multiplierValeurBase = multiplierValeurBase;
    }

    public Float getTaux() {
        return taux;
    }

    public void setTaux(Float taux) {
        this.taux = taux;
    }

}
