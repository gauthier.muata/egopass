/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author moussa.toure
 */
@Embeddable
public class EmpreintesPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "PERSONNE")
    private String personne;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FINGER_ID")
    private int fingerId;

    public EmpreintesPK() {
    }

    public EmpreintesPK(String personne, int fingerId) {
        this.personne = personne;
        this.fingerId = fingerId;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public int getFingerId() {
        return fingerId;
    }

    public void setFingerId(int fingerId) {
        this.fingerId = fingerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personne != null ? personne.hashCode() : 0);
        hash += (int) fingerId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmpreintesPK)) {
            return false;
        }
        EmpreintesPK other = (EmpreintesPK) object;
        if ((this.personne == null && other.personne != null) || (this.personne != null && !this.personne.equals(other.personne))) {
            return false;
        }
        if (this.fingerId != other.fingerId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.EmpreintesPK[ personne=" + personne + ", fingerId=" + fingerId + " ]";
    }
    
}
