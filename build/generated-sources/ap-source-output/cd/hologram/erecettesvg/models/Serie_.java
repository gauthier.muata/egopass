package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Serie.class)
public class Serie_ { 

    public static volatile SingularAttribute<Serie, String> dernierNumeroImprime;
    public static volatile SingularAttribute<Serie, Date> dateCreat;
    public static volatile SingularAttribute<Serie, String> prefix;
    public static volatile SingularAttribute<Serie, Integer> fin;
    public static volatile SingularAttribute<Serie, Integer> annee;
    public static volatile SingularAttribute<Serie, Boolean> etat;
    public static volatile SingularAttribute<Serie, Integer> nombreNoteImprimee;
    public static volatile SingularAttribute<Serie, Integer> debut;
    public static volatile SingularAttribute<Serie, String> site;
    public static volatile SingularAttribute<Serie, String> typeDocument;
    public static volatile SingularAttribute<Serie, Integer> id;
    public static volatile SingularAttribute<Serie, String> agentCreat;
    public static volatile SingularAttribute<Serie, Boolean> serieFinie;

}