package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Prefix.class)
public class Prefix_ { 

    public static volatile SingularAttribute<Prefix, String> code;
    public static volatile SingularAttribute<Prefix, Integer> nombreMaximum;
    public static volatile SingularAttribute<Prefix, Boolean> etat;

}