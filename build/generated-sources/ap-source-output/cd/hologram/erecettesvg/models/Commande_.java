package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Voucher;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Commande.class)
public class Commande_ { 

    public static volatile SingularAttribute<Commande, String> reference;
    public static volatile ListAttribute<Commande, Voucher> detailsVoucherList;
    public static volatile SingularAttribute<Commande, String> notePaiement;
    public static volatile SingularAttribute<Commande, Personne> fkPersonne;
    public static volatile SingularAttribute<Commande, CompteBancaire> fkCompteBancaire;
    public static volatile SingularAttribute<Commande, Date> dateCreat;
    public static volatile SingularAttribute<Commande, ArticleBudgetaire> fkAb;
    public static volatile SingularAttribute<Commande, BigDecimal> montant;
    public static volatile SingularAttribute<Commande, Integer> id;
    public static volatile SingularAttribute<Commande, String> preuvePaiement;
    public static volatile SingularAttribute<Commande, Integer> etat;
    public static volatile SingularAttribute<Commande, Devise> devise;

}