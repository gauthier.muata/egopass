package cd.hologram.erecettesvg.pojo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(EtatSoldePaiement.class)
public class EtatSoldePaiement_ { 

    public static volatile SingularAttribute<EtatSoldePaiement, Double> solde;
    public static volatile SingularAttribute<EtatSoldePaiement, Double> credit;
    public static volatile SingularAttribute<EtatSoldePaiement, Double> debit;

}