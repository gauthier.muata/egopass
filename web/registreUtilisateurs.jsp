<%-- 
    Document   : registreUtilisateurs
    Created on : 16 avr. 2020, 10:20:32
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registre des utilisateurs</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="cmbSearchTypeUser">
                                <option value ="1">Login</option>
                                <option value ="2">Nom</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" class="form-control" style="width: 300px" id="inputSearchUser" 
                                       placeholder="Veuillez saisir le login utilisateur">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnSearchUser">
                                        <i class="fa fa-search"></i> 
                                        Rechercher</button>
                                    <button class="btn btn-success" 
                                            id="btnCreateUser"
                                            style="margin-left: 5px">
                                        <i class="fa fa-plus-circle"></i> 
                                        Créer un utilisateur</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <button id="btnCallModalSearchUser" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp;
                        Effectuer une recherche avancée</button>
                </div>

                <hr/>

                <div class="journal" >
                    <table id="tableUser" class="table table-bordered"></table>
                </div>

            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAvanceeUser.html" %>
        <%@include file="assets/include/modal/displayModalRightUser.html" %>
        <%@include file="assets/include/modal/divisionBureauModal.html" %>
        <%@include file="assets/include/modal/modalTraitementUser.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>

        <script  type="text/javascript" src="assets/js/utilisateur/registreUtililisateurs.js"></script>      

    </body>
</html>
