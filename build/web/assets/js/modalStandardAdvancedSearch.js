/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var divisionList = [], abList = [], agentList = [];
var dateDebut,
        dateFin,
        datePick1,
        datePick2;

$(function () {

    dateDebut = $("#dateDebut"),
            dateFin = $("#dateFin");

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    $(".datePicker1").datepicker("setDate", new Date());
    $(".datePicker2").datepicker("setDate", new Date());

    $('.cmbDiviision').click(function (e) {
        e.preventDefault();
        setDataBureau($('.cmbDiviision').val());
    });

    setDataDivision();

    getArticleBudgetaireImpot();

    $('.cmbAnnee').html(getYears());
    $('.cmbMois').html(getAllMounths());

    setMonthVisibility();

    $('.cmbAB').click(function (e) {
        e.preventDefault();
        setMonthVisibility($('.cmbAB').val());
    });

    $('.cmbBureau').change(function (e) {

        if ($('.cmbBureau').val() == '--') {
            
            alertify.alert('Veuillez d\'abord sélectionner un bureau valide');
            return
            
        } else {
            getAgentByBureau($('.cmbBureau').val());
        }
    });
});

function setDataDivision() {
    var data = JSON.parse(userData.utilisateurDivisionList);

    var option = '';
    if (data.length > 1) {
        option = '<option value="">--</option>';
    }
    $.each(data, function (index, item, array) {
        if (!checkDivisionExists(item.codeDivision)) {
            option += '<option value="' + item.codeDivision + '">' + item.intituleDivision + '</option>';
            var division = {};
            division.code = item.codeDivision;
            divisionList.push(division);
        }
    });
    $('.cmbDiviision').append(option);
    $('.cmbBureau').html('<option value="">--</option>');
    if (data.length == 1) {
        setDataBureau($('.cmbDiviision').val());
    }
}

function setDataBureau(codeDivision) {

    var data = JSON.parse(userData.utilisateurDivisionList);
    var option = '';

    if (data.length > 1) {
        option = '<option value="">--</option>';
    }

    $.each(data, function (index, item, array) {
        if (item.codeDivision == codeDivision) {
            option += '<option value="' + item.codeBureau + '">' + item.intituleBureau + '</option>';
        }
    });

    $('.cmbBureau').html(option);

    getAgentByBureau($('.cmbBureau').val());
}

function getAgentByBureau(bureau) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getAgentByBureau',
            'bureauCode': bureau
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                agentList = JSON.parse(JSON.stringify(response));
                var option = '<option value="" selected disabled>--</optio>';

                if (controlAccess('REGISTRE_TAXATION_USER_ALL')) {

                    $.each(response, function (index, item, array) {

                        option += '<option value="' + item.code + '">' + item.nom + '</optio>';

                    });

                } else {
                    option += '<option value="' + userData.idUser + '">' + userData.nomComplet.toUpperCase() + '</optio>';
                }

                var all = '';

                if (controlAccess('REGISTRE_TAXATION_USER_ALL')) {
                    all = 'TOUS LES AGENTS';
                    option += '<option value="*">' + all + '</optio>';
                    $('.cmbAgent').html(option);
                    $('.cmbAgent').val(all);
                } else {
                    $('.cmbAgent').html(option);
                    $('.cmbAgent').val(userData.idUser);
                }



            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getArticleBudgetaireImpot() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getArticleBudgetaireImpot',
            'codes': JSON.stringify(AB_IMPOT)
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                abList = response;
                var option = '<option value="" selected disabled>--</optio>';
                $.each(response, function (index, item, array) {
                    option += '<option value="' + item.code + '">' + item.intitule + '</optio>';
                });

                var all = 'TOUS LES IMPÔTS';

                option += '<option value="*">' + all + '</optio>';
                $('.cmbAB').html(option);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getAllMounths() {
    var months = [
        'Janvier', 'Fevrier',
        'Mars', 'Avril', 'Mai',
        'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre', 'TOUS LES MOIS'
    ];
    var option = '<option value="" selected disabled>--</option>';
    var mois = '';
    $.each(months, function (index, item, array) {
        mois = index + 1;
        mois >= 10 ? mois : mois = '0' + mois;
        option += '<option value="' + mois + '">' + item + '</option>';
    });
    return option;
}

function getYears() {
    var year = [
        '2022', '2021', '2020',
        '2019', '2018', '2017',
        '2016', '2015', '2014', 'TOUTES'
    ];
    var option = '<option value="" selected disabled>--</option>';
    $.each(year, function (index, item, array) {
        option += '<option value="' + item + '">' + item + '</option>';
    });
    return option;
}

function checkDivisionExists(code) {
    for (var i = 0; i < divisionList.length; i++) {

        if (divisionList[i].code == code) {
            return true;
        }
    }
    return false;
}

function setMonthVisibility(ab) {
    abCode = ab;

    $.each(abList, function (index, item, array) {
        if (ab == item.code) {
            if (item.codePeriodicite == PERIODICITE_MENSUELLE) {
                isMensuel = true;
                $('.divAnnee').attr('class', 'col-lg-6 col-md-6');
                $('.month').removeClass('hidden');
            } else if (item.codePeriodicite == PERIODICITE_ANNUELLE) {
                isMensuel = false;
                $('.divAnnee').attr('class', 'col-lg-12 col-md-12');
                $('.month').addClass('hidden');

            }
        }
    });


}