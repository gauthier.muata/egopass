package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Equipement;
import cd.hologram.erecettesvg.models.ProfilCfg;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ProfilEquipement.class)
public class ProfilEquipement_ { 

    public static volatile SingularAttribute<ProfilEquipement, Equipement> fkEquipement;
    public static volatile SingularAttribute<ProfilEquipement, String> dateCreat;
    public static volatile SingularAttribute<ProfilEquipement, ProfilCfg> fkProfilCfg;
    public static volatile SingularAttribute<ProfilEquipement, String> fkUser;
    public static volatile SingularAttribute<ProfilEquipement, Integer> id;
    public static volatile SingularAttribute<ProfilEquipement, String> agentCreat;
    public static volatile SingularAttribute<ProfilEquipement, Boolean> etat;

}