/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inputSearchValue;
var selectSearchDernierA;
var tableRegistreDernierAvertissement, tableRegistreDernierAvertissement2;
var btnShowAdvancedSearchDernierA;
var btnSimpleSearchDernierA;
var btnAdvencedSearchRole;
var btnCreateContrainte, btnShowAccuserReception;
var isAdvancedtypeSearch = '1';
var isAdvanced;
var inputDateDebut, inputdateLast;
var selectService, selectSite;
var dernierAvertissementList = [];
var modalTitrePerception;
var lblArticleRole, lblNameAssujetti;
var tableTitrePerception;
var codeAssujetti = '', idDA = '';

var lblMinistere, lblService, lblDateDebut, lblDateFin, lblSite, lblProvince, lblEntite, lblAgent;
var isAdvance;

var idDernierAvertissement = '';

var btnImprimerDernierAvertissement;

var btnImprimerDA;
var codeAssujetti = '';

var advancedSearchParam;

$(function () {

    typeSearch = '1';
    mainNavigationLabel.text('RECOUVREMENT');
    secondNavigationLabel.text('Registre des derniers avertissements');

    removeActiveMenu();
    linkMenuRecouvrement.addClass('active');

    if (!controlAccess('14006')) {
        window.location = 'dashboard';
    }

    inputSearchValue = $('#inputSearchValue');
    selectSearchDernierA = $('#selectSearchDernierA');
    tableRegistreDernierAvertissement = $('#tableRegistreDernierAvertissement');
    tableRegistreDernierAvertissement2 = $('#tableRegistreDernierAvertissement2');
    btnSimpleSearchDernierA = $('#btnSimpleSearchDernierA');
    btnShowAdvancedSearchDernierA = $('#btnShowAdvancedSearchDernierA');
    btnAdvencedSearchRole = $('#btnAdvencedSearchRole');
    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');
    selectService = $('#selectService');
    selectSite = $('#selectSite');
    modalTitrePerception = $('#modalTitrePerception');
    lblArticleRole = $('#lblArticleRole');
    lblNameAssujetti = $('#lblNameAssujetti');
    tableTitrePerception = $('#tableTitrePerception');
    btnCreateContrainte = $('#btnCreateContrainte');
    btnShowAccuserReception = $('#btnShowAccuserReception');
    btnImprimerDA = $('#btnImprimerDA');

    lblMinistere = $('#lblMinistere');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblSite = $('#lblSite');
    isAdvance = $('#isAdvance');
    lblEntite = $('#lblEntite');
    lblProvince = $('#lblProvince');
    lblAgent = $('#lblAgent');

    btnImprimerDernierAvertissement = $('#btnImprimerDernierAvertissement');


    selectSearchDernierA.on('change', function () {
        var critere = selectSearchDernierA.val();
        inputSearchValue.val('');
        if (critere == '1') {
            typeSearch = '1';
            inputSearchValue.attr('placeholder', 'Article du rôle');
            inputSearchValue.val();
            inputSearchValue.removeAttr('disabled');
        } else {
            typeSearch = '2';
            inputSearchValue.attr('placeholder', 'Assujetti');
            inputSearchValue.val();
            inputSearchValue.attr('disabled', 'true');
            assujettiModal.modal('show');
        }
    });

    btnSimpleSearchDernierA.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectSearchDernierA.val() === '2') {
            allSearchAssujetti = '1';
            assujettiModal.modal('show');
        } else {

            if (inputSearchValue.val().trim() == '') {
                alertify.alert('Veuillez d\'abord saisir un critère de recherche.');
                ResearchValueRole.val('');
                return;
            }
            isAdvanced = '0';
            isAdvance.attr('style', 'display: none');
            getCurrentSearchParam(0);
            loadDernierAvertissement();
        }
    });

    btnShowAdvancedSearchDernierA.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        modalRechercheAvanceeNC.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        isAdvanced = '1';
        modalRechercheAvanceeNC.modal('hide');
        isAdvance.attr('style', 'display: block;');
        getCurrentSearchParam(1);
        loadDernierAvertissement();
    });

    btnCreateContrainte.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir générer une contrainte ?', function () {

            modalAdvancedSearchAgent.modal('show');

        });

    });

    $('#btnValidateSearchAgent').click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectAgent.val() == null) {
            alertify.alert('Veuillez sélectionner un huissier');
            return;
        }

        modalAdvancedSearchAgent.modal('hide');

        generateContrainte(selectAgentHuissier.val());

    });

    btnShowAccuserReception.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        initAccuseReceptionUI(idDA, 'DA');

    });

    btnImprimerDA.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        imprimerDA();
    });

    btnImprimerDernierAvertissement.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        printerDa();
    });

    setTimeout(function () {

        var paramDashboard = getDashboardParam();

        if (paramDashboard == 'null') {

            setTimeout(function () {
                getCurrentSearchParam(1);
                isAdvanced = '1';
                btnAdvencedSearch.trigger('click');
            }, 1000);

        } else {

            modeResearch = '2';
            advancedSearchParam = JSON.parse(paramDashboard);

            setTimeout(function () {
                isAdvanced = '1';
                loadDernierAvertissement();
            }, 1000);

        }

    }, 2000);


});

function printerDa() {
    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': idDernierAvertissement,
            'operation': 'printDocumentRecouvrement'
        },
        beforeSend: function () {
            modalTitrePerception.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            modalTitrePerception.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                showResponseError();
                return;
            }
            setTimeout(function () {
                modalTitrePerception.unblock();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalDetailExtraitRole.unblock();
            showResponseError();
        }

    });
}

function printDernierAvertissement(result) {

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
    header += '<th>ASUUJETI</th>';
    header += '<th>ARTICLE EXTRAIT DE RÔLE</th>';
    header += '<th>DATE CREATION</th>';
    header += '<th>DATE RECEPTION</th>';
    header += '<th>DATE EXI.</th>';
    header += '<th>ETAT</th>';
    header += '<th style="text-align:right">MONTANT DÛ</th>';
    header += '<th style="text-align:right">MONTANT PAYE</th>';
    header += '<th style="text-align:right">RESTE A PAYER</th>';
    header += '<th></th>';
    header += '</tr></thead>';
    var data = '';
    data += '<tbody id="bodyTable">';

    dernierAvertissementList = [];

    var sumMontantDuCDF = 0;
    var sumMontantPayerCDF = 0;

    for (var i = 0; i < result.length; i++) {

        var montantDuExtrait = getSumMontantDuExtraitById(JSON.parse(result[i].titrePerceptionList));
        sumMontantDuCDF += montantDuExtrait;
        var montantPayeExtrait = getSumMontantPayeExtraitById(JSON.parse(result[i].titrePerceptionList));
        sumMontantPayerCDF += montantPayeExtrait;
        var RestePayerExtrait = montantDuExtrait - montantPayeExtrait;

        var etat = "";

        if (result[i].niveauExtraitRole == 'N5') {
            etat = "(Niveau : ATD)";
        } else if (result[i].niveauExtraitRole == 'N4') {
            etat = "(Niveau : PV DE SAISIE)";
        } else if (result[i].niveauExtraitRole == 'N3') {
            etat = "(Niveau : COMMANDEMENT)";
        } else if (result[i].niveauExtraitRole == 'N2') {
            etat = "(Niveau : CONTRAINTE)";
        } else if (result[i].niveauExtraitRole == 'N1') {
            etat = "(Niveau : DERNIER AVERTISSEMENT)";
        } else if (result[i].niveauExtraitRole == 'N0') {

            if (result[i].stateDa == 1) {
                etat = "VALIDER";
            } else if (result[i].stateDa == 2) {
                etat = "EN ATTENTE DE VALIDATION";
            } else {
                etat = "";
            }

        }

        var color = ';color: black';

        if (result[i].echeanceDaDepasser == 1 && RestePayerExtrait > 0) {
            color = ';color: red';
        }

        var firstLineASS = '';
        var firstLineAB = '';

        if (result[i].assujettiName.length > 30) {
            firstLineASS = result[i].assujettiName.substring(0, 30) + ' ...';
        } else {
            firstLineASS = result[i].assujettiName;
        }

        if (result[i].articleRole.length > 60) {
            firstLineAB = result[i].articleRole.substring(0, 60) + ' ...';
        } else {
            firstLineAB = result[i].articleRole;
        }

        var dernierAvertissement = {};
        dernierAvertissement.daId = result[i].daId;
        dernierAvertissement.articleRole = result[i].articleRole;
        dernierAvertissement.assujettiCode = result[i].assujettiCode;
        dernierAvertissement.assujettiName = result[i].assujettiName;
        dernierAvertissement.dateReceptionDa = result[i].dateReceptionDa;
        dernierAvertissement.echeanceDepassee = result[i].echeanceDaDepasser;
        dernierAvertissement.isAllPaid = RestePayerExtrait > 0 ? '0' : '1';
        dernierAvertissement.titrePerceptionList = result[i].titrePerceptionList;
        dernierAvertissement.stateDa = result[i].stateDa;
        dernierAvertissementList.push(dernierAvertissement);

        idDernierAvertissement = result[i].daId;

        data += '<tr>';
        data += '<td style="text-align:left;width:20%" title="' + result[i].assujettiName + '">' + firstLineASS + '</td>';
        data += '<td style="text-align:left;width:12%" title="' + result[i].articleRole + '">' + firstLineAB + '</td>';
        data += '<td style="text-align:left">' + result[i].dateCreate + '</td>';
        data += '<td style="text-align:left">' + result[i].dateReceptionDa + '</td>';
        data += '<td style="text-align:left' + color + '">' + result[i].dateEcheanceDa + '</td>';
        data += '<td style="text-align:left">' + etat + '</td>';
        data += '<td style="text-align:right">' + formatNumber(montantDuExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(montantPayeExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(RestePayerExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:center;width:6%;vertical-align:middle;"><a  onclick="showModalDetailDernierA(\'' + result[i].daId + '\')" class="btn btn-warning"><i class="fa fa-list"></i></a></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    data += '<tfoot>';

    data += '<tr><th colspan="6" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableRegistreDernierAvertissement.html(TableContent);

    tableRegistreDernierAvertissement.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 5,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        dom: 'Bfrtip',
        columnDefs: [{
                targets: 1,
                className: 'noVis'
            }],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(6).footer()).html(
                    formatNumber(sumMontantDuCDF, 'CDF'));

            $(api.column(7).footer()).html(
                    formatNumber(sumMontantPayerCDF, 'CDF'));

            $(api.column(8).footer()).html(
                    formatNumber(sumMontantDuCDF - sumMontantPayerCDF, 'CDF'));
        }
    });
}

function loadDernierAvertissement() {


    if (isAdvanced == 1) {


        try {

            var service = advancedSearchParam.serviceLibelle;
            var site = advancedSearchParam.siteLibelle;
            var entite = advancedSearchParam.entiteLibelle;
            var province = advancedSearchParam.provinceLibelle;
            var agent = advancedSearchParam.agentName;

            lblProvince.text(getsearchbarText(advancedSearchParam.province.length, advancedSearchParam.provinceLibelle));
            lblProvince.attr('title', province);

            lblEntite.text(getsearchbarText(advancedSearchParam.entite.length, advancedSearchParam.entiteLibelle));
            lblEntite.attr('title', entite);

            lblService.text(getsearchbarText(advancedSearchParam.service.length, advancedSearchParam.serviceLibelle));
            lblService.attr('title', service);

            lblSite.text(getsearchbarText(advancedSearchParam.site.length, advancedSearchParam.siteLibelle));
            lblSite.attr('title', site);

            lblAgent.text(getsearchbarText(advancedSearchParam.agent.length, advancedSearchParam.agentName));
            lblAgent.attr('title', agent);

            lblDateDebut.text(advancedSearchParam.dateDebut);
            lblDateFin.text(advancedSearchParam.dateFin);

        } catch (err) {
            location.reload(true);
        }


    }

    var valueResearch = inputSearchValue.val().trim();

    if (selectSearchDernierA.val() === '2') {
        valueResearch = codeAssujetti;
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'loadDernierAvertissement',
            'typeSearch': typeSearch,
            'isAdvanced': isAdvanced,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'valueSearch': valueResearch,
            'codeService': JSON.stringify(advancedSearchParam.service),
            'codeEntite': JSON.stringify(advancedSearchParam.entite),
            'codeAgent': JSON.stringify(advancedSearchParam.agent),
            'codeSite': JSON.stringify(advancedSearchParam.site)
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                printDernierAvertissement('');
                return;
            }

            setTimeout(function () {
                var dernierAvertissementList = JSON.parse(JSON.stringify(response));
                printDernierAvertissement(dernierAvertissementList);
            }
            , 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function showModalDetailDernierA(daId) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    for (var i = 0; i < dernierAvertissementList.length; i++) {

        if (dernierAvertissementList[i].daId == daId) {

            var titrePerceptionList = dernierAvertissementList[i].titrePerceptionList;

            lblArticleRole.html(dernierAvertissementList[i].articleRole);
            lblNameAssujetti.html(dernierAvertissementList[i].assujettiName);
            codeAssujetti = dernierAvertissementList[i].assujettiCode;
            idDA = dernierAvertissementList[i].daId;

            if (dernierAvertissementList[i].dateReceptionDa == '') {

                btnShowAccuserReception.attr('style', 'display:inline');

            } else {

                btnShowAccuserReception.attr('style', 'display:none');
            }
            
             if (dernierAvertissementList[i].stateDa === 3) {
                 
                 btnCreateContrainte.attr('style', 'display:none');
                 
             } else {
                 
                 if(controlAccess('14011') && dernierAvertissementList[i].isAllPaid == '0') {
                     
                      btnCreateContrainte.attr('style', 'display:block');
                      
                 } else {
                     
                      if (dernierAvertissementList[i].echeanceDepassee == '1' && dernierAvertissementList[i].isAllPaid == '0') {
                          
                           btnCreateContrainte.attr('style', 'display:block');
                           
                      } else {
                          
                          btnCreateContrainte.attr('style', 'display:none');
                      }
                 }
             }

            printTitrePerception(JSON.parse(titrePerceptionList));

            modalTitrePerception.modal('show');

            break;
        }

    }
}

function printTitrePerception(listDetailRole) {

    tempExtraitRoleId = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> </th>';
    header += '<th scope="col" > EXERCICE </th>';
    header += '<th scope="col" > SERVICE D\'ASSIETTE </th>';
    header += '<th scope="col" > ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col" > TYPE </th>';
    header += '<th scope="col" > TYPE DOCUMENT </th>';
    header += '<th scope="col" > TITRE DE PERCEPTION</th>';
    header += '<th scope="col" > DATE EXIGIBILITE </th>';
    header += '<th scope="col" style="text-align:right"> MONTANT DÛ </th>';
    header += '<th scope="col" style="text-align:right"> PENALITE DÛ </th>';
    header += '</tr></thead>';

    var body = '<tbody>';
    var firstLineAB = '';
    var firstLineSA = '';

    for (var i = 0; i < listDetailRole.length; i++) {

        var extraitRole = {};

        extraitRole.documentReference = listDetailRole[i].documentReference;
        extraitRole.documentReferenceManuel = listDetailRole[i].documentReferenceManuel;
        extraitRole.typeDocument = listDetailRole[i].typeDocument;
        extraitRole.amountPenalite = 0;

        tempExtraitRoleId.push(extraitRole);

        var penaliteDu = listDetailRole[i].TOTAL_PENALITE;

        if (listDetailRole[i].libelleArticleBudgetaire.length > 40) {
            firstLineAB = listDetailRole[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
        } else {
            firstLineAB = listDetailRole[i].libelleArticleBudgetaire.toUpperCase();
        }

        if (listDetailRole[i].SERVICE.length > 40) {
            firstLineSA = listDetailRole[i].SERVICE.substring(0, 40) + ' ...';
        } else {
            firstLineSA = listDetailRole[i].SERVICE;
        }

        var etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">NON PAYE</a></center>';

        if (listDetailRole[i].isApured == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:green">PAYE </a></center>';
        } else if (listDetailRole[i].isApured == false && listDetailRole[i].isPaid == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">PAYE</a></center>';
        }

        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="width:5%">' + listDetailRole[i].exerciceFiscal + '</td>';

        body += '<td style="width:8%" title="' + listDetailRole[i].SERVICE + '">'
                + firstLineSA.toUpperCase() + '</td>';

        body += '<td style="text-align:left;width:20%;" title="' + listDetailRole[i].libelleArticleBudgetaire
                + '">' + firstLineAB + '</td>';

        body += '<td style="width:7%">' + listDetailRole[i].TYPE + '</td>';
        body += '<td style="width:7%">' + listDetailRole[i].typeDocument + '</td>';
        body += '<td style="width:9%">' + listDetailRole[i].documentReferenceManuel + '</td>';
        body += '<td style="width:9%">' + listDetailRole[i].echeance + '</td>';
        body += '<td style="text-align:right;width:9%">' + formatNumber(listDetailRole[i].MONTANTDU, listDetailRole[i].devise) + ' ' + etatPaiement + '</td>';
        body += '<td style="text-align:right;width:9%">' + formatNumber(penaliteDu, listDetailRole[i].devise) + '</td>';


        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;

    tableTitrePerception.html(tableContent);

    var myTableDetailRole = tableTitrePerception.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        columnDefs: [
            {"visible": false, "targets": 4}
        ],
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[4, 'asc']],
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(4, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="9">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableTitrePerception tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = myTableDetailRole.row(tr);
        var dataDetail = myTableDetailRole.row(tr).data();
        var numeroTitre = dataDetail[6];

        console.log(row.child.isShown());

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {


            var tableTitreDependant = '<center><h4>LES TITRES DES PERCEPTIONS DEPENDANTS AU TITRE N° : ' + numeroTitre + '</h4><br/></center><table class="table table-bordered">';
            tableTitreDependant += '<thead><tr style="background-color:#e6ceac;color:black"><td>TITRE DE PERCEPTION</td><td>TYPE TITRE DE PERCEPTION</td><td>ECHEANCE</td><td>MONTANT DÛ</td><td>ETAT PAIEMENT</td></tr></thead>';

            var titreList = '';

            for (var j = 0; j < listDetailRole.length; j++) {

                if (listDetailRole[j].documentReferenceManuel == numeroTitre) {

                    titreList = JSON.parse(listDetailRole[j].TITRE_PERCEPTION_DEPENDACY);

                    for (var i = 0; i < titreList.length; i++) {

                        var etatPaiement = '<span style="font-weight:bold;color:red">NON PAYE</span>';
                        ;

                        if (titreList[i].isApured == true && titreList[i].isPaid == true) {
                            etatPaiement = '<span style="font-weight:bold;color:green">PAYE</span>';
                        } else if (titreList[i].isApured == false && titreList[i].isPaid == true) {
                            etatPaiement = '<span style="font-weight:bold;color:red">PAYE</span>';
                        }

                        tableTitreDependant += '<tr>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].documentReferenceManuel + '</td>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].TYPE + '</td>';

                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].echeance + '</td>';

                        tableTitreDependant += '<td style="text-align:right;width:25%;">' + formatNumber(titreList[i].MONTANTDU, 'CDF') + '</td>';
                        tableTitreDependant += '<td style="text-align:center:width:15%;">' + etatPaiement + '</td>';
                        tableTitreDependant += '</tr>';
                    }

                }
            }

            tableTitreDependant += '<tbody>';

            tableTitreDependant += '</tbody>';
            row.child(tableTitreDependant).show();

            tr.addClass('shown');

        }

    });
}

function generateContrainte(codeAgent) {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': '1404',
            'idUser': userData.idUser,
            'codeAssujetti': codeAssujetti,
            'daId': idDA,
            'huissierId': codeAgent
        },
        beforeSend: function () {

            modalTitrePerception.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Génération de la contrainte...</h5>'});
        },
        success: function (response)
        {

            modalTitrePerception.unblock();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {

                setDocumentContent(response);
                window.open('visualisation-documents', '_blank');

                alertify.alert('La contrainte est générée avec succès.');
                codeAssujetti = '';
                idDA = '';

                if (isAdvanced == '0') {
                    btnSimpleSearchDernierA.trigger('click');
                } else {
                    btnAdvencedSearchRole.trigger('click');
                }
                modalTitrePerception.modal('hide');
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalTitrePerception.unblock();
            showResponseError();
        }

    });
}

function refrechDataAfterAccuserReception() {

    if (isAdvanced == '0') {
        btnSimpleSearchDernierA.trigger('click');
    } else {
        btnAdvencedSearch.trigger('click');
    }
    modalTitrePerception.modal('hide');
}

function imprimerDA() {
    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': idDA,
            'operation': 'printDocumentRecouvrement'
        },
        beforeSend: function () {
            modalTitrePerception.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            modalTitrePerception.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucun document(Commandement) trouvé.');
                return;
            }
            setTimeout(function () {
                modalTitrePerception.unblock();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalTitrePerception.unblock();
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    isAdvanced = '0';
    isAdvance.attr('style', 'display: none');

    var responsibleObject = {};

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.nameResponsible = selectAssujettiData.nomComplet;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;

    if (selectAssujettiData.nif !== '') {
        inputSearchValue.val('Nif : ' + selectAssujettiData.nif + ' / Noms complet : ' + responsibleObject.nameResponsible);
    } else {
        inputSearchValue.val('Code : ' + selectAssujettiData.code + ' / Noms complet : ' + responsibleObject.nameResponsible);
    }
    codeAssujetti = selectAssujettiData.code;

    getCurrentSearchParam(0);
    loadDernierAvertissement();
}

function getSumMontantDuExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].MONTANTDU + listeDetailRole[i].TOTAL_PENALITE;
    }
    return sum;
}

function getSumMontantPayeExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].TOTAL_PAYEE_PENALITE + listeDetailRole[i].montantPaye;
    }

    return sum;
}