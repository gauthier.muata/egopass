package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Avis.class)
public class Avis_ { 

    public static volatile SingularAttribute<Avis, String> code;
    public static volatile SingularAttribute<Avis, String> id;
    public static volatile SingularAttribute<Avis, String> intitule;
    public static volatile SingularAttribute<Avis, Short> etat;

}