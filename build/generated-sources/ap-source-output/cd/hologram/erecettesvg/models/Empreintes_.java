package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.EmpreintesPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Empreintes.class)
public class Empreintes_ { 

    public static volatile SingularAttribute<Empreintes, EmpreintesPK> empreintesPK;
    public static volatile SingularAttribute<Empreintes, byte[]> imageEmpreinte;

}