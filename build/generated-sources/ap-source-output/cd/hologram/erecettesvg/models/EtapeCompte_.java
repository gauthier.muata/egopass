package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(EtapeCompte.class)
public class EtapeCompte_ { 

    public static volatile SingularAttribute<EtapeCompte, String> etape;
    public static volatile SingularAttribute<EtapeCompte, Integer> id;
    public static volatile SingularAttribute<EtapeCompte, Boolean> etat;
    public static volatile SingularAttribute<EtapeCompte, String> compte;

}