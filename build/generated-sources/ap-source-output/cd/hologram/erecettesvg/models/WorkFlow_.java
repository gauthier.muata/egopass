package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(WorkFlow.class)
public class WorkFlow_ { 

    public static volatile SingularAttribute<WorkFlow, String> agentMaj;
    public static volatile SingularAttribute<WorkFlow, String> process;
    public static volatile SingularAttribute<WorkFlow, Date> dateCreat;
    public static volatile SingularAttribute<WorkFlow, String> ordre;
    public static volatile SingularAttribute<WorkFlow, String> uaKo;
    public static volatile SingularAttribute<WorkFlow, String> description;
    public static volatile SingularAttribute<WorkFlow, BigDecimal> montant;
    public static volatile SingularAttribute<WorkFlow, String> ua;
    public static volatile SingularAttribute<WorkFlow, Short> controlSolde;
    public static volatile SingularAttribute<WorkFlow, Short> etat;
    public static volatile SingularAttribute<WorkFlow, Date> dateMaj;
    public static volatile SingularAttribute<WorkFlow, Integer> duree;
    public static volatile SingularAttribute<WorkFlow, Long> id;
    public static volatile SingularAttribute<WorkFlow, String> agentCreat;
    public static volatile SingularAttribute<WorkFlow, String> uaOk;
    public static volatile SingularAttribute<WorkFlow, String> metier;

}