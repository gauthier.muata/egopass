package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Jdossier.class)
public class Jdossier_ { 

    public static volatile SingularAttribute<Jdossier, String> agentMaj;
    public static volatile SingularAttribute<Jdossier, BigDecimal> soldeCreaditImpot;
    public static volatile SingularAttribute<Jdossier, Date> dateCreat;
    public static volatile SingularAttribute<Jdossier, String> libelle;
    public static volatile SingularAttribute<Jdossier, String> dossier;
    public static volatile SingularAttribute<Jdossier, String> fkAb;
    public static volatile SingularAttribute<Jdossier, Boolean> estCreditImpot;
    public static volatile SingularAttribute<Jdossier, Short> etat;
    public static volatile SingularAttribute<Jdossier, String> devise;
    public static volatile SingularAttribute<Jdossier, Date> dateMaj;
    public static volatile SingularAttribute<Jdossier, String> reference;
    public static volatile SingularAttribute<Jdossier, BigDecimal> tonnageRestant;
    public static volatile SingularAttribute<Jdossier, BigDecimal> tonnageInitial;
    public static volatile SingularAttribute<Jdossier, BigDecimal> solde;
    public static volatile SingularAttribute<Jdossier, Integer> id;
    public static volatile SingularAttribute<Jdossier, BigDecimal> debit;
    public static volatile SingularAttribute<Jdossier, BigDecimal> credit;
    public static volatile SingularAttribute<Jdossier, String> agentCreat;

}