package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.DepotDeclaration;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Tarif;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailDepotDeclaration.class)
public class DetailDepotDeclaration_ { 

    public static volatile SingularAttribute<DetailDepotDeclaration, ArticleBudgetaire> articleBudgetaire;
    public static volatile SingularAttribute<DetailDepotDeclaration, Tarif> tarif;
    public static volatile SingularAttribute<DetailDepotDeclaration, Long> code;
    public static volatile SingularAttribute<DetailDepotDeclaration, DepotDeclaration> depotDeclaration;
    public static volatile SingularAttribute<DetailDepotDeclaration, BigDecimal> taux;
    public static volatile SingularAttribute<DetailDepotDeclaration, String> fkPeriode;
    public static volatile SingularAttribute<DetailDepotDeclaration, BigDecimal> valeurBase;
    public static volatile SingularAttribute<DetailDepotDeclaration, Integer> etat;
    public static volatile SingularAttribute<DetailDepotDeclaration, Devise> devise;
    public static volatile SingularAttribute<DetailDepotDeclaration, BigDecimal> quantite;

}