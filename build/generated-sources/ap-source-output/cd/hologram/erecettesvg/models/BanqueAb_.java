package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.CompteBancaire;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(BanqueAb.class)
public class BanqueAb_ { 

    public static volatile SingularAttribute<BanqueAb, CompteBancaire> fkCompte;
    public static volatile SingularAttribute<BanqueAb, Date> dateCreat;
    public static volatile SingularAttribute<BanqueAb, ArticleBudgetaire> fkAb;
    public static volatile SingularAttribute<BanqueAb, String> fkBanque;
    public static volatile SingularAttribute<BanqueAb, Integer> id;
    public static volatile SingularAttribute<BanqueAb, String> agentCreat;
    public static volatile SingularAttribute<BanqueAb, Boolean> etat;

}