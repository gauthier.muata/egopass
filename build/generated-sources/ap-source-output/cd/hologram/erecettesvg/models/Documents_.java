package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Documents.class)
public class Documents_ { 

    public static volatile SingularAttribute<Documents, String> code;
    public static volatile SingularAttribute<Documents, String> dateCreat;
    public static volatile SingularAttribute<Documents, String> personne;
    public static volatile SingularAttribute<Documents, String> fkTypeDocument;
    public static volatile SingularAttribute<Documents, String> html;
    public static volatile SingularAttribute<Documents, String> agentCreat;
    public static volatile SingularAttribute<Documents, Boolean> etat;

}