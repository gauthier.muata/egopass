/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class MepPrint implements Serializable {
    
    String id;
    String logo;
    String codeQR;
    String numImpot;
    String nomAssujetti;
    String adresseAssujetti;
    String telephoneAssujetti;
    String emailAssujetti;
    String dateImpression;
    String numeroMed;
    String articleBudgetaire;
    String exerciceNp;
    String dateEcheanceNp;
    String dateOrdonnancement;
    String amountNp;
    String dateEcheanceMep;
    String userPrint;
    String fonctionUserPrint;
    String numeroNp;
    String dateHeureInvitation;
    String numeroDocument;
    
    String adresseBureau;
    
    public String getDateHeureInvitation() {
        return dateHeureInvitation;
    }

    public void setDateHeureInvitation(String dateHeureInvitation) {
        this.dateHeureInvitation = dateHeureInvitation;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNumImpot() {
        return numImpot;
    }

    public void setNumImpot(String numImpot) {
        this.numImpot = numImpot;
    }

    public String getNomAssujetti() {
        return nomAssujetti;
    }

    public void setNomAssujetti(String nomAssujetti) {
        this.nomAssujetti = nomAssujetti;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTelephoneAssujetti() {
        return telephoneAssujetti;
    }

    public void setTelephoneAssujetti(String telephoneAssujetti) {
        this.telephoneAssujetti = telephoneAssujetti;
    }

    public String getEmailAssujetti() {
        return emailAssujetti;
    }

    public void setEmailAssujetti(String emailAssujetti) {
        this.emailAssujetti = emailAssujetti;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroMed() {
        return numeroMed;
    }

    public void setNumeroMed(String numeroMed) {
        this.numeroMed = numeroMed;
    }

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public String getExerciceNp() {
        return exerciceNp;
    }

    public void setExerciceNp(String exerciceNp) {
        this.exerciceNp = exerciceNp;
    }

    public String getDateEcheanceNp() {
        return dateEcheanceNp;
    }

    public void setDateEcheanceNp(String dateEcheanceNp) {
        this.dateEcheanceNp = dateEcheanceNp;
    }

    public String getDateOrdonnancement() {
        return dateOrdonnancement;
    }

    public void setDateOrdonnancement(String dateOrdonnancement) {
        this.dateOrdonnancement = dateOrdonnancement;
    }

    public String getAmountNp() {
        return amountNp;
    }

    public void setAmountNp(String amountNp) {
        this.amountNp = amountNp;
    }

    public String getDateEcheanceMep() {
        return dateEcheanceMep;
    }

    public void setDateEcheanceMep(String dateEcheanceMep) {
        this.dateEcheanceMep = dateEcheanceMep;
    }

    public String getUserPrint() {
        return userPrint;
    }

    public void setUserPrint(String userPrint) {
        this.userPrint = userPrint;
    }

    public String getFonctionUserPrint() {
        return fonctionUserPrint;
    }

    public void setFonctionUserPrint(String fonctionUserPrint) {
        this.fonctionUserPrint = fonctionUserPrint;
    }

    public String getNumeroNp() {
        return numeroNp;
    }

    public void setNumeroNp(String numeroNp) {
        this.numeroNp = numeroNp;
    }

    public String getNumeroDocument() {
        return numeroDocument;
    }

    public void setNumeroDocument(String numeroDocument) {
        this.numeroDocument = numeroDocument;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdresseBureau() {
        return adresseBureau;
    }

    public void setAdresseBureau(String adresseBureau) {
        this.adresseBureau = adresseBureau;
    }
    
    
    
    
    
}
