package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.Tarif;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(RemiseSousProvisionPeage.class)
public class RemiseSousProvisionPeage_ { 

    public static volatile SingularAttribute<RemiseSousProvisionPeage, Tarif> fkTarif;
    public static volatile SingularAttribute<RemiseSousProvisionPeage, Personne> fkPersonne;
    public static volatile SingularAttribute<RemiseSousProvisionPeage, Site> fkSite;
    public static volatile SingularAttribute<RemiseSousProvisionPeage, BigDecimal> taux;
    public static volatile SingularAttribute<RemiseSousProvisionPeage, Integer> fkAxe;
    public static volatile SingularAttribute<RemiseSousProvisionPeage, Integer> id;
    public static volatile SingularAttribute<RemiseSousProvisionPeage, String> type;
    public static volatile SingularAttribute<RemiseSousProvisionPeage, Integer> etat;

}