/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var typeSearch;
var btnShowAdvancedSerachModalRole;
var btnSimpleSearchRole, ResearchTypeRole, ResearchValueRole;
var btnOrdonnancerExtraitRole;
var tableRegistreExtraitRole, tableRegistreExtraitRole2, tableDetailRole;
var tempExtraitRole = [];
var lblNameAssujetti, lblArticleRole;
var paramData;
var modalDetailExtraitRole;
var tempExtraitRoleId = [];
var titreCheckingList = [];
var advancedSearchParam = {};

var idExtraitRole;
var page;

var lblMinistere, lblService, lblDateDebut, lblDateFin, lblSite, lblEntite, lblProvince, lblAgent;
var isAdvance;
var codeAssujetti = '';

var numberRowsTitle = 0;

var dispatchId;

$(function () {

    mainNavigationLabel.text('RECOUVREMENT');
    secondNavigationLabel.text('Génération des extraits des rôles');

    if (paramData === '2') {
        mainNavigationLabel.text('RECOUVREMENT');
        secondNavigationLabel.text('Registre des extraits des rôles');
    }

    removeActiveMenu();
    linkMenuRecouvrement.addClass('active');

//    if (!controlAccess('14004')) {
//        window.location = 'dashboard';
//    }

    tableRegistreExtraitRole = $('#tableRegistreExtraitRole');
    tableRegistreExtraitRole2 = $('#tableRegistreExtraitRole2');
    tableDetailRole = $('#tableDetailRole');
    ResearchValueRole = $('#ResearchValueRole');
    btnOrdonnancerExtraitRole = $('#btnOrdonnancerExtraitRole');
    btnSimpleSearchRole = $('#btnSimpleSearchRole');
    btnShowAdvancedSerachModalRole = $('#btnShowAdvancedSerachModalRole');
    ResearchTypeRole = $('#ResearchTypeRole');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblArticleRole = $('#lblArticleRole');
    modalDetailExtraitRole = $('#modalDetailExtraitRole');

    lblMinistere = $('#lblMinistere');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblSite = $('#lblSite');
    isAdvance = $('#isAdvance');
    lblProvince = $('#lblProvince');
    lblEntite = $('#lblEntite');
    lblAgent = $('#lblAgent');

    ResearchTypeRole.on('change', function () {

        ResearchValueRole.val('');
        var paramSearch = ResearchTypeRole.val();
        switch (paramSearch) {
            case '1':
                ResearchValueRole.attr('placeholder', 'Nom de l\'assujetti');
                ResearchValueRole.val();
                ResearchValueRole.attr('disabled', 'true');
                assujettiModal.modal('show');
                break;
            case '2':
                ResearchValueRole.attr('placeholder', 'Article du rôle');
                ResearchValueRole.val();
                ResearchValueRole.removeAttr('disabled');
        }
    });

    btnSimpleSearchRole.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (ResearchTypeRole.val() === '1') {
            allSearchAssujetti = '1';
            assujettiModal.modal('show');
        } else {
            if (ResearchValueRole.val().trim() == '') {
                alertify.alert('Veuillez d\'abord saisir un critère de recherche.');
                ResearchValueRole.val('');
                return;
            }
            paramData = '1';
            typeSearch = '1';
            isAdvance.attr('style', 'display: none');
            getCurrentSearchParam(0);
            searchExtraitRole();
        }

    });

    btnShowAdvancedSerachModalRole.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        modalRechercheAvanceeNC.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        if (page == '2') {
            paramData = '2';
        } else {
            paramData = '1';
        }
        typeSearch = '2';
        isAdvance.attr('style', 'display: block;');
        getCurrentSearchParam(1);
        searchExtraitRole();
        modalRechercheAvanceeNC.modal('hide');
    });

    btnOrdonnancerExtraitRole.click(function (e) {

        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var lastNum = '';

        for (var i = 0; i < tempExtraitRoleId.length; i++) {

//            var idTitreManuel = "#inputTitreManuel_" + i;
//
//            var valueTitreManuel = $('#tableDetailRole').find(idTitreManuel).val();
//
//            if (valueTitreManuel == '') {
//                alertify.alert('Veuillez renseigner le numéro manuel du titre de perception fractionner');
//                return;
//            }

            //tempExtraitRoleId[i].npPenaliteManuel = provincePrefix + '' + valueTitreManuel;

//            if (lastNum == valueTitreManuel) {
//                alertify.alert('Ce numéro : ' + lastNum + ' de note de perception intercalaire est déjà renseigner');
//                return;
//            }

            //lastNum = valueTitreManuel;
        }

        alertify.confirm('Etes-vous sûre de vouloir générer cet extrait de rôle ?', function () {

               ordonnancerExtraitRole();

        });

    });

//    setTimeout(function () {
//        btnAdvencedSearch.trigger('click');
//    }, 1000);

    loadRegistreExtraitRole('');

});

function searchExtraitRole() {

    if (typeSearch == '2') {

        try {

            var service = advancedSearchParam.serviceLibelle;
            var site = advancedSearchParam.siteLibelle;
            var entite = advancedSearchParam.entiteLibelle;
            var province = advancedSearchParam.provinceLibelle;
            var agent = advancedSearchParam.agentName;

            lblProvince.text(getsearchbarText(advancedSearchParam.province.length, advancedSearchParam.provinceLibelle));
            lblProvince.attr('title', province);

            lblEntite.text(getsearchbarText(advancedSearchParam.entite.length, advancedSearchParam.entiteLibelle));
            lblEntite.attr('title', entite);

            lblService.text(getsearchbarText(advancedSearchParam.service.length, advancedSearchParam.serviceLibelle));
            lblService.attr('title', service);

            lblSite.text(getsearchbarText(advancedSearchParam.site.length, advancedSearchParam.siteLibelle));
            lblSite.attr('title', site);

            lblAgent.text(getsearchbarText(advancedSearchParam.agent.length, advancedSearchParam.agentName));
            lblAgent.attr('title', agent);

            lblDateDebut.text(advancedSearchParam.dateDebut);
            lblDateFin.text(advancedSearchParam.dateFin);

        } catch (err) {
            location.reload(true);
        }
    }

    var valueResearch = ResearchValueRole.val().trim();

    if (ResearchTypeRole.val() === '1') {
        valueResearch = codeAssujetti;
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'typeSearch': typeSearch,
            'articleRole': valueResearch,
            'operation': 'loadExtraitRole',
            'typeRegister': paramData,
            'codeService': JSON.stringify(advancedSearchParam.service),
            'codeSite': JSON.stringify(advancedSearchParam.site),
            'codeEntite': JSON.stringify(advancedSearchParam.entite),
            'codeAgent': JSON.stringify(advancedSearchParam.agent),
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'critereSearch': ResearchTypeRole.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                loadRegistreExtraitRole('');
                return;
            }

            setTimeout(function () {
                var extraitRoleList = JSON.parse(JSON.stringify(response));
                loadRegistreExtraitRole(extraitRoleList);
            }
            , 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadRegistreExtraitRole(result) {

    tempExtraitRole = [];
    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
    header += '<th>ARTICLE EXTRAIT DE ROLE</th>';
    header += '<th>ASSUJETTI</th>';
    header += '<th>DATE CREATION</th>';
    header += '<th style="text-align:right">TOTAL MONTANT DÛ</th>';
    header += '<th></th></tr></thead>';

    var data = '';
    data += '<tbody id="bodyTable">';

    var sumMontantDuCDF = 0;

    listePenalite.length = 0;

    for (var i = 0; i < result.length; i++) {

        var extraitRole = {};
        extraitRole.extraitRoleId = result[i].extraitRoleId;
        extraitRole.articleRole = result[i].articleRole;
        extraitRole.assujettiName = result[i].assujettiName;
        extraitRole.detailRoleList = result[i].detailRoleList;
        tempExtraitRole.push(extraitRole);

        var montantDuExtrait = getSumMontantDuExtraitById(JSON.parse(result[i].detailRoleList));
        var montantPenalite = calculPenaliteDu(JSON.parse(result[i].detailRoleList));
        var montantPayeExtrait = getSumMontantPayeExtraitById(JSON.parse(result[i].detailRoleList));
        montantDuExtrait = 0;
        montantDuExtrait -= montantPayeExtrait;
        sumMontantDuCDF += montantDuExtrait;

        data += '<tr>';

        data += '<td style="text-align:left;vertical-align:middle;">' + result[i].articleRole + '</td>';
        data += '<td style="text-align:left;width:20%;" title="' + result[i].assujettiName + '"><span style="font-weight:bold;">' + result[i].assujettiName + '</span></td>';
        data += '<td style="text-align:left;vertical-align:middle;">' + result[i].dateCreateExtraitRole + '</td>';
        data += '<td style="text-align:right;vertical-align:middle;">' + formatNumber(montantDuExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:center;width:8%;vertical-align:middle;"> <a  onclick="showModalDetailRole(\'' + result[i].extraitRoleId + '\')" class="btn btn-warning"><i class="fa fa-list"></i></a></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    data += '<tfoot>';

    data += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableRegistreExtraitRole.html(TableContent);

    tableRegistreExtraitRole.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 5,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        dom: 'Bfrtip',
        columnDefs: [{
                targets: 1,
                className: 'noVis'
            }],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(3).footer()).html(
                    formatNumber(sumMontantDuCDF, 'CDF'));
        }
    });
}

function calculPenaliteDu(detailRoleList) {

    initTotPenalite(detailRoleList);

    var penalite = 0;

    for (var i = 0; i < detailRoleList.length; i++) {

        var penaliteDu = getSumByDoc(detailRoleList[i].documentReferenceManuel);

        penalite += penaliteDu;
    }

    return penalite;
}

function getSumMontantDuExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].MONTANTDU;
    }
    return sum;
}

function getSumMontantPayeExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].montantPaye;
    }

    return sum;
}

function showModalDetailRole(extraitRoleId) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    for (var i = 0; i < tempExtraitRole.length; i++) {

        if (tempExtraitRole[i].extraitRoleId == extraitRoleId) {

            lblArticleRole.html(tempExtraitRole[i].articleRole);
            lblNameAssujetti.html(tempExtraitRole[i].assujettiName);
            idExtraitRole = extraitRoleId;

            var detailRoleList = JSON.parse(tempExtraitRole[i].detailRoleList);

            printDetailRole(detailRoleList);

            modalDetailExtraitRole.modal('show');
        }

    }
}

function printDetailRole(listDetailRole) {

    numberRowsTitle = listDetailRole.length;

    tempExtraitRoleId = [];
    titreCheckingList = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th> EXERCICE </th>';
    header += '<th> SERVICE D\'ASSIETTE </th>';
    header += '<th> ARTICLE BUDGETAIRE </th>';
    header += '<th style="text-align:left"> TITRE DE PERCEPTION </th>';
    header += '<th hidden="true" style="text-align:center" > TYPE DOCUMENT </th>';
    header += '<th> DATE EXIGIBILITE </th>';
    header += '<th style="text-align:right"> MONTANT DÛ </th>';
    header += '<th style="text-align:right;width:10%"> PENALITE DÛ</th>';
    //header += '<th style="text-align:center"> NUMERO NP PENALITE</th>';
    header += '</tr></thead>';
    var body = '<tbody id="tbodyAdresses">';

    var firstLineAB = '';
    var firstLineSA = '';
    
    

    for (var i = 0; i < listDetailRole.length; i++) {

        var penaliteDu = getSumByDoc(listDetailRole[i].documentReference);

        var extraitRole = {};
        extraitRole.documentReference = listDetailRole[i].documentReference;
        extraitRole.typeDocument = listDetailRole[i].typeDocument;
        extraitRole.amountPenalite = penaliteDu;
        extraitRole.netAPaye = listDetailRole[i].MONTANTDU;
        extraitRole.budgetArticleCode = listDetailRole[i].budgetArticleCode;
        extraitRole.assujettiCode = listDetailRole[i].assujettiCode;
        extraitRole.codeTarif = listDetailRole[i].codeTarif;
        extraitRole.libelleArticleBudgetaire = listDetailRole[i].libelleArticleBudgetaire;
        extraitRole.devise = listDetailRole[i].devise;
        extraitRole.libelleService = listDetailRole[i].libelleService;
        extraitRole.dateCreate = listDetailRole[i].dateCreate;
        extraitRole.codeOfficiel = listDetailRole[i].codeOfficiel;
        extraitRole.npPenaliteManuel = '';
        tempExtraitRoleId.push(extraitRole);

        if (listDetailRole[i].codeOfficiel !== '') {
            if (listDetailRole[i].libelleArticleBudgetaire.length > 40) {
                firstLineAB = listDetailRole[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
            } else {
                firstLineAB = listDetailRole[i].libelleArticleBudgetaire;
            }
        } else {
            firstLineAB = '';
        }


        if (listDetailRole[i].SERVICE.length > 40) {
            firstLineSA = listDetailRole[i].SERVICE.substring(0, 40) + ' ...';
        } else {
            firstLineSA = listDetailRole[i].SERVICE;
        }

        var inputTitreManuel = "inputTitreManuel_" + i;

        var etatPaiement = '';

        if (listDetailRole[i].isApured == true) {
            etatPaiement = '<br/><a href="#" style="font-weight:bold;color:green;text-align:right">PAYE</a>';
        } else if (listDetailRole[i].isApured == false && listDetailRole[i].isPaid == true) {
            etatPaiement = '<br/><a href="#" style="font-weight:bold;color:red;text-align:right">PAYE</a>';
        } else {
            etatPaiement = '<br/><a href="#" style="font-weight:bold;color:red;text-align:right">NON PAYE</a>';
        }

        var typeTitreDoc = '<br/><center><a href="#" style="font-weight:bold;color:red">' + '(' + listDetailRole[i].typeDocument + ')' + '</a></center>';

        body += '<tr>';
        body += '<td style="width:4%">' + listDetailRole[i].exerciceFiscal + '</td>';

        body += '<td title="' + listDetailRole[i].SERVICE + '">'
                + firstLineSA.toUpperCase() + '</td>';

        body += '<td style="text-align:left;width:18%;" title="' + listDetailRole[i].libelleArticleBudgetaire + '">' + firstLineAB + '</td>';

        body += '<td style="width:8%;text-align:center">' + listDetailRole[i].documentReference + ' ' + typeTitreDoc + '</td>';
        body += '<td style="width:9%" hidden="true">' + listDetailRole[i].typeDocument + '</td>';
        body += '<td style="width:6%">' + listDetailRole[i].echeance + '</td>';
        body += '<td style="text-align:right;width:9%">' + formatNumber(listDetailRole[i].MONTANTDU, listDetailRole[i].devise) + ' ' + etatPaiement + '</td>';
        body += '<td style="text-align:right;width:9%">' + formatNumber(penaliteDu, listDetailRole[i].devise) + '</td>';
        //body += '<td style="text-align:left;width:14%;"><center><span style="font-weight: bold;text-align:center"></span><input id="' + inputTitreManuel + '" onchange="checkValidateTitre(' + inputTitreManuel + ',\'' + listDetailRole[i].codeSite + '\',\'' + listDetailRole[i].codeService + '\',\'' + listDetailRole[i].codeEntite + '\',\'' + listDetailRole[i].codeProvince + '\')" type="number" style="width : 70%""></input></center></td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableDetailRole.html(tableContent);
    tableDetailRole.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[1, 'asc']],
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        columnDefs: [
            {"visible": false, "targets": 1}
        ],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(1, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold" ><td colspan="7">' + group + '</td></tr>'
                            );
                    last = group;
                }
            });
        }
    });
}

function getAdresseAssujetti(detailRoleList) {

    var listDetailRole = JSON.parse(detailRoleList);

    var adresse = "";

    for (var i = 0; i < listDetailRole.length; i++) {

        adresse = listDetailRole[i].adresseAssujetti;
    }

    return adresse;
}

function ordonnancerExtraitRole() {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'userId': userData.idUser,
            'SiteCode': userData.SiteCode,
            'extraitRoleId': idExtraitRole,
            'idDispatch': dispatchId,
            'detailRoleList': JSON.stringify(tempExtraitRoleId),
            'operation': '1409'
        },
        beforeSend: function () {
            modalDetailExtraitRole.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Génération de l\'extrait de rôle en cours...</h5>'});
        },
        success: function (response)
        {

            modalDetailExtraitRole.unblock();

            if (response == '-1' || response == '0') {

                showResponseError();
                return;
            }

            setDocumentContent(response);
            window.open('document-paysage', '_blank');

            setTimeout(function () {

                alertify.alert('La génération de l\'extrait de rôle s\'est effectuée avec succès.');
                if (typeSearch == '1') {
                    btnSimpleSearchRole.trigger('click');
                } else {
                    btnAdvencedSearch.trigger('click');
                }
                modalDetailExtraitRole.modal('hide');
            }
            , 2000);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalDetailExtraitRole.unblock();
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    paramData = '1';
    typeSearch = '1';
    isAdvance.attr('style', 'display: none');


    var responsibleObject = {};

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.nameResponsible = selectAssujettiData.nomComplet;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;

    if (selectAssujettiData.nif !== '') {
        ResearchValueRole.val('Nif : ' + selectAssujettiData.nif + ' / Noms complet : ' + responsibleObject.nameResponsible);
    } else {
        ResearchValueRole.val('Code : ' + selectAssujettiData.code + ' / Noms complet : ' + responsibleObject.nameResponsible);
    }
    codeAssujetti = selectAssujettiData.code;

    getCurrentSearchParam(0);
    searchExtraitRole();
}

function initTotPenalite(listDetailRole) {

    for (var i = 0; i < listDetailRole.length; i++) {

        calculteInteretMoratoireV2ByDoc(
                listDetailRole[i].documentReferenceManuel,
                listDetailRole[i].MONTANTDU,
                0,
                listDetailRole[i].nombreMois,
                listDetailRole[i].devise
                );
    }

}



function checkValidateTitre(titreManuel, codeSite, codeService, codeEntite, codeProvince) {

    titreObj = {};
    var valueTitreChecking = $(titreManuel).val();
    var valueTitreComposite = provincePrefix + '' + $(titreManuel).val();
    titreObj.titreChecking = $(titreManuel).val();
    codeEntite = userData.idUser;

    for (var j = 0; j < titreCheckingList.length; j++) {

        if (titreCheckingList[j].titreChecking == valueTitreChecking) {
            alertify.alert('Ce numéro : ' + valueTitreComposite + ' de note de perception intercalaire est déjà renseigner');
            $(titreManuel).val('');
            return;
        }
    }

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'valueTitrePrincipalChecking': valueTitreChecking,
            'valueTitrePrincipalComposite': valueTitreComposite,
            'withPenality': 0,
            'serviceCode': codeService,
            'codeEntite': codeEntite,
            'SiteCode': codeSite,
            'ordonnancementNormal': 0,
            'provinceCode': codeProvince,
            'operation': 'checkingTitreManuel'
        },
        beforeSend: function () {
            modalDetailExtraitRole.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification du numéro en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalDetailExtraitRole.unblock();
                responseChecking = response;

                switch (responseChecking) {
                    case '2':
                        alertify.alert('Ce numéro de note de perception principale est déjà utilisé');
                        $(titreManuel).val('');
                        break;
                    case '3':
                        alertify.alert('Ce numéro de note de perception n\'existe dans votre plage d\'affectation.');
                        $(titreManuel).val('');
                        break;
                    case '4':
                        alertify.alert('Ce numéro de note de perception de pénalité est déjà utilisé');
                        $(titreManuel).val('');
                        break;
                    default:
                        dispatchId = responseChecking;
                        titreCheckingList.push(titreObj);
                        alertify.alert('OK');
                        break;
                }

            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalDetailExtraitRole.unblock();
            showResponseError();
        }
    });


}

function callThisMethod(response) {

    switch (response) {
        case '2':
            alertify.alert('Ce numéro de note de perception principale est déjà utilisé');
            break;
        case '3':
            alertify.alert('Ce numéro de note de perception n\'existe dans votre plage d\'affectation.');
            break;
        case '4':
            alertify.alert('Ce numéro de note de perception de pénalité est déjà utilisé');
            break;
        default:
            dispatchId = response;
            titreCheckingList.push(titreObj);
            break;
    }
}

//function getCurrentSearchParam(isAdvenced) {
//
//    if (isAdvenced == 0) {
//
//        if (!canViewAllSiteInService) {
//            var mySite = '[{"SiteCode":"' + userData.SiteCode + '"}]';
//            advancedSearchParam.site = JSON.parse(mySite);
//        } else {
//            advancedSearchParam.site = JSON.parse(userData.allSiteList);
//        }
//
//        if (!canViewAllAgentInSite) {
//            var myAgent = '[{"codeAgent":"' + userData.idUser + '"}]';
//            advancedSearchParam.agent = JSON.parse(myAgent);
//        } else {
//            advancedSearchParam.agent = JSON.parse(userData.agentList);
//        }
//
//        advancedSearchParam.service = JSON.parse(userData.allServiceList);
////        advancedSearchParam.entite = JSON.parse(userData.allEntiteList);
////        advancedSearchParam.province = JSON.parse(userData.allProvince);
////        advancedSearchParam.banque = JSON.parse(userData.banqueUserList);
//
//
//    } else {
//
//        try {
//            
//            advancedSearchParam.site = selectSite.val();
//            advancedSearchParam.service = selectService.val();
//            advancedSearchParam.entite = selectEntite.val();
//            advancedSearchParam.province = selectProvince.val();
//            advancedSearchParam.banque = selectBanque.val();
//            advancedSearchParam.agent = selectAgent.val();
//
//            advancedSearchParam.dateDebut = $('#inputDateDebut').val();
//            advancedSearchParam.dateFin = $('#inputdateLast').val();
//            advancedSearchParam.serviceLibelle = $('#selectService option:selected').text();
//            advancedSearchParam.siteLibelle = $('#selectSite option:selected').text();
//            advancedSearchParam.provinceLibelle = $('#selectProvince option:selected').text();
//            advancedSearchParam.entiteLibelle = $('#selectEntite option:selected').text();
//            advancedSearchParam.agentName = $('#selectAgent option:selected').text();
//            advancedSearchParam.banqueLibelle = $('#selectBanque option:selected').text();
//
//        } catch (err) {
//
//            location.reload(true);
//        }
//    }
//
//}

