package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.DemandeEchelonnement;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailEchelonnement.class)
public class DetailEchelonnement_ { 

    public static volatile SingularAttribute<DetailEchelonnement, String> dateCreat;
    public static volatile SingularAttribute<DetailEchelonnement, String> numeroTitre;
    public static volatile SingularAttribute<DetailEchelonnement, String> observation;
    public static volatile SingularAttribute<DetailEchelonnement, DemandeEchelonnement> fkDemandeEchelonnement;
    public static volatile SingularAttribute<DetailEchelonnement, Integer> id;
    public static volatile SingularAttribute<DetailEchelonnement, String> typeTitre;
    public static volatile SingularAttribute<DetailEchelonnement, String> dateValidation;
    public static volatile SingularAttribute<DetailEchelonnement, Integer> etat;
    public static volatile SingularAttribute<DetailEchelonnement, Agent> agentValidation;

}