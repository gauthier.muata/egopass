package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TypeDocument.class)
public class TypeDocument_ { 

    public static volatile SingularAttribute<TypeDocument, String> code;
    public static volatile SingularAttribute<TypeDocument, String> fenetre;
    public static volatile SingularAttribute<TypeDocument, Boolean> editable;
    public static volatile SingularAttribute<TypeDocument, Boolean> htmlPrint;
    public static volatile SingularAttribute<TypeDocument, String> anchor;
    public static volatile SingularAttribute<TypeDocument, String> anchorStyle;
    public static volatile SingularAttribute<TypeDocument, Boolean> controlSolde;
    public static volatile SingularAttribute<TypeDocument, String> intitule;
    public static volatile SingularAttribute<TypeDocument, Short> etat;
    public static volatile SingularAttribute<TypeDocument, Integer> echeance;

}