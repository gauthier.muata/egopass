package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.UtilisateurDivision;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Division.class)
public class Division_ { 

    public static volatile SingularAttribute<Division, String> code;
    public static volatile SingularAttribute<Division, EntiteAdministrative> entiteAdministrative;
    public static volatile ListAttribute<Division, UtilisateurDivision> utilisateurDivisionList;
    public static volatile SingularAttribute<Division, String> intitule;
    public static volatile SingularAttribute<Division, Short> etat;
    public static volatile SingularAttribute<Division, String> sigle;

}