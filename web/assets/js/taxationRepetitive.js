/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var dataAssujettissement;

var lblLegalForm, lblNifResponsible, lblNameResponsible, lblAddress, lblArticleBudgetaire, lblPeriodiciteName,
        lblInfoArticle, infosBaseCalculModal, inputBaseCalculOnCasePourcent, lblUniteBase, btnValidateInfoBase;

var tablePeriodeDeclaration;

var periodeDeclarationList = [];
var periodeDeclarationSelected;
var articleDependanceList = [];

var idAddress, codeResponsible, idAssujettissement;

var typeTaux, taux, devise, multiplierParBase, valeurBaseAss, unite, modalLongBaseTaxableTitle;
var dataDeclaration, lblAncienneNote, divOldNC;
var deviseNC;

var echeancePaiementExist;

var isCorrection = 0, oldNC = '';
var codeDoc = undefined;
var codeTypeDocs = 'TD00000035';
var chechArticle;
var codeBudgetaire;
var addNewPenalite = false;
var divInfosExo;
var montantDuPeriode;

var periodeIDSelected;
var fromCall;

var penalitieList = [];

var amountPenalite;
var amountPenaliteFormat;
var medIDSelected;
var numberRows;
var deviseSelected;

var modalFichePriseEnCharge;

$(function () {

    fromCall = '0';
    numberRows = empty;

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('TAXATION -- Périodique');

    removeActiveMenu();
    linkMenuTaxation.addClass('active');

    var urlCodeAssujettissement = getUrlParameter('id');
    var urlPeriode = getUrlParameter('periode');

    if (jQuery.type(urlCodeAssujettissement) !== 'undefined') {
        idAssujettissement = atob(urlCodeAssujettissement);
    }

    if (jQuery.type(urlPeriode) !== 'undefined') {
        idAssujettissement = atob(urlPeriode);
        isCorrection = 1;
    }

    lblLegalForm = $('#lblLegalForm');
    lblNifResponsible = $('#lblNifResponsible');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblArticleBudgetaire = $('#lblArticleBudgetaire');
    lblPeriodiciteName = $('#lblPeriodiciteName');
    lblInfoArticle = $('#lblInfoArticle');
    tablePeriodeDeclaration = $('#tablePeriodeDeclaration');
    infosBaseCalculModal = $('#infosBaseCalculModal');
    inputBaseCalculOnCasePourcent = $('#inputBaseCalculOnCasePourcent');
    lblUniteBase = $('#lblUniteBase');
    modalLongBaseTaxableTitle = $('#modalLongBaseTaxableTitle');
    btnValidateInfoBase = $('#btnValidateInfoBase');
    lblAncienneNote = $('#lblAncienneNote');
    divOldNC = $('#divOldNC');
    divInfosExo = $('#divInfosExo');

    modalFichePriseEnCharge = $('#modalFichePriseEnCharge');

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    loadAssujettissement();

    btnValidateInfoBase.click(function (e) {
        e.preventDefault();
        updateBaseTaxable();
    });

    lblAncienneNote.on('click', function (e) {
        e.preventDefault();
        setRegisterType('RNT');
        window.open('registre-notes-taxations?nc=' + btoa(lblAncienneNote.text()), '_blank');
    });

    if (!controlAccess('3010')) {
        var checkControl = $('#blockCancelPenalite');
        checkControl.hide();
    }

});

function loadAssujettissement() {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'idAssujettissement': idAssujettissement,
            'isCorrection': isCorrection,
            'operation': 'researchTaxationRepetitive'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des données en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {

                    printResultPeriodeDeclaration('');

                    alertify.alert('Une erreur inattendue s\'est produite lors de la récupération des données. <br/><br/> Soit l\'assujettissement n\'a plus des périodes valides. <br/><br/> Soit il s\'agit de la correction d\'un assujettissement multiple. Allez à via la déclaration');

                } else {
                    dataAssujettissement = JSON.parse(JSON.stringify(response));
                    if (dataAssujettissement.length > 0) {

                        //alert(JSON.stringify(dataAssujettissement));

                        displayInfosAssujettissement(dataAssujettissement);
                    } else {
                        printResultPeriodeDeclaration('');
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function displayInfosAssujettissement(dataAssujettissement) {

    lblLegalForm.html(dataAssujettissement[0].libelleFormeJuridique);
    lblNifResponsible.html(dataAssujettissement[0].nif);
    lblNameResponsible.html(dataAssujettissement[0].nomCompletAssujetti);
    lblAddress.html(dataAssujettissement[0].adresseAssujetti);

    if (dataAssujettissement[0].codeBudgetaire !== '') {
        lblArticleBudgetaire.html(dataAssujettissement[0].codeBudgetaire + ' / ' +
                dataAssujettissement[0].libelleArticleBudgetaire);
        codeBudgetaire = dataAssujettissement[0].codeBudgetaire;
    } else {
        lblArticleBudgetaire.html(dataAssujettissement[0].libelleArticleBudgetaire);
        codeBudgetaire = '';
    }

    lblPeriodiciteName.html(dataAssujettissement[0].periodiciteName);

    typeTaux = dataAssujettissement[0].typeTaux;
    codeResponsible = dataAssujettissement[0].codeResponsible;
    idAddress = dataAssujettissement[0].codeAdresse;
    taux = dataAssujettissement[0].tauxPalier;
    valeurBaseAss = dataAssujettissement[0].valeurBase;
    devise = dataAssujettissement[0].devisePalier;
    multiplierParBase = dataAssujettissement[0].multiplierParValeurBase;
    echeancePaiementExist = dataAssujettissement[0].echeancePaiementExist;
    unite = dataAssujettissement[0].unitePalier;

    switch (typeTaux) {
        case 'F':
            lblInfoArticle.html(formatNumber(dataAssujettissement[0].tauxPalier,
                    dataAssujettissement[0].devisePalier));
            break;
        case '%':
            lblInfoArticle.html(formatNumberOnly(dataAssujettissement[0].tauxPalier) + '%');
            break;
    }

    if (dataAssujettissement[0].periodeDeclarationList.length > 0) {

        periodeDeclarationList = JSON.parse(dataAssujettissement[0].periodeDeclarationList);

        /*if (dataAssujettissement[0].articleDependanceList.length > 0) {
         articleDependanceList = JSON.parse(dataAssujettissement[0].articleDependanceList);
         }*/


        if (isCorrection == 1) {
            divOldNC.show();
            oldNC = periodeDeclarationList[0].ncSource;
            lblAncienneNote.html(periodeDeclarationList[0].ncSource);
        }

        printResultPeriodeDeclaration(periodeDeclarationList);

    } else {
        printResultPeriodeDeclaration('');
    }

}

function refreshPeriodeDeclarationList() {
    setTimeout(function () {
        window.location = 'taxation-repetitive?id=' + btoa(idAssujettissement);
    }, 1500);
}

function printResultPeriodeDeclaration(periodeDeclarationList) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center" scope="col">EXERCICE</th>';
    tableContent += '<th style="text-align:center" scope="col">ECHEANCE</th>';

    tableContent += '<th style="text-align:center" scope="col">MOIS RETARD</th>';

    if (echeancePaiementExist == 1) {

        tableContent += '<th style="text-align:center" scope="col">ECHEANCE PAIEMENT</th>';
    }

    tableContent += '<th style="text-align:center" scope="col">TAUX AB.</th>';

    if (multiplierParBase == 1) {
        tableContent += '<th style="text-align:center" scope="col">BASE TAXABLE</th>';
    }


    tableContent += '<th style="text-align:center" scope="col">MONTANT DU</th>';
    tableContent += '<th style="text-align:center" scope="col">PENALITE DU</th>';
    tableContent += '<th style="text-align:center" scope="col">PIECES JOINTES</th>';
    tableContent += '<th style="text-align:left" scope="col"></th>';
    tableContent += '</tr></thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < periodeDeclarationList.length; i++) {

        var colorEcheance = '', colorEcheancePaiement = '';

        switch (periodeDeclarationList[i].estPenalise) {
            case '1':
                colorEcheance = 'color: red';
                break;
            case '0':
                colorEcheance = 'color: black';
                break;
        }

        switch (periodeDeclarationList[i].estPenalisePaiement) {
            case '1':
                colorEcheancePaiement = 'color: red';
                break;
            case '0':
                colorEcheancePaiement = 'color: black';
                break;
        }

        var base = 1;
        var montantDu = 0;
        var penaliteDu = 0;
        var isRecidiviste;

        if (multiplierParBase == 1) {

            base = valeurBaseAss == 0 ? 0 : valeurBaseAss;
            montantDu = getAmout(base);

            if (valeurBaseAss > 0) {

                if (periodeDeclarationList[i].estPenalise == '1') {
                    isRecidiviste = periodeDeclarationList[i].estRecidiviste;
                }
            }

        } else {

            montantDu = getAmout(base);

            if (periodeDeclarationList[i].estPenalise == '1') {
                var isRecidiviste = periodeDeclarationList[i].estRecidiviste;
            }
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:center">' + periodeDeclarationList[i].periodeDeclaration + '</td>';
        tableContent += '<td style="text-align:center;' + colorEcheance + '">' + periodeDeclarationList[i].echeance + '</td>';

        tableContent += '<td style="text-align:center;' + colorEcheancePaiement + '">' + periodeDeclarationList[i].moisRetard + '</td>';


        if (echeancePaiementExist == 1) {

            tableContent += '<td style="text-align:center;' + colorEcheancePaiement + '">' + periodeDeclarationList[i].echeancePaiement + '</td>';
        }

        tableContent += '<td style="text-align:center">' + getTauxLabel() + '</td>';

        if (multiplierParBase == 1) {
            if (valeurBaseAss > 0) {
                tableContent += '<td style="text-align:center"><input disabled id="inputBase_' + periodeDeclarationList[i].idPeriodeDeclaration + '" value="' + valeurBaseAss + '" class="form-control" placeholder="Base taxable" /></td>';
            } else {

                tableContent += '<td style="text-align:center"><input id="inputBase_' + periodeDeclarationList[i].idPeriodeDeclaration
                        + '"  onclick="showBaseTaxableInputV2('
                        + periodeDeclarationList[i].idPeriodeDeclaration + ','
                        + periodeDeclarationList[i].exoNumberActInit + ','
                        + periodeDeclarationList[i].exoBorneInferieure + ','
                        + periodeDeclarationList[i].exoBorneSuperieure
                        + ')" class="form-control" placeholder="Base taxable" /></td>';
            }

        }

        generatePenalies(periodeDeclarationList[i].idPeriodeDeclaration, montantDu, devise, isRecidiviste, periodeDeclarationList[i].moisRetard, 1);

        amountPenalite = 0;

        tableContent += '<td style="text-align:center"><span id="lblAmount_' + periodeDeclarationList[i].idPeriodeDeclaration + '">' + formatNumber(montantDu, devise) + '</span></td>';

        if (periodeDeclarationList[i].estPenalise == '1') {

            amountPenalite = getSumByReference(periodeDeclarationList[i].idPeriodeDeclaration);
            amountPenaliteFormat = formatNumber(amountPenalite, devise);

            tableContent += '<td style="text-align:center"><span id="lblPenalite_' + periodeDeclarationList[i].idPeriodeDeclaration + '">' + amountPenaliteFormat + '</span>&nbsp;&nbsp;<a  onclick="updatePenaliteV2(\'' + montantDu + '\',\'' + periodeDeclarationList[i].idPeriodeDeclaration + '\',\'' + devise + '\',\'' + isRecidiviste + '\',\'' + periodeDeclarationList[i].moisRetard + '\',\'' + periodeDeclarationList[i].periodeDeclaration + '\')"><i style="color:red" class="fa fa-edit"></i><a/></td>';
        } else {
            tableContent += '<td style="text-align:center"><span id="lblPenalite_' + periodeDeclarationList[i].idPeriodeDeclaration + '">' + formatNumber(amountPenalite, devise) + '</span></td>';
        }

        tableContent += '<td style="text-align:center"><input type="checkbox" checked="true" disabled="disabled" id="cb_Declaration' + periodeDeclarationList[i].idPeriodeDeclaration + '" name="cb_Declaration' + periodeDeclarationList[i].idPeriodeDeclaration + '"></td>';
        tableContent += '<td style="text-align:center"><button class="btn btn-success" onclick="createNoteCaluclFromPeriodeDeclaration(\'' + periodeDeclarationList[i].idPeriodeDeclaration + '\',\'' + periodeDeclarationList[i].estPenalise + '\',\'' + periodeDeclarationList[i].periodeDeclaration + '\')"><i class="fa fa-check-circle"></i> Taxer</button></td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tablePeriodeDeclaration.html(tableContent);

    var myDataTable = tablePeriodeDeclaration.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "La liste des périodes de déclaration est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });

    $('#tablePeriodeDeclaration tbody').on('click', 'tr', function () {
        periodeDeclarationSelected = myDataTable.row(this).data();
    });
}

var selectedBaseID,
        selectedExoTaux, qteExoInit, borneInferieure, borneSuperieure;

function showBaseTaxableInput(id, taux, qteInit, borneInf, borneSup) {

    for (var i = 0; i < periodeDeclarationList.length; i++) {
        if (id == periodeDeclarationList[i].idPeriodeDeclaration) {
            exerciceFiscal = periodeDeclarationList[i].periodeDeclaration;
            break;
        }
    }

    selectedBaseID = id;
    selectedExoTaux = taux;
    qteExoInit = qteInit;
    borneInferieure = borneInf;
    borneSuperieure = borneSup;
    lblUniteBase.html(unite);
    modalLongBaseTaxableTitle.html('Déclaration pour : ' + exerciceFiscal);
    var base = $("#inputBase_" + selectedBaseID).val();
    inputBaseCalculOnCasePourcent.val(base);

    if (selectedExoTaux == 0) {
        divInfosExo.hide();
    } else {
        printMessageExo();
    }

    infosBaseCalculModal.modal('show');
}

function showBaseTaxableInputV2(id, qteInit, borneInf, borneSup) {

    for (var i = 0; i < periodeDeclarationList.length; i++) {
        if (id == periodeDeclarationList[i].idPeriodeDeclaration) {
            exerciceFiscal = periodeDeclarationList[i].periodeDeclaration;
            break;
        }
    }

    selectedBaseID = id;
    selectedExoTaux = 0;
    qteExoInit = qteInit;
    borneInferieure = borneInf;
    borneSuperieure = borneSup;
    lblUniteBase.html(unite);
    modalLongBaseTaxableTitle.html('Déclaration pour : ' + exerciceFiscal);
    var base = $("#inputBase_" + selectedBaseID).val();
    inputBaseCalculOnCasePourcent.val(base);

    if (selectedExoTaux == 0) {
        divInfosExo.hide();
    } else {
        printMessageExo();
    }

    infosBaseCalculModal.modal('show');
}

function updateBaseTaxable() {

    for (var i = 0; i < periodeDeclarationList.length; i++) {
        if (selectedBaseID == periodeDeclarationList[i].idPeriodeDeclaration) {
            var isRecidiviste = periodeDeclarationList[i].estRecidiviste;
            isPenality = periodeDeclarationList[i].estPenalise;
            break;
        }
    }

    infosBaseCalculModal.modal('hide');

    var base = inputBaseCalculOnCasePourcent.val();
    var montantDu = getAmout(base);

    var caseExo = (qteExoInit != 0)
            ? 1 : (borneInferieure != 0 || borneSuperieure != 0)
            ? 2 : 3;

    switch (caseExo) {
        case 1:
            // Cas de la quantité | On exonère pas quand la qte n'est pas égale à la valeur de base
            if (qteExoInit != base) {
                selectedExoTaux = 0;
            }
            break;
        case 2:
            // Cas des bornes
            if (borneInferieure != 0 && borneSuperieure != 0) {
                // la base doit être comprise dans les bornes
                if (!(base >= borneInferieure && base <= borneSuperieure)) {
                    selectedExoTaux = 0;
                }
            } else if (borneInferieure != 0) {
                // la base doit être supérieure ou égale à la borne inférieure
                if (base < borneInferieure) {
                    selectedExoTaux = 0;
                }
            } else {
                // la base doit être inférieure ou égale à la borne supérieure
                if (base > borneSuperieure) {
                    selectedExoTaux = 0;
                }
            }
            break;
        default :
            // Ni l'un ni l'autre
            // Aucune contrainte sur la quantité ou les bornes
    }

    var montantExo = getExoAmount(selectedExoTaux, montantDu);
    var amoutPenalite = 0;

    if (isPenality == '1') {
        amoutPenalite = calculPenaliteAssiete(selectedBaseID, montantDu - montantExo, isRecidiviste, devise);
    }

    if (isUndefined(amoutPenalite)) {
        amoutPenalite = 0;
    }

    $("#inputBase_" + selectedBaseID).val(base);
    $("#lblInitAmount_" + selectedBaseID).html(formatNumber(montantDu, devise));
    $("#lblExoAmount_" + selectedBaseID).html(formatNumber(montantExo, devise));
    $("#lblAmount_" + selectedBaseID).html(formatNumber(montantDu - montantExo, devise));
    $("#lblPenalite_" + selectedBaseID).html(formatNumber(amoutPenalite, devise));
}

function printMessageExo() {

    var data;

    var caseExo = (qteExoInit != 0)
            ? 1 : (borneInferieure != 0 || borneSuperieure != 0)
            ? 2 : 3;

    switch (caseExo) {

        case 1:

            data = '<div class="row">';
            data += '<div class="col-lg-6"><strong>Exonération : </strong>' + selectedExoTaux + '%</div>';
            data += '<div class="col-lg-6"><strong>Base taxable egale à : </strong>' + qteExoInit + '</div>';
            data += "</div>";

            break;

        case 2:

            data = '<div class="row">';
            data += '<div class="col-lg-6"><strong>Exonération : </strong>' + selectedExoTaux + '%</div>';

            if (borneInferieure != 0 && borneSuperieure != 0) {
                data += '<div class="col-lg-6"><strong>Base taxable comprise entre: </strong>' + borneInferieure + ' et ' + borneSuperieure + '</div>';
            } else if (borneInferieure != 0) {
                data += '<div class="col-lg-6"><strong>Base taxable supérieure ou égale à : </strong>' + borneInferieure + '</div>';
            } else {
                data += '<div class="col-lg-6"><strong>Base taxable inférieure ou égale à : </strong>' + borneSuperieure + '</div>';
            }

            data += "</div>";

            break;

        default:

            data = '<div class="row">';
            data += '<div class="col-lg-12"><strong>Exonération : </strong>' + selectedExoTaux + '%</div>';
            data += "</div>";
    }


    divInfosExo.show();
    divInfosExo.html(data);
}

function updatePenaliteV2(montant, periodeID, devise, isRecidiviste, monthLate, periodeName) {

    periodeIDSelected = periodeID
    deviseSelected = devise;

    var obj = {};

    obj.amount = parseFloat(montant);
    obj.reference = periodeID;
    obj.libelleReference = periodeName;
    obj.currency = devise;
    obj.isRecidiviste = isRecidiviste == 1 ? true : false;
    obj.monthLate = parseInt(monthLate);
    obj.case = 1;

    initUIPenality(obj);
}

function updatePenalite(idPeriodeDeclaration) {

    if (!addNewPenalite) {
        alertify.alert('Désolé,Vous ne pouvez pas ajouter d\'autres pénalités');
        return;
    }

    var valBase = 1;

    if (multiplierParBase == 1) {

        valBase = $("#inputBase_" + idPeriodeDeclaration).val();
        if (valBase == empty || valBase == '0') {
            alertify.alert('Veuillez fournir une base taxable correct');
            return;
        }
    }

    for (var i = 0; i < periodeDeclarationList.length; i++) {

        if (idPeriodeDeclaration == periodeDeclarationList[i].idPeriodeDeclaration) {

            var initMontantDu = getAmout(valBase);
            var montantExo = getExoAmount(periodeDeclarationList[i].exoTaux, initMontantDu);
            amountDu = initMontantDu - montantExo;
            var echeance = periodeDeclarationList[i].echeancePaiement;
            var moisRetard = periodeDeclarationList[i].moisRetard;
            var isRecidiviste = periodeDeclarationList[i].estRecidiviste;
            alert(isRecidiviste);
            exerciceFiscal = periodeDeclarationList[i].periodeDeclaration;
            break;
        }

    }


    var ncData = {};

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();

    ncData.numeroLabelDocument = exerciceFiscal;
    ncData.numeroDocument = idPeriodeDeclaration;
    ncData.nomAssujetti = dataAssujettissement[0].nomCompletAssujetti;
    ncData.catAssujetti = dataAssujettissement[0].libelleFormeJuridique;
    ncData.codeAssujetti = dataAssujettissement[0].codeResponsible;
    ncData.dateCreate = day + '/' + month++ + '/' + year;
    ncData.montantDu = amountDu;
    ncData.dateEcheance = echeance;
    ncData.devise = devise;
    ncData.moisRetard = moisRetard;
    ncData.isRecidiviste = isRecidiviste;

    isTaxation = 1;
    initUI(ncData);
    modalpenalite.modal('show');
}

var applyPenalite = 1;
function createNoteCaluclFromPeriodeDeclaration(idPeriodeDeclaration, penalise, periodeName) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    var valBase = 1;

    if (multiplierParBase == 1) {

        var valBase = $("#inputBase_" + idPeriodeDeclaration).val();

        if (valBase == empty || valBase == '0') {
            alertify.alert('Veuillez fournir une base taxable correct');
            return;
        }
    }

    for (var i = 0; i < periodeDeclarationList.length; i++) {

        if (idPeriodeDeclaration == periodeDeclarationList[i].idPeriodeDeclaration) {

            var initMontantDu = getAmout(valBase);

            amountDu = initMontantDu;
            montantDuPeriode = amountDu;
            initialAmount = initMontantDu;
            exerciceFiscal = periodeDeclarationList[i].periodeDeclaration;

            break;
        }

    }

    if (penalise == 1) {

        if (penalitieList.length == 0) {

            alertify.alert('Veuillez d\'abord appliquer et valider la pénalité pour la période de déclaration : ' + periodeName);
            return;
            
        } else {

            for (var i = 0; i < penalitieList.length; i++) {

                if (penalitieList[i].referenceNumber !== parseInt(idPeriodeDeclaration)) {
                    alertify.alert('Veuillez d\'abord appliquer et valider la pénalité pour la période de déclaration : ' + periodeName);
                    return;
                }
            }

        }

    }

    if (amountDu == 0) {
        alertify.alert('Le montant du est incorrect.');
        return;
    }

    var checkControl = document.getElementById("checkBoxAddPenalite");
    if (checkControl.checked) {
        applyPenalite = 1;
    } else {
        applyPenalite = 0;
    }


    if (applyPenalite == 0 && penalise == 1) {

        alertify.confirm('Etes-vous sûre de vouloir créer la note de taxation pour la période : ' + periodeName + ' avec annulation des pénalités  ?', function () {

            dataDeclaration = {};
            dataDeclaration.idPeriodeDeclaration = idPeriodeDeclaration;
            dataDeclaration.penalise = penalise;
            dataDeclaration.valBase = valBase;
            dataDeclaration.cancelPenalite = 1;
            dataDeclaration.archivages = '';

            var idJoindre = '#cb_Declaration' + idPeriodeDeclaration;
            var joindre = $('#tablePeriodeDeclaration').find(idJoindre);

            if (joindre.is(':checked')) {
                initUpload(codeDoc, codeTypeDocs);
            } else {
                toDeclare();
            }

        });



    } else {

        alertify.confirm('Etes-vous sûre de vouloir créer la note de taxation pour cette période de déclaration : ' + periodeName + ' ?', function () {

            dataDeclaration = {};
            dataDeclaration.idPeriodeDeclaration = idPeriodeDeclaration;
            dataDeclaration.penalise = penalise;
            dataDeclaration.valBase = valBase;
            dataDeclaration.cancelPenalite = 0;
            dataDeclaration.archivages = '';

            var idJoindre = '#cb_Declaration' + idPeriodeDeclaration;
            var joindre = $('#tablePeriodeDeclaration').find(idJoindre);

            if (joindre.is(':checked')) {
                initUpload(codeDoc, codeTypeDocs);
            } else {
                toDeclare();
            }

        });

    }
}

function toDeclare() {

    var typeTaxation = 'TRP';

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codeResponsible': codeResponsible,
            'codeAdresse': idAddress,
            'codeSite': userData.SiteCode,
            'userId': userData.idUser,
            'depotDeclaration': null,
            'typeTaxation': typeTaxation,
            'codeService': userData.serviceCode,
            'idPeriodeDeclaration': dataDeclaration.idPeriodeDeclaration,
            'amountPeriode': montantDuPeriode,
            'devisePeriode': devise,
            'tauxPalier': taux,
            'typeTaux': typeTaux,
            'baseCalcul': dataDeclaration.valBase,
            'estPenalise': dataDeclaration.penalise,
            'exerciceFiscal': exerciceFiscal,
            'archives': dataDeclaration.archivages,
            'noteCalculSource': oldNC,
            'detailPenalite': penalitieList !== null ? JSON.stringify(penalitieList) : penalitieList,
            //'cancelPenalite': dataDeclaration.cancelPenalite,
            'operation': 'saveNoteCalcul'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Création de la note de taxation en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                }
                if (response == '0') {
                    alertify.alert('Echec opération. Une erreur s\'est produite lors de la création de la note de taxation !');
                    return;
                } else {

                    alertify.alert('Votre dossier est à la clôture avec comme référence : ' + response);
                    refreshPeriodeDeclarationList();
                    dataDeclaration = null;
                    return;
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}

function getDocumentsToUpload() {

    dataDeclaration.archivages = getUploadedData();
    toDeclare();
}

function getAmout(base) {
    switch (typeTaux) {
        case 'F':
            amount = (base * taux);
            break;
        case '%':
            amount = (base * (taux / 100));
            break;
    }
    return  amount;
}

function getExoAmount(taux, amout) {
    var amountExo = 0;
    if (taux > 0) {
        amountExo = (amout * (taux / 100));
    }
    return amountExo;
}

function getTauxLabel() {

    switch (typeTaux) {
        case 'F':
            return formatNumber(taux, devise);
            break;
        case '%':
            return taux + ' %';
            break;
    }
}

function validatePenalite() {
    $("#lblPenalite_" + documentReference).html(formatNumber(getSumByDoc(documentReference), devise));
}

function checkArticleMere(article) {

    var articleDependant;

    if (article == '1') {

        for (var i = 0; i < articleDependanceList.length; i++) {
            if (i == 0) {

                articleDependant = '<span style="font-weight:bold">' + articleDependanceList[i].codeBudgetaire
                        + '</span><br/>' + '<span style="color:red">'
                        + articleDependanceList[i].articleDependance + '</span>';
            } else {
                articleDependant += articleDependant = '<hr/>' + '<span style="font-weight:bold">'
                        + articleDependanceList[i].codeBudgetaire + '</span><br/>'
                        + '<span style="color:red">' + articleDependanceList[i].articleDependance
                        + '</span>' + '<hr/>' + '\n';
            }
        }

        if (articleDependant != undefined) {
            setTimeout(function () {
                alertify.alert('L\'article budgétaire de la taxation depend de(s) article(s) ci-après : '
                        + '<hr/>' + articleDependant);
            }, 5);
        }
    }

}

function validateFPC() {

    switch (fromCall) {
        case '0':
            alertify.confirm('Etes-vous sûre de vouloir valider prendre en compte ces pénalités ?', function () {

                penalitieList = getPenalitiesReference(periodeIDSelected);

                var idColPenalite = $("#lblPenalite_" + medIDSelected);
                idColPenalite.html(formatNumber(getSumByReference(periodeIDSelected), deviseSelected));

                modalFichePriseEnCharge.modal('hide');
            });
            break;
        case '1':
            penalitieList = getPenalitiesReference(periodeIDSelected);
            break;
    }


    return penalitieList;
}
