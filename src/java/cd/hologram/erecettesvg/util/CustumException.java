package cd.hologram.erecettesvg.util;

import java.sql.Connection;
import java.util.logging.Logger;

/**
 *
 * @author moussa.toure
 */
public class CustumException {

    private static final Logger LOG = Logger.getLogger(Connection.class.getName());

    public static void LogException(Exception exception) {

        LOG.log(java.util.logging.Level.SEVERE,
                "EXCEPTION : {0} "
                + "// MESSAGE : {1} "
                + "// CLASS : {2} "
                + "// METHOD : {3} "
                + "// LINE : {4} ",
                new Object[]{
                    exception.getClass(),
                    exception.getMessage(),
                    exception.getStackTrace()[0].getClassName(),
                    exception.getStackTrace()[0].getMethodName(),
                    exception.getStackTrace()[0].getLineNumber()
                });
    }

    public static void BasicLogException(Exception exception) {

        LOG.log(java.util.logging.Level.SEVERE,
                "EXCEPTION : {0} "
                + "// MESSAGE : {1} "
                + "// CLASS : {2} "
                + "// METHOD : {3} "
                + "// LINE : {4} ",
                new Object[]{
                    exception.getClass(),
                    exception.getMessage(),
                    exception.getStackTrace()[0].getClassName(),
                    exception.getStackTrace()[0].getMethodName(),
                    exception.getStackTrace()[0].getLineNumber()
                });

    }

}
