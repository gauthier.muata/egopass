/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_LETTRE_SAISIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LettreSaisie.findAll", query = "SELECT l FROM LettreSaisie l"),
    @NamedQuery(name = "LettreSaisie.findById", query = "SELECT l FROM LettreSaisie l WHERE l.id = :id"),
    @NamedQuery(name = "LettreSaisie.findByDateCreat", query = "SELECT l FROM LettreSaisie l WHERE l.dateCreat = :dateCreat"),
    @NamedQuery(name = "LettreSaisie.findByDateReception", query = "SELECT l FROM LettreSaisie l WHERE l.dateReception = :dateReception"),
    @NamedQuery(name = "LettreSaisie.findByDateEcheance", query = "SELECT l FROM LettreSaisie l WHERE l.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "LettreSaisie.findByEtat", query = "SELECT l FROM LettreSaisie l WHERE l.etat = :etat"),
    @NamedQuery(name = "LettreSaisie.findByFraisPoursuite", query = "SELECT l FROM LettreSaisie l WHERE l.fraisPoursuite = :fraisPoursuite"),
    @NamedQuery(name = "LettreSaisie.findByFraisPoursuiteCmd", query = "SELECT l FROM LettreSaisie l WHERE l.fraisPoursuiteCmd = :fraisPoursuiteCmd")})
public class LettreSaisie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ID")
    private String id;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Column(name = "ETAT")
    private Integer etat;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FRAIS_POURSUITE")
    private BigDecimal fraisPoursuite;
    @Column(name = "FRAIS_POURSUITE_CMD")
    private BigDecimal fraisPoursuiteCmd;
    @JoinColumn(name = "AGENT_CREAT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentCreat;
    @JoinColumn(name = "FK_COMMANDEMENT", referencedColumnName = "ID")
    @ManyToOne
    private Commandement fkCommandement;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;

    public LettreSaisie() {
    }

    public LettreSaisie(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public BigDecimal getFraisPoursuite() {
        return fraisPoursuite;
    }

    public void setFraisPoursuite(BigDecimal fraisPoursuite) {
        this.fraisPoursuite = fraisPoursuite;
    }

    public BigDecimal getFraisPoursuiteCmd() {
        return fraisPoursuiteCmd;
    }

    public void setFraisPoursuiteCmd(BigDecimal fraisPoursuiteCmd) {
        this.fraisPoursuiteCmd = fraisPoursuiteCmd;
    }

    public Agent getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Agent agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Commandement getFkCommandement() {
        return fkCommandement;
    }

    public void setFkCommandement(Commandement fkCommandement) {
        this.fkCommandement = fkCommandement;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LettreSaisie)) {
            return false;
        }
        LettreSaisie other = (LettreSaisie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.LettreSaisie[ id=" + id + " ]";
    }
    
}
