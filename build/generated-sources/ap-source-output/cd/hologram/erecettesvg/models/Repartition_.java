package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.Retribution;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Repartition.class)
public class Repartition_ { 

    public static volatile SingularAttribute<Repartition, DetailsNc> detailsNc;
    public static volatile SingularAttribute<Repartition, String> code;
    public static volatile SingularAttribute<Repartition, Retribution> retribution;
    public static volatile SingularAttribute<Repartition, BigDecimal> montant;

}