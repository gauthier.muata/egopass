/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class CommandementMock implements Serializable{
    
    String id;
    String logo;
    String codeQr;
    String lieuPrint;
    String dateImpression;
    String numeroCommandement;
    String annee;
    String jour;
    String mois;
    String heure;
    String numeroContrainte;
    String dateReceptionContrainte;
    String nomHuissier;
    String parquet;
    String contribuable;
    String adresseContribuable;
    String qualiteContribuable;
    String montantChiffre;
    String montantLettre;
    String principal;
    String penaliteRecouvrement;
    String penaliteAssiette;
    String fraisPoursuite;
    String cumulAmount;
    String amountTotal;
    String nameHuissier;
    
    String dateJour;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQr() {
        return codeQr;
    }

    public void setCodeQr(String codeQr) {
        this.codeQr = codeQr;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getNumeroCommandement() {
        return numeroCommandement;
    }

    public void setNumeroCommandement(String numeroCommandement) {
        this.numeroCommandement = numeroCommandement;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }

    public String getJour() {
        return jour;
    }

    public void setJour(String jour) {
        this.jour = jour;
    }

    public String getMois() {
        return mois;
    }

    public void setMois(String mois) {
        this.mois = mois;
    }

    public String getHeure() {
        return heure;
    }

    public void setHeure(String heure) {
        this.heure = heure;
    }

    public String getNumeroContrainte() {
        return numeroContrainte;
    }

    public void setNumeroContrainte(String numeroContrainte) {
        this.numeroContrainte = numeroContrainte;
    }

    public String getDateReceptionContrainte() {
        return dateReceptionContrainte;
    }

    public void setDateReceptionContrainte(String dateReceptionContrainte) {
        this.dateReceptionContrainte = dateReceptionContrainte;
    }

    public String getNomHuissier() {
        return nomHuissier;
    }

    public void setNomHuissier(String nomHuissier) {
        this.nomHuissier = nomHuissier;
    }

    public String getParquet() {
        return parquet;
    }

    public void setParquet(String parquet) {
        this.parquet = parquet;
    }

    public String getContribuable() {
        return contribuable;
    }

    public void setContribuable(String contribuable) {
        this.contribuable = contribuable;
    }

    public String getAdresseContribuable() {
        return adresseContribuable;
    }

    public void setAdresseContribuable(String adresseContribuable) {
        this.adresseContribuable = adresseContribuable;
    }

    public String getQualiteContribuable() {
        return qualiteContribuable;
    }

    public void setQualiteContribuable(String qualiteContribuable) {
        this.qualiteContribuable = qualiteContribuable;
    }

    public String getMontantChiffre() {
        return montantChiffre;
    }

    public void setMontantChiffre(String montantChiffre) {
        this.montantChiffre = montantChiffre;
    }

    public String getMontantLettre() {
        return montantLettre;
    }

    public void setMontantLettre(String montantLettre) {
        this.montantLettre = montantLettre;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getPenaliteRecouvrement() {
        return penaliteRecouvrement;
    }

    public void setPenaliteRecouvrement(String penaliteRecouvrement) {
        this.penaliteRecouvrement = penaliteRecouvrement;
    }

    public String getPenaliteAssiette() {
        return penaliteAssiette;
    }

    public void setPenaliteAssiette(String penaliteAssiette) {
        this.penaliteAssiette = penaliteAssiette;
    }

    public String getFraisPoursuite() {
        return fraisPoursuite;
    }

    public void setFraisPoursuite(String fraisPoursuite) {
        this.fraisPoursuite = fraisPoursuite;
    }

    public String getCumulAmount() {
        return cumulAmount;
    }

    public void setCumulAmount(String cumulAmount) {
        this.cumulAmount = cumulAmount;
    }

    public String getAmountTotal() {
        return amountTotal;
    }

    public void setAmountTotal(String amountTotal) {
        this.amountTotal = amountTotal;
    }

    public String getNameHuissier() {
        return nameHuissier;
    }

    public void setNameHuissier(String nameHuissier) {
        this.nameHuissier = nameHuissier;
    }

    public String getDateJour() {
        return dateJour;
    }

    public void setDateJour(String dateJour) {
        this.dateJour = dateJour;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    
    
}
