package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.DernierAvertissement;
import cd.hologram.erecettesvg.models.DetailsRole;
import cd.hologram.erecettesvg.models.Personne;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ExtraitDeRole.class)
public class ExtraitDeRole_ { 

    public static volatile SingularAttribute<ExtraitDeRole, String> fkRole;
    public static volatile SingularAttribute<ExtraitDeRole, BigDecimal> fraisPoursuiteAtdPv;
    public static volatile SingularAttribute<ExtraitDeRole, Boolean> estEnReclamation;
    public static volatile SingularAttribute<ExtraitDeRole, Date> dateCreat;
    public static volatile SingularAttribute<ExtraitDeRole, String> articleRole;
    public static volatile ListAttribute<ExtraitDeRole, DetailsRole> detailsRoleList;
    public static volatile ListAttribute<ExtraitDeRole, DernierAvertissement> dernierAvertissementList;
    public static volatile SingularAttribute<ExtraitDeRole, Integer> etat;
    public static volatile SingularAttribute<ExtraitDeRole, BigDecimal> fraisPoursuiteCmd;
    public static volatile SingularAttribute<ExtraitDeRole, Personne> fkPersonne;
    public static volatile SingularAttribute<ExtraitDeRole, String> fkSite;
    public static volatile SingularAttribute<ExtraitDeRole, Integer> ordonnance;
    public static volatile SingularAttribute<ExtraitDeRole, Date> dateEcheance;
    public static volatile SingularAttribute<ExtraitDeRole, String> id;
    public static volatile SingularAttribute<ExtraitDeRole, Agent> agentCreat;
    public static volatile SingularAttribute<ExtraitDeRole, Date> dateReception;

}