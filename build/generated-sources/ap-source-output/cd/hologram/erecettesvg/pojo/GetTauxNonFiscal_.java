package cd.hologram.erecettesvg.pojo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(GetTauxNonFiscal.class)
public class GetTauxNonFiscal_ { 

    public static volatile SingularAttribute<GetTauxNonFiscal, String> articleBudgetaire;
    public static volatile SingularAttribute<GetTauxNonFiscal, Boolean> multiplierValeurBase;
    public static volatile SingularAttribute<GetTauxNonFiscal, Float> taux;
    public static volatile SingularAttribute<GetTauxNonFiscal, String> typeTaux;
    public static volatile SingularAttribute<GetTauxNonFiscal, Float> borneInferieure;
    public static volatile SingularAttribute<GetTauxNonFiscal, Float> borneSuperieure;
    public static volatile SingularAttribute<GetTauxNonFiscal, String> devise;

}