package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Kit.class)
public class Kit_ { 

    public static volatile SingularAttribute<Kit, Integer> code;
    public static volatile SingularAttribute<Kit, String> type;
    public static volatile SingularAttribute<Kit, Boolean> etat;
    public static volatile SingularAttribute<Kit, String> mac;
    public static volatile SingularAttribute<Kit, String> marque;

}