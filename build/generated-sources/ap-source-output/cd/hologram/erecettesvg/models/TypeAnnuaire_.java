package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TypeAnnuaire.class)
public class TypeAnnuaire_ { 

    public static volatile SingularAttribute<TypeAnnuaire, String> agentMaj;
    public static volatile SingularAttribute<TypeAnnuaire, Integer> code;
    public static volatile SingularAttribute<TypeAnnuaire, Date> dateCreat;
    public static volatile SingularAttribute<TypeAnnuaire, String> agentCreat;
    public static volatile SingularAttribute<TypeAnnuaire, String> intitule;
    public static volatile SingularAttribute<TypeAnnuaire, Short> etat;
    public static volatile SingularAttribute<TypeAnnuaire, Date> dateMaj;

}