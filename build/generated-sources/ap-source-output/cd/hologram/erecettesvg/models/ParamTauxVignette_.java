package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.Tarif;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ParamTauxVignette.class)
public class ParamTauxVignette_ { 

    public static volatile SingularAttribute<ParamTauxVignette, Tarif> tarif;
    public static volatile SingularAttribute<ParamTauxVignette, Integer> code;
    public static volatile SingularAttribute<ParamTauxVignette, FormeJuridique> formeJuridique;
    public static volatile SingularAttribute<ParamTauxVignette, BigDecimal> montantImpot;
    public static volatile SingularAttribute<ParamTauxVignette, BigDecimal> montantTotal;
    public static volatile SingularAttribute<ParamTauxVignette, BigDecimal> montantTscr;

}