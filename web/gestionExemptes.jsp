<%-- 
    Document   : gestionPrevisionCredit
    Created on : 24 mars 2020, 14:31:07
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Registre des exemptés</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="assets/css/apercu.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

        <style>
            /*@media (max-width: 768px) {
                #divTablePrevisionCredit{
                    overflow: auto
                }
            }*/

            .details-control {
                background: url('assets/images/details_open.png') no-repeat center center;
                cursor: pointer;
            }
            tr.shown td.details-control {
                background: url('assets/images/details_close.png') no-repeat center center;
            }


        </style>

    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <form class="form-horizontal" >
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> INFORMATIONS DE L'ASSUJETTI </h4>
                                </div>

                                <div class="panel-wrapper collapse in">

                                    <div class="panel-body"> 

                                        <span id="lblTypeAss" style="font-size: 16px">Type assujetti :</span>
                                        <span  >
                                            <p id="lblTypeAssujet"
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></p>
                                        </span><br/><br/>

                                        <span id="lblNomAssujet" style="font-size: 16px">Nom :</span>
                                        <span  >
                                            <b id="lblNameAssujet" 
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></b>
                                        </span><br/><br/>

                                        <span id="lblAdress" style="font-size: 16px">Adresse :</span>
                                        <span  >
                                            <p id="lblAddresseAssujet" 
                                               style="font-size: 14px;
                                               font-weight: bold;color: black"></p>
                                        </span><br/>                                    

                                    </div>

                                    <div class="panel-footer">
                                        <button  
                                            id="btnSearchAssujettis"
                                            class="btn btn-primary">
                                            <i class="fa fa-search"></i>
                                            Rechercher un assujetti
                                        </button>
                                    </div>

                                </div>                               
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title"> LISTE DE BIENS (AUTOMOBILE) </h4>
                                </div>

                                <div class="panel-body">  

                                    <div id="divTablePrevisionCredit">

                                        <table id="tableBiens" class="table table-bordered" ></table>

                                    </div>                                  

                                </div> 

                                <div class="panel-footer" style="text-align: right">
                                    <button class="btn btn-success" id="btnAllValidateExempt" style="display: none" type="button">
                                        <i class="fa fa-check-circle"></i> Exempter tout
                                    </button>
                                    <button class="btn btn-danger" id="btnAllCanceledExempt" style="display: none" type="button">
                                        <i class="fa fa-close"></i> Annuler tout
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/modalRechercheAssujettiPrevisionCredit.html" %>
        <!--%@include file="assets/include/modal/modalDetailProvisionCredit.html" %-->
        <!--%@include file="assets/include/modal/modaTraitementlDetailProvisionCredit.html" %-->

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script src="assets/js/previsionCredit/modalRechercheAssujettiPrevisCredit.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/js/previsionCredit/gestionExemptes.js"></script>
    </body>
</html>
