/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author KASHALA
 */
@Entity
@Cacheable(false)
@Table(name = "T_GO_PASSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GoPasse.findAll", query = "SELECT c FROM GoPasse c"),
    @NamedQuery(name = "GoPasse.findByIdGopasse", query = "SELECT c FROM GoPasse c WHERE c.idGopasse = :idGopasse"),
    @NamedQuery(name = "GoPasse.findByNumeroGopasse", query = "SELECT c FROM GoPasse c WHERE c.numeroGopasse = :numeroGopasse"),
    @NamedQuery(name = "GoPasse.findByQuantite", query = "SELECT c FROM GoPasse c WHERE c.quantite = :quantite"),
    @NamedQuery(name = "GoPasse.findByMontant", query = "SELECT c FROM GoPasse c WHERE c.montant = :montant"),
    @NamedQuery(name = "GoPasse.findByDevise", query = "SELECT c FROM GoPasse c WHERE c.devise = :devise"),
    @NamedQuery(name = "GoPasse.findByTypeGopasse", query = "SELECT c FROM GoPasse c WHERE c.typeGopasse = :typeGopasse"),
    @NamedQuery(name = "GoPasse.findByNew_Id", query = "SELECT c FROM GoPasse c WHERE c.newId = :newId"),
    @NamedQuery(name = "GoPasse.findByDateMaj", query = "SELECT c FROM GoPasse c WHERE c.dateMaj = :dateMaj"),
    @NamedQuery(name = "GoPasse.findByEtat", query = "SELECT c FROM GoPasse c WHERE c.etat = :etat")})
public class GoPasse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdGopasse")
    private Integer idGopasse;
    @Size(max = 50)
    @Column(name = "NumeroGopasse")
    private String numeroGopasse;
    @Column(name = "Quantite")
    private BigDecimal quantite;
    @Column(name = "Montant")
    private BigDecimal montant;
    @Size(max = 50)
    @Column(name = "Devise")
    private String devise;
    @Size(max = 50)
    @Column(name = "New_Id")
    private String newId;
    @Size(max = 50)
    @Column(name = "TypeGopasse")
    private String typeGopasse;
    @Column(name = "Date_Maj")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "Fk_Tarif", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif fkTarif;
    @JoinColumn(name = "Agent_Maj", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentMaj;
//    @JoinColumn(name = "Fk_Personne", referencedColumnName = "CODE")
//    @ManyToOne
//    private Personne fkPersonne;
    @JoinColumn(name = "Fk_Commande", referencedColumnName = "ID")
    @ManyToOne
    private Commande fkCommande;

    public GoPasse() {
    }

    public GoPasse(Integer idGopasse) {
        this.idGopasse = idGopasse;
    }

    public Integer getIdGopasse() {
        return idGopasse;
    }

    public void setIdGopasse(Integer idGopasse) {
        this.idGopasse = idGopasse;
    }

    public String getNumeroGopasse() {
        return numeroGopasse;
    }

    public void setNumeroGopasse(String numeroGopasse) {
        this.numeroGopasse = numeroGopasse;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public String getTypeGopasse() {
        return typeGopasse;
    }

    public void setTypeGopasse(String typeGopasse) {
        this.typeGopasse = typeGopasse;
    }

    public BigDecimal getQuantite() {
        return quantite;
    }

    public void setQuantite(BigDecimal quantite) {
        this.quantite = quantite;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Tarif getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(Tarif fkTarif) {
        this.fkTarif = fkTarif;
    }

    public Agent getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Agent agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

//    public Personne getFkPersonne() {
//        return fkPersonne;
//    }
//
//    public void setFkPersonne(Personne fkPersonne) {
//        this.fkPersonne = fkPersonne;
//    }

    public Commande getFkCommande() {
        return fkCommande;
    }

    public void setFkCommande(Commande fkCommande) {
        this.fkCommande = fkCommande;
    }

    public String getNewId() {
        return newId;
    }

    public void setNewId(String newId) {
        this.newId = newId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GoPasse other = (GoPasse) obj;
        return true;
    }

    @Override
    public String toString() {
        return "GoPass{" + "idGopasse=" + idGopasse + '}';
    }
    
    

}
