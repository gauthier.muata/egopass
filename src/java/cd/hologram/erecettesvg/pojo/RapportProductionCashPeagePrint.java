/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class RapportProductionCashPeagePrint implements Serializable{
    
    String datePrint;
    String axepeage;
    String poste;
    String controleurName;
    String controleurCode;
    String controleurPhone;
    String percepteurName;
    String percepteurCode;
    String percepteurPhone;
    
    String fVCdf;
    String fVLCdf;
    String fCIPCdf;
    String fBMCdf;
    String fTOTALCdf;
    
    String tuVCdf;
    String tuVLCdf;
    String tuCIPCdf;
    String tuBMCdf;
    String tuTOTALCdf;
    
    String mVCdf;
    String mVLCdf;
    String mCIPCdf;
    String mBMCdf;
    String mTOTALCdf;
    
    String fC2PUsd;
    String fCRUsd;
    String fCRTRUsd;
    String fVITUsd;
    String fVLITUsd;
    String fCIPBMUsd;
    String fTOTALUsd;
    
    String tuC2PUsd;
    String tuCRUsd;
    String tuCRTRUsd;
    String tuVITUsd;
    String tuVLITUsd;
    String tuCIPBMUsd;
    String tuTOTALUsd;
    
    String mC2PUsd;
    String mCRUsd;
    String mCRTRUsd;
    String mVTUsd;
    String mVLITUsd;
    String mCIPBMUsd;
    String mTOTALUsd;
    
    String totlGenTour;
    String observation;
    String lieuPrint;

    public String getDatePrint() {
        return datePrint;
    }

    public void setDatePrint(String datePrint) {
        this.datePrint = datePrint;
    }

    public String getAxepeage() {
        return axepeage;
    }

    public void setAxepeage(String axepeage) {
        this.axepeage = axepeage;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getControleurName() {
        return controleurName;
    }

    public void setControleurName(String controleurName) {
        this.controleurName = controleurName;
    }

    public String getControleurCode() {
        return controleurCode;
    }

    public void setControleurCode(String controleurCode) {
        this.controleurCode = controleurCode;
    }

    public String getControleurPhone() {
        return controleurPhone;
    }

    public void setControleurPhone(String controleurPhone) {
        this.controleurPhone = controleurPhone;
    }

    public String getPercepteurName() {
        return percepteurName;
    }

    public void setPercepteurName(String percepteurName) {
        this.percepteurName = percepteurName;
    }

    public String getPercepteurCode() {
        return percepteurCode;
    }

    public void setPercepteurCode(String percepteurCode) {
        this.percepteurCode = percepteurCode;
    }

    public String getPercepteurPhone() {
        return percepteurPhone;
    }

    public void setPercepteurPhone(String percepteurPhone) {
        this.percepteurPhone = percepteurPhone;
    }

    public String getfVCdf() {
        return fVCdf;
    }

    public void setfVCdf(String fVCdf) {
        this.fVCdf = fVCdf;
    }

    public String getfVLCdf() {
        return fVLCdf;
    }

    public void setfVLCdf(String fVLCdf) {
        this.fVLCdf = fVLCdf;
    }

    public String getfCIPCdf() {
        return fCIPCdf;
    }

    public void setfCIPCdf(String fCIPCdf) {
        this.fCIPCdf = fCIPCdf;
    }

    public String getfBMCdf() {
        return fBMCdf;
    }

    public void setfBMCdf(String fBMCdf) {
        this.fBMCdf = fBMCdf;
    }

    public String getfTOTALCdf() {
        return fTOTALCdf;
    }

    public void setfTOTALCdf(String fTOTALCdf) {
        this.fTOTALCdf = fTOTALCdf;
    }

    public String getTuVCdf() {
        return tuVCdf;
    }

    public void setTuVCdf(String tuVCdf) {
        this.tuVCdf = tuVCdf;
    }

    public String getTuVLCdf() {
        return tuVLCdf;
    }

    public void setTuVLCdf(String tuVLCdf) {
        this.tuVLCdf = tuVLCdf;
    }

    public String getTuCIPCdf() {
        return tuCIPCdf;
    }

    public void setTuCIPCdf(String tuCIPCdf) {
        this.tuCIPCdf = tuCIPCdf;
    }

    public String getTuBMCdf() {
        return tuBMCdf;
    }

    public void setTuBMCdf(String tuBMCdf) {
        this.tuBMCdf = tuBMCdf;
    }

    public String getTuTOTALCdf() {
        return tuTOTALCdf;
    }

    public void setTuTOTALCdf(String tuTOTALCdf) {
        this.tuTOTALCdf = tuTOTALCdf;
    }

    public String getmVCdf() {
        return mVCdf;
    }

    public void setmVCdf(String mVCdf) {
        this.mVCdf = mVCdf;
    }

    public String getmVLCdf() {
        return mVLCdf;
    }

    public void setmVLCdf(String mVLCdf) {
        this.mVLCdf = mVLCdf;
    }

    public String getmCIPCdf() {
        return mCIPCdf;
    }

    public void setmCIPCdf(String mCIPCdf) {
        this.mCIPCdf = mCIPCdf;
    }

    public String getmBMCdf() {
        return mBMCdf;
    }

    public void setmBMCdf(String mBMCdf) {
        this.mBMCdf = mBMCdf;
    }

    public String getmTOTALCdf() {
        return mTOTALCdf;
    }

    public void setmTOTALCdf(String mTOTALCdf) {
        this.mTOTALCdf = mTOTALCdf;
    }

    public String getfC2PUsd() {
        return fC2PUsd;
    }

    public void setfC2PUsd(String fC2PUsd) {
        this.fC2PUsd = fC2PUsd;
    }

    public String getfCRUsd() {
        return fCRUsd;
    }

    public void setfCRUsd(String fCRUsd) {
        this.fCRUsd = fCRUsd;
    }

    public String getfCRTRUsd() {
        return fCRTRUsd;
    }

    public void setfCRTRUsd(String fCRTRUsd) {
        this.fCRTRUsd = fCRTRUsd;
    }

    public String getfVITUsd() {
        return fVITUsd;
    }

    public void setfVITUsd(String fVITUsd) {
        this.fVITUsd = fVITUsd;
    }

    public String getfVLITUsd() {
        return fVLITUsd;
    }

    public void setfVLITUsd(String fVLITUsd) {
        this.fVLITUsd = fVLITUsd;
    }

    public String getfCIPBMUsd() {
        return fCIPBMUsd;
    }

    public void setfCIPBMUsd(String fCIPBMUsd) {
        this.fCIPBMUsd = fCIPBMUsd;
    }

    public String getfTOTALUsd() {
        return fTOTALUsd;
    }

    public void setfTOTALUsd(String fTOTALUsd) {
        this.fTOTALUsd = fTOTALUsd;
    }

    public String getTuC2PUsd() {
        return tuC2PUsd;
    }

    public void setTuC2PUsd(String tuC2PUsd) {
        this.tuC2PUsd = tuC2PUsd;
    }

    public String getTuCRUsd() {
        return tuCRUsd;
    }

    public void setTuCRUsd(String tuCRUsd) {
        this.tuCRUsd = tuCRUsd;
    }

    public String getTuCRTRUsd() {
        return tuCRTRUsd;
    }

    public void setTuCRTRUsd(String tuCRTRUsd) {
        this.tuCRTRUsd = tuCRTRUsd;
    }

    public String getTuVITUsd() {
        return tuVITUsd;
    }

    public void setTuVITUsd(String tuVITUsd) {
        this.tuVITUsd = tuVITUsd;
    }

    public String getTuVLITUsd() {
        return tuVLITUsd;
    }

    public void setTuVLITUsd(String tuVLITUsd) {
        this.tuVLITUsd = tuVLITUsd;
    }

    public String getTuCIPBMUsd() {
        return tuCIPBMUsd;
    }

    public void setTuCIPBMUsd(String tuCIPBMUsd) {
        this.tuCIPBMUsd = tuCIPBMUsd;
    }

    public String getTuTOTALUsd() {
        return tuTOTALUsd;
    }

    public void setTuTOTALUsd(String tuTOTALUsd) {
        this.tuTOTALUsd = tuTOTALUsd;
    }

    public String getmC2PUsd() {
        return mC2PUsd;
    }

    public void setmC2PUsd(String mC2PUsd) {
        this.mC2PUsd = mC2PUsd;
    }

    public String getmCRUsd() {
        return mCRUsd;
    }

    public void setmCRUsd(String mCRUsd) {
        this.mCRUsd = mCRUsd;
    }

    public String getmCRTRUsd() {
        return mCRTRUsd;
    }

    public void setmCRTRUsd(String mCRTRUsd) {
        this.mCRTRUsd = mCRTRUsd;
    }

    public String getmVTUsd() {
        return mVTUsd;
    }

    public void setmVTUsd(String mVTUsd) {
        this.mVTUsd = mVTUsd;
    }

    public String getmVLITUsd() {
        return mVLITUsd;
    }

    public void setmVLITUsd(String mVLITUsd) {
        this.mVLITUsd = mVLITUsd;
    }

    public String getmCIPBMUsd() {
        return mCIPBMUsd;
    }

    public void setmCIPBMUsd(String mCIPBMUsd) {
        this.mCIPBMUsd = mCIPBMUsd;
    }

    public String getmTOTALUsd() {
        return mTOTALUsd;
    }

    public void setmTOTALUsd(String mTOTALUsd) {
        this.mTOTALUsd = mTOTALUsd;
    }

    public String getTotlGenTour() {
        return totlGenTour;
    }

    public void setTotlGenTour(String totlGenTour) {
        this.totlGenTour = totlGenTour;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }
    
    
    
}
