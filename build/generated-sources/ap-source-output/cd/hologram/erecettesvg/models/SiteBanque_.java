package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Banque;
import cd.hologram.erecettesvg.models.Site;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(SiteBanque.class)
public class SiteBanque_ { 

    public static volatile SingularAttribute<SiteBanque, Site> site;
    public static volatile SingularAttribute<SiteBanque, Banque> banque;
    public static volatile SingularAttribute<SiteBanque, Integer> id;
    public static volatile SingularAttribute<SiteBanque, Boolean> etat;

}