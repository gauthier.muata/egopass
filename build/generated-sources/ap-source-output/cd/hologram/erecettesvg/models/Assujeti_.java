package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Tarif;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Assujeti.class)
public class Assujeti_ { 

    public static volatile SingularAttribute<Assujeti, BigDecimal> valeur;
    public static volatile SingularAttribute<Assujeti, String> numActeNotarie;
    public static volatile SingularAttribute<Assujeti, Short> renouvellement;
    public static volatile SingularAttribute<Assujeti, Personne> personne;
    public static volatile SingularAttribute<Assujeti, Boolean> etat;
    public static volatile SingularAttribute<Assujeti, Integer> nbrJourLimite;
    public static volatile ListAttribute<Assujeti, PeriodeDeclaration> periodeDeclarationList;
    public static volatile SingularAttribute<Assujeti, String> uniteValeur;
    public static volatile SingularAttribute<Assujeti, String> referenceContrat;
    public static volatile SingularAttribute<Assujeti, ArticleBudgetaire> articleBudgetaire;
    public static volatile SingularAttribute<Assujeti, Tarif> tarif;
    public static volatile SingularAttribute<Assujeti, String> periodicite;
    public static volatile SingularAttribute<Assujeti, String> gestionnaire;
    public static volatile SingularAttribute<Assujeti, String> dateDebut;
    public static volatile SingularAttribute<Assujeti, String> complementBien;
    public static volatile SingularAttribute<Assujeti, Short> duree;
    public static volatile SingularAttribute<Assujeti, String> dateActeNotarie;
    public static volatile SingularAttribute<Assujeti, AdressePersonne> fkAdressePersonne;
    public static volatile SingularAttribute<Assujeti, String> id;
    public static volatile SingularAttribute<Assujeti, String> dateFin;
    public static volatile SingularAttribute<Assujeti, Bien> bien;
    public static volatile SingularAttribute<Assujeti, String> nouvellesEcheances;

}