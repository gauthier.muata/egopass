/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalRechercheAvancee;
var modalRechercheAvanceeNC;
var dataService;

var advancedSearchParam = {};

var selectBanque, selectCompteBancaire;
var selectSite, selectService, selectType;

var inputDateDebut;
var inputdateLast;

var datePickerDebut;
var datePickerFin;

var typeDocumentResearchAdvanced;
var selectStatePaiement;
var selectCompteBancaire;

$(function () {

    modalRechercheAvancee = $('#modalRechercheAvancee');
    modalRechercheAvanceeNC = $('#modalRechercheAvanceeModelTwo');

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    datePickerDebut = $("#datePicker1");
    datePickerFin = $('#datePicker2');

    selectBanque = $('#selectBanque');
    selectCompteBancaire = $('#selectCompteBancaire');
    selectSite = $('#selectSite');
    selectService = $('#selectService');
    selectType = $('#selectType');
    typeDocumentResearchAdvanced = $('#typeDocumentResearchAdvanced');
    selectStatePaiement = $('#selectStatePaiement');
    selectCompteBancaire = $('#selectCompteBancaire');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerDebut.datepicker("setDate", new Date());
    datePickerFin.datepicker("setDate", new Date());

    var UsersSiteList = JSON.parse(userData.siteUserList);
    var AllSiteList = JSON.parse(userData.allSiteList);
    var BanqueUserList = JSON.parse(userData.banqueUserList);
    var AllServiceList = JSON.parse(userData.allServiceList);
    var dataSite = '';
    var dataService = '';
    var dataBanque = '';



    if (!controlAccess('VIEW_ALL_SITE')) {

        for (var i = 0; i < UsersSiteList.length; i++) {
            dataSite += '<option value="' + UsersSiteList[i].SiteCode + '">' + UsersSiteList[i].siteName + '</option>';
        }
        var selectSiteContent = dataSite;

        selectSite.html(selectSiteContent);
    } else {
        dataSite += '<option value="*">TOUS LES SITES</option>';
        for (var i = 0; i < AllSiteList.length; i++) {
            dataSite += '<option value="' + AllSiteList[i].SiteCode + '">' + AllSiteList[i].siteName + '</option>';

        }
        var selectSiteContent = dataSite;

        selectSite.html(selectSiteContent);
    }

    if (!controlAccess('VIEW_ALL_SERVICE')) {

        for (var i = 0; i < AllServiceList.length; i++) {
            dataService += '<option value="' + AllServiceList[i].serviceCode + '">' + AllServiceList[i].serviceName + '</option>';
        }
        var selectServiceContent = dataService;

        selectService.html(selectServiceContent);
    } else {
        dataService += '<option value="*">TOUS LES SERVICES</option>';
        for (var i = 0; i < AllServiceList.length; i++) {
            dataService += '<option value="' + AllServiceList[i].serviceCode + '">' + AllServiceList[i].serviceName + '</option>';

        }
        var selectServiceContent = dataService;

        selectService.html(selectServiceContent);
    }

    //dataBanque += '<option value="*">*</option>';
    var bankCode = '';

    for (var i = 0; i < BanqueUserList.length; i++) {
        if (bankCode == '') {
            bankCode = BanqueUserList[i].banqueCode;
        } else {
            if (bankCode !== BanqueUserList[i].banqueCode) {
                bankCode = BanqueUserList[i].banqueCode;
            }
        }
        dataBanque += '<option value="' + BanqueUserList[i].banqueCode + '">' + BanqueUserList[i].banqueName + '</option>';
    }
    var selectSiteContent = dataBanque;

    selectBanque.html(selectSiteContent);
    selectBanque.val(bankCode);

    var dataBankAccountList;

    var getAccountList = JSON.parse(userData.accountList);
    var accountList = getAccountByBanque(getAccountList, bankCode);

    dataBankAccountList += '<option value="*">*</option>';

    for (var i = 0; i < accountList.length; i++) {
        dataBankAccountList += '<option value="' + accountList[i].accountCode + '">' + accountList[i].accountCode + ' - ' + accountList[i].accountName + '</option>';
    }

    selectCompteBancaire.html(dataBankAccountList);

});

function getAccountByBanque(bankAccountList, selectBanque) {

    var accountList = [];

    for (var i = 0; i < bankAccountList.length; i++) {

        if (selectBanque === bankAccountList[i].acountBanqueCode) {

            accountList.push(bankAccountList[i]);
        }
        else if (selectBanque === '') {
            accountList.push(bankAccountList[i].acountBanqueCode);
        }
    }

    return accountList;
}

function getCurrentSearchParam(advanced) {

    switch (advanced) {
        case 0:
        case '0':
            //advancedSearchParam.typeDocumentCode = typeDocumentResearch.val();
            advancedSearchParam.typeDocumentName = $('#typeDocumentResearch option:selected').text();
            advancedSearchParam.statePaiementName = $('#selectStatePaiement option:selected').text();
            advancedSearchParam.accountBankName = $('#selectCompteBancaire option:selected').text();

            break;
        case 1:
        case '1':
            //advancedSearchParam.typeDocumentCode = typeDocumentResearchAdvanced.val();
            advancedSearchParam.typeDocumentName = $('#typeDocumentResearchAdvanced option:selected').text();
            //advancedSearchParam.statePaiementCode = selectStatePaiement.val();
            advancedSearchParam.statePaiementName = $('#selectStatePaiement option:selected').text();
            advancedSearchParam.accountBankName = $('#selectCompteBancaire option:selected').text();
            //advancedSearchParam.accountBankCode = selectCompteBancaire.val();
            break;
    }

    advancedSearchParam.agentCode = userData.idUser;
    advancedSearchParam.dateDebut = $('#inputDateDebut').val();
    advancedSearchParam.dateFin = $('#inputdateLast').val();
    advancedSearchParam.agentName = userData.nomComplet;


}

