package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Site;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TicketPeage.class)
public class TicketPeage_ { 

    public static volatile SingularAttribute<TicketPeage, Integer> agentMaj;
    public static volatile SingularAttribute<TicketPeage, String> fkTarif;
    public static volatile SingularAttribute<TicketPeage, String> code;
    public static volatile SingularAttribute<TicketPeage, String> observation;
    public static volatile SingularAttribute<TicketPeage, Personne> personne;
    public static volatile SingularAttribute<TicketPeage, String> typePaiement;
    public static volatile SingularAttribute<TicketPeage, BigDecimal> montant;
    public static volatile SingularAttribute<TicketPeage, String> shiftTravail;
    public static volatile SingularAttribute<TicketPeage, Integer> etat;
    public static volatile SingularAttribute<TicketPeage, Devise> devise;
    public static volatile SingularAttribute<TicketPeage, Date> dateMaj;
    public static volatile SingularAttribute<TicketPeage, Integer> fkOperationSite;
    public static volatile SingularAttribute<TicketPeage, String> plaqueChassis;
    public static volatile SingularAttribute<TicketPeage, ArticleBudgetaire> articleBudgetaire;
    public static volatile SingularAttribute<TicketPeage, Site> site;
    public static volatile SingularAttribute<TicketPeage, Date> dateSync;
    public static volatile SingularAttribute<TicketPeage, Date> dateProd;
    public static volatile SingularAttribute<TicketPeage, Integer> agentCreat;
    public static volatile SingularAttribute<TicketPeage, String> motifAnnulation;
    public static volatile SingularAttribute<TicketPeage, String> ticketDocument;
    public static volatile SingularAttribute<TicketPeage, Bien> bien;

}