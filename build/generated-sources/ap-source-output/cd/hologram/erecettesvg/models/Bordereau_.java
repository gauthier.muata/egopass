package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.DetailBordereau;
import cd.hologram.erecettesvg.models.Etat;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Bordereau.class)
public class Bordereau_ { 

    public static volatile SingularAttribute<Bordereau, Integer> agentMaj;
    public static volatile SingularAttribute<Bordereau, Agent> agent;
    public static volatile SingularAttribute<Bordereau, CompteBancaire> compteBancaire;
    public static volatile SingularAttribute<Bordereau, Long> code;
    public static volatile SingularAttribute<Bordereau, Date> datePaiement;
    public static volatile SingularAttribute<Bordereau, String> centre;
    public static volatile SingularAttribute<Bordereau, String> numeroDeclaration;
    public static volatile SingularAttribute<Bordereau, Date> dateCreate;
    public static volatile SingularAttribute<Bordereau, Etat> etat;
    public static volatile SingularAttribute<Bordereau, Date> dateMaj;
    public static volatile ListAttribute<Bordereau, DetailBordereau> detailBordereauList;
    public static volatile SingularAttribute<Bordereau, String> numeroAttestationPaiement;
    public static volatile SingularAttribute<Bordereau, BigDecimal> totalMontantPercu;
    public static volatile SingularAttribute<Bordereau, Boolean> impressionRecu;
    public static volatile SingularAttribute<Bordereau, String> numeroBordereau;
    public static volatile SingularAttribute<Bordereau, String> nomComplet;
    public static volatile SingularAttribute<Bordereau, String> observationBanque;
    public static volatile SingularAttribute<Bordereau, String> totalMontantPercuToString;

}