package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Devise;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailsAssignationBudgetaire.class)
public class DetailsAssignationBudgetaire_ { 

    public static volatile SingularAttribute<DetailsAssignationBudgetaire, Integer> fkAssignation;
    public static volatile SingularAttribute<DetailsAssignationBudgetaire, Integer> agentMaj;
    public static volatile SingularAttribute<DetailsAssignationBudgetaire, String> fkArticleBudgetaire;
    public static volatile SingularAttribute<DetailsAssignationBudgetaire, Double> montantAssigne;
    public static volatile SingularAttribute<DetailsAssignationBudgetaire, Devise> fkDevise;
    public static volatile SingularAttribute<DetailsAssignationBudgetaire, Integer> id;
    public static volatile SingularAttribute<DetailsAssignationBudgetaire, Integer> etat;
    public static volatile SingularAttribute<DetailsAssignationBudgetaire, Date> dateMaj;

}