/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author WILLY KASHALA
 */
public class ContentieuxConst {

    public class Operation {

        public final static String LOAD_ASSUJETTI_DOSSIER = "loadAssujitteDossier";
        public final static String SAVE_RECLAMATION = "saveReclamation";
        public final static String LOAD_RECLAMATION = "loadReclamation";
        public final static String LOAD_RECOURS_JUIRIDICTIONNEL = "loadRecoursJuridictionnel";
        public final static String VALIDER_RECLAMATION = "validerReclamation";
        public final static String TRAITEMENT_DECISION = "traitementDecision";
        public final static String PRINT_DOCUMENT = "printDocument";
        public final static String LOAD_DOSSIER_RECOURS = "loadDossierRecours";
        public final static String LOAD_ARCHIVE_LETTRE_RECLAMATION = "loadArchiveLR";
        public final static String SAVE_RECOURS_JURIDICTIONNEL = "saveRecouursJuridictionnel";
    }

    public class ParamName {

        public final static String TYPE_SEARCH_DOCUMENT = "typeSearchDocument";
        public final static String NUMERO_DOCUMENT = "numeroDocument";
        public final static String NUMERO_ENREGISTREMENT_GREFFE = "numeroEnregistrementGreffe";
        public final static String CODE_ASSUJETTI = "codeAssujetti";
        public final static String OBSERVATION = "observation";
        public final static String OBSERVATION_LIST = "observationList";
        public final static String AVEC_SURCIS = "avecSurcis";
        public final static String DATE_DECISION_SURCIS = "dateDecisionSurcis";
        public final static String DATE_TRAITEMENT = "dateTraitement";
        public final static String AGENT_TRAITEMENT = "agentTraitement";
        public final static String EST_TRAITE = "estTraite";
        public final static String DECISION_SURCIS = "decisionSurcis";
        public final static String OBSERVATION_TRAITEMENT = "observationTraitement";
        public final static String EST_TRAITE_JURIDICTIONNEL = "estTraiteJuridictionnel";
        public final static String DECISION_ADMINISTRATIVE = "decisionAdministrative";
        public final static String CODE_DECISION_ADMINISTRATIVE = "CodeDecisionAdministrative";
        public final static String CODE_DECISION_JURIDICTIONNEL = "CodeDecisionJuridictionnel";
        public final static String DATE_TRAITEMENT_JURIDIQUE = "dateTraitementJuridique";
        public final static String DECISION_JURIDIQUE = "decisionJuridique";
        public final static String DETAILS_RECLAMATION = "detailsReclamation";
        public final static String MOTIF = "motif";
        public final static String ID_USER = "idUser";
        public final static String CODE_SITE = "codeSite";
        public final static String CODE_SERVICE = "codeService";
        public final static String DATE_ECHEANCE = "dateEcheance";
        public final static String DATE_RECEPTION = "dateReception";
        public final static String DATE_CREATE = "dateCreate";
        public final static String DATE_DEPOT_RECOURS = "dateDepotRecours";
        public final static String NOMBRE_JOUR = "nombreJour";
        public final static String ETAT_RECLAMATION = "etatReclamation";
        public final static String RECLAMATION_TRAITER = "reclamationTraiter";
        public final static String NOMBRE_MOIS = "nombreMois";
        public final static String TITRE_EXIST_TO_RECLAMATION = "titreExistToReclamation";
        public final static String REFERENCE_DOCUMENT = "referenceDocument";
        public final static String TYPE_DOCUMENT = "typeDocument";
        public final static String TYPE_DOCUMENT_2 = "typeDocument2";
        public final static String LIBELLE_ARTICLE_BUDGETAIRE = "libelleArticleBudgetaire";
        public final static String FICHE_PRISE_CHARGE = "numeroFichePriseEnCharge";
        public final static String CODE_OFFICIEL = "codeOfficiel";
        public final static String USER_ID = "userId";
        public final static String TYPE_RECLAMATION = "typeReclamation";
        public final static String LIST_DOCUMENT = "listDocument";
        public final static String DOCUMENT_LIST = "documentList";
        public final static String REFERENCE_COURRIER_RECLAMATION = "referenceCourrierReclamation";
        public final static String MOTIF_RECOURS = "textMotifRecours";
        public final static String DATE_AUDIENCE = "dateAudience";
        public final static String RECOURS_EXIST = "recourExist";
        public final static String ARCHIVES = "archives";
        public final static String FK_DOCUMENT = "fkDocument";
        public final static String OBSERVATION_DOCUMENT = "observationDocument";
        public final static String MONTANT_CONTESTER = "montantContester";
        public final static String POURCENTAGE_MONTANT_NON_CONTESTER = "pourcentageMontantNonContester";
        public final static String DOCUMENT_IMPRIMER = "documentImprimer";
        public final static String DOCUMENT_PAYER = "documentPayer";
        public final static String MONTANT_DU = "montantDu";
        public final static String PV_DOCUMENT = "pvDocument";
        public final static String AMOUNT = "amount";
        public final static String IS_ADVANCED_SEARCH = "isAdvancedSearch";
        public final static String TYPE_REGISTER = "typeRegister";
        public final static String RECLAMATION_ID = "reclamationId";
        public final static String RECOURS_ID = "recoursId";
        public final static String DETAIL_RECLAMATION_ID = "detailReclamationId";
        public final static String DATE_RECEPTION_COURRIER = "dateReceptionCourrier";
        public final static String ETAT = "etat";
        public final static String VALUE_STATE = "valueState";

        public final static String AssujettiCode = "assujettiCode";
        public final static String AssujettiName = "assujettiName";

        public final static String AdresseName = "adresseName";

        public final static String LegalFormName = "legalFormName";

        public final static String AGENT_CREATE_ID = "agentCreateId";
        public final static String AGENT_CREATE_NAME = "agentCreateName";
        public final static String AGENT_CREATE_QUALITY = "agentCreateQuality";

        public final static String DELAIS_RECOURS_DEPASSE = "delaisRecoursDepasse";

        public final static String DETAILS_RECLAMATION_IS_EXIST = "detailsReclamationIsExist";

        public final static String DECISION_ID = "decisionId";
        public final static String DOCUMENT_REFERENCE = "documentReference";
        public final static String AMOUNT_DEGREVE = "amountDegre";
        public final static String TYPE_REMISE_GRACIEUSE = "typeRemiseGracieuse";
        public final static String DOCUMENT_ARCHIVE = "documentArchive";
        public final static String TYPE_TRAITEMENT = "typeTraitement";

        public final static String CodeBap = "codeBap";
        public final static String CODE_GESTIONNAIRE = "codeGestionnaire";

        public final static String REFERENCE_DOCUMENT_MANUEL = "referenceDocumentManuel";

        public final static String DECLARATION_DOCUMENT = "declarationDocument";
        public final static String OBSERVATION_DOC = "observation";
        public final static String LIBELLE_DOCUMENT = "libelleDocument";

    }

    public class ResultCode {

        public final static String FAILED_EXCEPTION_SAVE_PRINT_BAP = "6";
    }

    public class ParamQuery {

        public final static String AGENT_CREAT = "AGENT_CREAT";
        public final static String OBSERVATION = "OBSERVATION";
        public final static String TYPE_RECL = "TYPE_RECL";
        public final static String RECLAMMATION_ID = "RECLAMMATION_ID";
        public final static String FK_PESONNE = "FK_PERSONNE";
        public final static String REFERENCE_COURIER_RECLAMATION = "REFERENCE_COURIER_RECLAMATION";
        public final static String ETAT_RECLAMATION = "ETAT_RECLAMATION";
    }

    public class DecisionTraitement {

        public final static String DEGREVEMENT_PARTIEL = "1";
        public final static String DEGREVEMENT_TOTAL = "2";
        public final static String REJET_RECLAMATION = "3";
        public final static String ADMISSION_EN_NON_VALEUR = "4";
        public final static String REMISE_GRACIEUSE = "5";
    }
}
