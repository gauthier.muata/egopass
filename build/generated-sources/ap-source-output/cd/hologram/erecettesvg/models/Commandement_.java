package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Contrainte;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Commandement.class)
public class Commandement_ { 

    public static volatile SingularAttribute<Commandement, BigDecimal> montantFraisPoursuite;
    public static volatile SingularAttribute<Commandement, Date> dateCreat;
    public static volatile SingularAttribute<Commandement, BigDecimal> montantPoursuivi;
    public static volatile SingularAttribute<Commandement, Date> dateExigibilite;
    public static volatile SingularAttribute<Commandement, Integer> etat;
    public static volatile SingularAttribute<Commandement, Contrainte> idContrainte;
    public static volatile SingularAttribute<Commandement, String> numeroDocument;
    public static volatile SingularAttribute<Commandement, String> faitGenerateur;
    public static volatile SingularAttribute<Commandement, Date> dateEmissionCmdt;
    public static volatile SingularAttribute<Commandement, Boolean> etatImpression;
    public static volatile SingularAttribute<Commandement, Date> dateEcheance;
    public static volatile SingularAttribute<Commandement, String> id;
    public static volatile SingularAttribute<Commandement, Date> dateReception;

}