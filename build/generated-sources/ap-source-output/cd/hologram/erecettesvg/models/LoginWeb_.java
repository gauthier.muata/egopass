package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Personne;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(LoginWeb.class)
public class LoginWeb_ { 

    public static volatile SingularAttribute<LoginWeb, String> password;
    public static volatile SingularAttribute<LoginWeb, Date> dateCreation;
    public static volatile SingularAttribute<LoginWeb, String> fkPersonne;
    public static volatile SingularAttribute<LoginWeb, String> mail;
    public static volatile SingularAttribute<LoginWeb, Personne> personne;
    public static volatile SingularAttribute<LoginWeb, String> telephone;
    public static volatile SingularAttribute<LoginWeb, Integer> agentCreat;
    public static volatile SingularAttribute<LoginWeb, Integer> etat;
    public static volatile SingularAttribute<LoginWeb, String> username;

}