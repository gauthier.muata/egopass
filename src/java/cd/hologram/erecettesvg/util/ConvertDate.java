/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.util;

import cd.hologram.erecettesvg.constants.GeneralConst;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author gauthier.muata
 */
public class ConvertDate {

    public static String formatDateToStringOfFormat(Date date, String format) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatDateHeureToString(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String timeDate(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat("HH");
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String getValidFormatDate(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {
            String[] parts = date.split("-");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];
            result = part3 + GeneralConst.EMPTY_STRING + part2 + GeneralConst.EMPTY_STRING + part1;
        }

        return result;
    }

    public static String getValidFormatDatePrint(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {
            String[] parts = date.split("-");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];
            result = part3 + GeneralConst.EMPTY_STRING + part2 + GeneralConst.EMPTY_STRING + part1;

            result = part3 + "/" + part2 + "/" + part1;
        }

        return result;
    }
    
    public static String getValidFormatDatePrintV2(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {
            String[] parts = date.split("-");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];
            result = part3 + GeneralConst.EMPTY_STRING + part2 + GeneralConst.EMPTY_STRING + part1;

            result = part3 + "-" + part2 + "-" + part1;
        }

        return result;
    }

    public static int dateToInt(Date date) {
        int result = -1;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            result = Integer.parseInt(dateFormat.format(date));
        } catch (Exception e) {
        }
        return result;
    }

    public static int daysBetween(Date d1, Date d2) {
        return (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    public static long getMonthsBetween(Date date1, Date dateJour) {
        Calendar otherDay = new GregorianCalendar();
        otherDay.setTime(date1);
        Calendar today = new GregorianCalendar();
        today.setTime(dateJour);
        int yearsInBetween = today.get(Calendar.YEAR) - otherDay.get(Calendar.YEAR);
        int monthsDiff = today.get(Calendar.MONTH) - otherDay.get(Calendar.MONTH);

        long months = (yearsInBetween * 12 + monthsDiff) + 1;
        return months;
    }

    public static long getMonthsBetweenV2(Date dateLimite, Date dateJour, int periode) {

        Calendar limitDate = new GregorianCalendar();
        limitDate.setTime(dateLimite);

        Calendar today = new GregorianCalendar();
        today.setTime(dateJour);

        int yearsInBetween = today.get(Calendar.YEAR) - limitDate.get(Calendar.YEAR);
        int monthsDiff = today.get(Calendar.MONTH) - limitDate.get(Calendar.MONTH);

        //System.out.println("Péiode ID : " + periode + " year : " + yearsInBetween);

        long months = 0;

        if (yearsInBetween > 1) {

            months = (yearsInBetween * 12 + monthsDiff) + 1;

        } else {
            int monthToDay = (today.get(Calendar.MONTH) + 1);
            int monthLimitDate = (limitDate.get(Calendar.MONTH) + 1);

            months = (monthLimitDate - monthToDay);
        }

        return Math.abs(months);
    }

    public static String formatDateToString(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static Date addDayOfDate(Date date, int day) {
        GregorianCalendar calendar = new java.util.GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    public static Date formatDate(String date) {
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
            Date dt = dateformat.parse(date);
            return dt;
        } catch (Exception e) {
            return null;
        }
    }

    public static String formatDateHeureToStringV2(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss");
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String formatDateToStringV2(Date date) {
        String result = GeneralConst.EMPTY_STRING;
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            result = dateFormat.format(date);
        } catch (Exception e) {
        }
        return result;
    }

    public static String getValidFormatDateString(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {
            String[] parts = date.split("-");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];
            result = part3 + "/" + part2 + "/" + part1;
        }

        return result;
    }
    public static String getYearPartDateFromDateParam(String date) {
        String result = GeneralConst.EMPTY_STRING;

        if (date != null && !date.isEmpty()) {
            String[] parts = date.split("/");
            String part1 = parts[0];
            String part2 = parts[1];
            String part3 = parts[2];
            
            result = part3 ;
        }

        return result;
    }

    public static Date formatDateV2(String date) {
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
            Date dt = dateformat.parse(date);
            return dt;
        } catch (Exception e) {
            return null;
        }
    }
    
    public static Date formatDateV3(String date) {
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy");
            Date dt = dateformat.parse(date);
            return dt;
        } catch (Exception e) {
            return null;
        }
    }

}
