/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.DeclarationMock;
import cd.hologram.erecettesvg.pojo.DeclarationPenaliteMock;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.pojo.PeriodeDeclarationSelection;
import cd.hologram.erecettesvg.sql.*;
import cd.hologram.erecettesvg.util.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author emmanuel.tsasa
 */
public class AssujettissementBusiness {

    static Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    private static final Dao myDao = new Dao();

    public static List<TypeBien> getTypeBien() {
        try {
            String query = SQLQueryAssujettissement.SELECT_TYPES_BIENS;
            List<TypeBien> typeBiens = (List<TypeBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBien.class), false);
            return typeBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<RetraitDeclaration> getListNoteTaxationPenaliteByPrincipal(int principalID) {
        try {
            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE RETRAIT_DECLARATION_MERE = ?1";
            List<RetraitDeclaration> noteTaxationList = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, principalID);
            return noteTaxationList;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailVoucher getVoucherByCode(String code) {
        try {

            String query = "select * from T_DETAIL_VOUCHER WITH (READPAST) WHERE CODE_VOUCHER = ?1";

            List<DetailVoucher> vouchers = (List<DetailVoucher>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailVoucher.class), false, code.toUpperCase());
            return vouchers.isEmpty() ? null : vouchers.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static TicketPeage getTicketPeageByCodeAndSite(String code, String site) {
        try {

            String query = "select * from t_ticket_peage where CODE like '%"+code+"%'  and SUBSTRING(code,len(code)-1,2) = (select SIGLE from T_SITE where CODE =?1)";

            List<TicketPeage> ticketPeages = (List<TicketPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TicketPeage.class), false, site);
            return ticketPeages.isEmpty() ? null : ticketPeages.get(0);

        } catch (Exception e) {
            throw (e);
        }

    }

    public static TicketPeage getTicketByCode(String code) {
        try {

            String query = "SELECT * FROM T_TICKET_PEAGE WITH (READPAST) WHERE CODE LIKE ?1";

            List<TicketPeage> ticketPeages = (List<TicketPeage>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TicketPeage.class), true, code);
            return ticketPeages.isEmpty() ? null : ticketPeages.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }
    
    public static Commande getCommandeByRef(String ref) {
        try {

            String query = "SELECT * FROM T_COMMANDE WITH (READPAST) WHERE REFERENCE = ?1";

            List<Commande> commandes = (List<Commande>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Commande.class), false, ref);
            return commandes.isEmpty() ? null : commandes.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean updateTicketByCode(String code) {
        boolean result = false;
        try {

            String query = "UPDATE T_TICKET_PEAGE SET ETAT = 1 WHERE CODE = ?1";

            result = myDao.getDaoImpl().executeNativeQuery(query, code);

        } catch (Exception e) {
            throw (e);
        }

        return result;
    }
    
     public static boolean updateCommandeCode(int code) {
        boolean result = false;
        try {

            String query = "UPDATE T_COMMANDE SET ETAT = 1 WHERE ID = ?1";
            
            String query2 = "UPDATE T_VOUCHER SET ETAT = 1 WHERE FK_COMMANDE = ?1";

            result = myDao.getDaoImpl().executeNativeQuery(query, code);
            
            myDao.getDaoImpl().executeNativeQuery(query2, code);

        } catch (Exception e) {
            throw (e);
        }

        return result;
    }

    public static RetraitDeclaration getRetraitDeclarationByPeriode(String periode) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE FK_PERIODE = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, periode);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static RetraitDeclaration getRetraitDeclarationPrincipalByPeriode(String periode) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE FK_PERIODE = ?1 "
                    + " AND RETRAIT_DECLARATION_MERE IS NULL";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, periode);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static RetraitDeclaration getRetraitDeclarationOfTaxationOfficeByPeriode(int periode) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE FK_PERIODE = ?1 AND EST_TAXATION_OFFICE = 1 AND ETAT = 1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, periode);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Unite getUnite(String code) {

        try {

            String query = "SELECT * FROM T_unite WHERE CODE = ?1 AND  ETAT = 1";

            List<Unite> unites = (List<Unite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Unite.class), false, code);

            return unites.isEmpty() ? null : unites.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static TypeComplement getTypeComplementbyComplementBien(String codeComplementBien) {

        try {

            String query = "SELECT * from T_TYPE_COMPLEMENT WHERE CODE IN (SELECT COMPLEMENT FROM T_TYPE_COMPLEMENT_BIEN WHERE CODE IN (select TYPE_COMPLEMENT from T_COMPLEMENT_BIEN WHERE ID = ?1))";

            List<TypeComplement> typeComplements = (List<TypeComplement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeComplement.class), false, codeComplementBien);

            return typeComplements.isEmpty() ? null : typeComplements.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static RetraitDeclaration getRetraitDeclarationByCode(String codeDeclaration) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE CODE_DECLARATION = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, codeDeclaration);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static RetraitDeclaration getRetraitDeclarationByCodeV2(String codeDeclaration) {

        try {

            String query = "SELECT TOP 1 * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE NEW_ID = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, codeDeclaration);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static PeriodeDeclaration getLastPeriodeDeclarationByAssujetissement(String code) {

        try {

            String query = "select TOP 1 * from T_PERIODE_DECLARATION WHERE ASSUJETISSEMENT = ?1 order by ID";

            List<PeriodeDeclaration> periodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, code);

            return periodeDeclarations.isEmpty() ? null : periodeDeclarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static RetraitDeclaration getRetraitDeclarationByID(int id) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ID = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, id);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static RetraitDeclaration getRetraitDeclarationByID_V2(int id) {

        try {

            String query = "SELECT ID,CODE_DECLARATION FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ID = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, id);

            return declarations.isEmpty() ? null : declarations.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static RetraitDeclaration getRetraitDeclarationByCodeMere(int id) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE RETRAIT_DECLARATION_MERE = ?1 AND ETAT = 4";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, id);

            return declarations.isEmpty() ? null : declarations.get(0);

        } catch (Exception e) {
            throw e;
        }

    }

    public static List<RetraitDeclaration> getListRetraitDeclarationByCodeMere(int id) {

        try {

            String query = "SELECT * FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE RETRAIT_DECLARATION_MERE = ?1";

            List<RetraitDeclaration> declarations = (List<RetraitDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(RetraitDeclaration.class), false, id);

            return declarations;

        } catch (Exception e) {
            throw e;
        }

    }

    public static Palier getPalierByAbTarifAndTypePersonne(String codeArticle, String codeTarif, String codeFormeJ) {

        try {
            String query = SQLQueryAssujettissement.SELECT_PALIER_BY_AB_TARIF_AND_TYPE_PERSONNE;
            List<Palier> paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeTarif, codeFormeJ);
            return paliers.isEmpty() ? null : paliers.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Palier getPalierByAbTarifAndTypePersonneV2(String codeArticle, String codeTarif, String codeFormeJ, String codeQuartier, String codeNature) {

        try {

            String query = GeneralConst.EMPTY_STRING;
            List<Palier> paliers;
            Palier palierObj = null;

            switch (codeArticle) {

                case "00000000000002282020": // RL
                case "00000000000002292020": // IRL

                    query = "SELECT top 1 * from T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1";
                    paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle);

                    if (!paliers.isEmpty()) {
                        palierObj = paliers.get(0);
                    }

                    break;

                case "00000000000002272020": // IF

                    if (codeNature.equals("TB00000026")) {

                        query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND FK_TYPE_BIEN = ?4";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeFormeJ, codeTarif, codeNature);

                        if (!paliers.isEmpty()) {

                            if (paliers.size() > 1) {

                                for (Palier palier : paliers) {

                                    if (palier.getFkEntiteAdministrative().equals(codeQuartier)) {
                                        palierObj = new Palier();

                                        palierObj = palier;
                                    }

                                }
                            } else if (paliers.size() == 1) {
                                palierObj = paliers.get(0);
                            }
                        }
                    } else {

                        query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND FK_TYPE_BIEN = ?4";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeFormeJ, codeTarif, codeNature);

                        if (!paliers.isEmpty()) {
                            palierObj = paliers.get(0);
                        }

                    }
                    break;

                case "00000000000002302020": // VIGNETTE

                    switch (codeNature) {

                        case "TB00000037":
                        case "TB00000038":
                        case "TB00000039":

                            query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND FK_TYPE_BIEN = ?2";

                            paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeNature);

                            if (!paliers.isEmpty()) {
                                palierObj = paliers.get(0);
                            }
                            break;

                        default:
                            query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3";

                            paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeFormeJ, codeTarif);

                            if (!paliers.isEmpty()) {
                                palierObj = paliers.get(0);
                            }
                            break;
                    }

                    break;
                default: // OTHERS
                    query = SQLQueryAssujettissement.SELECT_PALIER_BY_AB_TARIF_AND_TYPE_PERSONNE_V2;
                    paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeTarif,
                            codeFormeJ, codeQuartier, codeNature);

                    if (!paliers.isEmpty()) {
                        palierObj = paliers.get(0);
                    }
                    break;
            }

            return palierObj;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static Palier getPalierByAbTarifAndFJuridique(String codeArticle, String codeTarif, String codeFormeJ, float valeurBase, boolean isPalier) {

        List<Palier> listPaliers;

        if (!isPalier) {

            String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_PALIER_BY_TARIF_AND_FORME_JURIDIQUE;
            listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Palier.class), false,
                    codeArticle.trim(), codeTarif.trim(), codeFormeJ.trim());

        } else {

            String sqlQuery = SQLQueryTaxation.SimpleQuery.SELECT_PALIER_BY_TARIF_AND_FORME_JURIDIQUE_AND_BASE;
            listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Palier.class), false,
                    codeArticle.trim(), codeTarif.trim(), codeFormeJ.trim(), valeurBase);
        }

        return listPaliers.isEmpty() ? null : listPaliers.get(0);
    }

    public static Palier getPalierForBienImmobilier(
            String codeArticle,
            String codeTarif,
            String codeFormeJuridique,
            String codeCommune,
            float valeurBase,
            boolean isPalier,
            String codeNature) {

        List<Palier> listPaliers;
        Palier palierObj = null;

        if (!isPalier) {

            String sqlQuery = GeneralConst.EMPTY_STRING;

            switch (codeArticle) {

                case "00000000000002282020": // RL
                case "00000000000002292020": // IRL
                case "00000000000002352021": // RL 22% MOIS
                case "00000000000002362021": // RL 22% L'AN

                    //sqlQuery = "SELECT top 1 * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 AND ETAT = 1";
                    sqlQuery = "SELECT top 1 * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1";

                    listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery, Casting.getInstance().convertIntoClassType(Palier.class), false,
                            codeArticle.trim());

                    if (!listPaliers.isEmpty()) {
                        palierObj = listPaliers.get(0);
                    }
                    /*listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery, Casting.getInstance().convertIntoClassType(Palier.class), false,
                     codeArticle.trim(), codeTarif);*/
                    break;

                case "00000000000002272020": // IF

                    if (codeNature.equals("TB00000026")) {

                        sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND FK_TYPE_BIEN = ?4";

                        listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeFormeJuridique, codeTarif, codeNature);

                        if (!listPaliers.isEmpty()) {

//                            if (listPaliers.size() > 1) {
//
//                                for (Palier palier : listPaliers) {
//
//                                    if (palier.getFkEntiteAdministrative().equals(codeCommune)) {
//                                        palierObj = new Palier();
//
//                                        palierObj = palier;
//                                    }
//
//                                }
//                            } else if (listPaliers.size() == 1) {
//                                palierObj = listPaliers.get(0);
//                            }
                            palierObj = listPaliers.get(0);
                        }

                    } else if (codeNature.equals("TB00000031") || codeNature.equals("TB00000035") || codeNature.equals("TB00000033")
                            || codeNature.equals("TB00000034") || codeNature.equals("TB00000032") || codeNature.equals("TB00000030")) {

                        sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND FK_TYPE_BIEN = ?3";

                        listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeFormeJuridique, codeNature);

                        if (!listPaliers.isEmpty()) {
                            palierObj = listPaliers.get(0);
                        }

                    } else if (codeNature.equals("TB00000023")) { // ANTENNE DE COMMUNICATION

                        sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND FK_TYPE_BIEN = ?2";

                        listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeNature);

                        if (!listPaliers.isEmpty()) {
                            palierObj = listPaliers.get(0);
                        }

                    } else {

                        sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND FK_TYPE_BIEN = ?4";

                        listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeFormeJuridique, codeTarif, codeNature);

                        if (!listPaliers.isEmpty()) {
                            palierObj = listPaliers.get(0);
                        }

                    }

                    break;

                case "00000000000002302020": // VIGNETTE

                    switch (codeNature) {

                        case "TB00000037":
                        case "TB00000038":
                        case "TB00000039":

                            sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND FK_TYPE_BIEN = ?2";

                            listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeNature);

                            if (!listPaliers.isEmpty()) {
                                palierObj = listPaliers.get(0);
                            }
                            break;
                        default:

                            sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3";

                            listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeArticle, codeFormeJuridique, codeTarif);

                            if (!listPaliers.isEmpty()) {
                                palierObj = listPaliers.get(0);
                            }
                            break;
                    }

                    break;
                default:
                    sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 "
                            + " AND TYPE_PERSONNE = ?3 AND FK_ENTITE_ADMINISTRATIVE = ?4 AND ETAT = 1 AND FK_TYPE_BIEN = ?5";

                    listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery, Casting.getInstance().convertIntoClassType(Palier.class), false,
                            codeArticle.trim(), codeTarif.trim(), codeFormeJuridique.trim(), codeCommune.trim(), codeNature.trim());

                    if (!listPaliers.isEmpty()) {
                        palierObj = listPaliers.get(0);
                    }
                    break;
            }

        } else {

            /*String sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST)"
             + " WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 AND (TYPE_PERSONNE = ?3 OR TYPE_PERSONNE = '*')"
             + " AND ETAT = 1 AND (TAUX >= BORNE_INFERIEUR AND TAUX <= BORNE_SUPERIEUR) AND FK_ENTITE_ADMINISTRATIVE = ?4";*/
            String sqlQuery = "SELECT * FROM T_PALIER WITH (READPAST)"
                    + " WHERE ARTICLE_BUDGETAIRE = ?1 AND TARIF = ?2 AND (TYPE_PERSONNE = ?3)"
                    + " AND ETAT = 1 AND (TAUX >= BORNE_INFERIEUR AND TAUX <= BORNE_SUPERIEUR) AND FK_ENTITE_ADMINISTRATIVE = ?4";

            listPaliers = (List<Palier>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(Palier.class), false,
                    codeArticle.trim(), codeTarif.trim(), codeFormeJuridique.trim(), valeurBase, codeCommune.trim());

            if (!listPaliers.isEmpty()) {
                palierObj = listPaliers.get(0);
            }
        }

        //return listPaliers.isEmpty() ? null : listPaliers.get(0);
        return palierObj;
    }

    public static Boolean saveRetraitDeclaration(List<DeclarationMock> declarationListMock) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            for (DeclarationMock declarationMock : declarationListMock) {

                counter++;

                for (PeriodeDeclarationSelection pds : declarationMock.getListPeriode()) {
                    counter++;

                    bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_RETRAIT_DECLARATION_X, new Object[]{
                        declarationMock.getRequerant(),
                        declarationMock.getAssujetti(),
                        declarationMock.getCodeArticleBudgetaire(),
                        pds.getPeriode(),
                        declarationMock.getCodeAgentCreate(),
                        null,//declarationMock.getValeurBase(),
                        pds.getPrincipal(),
                        declarationMock.getDevise(),
                        declarationMock.getCodeDeclaration(),
                        pds.getPenalise(),
                        pds.getPenalite(),
                        declarationMock.getBanque(),
                        declarationMock.getCompteBancaire(),
                        declarationMock.getAmountRemisePenalite().floatValue(),
                        declarationMock.getTauxRemise(),
                        declarationMock.getObservationRemise()
                    // ,declarationMock.getDateCreate()
                    });

                    if (declarationMock.getCodeArticleBudgetaire().equals(propertiesConfig.getProperty("CODE_IMPOT_ICM"))) {

                        counter++;

                        bulkQuery.put(counter + ":UPDATE T_PERIODE_DECLARATION SET OBSERVATION = ?1 WHERE ID = ?2", new Object[]{
                            declarationMock.getIcmParamId(),
                            Integer.valueOf(pds.getPeriode())
                        });
                    }
                }

            }

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }

        return result;

    }

    public static Boolean saveRetraitDeclarationV2(List<DeclarationMock> declarationListMock, String numDeclaration, DeclarationPenaliteMock declarationPenaliteMock) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            for (DeclarationMock declarationMock : declarationListMock) {

                //counter++;
                for (PeriodeDeclarationSelection pds : declarationMock.getListPeriode()) {

                    counter++;

                    bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_RETRAIT_DECLARATION_X, new Object[]{
                        declarationMock.getRequerant(),
                        declarationMock.getAssujetti(),
                        declarationMock.getCodeArticleBudgetaire(),
                        pds.getPeriode(),
                        declarationMock.getCodeAgentCreate(),
                        null,
                        pds.getPrincipal(),
                        declarationMock.getDevise(),
                        numDeclaration,//declarationMock.getCodeDeclaration(),
                        pds.getPenalise(),
                        pds.getPenalite(),
                        declarationMock.getBanque(),
                        declarationMock.getCompteBancaire(),
                        declarationMock.getAmountRemisePenalite().floatValue(),
                        declarationMock.getTauxRemise(),
                        declarationMock.getObservationRemise()
                    });

                    if (declarationMock.getCodeArticleBudgetaire().equals(propertiesConfig.getProperty("CODE_IMPOT_ICM"))) {

                        counter++;

                        bulkQuery.put(counter + ":UPDATE T_PERIODE_DECLARATION SET OBSERVATION = ?1 WHERE ID = ?2", new Object[]{
                            declarationMock.getIcmParamId(),
                            Integer.valueOf(pds.getPeriode())
                        });
                    }

                }
            }

            if (declarationPenaliteMock != null) {

                counter++;

                bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_RETRAIT_DECLARATION_PENALITE, new Object[]{
                    declarationListMock.get(0).getRequerant(),
                    declarationListMock.get(0).getAssujetti(),
                    declarationListMock.get(0).getCodeArticleBudgetaire(),
                    null,
                    declarationListMock.get(0).getCodeAgentCreate(),
                    null,
                    declarationPenaliteMock.getAmountPenalite(),
                    declarationListMock.get(0).getDevise(),
                    numDeclaration,//declarationMock.getCodeDeclaration(),
                    0,
                    0,
                    declarationListMock.get(0).getBanque(),
                    declarationListMock.get(0).getCompteBancaire(),
                    0,//declarationMock.getAmountRemisePenalite().floatValue(),
                    declarationPenaliteMock.getTauxRemise(),
                    declarationPenaliteMock.getObservationRemise()
                });

            }

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }

        return result;

    }

    public static List<TypeComplementBien> getTypeComplementBienByTypeBien(String codeTypeBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_TYPES_COMPLEMENTS_BIENS;
            List<TypeComplementBien> typeComplementBiens = (List<TypeComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeComplementBien.class), false, codeTypeBien);
            return typeComplementBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String createBien(Bien bien, Acquisition acquisition) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = AssujettissementConst.ParamQUERY.BIENID;
        try {
            String query = SQLQueryAssujettissement.F_NEW_BIEN;

            params.put(AssujettissementConst.ParamQUERY.INTITULE, bien.getIntitule().trim());
            params.put(AssujettissementConst.ParamQUERY.DESCRIPTION, bien.getDescription().trim());
            params.put(AssujettissementConst.ParamQUERY.PERSONNE, acquisition.getPersonne().getCode().trim());
            params.put(AssujettissementConst.ParamQUERY.ADRESSE_PERSONNE, bien.getFkAdressePersonne().getCode() == null
                    ? GeneralConst.EMPTY_STRING
                    : bien.getFkAdressePersonne().getCode().trim());
            params.put(AssujettissementConst.ParamQUERY.TYPE_BIEN, bien.getTypeBien().getCode().trim());
            //params.put(AssujettissementConst.ParamQUERY.AGENT_CREAT, bien.);
            params.put(AssujettissementConst.ParamQUERY.DATE_ACQUISITION, acquisition.getDateAcquisition());

            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static boolean executeQueryBulkInsert(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }

    public static List<Acquisition> getBiensOfPersonne(String codePersonne, String codeService) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIENS_OF_PERSONNE;
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne, codeService);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Acquisition> getBiensOfPersonneV3(String codePersonne) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIENS_OF_PERSONNE_V3;
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Acquisition> getBiensOfPersonneV4(String codePersonne, String codeSevice, String codeTarif) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIENS_OF_PERSONNE_V4;
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne, codeSevice, codeTarif);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Acquisition> getBiensOfPersonneV2(String codePersonne, String param) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIENS_OF_PERSONNE_V2;

            query = String.format(query, param);

            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Acquisition> getBiensByIdBienAndPersonne(String codePersonne, String idBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIENS_BY_PERSONNE_AND_ID_BIEN;
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne, idBien);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getResponsableBien(String idBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_RESPONSABLE_BIEN;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, idBien);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

//    public static boolean createAcquisition(Acquisition acquisition) {
//
//        String query = SQLQueryAssujettissement.F_NEW_ACQUISITION_BIEN;
//
//        boolean result = myDao.getDaoImpl().executeStoredProcedure(
//                query,
//                acquisition.getPersonne().getCode(),
//                acquisition.getBien().getId(),
//                acquisition.getDateAcquisition(),
//                acquisition.getProprietaire(),
//                acquisition.getReferenceContrat(),
//                acquisition.getNumActeNotarie(),
//                acquisition.getDateActeNotarie());
//
//        return result;
//    }
    public static List<AbComplementBien> getAssujettissableAbByBien(String idBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_AB_COMPLEMENT_BY_BIEN;
            List<AbComplementBien> abComplementBiens = (List<AbComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AbComplementBien.class), false, idBien);
            return abComplementBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static ComplementBien getComplementBien(String codeTypeComplement, String idBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_COMPLEMENT_BIEN_BY_TYPE_COMPLEMENT_AND_BIEN;
            List<ComplementBien> complementBiens = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class),
                    false,
                    codeTypeComplement,
                    idBien);
            return complementBiens.isEmpty() ? null : complementBiens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Periodicite> getListPeriodicites() {
        try {
            String query = SQLQueryAssujettissement.SELECT_PERIODICITES;
            List<Periodicite> periodicites = (List<Periodicite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Periodicite.class), false);
            return periodicites;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Palier> getListPaliersByAB(String codeAB, String type, String tarif) {
        try {

            String query = "";

            List<Palier> paliers;

            if (tarif == null) {

                if (codeAB.equals("00000000000002282020")) {
                    query = SQLQueryAssujettissement.SELECT_PALIERS_BY_AB_V2;

                    paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB);
                } else {
                    query = SQLQueryAssujettissement.SELECT_PALIERS_BY_AB;

                    paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type);
                }

            } else {
                query = tarif.isEmpty() ? SQLQueryAssujettissement.SELECT_PALIERS_BY_AB : SQLQueryAssujettissement.SELECT_PALIERS_BY_AB_TARIF;

                paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type);
            }

            /*if (tarif.isEmpty()) {
             paliers = (List<Palier>) myDao.getDaoImpl().find(query,
             Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type);
             } else {
             paliers = (List<Palier>) myDao.getDaoImpl().find(query,
             Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, tarif, type);
             }*/
            return paliers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Palier> getListPaliersByABV2(String codeAB, String type, String tarif, String codeQuartier, String typeBien) {
        try {

            String query = "";

            List<Palier> paliers = new ArrayList<>();
            Palier palierObj;

            switch (codeAB) {

                case "00000000000002302020": // VIGNETTE

                    switch (typeBien) {

                        case "TB00000037":
                        case "TB00000038":
                        case "TB00000039":

                            query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND FK_TYPE_BIEN = ?2";

                            paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, typeBien);
                            break;
                        default:

                            query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3";

                            paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type, tarif);

                            break;
                    }

                    break;

                case "00000000000002282020": // RL
                case "00000000000002292020": // IRL
                case "00000000000002352021": // RL 22% MOIS
                case "00000000000002362021": // RL 22% L'AN

                    query = "SELECT TOP 1 * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1";

                    paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB);

                    break;

                case "00000000000002272020": // IF

                    //if(typeBien.equals(paliers))
                    /*query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND FK_TYPE_BIEN = ?4";

                     paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                     Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type, tarif, typeBien);*/
                    if (typeBien.equals("TB00000026")) {

                        query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND FK_TYPE_BIEN = ?4";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type, tarif, typeBien);

                        if (!paliers.isEmpty()) {

                            if (paliers.size() > 1) {

                                for (Palier palier : paliers) {

                                    if (palier.getFkEntiteAdministrative().equals(codeQuartier)) {
                                        palierObj = new Palier();

                                        palierObj = palier;
                                    }

                                }
                            } else if (paliers.size() == 1) {
                                palierObj = paliers.get(0);
                            }
                        }

                    } else if (typeBien.equals("TB00000031") || typeBien.equals("TB00000035") || typeBien.equals("TB00000033")
                            || typeBien.equals("TB00000034") || typeBien.equals("TB00000032") || typeBien.equals("TB00000030")) {

                        query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND FK_TYPE_BIEN = ?3";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type, typeBien);

                        if (!paliers.isEmpty()) {
                            palierObj = paliers.get(0);
                        }

                    } else if (typeBien.equals("TB00000023")) { // ANTENNE DE COMMUNICATION

                        query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND FK_TYPE_BIEN = ?2";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, typeBien);

                        if (!paliers.isEmpty()) {
                            palierObj = paliers.get(0);
                        }

                    } else {

                        query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TYPE_PERSONNE = ?2 AND TARIF = ?3 AND FK_TYPE_BIEN = ?4";

                        paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                                Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, type, tarif, typeBien);

                        if (!paliers.isEmpty()) {
                            palierObj = paliers.get(0);
                        }

                    }

                    break;
                case "00000000000002312021": // ICM
                case "00000000000002102016": // PUBLICITE

                    query = "SELECT * FROM T_PALIER WITH (READPAST) WHERE ARTICLE_BUDGETAIRE = ?1 AND ETAT = 1 AND TARIF = ?2";

                    paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                            Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, tarif);

                    break;
            }
            //}

            return paliers;
        } catch (Exception e) {
            throw (e);
        }
    }

//    public static List<Palier> getListPaliersByAB(String codeAB, String formeJurid, String tarif) {
//        try {
//            String query = tarif.isEmpty() ? SQLQueryAssujettissement.SELECT_PALIERS_BY_AB_FORME_JURIDIQUE : SQLQueryAssujettissement.SELECT_PALIERS_BY_AB_TARIF;
//            
//            List<Palier> paliers = (List<Palier>) myDao.getDaoImpl().find(query,
//                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, formeJurid);
//            
//            
//            return paliers;
//        
//        } catch (Exception e) {
//            throw (e);
//        }
//    }
    public static Assujeti getExistAssujettissement(String idBien, String codeAB, String idComplementBien, String codePersonne) {

        String query;
        List<Assujeti> assujetis;

        if (idBien.equals(GeneralConst.EMPTY_STRING) || idBien.equals(GeneralConst.Number.ZERO)) {

            query = SQLQueryAssujettissement.SELECT_EXISTED_ASSUJETTI_SANS_BIEN;
            assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false,
                    codeAB,
                    codePersonne);

        } else {

            query = SQLQueryAssujettissement.SELECT_EXISTED_ASSUJETTI;
            assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false,
                    idBien,
                    codeAB,
                    idComplementBien);
        }

        try {

            return assujetis.isEmpty() ? null : assujetis.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ArticleBudgetaire> getArticlesBudgetairesAssujettissable(String libelle) {
        try {
            String query = SQLQueryAssujettissement.LIST_ARTICLE_BUDGETAIRE_ASSUJETISSABLE;
            List<ArticleBudgetaire> articleBudgetaires = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class),
                    true, libelle);
            return articleBudgetaires;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Assujeti> getAssujettissementByPersonne(String codePersonne) {
        try {
            String query = SQLQueryAssujettissement.LIST_ASSUJETTISSEMENT_BY_PERSONNE_V3;
            List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class),
                    false,
                    codePersonne);
            return assujetis;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<PeriodeDeclaration> getPeriodeDeclarationDispo(String idAssujettissement) {
        try {
            String query = SQLQueryAssujettissement.LIST_PERIODE_DECLARATION_DISPO;
            List<PeriodeDeclaration> periodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class),
                    false,
                    idAssujettissement);
            return periodeDeclarations;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean savePeriodesDeclarations(String idAssujettissement,
            List<PeriodeDeclaration> periodeDeclarations, String dateFin) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {
            for (PeriodeDeclaration declaration : periodeDeclarations) {
                //bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_PERIODE_DECLARATION_V2, new Object[]{
                bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_PERIODE_DECLARATION_V3, new Object[]{
                    idAssujettissement,
                    declaration.getDebut(),
                    declaration.getFin(),
                    declaration.getDateLimite(),
                    null,
                    null
                });
                counter++;
            }

            bulkQuery.put(counter + SQLQueryAssujettissement.UPDATE_DATE_FIN_ASSUJETTISSEMENT, new Object[]{
                dateFin,
                idAssujettissement
            });

            result = executeQueryBulkInsert(bulkQuery);
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean removeAssujettissement(String idAssujettissement) {

        String query = SQLQueryAssujettissement.DESACTIVATE_ASSUJETTISSEMENT;

        boolean result = myDao.getDaoImpl().executeStoredProcedure(
                query, idAssujettissement);

        return result;
    }

    public static Bien getBienById(String idBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIEN_BY_ID;
            List<Bien> biens = (List<Bien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bien.class), false, idBien);
            return biens.isEmpty() ? null : biens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Palier getFistPalierByArticleBudgetaire(String codeAB) {
        try {
            String query = SQLQueryAssujettissement.SELECT_PALIERS_BY_AB;
            List<Palier> paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB);
            return paliers.isEmpty() ? null : paliers.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Palier getFistPalierByArticleBudgetaireV2(String codeAB, String codeFormeJuridique) {
        try {
            String query = SQLQueryAssujettissement.SELECT_PALIERS_BY_AB;
            List<Palier> paliers = (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false, codeAB, codeFormeJuridique);
            return paliers.isEmpty() ? null : paliers.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static AbComplementBien getAbComplementBienByArticleBudgetaire(String codeAB) {
        try {
            String query = SQLQueryAssujettissement.SELECT_AB_COMPLEMENT_BIEN_BY_AB;
            List<AbComplementBien> abComplementBiens = (List<AbComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AbComplementBien.class), false, codeAB);
            return abComplementBiens.isEmpty() ? null : abComplementBiens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String getNewid() {

        HashMap<String, Object> params = new HashMap<>();
        String query = "F_GET_NEWID";
        String returnValue = "ID";

        params.put("X", "");

        String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);
        return result;
    }

    public static Boolean saveBien(Bien bien, Acquisition acquisition, List<ComplementBien> complementBiens, Integer agentID) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            if (bien.getId() == null || bien.getId().isEmpty()) {

                String returnValue = AssujettissementConst.ParamQUERY.BIENID;
                String query = SQLQueryAssujettissement.F_NEW_BIEN;
                params.put(AssujettissementConst.ParamQUERY.INTITULE, bien.getIntitule().trim());
                params.put(AssujettissementConst.ParamQUERY.DESCRIPTION, bien.getDescription().trim());
                params.put(AssujettissementConst.ParamQUERY.PERSONNE, acquisition.getPersonne().getCode().trim());
                params.put(AssujettissementConst.ParamQUERY.ADRESSE_PERSONNE, bien.getFkAdressePersonne() == null ? null : bien.getFkAdressePersonne().getCode().trim());
                params.put(AssujettissementConst.ParamQUERY.TYPE_BIEN, bien.getTypeBien().getCode().trim());
                params.put(AssujettissementConst.ParamQUERY.DATE_ACQUISITION, acquisition.getDateAcquisition());

                params.put("CATEGORIE", bien.getFkTarif());

                params.put(AssujettissementConst.ParamQUERY.AGENT_CREAT, agentID);

                for (ComplementBien complementBien : complementBiens) {
                    bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_COMPLEMENT_BIEN, new Object[]{
                        complementBien.getTypeComplement().getCode().trim(),
                        complementBien.getValeur(),
                        agentID,
                        complementBien.getDevise()
                    });
                    counter++;
                }

                result = myDao.getDaoImpl().execBulkQuery(query, returnValue, params, bulkQuery);
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean saveBienImmobilier(Bien bien, Acquisition acquisition, List<ComplementBien> complementBiens, Integer agentID) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            if (bien.getId() == null || bien.getId().isEmpty()) {

                String returnValue = AssujettissementConst.ParamQUERY.BIENID;
                String query = SQLQueryAssujettissement.F_NEW_BIEN_IMMOBILIER_WEB;
                params.put(AssujettissementConst.ParamQUERY.INTITULE, bien.getIntitule().trim());
                params.put(AssujettissementConst.ParamQUERY.DESCRIPTION, null);
                params.put(AssujettissementConst.ParamQUERY.PERSONNE, acquisition.getPersonne().getCode().trim());
                params.put(AssujettissementConst.ParamQUERY.ADRESSE_PERSONNE, bien.getFkAdressePersonne() == null ? null : bien.getFkAdressePersonne().getCode().trim());
                params.put(AssujettissementConst.ParamQUERY.TYPE_BIEN, bien.getTypeBien().getCode().trim());
                params.put(AssujettissementConst.ParamQUERY.DATE_ACQUISITION, acquisition.getDateAcquisition());

                params.put("FK_USAGE_BIEN", bien.getFkUsageBien());
                params.put("FK_TARIF", bien.getFkTarif());
                params.put("FK_COMMUNE", bien.getFkCommune());
                params.put("FKQUARTIER", bien.getFkQuartier());

                params.put(AssujettissementConst.ParamQUERY.AGENT_CREAT, agentID);

                for (ComplementBien complementBien : complementBiens) {
                    bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_COMPLEMENT_BIEN, new Object[]{
                        complementBien.getTypeComplement().getCode().trim(),
                        complementBien.getValeur(),
                        agentID,
                        complementBien.getDevise()
                    });
                    counter++;
                }

                result = myDao.getDaoImpl().execBulkQuery(query, returnValue, params, bulkQuery);
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updateBien(Bien bien, Acquisition acquisition, List<ComplementBien> complementBiens, Integer agentID) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            if (bien.getId() != null || !bien.getId().isEmpty()) {

                bulkQuery.put(counter + SQLQueryAssujettissement.UPDATE_BIEN, new Object[]{
                    bien.getIntitule().trim(),
                    bien.getDescription().trim(),
                    bien.getTypeBien().getCode().trim(),
                    bien.getFkAdressePersonne() == null ? null : bien.getFkAdressePersonne().getCode().trim(),
                    bien.getFkTarif(),
                    bien.getId().trim()
                });
                counter++;

                for (ComplementBien complementBien : complementBiens) {

                    if (complementBien.getId().equals(GeneralConst.EMPTY_STRING)) {
                        bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_COMPLEMENT_BIEN2, new Object[]{
                            complementBien.getTypeComplement().getCode().trim(),
                            bien.getId().trim(),
                            complementBien.getValeur(),
                            agentID,
                            complementBien.getDevise()
                        });
                        counter++;
                    } else {
                        bulkQuery.put(counter + SQLQueryAssujettissement.UPDATE_COMPLEMENT_BIENS, new Object[]{
                            complementBien.getValeur(),
                            complementBien.getDevise(),
                            complementBien.getId().trim()
                        });
                        counter++;
                    }
                }
                result = executeQueryBulkInsert(bulkQuery);
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean updateBienImmobilier(Bien bien, Acquisition acquisition, List<ComplementBien> complementBiens, Integer agentID) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            if (bien.getId() != null || !bien.getId().isEmpty()) {

                bulkQuery.put(counter + SQLQueryAssujettissement.UPDATE_BIEN_IMMOBILIER, new Object[]{
                    bien.getIntitule().trim(),
                    null,
                    bien.getTypeBien().getCode().trim(),
                    bien.getFkAdressePersonne() == null ? null : bien.getFkAdressePersonne().getCode().trim(),
                    bien.getFkUsageBien(),
                    bien.getFkTarif(),
                    bien.getFkCommune(),
                    bien.getId().trim()
                });
                counter++;

                for (ComplementBien complementBien : complementBiens) {

                    if (complementBien.getId().equals(GeneralConst.EMPTY_STRING)) {
                        bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_COMPLEMENT_BIEN2, new Object[]{
                            complementBien.getTypeComplement().getCode().trim(),
                            bien.getId().trim(),
                            complementBien.getValeur(),
                            agentID,
                            complementBien.getDevise()
                        });
                        counter++;
                    } else {
                        bulkQuery.put(counter + SQLQueryAssujettissement.UPDATE_COMPLEMENT_BIENS, new Object[]{
                            complementBien.getValeur(),
                            complementBien.getDevise(),
                            complementBien.getId().trim()
                        });
                        counter++;
                    }
                }
                result = executeQueryBulkInsert(bulkQuery);
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<ComplementBien> getListComplementBien(String idBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_LIST_COMPLEMENT_BIEN_BY_BIEN;
            List<ComplementBien> listComplementBien = (List<ComplementBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ComplementBien.class), false, idBien);
            return listComplementBien;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean removeBienAcquisition(String idAcquisition) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;

        try {

            bulkQuery.put(counter + ":UPDATE T_ACQUISITION SET ETAT = 0 WHERE ID = ?1", new Object[]{
                idAcquisition});

            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean removeBienAcquisitionV2(String idAcquisition, String codeBien, String codePersonne) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;

        try {

            bulkQuery.put(counter + ":UPDATE T_ACQUISITION SET ETAT = 0 WHERE ID = ?1", new Object[]{
                idAcquisition});

            counter++;

            bulkQuery.put(counter + ":UPDATE T_ASSUJETI SET ETAT = 0 WHERE BIEN = ?1 AND PERSONNE = ?2", new Object[]{
                codeBien, codePersonne});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean createAcquisition(Acquisition acquisition) {

        String query = SQLQueryAssujettissement.F_NEW_ACQUISITION_BIEN;

        boolean result = myDao.getDaoImpl().executeStoredProcedure(
                query,
                acquisition.getPersonne().getCode(),
                acquisition.getBien().getId(),
                acquisition.getDateAcquisition(),
                acquisition.getProprietaire(),
                acquisition.getReferenceContrat(),
                acquisition.getNumActeNotarie(),
                acquisition.getDateActeNotarie());
        return result;
    }

    public static Assujeti getExistAssujettissement(String idBien, String codeAB, String idComplementBien, String codePersonne, String tarif) {

        String query;
        List<Assujeti> assujetis;

        if (idBien.equals(GeneralConst.EMPTY_STRING) || idBien.equals(GeneralConst.Number.ZERO)) {

            query = SQLQueryAssujettissement.SELECT_EXISTED_ASSUJETTI_SANS_BIEN;
            assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false,
                    codeAB,
                    codePersonne, tarif);

        } else {

            query = SQLQueryAssujettissement.SELECT_EXISTED_ASSUJETTI;
            assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false,
                    idBien,
                    codeAB,
                    codePersonne);
        }

        try {

            return assujetis.isEmpty() ? null : assujetis.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveAssujettissement(Assujeti assujeti, List<PeriodeDeclaration> periodeDeclarations, JSONArray selectedAB, LogUser logUser) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String query = SQLQueryAssujettissement.F_NEW_ASSUJETISSEMENT;
            String returnValue = AssujettissementConst.ParamQUERY.ASSUJETTISSEMENT_ID;

            String codeAdressePersonne;
            if (assujeti.getFkAdressePersonne().getCode().equals(GeneralConst.EMPTY_STRING)) {
                AdressePersonne adressePersonne = IdentificationBusiness.getDefaultAdressePersonneByPersonne(assujeti.getPersonne().getCode());
                if (adressePersonne == null) {
                    codeAdressePersonne = null;
                } else {
                    codeAdressePersonne = adressePersonne.getCode();
                }
            } else {
                codeAdressePersonne = assujeti.getFkAdressePersonne().getCode();
            }

            String idBien = assujeti.getBien().getId().equals(GeneralConst.Number.ZERO)
                    ? null : assujeti.getBien().getId();

            String macAdress = logUser.getMacAddress().trim();
            String getIpAddress = logUser.getIpAddress().trim();
            String getHostName = logUser.getHostName().trim();
            String getEvent = logUser.getEvent().trim();
            String getBrowserContext = logUser.getBrowserContext().trim();

            params.put(AssujettissementConst.ParamQUERY.BIEN, idBien);
            params.put(AssujettissementConst.ParamQUERY.ARTICLE_BUDGETAIRE, assujeti.getArticleBudgetaire().getCode());
            params.put(AssujettissementConst.ParamQUERY.DUREE, 1);
            params.put(AssujettissementConst.ParamQUERY.RENOUVELLEMENT, assujeti.getRenouvellement());
            params.put(AssujettissementConst.ParamQUERY.DATE_DEBUT, assujeti.getDateDebut());
            params.put(AssujettissementConst.ParamQUERY.DATE_FIN, assujeti.getDateFin());
            params.put(AssujettissementConst.ParamQUERY.VALEUR, assujeti.getValeur());
            params.put(AssujettissementConst.ParamQUERY.PERSONNE, assujeti.getPersonne().getCode());
            params.put(AssujettissementConst.ParamQUERY.ADRESSE, codeAdressePersonne);
            params.put(AssujettissementConst.ParamQUERY.NBR_JOUR_LIMITE, assujeti.getNbrJourLimite());
            params.put(AssujettissementConst.ParamQUERY.PERIODICITE, assujeti.getPeriodicite());
            params.put(AssujettissementConst.ParamQUERY.TARIF, assujeti.getTarif().getCode());
            params.put(AssujettissementConst.ParamQUERY.GESTIONNAIRE, assujeti.getGestionnaire());
            params.put(AssujettissementConst.ParamQUERY.COMPLEMENT_BIEN, assujeti.getComplementBien());
            params.put(AssujettissementConst.ParamQUERY.AGENT_CREAT, "");
            params.put(AssujettissementConst.ParamQUERY.NOUVELLES_ECHEANCES, "");
            params.put(AssujettissementConst.ParamQUERY.UNITE_VALEUR, assujeti.getUniteValeur());
            params.put(GeneralConst.LogParam.MAC_ADRESSE, macAdress);
            params.put(GeneralConst.LogParam.IP_ADRESSE, getIpAddress);
            params.put(GeneralConst.LogParam.HOST_NAME, getHostName);
            params.put(GeneralConst.LogParam.FK_EVENEMENT, getEvent);
            params.put(GeneralConst.LogParam.CONTEXT_BROWSER, getBrowserContext);

            for (PeriodeDeclaration declaration : periodeDeclarations) {
                bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_PERIODE_DECLARATION, new Object[]{
                    declaration.getDebut(),
                    declaration.getFin(),
                    declaration.getDateLimite(),
                    declaration.getDateLimitePaiement(),
                    null
                });
                counter++;
            }

            for (int i = 0; i < selectedAB.length(); i++) {

                JSONObject jsonobject = selectedAB.getJSONObject(i);
                String codeAB = jsonobject.getString("id");

                counter++;
//                bulkQuery.put(counter + SQLQueryAssujettissement.F_NEW_ASSUJ_AB, new Object[]{
//                    codeAB,});

            }

            result = myDao.getDaoImpl().execBulkQuery(query, returnValue, params, bulkQuery);
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean saveNewPeriodeDeclaration(Assujeti assujeti, List<PeriodeDeclaration> periodeDeclarations) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            for (PeriodeDeclaration pd : periodeDeclarations) {

                bulkQuery.put(counter + SQLQueryAssujettissement.F_ADD_NEW_PERIODE_DECLARATION, new Object[]{
                    pd.getAssujetissement().getId(),
                    pd.getDebut(),
                    pd.getFin(),
                    pd.getDateLimitePaiement(),
                    null,
                    null
                });
                counter++;
            }

            counter++;
            bulkQuery.put(counter + ":UPDATE T_ASSUJETI SET DATE_DEBUT = ?1, DATE_FIN = ?2, GESTIONNAIRE = ?3 WHERE ID = ?4", new Object[]{
                assujeti.getDateDebut(),
                assujeti.getDateFin(),
                assujeti.getGestionnaire(),
                assujeti.getId()
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static PeriodeDeclaration getPeriodeDeclarationByCode(String codePeriode) {
        try {
            String query = SQLQueryAssujettissement.SELECT_PERIODE_DECLARATION_BY_CODE;
            List<PeriodeDeclaration> periodeDeclarations = (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, codePeriode);
            return periodeDeclarations.isEmpty() ? null : periodeDeclarations.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Acquisition getBienEnLocation(String codeBien, String codeProprietaire) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIEN_EN_LOCATION;
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codeBien, codeProprietaire);
            return acquisitions.isEmpty() ? null : acquisitions.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Assujeti> getAssujettissementByPersonne(String codePersonne, String codeService) {
        try {
            String query = SQLQueryAssujettissement.LIST_ASSUJETTISSEMENT_BY_PERSONNE;
            List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class),
                    false,
                    codePersonne, codeService);
            return assujetis;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Assujeti> getAssujettissementByPersonneV2(String codePersonne) {
        try {
            String query = SQLQueryAssujettissement.LIST_ASSUJETTISSEMENT_BY_PERSONNE_V3;
            List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false, codePersonne);
            return assujetis;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ArticleBudgetaire> getArticlesBudgetairesAssujettissableBySite(String codeSite,
            String codeService, String codeEntite) {
        try {

            String sqlQuery;

            sqlQuery = SQLQueryAssujettissement.LIST_ARTICLE_BUDGETAIRE_ASSUJETISSABLE_BY_SITE;

            List<ArticleBudgetaire> articleBudgetaires = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class),
                    false,
                    codeSite.trim(), codeService.trim(), codeEntite.trim());

            return articleBudgetaires;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ArticleBudgetaire> getArticlesBudgetairesAssujettissable(String libelle,
            String codeService, String entite) {

        try {

            String sqlQuery;

            sqlQuery = SQLQueryAssujettissement.LIST_ARTICLE_BUDGETAIRE_ASSUJETISSABLE_BY_ENTITE_SERVICE;
            sqlQuery = String.format(sqlQuery, codeService.trim(), entite.trim());

            List<ArticleBudgetaire> articleBudgetaires = (List<ArticleBudgetaire>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class),
                    true,
                    libelle);
            return articleBudgetaires;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Acquisition> getBiensMobiliersOfPersonne(String codePersonne, String typeBienMobiliers) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIENS_MOBILIERS_OF_PERSONNE;
            query = String.format(query, typeBienMobiliers, codePersonne, codePersonne);
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Acquisition> getBiensMobiliersOfPersonneV2(String codePersonne) {
        try {
            String query = SQLQueryAssujettissement.SELECT_BIENS_MOBILIERS_OF_PERSONNE_FOR_PEAGE;
            query = String.format(query, codePersonne, codePersonne);
            List<Acquisition> acquisitions = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, codePersonne);
            return acquisitions;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<ArticleBudgetaire> getAllTaxesPeages(String taxesPeages) {
        try {
            String query = String.format(SQLQueryAssujettissement.SELECT_TAXES_PEAGES, taxesPeages);
            return (List<ArticleBudgetaire>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ArticleBudgetaire.class), false);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailAssujettissement getDetailAssujettissementByBien(String idBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_DETAIL_ASSUJETTISSEMENT_BIEN_BIEN;

            List<DetailAssujettissement> detailAssujettissementList = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, idBien);
            return detailAssujettissementList.isEmpty() ? null : detailAssujettissementList.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailAssujettissement getDetailAssujettissementByID(int id) {
        try {
            String query = "SELECT * FROM T_DETAIL_ASSUJETTISSEMENT WITH (READPAST) WHERE ID = ?1";

            List<DetailAssujettissement> detailAssujettissementList = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, id);
            return detailAssujettissementList.isEmpty() ? null : detailAssujettissementList.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailAssujettissement getDetailAssujettissementByAssujettiAndBien(String assujettiCode, String bienCode) {
        try {
            String query = SQLQueryAssujettissement.SELECT_DETAIL_ASSUJETTISSEMENT_BIEN_BY_ASSUJETTI_AND_BIEN;

            List<DetailAssujettissement> detailAssujettissementList = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, assujettiCode.trim(), bienCode.trim());
            return detailAssujettissementList.isEmpty() ? null : detailAssujettissementList.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean saveAssujettissementPeage(Assujettissement assuj) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object> params = new HashMap<>();
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            String returnValue = "ASSUJETTISSEMENT_ID";
            String query = "P_SAVE_ASSUJETTISSEMENT_PEAGE";
            params.put("PERSONNE", assuj.getPersonne());
            params.put("ARTICLE_BUDGETAIRE", assuj.getArticleBudgetaire());
            params.put("ETAT", assuj.getEtat());

            List<DetailAssujettissement> detailAssujettissementList = assuj.getDetailAssujettissementList();

            for (DetailAssujettissement da : detailAssujettissementList) {
                double taux = da.getTaux().doubleValue();
                bulkQuery.put(counter + ":EXEC P_SAVE_DETAIL_ASSUJETTISSEMENT_PEAGE '%s',?1,?2,?3,?4,?5", new Object[]{
                    da.getBien() == null ? null : da.getBien().getId(),
                    da.getTarif() == null ? null : da.getTarif().getCode(),
                    taux,
                    da.getDevise() == null ? null : da.getDevise().getCode(),
                    da.getEtat()
                });
                counter++;
            }

            result = myDao.getDaoImpl().execBulkQuery(query, returnValue, params, bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Bien getBienByImmatriculation(String imm) {
        try {

            String tcImm = propertiesConfig.getProperty("TYPE_COMPLEMENT_IMMATRICULATION");
            String tcChassis = propertiesConfig.getProperty("TYPE_COMPLEMENT_CHASSIS");

            String query = SQLQueryAssujettissement.SELECT_BIEN_BY_IMMATRICULATION;

            List<Bien> typeBiens = (List<Bien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bien.class), false, imm.trim(), tcImm.trim(), tcChassis.trim());
            return typeBiens.isEmpty() ? null : typeBiens.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static Bien getBienByCode(String code) {
        try {

            String query = "SELECT * FROM T_BIEN WITH (READPAST) WHERE ID = ?1";

            List<Bien> typeBiens = (List<Bien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Bien.class), false, code.trim());
            return typeBiens.isEmpty() ? null : typeBiens.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailAssujettissement getDetailAssujettissementByIdBien(String idBien) {
        try {

            String query = SQLQueryAssujettissement.SELECT_DETAIL_ASSUJETTISSEMENT_BY_BIEN;

            List<DetailAssujettissement> daList = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, idBien);
            return daList.isEmpty() ? null : daList.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static AcquitLiberatoire checkCPIFiche(String personne, String article, String cpi) {

        String primaryQuery = "SELECT * from T_ACQUIT_LIBERATOIRE AQ WITH (READPAST) WHERE CODE NOT IN (SELECT CPI_FICHE FROM T_NOTE_CALCUL NC WHERE NC.CPI_FICHE = AQ.CODE AND ETAT = 1) AND CODE = ?1 AND "
                + "NOTE_PERCEPTION IN (SELECT NUMERO from T_NOTE_PERCEPTION NP where NUMERO = AQ.NOTE_PERCEPTION AND NP.NOTE_CALCUL IN (SELECT NUMERO FROM T_NOTE_CALCUL NC WHERE NC.NUMERO = NP.NOTE_CALCUL AND NC.PERSONNE = ?2 AND NC.NUMERO IN (SELECT DNC.NOTE_CALCUL FROM T_DETAILS_NC DNC WHERE DNC.NOTE_CALCUL = NC.NUMERO AND DNC.ARTICLE_BUDGETAIRE = ?3 )))";

        List<AcquitLiberatoire> listAcquitLiberatoires;

        try {

            listAcquitLiberatoires = (List<AcquitLiberatoire>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(AcquitLiberatoire.class), false, cpi, personne, article);

        } catch (Exception e) {
            throw e;
        }

        return listAcquitLiberatoires.isEmpty() ? null : listAcquitLiberatoires.get(0);
    }

    public static Carte getCarteImpression(String reference) {
        try {

            String query = "select * from T_CARTE with(readpast) where ETAT = 1 AND ID =?1";

            List<Carte> cartes = (List<Carte>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Carte.class), false, reference);
            return cartes.isEmpty() ? null : cartes.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Carte> getCarte(String libelle, String typeSearch) {
        try {

            String secondPartSQL = GeneralConst.EMPTY_STRING;

            switch (typeSearch) {

                case GeneralConst.Number.ZERO:
                    secondPartSQL = "AND NUMERO_CPI = ?1";
                    break;
                case GeneralConst.Number.ONE:
                    secondPartSQL = "AND FK_CONDUCTEUR_VEHICULE in (select cv.ID from T_CONDUCTEUR_VEHICULE cv where cv.CONDUCTEUR =?1) ";
                    break;
                case GeneralConst.Number.TWO:
                    secondPartSQL = "AND FK_CONDUCTEUR_VEHICULE in (select cv.ID from T_CONDUCTEUR_VEHICULE cv where cv.PROPRIETAIRE =?1)";
                    break;
            }

            String query = "select * from T_CARTE with (readpast) where ETAT IN (1,2) %s";
            query = String.format(query, secondPartSQL);
            List<Carte> cartes = (List<Carte>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Carte.class), false, libelle);
            return cartes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean checkBien(String idBien) {
        try {
            boolean response = false;
            String query = "select * from T_CONDUCTEUR_VEHICULE with(readpast) where BIEN =?1";
            List<ConducteurVehicule> conducteurVehicules = (List<ConducteurVehicule>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ConducteurVehicule.class), false, idBien);
            if (conducteurVehicules.isEmpty()) {
                response = false;
            } else {
                response = true;
            }

            return response;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean updateBien(String reference) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            bulkQuery.put(counter + ":UPDATE T_CONDUCTEUR_VEHICULE SET ETAT = 1 WHERE ID = ?1", new Object[]{
                reference
            });

            result = executeQueryBulkInsert(bulkQuery);
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Acquisition getAcquisitionByIdBien(String idBien) {
        try {

            String query = SQLQueryAssujettissement.SELECT_ACQUISITION_BY_BIEN;

            List<Acquisition> aList = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, idBien);
            return aList.isEmpty() ? null : aList.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static PersonneExemptee getPersonneExempteeByCode(String codePersonne) {
        try {

            String query = SQLQueryAssujettissement.SELECT_PERSONNE_EXEMPTEE;

            List<PersonneExemptee> aList = (List<PersonneExemptee>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PersonneExemptee.class), false, codePersonne);
            return aList.isEmpty() ? null : aList.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static PrevisionCredit getPrevisionCreditByAssujettissement(String idAssuj) {
        try {

            String query = SQLQueryAssujettissement.SELECT_PREVISION_CREDIT_BY_ASSUJETTISSEMENT;

            List<PrevisionCredit> pcList = (List<PrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PrevisionCredit.class), false, idAssuj);
            return pcList.isEmpty() ? null : pcList.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailPrevisionCredit getDetailPrevisionCreditByDetailAssujettissement(int idDetailAssuj) {
        try {

            String query = SQLQueryAssujettissement.SELECT_DETAIL_PREVISION_CREDIT_BY_DETAIL_ASSUJETTISSEMENT;

            List<DetailPrevisionCredit> dpcList = (List<DetailPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailPrevisionCredit.class), false, idDetailAssuj);
            return dpcList.isEmpty() ? null : dpcList.get(0);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean saveTicketsPeage(String ticketsData) throws Exception {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            JSONArray dataArray = new JSONArray(ticketsData);

            for (int i = 0; i < dataArray.length(); i++) {

                JSONObject data = dataArray.getJSONObject(i);

                bulkQuery.put(counter + ":EXEC P_SAVE_TICKET_PEAGE_V2 ?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11,?12,?13,?14", new Object[]{
                    data.getString("code"),
                    data.getString("bien").equals("") ? null : data.getString("bien"),
                    data.getString("personne").equals("") ? null : data.getString("personne"),
                    data.getString("articleBudgetaire"),
                    data.getDouble("montant"),
                    data.getString("devise"),
                    data.getString("typePaiement"),
                    data.getString("motifAnnulation"),
                    data.getString("dateProd"),
                    //data.getString("dateSync"),
                    data.getInt("agentCreat"),
                    data.getInt("etat"),
                    //data.getString("provisionCredit"),
                    data.getString("detailProvisionCreditId").equals("0") ? null : data.getString("detailProvisionCreditId"),
                    data.getString("tarifCode"),
                    data.getString("immatriculation")
                });
                counter++;
            }

            result = myDao.getDaoImpl().executeBulkQuery(bulkQuery);
        } catch (JSONException e) {
            throw (e);
        }

        return result;
    }

    public static List<Complement> getComplements(String nif) {
        try {
            String query = SQLQueryAssujettissement.SELECT_COMPLEMENT_PERSONNE;
            return (List<Complement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Complement.class), false, nif);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static ValeurPredefinie getValeurComplement(String code) {
        try {
            String query = SQLQueryAssujettissement.SELECT_VALEUR_PREDEFINIE;
            List<ValeurPredefinie> vps = (List<ValeurPredefinie>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ValeurPredefinie.class), false, code);
            return vps.isEmpty() ? null : vps.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean disociateMotoWithConducteur(String idContrat, String userID) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            bulkQuery.put(counter + ":UPDATE T_CONDUCTEUR_VEHICULE SET ETAT = 0,DATE_MAJ = GETDATE(),AGENT_MAJ = ?1 WHERE ID = ?2", new Object[]{
                userID, idContrat
            });

            result = executeQueryBulkInsert(bulkQuery);
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static ConducteurVehicule getMotoSousContraitById(String id) {
        try {
            String query = SQLQueryAssujettissement.SELECT_MOTO_SOUS_CONTRAT_BY_ID;
            List<ConducteurVehicule> conducteurVehicules = (List<ConducteurVehicule>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ConducteurVehicule.class), false, id);
            return conducteurVehicules.isEmpty() ? null : conducteurVehicules.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TypeBien> getTypeBienByService(String codeService) {
        try {
            String query = SQLQueryAssujettissement.SELECT_TYPES_BIEN_BY_SERVICE;
            List<TypeBien> typeBiens = (List<TypeBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBien.class), false, codeService.trim());
            return typeBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TypeBien> getTypeBienByType(int type) {
        try {
            String query = SQLQueryAssujettissement.SELECT_TYPES_BIEN_BY_TYPE;
            List<TypeBien> typeBiens = (List<TypeBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBien.class), false, type);
            return typeBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListTarifVignetteOrConcessionMine(String codeTarif) {
        try {
            String query = "SELECT * FROM T_TARIF WITH (READPAST) WHERE ETAT = 1 AND CODE %s AND EST_TARIF_PEAGE = 0 ORDER BY INTITULE";

            query = String.format(query, codeTarif);

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
            return tarifs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListTarifPublicite(String codeAB) {
        try {

            String query = "SELECT t.CODE,t.INTITULE FROM T_TARIF t WITH (READPAST) WHERE t.ETAT = 1 \n"
                    + "AND t.CODE IN (select p.TARIF from T_PALIER p WITH (READPAST) \n"
                    + "	where ARTICLE_BUDGETAIRE %s and p.ETAT = 1) order by t.INTITULE";

            query = String.format(query, codeAB);

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
            return tarifs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<UsageBien> getListUsageBiens() {
        try {
            String query = "SELECT * FROM T_USAGE_BIEN WITH (READPAST) WHERE ETAT = 1 ORDER BY INTITULE";
            List<UsageBien> usageBiens = (List<UsageBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(UsageBien.class), false);
            return usageBiens;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListCategorieImmobilier(String codeTarif) {
        try {
            String query = "SELECT * FROM T_TARIF WITH (READPAST) WHERE ETAT = 1 AND CODE %s ORDER BY INTITULE";

            query = String.format(query, codeTarif);
            List<Tarif> communes = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
            return communes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<EntiteAdministrative> getListCommuneHautKatanga(String districtList) {
        try {
            String query = "SELECT * FROM T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE TYPE_ENTITE = 3 AND ETAT = 1"
                    + " AND ENTITE_MERE %s ORDER by INTITULE";

            query = String.format(query, districtList);
            List<EntiteAdministrative> communes = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false);
            return communes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<EntiteAdministrative> getListQuartierByCommune(String commune) {
        try {
            String query = "select * from T_ENTITE_ADMINISTRATIVE WITH (READPAST) WHERE ENTITE_MERE = ?1 AND TYPE_ENTITE = 2 AND ETAT = 1 ORDER BY INTITULE";

            List<EntiteAdministrative> quartiers = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, commune);
            return quartiers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListCategorieImmobilierByQuartier(String codeQuartier) {
        try {
            String query = "SELECT t.* FROM T_TARIF t WITH (READPAST) WHERE t.CODE IN (SELECT p.TARIF FROM T_PALIER p WITH (READPAST)"
                    + " WHERE p.FK_ENTITE_ADMINISTRATIVE = ?1 AND p.ETAT = 1) ORDER BY t.INTITULE";

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false, codeQuartier);
            return tarifs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getListCategorieByNatureImpot(String codeAB_IF, String codeNature) {
        try {
            String query = "SELECT t.* FROM T_TARIF t WITH (READPAST) WHERE t.CODE IN (SELECT p.TARIF FROM T_PALIER p WITH (READPAST)\n"
                    + "WHERE p.ARTICLE_BUDGETAIRE = ?1 AND p.FK_TYPE_BIEN = ?2 AND p.ETAT = 1) AND t.EST_TARIF_PEAGE = 0 AND t.ETAT = 1 AND t.CODE <> '0000001820'\n"
                    + "ORDER BY t.INTITULE";

            List<Tarif> tarifs = (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false, codeAB_IF, codeNature);
            return tarifs;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Agent> getListAgentByBureau(String codeBureau) {
        try {
            String query = "SELECT a.* FROM T_AGENT a WITH (READPAST) WHERE a.SITE = ?1 AND a.ETAT = 1 AND a.CODE %s ORDER BY a.NOM";
            query = String.format(query, propertiesConfig.getProperty("CODE_USER_SUPPORT"));

            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codeBureau);
            return agents;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean saveTypeBien(TypeBien typeBien, String codeServise) {

        String query = SQLQueryAssujettissement.F_NEW_TYPE_BIEN;

        boolean result = myDao.getDaoImpl().executeStoredProcedure(
                query,
                typeBien.getIntitule(),
                typeBien.getContractuel(),
                codeServise,
                typeBien.getCode());
        return result;
    }

    public static boolean updateTypeBien(TypeBien typeBien) {

        boolean result = false;
        try {
            String query = SQLQueryAssujettissement.UPDATE_TYPE_BIEN;

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    typeBien.getIntitule(),
                    typeBien.getContractuel(),
                    typeBien.getCode());

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean sageUsageBien(UsageBien usageBien) {

        boolean result = false;
        try {

            String query = GeneralConst.EMPTY_STRING;

            if (usageBien.getId() > 0) {

                query = SQLQueryAssujettissement.UPDATE_USAGE_BIEN;
                result = myDao.getDaoImpl().executeStoredProcedure(query,
                        usageBien.getIntitule(),
                        usageBien.getId());
            } else {
                query = SQLQueryAssujettissement.INSERT_USAGE_BIEN;
                result = myDao.getDaoImpl().executeStoredProcedure(query,
                        usageBien.getIntitule());
            }

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean disableUsageBien(int id) {

        boolean result = false;
        try {

            String query = SQLQueryAssujettissement.DELETE_USAGE_BIEN;
            result = myDao.getDaoImpl().executeStoredProcedure(query, id);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean canceledPenalite(int id) {

        boolean result = false;
        try {

            String query = "UPDATE T_RETRAIT_DECLARATION SET ETAT = 0 WHERE ID = ?1";
            result = myDao.getDaoImpl().executeStoredProcedure(query, id);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean disableTypeBien(TypeBien typeBien) {

        boolean result = false;
        try {
            String query = SQLQueryAssujettissement.DISABLE_TYPE_BIEN;

            result = myDao.getDaoImpl().executeStoredProcedure(query,
                    typeBien.getCode());

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TarifSite> getAllTaxesPeagesBySiteUser(String siteCode) {
        try {
            String query = "select * from T_TARIF_SITE ts WITH (READPAST) JOIN T_TARIF t WITH (READPAST) ON ts.FK_TARIF = t.CODE WHERE ts.FK_SITE_PROVENANCE = ?1 "
                    + " OR ts.FK_SITE_DESTINANTION = ?1 AND ts.ETAT = 1 ORDER BY t.INTITULE";
            return (List<TarifSite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TarifSite.class), false, siteCode.trim());
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<TarifSite> getAllTaxesPeagesBySiteUserV2(String siteCode, String sitePeage) {
        try {
            String query = "select * from T_TARIF_SITE ts WITH (READPAST) JOIN T_TARIF t WITH (READPAST) ON ts.FK_TARIF = t.CODE WHERE ts.FK_SITE_PROVENANCE = ?1 "
                    + " AND ts.FK_SITE_DESTINANTION = ?2 AND ts.ETAT = 1 ORDER BY t.INTITULE";
            return (List<TarifSite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TarifSite.class), false, siteCode.trim(), sitePeage.trim());
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Tarif> getAllTaxesPeages() {
        try {
            String query = "SELECT t. * FROM T_TARIF t WITH (READPAST) WHERE t.ETAT = 1 AND t.EST_TARIF_PEAGE = 1 ORDER BY t.INTITULE";
            return (List<Tarif>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Tarif.class), false);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Agent> getAllPercepteurBySite(String codeSite, String codeFonctionPercepeur) {
        try {

            String query = GeneralConst.EMPTY_STRING;
            String sqlParam = GeneralConst.EMPTY_STRING;

            switch (codeSite) {
                case "*":

                    query = "SELECT a. * FROM T_AGENT a WITH (READPAST) WHERE a.ETAT = 1 %s ORDER BY a.NOM";
                    query = String.format(query, codeFonctionPercepeur);
                    break;
                default:
                    sqlParam = " AND a.SITE = '%s'";
                    sqlParam = String.format(sqlParam, codeSite);
                    query = "SELECT a. * FROM T_AGENT a WITH (READPAST) WHERE a.ETAT = 1 %s %s ORDER BY a.NOM";
                    query = String.format(query, sqlParam, codeFonctionPercepeur);
                    break;
            }

            //query = String.format(query, sqlParam);
            return (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false);

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Agent> getAllControleurBySite(String codeSite, String codeFonctionControleur) {
        try {

            String query = GeneralConst.EMPTY_STRING;
            String sqlParam = GeneralConst.EMPTY_STRING;

            switch (codeSite) {
                case "*":
                    query = "SELECT a. * FROM T_AGENT a WITH (READPAST) WHERE a.ETAT = 1 AND a.FONCTION = ?1 %s ORDER BY a.NOM";
                    break;
                default:
                    sqlParam = " AND a.SITE = '%s'";
                    sqlParam = String.format(sqlParam, codeSite);
                    query = "SELECT a. * FROM T_AGENT a WITH (READPAST) WHERE a.ETAT = 1 AND a.FONCTION = ?1 %s ORDER BY a.NOM";
                    break;
            }

            query = String.format(query, sqlParam);

            return (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codeFonctionControleur);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<DetailsPrevisionCredit> getDetailsPrevisionCreditActifByBien(String bien) {
        try {
            String query = "select * from T_DETAILS_PREVISION_CREDIT WITH (READPAST) WHERE FK_BIEN = ?1 AND ETAT = 1 "
                    + " AND FK_PREVISION_CREDIT IN (SELECT CODE FROM T_PREVISION_CREDIT WITH (READPAST) WHERE ETAT = 1)";
            return (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, bien);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailsPrevisionCredit getDetailsPrevisionCreditByAssujettissementID(int id) {
        try {
            String query = "select * from T_DETAILS_PREVISION_CREDIT WITH (READPAST) WHERE FK_DETAIL_ASSUJETTISSEMENT = ?1 AND ETAT = 1";
            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, id);
            return detailsPrevisionCredits.isEmpty() ? null : detailsPrevisionCredits.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailsPrevisionCredit getDetailsPrevisionCreditByBien(String bienId) {
        try {
            String query = "select top 1 * from T_DETAILS_PREVISION_CREDIT WITH (READPAST) WHERE FK_BIEN = ?1 AND ETAT = 1";
            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, bienId);
            return detailsPrevisionCredits.isEmpty() ? null : detailsPrevisionCredits.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailsPrevisionCredit getDetailsPrevisionCreditByPersonneAndBienAndType(String personne, String bien, int type) {
        try {
            String query = "select * from T_DETAILS_PREVISION_CREDIT WITH (READPAST) WHERE FK_BIEN = ?1 AND ETAT = 1 "
                    + " AND TYPE = ?2 AND FK_PREVISION_CREDIT IN (SELECT CODE FROM T_PREVISION_CREDIT WITH (READPAST) "
                    + " WHERE FK_PERSONNE = ?3 AND ETAT = 1)";
            List<DetailsPrevisionCredit> detailsPrevisionCredits = (List<DetailsPrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailsPrevisionCredit.class), false, bien.trim(), type, personne.trim());
            return detailsPrevisionCredits.isEmpty() ? null : detailsPrevisionCredits.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Assujettissement getAssujettissementByID(int id) {
        try {
            String query = "select TOP 1 * from T_ASSUJETTISSEMENT WITH (READPAST) "
                    + " WHERE CODE IN (select ASSUJETTISSEMENT from T_DETAIL_ASSUJETTISSEMENT WITH (READPAST) WHERE ID = ?1)";
            List<Assujettissement> assujettissements = (List<Assujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujettissement.class), false, id);
            return assujettissements.isEmpty() ? null : assujettissements.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Assujeti getAssujettissementDrhkatByCode(String id) {
        try {
            String query = "select * from T_ASSUJETI WITH (READPAST) WHERE ID = ?1";
            List<Assujeti> assujetis = (List<Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Assujeti.class), false, id);
            return assujetis.isEmpty() ? null : assujetis.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static cd.hologram.erecettesvg.models.Assujeti getAssujettiNormalById(String id) {
        try {
            String query = "select * from T_ASSUJETI WITH (READPAST) WHERE ID = ?1";
            List<cd.hologram.erecettesvg.models.Assujeti> assujetis = (List<cd.hologram.erecettesvg.models.Assujeti>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(cd.hologram.erecettesvg.models.Assujeti.class), false, id);
            return assujetis.isEmpty() ? null : assujetis.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static DetailAssujettissement getDetailAssujettissementByPersonneAndBien(String personne, String bien) {
        try {
            String query = "select * from T_DETAIL_ASSUJETTISSEMENT WITH (READPAST) WHERE FK_PERSONNE = ?1 AND BIEN = ?2 AND ETAT = 1";
            List<DetailAssujettissement> detailAssujettissements = (List<DetailAssujettissement>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(DetailAssujettissement.class), false, personne, bien);
            return detailAssujettissements.isEmpty() ? null : detailAssujettissements.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static PrevisionCredit getAPrevisionCreditByID(String code) {
        try {
            String query = "select * from T_PREVISION_CREDIT where CODE = ?1 AND ETAT = 1";
            List<PrevisionCredit> previsionCredits = (List<PrevisionCredit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PrevisionCredit.class), false, code);
            return previsionCredits.isEmpty() ? null : previsionCredits.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getPersonneByProvisionCredit(String codeProvisionCredit) {
        try {
            String query = "select * from T_PERSONNE WITH (READPAST) WHERE CODE IN (select FK_PERSONNE from T_PREVISION_CREDIT WITH (READPAST) "
                    + " WHERE CODE = ?1 AND ETAT = 1) AND ETAT = 1 ORDER BY NOM";
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, codeProvisionCredit);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Palier> getPaliersPeages(String taxesPeages) {
        try {
            String query = String.format(SQLQueryAssujettissement.SELECT_PALIERS_PEAGES, taxesPeages);
            return (List<Palier>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Palier.class), false);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static UsageBien getUsageBienByCode(int id) {
        try {
            String query = "SELECT * FROM T_USAGE_BIEN WITH (READPAST) WHERE ID = ?1";
            List<UsageBien> usageBiens = (List<UsageBien>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(UsageBien.class), false, id);
            return usageBiens.isEmpty() ? null : usageBiens.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static boolean createPermutation(Acquisition acquisition, String idAcquisition) {

        int counter = 0;
        Boolean result = false;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();

        try {

            bulkQuery.put(counter + SQLQueryAssujettissement.INSERT_ACQUISITION, new Object[]{
                acquisition.getPersonne().getCode(),
                acquisition.getBien().getId(),
                acquisition.getDateAcquisition(),
                acquisition.getProprietaire(),
                acquisition.getReferenceContrat(),
                acquisition.getNumActeNotarie(),
                acquisition.getDateActeNotarie()

            });
            counter++;

            bulkQuery.put(counter + SQLQueryAssujettissement.UPDATE_ACQUISITION, new Object[]{
                idAcquisition
            });
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Acquisition getAcquisistionByProprietaire(String proprietaire, String codeBien) {
        try {
            String query = SQLQueryAssujettissement.SELECT_ACQUISITION_BY_PROPRIETAIRE;
            List<Acquisition> acquisition = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, proprietaire, codeBien);
            return acquisition.isEmpty() ? null : acquisition.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Acquisition getAcquisistionByCode(String code) {
        try {
            String query = "SELECT * FROM T_ACQUISITION WITH (READPAST) WHERE ID = ?1";
            List<Acquisition> acquisition = (List<Acquisition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Acquisition.class), false, code);
            return acquisition.isEmpty() ? null : acquisition.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static TypeBienService getTypeBienServiceByTypeBien(String codeypeBien) {
        try {
            String query = "SELECT * FROM T_TYPE_BIEN_SERVICE WITH (READPAST) WHERE TYPE_BIEN = ?1 AND ETAT = 1";
            List<TypeBienService> typeBienServices = (List<TypeBienService>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(TypeBienService.class), false, codeypeBien);
            return typeBienServices.isEmpty() ? null : typeBienServices.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static ValeurPredefinie getValeurPredefinieByCode(String codeVP, String typeComplement) {
        try {
            String query = "select * from T_VALEUR_PREDEFINIE WITH (READPAST) WHERE CODE = ?1 AND FK_TYPE_COMPLEMENT = ?2 AND ETAT = 1";
            List<ValeurPredefinie> valeurPredefinies = (List<ValeurPredefinie>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(ValeurPredefinie.class), false, codeVP, typeComplement);
            return valeurPredefinies.isEmpty() ? null : valeurPredefinies.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static IcmParam getIcmParamByTarifAndAnnee(String tarifCode, int annee) {

        try {

            String query = "SELECT * FROM T_ICM_PARAM WITH (READPAST) WHERE TARIF = ?1 AND ANNEE = ?2 AND ETAT = 1";

            List<IcmParam> icmParams = (List<IcmParam>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(IcmParam.class), false, tarifCode, annee);

            return icmParams.isEmpty() ? null : icmParams.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static IcmParam getIcmParamByID(int id) {

        try {

            String query = "SELECT * FROM T_ICM_PARAM WITH (READPAST) WHERE ID = ?1 AND ETAT = 1";

            List<IcmParam> icmParams = (List<IcmParam>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(IcmParam.class), false, id);

            return icmParams.isEmpty() ? null : icmParams.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Periodicite getPeriodiciteByCode(String code) {

        try {

            String query = "SELECT * FROM T_PERIODICITE WITH (READPAST) WHERE CODE = ?1 AND ETAT = 1";

            List<Periodicite> periodicites = (List<Periodicite>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Periodicite.class), false, code);

            return periodicites.isEmpty() ? null : periodicites.get(0);
        } catch (Exception e) {
            throw e;
        }

    }

    public static Boolean saveRedressement(String idAssujettissement, BigDecimal newBase, String uniteCode, String observation) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;

        try {

            bulkQuery.put(counter + ":UPDATE T_ASSUJETI SET VALEUR = ?1, UNITE_VALEUR = ?2 WHERE ID = ?3", new Object[]{
                newBase, uniteCode, idAssujettissement});

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<PeriodeDeclaration> getListPeriodeDeclarationByAssuj(String code) {
        try {
            String query = "SELECT * FROM T_PERIODE_DECLARATION PD WITH (READPAST) WHERE PD.ASSUJETISSEMENT = ?1 "
                    + " AND PD.ETAT = 1 AND PD.ID NOT IN (SELECT ISNULL(FK_PERIODE,0) FROM T_RETRAIT_DECLARATION WITH (READPAST) WHERE ETAT > 0) order by ID";
            return (List<PeriodeDeclaration>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(PeriodeDeclaration.class), false, code);
        } catch (Exception e) {
            throw (e);
        }
    }
}
