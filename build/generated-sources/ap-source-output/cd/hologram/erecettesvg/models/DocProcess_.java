package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DocProcess.class)
public class DocProcess_ { 

    public static volatile SingularAttribute<DocProcess, String> agentMaj;
    public static volatile SingularAttribute<DocProcess, String> process;
    public static volatile SingularAttribute<DocProcess, Date> dateCreat;
    public static volatile SingularAttribute<DocProcess, String> ordre;
    public static volatile SingularAttribute<DocProcess, Short> archiveObligatoire;
    public static volatile SingularAttribute<DocProcess, String> observation;
    public static volatile SingularAttribute<DocProcess, String> document;
    public static volatile SingularAttribute<DocProcess, String> dossier;
    public static volatile SingularAttribute<DocProcess, Short> obligatoire;
    public static volatile SingularAttribute<DocProcess, Short> archive;
    public static volatile SingularAttribute<DocProcess, Short> etat;
    public static volatile SingularAttribute<DocProcess, Date> dateMaj;
    public static volatile SingularAttribute<DocProcess, String> reference;
    public static volatile SingularAttribute<DocProcess, String> workFlow;
    public static volatile SingularAttribute<DocProcess, Integer> initiateur;
    public static volatile SingularAttribute<DocProcess, Integer> id;
    public static volatile SingularAttribute<DocProcess, String> agentCreat;

}