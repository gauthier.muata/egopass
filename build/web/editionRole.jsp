<%-- 
    Document   : editionRole
    Created on : 18 nov. 2020, 09:12:18
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Edition de rôle</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
        <link href="assets/lib/choosen/chosen.css" rel="stylesheet" type="text/css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">

                            <!--Filtrer par-->
                            <select class="form-control" id="cbxResearchType">
                                <option value="1" selected>Assujetti</option>
                                <!--option  value="2">Note de perception</option-->
                                <option value="3">Article budgetaire</option>
                            </select>

                            <div class="input-group" >
                                <input type="text" class="form-control" style="width: 600px" readonly="true"
                                       id="inputDefaillantSelected" placeholder="Assujetti">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" type="submit" id="btnCallModalSearchResponsable"><i class="fa fa-search"></i> Rechercher</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <button id="btnRechercheDefaillant" style="margin-right: 20px" class="btn btn-warning pull-right">
                        <i class="fa fa-filter"></i> &nbsp; Rechercher avanc&eacute;e des défaillants
                    </button>

                </div>

                <hr/>

                <div class="row" id="isAdvance" style="display: none">
                    <div  class="col-lg-12">
                        <label style="color: #ff0100; font-style: italic">R&eacute;sultats pour:</label>
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <br/>
                                <div class="row" style="margin-left: 5px;">
                                    <div class="journal" style="float: right">
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblMinistere" >&nbsp;</b>
                                        </span>

                                        <span id="labelProvince" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Province : &nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblProvince" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span id="labelEntite" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Service :&nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblEntite" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span id="lbl3" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Service d'assiete :&nbsp;&nbsp;&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblService" >&nbsp;&nbsp;&nbsp;</b>
                                        </span>

                                        <span id="lbl1" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Centre : 
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblSite" >&nbsp;</b>
                                        </span>

                                        <span id="lbl6" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Ann&eacute;e :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblAnnee" >&nbsp;</b>
                                        </span>

                                        <span id="lbl4" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Mois d&eacute;but :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblMoisDebut" >&nbsp;</b>
                                        </span>

                                        <span id="lbl5" style="font-size: 16px; color: black">
                                            &nbsp;&nbsp;&nbsp;&nbsp;Mois fin :&nbsp;
                                        </span>
                                        <span style="font-size: 16px; color: black">
                                            <b style="font-style: italic" id="lblMoisFin" >&nbsp;</b>
                                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>

                                <br/>
                            </div>
                        </div>
                    </div>
                </div>

                <button id="btnCreateRole" style="margin-bottom: 15px; display: none" class="btn btn-success pull-right">
                    <i class="fa fa-plus-circle"></i> &nbsp; Créer un projet de r&ocirc;le
                </button>



                <div class="journal" >
                    <table id="tableDefaillants" class="table table-bordered" > </table>
                    <table id="tableDefaillants2" class="table table-bordered" > </table>
                </div>

                <div class="modal fade" id="modalRechercheAvanceeDefaillantPaiement" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" style="width: 40%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Recherche des défaillants</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>


                                    <div class="form-group">
                                        <label>Province :</label>

                                        <select data-placeholder = "S&eacute;lectionner le province" class="chosen" id="selectProvince" multiple="true" style="width:100%; height: 80px;">

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Service :</label>

                                        <select data-placeholder = "S&eacute;lectionner l'entit&eacute;" class="chosen" id="selectEntite" multiple="true" style="width:100%; height: 80px;">

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Service d'assiete :</label>

                                        <select class="chosen" id="selectService" multiple="true" style="width:100%; height: 80px;">

                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Centre :</label>

                                        <select class="chosen" id="selectSite" multiple="true" style="width:100%;">

                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <label>Anne&eacute; :</label> &nbsp;
                                        <select class="form-control" id="selectAnnee" placeholder="Année"></select>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Mois debut :</label> &nbsp;
                                                <select class="form-control" id="selectMoisDebut" placeholder="Mois"></select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Mois fin :</label> &nbsp;
                                                <select class="form-control" id="selectMoisFin" placeholder="Mois"></select>
                                            </div>
                                        </div>
                                    </div>


                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="btnAdvencedSearch"><i class="fa fa-search"></i> &nbsp;Rechercher</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modalCreateProjectRole" tabindex="-1" role="dialog" 
                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" style="width: 40%">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Création d'un projet de rôle</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <label style="font-weight: normal">Article du rôle :</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="textArticleRole"
                                               placeholder="Saisissez l'article du rôle"/>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" id="btnValidateCreateRole">
                                    <i class="fa fa-check-circle"></i> &nbsp;Valider la création du projet de rôle
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/rechercheAssujetti.html" %>
        <%@include file="assets/include/modal/rechercheArticleBudgetaire.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/lib/js/dknotus-tour.min.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 

        <script src="assets/lib/choosen/chosen.jquery.js" type="text/javascript"></script>

        <script type="text/javascript" src="assets/js/rechercheAvancee_1.js"></script>
        <script type="text/javascript" src="assets/js/Recouvrement/editRole.js"></script>
        <script src="assets/js/modalAddPenalite.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/js/modalSearchAssujetti.js"></script>
        <!--script src="assets/js/modalSearchArticleBudgetaire.js" type="text/javascript"></script-->

    </body>
</html>
