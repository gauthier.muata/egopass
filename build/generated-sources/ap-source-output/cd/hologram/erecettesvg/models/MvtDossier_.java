package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(MvtDossier.class)
public class MvtDossier_ { 

    public static volatile SingularAttribute<MvtDossier, String> agentMaj;
    public static volatile SingularAttribute<MvtDossier, Short> valider;
    public static volatile SingularAttribute<MvtDossier, Date> dateCreat;
    public static volatile SingularAttribute<MvtDossier, String> observation;
    public static volatile SingularAttribute<MvtDossier, String> dossier;
    public static volatile SingularAttribute<MvtDossier, String> ua;
    public static volatile SingularAttribute<MvtDossier, Short> etat;
    public static volatile SingularAttribute<MvtDossier, Date> dateMaj;
    public static volatile SingularAttribute<MvtDossier, String> service;
    public static volatile SingularAttribute<MvtDossier, Short> typeMvt;
    public static volatile SingularAttribute<MvtDossier, Integer> id;
    public static volatile SingularAttribute<MvtDossier, String> agentCreat;
    public static volatile SingularAttribute<MvtDossier, Short> sensMvt;
    public static volatile SingularAttribute<MvtDossier, String> referenceInterne;

}