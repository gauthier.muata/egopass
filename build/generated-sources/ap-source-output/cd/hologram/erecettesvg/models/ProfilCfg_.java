package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ProfilEquipement;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ProfilCfg.class)
public class ProfilCfg_ { 

    public static volatile ListAttribute<ProfilCfg, ProfilEquipement> profilEquipementList;
    public static volatile SingularAttribute<ProfilCfg, Boolean> smsNotificationRequerant;
    public static volatile SingularAttribute<ProfilCfg, Boolean> metierAccuseReceptionNpAuto;
    public static volatile SingularAttribute<ProfilCfg, Boolean> metierDeclarationPeriodeUnique;
    public static volatile SingularAttribute<ProfilCfg, Integer> trackingDocPort;
    public static volatile SingularAttribute<ProfilCfg, Boolean> metierProcedureCourteActive;
    public static volatile SingularAttribute<ProfilCfg, String> printModeleNp;
    public static volatile SingularAttribute<ProfilCfg, String> enteteA;
    public static volatile SingularAttribute<ProfilCfg, String> enteteB;
    public static volatile SingularAttribute<ProfilCfg, Boolean> metierPrintDirectAl;
    public static volatile SingularAttribute<ProfilCfg, Boolean> etat;
    public static volatile SingularAttribute<ProfilCfg, Integer> algorithmeCalculPenalite;
    public static volatile SingularAttribute<ProfilCfg, String> metierBanque;
    public static volatile SingularAttribute<ProfilCfg, String> metierAbUnique;
    public static volatile SingularAttribute<ProfilCfg, Integer> id;
    public static volatile SingularAttribute<ProfilCfg, String> mailAdresseIp;
    public static volatile SingularAttribute<ProfilCfg, Integer> smsPort;
    public static volatile SingularAttribute<ProfilCfg, String> mailPassword;
    public static volatile SingularAttribute<ProfilCfg, Boolean> mailNotificationRequerant;
    public static volatile SingularAttribute<ProfilCfg, Boolean> mailNotificationUser;
    public static volatile SingularAttribute<ProfilCfg, String> dateCreat;
    public static volatile SingularAttribute<ProfilCfg, String> observation;
    public static volatile SingularAttribute<ProfilCfg, String> smsIp;
    public static volatile SingularAttribute<ProfilCfg, String> trackingDocIp;
    public static volatile SingularAttribute<ProfilCfg, String> mailTypeCompte;
    public static volatile SingularAttribute<ProfilCfg, String> metierModePaiement;
    public static volatile SingularAttribute<ProfilCfg, String> metierCompte;
    public static volatile SingularAttribute<ProfilCfg, String> intitule;
    public static volatile SingularAttribute<ProfilCfg, Boolean> inclurePenaliteRecouvrementDansDeclaration;
    public static volatile SingularAttribute<ProfilCfg, Boolean> metierNotPrintNp;
    public static volatile SingularAttribute<ProfilCfg, Boolean> metierPaiementAutoActif;
    public static volatile SingularAttribute<ProfilCfg, String> titreCpi;
    public static volatile SingularAttribute<ProfilCfg, String> printModeleAl;
    public static volatile SingularAttribute<ProfilCfg, String> metierExecuteApplication;
    public static volatile SingularAttribute<ProfilCfg, String> agentCreat;

}