/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tableResultDocumentOfficiel, modalDocumentOfficiel;

var inputValueResearchDocumentOfficiel;

var btnLauchSearchDocumentOfficiel, btnSelectDocumentOfficiel;

$(function () {
    
    
    tableResultDocumentOfficiel = $('#tableResultDocumentOfficiel');
    modalDocumentOfficiel = $('#modalDocumentOfficiel');
    
    inputValueResearchDocumentOfficiel = $('#inputValueResearchDocumentOfficiel');
    
    btnLauchSearchDocumentOfficiel = $('#btnLauchSearchDocumentOfficiel');
    btnSelectDocumentOfficiel = $('#btnSelectDocumentOfficiel');
    
    btnLauchSearchDocumentOfficiel.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        searchDocumentOfficiel();
    });
    
    inputValueResearchDocumentOfficiel.on('keypress', function (e) {
        if (e.keyCode == 13) {
            searchDocumentOfficiel();
        }

    });
    
    printDocumentOfficielTable('');
    
});


function searchDocumentOfficiel() {

    if (inputValueResearchDocumentOfficiel.val() === empty || inputValueResearchDocumentOfficiel.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchDocumentOfficiel.val(),
            'operation': 'getDocumentOfficiel'
        },
        beforeSend: function () {
            modalDocumentOfficiel.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalDocumentOfficiel.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectDocumentOfficiel.hide();
                    printDocumentOfficielTable('');
                    return;
                } else {
                    var dataDocumentOfficiel = JSON.parse(JSON.stringify(response));
                    if (dataDocumentOfficiel.length > 0) {
                        btnSelectDocumentOfficiel.show();
                        printDocumentOfficielTable(dataDocumentOfficiel);
                    } else {
                        btnSelectDocumentOfficiel.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalDocumentOfficiel.unblock();
            showResponseError();
        }

    });
}

function printDocumentOfficielTable(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:40%">DOCUMENT</th>';
    tableContent += '<th style="text-align:left;width:42%">REFERENCE</th>';
    tableContent += '<th style="text-align:left;width:18%">DATE PUBLICATION</th>';
    tableContent += '<th hidden="true" scope="col">Code officiel</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="width:40%;vertical-align:middle;text-align:left">' + data[i].intituleDocumentOfficiel.toUpperCase() + '</td>';
        tableContent += '<td style="width:42%;vertical-align:middle;text-align:left">' + data[i].numeroArrete.toUpperCase() + '</td>';
        tableContent += '<td style="width:18%;vertical-align:middle;text-align:left">' + data[i].dateArrete + '</td>';
        tableContent += '<td hidden="true">' + data[i].codeDocumentOfficiel + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultDocumentOfficiel.html(tableContent);

    var myDt = tableResultDocumentOfficiel.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 15,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableResultDocumentOfficiel tbody').on('click', 'tr', function () {
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        selectDocumentOfficielData = new Object();
        selectDocumentOfficielData.intituleDocumentOfficiel = myDt.row(this).data()[0].toUpperCase();
        selectDocumentOfficielData.numeroArrete = myDt.row(this).data()[1].toUpperCase();
        selectDocumentOfficielData.dateArrete = myDt.row(this).data()[2].toUpperCase();
        selectDocumentOfficielData.codeDocumentOfficiel = myDt.row(this).data()[3];

    });
}