package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.DetailAssujettissement;
import cd.hologram.erecettesvg.models.PrevisionCredit;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Assujettissement.class)
public class Assujettissement_ { 

    public static volatile SingularAttribute<Assujettissement, String> articleBudgetaire;
    public static volatile SingularAttribute<Assujettissement, String> code;
    public static volatile ListAttribute<Assujettissement, PrevisionCredit> previsionCreditList;
    public static volatile ListAttribute<Assujettissement, DetailAssujettissement> detailAssujettissementList;
    public static volatile SingularAttribute<Assujettissement, String> personne;
    public static volatile SingularAttribute<Assujettissement, Date> dateCreate;
    public static volatile SingularAttribute<Assujettissement, Integer> etat;

}