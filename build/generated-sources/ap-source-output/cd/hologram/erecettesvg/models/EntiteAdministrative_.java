package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.TypeEntite;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(EntiteAdministrative.class)
public class EntiteAdministrative_ { 

    public static volatile ListAttribute<EntiteAdministrative, Adresse> adresseList4;
    public static volatile ListAttribute<EntiteAdministrative, Adresse> adresseList3;
    public static volatile ListAttribute<EntiteAdministrative, Adresse> adresseList2;
    public static volatile SingularAttribute<EntiteAdministrative, TypeEntite> typeEntite;
    public static volatile SingularAttribute<EntiteAdministrative, String> code;
    public static volatile ListAttribute<EntiteAdministrative, Adresse> adresseList1;
    public static volatile SingularAttribute<EntiteAdministrative, EntiteAdministrative> entiteMere;
    public static volatile ListAttribute<EntiteAdministrative, Adresse> adresseList;
    public static volatile ListAttribute<EntiteAdministrative, EntiteAdministrative> entiteAdministrativeList;
    public static volatile SingularAttribute<EntiteAdministrative, String> intitule;
    public static volatile SingularAttribute<EntiteAdministrative, Short> etat;
    public static volatile ListAttribute<EntiteAdministrative, Adresse> adresseList5;

}