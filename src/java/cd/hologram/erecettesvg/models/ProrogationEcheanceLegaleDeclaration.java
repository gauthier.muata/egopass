/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_PROROGATION_ECHEANCE_LEGALE_DECLARATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findAll", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findById", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.id = :id"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByFkArticleBudgetaire", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.fkArticleBudgetaire = :fkArticleBudgetaire"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByDateDeclarationLegale", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.dateDeclarationLegale = :dateDeclarationLegale"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByDateDeclarationProrogee", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.dateDeclarationProrogee = :dateDeclarationProrogee"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByExercice", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.exercice = :exercice"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByAgentCreate", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.agentCreate = :agentCreate"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByAgentMaj", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.agentMaj = :agentMaj"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByDateCreate", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.dateCreate = :dateCreate"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByDateMaj", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.agentMaj = :agentMaj"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByDateLimitePaiement", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.dateLimitePaiement = :dateLimitePaiement"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByDateLimitePaiementProrogee", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.dateLimitePaiementProrogee = :dateLimitePaiementProrogee"),
    @NamedQuery(name = "ProrogationEcheanceLegaleDeclaration.findByEtat", query = "SELECT l FROM ProrogationEcheanceLegaleDeclaration l WHERE l.etat = :etat")})
public class ProrogationEcheanceLegaleDeclaration implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;

    @Column(name = "FK_ARTICLE_BUDGETAIRE")
    private String fkArticleBudgetaire;

    @Column(name = "DATE_DECLARATION_LEGALE")
    //@Temporal(TemporalType.TIMESTAMP)
    private String dateDeclarationLegale;

    @Column(name = "DATE_DECLARATION_PROROGEE")
    //@Temporal(TemporalType.TIMESTAMP)
    private String dateDeclarationProrogee;

    @Column(name = "DATE_LIMITE_PAIEMENT")
    //@Temporal(TemporalType.TIMESTAMP)
    private String dateLimitePaiement;

    @Column(name = "DATE_LIMITE_PAIEMENT_PROROGEE")
    //@Temporal(TemporalType.TIMESTAMP)
    private String dateLimitePaiementProrogee;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    @Column(name = "EXERCICE")
    private String exercice;

    @Column(name = "ETAT")
    private Integer etat;

    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    
    @Column(name = "AGENT_MAJ")
    private Integer agentMaj;

    @Column(name = "MOTIF")
    private String motif;

    public ProrogationEcheanceLegaleDeclaration() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkArticleBudgetaire() {
        return fkArticleBudgetaire;
    }

    public void setFkArticleBudgetaire(String fkArticleBudgetaire) {
        this.fkArticleBudgetaire = fkArticleBudgetaire;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Integer getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Integer agentMaj) {
        this.agentMaj = agentMaj;
    }
    
    
    public String getDateDeclarationLegale() {
        return dateDeclarationLegale;
    }

    public void setDateDeclarationLegale(String dateDeclarationLegale) {
        this.dateDeclarationLegale = dateDeclarationLegale;
    }

    public String getDateDeclarationProrogee() {
        return dateDeclarationProrogee;
    }

    public void setDateDeclarationProrogee(String dateDeclarationProrogee) {
        this.dateDeclarationProrogee = dateDeclarationProrogee;
    }

    public String getDateLimitePaiement() {
        return dateLimitePaiement;
    }

    public void setDateLimitePaiement(String dateLimitePaiement) {
        this.dateLimitePaiement = dateLimitePaiement;
    }

    public String getDateLimitePaiementProrogee() {
        return dateLimitePaiementProrogee;
    }

    public void setDateLimitePaiementProrogee(String dateLimitePaiementProrogee) {
        this.dateLimitePaiementProrogee = dateLimitePaiementProrogee;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getExercice() {
        return exercice;
    }

    public void setExercice(String exercice) {
        this.exercice = exercice;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProrogationEcheanceLegaleDeclaration)) {
            return false;
        }
        ProrogationEcheanceLegaleDeclaration other = (ProrogationEcheanceLegaleDeclaration) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.ProrogationEcheanceLegaleDeclaration[ id=" + id + " ]";
    }
}
