/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

//import cd.hologram.erecettesvg.business.AdministrationBusiness;
//import cd.hologram.erecettesvg.business.EchelonnementBusiness;
import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.DeclarationBusiness;
import cd.hologram.erecettesvg.business.FractionnementBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.GestionArticleBudgetaireBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.ContentieuxConst;
import cd.hologram.erecettesvg.constants.ControleConst;
//import cd.hologram.erecettesvg.constants.ControleConst;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.FractionnementConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.NotePerceptionConst;
import cd.hologram.erecettesvg.constants.PropertiesConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.ArchiveAccuseReception;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.DemandeEchelonnement;
import cd.hologram.erecettesvg.models.DetailEchelonnement;
import cd.hologram.erecettesvg.models.DetailFichePriseCharge;
import cd.hologram.erecettesvg.models.Division;
//import cd.hologram.erecettesvg.models.DemandeEchelonnement;
//import cd.hologram.erecettesvg.models.DetailEchelonnement;
import cd.hologram.erecettesvg.models.FichePriseCharge;
//import cd.hologram.erecettesvg.models.Evenement;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.TypeDocument;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.properties.PropertiesConfig;
import cd.hologram.erecettesvg.properties.PropertiesMessage;
import cd.hologram.erecettesvg.sql.SQLQueryFractionnement;
//import cd.hologram.erecettesvg.sql.SQLQueryEchelonnement;
import cd.hologram.erecettesvg.sql.SQLQueryMed;
//import cd.hologram.erecettesvg.sql.SQLQueryMed;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.sql.SQLQueryTaxation;
import cd.hologram.erecettesvg.util.Compare;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.Tools;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author WILLY KASHALA
 */
@WebServlet(name = "Fractionnement", urlPatterns = {"/fractionnement_servlet"})
public class Fractionnement extends HttpServlet {

    PropertiesMessage propertiesMessage;
    PropertiesConfig propertiesConfig;

    String valueOperation = GeneralConst.EMPTY_STRING;

    DemandeEchelonnement demandeEchelonnement;
    LogUser logUser;

    List<DemandeEchelonnement> listDemandeEchelonnements;
    List<JsonObject> listDeJson;
    JsonObject deJson;

    List<ArchiveAccuseReception> listArchiveAccuseReception;
    List<JsonObject> listJsonObjectArchAcc;
    JsonObject jsonObjectArchAcc;

    List<JsonObject> listNpFilleJson;
    JsonObject npFilleJson;

    HashMap<String, Object[]> bulkQuery;
    int counter = GeneralConst.Numeric.ZERO;

    List<NotePerception> listNotePerceptionFilles;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        propertiesMessage = new PropertiesMessage();
        propertiesConfig = new PropertiesConfig();

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation.trim()) {
            case FractionnementConst.Operation.LOAD_TITRE_DETAIL:
                valueOperation = operation.trim();
                result = loadTitreDetail(request);
                break;
            case FractionnementConst.Operation.SAVE_DEMANDE_ECHELONNEMENT:
                valueOperation = operation.trim();
                result = saveDemandeFractionnement(request);
                break;
            case FractionnementConst.Operation.LOAD_DEMANDE_ECHELONNEMENT:
                result = loadDemandeFractionnements(request);
                break;
            case FractionnementConst.Operation.VALIDER_DEMANDE_ECHELONNEMENT:
                result = validerDemandeFractionnements(request);
                break;
            case FractionnementConst.Operation.REJETER_DEMANDE_ECHELONNEMENT:
                result = rejeterDemandeEchelonnements(request);
                break;
            case FractionnementConst.Operation.ORDONNANCEMENT_NP_ECHELONNEMENT:
                valueOperation = operation.trim();
                result = ordonnancerEchelonnement(request);
                break;
            case FractionnementConst.Operation.ANNULATION_NP_FRACTIONNE:
                valueOperation = operation.trim();
                result = revocationFractionnement(request);
                break;
            case "initDataEchelonnement":
                valueOperation = operation.trim();
                result = initDataEchelonnement(request);
                break;
//            case EchelonnementConst.Operation.PRINT_NP_INTERCALAIRE:
//                result = printerNpIntercalaire(request);
//                break;
//            case EchelonnementConst.Operation.LOAD_ARCHIVE:
//                result = loadArchive(request);
//                break;
//            case EchelonnementConst.Operation.PROROGER_ECHEANCE_PAIEMENT:
//                result = prorogerEcheancePaiment(request);
//                break;

        }
        out.print(result);
    }

//    public String prorogerEcheancePaiment(HttpServletRequest request) {
//
//        String dataReturn;
//
//        String idUser = request.getParameter(GeneralConst.ID_USER);
//        String codeDemandeEchelonnement = request.getParameter(EchelonnementConst.ParamName.DemandeEchelonnementCode);
//        String npFilleProrogationList = request.getParameter(EchelonnementConst.ParamName.ListNpFilleProrogation);
//        String typeDocument = request.getParameter(GeneralConst.AccuserReception.TYPE_DOCUMENT);
//        String archives = request.getParameter(TaxationConst.ParamName.ARCHIVES);
//        String observation = request.getParameter(EchelonnementConst.ParamName.OBSERVATION);
//        String fkDocument = request.getParameter(EchelonnementConst.ParamName.FK_DOCUMENT);
//
//        JSONArray jsonNpFilleProrogationArray, jsonArchivesArray;
//        List<String> documentList = new ArrayList<>();
//
//        try {
//
//            jsonNpFilleProrogationArray = new JSONArray(npFilleProrogationList);
//            jsonArchivesArray = new JSONArray(archives);
//
//            for (int i = 0; i < jsonArchivesArray.length(); i++) {
//                JSONObject jsonObjectArchive = jsonArchivesArray.getJSONObject(i);
//                documentList.add(jsonObjectArchive.getString(ControleConst.ParamName.PV_DOCUMENT));
//            }
//
//            logUser = new LogUser(request, EchelonnementConst.Operation.PROROGER_ECHEANCE_PAIEMENT);
//
//            Log currentLog = new Log();
//
//            currentLog.setAgentCreat(new Agent(Integer.valueOf(idUser)));
//            Evenement env = new Evenement();
//            env.setCode("1207");
//            currentLog.setFkEvenement(env);
//            currentLog.setHostName(ClientInfo.getHostName(request));
//            currentLog.setIpAdress(ClientInfo.getClientIpAddr(request));
//            currentLog.setMacAdress(ClientInfo.getMACAddress(ClientInfo.getClientIpAddr(request)));
//            currentLog.setRefenceOccuernce(GeneralConst.EMPTY_STRING);
//            currentLog.setContextBrowser(ClientInfo.getClientInfo(request));
//
//            boolean result = EchelonnementBusiness.ProrogerEcheance(jsonNpFilleProrogationArray,
//                    typeDocument, documentList, fkDocument, observation,
//                    codeDemandeEchelonnement);
//
//            if (result) {
//
//                AdministrationBusiness.createLogUser(currentLog);
//
//                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
//            } else {
//
//                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
//            }
//
//        } catch (JSONException ex) {
//
//            CustumException.LogException(ex);
//            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
//        }
//
//        return dataReturn;
//    }
//
    public String loadTitreDetail(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String numeberDossier2;

        String typesearch = request.getParameter(ContentieuxConst.ParamName.TYPE_SEARCH_DOCUMENT);
        String codeAssujetti = request.getParameter(ContentieuxConst.ParamName.CODE_ASSUJETTI);
        String numeberDossier = request.getParameter(ContentieuxConst.ParamName.NUMERO_DOCUMENT);

        JsonObject titreJsonObject;
        boolean isEcheanceEchus = false;

        try {
            switch (typesearch) {

                case DocumentConst.DocumentCode.NP:

                    NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByAssujetti(
                            numeberDossier.trim(), codeAssujetti.trim());

                    if (notePerception != null) {

                        numeberDossier2 = notePerception.getNumero().trim();

                        titreJsonObject = new JsonObject();

                        Date dateCreateNP = Tools.formatStringFullToDate(notePerception.getDateCreat());
                        Date dateEcheanceNP;

                        if (notePerception.getDateEcheancePaiement() != null && notePerception.getNbrImpression() > GeneralConst.Numeric.ZERO) {
                            dateEcheanceNP = Tools.formatStringFullToDate(notePerception.getDateEcheancePaiement());
                            if (Compare.before(dateEcheanceNP, new Date())) {
                                return FractionnementConst.code.NINE_CENT_TWO;
                            }
                        } else {

                            return FractionnementConst.code.FIVE_CENT; // ce titre n'est pas un accusé de réception
                        }

                        if (notePerception.getFractionnee().toString().equals(GeneralConst.Number.ONE)) {

                            return FractionnementConst.code.SEVEN_CENT; // ce titre est déjà fractionné
                        }

                        Journal journal = PaiementBusiness.getJournalByDocument(notePerception.getNumero());

                        if (journal != null) {

                            return FractionnementConst.code.HEIGHT_CENT; // ce titre est déjà payé;

                        }

                        demandeEchelonnement = FractionnementBusiness.getDemandeEchelonnementBytitre(numeberDossier2);

                        if (demandeEchelonnement != null) {

                            return FractionnementConst.code.NINE_CENT; // ce titre a une demande de fractionnement en cours;

                        }

                        titreJsonObject.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT, notePerception.getNumero().trim());

                        titreJsonObject.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT_MANUEL, notePerception.getNumero().trim());

                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, notePerception.getNetAPayer());
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, notePerception.getDevise());
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceEchus);
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_CREAT, Tools.formatDateToString(dateCreateNP));
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, Tools.formatDateToString(dateEcheanceNP));

                        Service service = TaxationBusiness.getServiceByCode(notePerception.getNoteCalcul().getService());
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.SERVICE, service == null ? GeneralConst.EMPTY_STRING : service.getIntitule());

                        String articleName = Tools.getArticleByNC(notePerception.getNoteCalcul());
                        String articleCode = Tools.getCodeBudgetaireByNC(notePerception.getNoteCalcul());

                        titreJsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, articleName);

                        titreJsonObject.addProperty(TaxationConst.ParamName.CODE_BUDGETAIRE, articleCode.toUpperCase());

                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.NP);

                        dataReturn = titreJsonObject.toString();

                    } else {

                        dataReturn = GeneralConst.ResultCode.NO_FOUND;
                    }

                    break;

                case DocumentConst.DocumentCode.NT:

                    RetraitDeclaration retraitDeclaration = NotePerceptionBusiness.getRetraitDeclarationByCode(
                            Integer.valueOf(numeberDossier), codeAssujetti.trim());

                    if (retraitDeclaration != null) {

                        BigDecimal amountRD = new BigDecimal("0");

                        if (retraitDeclaration.getNewId() != null) {

                            List<RetraitDeclaration> listRetraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID(retraitDeclaration.getNewId());

                            if (!listRetraitDeclarations.isEmpty()) {

                                for (RetraitDeclaration rd : listRetraitDeclarations) {
                                    amountRD = amountRD.add(rd.getMontant());
                                }
                            }
                        } else {
                            amountRD = amountRD.add(retraitDeclaration.getMontant());
                        }

                        numeberDossier2 = retraitDeclaration.getId() + "";

                        titreJsonObject = new JsonObject();

                        String dateCreateNP = Tools.formatDateToStringV2(retraitDeclaration.getDateCreate());

                        String dateEcheanceNP = Tools.formatDateToStringV2(retraitDeclaration.getDateEcheancePaiement());

                        if (retraitDeclaration.getEtat() == 2) {

                            return FractionnementConst.code.SEVEN_CENT; // ce titre est déjà fractionné

                        } else if (retraitDeclaration.getEtat() == 3) {

                            return FractionnementConst.code.LIGHT; // ce titre est un échelonnment
                        }

                        Journal journal = PaiementBusiness.getJournalByDocument(retraitDeclaration.getId() + "");

                        if (journal != null) {

                            return FractionnementConst.code.HEIGHT_CENT; // ce titre est déjà payé;

                        }

                        demandeEchelonnement = FractionnementBusiness.getDemandeEchelonnementBytitre(numeberDossier2);

                        if (demandeEchelonnement != null) {

                            return FractionnementConst.code.NINE_CENT; // ce titre a une demande de fractionnement en cours;

                        }

                        titreJsonObject.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT, retraitDeclaration.getId() + "");

                        titreJsonObject.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT_MANUEL, retraitDeclaration.getId() + "");

                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, amountRD);
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, retraitDeclaration.getDevise());
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceEchus);
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_CREAT, dateCreateNP);
                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, dateEcheanceNP);

                        Site site = TaxationBusiness.getSiteByCode(retraitDeclaration.getFkSite());

                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.SERVICE, site == null ? GeneralConst.EMPTY_STRING : site.getIntitule().toUpperCase());

                        ArticleBudgetaire articleBudgetaire = GestionArticleBudgetaireBusiness.getArticleBudgetaireByAG(retraitDeclaration.getFkAb());

                        String articleName = articleBudgetaire.getIntitule().toUpperCase();
                        String articleCode = articleBudgetaire.getCode();

                        titreJsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, articleName);

                        titreJsonObject.addProperty(TaxationConst.ParamName.CODE_BUDGETAIRE, articleCode.toUpperCase());

                        titreJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.NT);

                        dataReturn = titreJsonObject.toString();

                    } else {

                        dataReturn = GeneralConst.ResultCode.NO_FOUND;
                    }

                    break;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }
//
//    public String printerNpIntercalaire(HttpServletRequest request) {
//
//        String dataReturn = GeneralConst.EMPTY_STRING;
//        String numeroNpIntercaleire;
//
//        try {
//
//            numeroNpIntercaleire = request.getParameter(EchelonnementConst.ParamName.NumeroNpFille);
//
//            NotePerception notePerception = EchelonnementBusiness.getNotePerceptionIntercalaireByNumero(
//                    numeroNpIntercaleire.trim());
//
//            if (notePerception != null) {
//                PrintDocument printDocument = new PrintDocument();
//                dataReturn = printDocument.createNP(notePerception);
//            }
//        } catch (NumberFormatException | InvocationTargetException e) {
//            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
//        }
//
//        return dataReturn;
//    }
//

    public String rejeterDemandeEchelonnements(HttpServletRequest request) {

        boolean result;

        String dataReturn;
        String codeLettreDemande = request.getParameter(FractionnementConst.ParamName.CodeDemande);
        String etatDemandeEchel = request.getParameter(FractionnementConst.ParamName.Etat);
        String observationDemandeEchel = request.getParameter(FractionnementConst.ParamName.Observation);
        String userId = request.getParameter(FractionnementConst.ParamName.UserId);
        demandeEchelonnement = new DemandeEchelonnement();

        try {

            demandeEchelonnement.setCode(codeLettreDemande);
            demandeEchelonnement.setObservationValidation(observationDemandeEchel);
            demandeEchelonnement.setEtat(Integer.valueOf(etatDemandeEchel));
            demandeEchelonnement.setAgentValidation(Integer.valueOf(userId));

//          saveLogUserInfo(request);
            result = FractionnementBusiness.validerDemandeEchelonnement(demandeEchelonnement, logUser);

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String validerDemandeFractionnements(HttpServletRequest request) {

        boolean result;
        bulkQuery = new HashMap<>();
        counter = GeneralConst.Numeric.ONE;
        Date dateEcheanceFirstNP = null;
        String dataReturn;

        NotePerception notePerception;
        BonAPayer bonApayer;

        try {

            String observationValidation = request.getParameter(FractionnementConst.ParamName.Observation);
            String etatDemandeEchel = request.getParameter(FractionnementConst.ParamName.Etat);
            String userId = request.getParameter(FractionnementConst.ParamName.UserId);

            String codeDemandeEchelonnement = request.getParameter(FractionnementConst.ParamName.DemandeEchelonnementCode);
            String numeroTitre = request.getParameter(FractionnementConst.ParamName.NUMERO_TITRE);
            String numeroTitreManuel = request.getParameter(FractionnementConst.ParamName.NUMERO_TITRE_MANUEL);
            String typeTitre = request.getParameter(FractionnementConst.ParamName.TYPE_TITRE);
            String intercalaireList = request.getParameter(FractionnementConst.ParamName.INTERCALAIRE_LIST);
            String montantInteret = request.getParameter(FractionnementConst.ParamName.MontantInteret);
            String archives = request.getParameter(TaxationConst.ParamName.ARCHIVES);
            String fkDocument = request.getParameter(FractionnementConst.ParamName.FK_DOCUMENT);
            String codeSite = request.getParameter(GeneralConst.ParamName.SITE_CODE);
            String observationDoc = request.getParameter(FractionnementConst.ParamName.OBSERVATION_DOCUMENT);

            List<String> documentList = new ArrayList<>();
            JSONArray jsonDocumentArray;

            BigDecimal amountInteret = BigDecimal.valueOf(Double.valueOf(montantInteret) / 2);

            if (archives != null && !archives.isEmpty()) {

                jsonDocumentArray = new JSONArray(archives);

                for (int i = 0; i < jsonDocumentArray.length(); i++) {
                    JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                    documentList.add(jsonObject.getString(ControleConst.ParamName.PV_DOCUMENT));
                }
            }

            for (String docu : documentList) {
                counter++;
                bulkQuery.put(counter + SQLQueryNotePerception.ARCHIVE_ACCUSER_RECEPTION,
                        new Object[]{
                            codeDemandeEchelonnement,
                            typeTitre,
                            docu,
                            fkDocument, observationDoc
                        });
            }

//            saveLogUserInfo(request);
            counter++;
            bulkQuery.put(counter + SQLQueryFractionnement.VALIDER_DEMANDE_ECHELONNEMENT, new Object[]{
                numeroTitre,
                Integer.valueOf(etatDemandeEchel),
                observationValidation,
                Integer.valueOf(userId),
                "",//logUser.getMacAddress(),
                "",//logUser.getIpAddress(),
                "",//logUser.getHostName().trim(),
                ""//logUser.getBrowserContext()
            });

            if (typeTitre.equals(DocumentConst.DocumentCode.NP) || typeTitre.equals(DocumentConst.DocumentCode.NP_P)) {

                notePerception = NotePerceptionBusiness.getNotePerceptionByCode(numeroTitre);

                if (notePerception != null) {

                    counter++;
                    bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                        notePerception.getNoteCalcul().getNumero(),
                        notePerception.getNumero(),
                        etatDemandeEchel.equals(GeneralConst.Number.ONE)
                        ? propertiesMessage.getContent(PropertiesConst.Message.ECHELONNEMENT_VALIDATION)
                        : propertiesMessage.getContent(PropertiesConst.Message.ECHELONNEMENT_REJET),
                        GeneralConst.Numeric.ZERO,
                        GeneralConst.Numeric.ZERO,
                        propertiesMessage.getContent(PropertiesConst.Message.TRAITEMENT_DEMANDE_ECHELONNEMENT) + notePerception.getNumero().trim(),
                        notePerception.getDevise()
                    });

                    counter++;
                    bulkQuery.put(counter + SQLQueryFractionnement.UPDATE_NP_FRACTIONNER, new Object[]{
                        notePerception.getNumero()
                    });

                    if (etatDemandeEchel.equals(GeneralConst.Number.ONE)) {

                        JSONArray jsonIntercalaireArray = new JSONArray(intercalaireList);

//                        ArticleBudgetaire abAmende = Tools.getAmendeBySecteur(notePerception.getNoteCalcul().getService(),
//                                propertiesConfig.getContent(PropertiesConst.Config.ID_NATURE_AB_AMENDE));
                        for (int i = GeneralConst.Numeric.ZERO; i < jsonIntercalaireArray.length(); i++) {

                            JSONObject jsonObject = jsonIntercalaireArray.getJSONObject(i);

                            BigDecimal trancheAmount = BigDecimal.valueOf(Double.valueOf(jsonObject.getString(FractionnementConst.ParamName.AmountNpIntercalaire)));
                            String trancheEcheance = jsonObject.getString(FractionnementConst.ParamName.EcheanceNpIntercalaire);
                            trancheEcheance = ConvertDate.getValidFormatDateString(trancheEcheance);

                            if (i == GeneralConst.Numeric.ZERO) {
                                dateEcheanceFirstNP = Tools.formatStringFullToDateV2(trancheEcheance);
                            }

                            counter++;
                            bulkQuery.put(counter + SQLQueryFractionnement.F_NEW_NOTE_PERCEPTION_INTERCALAIRE, new Object[]{
                                notePerception.getNoteCalcul().getNumero(),
                                trancheAmount,
                                GeneralConst.Numeric.ZERO,
                                trancheAmount,
                                notePerception.getNoteCalcul().getExercice(),
                                codeSite,
                                trancheEcheance,
                                numeroTitre,
                                userId,
                                GeneralConst.EMPTY_STRING,
                                GeneralConst.EMPTY_STRING,
                                "",//logUser.getMacAddress().trim(),
                                "",//logUser.getIpAddress().trim(),
                                "",//logUser.getHostName().trim(),
                                1202,
                                ""//logUser.getBrowserContext().trim()
                            });
                        }

                    }
                }

            } else {

                RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByCode(numeroTitre);

                if (retraitDeclaration != null) {

                    if (etatDemandeEchel.equals(GeneralConst.Number.ONE)) {

                        counter++;
                        bulkQuery.put(counter + ":UPDATE T_RETRAIT_DECLARATION SET ETAT = 2 WHERE ID = ?1", new Object[]{
                            retraitDeclaration.getId()
                        });

                        JSONArray jsonIntercalaireArray = new JSONArray(intercalaireList);

                        for (int i = GeneralConst.Numeric.ZERO; i < jsonIntercalaireArray.length(); i++) {

                            counter++;

                            JSONObject jsonObject = jsonIntercalaireArray.getJSONObject(i);

                            BigDecimal trancheAmount = BigDecimal.valueOf(Double.valueOf(jsonObject.getString(FractionnementConst.ParamName.AmountNpIntercalaire)));
                            String trancheEcheance = jsonObject.getString(FractionnementConst.ParamName.EcheanceNpIntercalaire);
                            trancheEcheance = ConvertDate.getValidFormatDateString(trancheEcheance);

                            bulkQuery.put(counter + SQLQueryMed.EXEC_F_NEW_RETRAIT_DECLARATION_ECHELONNE, new Object[]{
                                userId,
                                retraitDeclaration.getId(),
                                trancheAmount,
                                trancheEcheance
                            });
                        }

                    }
                }
            }

            result = executeQueryBulkInsert(bulkQuery);

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                resetBulkQuery();
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NumberFormatException | JSONException e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    private void resetBulkQuery() {
        bulkQuery = new HashMap<>();
        counter = GeneralConst.Numeric.ZERO;
    }

    public String initDataEchelonnement(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<Division> listDivisionEchelonnement = FractionnementBusiness.getListDivisionEchelonnement(
                    propertiesConfig.getContent(PropertiesConst.Config.CODE_DIVISION_ECHELONNEMENT));

            if (!listDivisionEchelonnement.isEmpty()) {

                List<JsonObject> divisionJsonList = new ArrayList<>();

                for (Division division : listDivisionEchelonnement) {

                    JsonObject divisionJson = new JsonObject();

                    divisionJson.addProperty("code", division.getCode());
                    divisionJson.addProperty("intitule", division.getIntitule().toUpperCase());

                    divisionJsonList.add(divisionJson);
                }

                dataReturn = divisionJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String revocationFractionnement(HttpServletRequest request) {

        String dataReturn = null;
        boolean result;
        counter = GeneralConst.Numeric.ZERO;
        bulkQuery = new HashMap<>();

        String numeroTitre = request.getParameter(FractionnementConst.ParamName.NUMERO_TITRE);
//        String userId = request.getParameter(FractionnementConst.ParamName.UserId);
        String codeDemandeEchelonnement = request.getParameter(FractionnementConst.ParamName.DemandeEchelonnementCode);
//        String dateLimite = request.getParameter(FractionnementConst.ParamName.DateLimiteEcheance);
        String typeTitre = request.getParameter(FractionnementConst.ParamName.TYPE_TITRE);

        String codeSite = request.getParameter(GeneralConst.ParamName.SITE_CODE);

//        String isGetDateLimiteByIntercalaire = propertiesConfig.getContent(PropertiesConst.Config.IS_GET_DATE_LIMITE_REVOCATION_BY_INTERCALAIRE);
        try {

            switch (typeTitre) {
                case DocumentConst.DocumentCode.NP:
                case DocumentConst.DocumentCode.NP_P:

                    NotePerception notePerceptionMere = NotePerceptionBusiness.getNotePerceptionFractionne(numeroTitre.trim());

                    numeroTitre = notePerceptionMere.getNumero().trim();

//                    List<NotePerception> notePenaliteInteretList = NotePerceptionBusiness.getListNpInteretByNp(numeroTitre.trim());
//                    BonAPayer bonAPayerInteret = EchelonnementBusiness.getBonAPayerInteretByNP(numeroTitre.trim());
                    listNotePerceptionFilles = NotePerceptionBusiness.getListNpFilleByMere(numeroTitre.trim());

                    if (!listNotePerceptionFilles.isEmpty()) {

                        for (NotePerception np : listNotePerceptionFilles) {

                            Journal journal = PaiementBusiness.getJournalByDocument(np.getNumero().trim());

                            if (journal == null) {

                                // On annule toutes les notes filles fractionnées
                                counter++;
                                bulkQuery.put(counter + SQLQueryNotePerception.F_CLOSE_NP_FRACTIONNE, new Object[]{
                                    np.getNumero().trim()
                                });
                            }
                        }

                        counter++;
                        bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                            notePerceptionMere.getNoteCalcul().getNumero().trim(),
                            notePerceptionMere.getNumero().trim(),
                            propertiesMessage.getContent(PropertiesConst.Message.FRACTIONNEMENT_REVOCATION),
                            GeneralConst.Numeric.ZERO,
                            GeneralConst.Numeric.ZERO,
                            propertiesMessage.getContent(PropertiesConst.Message.FRACTIONNEMENT_REVOCATION) + GeneralConst.TWO_POINTS + notePerceptionMere.getNumero().trim(),
                            notePerceptionMere.getDevise()
                        });

                        // On annule le fractionnement de la note mere
                        counter++;
                        bulkQuery.put(counter + SQLQueryNotePerception.F_ANNULE_FRACTIONNEMENT_NP_MERE, new Object[]{
                            notePerceptionMere.getNumero().trim()
                        });

                        // On annule la demande de fractionnement
                        counter++;
                        bulkQuery.put(counter + SQLQueryNotePerception.F_ANNULATION_DEMANDE_FRACTIONNEMENT_AFTER_REVOCATION, new Object[]{
                            codeDemandeEchelonnement.trim()
                        });
                        // On annule les détails de demande de fractionnement

                        counter++;
                        bulkQuery.put(counter + SQLQueryNotePerception.F_ANNULATION_DETAILS_FRACTIONNEMENT_AFTER_REVOCATION, new Object[]{
                            codeDemandeEchelonnement.trim()
                        });

                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
                case DocumentConst.DocumentCode.BP:

//                    BonAPayer bonApayerParent = PaiementBusiness.getBonAPayerByCode(numeroTitre);
//                    List<NotePerception> notePenaliteInteretListBP = NotePerceptionBusiness.getListNPInteretByBP(numeroTitre.trim());
//                    BonAPayer bonAPayerInteretBP = EchelonnementBusiness.getBonAPayerInteretByBP(numeroTitre.trim());
//                    List<BonAPayer> listBonApayerIntercalaire = EchelonnementBusiness.getListBonApayerFilleByMere(numeroTitre.trim());
//                    if (!listBonApayerIntercalaire.isEmpty()) {
//                        Date dateLimiteRevocation = null;
//                        double netAPayerNPenalite = GeneralConst.Numeric.ZERO;
//                        double netAPayerBpMere = bonApayerParent.getMontant().floatValue();
//                        double sumBpFilleNonPayee = GeneralConst.Numeric.ZERO;
//                        double netAPayerBP = GeneralConst.Numeric.ZERO;
//
//                        if (bonAPayerInteretBP != null) {
//                            // Ce n'est pas payé
//                            netAPayerBP = bonApayerParent.getMontant().floatValue();
//
//                            // On annule le bon à payer que si ce n'est pas encore payé
//                            counter++;
//                            bulkQuery.put(counter + SQLQueryEchelonnement.F_CLOSE_BON_A_PAYER, new Object[]{
//                                bonAPayerInteretBP.getCode().trim()
//                            });
//
//                        } else {
//                            bonAPayerInteretBP = EchelonnementBusiness.getBonAPayerFromFractionByNoteCalcul(bonApayerParent.getFkNoteCalcul().getNumero());
//                        }
//
//                        if (bonAPayerInteretBP != null) {
//
//                            if (notePenaliteInteretListBP != null) {
//
//                                for (NotePerception notePenalite : notePenaliteInteretListBP) {
//
//                                    String dateBP = Tools.formatDateToStringV4(bonAPayerInteretBP.getDateEcheance());
//                                    String dateNPenalite = notePenalite.getDateEcheancePaiement();
//                                    double montNPenalite = notePenalite.getNetAPayer().floatValue();
//
//                                    if (dateBP.equals(dateNPenalite) && ((bonAPayerInteretBP.getMontant().floatValue()) == montNPenalite)) {
//
//                                        Journal journal = PaiementBusiness.getJournalApurerByDocument(notePenalite.getNumero().trim());
//
//                                        if (journal == null) {
//
//                                            // Ce n'est pas payé
//                                            netAPayerNPenalite = montNPenalite;
//
//                                            //On annule la note de pénalité que si elle n'est pas encore payée
//                                            counter++;
//                                            bulkQuery.put(counter + SQLQueryEchelonnement.F_CLOSE_NP_ECHELONNEE, new Object[]{
//                                                notePenalite.getNumero().trim()
//                                            });
//                                        }
//
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//
//                        for (BonAPayer bp : listBonApayerIntercalaire) {
//
//                            Journal journal = PaiementBusiness.getJournalApurerByDocument(bp.getCode().trim());
//                            if (journal == null) {
//                                sumBpFilleNonPayee += bp.getMontant().floatValue();
//
//                                // On annule toutes les notes intercalaires
//                                // Mais la nouvelle NC ne tiendra compte que de la somme des notes non payées
//                                counter++;
//                                bulkQuery.put(counter + SQLQueryEchelonnement.F_CLOSE_BON_A_PAYER, new Object[]{
//                                    bp.getCode().trim()
//                                });
//                            }
//                        }
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryEchelonnement.F_CLOSE_BON_A_PAYER, new Object[]{
//                            numeroTitre.trim()
//                        });
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
//                            bonApayerParent.getFkNoteCalcul().getNumero().trim(),
//                            bonApayerParent.getCode().trim(),
//                            propertiesMessage.getContent(PropertiesConst.Message.ECHELONNEMENT_REVOCATION),
//                            GeneralConst.Numeric.ZERO,
//                            GeneralConst.Numeric.ZERO,
//                            propertiesMessage.getContent(PropertiesConst.Message.ECHELONNEMENT_REVOCATION) + GeneralConst.TWO_POINTS + bonApayerParent.getCode().trim(),
//                            bonApayerParent.getDevise()
//                        });
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryEchelonnement.F_CLOSE_BON_A_PAYER, new Object[]{
//                            bonApayerParent.getCode().trim()
//                        });
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryGeneral.EXEC_F_NEW_LOG, new Object[]{
//                            userId.trim(),
//                            logUser.getMacAddress().trim(),
//                            logUser.getIpAddress().trim(),
//                            logUser.getHostName().trim(),
//                            codeDemandeEchelonnement.trim(),
//                            valueOperation,
//                            logUser.getBrowserContext().trim(),
//                            GeneralConst.TablesNames.T_DEMANDE_ECHELONNEMENT
//                        });
//
//                        if (sumBpFilleNonPayee == netAPayerBpMere) {
//                            //Rien n'est payé
//                            dateLimiteRevocation = Tools.formatStringFullToDate(bonApayerParent.getDateEcheance().toString());
//                        } else if (sumBpFilleNonPayee < netAPayerBpMere) {
//                            //Payé une partie           
//                            if (isGetDateLimiteByIntercalaire.equals(GeneralConst.Number.ONE)) {
//                                dateLimiteRevocation = Tools.formatStringFullToDateV2(dateLimite);
//                            } else {
//                                dateLimiteRevocation = Tools.formatStringFullToDate(bonApayerParent.getDateEcheance().toString());
//                            }
//                        }
//
//                        sumBpFilleNonPayee += (netAPayerBP + netAPayerNPenalite);
//
//                        long moisRetard = ConvertDate.getMonthsBetween(dateLimiteRevocation, new Date());
//                        double tauxInteret = Double.valueOf(propertiesConfig.getContent(PropertiesConst.Config.TAUX_INTERET_MORATOIRE));
//                        double interetMoratoire = (sumBpFilleNonPayee * tauxInteret) * moisRetard;
//
//                        int counterFPC = GeneralConst.Numeric.ONE;
//                        HashMap<String, Object[]> secondBulk = new HashMap<>();
//                        secondBulk.put(counterFPC + SQLQueryPoursuite.ExecuteQuery.CREATE_DETAIL_PRISE_CHARGE_v1, new Object[]{
//                            sumBpFilleNonPayee,
//                            interetMoratoire,
//                            GeneralConst.POURCENT,
//                            (tauxInteret * 100),
//                            sumBpFilleNonPayee,
//                            propertiesConfig.getContent(PropertiesConst.Config.CODE_PENALITE_INTERET_MORATOIRE).trim(),
//                            moisRetard
//                        });
//
//                        List<ArticleBudgetaireTaxation> listArticleBudgetaireTaxation = new ArrayList<>();
//
//                        for (DetailsNc dnc : bonApayerParent.getFkNoteCalcul().getDetailsNcList()) {
//                            ArticleBudgetaireTaxation articleBudgetaireTaxation = new ArticleBudgetaireTaxation();
//                            articleBudgetaireTaxation.setArticleBudgetaire(new ArticleBudgetaire(dnc.getArticleBudgetaire().getCode().trim()));
//                            articleBudgetaireTaxation.setBaseCalcul(dnc.getValeurBase());
//                            articleBudgetaireTaxation.setCodeTarif(dnc.getTarif().getCode().trim());
//                            articleBudgetaireTaxation.setUnite(null);
//                            articleBudgetaireTaxation.setDevise(bonApayerParent.getDevise());
//                            articleBudgetaireTaxation.setMontantDu(BigDecimal.valueOf(Double.valueOf(String.valueOf(sumBpFilleNonPayee))));
//                            articleBudgetaireTaxation.setQte(dnc.getQte());
//                            articleBudgetaireTaxation.setTypeTaux(dnc.getTypeTaux());
//                            articleBudgetaireTaxation.setTaux(dnc.getTauxArticleBudgetaire());
//                            articleBudgetaireTaxation.setQte(dnc.getQte());
//                            listArticleBudgetaireTaxation.add(articleBudgetaireTaxation);
//                        }
//
//                        NewNoteCalcul newNoteCalcul = new NewNoteCalcul();
//                        newNoteCalcul.setCodePersonne(bonApayerParent.getFkNoteCalcul().getPersonne().getCode().trim());
//                        newNoteCalcul.setCodeAdresse(bonApayerParent.getFkNoteCalcul().getFkAdressePersonne().getAdresse().getId().trim());
//                        newNoteCalcul.setCodeService(bonApayerParent.getFkNoteCalcul().getService().getCode().trim());
//                        newNoteCalcul.setCodeSite(codeSite);
//                        newNoteCalcul.setAgentCreate(propertiesConfig.getContent(PropertiesConst.Config.CODE_AGENT_AUTO));
//
//                        String dateCreate = ConvertDate.formatDateToStringOfFormat(new Date(), GeneralConst.Format.FORMAT_DATE);
//
//                        newNoteCalcul.setDateCreate(dateCreate);
//                        newNoteCalcul.setCodeDepot(null);
//                        newNoteCalcul.setExerciceFiscal(bonApayerParent.getFkNoteCalcul().getExercice().getCode().trim());
//                        newNoteCalcul.setIsTaxationRegularisation(Boolean.FALSE);
//                        newNoteCalcul.setIsTaxationOffice(Boolean.FALSE);
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryEchelonnement.F_CLOTURE_NC_PENALITE_V1, new Object[]{
//                            new Date(),
//                            propertiesConfig.getContent(PropertiesConst.Config.CODE_AGENT_AUTO),
//                            propertiesMessage.getContent(PropertiesConst.Message.TXT_OBSERVATION_CLOTURE_NC_SPECIALE),
//                            Integer.valueOf(propertiesConfig.getContent(PropertiesConst.Config.STATE_NC_REVOCATION))});
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryEchelonnement.F_UPDATE_DATE_LIMITE_REVOCATION_V1, new Object[]{
//                            dateLimiteRevocation
//                        });
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryEchelonnement.F_ANNULATION_DEMANDE_ECHELONNEMENT_AFTER_REVOCATION, new Object[]{
//                            codeDemandeEchelonnement.trim()
//                        });
//
//                        for (ArticleBudgetaireTaxation abt : listArticleBudgetaireTaxation) {
//                            counter++;
//                            bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_DNC_V2, new Object[]{
//                                abt.getArticleBudgetaire().getCode().trim(),
//                                abt.getTaux(),
//                                abt.getBaseCalcul(),
//                                abt.getMontantDu(),
//                                abt.getQte(),
//                                Integer.valueOf(GeneralConst.Number.ZERO),
//                                abt.getTypeTaux().trim(),
//                                GeneralConst.Numeric.ZERO,
//                                abt.getCodeTarif().trim(),
//                                abt.getDevise().trim()
//                            });
//
//                        }
//
////                        dataReturn = EchelonnementBusiness.revocation(
////                                newNoteCalcul,
////                                bulkQuery,
////                                secondBulk,
////                                logUser,
////                                interetMoratoire,
////                                sumBpFilleNonPayee,
////                                bonApayerParent.getDevise());
//                        //if (dataReturn.isEmpty()) {
//                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
////                        } else {
////                            dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
////                        }
//
//                    } else {
//                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
//                    }
                    break;
            }

            result = executeQueryBulkInsert(bulkQuery);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String ordonnancerEchelonnement(HttpServletRequest request) {

        boolean result;
        String dataReturn;
        bulkQuery = new HashMap<>();

        try {
            String titreMere = request.getParameter(FractionnementConst.ParamName.NUMERO_TITRE);
            String codeDemandeEchelonnement = request.getParameter(FractionnementConst.ParamName.DemandeEchelonnementCode);
            String typeTitre = request.getParameter(FractionnementConst.ParamName.TYPE_TITRE);
//            String titreEchelonnement = request.getParameter(FractionnementConst.ParamName.TITRE_MANUEL_ECHELONNEMENT);
//            String prefixProvince = request.getParameter(FractionnementConst.ParamName.PREFIX_PROVINCE);
//            String idDispatch = request.getParameter(TaxationConst.ParamName.ID_DISPATCH_SERIE);
            String siteCode = request.getParameter(GeneralConst.ParamName.SITE_CODE);

            counter = GeneralConst.Numeric.ZERO;

            bulkQuery.put(counter + SQLQueryFractionnement.F_UPDATE_DETAIL_ECHELONNEMENT_AFTER_ORDONNANCEMENT, new Object[]{
                titreMere, codeDemandeEchelonnement.trim()
            });

            counter++;
            bulkQuery.put(counter + SQLQueryFractionnement.F_UPDATE_NOTES_PERCEPTIONS_FRACTIONNEES, new Object[]{titreMere});

            String nc = GeneralConst.EMPTY_STRING;
            NotePerception np = PaiementBusiness.getNotePerceptionByCode(titreMere);
            nc = np.getNoteCalcul().getNumero().trim();

            String devise = GeneralConst.EMPTY_STRING;
            devise = np.getDevise();
//
//            if (typeTitre.equals(DocumentConst.DocumentCode.NP) || typeTitre.equals(DocumentConst.DocumentCode.NP_P)) {
//
//                NotePerception np = PaiementBusiness.getNotePerceptionByCode(titreMere);
//
//                nc = np.getNoteCalcul().getNumero().trim();
//                devise = np.getDevise();
//
//                List<BonAPayer> listBonAPayers;
//                listBonAPayers = FractionnementBusiness.getListBonAPayerByNpMere(np.getNumero().trim());
//
//                BigDecimal interetEchelonnement = new BigDecimal(BigInteger.ZERO);
//
//                if (!listBonAPayers.isEmpty()) {
//
//                    int idBapInt = TaxationBusiness.getLastIDBonAPayer();
//                    Calendar toDay = new GregorianCalendar();
//                    toDay.setTime(new Date());
//                    int yearToInt = toDay.get(Calendar.YEAR);
//
//                    for (BonAPayer bap : listBonAPayers) {
//
//                        interetEchelonnement = bap.getMontant();
//
//                        idBapInt++;
//                        String idBapStr = String.valueOf(idBapInt);
//                        String yearToStr = String.valueOf(yearToInt);
//                        String numeroManuelBap = idBapStr.concat("/DGRK/").concat(yearToStr);
//
//                        counter++;
//                        bulkQuery.put(counter + SQLQueryFractionnement.F_UPDATE_NUMERO_MANUEL_BON_A_PAYER, new Object[]{
//                            numeroManuelBap.trim(), bap.getCode().trim()
//                        });
//                    }
//                }
//
//                int nbre = 0;
//
//                if (titreEchelonnement != null && !titreEchelonnement.isEmpty()) {
//
//                    JSONArray jsonEchelonnementArray = new JSONArray(titreEchelonnement);
//
//                    for (int i = GeneralConst.Numeric.ZERO; i < jsonEchelonnementArray.length(); i++) {
//                        nbre++;
//
//                        JSONObject jsonObject = jsonEchelonnementArray.getJSONObject(i);
//                        String numeroTitre = jsonObject.getString(FractionnementConst.ParamName.NUMERO_TITRE);
//                        String numeroTitreManuel = jsonObject.getString(FractionnementConst.ParamName.NUMERO_TITRE_MANUEL);
//
//                        counter++;
//
//                        String titreManuel = prefixProvince + "" + numeroTitreManuel.trim();
//                        bulkQuery.put(counter + SQLQueryFractionnement.F_UPDATE_NUMERO_MANUEL_NOTE_INTERCALIERE, new Object[]{
//                            titreManuel.trim(), siteCode.trim(), numeroTitre.trim()
//                        });
//                    }
//
//                    counter++;
//                    String queryUpdate = SQLQueryTaxation.ExecuteQuery.F_UPDATE_QTE_UTILISEE_DISPATCH;
//                    queryUpdate = String.format(queryUpdate, nbre++);
//
//                    bulkQuery.put(counter + queryUpdate, new Object[]{
//                        idDispatch
//                    });
//
//                    interetEchelonnement = interetEchelonnement.multiply(BigDecimal.valueOf(2));
//
//                    counter++;
//                    bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
//                        nc,
//                        titreMere.trim(),
//                        propertiesMessage.getContent(PropertiesConst.Message.TXT_OPERATION_NP_PENALITE),
//                        interetEchelonnement,
//                        GeneralConst.Numeric.ZERO,
//                        propertiesMessage.getContent(PropertiesConst.Message.TXT_OBSERVATION_NP_PENALITE),
//                        devise
//                    });
//                }
//
//            } else if (typeTitre.equals(DocumentConst.DocumentCode.BP)) {
//                BonAPayer bp = PaiementBusiness.getBonAPayerByCode(titreMere);
//                nc = bp.getFkNoteCalcul().getNumero().trim();
//                devise = "CDF";//bp.getDevise();
//            }

            counter++;
            bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                nc,
                titreMere,
                propertiesMessage.getContent(PropertiesConst.Message.ECHELONNEMENT_ORDONANCEMENT),
                GeneralConst.Numeric.ZERO,
                GeneralConst.Numeric.ZERO,
                propertiesMessage.getContent(PropertiesConst.Message.ECHELONNEMENT_ORDONANCEMENT) + GeneralConst.TWO_POINTS + titreMere.trim(),
                devise
            });

            result = executeQueryBulkInsert(bulkQuery);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadDemandeFractionnements(HttpServletRequest request) {

        String valueSearch = request.getParameter(FractionnementConst.ParamName.ValueSearch);
        String typeSearch = request.getParameter(FractionnementConst.ParamName.TypeeSearch);
        String modeSearch = request.getParameter(FractionnementConst.ParamName.ModeSearch);
        String codeDivision = request.getParameter("codeDivision");

        String codeSite = request.getParameter(FractionnementConst.ParamName.CodeSite);
        String codeService = request.getParameter(FractionnementConst.ParamName.CodeService);
        String dateDebut = request.getParameter(FractionnementConst.ParamName.DateDebut);
        String dateFin = request.getParameter(FractionnementConst.ParamName.DateFin);

        codeService = codeDivision;

        try {

            listDemandeEchelonnements = new ArrayList<>();

            switch (modeSearch.trim()) {
                case GeneralConst.Number.ONE:
                    listDemandeEchelonnements = FractionnementBusiness.getListDemandeFractionnements(typeSearch, valueSearch);
                    break;
                case GeneralConst.Number.TWO:
                    listDemandeEchelonnements = FractionnementBusiness.getListDemandeFractionnementsAdvanced(
                            codeService, codeSite, dateDebut, dateFin.trim());
                    break;
            }

            listDeJson = new ArrayList<>();
            Personne personne;
            List<JsonObject> listJsonObjectArchive;
            List<JsonObject> listDetailJson;
            List<JsonObject> titreIntercalaireJsonList;
            List<JsonObject> titreIntercalaireJsonListOfOrdonnancement = new ArrayList<>();
            boolean isEcheanceEchus;

            for (DemandeEchelonnement de : listDemandeEchelonnements) {

                personne = new Personne();
                deJson = new JsonObject();
                listDetailJson = new ArrayList<>();
                listJsonObjectArchive = new ArrayList<>();

                deJson.addProperty(FractionnementConst.ParamName.DemandeEchelonnementCode, de.getCode().trim());
                deJson.addProperty(FractionnementConst.ParamName.StateDemandeEchelonnement, de.getEtat());
                deJson.addProperty(FractionnementConst.ParamName.LetterReference, de.getReferenceLettre().toUpperCase().trim());
                deJson.addProperty(FractionnementConst.ParamName.DateCreateDe, Tools.formatDateToString(de.getDateCreat()));

                Agent agent = GeneralBusiness.getAgentByCode(de.getAgentCreat().toString());

                deJson.addProperty(FractionnementConst.ParamName.AgentCreate, agent != null ? agent.toString() : GeneralConst.DASH_SEPARATOR);

                List<ArchiveAccuseReception> listArchives = TaxationBusiness.getListArchiveAccuseByIdDocumenet(de.getCode());

                for (ArchiveAccuseReception arch : listArchives) {

                    JsonObject jsonObjectArchive = new JsonObject();

                    jsonObjectArchive.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT,
                            arch.getArchive() == null ? GeneralConst.EMPTY_STRING : arch.getArchive());
                    jsonObjectArchive.addProperty(TaxationConst.ParamName.NUMERO_NC,
                            arch.getDocumentReference() == null ? GeneralConst.EMPTY_STRING : arch.getDocumentReference());

                    if (arch.getObservation() != null && !arch.getObservation().isEmpty()) {
                        jsonObjectArchive.addProperty(ControleConst.ParamName.OBSERVATION_DOC, arch.getObservation());
                    } else {
                        jsonObjectArchive.addProperty(ControleConst.ParamName.OBSERVATION_DOC, GeneralConst.EMPTY_STRING);
                    }

                    if (arch.getFkDocument() != null && !arch.getFkDocument().isEmpty()) {
                        TypeDocument typeDocument = GeneralBusiness.getTypeDocumentByCode(arch.getFkDocument().trim());
                        if (typeDocument != null) {
                            jsonObjectArchive.addProperty(FractionnementConst.ParamName.LIBELLE_DOCUMENT, typeDocument.getIntitule());
                        } else {
                            jsonObjectArchive.addProperty(FractionnementConst.ParamName.LIBELLE_DOCUMENT, GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        jsonObjectArchive.addProperty(FractionnementConst.ParamName.LIBELLE_DOCUMENT, GeneralConst.EMPTY_STRING);
                    }

                    listJsonObjectArchive.add(jsonObjectArchive);
                }

                deJson.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT_LIST, listJsonObjectArchive.toString());

                for (DetailEchelonnement detailEchelonnement : de.getDetailEchelonnementList()) {

                    titreIntercalaireJsonList = new ArrayList<>();
                    titreIntercalaireJsonListOfOrdonnancement = new ArrayList<>();
                    JsonObject titreJsonObject = new JsonObject();

                    String typeDoc = detailEchelonnement.getTypeTitre();
                    String documentReference = detailEchelonnement.getNumeroTitre();

                    titreJsonObject.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT, documentReference);
                    titreJsonObject.addProperty("referenceDocumentManuel", documentReference);
                    titreJsonObject.addProperty(FractionnementConst.ParamName.Etat, detailEchelonnement.getEtat());

                    titreJsonObject.addProperty(FractionnementConst.ParamName.AgentValidation,
                            detailEchelonnement.getAgentValidation() != null
                                    ? detailEchelonnement.getAgentValidation().toString()
                                    : GeneralConst.EMPTY_STRING);

                    titreJsonObject.addProperty(FractionnementConst.ParamName.DateValidation,
                            detailEchelonnement.getDateValidation() != null
                                    ? Tools.formatDateToString((Tools.formatStringFullToDate(
                                                    detailEchelonnement.getDateValidation())))
                                    : GeneralConst.EMPTY_STRING);

                    titreJsonObject.addProperty(FractionnementConst.ParamName.OBSERVATION,
                            detailEchelonnement.getObservation() != null
                                    ? detailEchelonnement.getObservation()
                                    : GeneralConst.EMPTY_STRING);

                    Journal journal;

                    if (typeDoc.equals(DocumentConst.DocumentCode.NP) || typeDoc.equals(DocumentConst.DocumentCode.NP_P)) {

                        isEcheanceEchus = false;

                        NotePerception notePerception = NotePerceptionBusiness.getNotePerceptionByCode(documentReference);

                        if (notePerception != null) {

                            String code = notePerception.getNoteCalcul().getPersonne();
                            personne = IdentificationBusiness.getPersonneByCode(code);

                            Date dateCreateNP = Tools.formatStringFullToDate(notePerception.getDateCreat());
                            Date dateEcheanceNP = Tools.formatStringFullToDate(notePerception.getDateEcheancePaiement());

                            if (Compare.before(dateEcheanceNP, new Date())) {
                                isEcheanceEchus = true;
                            }

                            titreJsonObject.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT_MANUEL, notePerception.getNumero().trim());
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, notePerception.getNetAPayer());
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, notePerception.getDevise());
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceEchus);
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_CREAT, Tools.formatDateToString(dateCreateNP));
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, Tools.formatDateToString(dateEcheanceNP));

                            Service service = TaxationBusiness.getServiceByCode(notePerception.getNoteCalcul().getService());
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.SERVICE, service.getIntitule());

                            titreJsonObject.addProperty(GeneralConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);//notePerception.getNoteCalcul().getSite().getFkEntite
                            titreJsonObject.addProperty(GeneralConst.ParamName.CODE_SITE, notePerception.getNoteCalcul().getSite());
                            //titreJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, notePerception.getNoteCalcul().getSite().getFkEntite().getFkProvince().getId());
                            titreJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, GeneralConst.EMPTY_STRING);
                            titreJsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE, service.getCode());

                            for (NotePerception npIntercalaire : notePerception.getNotePerceptionList()) {

                                isEcheanceEchus = false;
                                JsonObject intercalaireJsonObject = new JsonObject();

                                if (Compare.before(Tools.formatStringFullToDate(npIntercalaire.getDateEcheancePaiement()), new Date())) {
                                    isEcheanceEchus = true;
                                }

                                journal = PaiementBusiness.getJournalByDocument(npIntercalaire.getNumero(), false);

                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.NP);
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ASSUJETTI, npIntercalaire.getNoteCalcul().getPersonne().toUpperCase());

                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_INTERET, false);
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO, npIntercalaire.getNumero());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO_MANUEL, npIntercalaire.getNumero());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, npIntercalaire.getNetAPayer());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, npIntercalaire.getDevise());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE, npIntercalaire.getNpMere().getNumero());

                                intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                                intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_SITE, npIntercalaire.getNoteCalcul().getSite());
                                //intercalaireJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, npIntercalaire.getNoteCalcul().getSite().getFkEntite().getFkProvince().getId());
                                intercalaireJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, GeneralConst.EMPTY_STRING);
                                intercalaireJsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE, npIntercalaire.getNoteCalcul().getService());

                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE_MANUEL, npIntercalaire.getNpMere().getNumero().trim());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ETAT_ECHELONNEMENT, detailEchelonnement.getEtat());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceEchus);
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, Tools.formatDateToString(Tools.formatStringFullToDate(npIntercalaire.getDateEcheancePaiement())));
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT_V2, Tools.formatDateToString(Tools.formatStringFullToDate(npIntercalaire.getDateEcheancePaiement())).replace("/", "_"));
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_PAID, journal != null);
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal != null ? journal.getEtat() == GeneralConst.Numeric.ONE : false);

                                titreIntercalaireJsonList.add(intercalaireJsonObject);
                            }

                            titreIntercalaireJsonListOfOrdonnancement.addAll(titreIntercalaireJsonList);

//                            if (!notePerception.getFkTypeTitrePerception().getCode().equals(GeneralConst.Numeric.THREE)) {
                            String articleName = Tools.getArticleByNC(notePerception.getNoteCalcul());
                            String articleCode = Tools.getCodeBudgetaireByNC(notePerception.getNoteCalcul());

                            titreJsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, articleName);
                            titreJsonObject.addProperty(TaxationConst.ParamName.CODE_BUDGETAIRE, articleCode.toUpperCase());
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.NP);

                            List<BonAPayer> bapsInteret = NotePerceptionBusiness.getBpInteretByNP(notePerception.getNumero());

                            BonAPayer bonAPayerInteret = !bapsInteret.isEmpty() ? bapsInteret.get(GeneralConst.Numeric.ZERO) : null;

                            if (bonAPayerInteret != null) {

                                JsonObject intercalaireJsonObject = new JsonObject();

                                boolean isEcheanceBpInteretEchus = false;

                                if (Compare.before(bonAPayerInteret.getDateEcheance(), new Date())) {
                                    isEcheanceBpInteretEchus = true;
                                }

                                journal = PaiementBusiness.getJournalByDocument(bonAPayerInteret.getCode(), false);

                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.BP);
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO, bonAPayerInteret.getCode());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO_MANUEL, bonAPayerInteret.getCode());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_INTERET, true);
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, bonAPayerInteret.getMontant());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, "CDF");
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE, notePerception.getNumero());

                                intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                                intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_SITE, bonAPayerInteret.getFkNoteCalcul().getSite());
                                intercalaireJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, GeneralConst.EMPTY_STRING);
                                intercalaireJsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE, GeneralConst.EMPTY_STRING);

                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE_MANUEL, notePerception.getNumero());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ETAT_ECHELONNEMENT, detailEchelonnement.getEtat());
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceBpInteretEchus);
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, Tools.formatDateToString(bonAPayerInteret.getDateEcheance()));
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT_V2, Tools.formatDateToString(bonAPayerInteret.getDateEcheance()).replace("/", "_"));
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_PAID, journal != null);
                                intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal != null ? journal.getEtat() == GeneralConst.Numeric.ONE : false);

                                titreIntercalaireJsonList.add(intercalaireJsonObject);
                            }

                            titreJsonObject.addProperty(FractionnementConst.ParamName.listIntercalaireDetail, titreIntercalaireJsonList.toString());

                            listDetailJson.add(titreJsonObject);
                        }

                    } else {

                        RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByCode(documentReference);

                        isEcheanceEchus = false;

                        if (retraitDeclaration != null) {

                            personne = DeclarationBusiness.getPersonneByCode(retraitDeclaration.getFkAssujetti());

                            if (Compare.before(retraitDeclaration.getDateEcheancePaiement(), new Date())) {
                                isEcheanceEchus = true;
                            }

                            BigDecimal amountRD = new BigDecimal("0");

                            if (retraitDeclaration.getNewId() != null) {

                                List<RetraitDeclaration> listRetraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID(retraitDeclaration.getNewId());

                                if (!listRetraitDeclarations.isEmpty()) {

                                    for (RetraitDeclaration rd : listRetraitDeclarations) {
                                        amountRD = amountRD.add(rd.getMontant());
                                    }
                                }
                            } else {
                                amountRD = amountRD.add(retraitDeclaration.getMontant());
                            }

                            Site site = TaxationBusiness.getSiteByCode(retraitDeclaration.getFkSite());

                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, amountRD);

                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, retraitDeclaration.getDevise());
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceEchus);
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_CREAT, Tools.formatDateToString(retraitDeclaration.getDateCreate()));
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, Tools.formatDateToString(retraitDeclaration.getDateEcheancePaiement()));
                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.SERVICE, (site == null) ? GeneralConst.EMPTY_STRING : site.getIntitule().toUpperCase());

                            titreJsonObject.addProperty(GeneralConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                            titreJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, GeneralConst.EMPTY_STRING);
                            titreJsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE, GeneralConst.EMPTY_STRING);

                            ArticleBudgetaire articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(retraitDeclaration.getFkAb());

                            titreJsonObject.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                    articleBudgetaire != null ? articleBudgetaire.getIntitule() : GeneralConst.EMPTY_STRING);

                            titreJsonObject.addProperty(TaxationConst.ParamName.CODE_BUDGETAIRE,
                                    articleBudgetaire != null ? articleBudgetaire.getCodeOfficiel().toUpperCase() : GeneralConst.EMPTY_STRING);

                            titreJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.NT);

                            List<RetraitDeclaration> listTaxationEchelonnement = FractionnementBusiness.getListRetraitDeclarationEchelonnes(retraitDeclaration.getId());

                            if (!listTaxationEchelonnement.isEmpty()) {

                                for (RetraitDeclaration rdEchelonnement : listTaxationEchelonnement) {

                                    isEcheanceEchus = false;

                                    JsonObject intercalaireJsonObject = new JsonObject();
                                    Personne personneEchelonnement = IdentificationBusiness.getPersonneByCode(rdEchelonnement.getFkAssujetti());
                                    
                                    if (Compare.before(rdEchelonnement.getDateEcheancePaiement(), new Date())) {
                                        isEcheanceEchus = true;
                                    }

                                    journal = PaiementBusiness.getJournalByDocument(rdEchelonnement.getId() + "", false);

                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.NT);
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ASSUJETTI, personneEchelonnement.toString().toUpperCase());

                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_INTERET, false);
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO, rdEchelonnement.getId());
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO_MANUEL, rdEchelonnement.getId());
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, rdEchelonnement.getMontant());
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, rdEchelonnement.getDevise());
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE, rdEchelonnement.getRetraitDeclarationMere());

                                    intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                                    intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_SITE, rdEchelonnement.getFkSite());
                                    intercalaireJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, GeneralConst.EMPTY_STRING);
                                    intercalaireJsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE, rdEchelonnement.getFkSite());

                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE_MANUEL, rdEchelonnement.getRetraitDeclarationMere());
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ETAT_ECHELONNEMENT, detailEchelonnement.getEtat());
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceEchus);
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, Tools.formatDateToString(rdEchelonnement.getDateEcheancePaiement()));
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT_V2, Tools.formatDateToString(rdEchelonnement.getDateEcheancePaiement()).replace("/", "_"));
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_PAID, journal != null);
                                    intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal != null ? journal.getEtat() == GeneralConst.Numeric.ONE : false);

                                    titreIntercalaireJsonList.add(intercalaireJsonObject);
                                }

                            }

                            titreJsonObject.addProperty(FractionnementConst.ParamName.listIntercalaireDetail, titreIntercalaireJsonList.toString());

                            listDetailJson.add(titreJsonObject);
                        }
                    }
                }

                deJson.addProperty(FractionnementConst.ParamName.Observation, de.getObservation());
                deJson.addProperty(FractionnementConst.ParamName.AssujettiName, personne != null ? personne.toString().toUpperCase() : GeneralConst.EMPTY_STRING);
                deJson.addProperty(IdentificationConst.ParamName.USER_NAME,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());
                Adresse adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                        personne != null ? personne.getCode().trim() : GeneralConst.EMPTY_STRING);

                deJson.addProperty(FractionnementConst.ParamName.AdresseName, adresse != null ? adresse.toString().toUpperCase().trim() : GeneralConst.EMPTY_STRING);
                deJson.addProperty(FractionnementConst.ParamName.DetailEchelonnement, listDetailJson.toString());
                deJson.addProperty(FractionnementConst.ParamName.TitreEchelonnementForOrdonnancement, titreIntercalaireJsonListOfOrdonnancement.toString());

                listDeJson.add(deJson);
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return listDeJson.toString();
    }

//    private String printerLetterNotificationAvisEchelonnement(
//            String codeDE,
//            List<NotePerception> listNpFille,
//            String deviseNpFille) {
//
//        String dataReturn = GeneralConst.EMPTY_STRING;
//        PrintDocument printDocument = new PrintDocument();
//
//        try {
//
//            demandeEchelonnement = new DemandeEchelonnement();
//
//            demandeEchelonnement = EchelonnementBusiness.getDemandeEchelonnementByCode(codeDE.trim());
//
//            if (demandeEchelonnement != null) {
//
//                dataReturn = printDocument.createLettreAvisEchelonnement(
//                        demandeEchelonnement,
//                        listNpFille,
//                        deviseNpFille);
//            }
//
//        } catch (NumberFormatException | InvocationTargetException e) {
//            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
//        }
//
//        return dataReturn;
//    }
//
    public String saveDemandeFractionnement(HttpServletRequest request) {

        String dataRetutn;

        String titreList = request.getParameter(FractionnementConst.ParamName.listTitreJson);
        String referenceLetter = request.getParameter(FractionnementConst.ParamName.LetterReference);
        String codePersonne = request.getParameter(FractionnementConst.ParamName.CodePersonne);
        String observation = request.getParameter(FractionnementConst.ParamName.Observation);
        String userId = request.getParameter(FractionnementConst.ParamName.UserId);
        String archives = request.getParameter(TaxationConst.ParamName.ARCHIVES);
        String fkDocument = request.getParameter(FractionnementConst.ParamName.FK_DOCUMENT);
        String observationDoc = request.getParameter(FractionnementConst.ParamName.OBSERVATION_DOCUMENT);
        String divisionCode = request.getParameter("divisionCode");

        try {

            List<String> documentList = new ArrayList<>();
            JSONArray jsonDocumentArray = null;
            JSONArray jsonDetailArray = null;

            if (archives != null && !archives.isEmpty()) {

                jsonDocumentArray = new JSONArray(archives);

                for (int i = 0; i < jsonDocumentArray.length(); i++) {
                    JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                    documentList.add(jsonObject.getString(FractionnementConst.ParamName.PV_DOCUMENT));
                }
            }

            if (titreList != null && !titreList.isEmpty()) {
                jsonDetailArray = new JSONArray(titreList);
            }

            demandeEchelonnement = new DemandeEchelonnement();
            demandeEchelonnement.setFkNp(null);
            demandeEchelonnement.setReferenceLettre(referenceLetter.trim());
            demandeEchelonnement.setObservation(observation.trim());
            demandeEchelonnement.setAgentCreat(Integer.valueOf(userId));
            demandeEchelonnement.setFkDivision(divisionCode);

//            saveLogUserInfo(request);
            dataRetutn = TaxationBusiness.saveDemandeFractionnement(demandeEchelonnement, jsonDetailArray, documentList, fkDocument, observationDoc, codePersonne);

            if (!dataRetutn.isEmpty()) {
                dataRetutn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataRetutn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataRetutn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataRetutn;
    }

//    public String loadArchive(HttpServletRequest request) {
//
//        String dataRetutn = null;
//        listArchiveAccuseReception = new ArrayList<>();
//        try {
//
//            String npMere = request.getParameter(EchelonnementConst.ParamName.NUMERO_TITRE);
//            listArchiveAccuseReception = EchelonnementBusiness.getListArchiveByReference(npMere);
//
//            if (!listArchiveAccuseReception.isEmpty()) {
//
//                listJsonObjectArchAcc = new ArrayList<>();
//
//                for (ArchiveAccuseReception archAcc : listArchiveAccuseReception) {
//                    jsonObjectArchAcc = new JsonObject();
//                    jsonObjectArchAcc.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT, archAcc.getArchive());
//
//                    if (!archAcc.getObservation().isEmpty()) {
//                        jsonObjectArchAcc.addProperty(ControleConst.ParamName.OBSERVATION_DOC, archAcc.getObservation());
//                    } else {
//                        jsonObjectArchAcc.addProperty(ControleConst.ParamName.OBSERVATION_DOC, GeneralConst.EMPTY_STRING);
//                    }
//
//                    listJsonObjectArchAcc.add(jsonObjectArchAcc);
//                }
//
//            } else {
//                return GeneralConst.ResultCode.FAILED_OPERATION;
//            }
//
//        } catch (Exception e) {
//            CustumException.LogException(e);
//            dataRetutn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
//        }
//        return listJsonObjectArchAcc.toString();
//    }
//
//    private void saveLogUserInfo(HttpServletRequest request) {
//        logUser = new LogUser(request, valueOperation);
//    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// 

}
