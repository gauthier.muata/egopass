package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Devise;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Taux.class)
public class Taux_ { 

    public static volatile SingularAttribute<Taux, String> agentMaj;
    public static volatile SingularAttribute<Taux, Date> dateCreat;
    public static volatile SingularAttribute<Taux, BigDecimal> taux;
    public static volatile SingularAttribute<Taux, Integer> id;
    public static volatile SingularAttribute<Taux, String> agentCreat;
    public static volatile SingularAttribute<Taux, Devise> devise;
    public static volatile SingularAttribute<Taux, String> deviseDest;
    public static volatile SingularAttribute<Taux, Date> dateMaj;

}