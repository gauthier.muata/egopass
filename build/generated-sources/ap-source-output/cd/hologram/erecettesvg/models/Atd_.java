package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Atd.class)
public class Atd_ { 

    public static volatile SingularAttribute<Atd, String> agentMaj;
    public static volatile SingularAttribute<Atd, Date> dateCreat;
    public static volatile SingularAttribute<Atd, BigDecimal> montantFonctionnement;
    public static volatile SingularAttribute<Atd, Short> etat;
    public static volatile SingularAttribute<Atd, BigDecimal> montantGlobal;
    public static volatile SingularAttribute<Atd, Date> dateMaj;
    public static volatile SingularAttribute<Atd, String> idCommandement;
    public static volatile SingularAttribute<Atd, String> numeroDocument;
    public static volatile SingularAttribute<Atd, String> idRedevable;
    public static volatile SingularAttribute<Atd, String> dateEmission;
    public static volatile SingularAttribute<Atd, BigDecimal> montantPenalite;
    public static volatile SingularAttribute<Atd, BigDecimal> montantPrincipal;
    public static volatile SingularAttribute<Atd, String> id;
    public static volatile SingularAttribute<Atd, String> agentCreat;
    public static volatile SingularAttribute<Atd, String> idDetenteur;

}