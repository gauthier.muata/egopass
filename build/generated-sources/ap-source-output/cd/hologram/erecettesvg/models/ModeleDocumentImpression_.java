package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ModeleDocumentImpression.class)
public class ModeleDocumentImpression_ { 

    public static volatile SingularAttribute<ModeleDocumentImpression, String> modele;
    public static volatile SingularAttribute<ModeleDocumentImpression, String> description;
    public static volatile SingularAttribute<ModeleDocumentImpression, String> typeDocument;
    public static volatile SingularAttribute<ModeleDocumentImpression, Integer> id;
    public static volatile SingularAttribute<ModeleDocumentImpression, Short> etat;

}