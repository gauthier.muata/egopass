/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class RecouvrementConst {

    public class ParamName {

        public final static String TYPE_DOCUMENT = "typeDocument";
        public final static String DOCUMENT_REFERENCE = "documentReference";
        public final static String operation = "operation";
        public final static String PERIODE_DECLARATION_ID = "periodeDeclarationId";
        public final static String ARTICLE_BUDGETAIRE_CODE = "articleBudgetaireCode";
        public final static String ARTICLE_BUDGETAIRE_NAME = "articleBudgetaireName";
        public final static String ASSUJETTI_CODE = "assujettiCode";
        public final static String ASSUJETTI_NAME = "assujettiName";
        public final static String ASSUJETTI_NAME_COMPOSITE = "assujettiNameComposite";
        public final static String PERIODE_DECLARATION = "periodeDeclaration";
        public final static String ECHEANCE_DECLARATION = "echeanceDeclaration";
        public final static String ADVANCED_SEARCH = "advancedSearch";
        public final static String ADRESSE_CODE = "adresseCode";
        public final static String ADRESSE_NAME = "adresseName";
        public final static String PRINT_EXISTS = "printExist";
        public final static String DOCUMENT_PRINT = "documentPrint";
        public final static String AMOUNT_PERIODE_DECLARATION = "amountPeriodeDeclaration";
        public final static String AMOUNT_PERIODE_DECLARATION_TO_STRING = "amountPeriodeDeclarationToString";
        public final static String DEVISE_PERIODE_DECLARATION = "devisePeriodeDeclaration";
        public final static String USER_ID = "idUser";
        public final static String NUMERO_MED = "numeroMed";
        public final static String PERIOD_VALUE_MONTH = "periodValueMonth";
        public final static String PERIOD_VALUE_YEAR = "periodValueYear";
        public final static String CODE_PERIODICITE = "codePeriodicite";
        public final static String DATE_HEURE_INVITATION = "dateHeureInvitation";
        public final static String NUMERO_NP = "numeroNp";
        public final static String NUMERO_NC = "numeroNc";
        public final static String DATE_ORDONNANCEMENT_NC = "dateOrdonnancement";
        public final static String DATE_ECHEANCE_NP = "dateEcheanceNp";
        public final static String AMOUNT_NP = "amountNp";
        public final static String DEVISE_NP = "deviseNp";
        public final static String EXERCICE_NP = "exerciceNp";
        public final static String TYPE_REGISTER = "typeRegister";
        public final static String CRITERE_SEARCH = "critereSearch";
        public final static String AssujettiCode = "assujettiCode";
        public final static String AssujettiName = "assujettiName";

        public final static String DATE_CREATE_EXTRAIT_ROLE = "dateCreateExtraitRole";
        public final static String DATE_RECEPTION_EXTRAIT_ROLE = "dateReceptionExtraitRole";
        public final static String DATE_ECHEANCE_EXTRAIT_ROLE = "dateEcheanceExtraitRole";
        public final static String STATE_EXTRAIT_ROLE = "stateExtraitRole";

        public final static String DESCRIPTION_ROLE = "description";
        public final static String DETAIL_ROLE = "detailRoles";
        public final static String MONTH_SEARCH_START = "monthSearchStart";
        public final static String MONTH_SEARCH_END = "monthSearchEnd";
        public final static String YEAR_SEARCH = "yearSearch";
        public final static String ARTICLE_ROLE = "articleRole";
        public final static String DATE_CREATE_ROLE = "dateCreateRole";

        public final static String DATE_ECHEANCE_MED_OR_MEP = "dateEcheanceMepOrMed";
        public final static String DATE_RECEPTION_MED_OR_MEP = "dateReceptionMepOrMed";
        public final static String DATE_CREATION_MED_OR_MEP = "dateCreationMepOrMed";
        public final static String AGENT_CREATION_MED_OR_MEP = "agentCreationMepOrMed";

        public final static String DATE_DEBUT = "dateDebut";
        public final static String DATE_FIN = "dateFin";
        public final static String CODE_SERVICE = "codeService";
        public final static String CODE_SITE = "codeSite";
        public final static String AMOUNT_NP_1 = "amountNp1";
        public final static String AMOUNT_NP_2 = "amountNp2";

        public final static String ROLE_ID = "roleId";
        public final static String STATE_ROLE = "roleState";

        public final static String OBSERVATION_RECEVEUR = "observationReceveur";
        public final static String OBSERVATION_DIRECTION = "observationDirection";

        public final static String FRAIS_POURSUITE = "fraisPoursuite";
        public final static String FRAIS_POURSUITE_GLOBAL = "fraisPoursuiteGlobal";
        public final static String TOTAL_FRAIS_POURSUITE = "totalFraisPoursuite";
        public final static String TOTAL_PAYE_FRAIS_POURSUITE = "totalPayeFraisPoursuite";

        public final static String OBSERVATION_LIST = "observationList";

        public final static String DETAIL_ROLE_LIST = "detailRoleList";
        public final static String AMOUNT_PENALITE = "amountPenalite";
        public final static String EXTRAIT_ROLE_ID = "extraitRoleId";
        public final static String NIVEAU_EXTRAIT_ROLE = "niveauExtraitRole";

        public final static String EXTRAIT_ROLE_ORDONNANCE = "extraitRoleOrdonnance";

        public final static String TRACKER_LINE_LIST = "trackerLineList";

        public final static String LEVEL_VALUE = "levelValue";
        public final static String LEVEL_DATE = "levelDate";

        public final static String LEVEL_FIVE_VALUE = "levelFive";
        public final static String LEVEL_FIVE_DATE = "levelFiveDate";

        public final static String LEVEL_FOUR_VALUE = "levelFour";
        public final static String LEVEL_FOUR_DATE = "levelFourDate";

        public final static String LEVEL_THREE_VALUE = "levelThree";
        public final static String LEVEL_THREE_DATE = "levelThreeDate";

        public final static String LEVEL_TWO_VALUE = "levelTwo";
        public final static String LEVEL_TWO_DATE = "levelTwoDate";

        public final static String LEVEL_ONE_VALUE = "levelOne";
        public final static String LEVEL_ONE_DATE = "levelOneDate";

        public final static String LEVEL_ZERO_VALUE = "levelZero";
        public final static String LEVEL_ZERO_DATE = "levelZeroDate";
        public final static String ECHEANCE_EXTRAIT_ROLE_DEPASSE = "echeanceExtraitRoleDepasser";

        public final static String CODE_ASSUJETTI = "codeAssujetti";
        public final static String DERNIER_AVERTISSEMENT_ID = "daId";
        public final static String HUISSIER_ID = "huissierId";

        public final static String FRAIS_POURSUITE_COMMANDEMENT = "FRAIS_POURSUITE_COMMANDEMENT";
        public final static String FRAIS_POURSUITE_ATD = "FRAIS_POURSUITE_ATD";
        public final static String FRAIS_POURSUITE_SAISIE = "FRAIS_POURSUITE_SAISIE";

        public final static String CONTRAINTE_ID = "contrainteId";
        public final static String STATE_CONTRAINTE = "stateContrainte";
        public final static String DATE_RECEPTION_CONTRAINTE = "dateReceptionContrainte";
        public final static String DATE_ECHEANCE_CONTRAINTE = "dateEcheanceContrainte";
        public final static String DATE_CREATE_CONTRAINTE = "dateCreateContrainte";

        public final static String NOM_PERSONNE_TIERS = "nomPersonneTiers";
        public final static String FONCTION_PERSONNE_TIERS = "fonctionPersonneTiers";

    }

    public class ParamQuery {

        public final static String EXTRAIT_ROLE_ID = "EXTRAIT_ROLE_ID";
        public final static String ROLE_ID = "ROLE_ID";
        public final static String PERIODE_DEBUT = "PERIODE_DEBUT";
        public final static String PERIODE_FIN = "PERIODE_FIN";
        public final static String AGENT_CREAT = "AGENT_CREAT";
        public final static String DESCRIPTION = "DESCRIPTION";
        public final static String OBSERVATION_RECEVEUR = "OBSERVATION RECEVEUR";
        public final static String OBSERVATION_DIRECTION = "OBSERVATION DIRECTION";
        public final static String MAC_ADRESSE = "MAC_ADRESSE";
        public final static String IP_ADRESSE = "IP_ADRESSE";
        public final static String HOST_NAME = "HOST_NAME";
        public final static String FK_EVENEMENT = "FK_EVENEMENT";
        public final static String CONTEXT_BROWSER = "CONTEXT_BROWSER";
        public final static String ARTICLE_ROLE = "ARTICLE_ROLE";
        public final static String DATE_RECEPTION = "DATE_RECEPTION";
        public final static String DATE_ECHEANCE = "DATE_ECHEANCE";
        public final static String FK_TYPE_DOCUMENT = "FK_TYPE_DOCUMENT";
        public final static String PERSONNE = "PERSONNE";
        public final static String FK_ROLE = "FK_ROLE";

        public final static String T_EXTRAIT_DE_ROLE = "T_EXTRAIT_DE_ROLE";

        public final static String NC_RETURN = "NC_RETURN";
        public final static String ADRESSE = "ADRESSE";
        public final static String EXERCICE = "EXERCICE";
        public final static String SITE = "SITE";
        public final static String SERVICE = "SERVICE";
        public final static String REFERENCE_PROCEDURE = "REFERENCE_PROCEDURE";
        public final static String TYPE_PROCEDURE = "TYPE_PROCEDURE";
        public final static String ETAT = "ETAT";
        public final static String TARIF = "TARIF";
        public final static String ARTICLE_BUDGETAIRE = "ARTICLE_BUDGETAIRE";
        public final static String MONTANT_PENALITE = "MONTANT_PENALITE";
        public final static String MONTANT_PRINCIPAL = "MONTANT_PRINCIPAL";
        public final static String NC_OLD = "NC_OLD";
        public final static String DEVISE = "DEVISE";
        public final static String FK_AB_AMENDE = "FK_AB_AMENDE";

        public final static String MOTIF_PENALITE = "MOTIF_PENALITE";
        public final static String FK_COMPTE = "FK_COMPTE";
        public final static String NC = "NC";
        public final static String TOTAL = "TOTAL";
        public final static String NET_A_PAYER = "NET_A_PAYER";
        public final static String PAYER = "PAYER";
        public final static String SOLDE = "SOLDE";
        public final static String NP_OLD = "NP_OLD";
        public final static String NP_RETURN = "NP_RETURN";
    }

    public class Operation {

        public final static String LOAD_DEFAILLANT_DECLARATION = "loadDefaillantDeclaration";
        public final static String LOAD_DEFAILLANT_PAIEMENT = "loadDefaillantPaiement";
        public final static String LOAD_DEFAILLANT_PAIEMENT_ROLE = "loadDefaillantPaiementRole";
        public final static String PRINT_MED = "printMed";
        public final static String PRINT_MEP = "printMep";
        public final static String PRINT_INVITATION_SERVICE = "printInvitationService";
        public final static String SAVE_ROLE = "1401";
        public final static String LOAD_ROLE = "loadRole";
        public final static String VALIDER_ROLE = "validateRole";
        public final static String VALIDER_ROLE_BY_RECEVEUR = "1408";
        public final static String VALIDER_ROLE_BY_DIRECTION = "1410";
        public final static String ORDONNANCER_EXTRAIT_ROLE = "1409";
        public final static String LOAD_EXTRAIT_ROLE = "loadExtraitRole";

        public final static String LOAD_EXTRAIT_ROLE_BY_ROLE = "loadExtraitRoleByRole";

        public final static String LOAD_DERNIER_AVERTISSEMENT = "loadDernierAvertissement";
        public final static String SAVE_CONTRAINTE = "1404";
        public final static String LOAD_CONTRAINTE = "loadContrainte";
        public final static String SAVE_COMMANDEMENT = "1405";
        public final static String SAVE_DERNIER_AVERTISSEMENT = "1403";
        public final static String PRINTER_PROJECT_ROLE = "printProjectRole";
        public final static String LOAD_INVITE_SERVICE = "loadInvitationService";
        public final static String LOAD_RELANCE = "loadRelance";

        public final static String PRINT_INVITE_SERVICE = "printInviteService";
        public final static String PRINT_RELANCE = "printRelance";

        public final static String TAXATION_OFFICE = "taxationOffice";
    }
}
