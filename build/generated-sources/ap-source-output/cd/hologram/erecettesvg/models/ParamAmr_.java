package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ParamAmr.class)
public class ParamAmr_ { 

    public static volatile SingularAttribute<ParamAmr, String> compteDgrkCdf;
    public static volatile SingularAttribute<ParamAmr, String> compteDgrkUsd;
    public static volatile SingularAttribute<ParamAmr, Integer> id;
    public static volatile SingularAttribute<ParamAmr, String> receveurRecetteFiscale;
    public static volatile SingularAttribute<ParamAmr, Boolean> etat;

}