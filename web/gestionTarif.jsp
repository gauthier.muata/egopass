<%-- 
    Document   : gestionTarif
    Created on : 21 sept. 2020, 08:51:46
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des tarifs</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">

                        <div class="col-lg-12">

                            <div class="form-group">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"> Edition d'un tarif</h4>
                                    </div>
                                </div>

                                <div class="panel panel-primary">

                                    <div class="panel-wrapper collapse in">

                                        <div class="panel-body">

                                            <div class="form-group">
                                                <p class="control-label col-sm-1" for="inputValeurTarif">
                                                    Valeur <span style="color:red"> *</span></p>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="inputValeurTarif" placeholder="Valeur tarif"/>
                                                </div>

                                                <p class="control-label col-sm-2" for="cmbTypeValeutTarif">
                                                    Type valeur <span style="color:red"> *</span>
                                                <div class="col-sm-4">
                                                    <select class="form-control" id="cmbTypeValeutTarif">
                                                        <option value ="0">-- Sélectionner --</option>
                                                        <option value="F">Valeur fixe</option>
                                                        <option value="%">Valeur en pourcentage</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <p class="control-label col-sm-1" for="inputIntituleTarif">
                                                    Intitulé <span style="color:red"> *</span></p>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="inputIntituleTarif" placeholder="Intitulé tarif"/>
                                                </div>
                                                
                                                <p class="control-label col-sm-2" for="cmbTypeTarif">
                                                    Type tarif <span style="color:red"> *</span>
                                                <div class="col-sm-2">
                                                    <select class="form-control" id="cmbTypeTarif">
                                                        <option value ="-1">-- Sélectionner --</option>
                                                        <option value="0">Tarif DRHKAT</option>
                                                        <option value="1">Tarif PEAGE</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="input-group-btn">
                                                    <button class="btn btn-warning pull-right" id="btnInitialiser" style="right: 36%">
                                                        <i class="fa fa-refresh"></i> Initialiser
                                                    </button>
                                                    
                                                    <button class="btn btn-success pull-right" id="btnEnregistrerTarif" style="right: 38%">
                                                        <i class="fa fa-save"></i> Enregistrer
                                                    </button>
                                                    <button class="btn btn-success pull-right" id="btnModifierTarif" style="display: none; right: 40%">
                                                        <i class="fa fa-edit"></i> Modifier
                                                    </button>
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <table width="100%" 
                                   class="table table-bordered table-hover" 
                                   id="tableResultTarifs">
                            </table>

                        </div>
                    </div>
                </form>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>

        <script type="text/javascript" src="assets/js/articleBudgetaire/getionTarif.js"></script>
    </body>
</html>
