package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Acquisition;
import cd.hologram.erecettesvg.models.AcquitLiberatoire;
import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Commande;
import cd.hologram.erecettesvg.models.Complement;
import cd.hologram.erecettesvg.models.DepotDeclaration;
import cd.hologram.erecettesvg.models.FichePriseCharge;
import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.LoginWeb;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.PersonneExemptee;
import cd.hologram.erecettesvg.models.Reclamation;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Personne.class)
public class Personne_ { 

    public static volatile SingularAttribute<Personne, String> postnom;
    public static volatile ListAttribute<Personne, Assujeti> assujetiList;
    public static volatile SingularAttribute<Personne, String> code;
    public static volatile SingularAttribute<Personne, FormeJuridique> formeJuridique;
    public static volatile SingularAttribute<Personne, String> signature;
    public static volatile SingularAttribute<Personne, String> nif;
    public static volatile ListAttribute<Personne, FichePriseCharge> fichePriseChargeList;
    public static volatile SingularAttribute<Personne, String> nom;
    public static volatile SingularAttribute<Personne, Integer> etat;
    public static volatile SingularAttribute<Personne, String> prenoms;
    public static volatile ListAttribute<Personne, PersonneExemptee> personneExempteeList;
    public static volatile ListAttribute<Personne, AdressePersonne> adressePersonneList;
    public static volatile ListAttribute<Personne, Acquisition> acquisitionList;
    public static volatile ListAttribute<Personne, Journal> journalList;
    public static volatile ListAttribute<Personne, Complement> complementList;
    public static volatile ListAttribute<Personne, DepotDeclaration> depotDeclarationList;
    public static volatile SingularAttribute<Personne, String> photoProfil;
    public static volatile ListAttribute<Personne, Commande> commandeList;
    public static volatile SingularAttribute<Personne, byte[]> photo;
    public static volatile ListAttribute<Personne, Reclamation> reclamationList;
    public static volatile SingularAttribute<Personne, byte[]> empreinte;
    public static volatile ListAttribute<Personne, BonAPayer> bonAPayerList;
    public static volatile ListAttribute<Personne, AcquitLiberatoire> acquitLiberatoireList;
    public static volatile SingularAttribute<Personne, LoginWeb> loginWeb;
    public static volatile SingularAttribute<Personne, String> agentCreat;
    public static volatile ListAttribute<Personne, NotePerception> notePerceptionList;

}