/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inputResearchFilter, typeResearch, detailCarteContainer;
var urlId = '', code = '';
var btnResearchCarte;
var globalListData;


$(function () {

    inputResearchFilter = $('#inputResearchFilter');
    inputResearchFilter.attr('placeholder', 'Veuillez saisir le numéro de la carte');

    typeResearch = $('#typeResearch');
    btnResearchCarte = $('#btnResearchCarte');
    detailCarteContainer = $('#detailCarteContainer');

    urlId = getUrlParameter('id');

    if (!isUndefined(urlId)) {

        setTimeout(function () {

            inputResearchFilter.val(urlId);
            btnResearchCarte.trigger('click');

        }, 1000);
    }

    btnResearchCarte.on('click', function (e) {
        e.preventDefault();

        if (inputResearchFilter.val() === empty) {
            alertify.alert('Veuillez fournir un critère de recherche pour continuer');
            return;
        } else {

            getInformationsCarte();

        }

    });

    typeResearch.on('change', function (e) {
        inputResearchFilter.val('');
        if (typeResearch.val() === '1') {
            inputResearchFilter.attr('placeholder', 'Veuillez saisir le numéro de la carte');
        } else if (typeResearch.val() === '2') {
            inputResearchFilter.attr('placeholder', 'Veuillez saisir le nom et prénom du conducteur');
        }
    });

    inputResearchFilter.keypress(function (e) {
        if (e.keyCode === 13) {
            btnResearchCarte.trigger('click');
        }
    });

    btnSimpleSearch = $('#btnSimpleSearch');
    btnAdvencedSearch = $('#btnAdvencedSearch');
});

function getInformationsCarte() {

    $.ajax({
        type: 'POST',
        url: 'trackingCarte_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadInformationsCarte',
            'numero_carte': inputResearchFilter.val(),
            'nom_conducteur': 'loadInformationsCarte',
            'prenom_conducteur': 'loadInformationsCarte',
            'filtre': typeResearch.val()
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {

                $.unblockUI();

                if (response == '-1') {
                    showResponseError();
                    return;
                }

                if (response == '0') {
                    var msgContent = '';
                    msgContent += '<div class="panel panel-succes"><div class="panel-footer" ';
                    msgContent += '<h2 class="panel-title">';
                    msgContent += '<div class="fa fa-warning" style=" font-size: 35px; color:red" class="myLang" key="noFolderFound">CETTE CARTE N\'EST PAS AUTHENTIQUE</div>';
                    msgContent += '</h2></div>';
                    detailCarteContainer.html(msgContent);
                } else {
                    var listData = $.parseJSON(JSON.stringify(response));
                    console.log(listData);
                    panelDetailCarte(listData);
                }

            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            alertify.alert('Une erreur inattendue est survenue. Veuillez réesayer ou contacter l\'administrateur.');
        }
    });
}

function panelDetailCarte(data) {

    var content = '';

    content += '<div style="margin-left:20px;margin-right:20px;" class="panel panel-info">';
    for (var i = 0; i < data.length; i++) {
        var etatCarte;
        if (data[i].carteExpiree == false) {
            etatCarte = '<span style="color:green">(CARTE VALIDE)</span>';
        } else {
            etatCarte = '<span style="color:red">(CARTE EXPIREE)</span>';
        }
        var nomsProprietaire = data[i].prenomProprietaire + ' ' + data[i].nomProprietaire + ' ' + data[i].postnomProprietaire;
        content += '<div class = "panel-heading" style="height:40px;"><h3 style="font-size: 18px; font-weight:bold;" class="panel-title">Carte n°&nbsp;' +
                data[i].idCarte + '&nbsp;' + etatCarte + '<span class="pull-right">Date de fin de validité:&nbsp;' + data[i].dateFinValiditeCarte + '</span></h3></div>';
        content += '<div class="panel-body" style="font-size: 16px;">';
        content += '<ul class="list-group">';
        content += '<li class="list-group-item panel panel-info">';
        content += '<div class="row">';
        content += '<div class="col-md-12" style="font-weight: bolder;color:orange">INFORMATIONS SUR LE CONDUCTEUR</div>';
        content += '</div></li>';
        content += '<li class="list-group-item">';
        content += '<div class="row">';
        content += '<div class="col-md-3">Nom&nbsp;: </div>';
        content += '<div class="col-md-6"><b><i>' + data[i].nomConducteur + '</i></b></div>';
        content += '</div></li>';
        content += '<li class="list-group-item">';
        content += '<div class="row">';
        content += '<div class="col-md-3">Postnom&nbsp;: </div>';
        content += '<div class="col-md-6"><b><i>' + data[i].postnomConducteur + '</i></b></div>';
        content += '</div></li>';
        content += '<li class="list-group-item">';
        content += '<div class="row">';
        content += '<div class="col-md-3">Prénom&nbsp;: </div>';
        content += '<div class="col-md-6"><b><i>' + data[i].prenomConducteur + '</i></b></div>';
        content += '</div></li>';
        content += '<li class="list-group-item">';
        content += '<div class="row">';
        content += '<div class="col-md-3">Date de naissance&nbsp;: </div>';
        content += '<div class="col-md-6"><b><i>' + data[i].dateNaissanceConducteur + '</i></b></div>';
        content += '</div></li>';
        content += '<li class="list-group-item">';
        content += '<div class="row">';
        content += '<div class="col-md-3">Lieu de naissance&nbsp;: </div>';
        content += '<div class="col-md-6"><b><i>' + data[i].lieuNaissanceConducteur + '</i></b></div>';
        content += '</div></li>';
        content += '<li class="list-group-item">';
        content += '<div class="row">';
        content += '<div class="col-md-3">Sexe&nbsp;: </div>';
        content += '<div class="col-md-6"><b><i>' + data[i].sexeConducteur + '</i></b></div>';
        content += '</div></li>';
        content += '<br/>';
        content += '<li class="list-group-item panel panel-info">';
        content += '<div class="row">';
        content += '<div class="col-md-12" style="font-weight: bolder;color:orange">INFORMATIONS SUR LE PROPRIETAIRE</div>';
        content += '</div></li>';
        content += '<li class="list-group-item">';
        content += '<div class="row">';
        content += '<div class="col-md-3">Propriétaire&nbsp;: </div>';
        content += '<div class="col-md-6"><b><i>' + nomsProprietaire + '</i></b></div>';
        content += '</div></li>';

        if (data[i].bienExist == '1') {

            content += '<br/>';
            content += '<li class="list-group-item panel panel-info">';
            content += '<div class="row">';
            content += '<div class="col-md-12" style="font-weight: bolder; color:orange">INFORMATIONS SUR LA MOTO</div>';
            content += '</div></li>';
            content += '<li class="list-group-item">';
            content += '<div class="row">';
            content += '<div class="col-md-3">Type du bien&nbsp;: </div>';
            content += '<div class="col-md-6"><b><i>' + data[i].typeBien + '</i></b></div>';
            content += '</div></li>';
            content += '<li class="list-group-item">';
            content += '<div class="row">';
            content += '<div class="col-md-3">Nom du bien&nbsp;: </div>';
            content += '<div class="col-md-6"><b><i>' + data[i].bien + '</i></b></div>';
            content += '</div></li>';

            var description = 'N° plaque : ' + '<span style="font-weight: bold">' + data[i].IMMATRICULATION.toUpperCase() + '</span>' + '<br/>'
                    + 'Marque : ' + '<span style="font-weight: bold">' + data[i].MARQUE.toUpperCase() + '</span>' + '<br/>' + 'Couleur : '
                    + '<span style="font-weight: bold">' + data[i].COULEUR.toUpperCase() + '</span>' + '<br/>' + 'N° Chassis : ' + '<span style="font-weight: bold">' + data[i].NUMEROCHASIS.toUpperCase() + '</span>' + '<br/>'
                    + 'CV : ' + '<span style="font-weight: bold">' + data[i].CV + '</span>';

            content += '<li class="list-group-item">';
            content += '<div class="row">';
            content += '<div class="col-md-3">Description&nbsp;: </div>';
            content += '<div class="col-md-6"><b><i>' + description + '</i></b></div>';
            content += '</div></li>';

            content += '<li class="list-group-item">';
            content += '<div class="row">';
            content += '<div class="col-md-3">Auto-collant&nbsp;: </div>';
            content += '<div class="col-md-6"><b><i>' + data[i].autocollant + '</i></b></div>';
            content += '</div></li>';

            content += '<li class="list-group-item">';
            content += '<div class="row">';
            content += '<div class="col-md-3">Autorisation de transport&nbsp;: </div>';
            content += '<div class="col-md-6"><b><i>' + data[i].autorisaionTransport + '</i></b></div>';
            content += '</div></li>';

            content += '<li class="list-group-item">';
            content += '<div class="row">';
            content += '<div class="col-md-3">Date de délivrance &nbsp;: </div>';
            content += '<div class="col-md-6"><b><i>' + data[i].dateDebutContrat + '</i></b></div>';


        } else {
            content += '<li class="list-group-item">';
            content += '<div class="row">';
            content += '<div class="col-md-3">Date de délivrance &nbsp;: </div>';
            content += '<div class="col-md-6"><b><i>' + data[i].dateDebutContrat + '</i></b></div>';
        }


        content += '</div></li>';
        content += '</ul>';
        content += '<div class="row">';
        content += '<div class="col-md-3"><button style="display:none" onclick="viewCarte(\'' + data[i].idCarte + '\')" class="btn btn-primary">Visualiser &nbsp;<i class="glyphicon glyphicon-eye-open"></i></a></div>';
        content += '<div class="col-md-6"></div>';
        content += '</div>';
        content += '</div>';
    }
    content += '</div>';
    detailCarteContainer.html(content);
}

function viewCarte(idCarte) {
    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'printCarte',
            'reference': idCarte,
            'isToTracking': '1'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                setDocumentContent(response);
                window.open('impression-carte ', '_blank');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}