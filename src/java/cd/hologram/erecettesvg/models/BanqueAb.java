/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_BANQUE_AB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BanqueAb.findAll", query = "SELECT b FROM BanqueAb b"),
    @NamedQuery(name = "BanqueAb.findById", query = "SELECT b FROM BanqueAb b WHERE b.id = :id"),
    @NamedQuery(name = "BanqueAb.findByFkBanque", query = "SELECT b FROM BanqueAb b WHERE b.fkBanque = :fkBanque"),
    @NamedQuery(name = "BanqueAb.findByEtat", query = "SELECT b FROM BanqueAb b WHERE b.etat = :etat"),
    @NamedQuery(name = "BanqueAb.findByAgentCreat", query = "SELECT b FROM BanqueAb b WHERE b.agentCreat = :agentCreat"),
    @NamedQuery(name = "BanqueAb.findByDateCreat", query = "SELECT b FROM BanqueAb b WHERE b.dateCreat = :dateCreat")})
public class BanqueAb implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 5)
    @Column(name = "FK_BANQUE")
    private String fkBanque;
    @Column(name = "ETAT")
    private Boolean etat;
    @Size(max = 50)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @JoinColumn(name = "FK_AB", referencedColumnName = "CODE")
    @ManyToOne
    private ArticleBudgetaire fkAb;
    @JoinColumn(name = "FK_COMPTE", referencedColumnName = "CODE")
    @ManyToOne
    private CompteBancaire fkCompte;

    public BanqueAb() {
    }

    public BanqueAb(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkBanque() {
        return fkBanque;
    }

    public void setFkBanque(String fkBanque) {
        this.fkBanque = fkBanque;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public ArticleBudgetaire getFkAb() {
        return fkAb;
    }

    public void setFkAb(ArticleBudgetaire fkAb) {
        this.fkAb = fkAb;
    }

    public CompteBancaire getFkCompte() {
        return fkCompte;
    }

    public void setFkCompte(CompteBancaire fkCompte) {
        this.fkCompte = fkCompte;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BanqueAb)) {
            return false;
        }
        BanqueAb other = (BanqueAb) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.BanqueAb[ id=" + id + " ]";
    }
    
}
