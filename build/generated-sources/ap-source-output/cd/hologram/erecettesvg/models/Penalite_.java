package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.DetailsAmr;
import cd.hologram.erecettesvg.models.FichePriseCharge;
import cd.hologram.erecettesvg.models.PalierTauxPenalite;
import cd.hologram.erecettesvg.models.TypePenalite;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Penalite.class)
public class Penalite_ { 

    public static volatile SingularAttribute<Penalite, String> agentMaj;
    public static volatile SingularAttribute<Penalite, Boolean> appliquerMoisRetard;
    public static volatile ListAttribute<Penalite, DetailsAmr> detailsAmrList;
    public static volatile SingularAttribute<Penalite, String> code;
    public static volatile SingularAttribute<Penalite, Date> dateCreat;
    public static volatile ListAttribute<Penalite, FichePriseCharge> fichePriseChargeList;
    public static volatile SingularAttribute<Penalite, String> intitule;
    public static volatile SingularAttribute<Penalite, Short> etat;
    public static volatile SingularAttribute<Penalite, Date> dateMaj;
    public static volatile SingularAttribute<Penalite, Boolean> palier;
    public static volatile ListAttribute<Penalite, PalierTauxPenalite> palierTauxPenaliteList;
    public static volatile SingularAttribute<Penalite, TypePenalite> typePenalite;
    public static volatile SingularAttribute<Penalite, String> agentCreat;
    public static volatile SingularAttribute<Penalite, Boolean> visibilteUtilisateur;

}