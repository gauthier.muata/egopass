package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.TypeBien;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TypeBienService.class)
public class TypeBienService_ { 

    public static volatile SingularAttribute<TypeBienService, Service> service;
    public static volatile SingularAttribute<TypeBienService, Integer> id;
    public static volatile SingularAttribute<TypeBienService, Integer> type;
    public static volatile SingularAttribute<TypeBienService, Short> etat;
    public static volatile SingularAttribute<TypeBienService, TypeBien> typeBien;

}