/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalBienPersonne;

var tableBienPersonne;

var btnSelectAssujetiBien;

var selectAssujettiBien;
var checkExist;

var dataBienTaxation = [];
var bienTaxation = {};
var lblNbreBienSelected;
var bienList = [];

$(function () {

    modalBienPersonne = $('#modalBienPersonne');
    tableBienPersonne = $('#tableBienPersonne');
    btnSelectAssujetiBien = $('#btnSelectAssujetiBien');
    lblNbreBienSelected = $('#lblNbreBienSelected');

    checkExist = false;

    btnSelectAssujetiBien.click(function (e) {

        e.preventDefault();

        if (!checkExist) {

            alertify.alert('Veuillez d\'abord sélectionner au moins un bien');
            return;
        }

        var idMessage = '';

        for (var i = 0; i < dataBienTaxation.length; i++) {

            if (dataBienTaxation[i].isMaster == false) {

                idMessage = $("#message_" + dataBienTaxation[i].bien);

                if (idMessage.val() == empty) {

                    alertify.alert('Pour ce panneau : ' + dataBienTaxation[i].intituleBien.toUpperCase() + ', veuillez d\'abord fournir le message d\'annonce relatif à cette publicité et/ou panneau');
                    return;
                } else {

                    idMessage = $("#message_" + dataBienTaxation[i].bien);
                    dataBienTaxation[i].messageAnnonceur = idMessage.val();
                }

            } else {
                idMessage = $("#message_" + dataBienTaxation[i].bien);
                dataBienTaxation[i].messageAnnonceur = idMessage.val();
            }
        }
    
        alertify.confirm('Etes-vous sûr de vouloir confirmer la sélection de ce(s) bien(s) ?', function () {


            modalBienPersonne.modal('hide');

            taxationWithBien = true;
            dataBienTaxationList = dataBienTaxation;

            selectPersonneBien();

            associatedExist = true;


        });


    });

});

function loadBiensPersonne(personneCode) {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensPersonne',
            'codePersonne': personneCode,
            'codeService': userData.serviceCode,
            'cmbTarif': codeTarif
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var assujettissementList = $.parseJSON(JSON.stringify(response));

                bienList = $.parseJSON(JSON.stringify(assujettissementList.listBiens));

                if (bienList.length > 0) {

                    lblNbreBienSelected.html(0);

                    printBienByPersonne(bienList);
                    modalBienPersonne.modal('show');

                } else {

                    printBienByPersonne('');

                    var valueTarif = '<span style="font-weight:bold">' + tarifName + '</span>';

                    switch (codeBdgetArticle) {
                        case '00000000000002102016':
                            alertify.alert('Cet assujetti ne possède pas de panneau de publicité lié à ce tarif : ' + valueTarif);
                            break;
                        default :
                            alertify.alert('Cet assujetti ne possède pas de bien lié à ce tarif : ' + valueTarif);
                            break;

                    }
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

var isSelect = false;

function printBienByPersonne(bienList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> BIEN </th>';
    header += '<th scope="col">ADRESSE</th>';
    header += '<th scope="col">PROPRIETAIRE</th>';
    header += '<th scope="col">MESSAGE</th>';
    header += '<th scope="col"></th>';
    header += '<th hidden="true" scope="col">Id bien</th>';

    var body = '<tbody>';

    for (var i = 0; i < bienList.length; i++) {


        var genreInfo = 'Nature : ' + '<span style="font-weight:bold">' + bienList[i].libelleTypeBien + '</span>';
        var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + bienList[i].tarifName + '</span>';
        var nameInfo = 'Dénomination : ' + '<span style="font-weight:bold">' + bienList[i].intituleBien + '</span>';

        var descriptionBien = nameInfo + '<br/>' + genreInfo + '<br/>' + categorieInfo + '<hr/>' + bienList[i].complement;


        body += '<tr id="row_' + bienList[i].idBien + '">';
        body += '<td style="vertical-align:middle;width:45px">' + descriptionBien + '</td>';
        body += '<td style="vertical-align:middle;width:15px">' + bienList[i].chaineAdresse.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;width:15px;text-align:left">' + bienList[i].responsable.toUpperCase() + '</td>';

        body += '<td style="text-align:left;width:25%;vertical-align:middle"><textarea rows="5" id="message_' + bienList[i].idBien + '" style="width: 300px"></textarea></th>';

        body += '<td style="text-align:center;width:5%;vertical-align:middle"><input  type="checkbox" id="checkbox_' + bienList[i].idBien + '" onclick="getBienTaxation(\'' + bienList[i].idBien + '\')" /></th>';
        body += '<td hidden="true" style="width:0px">' + bienList[i].idBien + '</td>';
        body += '</tr>';

    }

    body += '</tbody>';

    var tableContent = header + body;

    tableBienPersonne.html(tableContent);

    var dtBien = tableBienPersonne.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher le bien ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableBienPersonne tbody').on('click', 'tr', function () {
        var data = dtBien.row(this).data();
        isSelect = true;
        selectAssujettiBien = {};
        selectAssujettiBien.id = data[3];
        selectAssujettiBien.libelle = data[0];
    });
}

function printBienByPersonne2(bienList) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">BIEN </th>';
    tableContent += '<th style="text-align:center"> EST SOUS CONTRAT ? </th>';
    tableContent += '<th style="text-align:center"></th>';
    tableContent += '<th hidden="true" scope="col">Id bien</th>';
    tableContent += '<th hidden="true" scope="col">Intitule</th>';
    tableContent += '<th hidden="true" scope="col">Description</th>';
    tableContent += '<th hidden="true" scope="col">Adresse</th>';
    tableContent += '<th hidden="true" scope="col">ContratExist</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < bienList.length; i++) {

        var valueCheck = 'checked';
        var buttonDeleteContract = '';
        var buttonDisplayInfoContract = '';
        var buttonAction = '';

        if (bienList[i].contratExist === '0') {
            valueCheck = empty;
        } else {

            if (controlAccess('SET_CONTRACT_MOTO')) {
                buttonDeleteContract = '<button title="Cliquez ici pour casser le contrat d\'exploitation de cette moto" class="btn btn-danger" onclick="dissociateContrat(\'' + bienList[i].contratId + '\',\'' + bienList[i].codePersonne + '\')"><i class="fa fa-remove"></i>&nbsp;Casser</button>'
            }

            if (controlAccess('VIEW_CARTE')) {

                buttonDisplayInfoContract = '<br/><br/>' + '<button title="Cliquez ici pour afficher plus d\'informations sur le contrat d\'exploitation de cette moto" class="btn btn-warning" onclick="displayInfoContrat(\'' + bienList[i].numeroCpi + '\')"><i class="fa fa-list"></i>&nbsp;Voir plus</button>'
            }

            buttonAction = buttonDeleteContract + '' + buttonDisplayInfoContract;
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:36%;vertical-align:middle">' + bienList[i].intituleBienComposite + '</td>';
        tableContent += '<td style="text-align:center;width:4%;vertical-align:middle"><input class="form-check-input position-static" type="checkbox" id="checkbox" disabled ' + valueCheck + '></td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonAction + '</td>';
        tableContent += '<td hidden="true" style="width:0px">' + bienList[i].idBien + '</td>';
        tableContent += '<td hidden="true" style="width:0px">' + bienList[i].intituleBien + '</td>';
        tableContent += '<td hidden="true" style="width:0px">' + bienList[i].descriptionBien + '</td>';
        tableContent += '<td hidden="true" style="width:0px">' + bienList[i].adresseBien + '</td>';
        tableContent += '<td hidden="true" style="width:0px">' + bienList[i].contratExist + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableBienPersonne.html(tableContent);
    var dtBien = tableBienPersonne.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 25,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableBienPersonne tbody').on('click', 'tr', function () {
        var data = dtBien.row(this).data();

        isSelect = true;
        selectAssujettiBien = {};
        selectAssujettiBien.id = data[3];
        selectAssujettiBien.libelle = data[4];
        selectAssujettiBien.description = data[5];
        selectAssujettiBien.adresse = data[6];
        selectAssujettiBien.contratExist = data[7];

    });
}

function displayInfoContrat(cpi) {

    alertify.confirm('Etes-vous sûre de vouloir visualiser plus d\'informations relative au contrat d\'exploitation de cette moto ? <br/><br/> Cette action vas vous rédirigez vers le registre des cartes.', function () {
        window.location = 'registre-carte?cpi=' + btoa(cpi);
    });

}

function dissociateContrat(id, responsableId) {

    alertify.confirm('Etes-vous sûre de vouloir casser ce contrat d\'exploitation de la moto ?', function () {

        $.ajax({
            type: 'POST',
            url: 'carteConducteur_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: true,
            data: {
                'operation': 'dissociateContrat',
                'contratId': id,
                'userId': userData.idUser
            },
            beforeSend: function () {
                $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Dissociation en cours ...</h5>'});
            },
            success: function (response)
            {
                $.unblockUI();

                if (response == '-1' || response == '0') {
                    $.unblockUI();
                    showResponseError();
                    return;
                } else {

                    alertify.alert('Le contrat d\'exploitation est dissocié avec succès');

                    setTimeout(function () {

                        loadBiensPersonne2(responsableId);

                    }
                    , 1);

                }


            },
            complete: function () {

            },
            error: function (xhr, status, error) {
                $.unblockUI();
                showResponseError();
            }

        });
    });

}

function loadBiensPersonne2(personneCode) {

    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensPersonne2',
            'codePersonne': personneCode
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                var assujettissementList = $.parseJSON(JSON.stringify(response));
                var bienList = $.parseJSON(JSON.stringify(assujettissementList.listBiens));
                printBienByPersonne2(bienList);
                modalBienPersonne.modal('show');
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadBiensPersonne3(personneCode) {

    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadBiensMoto',
            'codePersonne': personneCode
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                var assujettissementList = $.parseJSON(JSON.stringify(response));
                var bienList = $.parseJSON(JSON.stringify(assujettissementList.listBiens));
                printBienByPersonne2(bienList);
                modalBienPersonne.modal('show');
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getBienTaxation(id) {

    var row = $("#row_" + id);
    var checkBox = document.getElementById("checkbox_" + id);



    if (checkBox.checked) {

        row.attr('style', 'background-color:#daf2c9;color:black');

        checkExist = true;

        for (var j = 0; j < bienList.length; j++) {

            if (bienList[j].idBien == id) {

                bienTaxation = {};

                bienTaxation.bien = id;
                bienTaxation.libelleTypeBien = bienList[j].libelleTypeBien;
                bienTaxation.tarifName = bienList[j].tarifName;
                bienTaxation.chaineAdresse = bienList[j].chaineAdresse;
                bienTaxation.complement = bienList[j].complement;
                bienTaxation.intituleBien = bienList[j].intituleBien;
                bienTaxation.messageAnnonceur = '';
                bienTaxation.isMaster = bienList[j].isMaster

                dataBienTaxation.push(bienTaxation);
            }
        }

    } else {

        row.removeAttr('style');

        for (var i = 0; i < dataBienTaxation.length; i++) {

            if (dataBienTaxation[i].bien == id) {

                dataBienTaxation.splice(i, 1);
                break;
            }
        }

        if (dataBienTaxation.length == 0) {
            checkExist = false;
        } else {
            checkExist = true;
        }
    }

    lblNbreBienSelected.html(dataBienTaxation.length);
    //alert(checkExist);

}