package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Assujettissement;
import cd.hologram.erecettesvg.models.Devise;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(PrevisionCredit.class)
public class PrevisionCredit_ { 

    public static volatile SingularAttribute<PrevisionCredit, Integer> agentMaj;
    public static volatile SingularAttribute<PrevisionCredit, Integer> nbreTourInitial;
    public static volatile SingularAttribute<PrevisionCredit, String> code;
    public static volatile SingularAttribute<PrevisionCredit, BigDecimal> montantTotal;
    public static volatile SingularAttribute<PrevisionCredit, String> type;
    public static volatile SingularAttribute<PrevisionCredit, Date> dateCreate;
    public static volatile SingularAttribute<PrevisionCredit, Integer> etat;
    public static volatile SingularAttribute<PrevisionCredit, Devise> devise;
    public static volatile SingularAttribute<PrevisionCredit, Date> dateMaj;
    public static volatile SingularAttribute<PrevisionCredit, String> fkPersonne;
    public static volatile SingularAttribute<PrevisionCredit, Integer> agentCreate;
    public static volatile SingularAttribute<PrevisionCredit, String> fkSite;
    public static volatile SingularAttribute<PrevisionCredit, Integer> nbreTourReel;
    public static volatile SingularAttribute<PrevisionCredit, Assujettissement> assujettissement;

}