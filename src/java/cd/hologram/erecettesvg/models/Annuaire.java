/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ANNUAIRE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Annuaire.findAll", query = "SELECT a FROM Annuaire a"),
    @NamedQuery(name = "Annuaire.findByCode", query = "SELECT a FROM Annuaire a WHERE a.code = :code"),
    @NamedQuery(name = "Annuaire.findByReference", query = "SELECT a FROM Annuaire a WHERE a.reference = :reference"),
    @NamedQuery(name = "Annuaire.findByTypeAnnuaire", query = "SELECT a FROM Annuaire a WHERE a.typeAnnuaire = :typeAnnuaire"),
    @NamedQuery(name = "Annuaire.findByLibelle", query = "SELECT a FROM Annuaire a WHERE a.libelle = :libelle"),
    @NamedQuery(name = "Annuaire.findByEtat", query = "SELECT a FROM Annuaire a WHERE a.etat = :etat"),
    @NamedQuery(name = "Annuaire.findByAgentCreat", query = "SELECT a FROM Annuaire a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "Annuaire.findByDateCreat", query = "SELECT a FROM Annuaire a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "Annuaire.findByAgentMaj", query = "SELECT a FROM Annuaire a WHERE a.agentMaj = :agentMaj"),
    @NamedQuery(name = "Annuaire.findByDateMaj", query = "SELECT a FROM Annuaire a WHERE a.dateMaj = :dateMaj")})
public class Annuaire implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 25)
    @Column(name = "REFERENCE")
    private String reference;
    @Size(max = 10)
    @Column(name = "TYPE_ANNUAIRE")
    private String typeAnnuaire;
    @Size(max = 50)
    @Column(name = "LIBELLE")
    private String libelle;
    @Column(name = "ETAT")
    private Short etat;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    public Annuaire() {
    }

    public Annuaire(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTypeAnnuaire() {
        return typeAnnuaire;
    }

    public void setTypeAnnuaire(String typeAnnuaire) {
        this.typeAnnuaire = typeAnnuaire;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Annuaire)) {
            return false;
        }
        Annuaire other = (Annuaire) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Annuaire[ code=" + code + " ]";
    }
    
}
