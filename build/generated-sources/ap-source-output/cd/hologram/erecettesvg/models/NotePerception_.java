package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.AcquitLiberatoire;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.Personne;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(NotePerception.class)
public class NotePerception_ { 

    public static volatile SingularAttribute<NotePerception, String> compteBancaire;
    public static volatile SingularAttribute<NotePerception, String> numero;
    public static volatile SingularAttribute<NotePerception, String> amr;
    public static volatile SingularAttribute<NotePerception, NoteCalcul> noteCalcul;
    public static volatile SingularAttribute<NotePerception, BigDecimal> payer;
    public static volatile SingularAttribute<NotePerception, Short> etat;
    public static volatile SingularAttribute<NotePerception, Integer> nbrImpression;
    public static volatile SingularAttribute<NotePerception, Short> taxationOffice;
    public static volatile SingularAttribute<NotePerception, String> timbre;
    public static volatile SingularAttribute<NotePerception, String> papierSecurise;
    public static volatile SingularAttribute<NotePerception, BigDecimal> montantInitial;
    public static volatile SingularAttribute<NotePerception, BigDecimal> solde;
    public static volatile SingularAttribute<NotePerception, String> quittance;
    public static volatile SingularAttribute<NotePerception, String> dateReception;
    public static volatile SingularAttribute<NotePerception, BigDecimal> netAPayer;
    public static volatile SingularAttribute<NotePerception, BigDecimal> tauxApplique;
    public static volatile SingularAttribute<NotePerception, String> dateEcheancePaiement;
    public static volatile SingularAttribute<NotePerception, String> dateCreat;
    public static volatile SingularAttribute<NotePerception, Short> fractionnee;
    public static volatile SingularAttribute<NotePerception, String> devise;
    public static volatile SingularAttribute<NotePerception, NotePerception> npMere;
    public static volatile SingularAttribute<NotePerception, Personne> receptionniste;
    public static volatile SingularAttribute<NotePerception, String> site;
    public static volatile ListAttribute<NotePerception, DetailsNc> detailsNcList;
    public static volatile ListAttribute<NotePerception, AcquitLiberatoire> acquitLiberatoireList;
    public static volatile SingularAttribute<NotePerception, String> agentCreat;
    public static volatile ListAttribute<NotePerception, NotePerception> notePerceptionList;

}