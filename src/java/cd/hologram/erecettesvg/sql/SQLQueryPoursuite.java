/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.sql;

/**
 *
 * @author gauthier.muata
 */
public class SQLQueryPoursuite {

    public static String SELECT_LIST_MED_V1 = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.ETAT > 0  AND M.TYPE_MED = 'INVITATION_PAIEMENT' %s";
    public static String SELECT_LIST_MED_INVITATION_SERVICE_PAIEMENT = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.ETAT > 0 AND M.TYPE_MED = 'INVITATION_SERVICE'  %s";

    public static String SELECT_LIST_MED_V2 = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.ETAT > 0 %s %s %s %s";

    public static String SELECT_LIST_AMR_V1 = "SELECT A.* FROM T_AMR A WITH (READPAST) WHERE A.ETAT > 0 %s";

    public static String SELECT_LIST_AMR_V2 = "SELECT A.* FROM T_AMR A WITH (READPAST) WHERE A.ETAT > 0 %s %s %s %s";
    
    public static String SELECT_LIST_AMR_INVITATION_A_PAYER = "SELECT A.* FROM T_AMR A WITH (READPAST) WHERE A.ETAT > 0 AND TYPE_AMR = 'AMR' %s %s %s %s";
    
    public static String SELECT_LIST_AMR_RELANCE = "SELECT A.* FROM T_AMR A WITH (READPAST) WHERE A.ETAT > 0 AND TYPE_AMR = 'AMRR' %s %s %s %s";

    public static String SELECT_LIST_CONTRAINTE_V1 = "SELECT C.* FROM T_CONTRAINTE C WITH (READPAST) WHERE C.ETAT = 1 %s";

    public static String SELECT_LIST_CONTRAINTE_V2 = "SELECT C.* FROM T_CONTRAINTE C WITH (READPAST) WHERE C.ETAT = 1 %s %s";

    public static String SELECT_LIST_COMMANDEMENT_V1 = "SELECT CMD.* FROM T_COMMANDEMENT CMD WITH (READPAST) WHERE CMD.ETAT = 2 %s";

    public static String SELECT_LIST_ATD_V1 = "SELECT A.* FROM T_ATD A WITH (READPAST) WHERE A.ETAT = 1 %s";

    public static String SELECT_LIST_COMMANDEMENT_V2 = "SELECT CMD.* FROM T_COMMANDEMENT CMD WITH (READPAST) WHERE CMD.ETAT IN (1,2) %s %s";

    public static String SELECT_ATD_BY_COMMANDEMENT = "SELECT A.ID,ID_COMMANDEMENT FROM T_ATD A WITH (READPAST) WHERE A.ID_COMMANDEMENT = ?1 AND A.ETAT = 1";

    public static String SELECT_ATD_BY_ID = "SELECT A.* FROM T_ATD A WITH (READPAST) WHERE A.ID = ?1 AND A.ETAT = 1";

    public static String CHECK_RECIDIVISTE = "SELECT dbo.F_GET_RECIDIVISTE('%s', '%s')";

    public static String CHECK_RECIDIVISTE_DECLARATION = "SELECT dbo.F_GET_RECIDIVISTE_DECLARATION('%s',%s)";

    public static String CHECK_RECIDIVISTE_REDRESSEMENT = "SELECT dbo.F_GET_RECIDIVISTE_REDRESSEMENT('%s')";

    public static String SELECT_LIST_FICHE_PRISE_CHARGE_V1 = "SELECT FPC.* FROM T_FICHE_PRISE_CHARGE FPC WITH (READPAST) WHERE FPC.ETAT IN (0,1,2,3,4) %s";

    public static String SELECT_LIST_FICHE_PRISE_CHARGE_V2 = "SELECT FPC.* FROM T_FICHE_PRISE_CHARGE FPC WITH (READPAST) %s %s";

    public static String SELECT_LIST_BON_A_PAYER_V1 = "SELECT BP.* FROM T_BON_A_PAYER BP WITH (READPAST) WHERE BP.ETAT IN (0,1,2,3,4,5) %s";
    
    public static String SELECT_LIST_BON_A_PAYER_V2 = "SELECT BP.* FROM T_BON_A_PAYER BP WITH (READPAST) %s %s";
    
    public static String SELECT_LIST_MEP = "SELECT M.* FROM T_MED M WITH (READPAST) WHERE M.ETAT > 0"
            + " AND M.TYPE_MED = 'PAIEMENT' AND CONVERT(DATE,M.DATE_ECHEANCE,103) < CONVERT(DATE,GETDATE(),103)"
            + " AND CONVERT(DATE,M.DATE_RECEPTION,103) <= CONVERT(DATE,GETDATE(),103) "
            + " AND M.NOTE_CALCUL IN (SELECT NC.NUMERO "
            + " FROM T_NOTE_CALCUL NC WITH (READPAST) WHERE NC.SERVICE = ?1)";

}
