package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.DetailEchelonnement;
import cd.hologram.erecettesvg.models.NotePerception;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DemandeEchelonnement.class)
public class DemandeEchelonnement_ { 

    public static volatile SingularAttribute<DemandeEchelonnement, String> observationValidation;
    public static volatile SingularAttribute<DemandeEchelonnement, String> code;
    public static volatile SingularAttribute<DemandeEchelonnement, Date> dateCreat;
    public static volatile SingularAttribute<DemandeEchelonnement, String> observation;
    public static volatile SingularAttribute<DemandeEchelonnement, String> fkDivision;
    public static volatile SingularAttribute<DemandeEchelonnement, NotePerception> fkNp;
    public static volatile ListAttribute<DemandeEchelonnement, DetailEchelonnement> detailEchelonnementList;
    public static volatile SingularAttribute<DemandeEchelonnement, Integer> agentCreat;
    public static volatile SingularAttribute<DemandeEchelonnement, Integer> etat;
    public static volatile SingularAttribute<DemandeEchelonnement, Date> dateValidation;
    public static volatile SingularAttribute<DemandeEchelonnement, String> referenceLettre;
    public static volatile SingularAttribute<DemandeEchelonnement, Integer> agentValidation;

}