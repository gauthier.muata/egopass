package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.CarteVoucher;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Personne;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(CommandeCarte.class)
public class CommandeCarte_ { 

    public static volatile SingularAttribute<CommandeCarte, String> reference;
    public static volatile SingularAttribute<CommandeCarte, BigDecimal> prixUnitaire;
    public static volatile SingularAttribute<CommandeCarte, Integer> qte;
    public static volatile SingularAttribute<CommandeCarte, Agent> agentUpdate;
    public static volatile SingularAttribute<CommandeCarte, Personne> fkPersonne;
    public static volatile SingularAttribute<CommandeCarte, Date> dateUpdate;
    public static volatile SingularAttribute<CommandeCarte, Integer> id;
    public static volatile SingularAttribute<CommandeCarte, Date> dateCreate;
    public static volatile SingularAttribute<CommandeCarte, BigDecimal> prixTotal;
    public static volatile SingularAttribute<CommandeCarte, Integer> etat;
    public static volatile SingularAttribute<CommandeCarte, Devise> devise;
    public static volatile ListAttribute<CommandeCarte, CarteVoucher> carteVoucherList;

}