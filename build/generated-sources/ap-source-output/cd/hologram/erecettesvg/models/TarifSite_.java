package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TarifSite.class)
public class TarifSite_ { 

    public static volatile SingularAttribute<TarifSite, Integer> agentMaj;
    public static volatile SingularAttribute<TarifSite, String> fkTarif;
    public static volatile SingularAttribute<TarifSite, Integer> agentCreate;
    public static volatile SingularAttribute<TarifSite, String> fkSiteDestination;
    public static volatile SingularAttribute<TarifSite, BigDecimal> taux;
    public static volatile SingularAttribute<TarifSite, String> fkDevise;
    public static volatile SingularAttribute<TarifSite, String> fkSiteProvenance;
    public static volatile SingularAttribute<TarifSite, Integer> id;
    public static volatile SingularAttribute<TarifSite, Date> dateCreate;
    public static volatile SingularAttribute<TarifSite, Integer> etat;
    public static volatile SingularAttribute<TarifSite, Date> dateMaj;

}