package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.Service;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ArticleGenerique.class)
public class ArticleGenerique_ { 

    public static volatile SingularAttribute<ArticleGenerique, Service> serviceAssiette;
    public static volatile SingularAttribute<ArticleGenerique, String> code;
    public static volatile SingularAttribute<ArticleGenerique, FormeJuridique> formeJuridique;
    public static volatile ListAttribute<ArticleGenerique, ArticleBudgetaire> articleBudgetaireList;
    public static volatile SingularAttribute<ArticleGenerique, String> intitule;
    public static volatile SingularAttribute<ArticleGenerique, Boolean> etat;

}