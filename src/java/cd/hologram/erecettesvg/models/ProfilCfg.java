/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_PROFIL_CFG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProfilCfg.findAll", query = "SELECT p FROM ProfilCfg p"),
    @NamedQuery(name = "ProfilCfg.findById", query = "SELECT p FROM ProfilCfg p WHERE p.id = :id"),
    @NamedQuery(name = "ProfilCfg.findByMailNotificationUser", query = "SELECT p FROM ProfilCfg p WHERE p.mailNotificationUser = :mailNotificationUser"),
    @NamedQuery(name = "ProfilCfg.findByMailNotificationRequerant", query = "SELECT p FROM ProfilCfg p WHERE p.mailNotificationRequerant = :mailNotificationRequerant"),
    @NamedQuery(name = "ProfilCfg.findByMailTypeCompte", query = "SELECT p FROM ProfilCfg p WHERE p.mailTypeCompte = :mailTypeCompte"),
    @NamedQuery(name = "ProfilCfg.findByMailPassword", query = "SELECT p FROM ProfilCfg p WHERE p.mailPassword = :mailPassword"),
    @NamedQuery(name = "ProfilCfg.findByMailAdresseIp", query = "SELECT p FROM ProfilCfg p WHERE p.mailAdresseIp = :mailAdresseIp"),
    @NamedQuery(name = "ProfilCfg.findBySmsNotificationRequerant", query = "SELECT p FROM ProfilCfg p WHERE p.smsNotificationRequerant = :smsNotificationRequerant"),
    @NamedQuery(name = "ProfilCfg.findBySmsIp", query = "SELECT p FROM ProfilCfg p WHERE p.smsIp = :smsIp"),
    @NamedQuery(name = "ProfilCfg.findBySmsPort", query = "SELECT p FROM ProfilCfg p WHERE p.smsPort = :smsPort"),
    @NamedQuery(name = "ProfilCfg.findByTrackingDocIp", query = "SELECT p FROM ProfilCfg p WHERE p.trackingDocIp = :trackingDocIp"),
    @NamedQuery(name = "ProfilCfg.findByTrackingDocPort", query = "SELECT p FROM ProfilCfg p WHERE p.trackingDocPort = :trackingDocPort"),
    @NamedQuery(name = "ProfilCfg.findByPrintModeleNp", query = "SELECT p FROM ProfilCfg p WHERE p.printModeleNp = :printModeleNp"),
    @NamedQuery(name = "ProfilCfg.findByPrintModeleAl", query = "SELECT p FROM ProfilCfg p WHERE p.printModeleAl = :printModeleAl"),
    @NamedQuery(name = "ProfilCfg.findByMetierAbUnique", query = "SELECT p FROM ProfilCfg p WHERE p.metierAbUnique = :metierAbUnique"),
    @NamedQuery(name = "ProfilCfg.findByMetierDeclarationPeriodeUnique", query = "SELECT p FROM ProfilCfg p WHERE p.metierDeclarationPeriodeUnique = :metierDeclarationPeriodeUnique"),
    @NamedQuery(name = "ProfilCfg.findByMetierProcedureCourteActive", query = "SELECT p FROM ProfilCfg p WHERE p.metierProcedureCourteActive = :metierProcedureCourteActive"),
    @NamedQuery(name = "ProfilCfg.findByMetierAccuseReceptionNpAuto", query = "SELECT p FROM ProfilCfg p WHERE p.metierAccuseReceptionNpAuto = :metierAccuseReceptionNpAuto"),
    @NamedQuery(name = "ProfilCfg.findByMetierNotPrintNp", query = "SELECT p FROM ProfilCfg p WHERE p.metierNotPrintNp = :metierNotPrintNp"),
    @NamedQuery(name = "ProfilCfg.findByMetierPaiementAutoActif", query = "SELECT p FROM ProfilCfg p WHERE p.metierPaiementAutoActif = :metierPaiementAutoActif"),
    @NamedQuery(name = "ProfilCfg.findByMetierModePaiement", query = "SELECT p FROM ProfilCfg p WHERE p.metierModePaiement = :metierModePaiement"),
    @NamedQuery(name = "ProfilCfg.findByMetierBanque", query = "SELECT p FROM ProfilCfg p WHERE p.metierBanque = :metierBanque"),
    @NamedQuery(name = "ProfilCfg.findByMetierCompte", query = "SELECT p FROM ProfilCfg p WHERE p.metierCompte = :metierCompte"),
    @NamedQuery(name = "ProfilCfg.findByMetierPrintDirectAl", query = "SELECT p FROM ProfilCfg p WHERE p.metierPrintDirectAl = :metierPrintDirectAl"),
    @NamedQuery(name = "ProfilCfg.findByMetierExecuteApplication", query = "SELECT p FROM ProfilCfg p WHERE p.metierExecuteApplication = :metierExecuteApplication"),
    @NamedQuery(name = "ProfilCfg.findByIntitule", query = "SELECT p FROM ProfilCfg p WHERE p.intitule = :intitule"),
    @NamedQuery(name = "ProfilCfg.findByObservation", query = "SELECT p FROM ProfilCfg p WHERE p.observation = :observation"),
    @NamedQuery(name = "ProfilCfg.findByAgentCreat", query = "SELECT p FROM ProfilCfg p WHERE p.agentCreat = :agentCreat"),
    @NamedQuery(name = "ProfilCfg.findByDateCreat", query = "SELECT p FROM ProfilCfg p WHERE p.dateCreat = :dateCreat"),
    @NamedQuery(name = "ProfilCfg.findByEtat", query = "SELECT p FROM ProfilCfg p WHERE p.etat = :etat"),
    @NamedQuery(name = "ProfilCfg.findByInclurePenaliteRecouvrementDansDeclaration", query = "SELECT p FROM ProfilCfg p WHERE p.inclurePenaliteRecouvrementDansDeclaration = :inclurePenaliteRecouvrementDansDeclaration"),
    @NamedQuery(name = "ProfilCfg.findByAlgorithmeCalculPenalite", query = "SELECT p FROM ProfilCfg p WHERE p.algorithmeCalculPenalite = :algorithmeCalculPenalite"),
    @NamedQuery(name = "ProfilCfg.findByEnteteA", query = "SELECT p FROM ProfilCfg p WHERE p.enteteA = :enteteA"),
    @NamedQuery(name = "ProfilCfg.findByEnteteB", query = "SELECT p FROM ProfilCfg p WHERE p.enteteB = :enteteB"),
    @NamedQuery(name = "ProfilCfg.findByTitreCpi", query = "SELECT p FROM ProfilCfg p WHERE p.titreCpi = :titreCpi")})
public class ProfilCfg implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "MAIL_NOTIFICATION_USER")
    private Boolean mailNotificationUser;
    @Column(name = "MAIL_NOTIFICATION_REQUERANT")
    private Boolean mailNotificationRequerant;
    @Size(max = 50)
    @Column(name = "MAIL_TYPE_COMPTE")
    private String mailTypeCompte;
    @Size(max = 50)
    @Column(name = "MAIL_PASSWORD")
    private String mailPassword;
    @Size(max = 30)
    @Column(name = "MAIL_ADRESSE_IP")
    private String mailAdresseIp;
    @Column(name = "SMS_NOTIFICATION_REQUERANT")
    private Boolean smsNotificationRequerant;
    @Size(max = 30)
    @Column(name = "SMS_IP")
    private String smsIp;
    @Column(name = "SMS_PORT")
    private Integer smsPort;
    @Size(max = 30)
    @Column(name = "TRACKING_DOC_IP")
    private String trackingDocIp;
    @Column(name = "TRACKING_DOC_PORT")
    private Integer trackingDocPort;
    @Size(max = 50)
    @Column(name = "PRINT_MODELE_NP")
    private String printModeleNp;
    @Size(max = 50)
    @Column(name = "PRINT_MODELE_AL")
    private String printModeleAl;
    @Size(max = 50)
    @Column(name = "METIER_AB_UNIQUE")
    private String metierAbUnique;
    @Column(name = "METIER_DECLARATION_PERIODE_UNIQUE")
    private Boolean metierDeclarationPeriodeUnique;
    @Column(name = "METIER_PROCEDURE_COURTE_ACTIVE")
    private Boolean metierProcedureCourteActive;
    @Column(name = "METIER_ACCUSE_RECEPTION_NP_AUTO")
    private Boolean metierAccuseReceptionNpAuto;
    @Column(name = "METIER_NOT_PRINT_NP")
    private Boolean metierNotPrintNp;
    @Column(name = "METIER_PAIEMENT_AUTO_ACTIF")
    private Boolean metierPaiementAutoActif;
    @Size(max = 50)
    @Column(name = "METIER_MODE_PAIEMENT")
    private String metierModePaiement;
    @Size(max = 50)
    @Column(name = "METIER_BANQUE")
    private String metierBanque;
    @Size(max = 50)
    @Column(name = "METIER_COMPTE")
    private String metierCompte;
    @Column(name = "METIER_PRINT_DIRECT_AL")
    private Boolean metierPrintDirectAl;
    @Size(max = 50)
    @Column(name = "METIER_EXECUTE_APPLICATION")
    private String metierExecuteApplication;
    @Size(max = 50)
    @Column(name = "INTITULE")
    private String intitule;
    @Size(max = 50)
    @Column(name = "OBSERVATION")
    private String observation;
    @Size(max = 50)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 50)
    @Column(name = "DATE_CREAT")
    private String dateCreat;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "INCLURE_PENALITE_RECOUVREMENT_DANS_DECLARATION")
    private Boolean inclurePenaliteRecouvrementDansDeclaration;
    @Column(name = "ALGORITHME_CALCUL_PENALITE")
    private Integer algorithmeCalculPenalite;
    @Size(max = 600)
    @Column(name = "ENTETE_A")
    private String enteteA;
    @Size(max = 600)
    @Column(name = "ENTETE_B")
    private String enteteB;
    @Size(max = 1000)
    @Column(name = "TITRE_CPI")
    private String titreCpi;
    @OneToMany(mappedBy = "fkProfilCfg")
    private List<ProfilEquipement> profilEquipementList;

    public ProfilCfg() {
    }

    public ProfilCfg(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getMailNotificationUser() {
        return mailNotificationUser;
    }

    public void setMailNotificationUser(Boolean mailNotificationUser) {
        this.mailNotificationUser = mailNotificationUser;
    }

    public Boolean getMailNotificationRequerant() {
        return mailNotificationRequerant;
    }

    public void setMailNotificationRequerant(Boolean mailNotificationRequerant) {
        this.mailNotificationRequerant = mailNotificationRequerant;
    }

    public String getMailTypeCompte() {
        return mailTypeCompte;
    }

    public void setMailTypeCompte(String mailTypeCompte) {
        this.mailTypeCompte = mailTypeCompte;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    public String getMailAdresseIp() {
        return mailAdresseIp;
    }

    public void setMailAdresseIp(String mailAdresseIp) {
        this.mailAdresseIp = mailAdresseIp;
    }

    public Boolean getSmsNotificationRequerant() {
        return smsNotificationRequerant;
    }

    public void setSmsNotificationRequerant(Boolean smsNotificationRequerant) {
        this.smsNotificationRequerant = smsNotificationRequerant;
    }

    public String getSmsIp() {
        return smsIp;
    }

    public void setSmsIp(String smsIp) {
        this.smsIp = smsIp;
    }

    public Integer getSmsPort() {
        return smsPort;
    }

    public void setSmsPort(Integer smsPort) {
        this.smsPort = smsPort;
    }

    public String getTrackingDocIp() {
        return trackingDocIp;
    }

    public void setTrackingDocIp(String trackingDocIp) {
        this.trackingDocIp = trackingDocIp;
    }

    public Integer getTrackingDocPort() {
        return trackingDocPort;
    }

    public void setTrackingDocPort(Integer trackingDocPort) {
        this.trackingDocPort = trackingDocPort;
    }

    public String getPrintModeleNp() {
        return printModeleNp;
    }

    public void setPrintModeleNp(String printModeleNp) {
        this.printModeleNp = printModeleNp;
    }

    public String getPrintModeleAl() {
        return printModeleAl;
    }

    public void setPrintModeleAl(String printModeleAl) {
        this.printModeleAl = printModeleAl;
    }

    public String getMetierAbUnique() {
        return metierAbUnique;
    }

    public void setMetierAbUnique(String metierAbUnique) {
        this.metierAbUnique = metierAbUnique;
    }

    public Boolean getMetierDeclarationPeriodeUnique() {
        return metierDeclarationPeriodeUnique;
    }

    public void setMetierDeclarationPeriodeUnique(Boolean metierDeclarationPeriodeUnique) {
        this.metierDeclarationPeriodeUnique = metierDeclarationPeriodeUnique;
    }

    public Boolean getMetierProcedureCourteActive() {
        return metierProcedureCourteActive;
    }

    public void setMetierProcedureCourteActive(Boolean metierProcedureCourteActive) {
        this.metierProcedureCourteActive = metierProcedureCourteActive;
    }

    public Boolean getMetierAccuseReceptionNpAuto() {
        return metierAccuseReceptionNpAuto;
    }

    public void setMetierAccuseReceptionNpAuto(Boolean metierAccuseReceptionNpAuto) {
        this.metierAccuseReceptionNpAuto = metierAccuseReceptionNpAuto;
    }

    public Boolean getMetierNotPrintNp() {
        return metierNotPrintNp;
    }

    public void setMetierNotPrintNp(Boolean metierNotPrintNp) {
        this.metierNotPrintNp = metierNotPrintNp;
    }

    public Boolean getMetierPaiementAutoActif() {
        return metierPaiementAutoActif;
    }

    public void setMetierPaiementAutoActif(Boolean metierPaiementAutoActif) {
        this.metierPaiementAutoActif = metierPaiementAutoActif;
    }

    public String getMetierModePaiement() {
        return metierModePaiement;
    }

    public void setMetierModePaiement(String metierModePaiement) {
        this.metierModePaiement = metierModePaiement;
    }

    public String getMetierBanque() {
        return metierBanque;
    }

    public void setMetierBanque(String metierBanque) {
        this.metierBanque = metierBanque;
    }

    public String getMetierCompte() {
        return metierCompte;
    }

    public void setMetierCompte(String metierCompte) {
        this.metierCompte = metierCompte;
    }

    public Boolean getMetierPrintDirectAl() {
        return metierPrintDirectAl;
    }

    public void setMetierPrintDirectAl(Boolean metierPrintDirectAl) {
        this.metierPrintDirectAl = metierPrintDirectAl;
    }

    public String getMetierExecuteApplication() {
        return metierExecuteApplication;
    }

    public void setMetierExecuteApplication(String metierExecuteApplication) {
        this.metierExecuteApplication = metierExecuteApplication;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Boolean getInclurePenaliteRecouvrementDansDeclaration() {
        return inclurePenaliteRecouvrementDansDeclaration;
    }

    public void setInclurePenaliteRecouvrementDansDeclaration(Boolean inclurePenaliteRecouvrementDansDeclaration) {
        this.inclurePenaliteRecouvrementDansDeclaration = inclurePenaliteRecouvrementDansDeclaration;
    }

    public Integer getAlgorithmeCalculPenalite() {
        return algorithmeCalculPenalite;
    }

    public void setAlgorithmeCalculPenalite(Integer algorithmeCalculPenalite) {
        this.algorithmeCalculPenalite = algorithmeCalculPenalite;
    }

    public String getEnteteA() {
        return enteteA;
    }

    public void setEnteteA(String enteteA) {
        this.enteteA = enteteA;
    }

    public String getEnteteB() {
        return enteteB;
    }

    public void setEnteteB(String enteteB) {
        this.enteteB = enteteB;
    }

    public String getTitreCpi() {
        return titreCpi;
    }

    public void setTitreCpi(String titreCpi) {
        this.titreCpi = titreCpi;
    }

    @XmlTransient
    public List<ProfilEquipement> getProfilEquipementList() {
        return profilEquipementList;
    }

    public void setProfilEquipementList(List<ProfilEquipement> profilEquipementList) {
        this.profilEquipementList = profilEquipementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfilCfg)) {
            return false;
        }
        ProfilCfg other = (ProfilCfg) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ProfilCfg[ id=" + id + " ]";
    }
    
}
