package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.EntiteAdministrative;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TypeEntite.class)
public class TypeEntite_ { 

    public static volatile SingularAttribute<TypeEntite, String> agentMaj;
    public static volatile SingularAttribute<TypeEntite, Integer> code;
    public static volatile SingularAttribute<TypeEntite, Date> dateCreat;
    public static volatile ListAttribute<TypeEntite, EntiteAdministrative> entiteAdministrativeList;
    public static volatile SingularAttribute<TypeEntite, String> agentCreat;
    public static volatile SingularAttribute<TypeEntite, String> intitule;
    public static volatile SingularAttribute<TypeEntite, Short> etat;
    public static volatile SingularAttribute<TypeEntite, Date> dateMaj;

}