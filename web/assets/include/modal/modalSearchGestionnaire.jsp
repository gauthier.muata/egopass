<%-- 
    Document   : modalSearchGestionnaire
    Created on : 17 mars 2020, 14:13:23
    Author     : WILLY
--%>

<div class="modal fade" id="gestionnaireModal" 
     role="dialog"
     data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" style="width: 60%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" 
                    id="exampleModalLongTitle">Rechercher et s&eacute;lectionner un gestionnaire</h5>
            </div>
            <div class="modal-body" id="divAssujetti">
                <div class="navbar-form" role="search">
                    <div class="input-group">
                        <input 
                            type="text" class="form-control" 
                            id="inputValueResearchGestionnaire" 
                            placeholder="Crit&egrave;re de recherche"/>
                        <div class="input-group-btn">
                            <button 
                                class="btn btn-primary" 
                                id="btnLauchSearchGestionnaire">
                                <i class="fa fa-search"></i>
                                Rechercher
                            </button>
                        </div>
                    </div>
                    <a href="identification"
                       class="btn btn-warning pull-right">
                        <i class="fa fa-plus-circle"></i>  
                        Nouveau Gestionnaire
                    </a>
                </div>
                <table width="100%" 
                       class="table table-bordered table-hover" 
                       id="tableResultSearchGestionnaire">
                </table>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" 
                        id="btnSelectGestionnaire"
                        style="display: none">
                    <i class="fa fa-check-circle"></i> &nbsp;
                    S&eacute;lectionner</button>
                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
            </div>
        </div>
    </div>
</div>