package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(LogCpiCarteConducteurMoto.class)
public class LogCpiCarteConducteurMoto_ { 

    public static volatile SingularAttribute<LogCpiCarteConducteurMoto, Date> dateLog;
    public static volatile SingularAttribute<LogCpiCarteConducteurMoto, Integer> id;
    public static volatile SingularAttribute<LogCpiCarteConducteurMoto, String> cpi;
    public static volatile SingularAttribute<LogCpiCarteConducteurMoto, Integer> etat;

}