package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.Personne;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AdressePersonne.class)
public class AdressePersonne_ { 

    public static volatile ListAttribute<AdressePersonne, Assujeti> assujetiList;
    public static volatile SingularAttribute<AdressePersonne, String> code;
    public static volatile ListAttribute<AdressePersonne, Bien> bienList;
    public static volatile SingularAttribute<AdressePersonne, Personne> personne;
    public static volatile SingularAttribute<AdressePersonne, Adresse> adresse;
    public static volatile SingularAttribute<AdressePersonne, Boolean> etat;
    public static volatile SingularAttribute<AdressePersonne, Boolean> parDefaut;

}