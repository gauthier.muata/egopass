package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Assujettissement;
import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.ComplementBien;
import cd.hologram.erecettesvg.models.DetailPrevisionCredit;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Tarif;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailAssujettissement.class)
public class DetailAssujettissement_ { 

    public static volatile SingularAttribute<DetailAssujettissement, Date> dateMajExemption;
    public static volatile SingularAttribute<DetailAssujettissement, Date> dateCreateExemption;
    public static volatile SingularAttribute<DetailAssujettissement, BigDecimal> taux;
    public static volatile SingularAttribute<DetailAssujettissement, Integer> agentMajExemption;
    public static volatile SingularAttribute<DetailAssujettissement, Devise> devise;
    public static volatile SingularAttribute<DetailAssujettissement, Integer> etat;
    public static volatile SingularAttribute<DetailAssujettissement, Integer> agentCreateExemption;
    public static volatile SingularAttribute<DetailAssujettissement, Tarif> tarif;
    public static volatile SingularAttribute<DetailAssujettissement, String> fkPersonne;
    public static volatile SingularAttribute<DetailAssujettissement, ComplementBien> complementBien;
    public static volatile SingularAttribute<DetailAssujettissement, Assujettissement> assujettissement;
    public static volatile SingularAttribute<DetailAssujettissement, Integer> id;
    public static volatile SingularAttribute<DetailAssujettissement, Bien> bien;
    public static volatile ListAttribute<DetailAssujettissement, DetailPrevisionCredit> detailPrevisionCreditList;

}