package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ProcessusUa.class)
public class ProcessusUa_ { 

    public static volatile SingularAttribute<ProcessusUa, String> agentMaj;
    public static volatile SingularAttribute<ProcessusUa, String> process;
    public static volatile SingularAttribute<ProcessusUa, Date> dateCreat;
    public static volatile SingularAttribute<ProcessusUa, Integer> id;
    public static volatile SingularAttribute<ProcessusUa, String> ua;
    public static volatile SingularAttribute<ProcessusUa, String> agentCreat;
    public static volatile SingularAttribute<ProcessusUa, Short> etat;
    public static volatile SingularAttribute<ProcessusUa, Date> dateMaj;

}