package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.AgentSite;
import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.DepotDeclaration;
import cd.hologram.erecettesvg.models.DetailsReclamation;
import cd.hologram.erecettesvg.models.Fonction;
import cd.hologram.erecettesvg.models.Reclamation;
import cd.hologram.erecettesvg.models.RecoursJuridictionnel;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.Ua;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Agent.class)
public class Agent_ { 

    public static volatile SingularAttribute<Agent, Integer> agentSuppression;
    public static volatile SingularAttribute<Agent, String> agentMaj;
    public static volatile SingularAttribute<Agent, String> etape;
    public static volatile SingularAttribute<Agent, Short> compteExpire;
    public static volatile SingularAttribute<Agent, Integer> code;
    public static volatile SingularAttribute<Agent, String> matricule;
    public static volatile SingularAttribute<Agent, String> signature;
    public static volatile ListAttribute<Agent, Bordereau> bordereauList;
    public static volatile SingularAttribute<Agent, String> login;
    public static volatile SingularAttribute<Agent, Ua> ua;
    public static volatile SingularAttribute<Agent, String> nom;
    public static volatile SingularAttribute<Agent, Short> etat;
    public static volatile SingularAttribute<Agent, Date> dateMaj;
    public static volatile SingularAttribute<Agent, String> prenoms;
    public static volatile SingularAttribute<Agent, String> changePwd;
    public static volatile SingularAttribute<Agent, Date> dateDerniereConnexion;
    public static volatile SingularAttribute<Agent, Date> dateExpiration;
    public static volatile ListAttribute<Agent, AgentSite> agentSiteList;
    public static volatile ListAttribute<Agent, DetailsReclamation> detailsReclamationList;
    public static volatile SingularAttribute<Agent, Integer> connecte;
    public static volatile ListAttribute<Agent, DepotDeclaration> depotDeclarationList;
    public static volatile SingularAttribute<Agent, Date> dateCreat;
    public static volatile SingularAttribute<Agent, String> personne;
    public static volatile ListAttribute<Agent, Reclamation> reclamationList;
    public static volatile SingularAttribute<Agent, Date> dateSuppression;
    public static volatile SingularAttribute<Agent, Date> dateDerniereDeconnexion;
    public static volatile SingularAttribute<Agent, Site> site;
    public static volatile ListAttribute<Agent, RecoursJuridictionnel> recoursJuridictionnelList;
    public static volatile SingularAttribute<Agent, Service> service;
    public static volatile SingularAttribute<Agent, String> district;
    public static volatile SingularAttribute<Agent, String> grade;
    public static volatile SingularAttribute<Agent, String> mdp;
    public static volatile SingularAttribute<Agent, Fonction> fonction;
    public static volatile SingularAttribute<Agent, String> agentCreat;
    public static volatile SingularAttribute<Agent, String> observationSuppression;

}