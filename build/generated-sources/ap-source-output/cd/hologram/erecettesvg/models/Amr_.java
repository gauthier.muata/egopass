package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Contrainte;
import cd.hologram.erecettesvg.models.DetailsAmr;
import cd.hologram.erecettesvg.models.FichePriseCharge;
import cd.hologram.erecettesvg.models.FraisPoursuite;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.PenaliteMajoree;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Amr.class)
public class Amr_ { 

    public static volatile ListAttribute<Amr, FraisPoursuite> fraisPoursuiteList;
    public static volatile SingularAttribute<Amr, String> numero;
    public static volatile SingularAttribute<Amr, Amr> amr1;
    public static volatile SingularAttribute<Amr, Amr> amr;
    public static volatile SingularAttribute<Amr, String> noteCalcul;
    public static volatile SingularAttribute<Amr, String> fkMed;
    public static volatile SingularAttribute<Amr, Boolean> genererPenalite;
    public static volatile SingularAttribute<Amr, Boolean> enContentieux;
    public static volatile SingularAttribute<Amr, String> typeAmr;
    public static volatile SingularAttribute<Amr, BigDecimal> payer;
    public static volatile SingularAttribute<Amr, Short> etat;
    public static volatile ListAttribute<Amr, Journal> journalList;
    public static volatile SingularAttribute<Amr, String> amrMere;
    public static volatile SingularAttribute<Amr, BigDecimal> solde;
    public static volatile ListAttribute<Amr, FraisPoursuite> fraisPoursuiteList1;
    public static volatile SingularAttribute<Amr, String> motif;
    public static volatile SingularAttribute<Amr, BigDecimal> netAPayer;
    public static volatile SingularAttribute<Amr, Integer> fkRetraitDeclaration;
    public static volatile ListAttribute<Amr, DetailsAmr> detailsAmrList;
    public static volatile ListAttribute<Amr, PenaliteMajoree> penaliteMajoreeList;
    public static volatile SingularAttribute<Amr, String> dateCreat;
    public static volatile SingularAttribute<Amr, String> notePerception;
    public static volatile SingularAttribute<Amr, String> observation;
    public static volatile SingularAttribute<Amr, Date> dateReceptionAmr;
    public static volatile SingularAttribute<Amr, BigDecimal> soldeInitial;
    public static volatile ListAttribute<Amr, Contrainte> contrainteList;
    public static volatile SingularAttribute<Amr, String> numeroDocument;
    public static volatile ListAttribute<Amr, BonAPayer> bonAPayerList;
    public static volatile SingularAttribute<Amr, FichePriseCharge> fichePriseCharge;
    public static volatile SingularAttribute<Amr, Date> dateEcheance;
    public static volatile SingularAttribute<Amr, String> agentCreat;
    public static volatile SingularAttribute<Amr, Boolean> fractionner;

}