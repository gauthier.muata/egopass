package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Annuaire.class)
public class Annuaire_ { 

    public static volatile SingularAttribute<Annuaire, String> reference;
    public static volatile SingularAttribute<Annuaire, String> agentMaj;
    public static volatile SingularAttribute<Annuaire, String> code;
    public static volatile SingularAttribute<Annuaire, Date> dateCreat;
    public static volatile SingularAttribute<Annuaire, String> libelle;
    public static volatile SingularAttribute<Annuaire, String> agentCreat;
    public static volatile SingularAttribute<Annuaire, Short> etat;
    public static volatile SingularAttribute<Annuaire, Date> dateMaj;
    public static volatile SingularAttribute<Annuaire, String> typeAnnuaire;

}