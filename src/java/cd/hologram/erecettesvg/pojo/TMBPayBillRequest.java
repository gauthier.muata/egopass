/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;


import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nathan.shamba
 */
@XmlRootElement
public class TMBPayBillRequest implements Serializable{
    private String Type;
    private String UserFirstName;
    private String UserLastName;
    private String UserLang;
    private String UserMobile;
    private String PayMode;
    private String BillUserId;
    private String BillIssuerName;
    private String ExternalReference;
    private String BillRef;
    private String Option;
    private String AgentShortCode;
    private String Media;
    private String SessionId;
    private String Amount;
    private String Currency;
    private String bankAccount;

    public TMBPayBillRequest() {
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getOption() {
        return Option;
    }

    public void setOption(String Option) {
        this.Option = Option;
    }

    
    public String getUserFirstName() {
        return UserFirstName;
    }

    public void setUserFirstName(String UserFirstName) {
        this.UserFirstName = UserFirstName;
    }

    public String getUserLastName() {
        return UserLastName;
    }

    public void setUserLastName(String UserLastName) {
        this.UserLastName = UserLastName;
    }

    public String getUserLang() {
        return UserLang;
    }

    public void setUserLang(String UserLang) {
        this.UserLang = UserLang;
    }

    public String getUserMobile() {
        return UserMobile;
    }

    public void setUserMobile(String UserMobile) {
        this.UserMobile = UserMobile;
    }

    public String getPayMode() {
        return PayMode;
    }

    public void setPayMode(String PayMode) {
        this.PayMode = PayMode;
    }

    public String getBillUserId() {
        return BillUserId;
    }

    public void setBillUserId(String BillUserId) {
        this.BillUserId = BillUserId;
    }

    public String getBillIssuerName() {
        return BillIssuerName;
    }

    public void setBillIssuerName(String BillIssuerName) {
        this.BillIssuerName = BillIssuerName;
    }

    public String getExternalReference() {
        return ExternalReference;
    }

    public void setExternalReference(String ExternalReference) {
        this.ExternalReference = ExternalReference;
    }

    public String getBillRef() {
        return BillRef;
    }

    public void setBillRef(String BillRef) {
        this.BillRef = BillRef;
    }

    public String getAgentShortCode() {
        return AgentShortCode;
    }

    public void setAgentShortCode(String AgentShortCode) {
        this.AgentShortCode = AgentShortCode;
    }

    public String getMedia() {
        return Media;
    }

    public void setMedia(String Media) {
        this.Media = Media;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String SessionId) {
        this.SessionId = SessionId;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String Currency) {
        this.Currency = Currency;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Override
    public String toString() {
        return "TMBPayBillRequest{" + "ExternalReference=" + ExternalReference + '}';
    }
  
}
