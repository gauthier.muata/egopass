/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author emmanuel.tsasa
 */
@Cacheable(false)
@Entity
@Table(name = "T_ROLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r"),
    @NamedQuery(name = "Role.findById", query = "SELECT r FROM Role r WHERE r.id = :id"),
    @NamedQuery(name = "Role.findByPeriodeDebut", query = "SELECT r FROM Role r WHERE r.periodeDebut = :periodeDebut"),
    @NamedQuery(name = "Role.findByPeriodeFin", query = "SELECT r FROM Role r WHERE r.periodeFin = :periodeFin"),
    @NamedQuery(name = "Role.findByDateCreat", query = "SELECT r FROM Role r WHERE r.dateCreat = :dateCreat"),
    @NamedQuery(name = "Role.findByArticleRole", query = "SELECT r FROM Role r WHERE r.articleRole = :articleRole"),
    @NamedQuery(name = "Role.findByFkSite", query = "SELECT r FROM Role r WHERE r.fkSite = :fkSite"),
    @NamedQuery(name = "Role.findByEtat", query = "SELECT r FROM Role r WHERE r.etat = :etat"),
    @NamedQuery(name = "Role.findByFraisPoursuiteGlobal", query = "SELECT r FROM Role r WHERE r.fraisPoursuiteGlobal = :fraisPoursuiteGlobal")})
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ID")
    private String id;
    @Column(name = "PERIODE_DEBUT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodeDebut;
    @Column(name = "PERIODE_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodeFin;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 50)
    @Column(name = "ARTICLE_ROLE")
    private String articleRole;
    
    @Column(name = "FK_SITE")
    private String fkSite;
        
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION_RECEVEUR")
    private String observationReceveur;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION_DIRECTION")
    private String observationDirection;
    @Column(name = "ETAT")
    private Integer etat;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "FRAIS_POURSUITE_GLOBAL")
    private BigDecimal fraisPoursuiteGlobal;
    @JoinColumn(name = "AGENT_CREAT", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentCreat;
    @OneToMany(mappedBy = "fkRole")
    private List<DetailsRole> detailsRoleList;

    public Role() {
    }

    public Role(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getPeriodeDebut() {
        return periodeDebut;
    }

    public void setPeriodeDebut(Date periodeDebut) {
        this.periodeDebut = periodeDebut;
    }

    public Date getPeriodeFin() {
        return periodeFin;
    }

    public void setPeriodeFin(Date periodeFin) {
        this.periodeFin = periodeFin;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getArticleRole() {
        return articleRole;
    }

    public void setArticleRole(String articleRole) {
        this.articleRole = articleRole;
    }

    public String getObservationReceveur() {
        return observationReceveur;
    }

    public void setObservationReceveur(String observationReceveur) {
        this.observationReceveur = observationReceveur;
    }

    public String getObservationDirection() {
        return observationDirection;
    }

    public void setObservationDirection(String observationDirection) {
        this.observationDirection = observationDirection;
    }

    public String getFkSite() {
        return fkSite;
    }

    public void setFkSite(String fkSite) {
        this.fkSite = fkSite;
    }
    
    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public BigDecimal getFraisPoursuiteGlobal() {
        return fraisPoursuiteGlobal;
    }

    public void setFraisPoursuiteGlobal(BigDecimal fraisPoursuiteGlobal) {
        this.fraisPoursuiteGlobal = fraisPoursuiteGlobal;
    }

    public Agent getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Agent agentCreat) {
        this.agentCreat = agentCreat;
    }

    @XmlTransient
    public List<DetailsRole> getDetailsRoleList() {
        return detailsRoleList;
    }

    public void setDetailsRoleList(List<DetailsRole> detailsRoleList) {
        this.detailsRoleList = detailsRoleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Role)) {
            return false;
        }
        Role other = (Role) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Role[ id=" + id + " ]";
    }
    
}
