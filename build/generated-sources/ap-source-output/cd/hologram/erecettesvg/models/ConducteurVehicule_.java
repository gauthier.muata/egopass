package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.Carte;
import cd.hologram.erecettesvg.models.Personne;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ConducteurVehicule.class)
public class ConducteurVehicule_ { 

    public static volatile SingularAttribute<ConducteurVehicule, Integer> agentMaj;
    public static volatile SingularAttribute<ConducteurVehicule, String> dateCreation;
    public static volatile SingularAttribute<ConducteurVehicule, Personne> proprietaire;
    public static volatile ListAttribute<ConducteurVehicule, Carte> carteList;
    public static volatile SingularAttribute<ConducteurVehicule, String> id;
    public static volatile SingularAttribute<ConducteurVehicule, Integer> agentCreat;
    public static volatile SingularAttribute<ConducteurVehicule, Integer> etat;
    public static volatile SingularAttribute<ConducteurVehicule, Personne> conducteur;
    public static volatile SingularAttribute<ConducteurVehicule, Date> dateMaj;
    public static volatile SingularAttribute<ConducteurVehicule, Bien> bien;

}