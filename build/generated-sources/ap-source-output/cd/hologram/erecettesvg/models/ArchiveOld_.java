package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ArchiveOld.class)
public class ArchiveOld_ { 

    public static volatile SingularAttribute<ArchiveOld, String> agentMaj;
    public static volatile SingularAttribute<ArchiveOld, String> refDocument;
    public static volatile SingularAttribute<ArchiveOld, Date> dateCreat;
    public static volatile SingularAttribute<ArchiveOld, Boolean> estSynchronise;
    public static volatile SingularAttribute<ArchiveOld, String> observation;
    public static volatile SingularAttribute<ArchiveOld, byte[]> document;
    public static volatile SingularAttribute<ArchiveOld, String> dossier;
    public static volatile SingularAttribute<ArchiveOld, String> typeFormat;
    public static volatile SingularAttribute<ArchiveOld, String> intitule;
    public static volatile SingularAttribute<ArchiveOld, Short> etat;
    public static volatile SingularAttribute<ArchiveOld, Date> dateMaj;
    public static volatile SingularAttribute<ArchiveOld, String> reference;
    public static volatile SingularAttribute<ArchiveOld, String> application;
    public static volatile SingularAttribute<ArchiveOld, Date> dateProduction;
    public static volatile SingularAttribute<ArchiveOld, Short> typeDoc;
    public static volatile SingularAttribute<ArchiveOld, String> typeDocument;
    public static volatile SingularAttribute<ArchiveOld, String> id;
    public static volatile SingularAttribute<ArchiveOld, String> agentCreat;

}