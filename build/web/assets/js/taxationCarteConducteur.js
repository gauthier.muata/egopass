/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inputValueResearchResponsible, inputValueResearchBudgetArticle;
var inputRate, inputBaseCalcul, inputBaseCalculOnCasePourcent, inputQuantity, inputAmount, labelTotalAmount,
        inputMinimum, inputMaximum;

var cmbResearchType, cmbTarif;

var lblLegalForm,
        lblNifResponsible,
        lblNameResponsible,
        lblAddress,
        lblGeneratorAct,
        lblBudgetArticle,
        lblExampleBase,
        lblUniteBase, lblUniteBase2, lblUnityBaseCalcul;

var btnResearchResponsible, btnResearchBudgetArticle,
        btnAddBudgetArticle, btnSaveTaxation, btnRemoveTaxation, btnCallModalSearchResponsable, btnAssociateBien;

var btnSelectedResponsible, btnSelectedBudgetArticle, btnValidateInfoBase;

var tbodyTaxation, tableResponsibles, tableBudgetArticles, tableTaxationBudgetArticles, tableTaxation;

var responsibleModal, budgetArticleModal;

var modalTransactionnel,
        inputMontanTransactionnel,
        lblTransactionnelValue,
        btnValidateTTransactionnel;

var infoResponsible, infoTaxationBudgetArticle, infosBaseCalculModal, tarifBudgetArticleModal;

var dataArrayBudgetArticles, dataTarifs = null;

var codeTarif, codeFormeJuridique, libelleTarif, libelleArticleBudgetaire, codeResponsible, adresseId;
var typeTaux, tauxPalier, tauxAppliquer, montant, montantDu, devise, codeBdgetArticle,
        baseCalculOnCaseIsPalier, uniteBudgetArticle = '';
var isPalier, isTransactionnel;
var multiplier = '';
var amount = 0;
var accept_N_ArticleToPanier = '';

var totalAmout2 = 0;

infoBudgetArticle = {};

var transacMinimum, transacMaximum, codeServiceAB;

var listTaxationArticle = [];
var listMotoCycle = [];

var responsibleObject = new Object();
var objetDetailNoteCalcul = new Object();
var erase;

var typeTaxation;

var lbl1, lbl2, lbl3, lbl4;

var lblZoneBien, lblSelectedBien, codeBien;

var abConducteurMotoList = [];

var lblMarque, lblPlaque, lblDescription, lblEtat, btnCallModalSearchMoto;

var motoCycleModal, tableMotoCycle, btnSelectedMoto;
var dataArrayMotoCycle;
var selectMotoCycle;
var motoCycleId;

var lblMarque1, lblPlaque1, lblDescription1, lblEtat1;

var typeTaxationModal, cmbTypeTaxation, btnSelectedTypeTaxation;
var cmbTypeTaxationId;
var nbreTax;

$(function () {

    mainNavigationLabel.text('TAXATION');
    secondNavigationLabel.text('Taxation carte conducteur de la moto');

    typeTaxation = 'TO';

    removeActiveMenu();
    linkMenuTaxation.addClass('active');

    erase = '0';
    isPalier = 0;
    transacMinimum = 0;
    transacMaximum = 0;
    isTransactionnel = 0;
    codeBdgetArticle = '';
    codeResponsible = '';
    tableResponsibles = $('#tableResponsibles');
    tableBudgetArticles = $('#tableBudgetArticles');
    tableTaxationBudgetArticles = $('#tableTaxation');

    inputValueResearchBudgetArticle = $('#inputValueResearchBudgetArticle');
    inputRate = $('#inputRate');
    inputBaseCalcul = $('#inputBaseCalcul');
    inputQuantity = $('#inputQuantity');
    inputAmount = $('#inputAmount');
    labelTotalAmount = $('#labelTotalAmount');


    inputMinimum = $('#inputMinimum');
    inputMaximum = $('#inputMaximum');
    inputBaseCalculOnCasePourcent = $('#inputBaseCalculOnCasePourcent');

    cmbResearchType = $('#cmbResearchType');
    cmbTarif = $('#cmbTarif');

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');
    lblGeneratorAct = $('#lblGeneratorAct');
    lblBudgetArticle = $('#lblBudgetArticle');
    lblExampleBase = $('#lblExampleBase');
    lblUniteBase = $('#lblUniteBase');
    lblUniteBase2 = $('#lblUniteBase2');
    lblUnityBaseCalcul = $('#lblUnityBaseCalcul');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    lbl4 = $('#lbl4');

    typeTaxationModal = $('#typeTaxationModal');
    cmbTypeTaxation = $('#cmbTypeTaxation');
    btnSelectedTypeTaxation = $('#btnSelectedTypeTaxation');

    lblMarque1 = $('#lblMarque1');
    lblPlaque1 = $('#lblPlaque1');
    lblDescription1 = $('#lblDescription1');
    lblEtat1 = $('#lblEtat1');

    lblMarque = $('#lblMarque');
    lblPlaque = $('#lblPlaque');
    lblDescription = $('#lblDescription');
    lblEtat = $('#lblEtat');
    btnCallModalSearchMoto = $('#btnCallModalSearchMoto');

    btnResearchBudgetArticle = $('#btnResearchBudgetArticle');
    btnAddBudgetArticle = $('#btnAddBudgetArticle');
    btnSaveTaxation = $('#btnSaveTaxation');
    btnRemoveTaxation = $('#btnRemoveTaxation');
    btnSelectedBudgetArticle = $('#btnSelectedBudgetArticle');
    btnSelectedResponsible = $('#btnSelectedResponsible');
    btnSearchBudgetArticle = $('#btnSearchBudgetArticle');
    btnSelectedTarif = $('#btnSelectedTarif');
    btnValidateInfoBase = $('#btnValidateInfoBase');
    btnAssociateBien = $('#btnAssociateBien');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    responsibleModal = $('#responsibleModal');
    budgetArticleModal = $('#budgetArticleModal');
    infosBaseCalculModal = $('#infosBaseCalculModal');
    tarifBudgetArticleModal = $('#tarifBudgetArticleModal');

    modalTransactionnel = $('#modalTransactionnel');
    inputMontanTransactionnel = $('#inputMontanTransactionnel');
    lblTransactionnelValue = $('#lblTransactionnelValue');
    btnValidateTTransactionnel = $('#btnValidateTTransactionnel');

    lblZoneBien = $('#lblZoneBien');
    lblSelectedBien = $('#lblSelectedBien');

    motoCycleModal = $('#motoCycleModal');
    tableMotoCycle = $('#tableMotoCycle');
    btnSelectedMoto = $('#btnSelectedMoto');


    lblZoneBien.attr('style', 'display: none');

    cmbTarif.on('change', function (e) {
        codeTarif = cmbTarif.val();
    });

    cmbTypeTaxation.on('change', function (e) {

        cmbTypeTaxationId = cmbTypeTaxation.val();

        if (cmbTypeTaxation.val() == '0') {

            alertify.alert('Veuillez d\'abord sélectionner un type de taxation');
            return;

        }
    });

    btnSelectedTypeTaxation.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbTypeTaxationId == empty || cmbTypeTaxationId == null) {
            alertify.alert('Veuillez d\'abord sélectionner un type de taxation');
            return;
        }

        loadBudgetArticlesConducteurMoto(cmbTypeTaxationId);
    });



    inputBaseCalcul.on('change', function (e) {
        calculateBaseCalcul('B');
    });

    inputQuantity.on('change', function (e) {
        calculateBaseCalcul('Q');
    });

    btnValidateInfoBase.on('click', function (e) {
        e.preventDefault();
        validateBaseCalcul();
    });

    btnCallModalSearchMoto.on('click', function (e) {
        e.preventDefault();
        callMotoCycleModal();
    });

    btnSelectedMoto.on('click', function (e) {
        e.preventDefault();
        getCurrentMotoCycle();
    });

    btnSelectedTarif.on('click', function (e) {
        e.preventDefault();
        displayInfosPalierAndBudgetArticle();
    });

    btnSearchBudgetArticle.on('click', function (e) {
        e.preventDefault();
        callModalResearchBudgetArticles();
    });

    btnSelectedBudgetArticle.on('click', function (e) {
        e.preventDefault();
        getCurrentBudgetArticle();
    });

    btnValidateTTransactionnel.on('click', function (e) {
        e.preventDefault();
        showTransactionnelTaux();
    });

    btnCallModalSearchResponsable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        assujettiModal.modal('show');
    });

    btnResearchBudgetArticle.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadBudgetArticles();
    });

    btnAddBudgetArticle.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeBdgetArticle === empty) {
            alertify.alert('Veuillez d\'abord rechercher et sélectionner un article budgétaire avant d\'ajouter dans le panier !');
            return;
        }

        if (inputBaseCalcul.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir la valeur de la base de calcul.');
            return;
        }

        if (inputQuantity.val() === empty) {
            alertify.alert('Veuillez d\'abord fournir la valeur de la quantité.');
            return;
        }

        if (inputAmount.val() === empty || amount == 0) {
            alertify.alert('Le montant dû est invalide.');
            return;
        }

        if (listTaxationArticle.length > 0) {
            if (accept_N_ArticleToPanier == '0') {
                alertify.confirm('Vous avez déjà ajouter un autre article budgétaire dans le panier. Voulez-vous le remplacer ?', function () {
                    listTaxationArticle = new Array();
                    erase = '1';
                    insertBudgetArticleInTaxationList();
                    return;
                });
            } else {
                erase = '0';
                insertBudgetArticleInTaxationList();
                return;
            }
        } else {
            erase = '0';
            insertBudgetArticleInTaxationList();
            return;
        }

    });

    btnSaveTaxation.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (cmbTypeTaxationId === '2' || cmbTypeTaxationId === '3') {

            if (motoCycleId == empty || isUndefined(motoCycleId)) {
                alertify.alert('Désole, veuillez d\'abord sélectionner une moto avant de poursuivre l\'opération');
                return;
            }
        } else {
            motoCycleId = empty;
        }


        switch (cmbTypeTaxationId) {
            case '1':
                for (i = 0; i < listTaxationArticle.length; i++) {
                    if (listTaxationArticle[i].taux == empty) {

                        switch (i) {
                            case 0:
                                alertify.alert('Désole, veuillez d\'abord sélectionner le tarif de l\'article budgétaire : '
                                        + listTaxationArticle[i].budgetArticleText);
                                return;
                                break;

                        }

                    }
                }
                break;
            case '2':
                for (i = 0; i < listTaxationArticle.length; i++) {
                    if (listTaxationArticle[i].taux == empty) {

                        switch (i) {
                            case 0:
                                alertify.alert('Désole, veuillez d\'abord sélectionner le tarif de l\'article budgétaire : '
                                        + listTaxationArticle[i].budgetArticleText);
                                return;
                                break;
                            case 1:
                                alertify.alert('Désole, veuillez d\'abord sélectionner le tarif de l\'article budgétaire : '
                                        + listTaxationArticle[i].budgetArticleText);
                                return;
                                break;

                        }

                    }
                }
                break;
            case '3':

                for (i = 0; i < listTaxationArticle.length; i++) {
                    if (listTaxationArticle[i].taux == empty) {

                        switch (i) {
                            case 0:
                                alertify.alert('Désole, veuillez d\'abord sélectionner le tarif de l\'article budgétaire : '
                                        + listTaxationArticle[i].budgetArticleText);
                                return;
                                break;
                            case 1:
                                alertify.alert('Désole, veuillez d\'abord sélectionner le tarif de l\'article budgétaire : '
                                        + listTaxationArticle[i].budgetArticleText);
                                return;
                                break;
                            case 2:
                                alertify.alert('Désole, veuillez d\'abord sélectionner le tarif de l\'article budgétaire : '
                                        + listTaxationArticle[i].budgetArticleText);
                                return;
                                break;
                        }

                    }
                }

                break;
        }

        callDialogConfirmSaveNoteCalcul();
    });

    inputValueResearchBudgetArticle.keypress(function (e) {
        if (e.keyCode === 13) {
            btnResearchBudgetArticle.trigger('click');
        }
    });

    inputBaseCalculOnCasePourcent.keypress(function (e) {
        if (e.keyCode === 13) {
            btnValidateInfoBase.trigger('click');
        }
    });


    btnAssociateBien.click(function (e) {

        e.preventDefault();

        if (codeResponsible == empty) {

            alertify.alert('Veuillez d\'abord rechercher et sélectionner un assujetti.');
            return;
        }

        loadBiensPersonne(codeResponsible);
    });

    printResultBudgetArticle('');
    printTaxationBudgetArticle('');

    typeTaxationModal.modal('show');

});

function selectPersonneBien() {

    lblZoneBien.attr('style', 'display: inline');
    codeBien = selectAssujettiBien.id;
    lblSelectedBien.html(selectAssujettiBien.libelle);
}

function calculateBaseCalcul(type) {

    switch (type) {
        case 'Q':
            if (inputQuantity === '') {
                alertify.alert('Veuillez d\'abord fournir le nombre d\'actes');
                return;
                break;
            }
        case 'B':
            if (inputBaseCalcul === '') {
                alertify.alert('Veuillez d\'abord fournir la base de calcul');
                return;
                break;
            }
    }

    if (isTransactionnel == 1) {
        amount = tauxPalier * inputQuantity.val();
        inputAmount.val(formatNumber(amount, devise));
        return;
    }

    switch (typeTaux) {
        case 'F':
            amount = tauxPalier * inputQuantity.val() * inputBaseCalcul.val();
            inputAmount.val(formatNumber(amount, devise));
            if (multiplier == '0') {
                inputBaseCalcul.attr('disabled', true);
            } else {
                inputBaseCalcul.attr('disabled', false);
            }
            break;
        case '%':
            amount = inputBaseCalcul.val() * (tauxPalier / 100) * inputQuantity.val();
            inputAmount.val(formatNumber(amount, devise));
            inputBaseCalcul.attr('disabled', false);
            break;
    }
}

function validateBaseCalcul() {

    amount = 0;

    if (inputBaseCalculOnCasePourcent.val() === '') {
        alertify.alert('Veuillez d\'abord fournir la base de calcul');
        return;
    }

    infosBaseCalculModal.modal('hide');

    baseCalculOnCaseIsPalier = inputBaseCalculOnCasePourcent.val();

    switch (isPalier) {
        case 0:
            if (typeTaux == 'F') {
                inputRate.val(tauxPalier);
                inputBaseCalcul.val(baseCalculOnCaseIsPalier);
                amount = (baseCalculOnCaseIsPalier * inputQuantity.val() * tauxPalier);
                inputAmount.val(formatNumber(amount, devise));
            } else if (typeTaux == '%') {
                inputRate.val(tauxPalier + ' (' + typeTaux + ')');
                inputBaseCalcul.val(baseCalculOnCaseIsPalier);
                amount = (baseCalculOnCaseIsPalier * (tauxPalier / 100) * inputQuantity.val());
                inputAmount.val(formatNumber(amount, devise));
            }
            inputBaseCalcul.attr('disabled', false);
            break;
        case 1:
            inputBaseCalcul.val(baseCalculOnCaseIsPalier);
            loadTarifByBudgetArticleAndBase(codeBdgetArticle, baseCalculOnCaseIsPalier);
            inputBaseCalcul.val('1');
            break;
    }

}

function displayInfosPalierAndBudgetArticle() {

    if (codeTarif === '0') {
        alertify.alert('Veuillez d\'abord sélectionner un tarif.');
        return;
    }

    tarifBudgetArticleModal.modal('hide');

    if (dataTarifs.length > 1) {

        for (var i = 0; i < dataTarifs.length; i++) {
            if (dataTarifs[i].codeTarif === codeTarif) {
                showSelectedTaux(dataTarifs[i]);
                return;
            }

        }
    }

}

function insertBudgetArticleInTaxationList() {

    if (erase == '0') {
        alertify.confirm('Etes-vous sûre de vouloir ajouter cet article budgétaire dans le panier ?', function () {
            addBudgetArticleInTaxationList();
        });
        return;
    } else if (erase == '1') {
        addBudgetArticleInTaxationList();
    }
}

function addBudgetArticleInTaxationListV2(result) {

    for (i = 0; i < result.length; i++) {

        objetDetailNoteCalcul = new Object();

        objetDetailNoteCalcul.budgetArticleCode = result[i].codeArticleBudgetaire;
        objetDetailNoteCalcul.budgetArticleText = result[i].libelleArticleBudgetaire;
        objetDetailNoteCalcul.baseCalcul = 1;
        objetDetailNoteCalcul.unity = result[i].libelleUnite; //codeUnite
        objetDetailNoteCalcul.quantity = 1;
        objetDetailNoteCalcul.devise = 'USD';
        objetDetailNoteCalcul.typeTaux = '';
        objetDetailNoteCalcul.codeTarif = '';
        objetDetailNoteCalcul.amount = 0;
        objetDetailNoteCalcul.tauxDisplay = '';
        objetDetailNoteCalcul.taux = 0;
        objetDetailNoteCalcul.palierExist = result[i].palierExist;
        objetDetailNoteCalcul.palierList = JSON.parse(result[i].palierList);


        listTaxationArticle.push(objetDetailNoteCalcul);

    }

    var totalAmout = 0;
    for (i = 0; i < listTaxationArticle.length; i++) {
        totalAmout += listTaxationArticle[i].amount;
    }

    labelTotalAmount.attr('style', 'display: inline');
    labelTotalAmount.html(formatNumber(totalAmout, 'USD'));

    printTaxationBudgetArticle(listTaxationArticle);

}

function addBudgetArticleInTaxationList() {

    objetDetailNoteCalcul = new Object();

    objetDetailNoteCalcul.budgetArticleCode = codeBdgetArticle;
    objetDetailNoteCalcul.budgetArticleText = libelleArticleBudgetaire;
    objetDetailNoteCalcul.baseCalcul = inputBaseCalcul.val();
    objetDetailNoteCalcul.unity = uniteBudgetArticle;
    objetDetailNoteCalcul.quantity = inputQuantity.val();
    objetDetailNoteCalcul.devise = devise;
    objetDetailNoteCalcul.typeTaux = typeTaux;
    objetDetailNoteCalcul.codeTarif = codeTarif;

    switch (typeTaux) {
        case 'F':
            objetDetailNoteCalcul.amount = (tauxPalier * inputQuantity.val() * inputBaseCalcul.val());
            objetDetailNoteCalcul.taux = tauxPalier;
            objetDetailNoteCalcul.tauxDisplay = tauxPalier;
            break;
        case '%':
            objetDetailNoteCalcul.amount = (inputBaseCalcul.val() * (tauxPalier / 100) * inputQuantity.val());
            objetDetailNoteCalcul.tauxDisplay = tauxPalier + ' (' + typeTaux + ')';
            objetDetailNoteCalcul.taux = tauxPalier;
            break;
    }

    if (listTaxationArticle.length > 0) {
        for (i = 0; i < listTaxationArticle.length; i++) {

            if (codeBdgetArticle === listTaxationArticle[i].budgetArticleCode
                    && codeTarif === listTaxationArticle[i].codeTarif) {
                alertify.alert('Cet article budgétaire et tarif se trouvent déjà dans le panier.');
                return;
            }
        }
    }

    listTaxationArticle.push(objetDetailNoteCalcul);

    var totalAmout = 0;
    for (i = 0; i < listTaxationArticle.length; i++) {
        totalAmout += listTaxationArticle[i].amount;
    }

    printTaxationBudgetArticle(listTaxationArticle);
    labelTotalAmount.html(formatNumber(totalAmout, devise));

    amount = 0;
    isTransactionnel = 0;
    transacMinimum = 0;
    transacMaximum = 0;
    lblUniteBase.html(empty);
    lblUnityBaseCalcul.html(empty);
    lblUniteBase2.html(empty);
    lblBudgetArticle.text(empty);
    inputRate.val(empty);
    inputBaseCalcul.val('1');
    inputQuantity.val('1');
    inputAmount.val(empty);
    codeBdgetArticle = empty;
    inputBaseCalculOnCasePourcent.val(empty);
    libelleArticleBudgetaire = empty;
    uniteBudgetArticle = empty;
    dataArrayBudgetArticles = empty;
    infoBudgetArticle = {};
    infoMotoCycle = {};
    inputValueResearchBudgetArticle.val(empty);
    printResultBudgetArticle(empty);
    btnSelectedBudgetArticle.hide();
    objetDetailNoteCalcul = new Object();
}

function callModalResearchBudgetArticles() {

    if (codeResponsible === empty) {
        alertify.alert('Veuillez d\'abord rechercher et sélectionner un assujetti.');
        return;
    }

    budgetArticleModal.modal('show');
}

function loadBudgetArticles() {
    if (inputValueResearchBudgetArticle.val() === empty) {
        alertify.alert('Veuillez fournir le critère de recherche de l\'article budgétaire.');
        return;
    }

    var viewAllService = controlAccess('SEARCH_FOR_ALL_SERVICES');

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchBudgetArticle.val(),
            'seeEverything': viewAllService,
            'codeService': userData.serviceCode,
            'operation': 'researchBudgetArticle'
        },
        beforeSend: function () {
            budgetArticleModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {
                budgetArticleModal.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectedBudgetArticle.hide();
                    printResultBudgetArticle('');
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                } else {
                    var dataBudgetArticle = JSON.parse(JSON.stringify(response));

                    if (dataBudgetArticle.length > 0) {
                        btnSelectedBudgetArticle.show();
                        printResultBudgetArticle(dataBudgetArticle);
                    } else {
                        btnSelectedBudgetArticle.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            budgetArticleModal.unblock();
            showResponseError();
        }
    });
}

function loadResponsibles() {

    if (inputValueResearchResponsible.val() === '') {
        alertify.alert('Veuillez fournir le critère de recherche de l\'assujetti.');
        return;
    }
    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchResponsible.val(),
            'operation': 'researchResponsible'
        },
        beforeSend: function () {
            responsibleModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {
                responsibleModal.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnSelectedResponsible.hide();
                    printResultResponsible('');
                    alertify.alert('Aucune donnée ne correspond au critère de recherche fournis.');
                    return;
                } else {
                    var dataResponsible = JSON.parse(JSON.stringify(response));
                    if (dataResponsible.length > 0) {
                        btnSelectedResponsible.show();
                        printResultResponsible(dataResponsible);
                    } else {
                        btnSelectedResponsible.hide();
                    }
                }

            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            responsibleModal.unblock();
            showResponseError();
        }

    });
}

function printTaxationBudgetArticle(taxationBudgetArticle) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">Article budgétaire</th>';
    tableContent += '<th style="text-align:left" >Tarif</th>';
    tableContent += '<th style="text-align:center">Taux</th>';
    tableContent += '<th style="text-align:center">Nbre actes</th>';
    tableContent += '<th style="text-align:right">Montant Dû</th>';
    //tableContent += '<th style="text-align:center"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < taxationBudgetArticle.length; i++) {

        var firstLineAB = '';
        if (taxationBudgetArticle[i].budgetArticleText.length > 50) {
            firstLineAB = taxationBudgetArticle[i].budgetArticleText.substring(0, 50).toUpperCase() + ' ...';
        } else {
            firstLineAB = taxationBudgetArticle[i].budgetArticleText.toUpperCase();
        }

        var items = '<option value="0">--</option>';
        var palierList = taxationBudgetArticle[i].palierList;
        for (var j = 0; j < palierList.length; j++) {
            items += '<option value="' + palierList[j].codeTarif + '">' + palierList[j].libelleTarif + '</option>';
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:38%;vertical-align:middle">' + taxationBudgetArticle[i].budgetArticleText + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle"><select onchange="selectTarif(\'' + taxationBudgetArticle[i].budgetArticleCode + '\',\'' + i + '\')" class="form-control" id="selectTarif_' + i + '" style="width: 400px">' + items + '</select></td>';

        tableContent += '<td style="text-align:right;width:10%;vertical-align:middle"><input style="text-align:center;width:120px" readOnly="true" id="tauxColumn_' + i + '" ></input></td>';

        tableContent += '<td style="text-align:center;width:11;vertical-align:middle">' + taxationBudgetArticle[i].quantity + '</td>';

        tableContent += '<td style="text-align:right;width:13%;vertical-align:middle"><input style="text-align:right;font-weight:bold;width:150px" readOnly="true" id="amountColumn_' + i + '" ></input></td>';

        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableTaxationBudgetArticles.html(tableContent);

    var myDataTable = tableTaxationBudgetArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des articles budgétaires est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true, searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}


function selectTarif(codeAb, indice) {

    //alert(JSON.stringify(listTaxationArticle));

    var valueTarif = $('#selectTarif_' + indice).val();

    if (valueTarif === '0') {

        if ($('#tauxColumn_' + indice).val() !== '') {
            totalAmout2 -= $('#tauxColumn_' + indice).val();
            labelTotalAmount.html(formatNumber(totalAmout2, 'USD'));
        }

        $('#tauxColumn_' + indice).val(empty);
        $('#amountColumn_' + indice).val(empty);

        alertify.alert('Désoler, veuillez sélectionner un tarif valide');
        return;

    } else {

        var valueTaux = $('#tauxColumn_' + indice);
        var valueAmount = $('#amountColumn_' + indice);

        for (var i = 0; i < abConducteurMotoList.length; i++) {

            if (abConducteurMotoList[i].codeArticleBudgetaire === codeAb) {

                var listTarif = JSON.parse(abConducteurMotoList[i].palierList);

                for (var j = 0; j < listTarif.length; j++) {

                    var amount = '';
                    var amountDisplay = '';
                    var taux = '';

                    if (listTarif[j].codeTarif === valueTarif) {

                        switch (listTarif[j].typeTaux) {
                            case 'F':
                                amount = (listTarif[j].taux * 1 * 1);
                                taux = listTarif[j].taux;
                                amountDisplay = formatNumber(amount, listTarif[j].devise);
                                break;

                            case '%':
                                amount = (1 * (listTarif[j].taux / 100) * 1);
                                taux = listTarif[j].taux;
                                amountDisplay = formatNumber(amount, listTarif[j].devise);
                                break;
                        }

                        valueTaux.val(taux);
                        valueAmount.val(amountDisplay);

                        for (var k = 0; k < listTaxationArticle.length; k++) {

                            if (listTaxationArticle[k].budgetArticleCode == codeAb) {


                                listTaxationArticle[k].devise = listTarif[j].devise;
                                listTaxationArticle[k].typeTaux = listTarif[j].typeTaux;
                                listTaxationArticle[k].codeTarif = valueTarif;
                                listTaxationArticle[k].amount = amount;
                                listTaxationArticle[k].taux = listTarif[j].taux;
                                listTaxationArticle[k].tauxDisplay = listTarif[j].taux;

                                //totalAmout += listTaxationArticle[i].amount;

                            }
                        }
                        //alert(JSON.stringify(listTaxationArticle));
                    }
                }

                totalAmout2 += amount;
            }
        }

        labelTotalAmount.html(formatNumber(totalAmout2, 'USD'));
    }
}

function printResultBudgetArticle(dataBudgetArticle) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th scope="col">Article budgétaire</th>';
    tableContent += '<th scope="col">Service d\'assiètte</th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';
    var firstLineAB = '';
    for (var i = 0; i < dataBudgetArticle.length; i++) {

        if (dataBudgetArticle[i].libelleArticleBudgetaire.length > 28) {
            firstLineAB = dataBudgetArticle[i].libelleArticleBudgetaire.substring(0, 28).toUpperCase() + ' ...';
        } else {
            firstLineAB = dataBudgetArticle[i].libelleArticleBudgetaire.toUpperCase();
        }

        tableContent += '<tr>';
        tableContent += '<td title="' + dataBudgetArticle[i].libelleArticleBudgetaire + '">' + firstLineAB + '</td>';
        tableContent += '<td>' + dataBudgetArticle[i].libelleService + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].codeArticleBudgetaire + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].palier + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].libelleUnite + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].addNArticleToPanier + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnel + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnelMinimum + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].transactionnelMaximum + '</td>';
        tableContent += '<td hidden="true">' + dataBudgetArticle[i].codeService + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableBudgetArticles.html(tableContent);

    var myDataTable = tableBudgetArticles.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true, searching: false,
        paging: true, lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os', blurable: true
        },
        datalength: 3
    });

    $('#tableBudgetArticles tbody').on('click', 'tr', function () {
        infoBudgetArticle = myDataTable.row(this).data();
    });
}

function getCurrentBudgetArticle() {

    if (infoBudgetArticle.code === '') {
        alertify.alert('Veuillez d\'abord sélectionner un article budgétaire');
        return;
    }

    dataArrayBudgetArticles = infoBudgetArticle;

    codeBdgetArticle = dataArrayBudgetArticles[2];
    libelleArticleBudgetaire = dataArrayBudgetArticles[0];
    uniteBudgetArticle = dataArrayBudgetArticles[4];
    accept_N_ArticleToPanier = dataArrayBudgetArticles[5];
    isTransactionnel = dataArrayBudgetArticles[6];

    transacMinimum = dataArrayBudgetArticles[7];
    transacMaximum = dataArrayBudgetArticles[8];
    codeServiceAB = dataArrayBudgetArticles[9];

    inputAmount.val('');
    inputRate.val('');
    inputBaseCalculOnCasePourcent.val('');
    inputQuantity.val('1');
    inputBaseCalcul.val('1');

    lblUniteBase2.html('(' + uniteBudgetArticle + ')');
    budgetArticleModal.modal('hide');

    if (isTransactionnel == 1) {
        lblBudgetArticle.html('Article budgétaire : ' + libelleArticleBudgetaire);
        lblTransactionnelValue.html('Veuillez saisir un montant compris entre ' + formatNumber(transacMinimum, 'CDF') + ' et ' + formatNumber(transacMaximum, 'CDF'));
        inputMontanTransactionnel.val('1');
        lblUnityBaseCalcul.html('(' + uniteBudgetArticle + ')');
        codeTarif = '0000000420';
        typeTaux = 'F';
        devise = 'CDF';
        modalTransactionnel.modal('show');
        return;
    }

    if (dataArrayBudgetArticles[3] === '1') {
        isPalier = 1;
        inputBaseCalculOnCasePourcent.val('1');
        infosBaseCalculModal.modal('show');

    } else {
        isPalier = 0;
        loadTarifByBudgetArticleAndBase(codeBdgetArticle, 0);
    }
}

function getSelectedAssujetiData() {

    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);
    codeResponsible = selectAssujettiData.code;
    lblNifResponsible.html(selectAssujettiData.nif);
    codeFormeJuridique = selectAssujettiData.codeForme;
    adresseId = selectAssujettiData.codeAdresse;

    lbl1.show();
    lbl2.show();
    lbl3.show();
    lbl4.show();

    responsibleObject = new Object();

    responsibleObject.codeResponsible = codeResponsible;
    responsibleObject.codeFormeJuridique = codeFormeJuridique;
    responsibleObject.adresseId = adresseId;

    //loadBudgetArticlesConducteurMoto();
    loadMotoCycleListByPersonne(codeResponsible);

}

function loadMotoCycleListByPersonne(code) {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'codePersonne': code,
            'operation': 'loadMotoCycle'
        },
        beforeSend: function () {
            //$.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();

                if (response == '-1' || response == '0') {

                    return;

                } else {
                    listMotoCycle = JSON.parse(JSON.stringify(response));
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadTarifByBudgetArticleAndBase(budgetArticleCode, baseCalcul) {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'codeArticleBudgetaire': budgetArticleCode,
            'codeFormeJuridique': codeFormeJuridique,
            'baseCalcul': baseCalcul,
            'palier': isPalier,
            'operation': 'researchRateArticle'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();

                if (response == '-1') {
                    alertify.alert('Une erreur inattendue est survenue. Veuillez réesayer ou contacter l\'administrateur.');
                    return;
                }

                dataTarifs = JSON.parse(JSON.stringify(response));

                if (dataTarifs.length < 1) {
                    alertify.alert('Aucun tarif trouvé. Veuillez configurer correctement l\'article budgétaire');
                    return;
                }

                if (dataTarifs.length > 1) {
                    consituateTarifList();

                } else {

                    showSelectedTaux(dataTarifs[0]);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function consituateTarifList() {

    var dataTarifDisplay = '<option value ="0">-- Sélectionner un tarif --</option>';
    for (var i = 0; i < dataTarifs.length; i++) {
        dataTarifDisplay += '<option value =' + dataTarifs[i].codeTarif + '>' + dataTarifs[i].libelleTarif + '</option>';
    }
    cmbTarif.html(dataTarifDisplay);
    budgetArticleModal.modal('hide');
    tarifBudgetArticleModal.modal('show');
}

function callDialogConfirmSaveNoteCalcul() {
    if (listTaxationArticle.length > 0) {
        alertify.confirm('Etes-vous sûre de vouloir enregistrer cette taxation ?', function () {
            saveNoteCalcul();
        });
    } else {
        alertify.alert('Veuillez d\'abord ajouter un article budgétaire dans le panier.');
    }
}

function emptyDataTaxation() {
    listTaxationArticle = [];
    responsibleObject = {};
    objetDetailNoteCalcul = {};
}
function saveNoteCalcul() {

    var taxations = JSON.stringify(listTaxationArticle);

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'codeResponsible': responsibleObject.codeResponsible,
            'codeAdresse': responsibleObject.adresseId,
            'codeSite': userData.SiteCode,
            'userId': userData.idUser,
            'depotDeclaration': null, 'typeTaxation': typeTaxation,
            'codeService': userData.serviceCode,
            'detailTaxation': taxations,
            'codeBien': motoCycleId,
            'nbreTaxe': cmbTypeTaxationId,
            'operation': 'saveNoteCalcul2'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Création de la note de taxation en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                }
                if (response == '0') {
                    alertify.alert('Echec opération. Une erreur s\'est produite lors de la création de la note de taxation !');
                    return;
                } else {

                    alertify.alert('La taxaion s\est effectuée avec succès.');

                    listTaxationArticle = new Array();
                    objetDetailNoteCalcul = new Object();

                    setDocumentContent(response);
                    window.open('visualisation-document', '_blank');

                    setTimeout(function () {
                        window.location = 'taxation-carte-conducteur-moto';
                    }, 3000);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function removeBudgetArticle(codeBudgetArticle, codeTarif) {

    alertify.confirm('Etes-vous sûre de vouloir retirer cet article budgétaire dans le panier ?', function () {
        for (i = 0; i < listTaxationArticle.length; i++) {

            if (codeBudgetArticle === listTaxationArticle[i].budgetArticleCode
                    && codeTarif === listTaxationArticle[i].codeTarif) {

                listTaxationArticle.splice(i, 1);

                var totalAmout = 0;
                for (i = 0; i < listTaxationArticle.length; i++) {
                    totalAmout += listTaxationArticle[i].amount;
                }

                printTaxationBudgetArticle(listTaxationArticle);
                labelTotalAmount.html(formatNumber(totalAmout, devise));

                return;
            }
        }
    });

}

function showTransactionnelTaux() {

    var value = parseFloat(inputMontanTransactionnel.val());

    if (value < transacMinimum) {
        alertify.alert('Le montant dû doit être supérieur à ' + formatNumber(transacMinimum, devise));
        return;
    }

    if (value > transacMaximum) {
        alertify.alert('Le montant dû doit être inférieur à ' + formatNumber(transacMaximum, devise));
        return;
    }

    modalTransactionnel.modal('hide');

    inputRate.val(value);
    amount = value;
    inputAmount.val(formatNumber(value, devise));
    tauxPalier = amount;
    inputBaseCalcul.attr('disabled', true);

}

function showSelectedTaux(tarifObj) {

    codeTarif = tarifObj.codeTarif;
    lblBudgetArticle.html('Article budgétaire : ' + libelleArticleBudgetaire + ' : ' + tarifObj.libelleTarif.toUpperCase());
    libelleArticleBudgetaire = libelleArticleBudgetaire + ' : ' + tarifObj.libelleTarif.toUpperCase();
    devise = tarifObj.devisePalier;
    typeTaux = tarifObj.typeTaux;
    multiplier = tarifObj.multiplierParValeurBase;
    tauxPalier = tarifObj.tauxPalier;

    inputRate.val(tauxPalier);
    inputQuantity.val('1');
    lblUnityBaseCalcul.html('(' + uniteBudgetArticle + ')');
    lblUniteBase2.html('(' + tarifObj.uniteLibellePalier + ')');
    switch (typeTaux) {
        case 'F':
            if (multiplier == '0') {
                inputBaseCalcul.val('1');
                inputBaseCalcul.attr('disabled', true);
                amount = (tarifObj.tauxPalier * inputQuantity.val() * inputBaseCalcul.val());
                inputAmount.val(formatNumber(amount, devise));
            } else {
                inputBaseCalcul.val('1');
                inputBaseCalcul.attr('disabled', false);
                amount = (tauxPalier * inputQuantity.val() * inputBaseCalcul.val());
                inputAmount.val(formatNumber(amount, devise));

                if (isPalier == 0) {
                    infosBaseCalculModal.modal('show');
                }

            }
            break;
        case '%':
            inputRate.val(tauxPalier + ' (' + typeTaux + ')');
            amount = inputBaseCalcul.val() * (tauxPalier / 100) * inputQuantity.val();
            inputAmount.val(formatNumber(amount, devise));
            if (isPalier == 0) {
                infosBaseCalculModal.modal('show');
            }
            break;
    }

}

function loadBudgetArticlesConducteurMoto(nbre) {

    nbreTax = nbre;

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'researchAbConducteurMoto',
            'nbreTaxe': nbre
        },
        beforeSend: function () {
            //budgetArticleModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {

                abConducteurMotoList = JSON.parse(JSON.stringify(response));
                addBudgetArticleInTaxationListV2(abConducteurMotoList);

                typeTaxationModal.modal('hide');
            }

        },
        complete: function () {
        },
        error: function () {
            budgetArticleModal.unblock();
            showResponseError();
        }
    });


}

function callMotoCycleModal() {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> ID </th>';
    header += '<th scope="col"> INTITUlE </th>';
    header += '<th scope="col"> DESCRIPTION </th>';
    header += '<th hidden="true"scope="col"> Propriétaire </th>';
    header += '<th hidden="true" scope="col">Id bien</th>';

    var body = '<tbody>';

    if (listMotoCycle.length > 0) {
        btnSelectedMoto.attr('style', 'display:block');
    } else {
        btnSelectedMoto.attr('style', 'display:none');
    }



    for (var i = 0; i < listMotoCycle.length; i++) {

        var cv = '';

        if (listMotoCycle[i].CV == empty) {
            cv = '--';
        }


        var intituleMoto = '<span style="font-weight: bold">' + listMotoCycle[i].intituleBien.toUpperCase() + '</span>' + '<br/><br/>' + '<span style="color:red;font-style: italic;font-size:13px">' + listMotoCycle[i].description + '</span>';

        var description = 'N° plaque : ' + '<span style="font-weight: bold">' + listMotoCycle[i].IMMATRICULATION.toUpperCase() + '</span>' + '<br/>'
                + 'Marque : ' + '<span style="font-weight: bold">' + listMotoCycle[i].MARQUE.toUpperCase() + '</span>' + '<br/>' + 'Couleur : '
                + '<span style="font-weight: bold">' + listMotoCycle[i].COULEUR.toUpperCase() + '</span>' + '<br/>' + 'N° Chassis : ' + '<span style="font-weight: bold">' + listMotoCycle[i].NUMEROCHASIS.toUpperCase() + '</span>' + '<br/>'
                + 'CV : ' + '<span style="font-weight: bold">' + cv.toUpperCase() + '</span>';

        body += '<tr>';
        body += '<td style="text-align:left;width:10%;vertical-align:middle">' + listMotoCycle[i].idBien + '</td>';
        body += '<td style="text-align:left;width:20%;vertical-align:middle">' + intituleMoto + '</td>';
        body += '<td style="text-align:left;width:30%;vertical-align:middle">' + description + '</td>';
        body += '<td hidden="true">' + listMotoCycle[i].idBien + '</td>';
        body += '<td hidden="true">' + listMotoCycle[i].idBien + '</td>';
        body += '</tr>';

    }

    body += '</tbody>';

    var tableContent = header + body;

    tableMotoCycle.html(tableContent);

    var dtBien = tableMotoCycle.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 3,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableMotoCycle tbody').on('click', 'tr', function () {
        var data = dtBien.row(this).data();
        isSelect = true;
        selectBien = {};

        selectBien.id = data[0];

    });

    motoCycleModal.modal('show');

}

function getCurrentMotoCycle() {

    if (selectBien.id === '') {
        alertify.alert('Veuillez d\'abord sélectionner une moto');
        return;
    }

    for (var i = 0; i < listMotoCycle.length; i++) {

        if (listMotoCycle[i].idBien == selectBien.id) {

            motoCycleId = selectBien.id;

            //lblMarque1.attr('style', 'display:block');
            //lblPlaque1.attr('style', 'display:block');
            //lblDescription1.attr('style', 'display:block');
            //lblEtat1.attr('style', 'display:block');

            if (listMotoCycle[i].MARQUE == empty) {

                lblMarque.html('Moto : ' + listMotoCycle[i].intituleBien.toUpperCase());
            } else {
                lblMarque.html('Moto : ' + listMotoCycle[i].MARQUE + ' (' + listMotoCycle[i].intituleBien.toUpperCase() + ')');
            }

            if (listMotoCycle[i].IMMATRICULATION == empty) {
                lblPlaque.html('N° plaque : ' + empty);
            } else {
                lblPlaque.html('N° plaque : ' + listMotoCycle[i].IMMATRICULATION);
            }

            if (listMotoCycle[i].description == empty) {
                lblDescription.html('Description : ' + empty);
            } else {
                lblDescription.html('Description : ' + listMotoCycle[i].description);
            }

            if (listMotoCycle[i].etat == empty) {
                lblEtat.html('Etat : ' + ' -- ');
            } else {
                lblEtat.html('Etat : ' + ' -- ');
            }

            motoCycleModal.modal('hide');
        }
    }


}
