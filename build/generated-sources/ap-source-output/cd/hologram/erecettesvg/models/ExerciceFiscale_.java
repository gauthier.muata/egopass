package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ExerciceFiscale.class)
public class ExerciceFiscale_ { 

    public static volatile SingularAttribute<ExerciceFiscale, Date> debut;
    public static volatile SingularAttribute<ExerciceFiscale, String> agentMaj;
    public static volatile SingularAttribute<ExerciceFiscale, String> code;
    public static volatile SingularAttribute<ExerciceFiscale, Date> dateCreat;
    public static volatile SingularAttribute<ExerciceFiscale, Date> fin;
    public static volatile SingularAttribute<ExerciceFiscale, String> agentCreat;
    public static volatile SingularAttribute<ExerciceFiscale, String> intitule;
    public static volatile SingularAttribute<ExerciceFiscale, Short> etat;
    public static volatile SingularAttribute<ExerciceFiscale, Date> dateMaj;

}