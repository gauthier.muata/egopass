package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailsBonAPayer.class)
public class DetailsBonAPayer_ { 

    public static volatile SingularAttribute<DetailsBonAPayer, Integer> code;
    public static volatile SingularAttribute<DetailsBonAPayer, String> fkArticleBudgetaire;
    public static volatile SingularAttribute<DetailsBonAPayer, String> fkBonAPayer;
    public static volatile SingularAttribute<DetailsBonAPayer, BigDecimal> montant;

}