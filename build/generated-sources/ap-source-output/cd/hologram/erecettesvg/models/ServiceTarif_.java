package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Tarif;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ServiceTarif.class)
public class ServiceTarif_ { 

    public static volatile SingularAttribute<ServiceTarif, String> agentMaj;
    public static volatile SingularAttribute<ServiceTarif, Tarif> tarif;
    public static volatile SingularAttribute<ServiceTarif, Date> dateCreat;
    public static volatile SingularAttribute<ServiceTarif, Service> service;
    public static volatile SingularAttribute<ServiceTarif, String> id;
    public static volatile SingularAttribute<ServiceTarif, String> agentCreat;
    public static volatile SingularAttribute<ServiceTarif, Short> etat;
    public static volatile SingularAttribute<ServiceTarif, Date> dateMaj;

}