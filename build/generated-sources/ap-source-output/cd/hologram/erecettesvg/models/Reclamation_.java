package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.DetailsReclamation;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RecoursJuridictionnel;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Reclamation.class)
public class Reclamation_ { 

    public static volatile SingularAttribute<Reclamation, String> referenceCourierReclamation;
    public static volatile SingularAttribute<Reclamation, Date> dateCreat;
    public static volatile SingularAttribute<Reclamation, Integer> etatReclamation;
    public static volatile SingularAttribute<Reclamation, String> observation;
    public static volatile SingularAttribute<Reclamation, Integer> estTraiter;
    public static volatile SingularAttribute<Reclamation, Integer> etat;
    public static volatile SingularAttribute<Reclamation, Date> dateReceptionCourier;
    public static volatile SingularAttribute<Reclamation, Personne> fkPersonne;
    public static volatile ListAttribute<Reclamation, RecoursJuridictionnel> recoursJuridictionnelList;
    public static volatile SingularAttribute<Reclamation, String> typeReclamation;
    public static volatile ListAttribute<Reclamation, DetailsReclamation> detailsReclamationList;
    public static volatile SingularAttribute<Reclamation, String> id;
    public static volatile SingularAttribute<Reclamation, Agent> agentCreat;
    public static volatile SingularAttribute<Reclamation, Integer> agentValidation;

}