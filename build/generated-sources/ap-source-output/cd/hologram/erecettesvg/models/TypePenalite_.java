package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Penalite;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TypePenalite.class)
public class TypePenalite_ { 

    public static volatile SingularAttribute<TypePenalite, String> agentMaj;
    public static volatile SingularAttribute<TypePenalite, String> code;
    public static volatile SingularAttribute<TypePenalite, Date> dateCreat;
    public static volatile ListAttribute<TypePenalite, Penalite> penaliteList;
    public static volatile SingularAttribute<TypePenalite, String> agentCreat;
    public static volatile SingularAttribute<TypePenalite, String> intitule;
    public static volatile SingularAttribute<TypePenalite, Short> etat;
    public static volatile SingularAttribute<TypePenalite, Date> dateMaj;

}