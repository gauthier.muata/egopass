package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.CarteDetailVoucher;
import cd.hologram.erecettesvg.models.CommandeCarte;
import cd.hologram.erecettesvg.models.Personne;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(CarteVoucher.class)
public class CarteVoucher_ { 

    public static volatile SingularAttribute<CarteVoucher, Personne> fkPersonne;
    public static volatile SingularAttribute<CarteVoucher, String> numeroCarte;
    public static volatile ListAttribute<CarteVoucher, CarteDetailVoucher> carteDetailVoucherList;
    public static volatile SingularAttribute<CarteVoucher, String> observation;
    public static volatile SingularAttribute<CarteVoucher, Date> dateGenerate;
    public static volatile SingularAttribute<CarteVoucher, CommandeCarte> fkCommandeCarte;
    public static volatile SingularAttribute<CarteVoucher, Integer> id;
    public static volatile SingularAttribute<CarteVoucher, Integer> etat;

}