/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var selectAdresseData;
var inputNumber, btnValiderAdresse, tableAdresses, btnAddEntite, btnRechercherAdresses,
        inputLibelle, divAdresses, modalAdresses;

var labelSelectedAdresse;
var adresse;

$(function () {


    modalAdresses = $('#modalAdresses');
    tableAdresses = $('#tableAdresses');

    inputNumber = $('#inputNumber');
    btnValiderAdresse = $('#btnValiderAdresse');
    btnAddEntite = $('#btnAddEntite');
    btnRechercherAdresses = $('#btnRechercherAdresses');
    inputLibelle = $('#inputLibelle');
    divAdresses = $('#divAdresses');
    labelSelectedAdresse = $('#labelSelectedAdresse');

    btnRechercherAdresses.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadAdresses();
    });

    btnValiderAdresse.on('click', function (e) {
        e.preventDefault();
        validateAdresse();
    });

    printAdresse('');

});

function loadAdresses() {

    if (inputLibelle.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir une adresse.');
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAdresses',
            'libelle': inputLibelle.val(),
        },
        beforeSend: function () {

            modalAdresses.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                modalAdresses.unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalAdresses.unblock();
                var adresseList = $.parseJSON(JSON.stringify(response));
                if (adresseList.length > 0) {
                    inputNumber.attr('style', 'visibility: visible');
                    btnValiderAdresse.attr('style', 'visibility: visible');
                    printAdresse(adresseList);
                } else {
                    inputNumber.attr('style', 'visibility: hidden');
                    btnValiderAdresse.attr('style', 'visibility: hidden');
                    printAdresse('');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalAdresses.unblock();
            showResponseError();
        }

    });
}

function printAdresse(adresseList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> PROVINCE </th>';
    header += '<th scope="col"> VILLE </th>';
    header += '<th hidden="true" scope="col"> DISTRICT </th>';
    header += '<th scope="col"> COMMUNE </th>';
    header += '<th scope="col"> QUARTIER </th>';
    header += '<th scope="col"> AVENUE </th>';
    header += '<th hidden="true" scope="col"> Code avenue </th>';
    header += '<th hidden="true" scope="col"> Code quartier </th>';
    header += '<th hidden="true" scope="col"> Code commune </th>';
    header += '<th hidden="true" scope="col"> Code district </th>';
    header += '<th hidden="true" scope="col"> Code ville </th>';
    header += '<th hidden="true" scope="col"> Code province </th>';
    header += '</tr></thead>';
    var body = '<tbody id="tbodyAdresses">';
    for (var i = 0; i < adresseList.length; i++) {
        body += '<tr>';
        body += '<td>' + adresseList[i].province.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].ville.toUpperCase() + '</td>';
        body += '<td hidden="true">' + i + '</td>';
        body += '<td>' + adresseList[i].commune.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].quartier.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].avenue.toUpperCase() + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeAvenue + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeQuartier + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeCommune + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeDistrict + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeVille + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeProvince + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableAdresses.html(tableContent);
    var dtAdresse = tableAdresses.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableAdresses tbody').on('click', 'tr', function () {
        var data = dtAdresse.row(this).data();
        adresse = new Object();
        adresse.codeAvenue = data[6];
        adresse.codeQuartier = data[7];
        adresse.codeCommune = data[8];
        adresse.codeDistrict = data[9];
        adresse.codeVille = data[10];
        adresse.codeProvince = data[11];
        adresse.chaine = data[0].toUpperCase() + '--> C/' + data[3].toUpperCase() + ' - Q/' + data[4].toUpperCase() + ' - Av. ' + data[5].toUpperCase();
        adresse.numero = '';
        adresse.defaut = '';
        adresse.codeAP = '';
        adresse.etat = '1';
        labelSelectedAdresse.html(adresse.chaine);
    });
}

function validateAdresse() {

    if (labelSelectedAdresse.text().trim() === '') {
        alertify.alert('Veuillez d\'abord sélectionner une adresse.');
        return;
    }

    if (inputNumber.val().trim() === '') {
        alertify.alert('Veuillez d\'abord saisir un numéro de l\'adresse sélectionner.');
        return;
    }

    var numero = inputNumber.val();

    alertify.confirm('Etes-vous sûre de vouloir confirmer cette adresse ?', function () {

        adresse.numero = numero;
        getSelectedAdresseData();
        modalAdresses.modal('hide');
    });
}