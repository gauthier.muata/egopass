package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ParamTauxVignette;
import cd.hologram.erecettesvg.models.Personne;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(FormeJuridique.class)
public class FormeJuridique_ { 

    public static volatile SingularAttribute<FormeJuridique, Boolean> visibleUtilisateur;
    public static volatile SingularAttribute<FormeJuridique, String> code;
    public static volatile ListAttribute<FormeJuridique, Personne> personneList;
    public static volatile SingularAttribute<FormeJuridique, String> intitule;
    public static volatile SingularAttribute<FormeJuridique, Boolean> etat;
    public static volatile ListAttribute<FormeJuridique, ParamTauxVignette> paramTauxVignettesList;

}