/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author WILLY
 */
@Entity
@Table(name = "T_AXE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Axe.findAll", query = "SELECT a FROM Axe a"),
    @NamedQuery(name = "Axe.findById", query = "SELECT a FROM Axe a WHERE a.id = :id"),
    @NamedQuery(name = "Axe.findByIntitule", query = "SELECT a FROM Axe a WHERE a.intitule = :intitule"),
    @NamedQuery(name = "Axe.findByEtat", query = "SELECT a FROM Axe a WHERE a.etat = :etat")})
public class Axe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 200)
    @Column(name = "INTITULE")
    private String intitule;
    @Column(name = "ETAT")
    private Integer etat;

    public Axe() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Axe other = (Axe) obj;
        return true;
    }

    @Override
    public String toString() {
        return "Axe{" + "id=" + id + '}';
    }
    
}
