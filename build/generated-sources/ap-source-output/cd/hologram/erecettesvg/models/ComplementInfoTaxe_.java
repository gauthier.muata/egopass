package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ComplementInfoTaxe.class)
public class ComplementInfoTaxe_ { 

    public static volatile SingularAttribute<ComplementInfoTaxe, String> transporteur;
    public static volatile SingularAttribute<ComplementInfoTaxe, String> fkNotePerception;
    public static volatile SingularAttribute<ComplementInfoTaxe, String> numeroPlaque;
    public static volatile SingularAttribute<ComplementInfoTaxe, String> fkNoteCalcul;
    public static volatile SingularAttribute<ComplementInfoTaxe, String> natureProduit;
    public static volatile SingularAttribute<ComplementInfoTaxe, Integer> agentValidate;
    public static volatile SingularAttribute<ComplementInfoTaxe, BigDecimal> tonnageSortie;
    public static volatile SingularAttribute<ComplementInfoTaxe, String> modePaiement;
    public static volatile SingularAttribute<ComplementInfoTaxe, BigDecimal> montantTonnageSortie;
    public static volatile SingularAttribute<ComplementInfoTaxe, Integer> id;
    public static volatile SingularAttribute<ComplementInfoTaxe, Integer> etat;
    public static volatile SingularAttribute<ComplementInfoTaxe, Date> dateValidation;

}