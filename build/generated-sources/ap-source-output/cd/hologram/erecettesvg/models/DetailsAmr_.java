package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.Penalite;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailsAmr.class)
public class DetailsAmr_ { 

    public static volatile SingularAttribute<DetailsAmr, Penalite> penalite;
    public static volatile SingularAttribute<DetailsAmr, DetailsNc> detailsNc;
    public static volatile SingularAttribute<DetailsAmr, BigDecimal> taux;
    public static volatile SingularAttribute<DetailsAmr, Amr> amr;
    public static volatile SingularAttribute<DetailsAmr, Long> id;
    public static volatile SingularAttribute<DetailsAmr, Boolean> etat;
    public static volatile SingularAttribute<DetailsAmr, BigDecimal> montantDu;

}