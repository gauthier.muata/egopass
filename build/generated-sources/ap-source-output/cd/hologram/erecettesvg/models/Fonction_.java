package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Fonction.class)
public class Fonction_ { 

    public static volatile SingularAttribute<Fonction, String> code;
    public static volatile ListAttribute<Fonction, Agent> agentList;
    public static volatile SingularAttribute<Fonction, String> intitule;
    public static volatile SingularAttribute<Fonction, Boolean> etat;

}