/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.ContentieuxBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.AcquitLiberatoireConst;
import cd.hologram.erecettesvg.constants.ContentieuxConst;
import cd.hologram.erecettesvg.constants.ControleConst;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.NotePerceptionConst;
import cd.hologram.erecettesvg.constants.PropertiesConst;
import cd.hologram.erecettesvg.constants.RecouvrementConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.Archive;
import cd.hologram.erecettesvg.models.ArchiveAccuseReception;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Decision;
import cd.hologram.erecettesvg.models.DetailFichePriseCharge;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.DetailsReclamation;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Reclamation;
import cd.hologram.erecettesvg.models.RecoursJuridictionnel;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.SuiviComptableDeclaration;
import cd.hologram.erecettesvg.models.TypeDocument;
import cd.hologram.erecettesvg.pojo.DetailsReclamationPrint;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.pojo.NewNoteCalcul;
import cd.hologram.erecettesvg.properties.PropertiesConfig;
import cd.hologram.erecettesvg.properties.PropertiesMessage;
import cd.hologram.erecettesvg.sql.SQLQueryContentieux;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.sql.SQLQueryTaxation;
import cd.hologram.erecettesvg.util.Compare;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.PrintDocument;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author WILLY KASHALA
 */
@WebServlet(name = "Contentieux", urlPatterns = {"/contentieux_servlet"})
public class Contentieux extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE);
    static PropertiesConfig propertiesConfig;

    Amr amr;
    LogUser logUser;
    JsonObject amrJson;
    JsonObject reclamationJson;
    JsonObject recoursJson;
    JsonObject detailReclamationJson;

    DetailFichePriseCharge detailFPC;

    List<DetailsReclamation> listDetailReclamation;
    List<Reclamation> listReclamation;
    List<RecoursJuridictionnel> listRecoursJuridictionnel;
    List<JsonObject> reclamationJsonListData;
    List<ArchiveAccuseReception> listArchiveAccuseReception;
    List<JsonObject> listJsonObjectArchAcc;
    Reclamation objReclamation;
    JsonObject jsonObjectArchAcc;

    HashMap<String, Object[]> bulkUpdateDoc;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case ContentieuxConst.Operation.LOAD_ASSUJETTI_DOSSIER:
                result = loadAssujettiDossier(request);
                break;
            case ContentieuxConst.Operation.SAVE_RECLAMATION:
                result = saveReclamation(request);
                break;
            case ContentieuxConst.Operation.LOAD_RECLAMATION:
                result = loadReclamation(request);
                break;
            case ContentieuxConst.Operation.VALIDER_RECLAMATION:
                result = validerReclamation(request);
                break;
            case ContentieuxConst.Operation.TRAITEMENT_DECISION:
                result = traitementDeicsions(request);
                break;
            case ContentieuxConst.Operation.PRINT_DOCUMENT:
                result = printerDocumentGenerique(request);
                break;
            case ContentieuxConst.Operation.LOAD_DOSSIER_RECOURS:
                result = loadDossierRecours(request);
                break;
            case ContentieuxConst.Operation.LOAD_ARCHIVE_LETTRE_RECLAMATION:
                result = loadArchiveLR(request);
                break;
            case ContentieuxConst.Operation.SAVE_RECOURS_JURIDICTIONNEL:
                result = saveRecoursJuridictionnel(request);
                break;
            case ContentieuxConst.Operation.LOAD_RECOURS_JUIRIDICTIONNEL:
                result = loadRecoursJuridictionnel(request);
                break;
        }
        out.print(result);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public String loadAssujettiDossier(HttpServletRequest request) {
        String dataReturn = GeneralConst.EMPTY_STRING;
        Reclamation reclamation = new Reclamation();
        detailFPC = new DetailFichePriseCharge();

        String typesearch = request.getParameter(ContentieuxConst.ParamName.TYPE_SEARCH_DOCUMENT);
        String codeAssujetti = request.getParameter(ContentieuxConst.ParamName.CODE_ASSUJETTI);
        String numeberDossier = request.getParameter(ContentieuxConst.ParamName.NUMERO_DOCUMENT);
        String idUser = request.getParameter(ContentieuxConst.ParamName.ID_USER);
        String codeSite = request.getParameter(ContentieuxConst.ParamName.CODE_SITE);
        String codeService = request.getParameter(ContentieuxConst.ParamName.CODE_SERVICE);

        try {

            String article;
            Date receptionDate;
            String codeBudgetaire;
            List<JsonObject> jsonAmrList = new ArrayList<>();

            switch (typesearch) {

                case DocumentConst.DocumentCode.AMR:
                    amr = ContentieuxBusiness.getAmrByNumero(numeberDossier, codeAssujetti);
                    if (amr != null) {

                        amrJson = new JsonObject();

                        if (amr.getDateReceptionAmr() != null) {

                            Date dateDuJour = new Date();
                            if (amr.getDateReceptionAmr().before(dateDuJour)) {
                                long nombreJour = Math.abs(ConvertDate.daysBetween(amr.getDateReceptionAmr(), dateDuJour));
                                amrJson.addProperty(ContentieuxConst.ParamName.NOMBRE_JOUR, nombreJour);
                                long nombreMois = nombreJour == GeneralConst.Numeric.ZERO
                                        ? GeneralConst.Numeric.ZERO : Math.abs(ConvertDate.getMonthsBetween(amr.getDateReceptionAmr(), dateDuJour));
                                amrJson.addProperty(ContentieuxConst.ParamName.NOMBRE_MOIS, nombreMois);

                                if (nombreMois >= 6) {
                                    amrJson.addProperty(ContentieuxConst.ParamName.ETAT_RECLAMATION, GeneralConst.Number.NINE);
                                } else {
                                    amrJson.addProperty(ContentieuxConst.ParamName.ETAT_RECLAMATION, GeneralConst.Number.TEN);
                                }

                            } else {
                                amrJson.addProperty(ContentieuxConst.ParamName.NOMBRE_JOUR, GeneralConst.Number.ZERO);
                                amrJson.addProperty(ContentieuxConst.ParamName.NOMBRE_MOIS, GeneralConst.Number.ZERO);
                            }

                            Date dateCreateAMR = Tools.formatStringFullToDate(amr.getDateCreat());

                            amrJson.addProperty(ContentieuxConst.ParamName.DATE_CREATE, Tools.formatDateToString(dateCreateAMR));

                            String dateEcheanceAMR = GeneralConst.EMPTY_STRING;

                            if (amr.getDateEcheance() != null) {
                                dateEcheanceAMR = Tools.formatDateToString(amr.getDateEcheance());
                                amrJson.addProperty(ContentieuxConst.ParamName.DATE_ECHEANCE, dateEcheanceAMR);
                            }
                            String dateReceptionAMR = GeneralConst.EMPTY_STRING;

                            if (amr.getDateReceptionAmr() != null) {
                                dateReceptionAMR = Tools.formatDateToString(amr.getDateReceptionAmr());
                                amrJson.addProperty(ContentieuxConst.ParamName.DATE_RECEPTION_COURRIER, dateReceptionAMR);
                            }

                            reclamation = ContentieuxBusiness.getReclamationByTitre(
                                    amr.getNumero().trim());

                            if (reclamation != null) {
                                amrJson.addProperty(ContentieuxConst.ParamName.TITRE_EXIST_TO_RECLAMATION,
                                        GeneralConst.Number.ONE);
                            } else {
                                amrJson.addProperty(ContentieuxConst.ParamName.TITRE_EXIST_TO_RECLAMATION,
                                        GeneralConst.Number.ZERO);
                            }

                            amrJson.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT,
                                    amr.getNumero().trim());
                            amrJson.addProperty(ContentieuxConst.ParamName.TYPE_DOCUMENT,
                                    DocumentConst.DocumentCode.AMR);
                            amrJson.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, amr.getNetAPayer());

                            if (amr.getFichePriseCharge() != null) {
                                amrJson.addProperty(NotePerceptionConst.ParamName.DEVISE,
                                        !amr.getFichePriseCharge().getDevise().equals(GeneralConst.EMPTY_STRING) ? amr.getFichePriseCharge().getDevise()
                                                : GeneralConst.Devise.DEVISE_CDF);

                                amrJson.addProperty(ContentieuxConst.ParamName.FICHE_PRISE_CHARGE,
                                        amr.getFichePriseCharge().getCode().trim());

                                detailFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amr.getFichePriseCharge().getCode().trim());
                                if (detailFPC != null) {
                                    amrJson.addProperty(ContentieuxConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                            detailFPC.getArticleBudgetaire() == null ? GeneralConst.EMPTY_STRING
                                                    : detailFPC.getArticleBudgetaire().getIntitule()
                                                    == null ? GeneralConst.EMPTY_STRING : detailFPC.getArticleBudgetaire().getIntitule());

                                    amrJson.addProperty(ContentieuxConst.ParamName.CODE_OFFICIEL,
                                            detailFPC.getArticleBudgetaire() == null ? GeneralConst.EMPTY_STRING
                                                    : detailFPC.getArticleBudgetaire().getCodeOfficiel()
                                                    == null ? GeneralConst.EMPTY_STRING : detailFPC.getArticleBudgetaire().getCodeOfficiel());
                                }

                            } else {
                                amrJson.addProperty(NotePerceptionConst.ParamName.DEVISE, GeneralConst.Devise.DEVISE_CDF);
                                amrJson.addProperty(ContentieuxConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, GeneralConst.EMPTY_STRING);
                            }

                            jsonAmrList.add(amrJson);

                        } else {
                            dataReturn = GeneralConst.ResultCode.DATE_RECEPTION_NON_EXIST;
                        }

                        dataReturn = jsonAmrList.toString();

                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;

            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveReclamation(HttpServletRequest request) {

        String userId = request.getParameter(ContentieuxConst.ParamName.USER_ID);
        String observation = request.getParameter(ContentieuxConst.ParamName.OBSERVATION);
        String typeReclamation = request.getParameter(ContentieuxConst.ParamName.TYPE_RECLAMATION);
        String codeAssujetti = request.getParameter(ContentieuxConst.ParamName.CODE_ASSUJETTI);
        String listDocument = request.getParameter(ContentieuxConst.ParamName.LIST_DOCUMENT);
        String referenceCourier = request.getParameter(ContentieuxConst.ParamName.REFERENCE_COURRIER_RECLAMATION);
        String archives = request.getParameter(ContentieuxConst.ParamName.ARCHIVES);
        String fkDocument = request.getParameter(ContentieuxConst.ParamName.FK_DOCUMENT);
        String observationDoc = request.getParameter(ContentieuxConst.ParamName.OBSERVATION_DOCUMENT);
        String etatReclamation = request.getParameter(ContentieuxConst.ParamName.ETAT_RECLAMATION);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            JSONArray jsonArray = new JSONArray(listDocument);
            List<DetailsReclamationPrint> listDetailReclamation = new ArrayList<>();
            List<String> ListArchiveDocument = new ArrayList<>();
            JSONArray jsonDocumentArray = null;
            String natureAB = GeneralConst.EMPTY_STRING;

            for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                DetailsReclamationPrint detailReclamation = new DetailsReclamationPrint();
                BigDecimal amountPourcent = new BigDecimal(GeneralConst.Number.ZERO);
                BigDecimal amountDu = new BigDecimal(GeneralConst.Number.ZERO);
                BigDecimal amountDifference = new BigDecimal(GeneralConst.Number.ZERO);
                BigDecimal twentyPourcent = new BigDecimal(GeneralConst.POURCENTAGE_PARTIE_NON_CONTESTER);
                BigDecimal amountConteste = new BigDecimal(GeneralConst.Number.ZERO);

                detailReclamation.setMotif(jsonObject.getString(ContentieuxConst.ParamName.MOTIF));
                detailReclamation.setReferenceDocument(jsonObject.getString(ContentieuxConst.ParamName.REFERENCE_DOCUMENT));
                detailReclamation.setTypeDocument(jsonObject.getString(ContentieuxConst.ParamName.TYPE_DOCUMENT));
                detailReclamation.setDocumentId(jsonObject.getString(ContentieuxConst.ParamName.REFERENCE_DOCUMENT));
                amountDu = BigDecimal.valueOf(Double.valueOf(jsonObject.getString(ContentieuxConst.ParamName.AMOUNT)));
                amountConteste = BigDecimal.valueOf(Double.valueOf(jsonObject.getString(ContentieuxConst.ParamName.MONTANT_CONTESTER)));
                amountDifference = amountDu.subtract(amountConteste);
                amountPourcent = amountDifference.multiply(twentyPourcent);

                SuiviComptableDeclaration sc;
                Amr amr;
                detailFPC = new DetailFichePriseCharge();

                switch (detailReclamation.getTypeDocument()) {
                    case DocumentConst.DocumentCode.AMR:

                        amr = ContentieuxBusiness.loaderAmrByNumero(fkDocument);
                        if (amr != null) {
                            detailFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amr.getFichePriseCharge().getCode().trim());
                            if (detailFPC != null) {
                                if (detailFPC.getNc() != null
                                        && !detailFPC.getNc().equals(GeneralConst.EMPTY_STRING)) {
                                    sc = ContentieuxBusiness.getSuiviComptableDeclarationByNc(detailFPC.getNc().trim());
                                    if (sc != null) {
                                        if (sc.getDevise() != null) {
                                            detailReclamation.setDevise(sc.getDevise());
                                        } else {
                                            detailReclamation.setDevise(GeneralConst.Devise.DEVISE_CDF);
                                        }

                                    } else {
                                        detailReclamation.setDevise(GeneralConst.Devise.DEVISE_CDF);
                                    }
                                    detailReclamation.setNcDocumentReference(detailFPC.getNc());
                                } else {
                                    detailReclamation.setDevise(detailFPC.getFichePriseCharge().getDevise().trim());
                                    detailReclamation.setNcDocumentReference(GeneralConst.EMPTY_STRING);
                                }
                            }

                        }
                        detailReclamation.setReferenceDocument(fkDocument);

                        break;

                    case DocumentConst.DocumentCode.NP:
                        break;
                }

                detailReclamation.setPourcentageMontantNonContster(amountPourcent);
                detailReclamation.setMontantContester(amountConteste);

                listDetailReclamation.add(detailReclamation);
            }

            if (archives != null && !archives.isEmpty()) {
                jsonDocumentArray = new JSONArray(archives);

                for (int i = 0; i < jsonDocumentArray.length(); i++) {
                    JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                    ListArchiveDocument.add(jsonObject.getString(ContentieuxConst.ParamName.PV_DOCUMENT));
                }
            }

            Amr amrs = ContentieuxBusiness.loaderAmrByNumero(fkDocument);
            if (amrs != null) {
                DetailFichePriseCharge detailsFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amrs.getFichePriseCharge().getCode().trim());
                if (detailsFPC != null) {
                    if (detailsFPC.getArticleBudgetaire() != null) {
                        natureAB = detailsFPC.getArticleBudgetaire().getNature().getCode().trim();
                    }
                }
            }

            Reclamation reclamation = new Reclamation();

            reclamation.setObservation(observation);
            reclamation.setAgentCreat(new Agent(Integer.valueOf(userId)));
            reclamation.setTypeReclamation(typeReclamation);
            reclamation.setFkPersonne(new Personne(String.valueOf(codeAssujetti)));
            reclamation.setReferenceCourierReclamation(referenceCourier);
            reclamation.setEtatReclamation(GeneralConst.Numeric.TWO);

            boolean result = ContentieuxBusiness.saveReclamation(
                    reclamation, listDetailReclamation, ListArchiveDocument,
                    fkDocument, observationDoc, natureAB);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadReclamation(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;
        String decisionJuridictonnel = GeneralConst.EMPTY_STRING;

        String valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
        String typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
        String isAdvancedSearch = request.getParameter(ContentieuxConst.ParamName.IS_ADVANCED_SEARCH);
        if (isAdvancedSearch.equals(GeneralConst.Number.ONE)) {
            dateDebut = Tools.getValidFormat(request.getParameter(GeneralConst.ParamName.DATE_DEBUT));
            dateFin = Tools.getValidFormat(request.getParameter(GeneralConst.ParamName.DATE_FIN));
        }

        String typeReclamation = request.getParameter(ContentieuxConst.ParamName.TYPE_RECLAMATION);
//        String codeSite = request.getParameter(AcquitLiberatoireConst.ParamName.CODE_SITE);
//        String codeService = request.getParameter(AcquitLiberatoireConst.ParamName.CODE_SERVICE);

        try {

            listReclamation = new ArrayList<>();

            listReclamation = ContentieuxBusiness.loadListReclamations(
                    isAdvancedSearch.trim(),
                    typeSearch.trim(),
                    valueSearch.trim(),
                    dateDebut.trim(),
                    dateFin.trim(),
                    typeReclamation);

            if (!listReclamation.isEmpty()) {

                List<JsonObject> reclamationJsonList = new ArrayList<>();
                List<JsonObject> listJsonObjectArchive;

                for (Reclamation reclamation : listReclamation) {

                    reclamationJson = new JsonObject();

                    reclamationJson.addProperty(ContentieuxConst.ParamName.RECLAMATION_ID,
                            reclamation.getId());
                    reclamationJson.addProperty(TaxationConst.ParamName.DATE_CREATE,
                            Tools.formatDateToString(reclamation.getDateCreat()));
                    if (reclamation.getObservation() != null) {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.OBSERVATION,
                                reclamation.getObservation());
                    } else {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.OBSERVATION,
                                GeneralConst.EMPTY_STRING);
                    }
                    reclamationJson.addProperty(ContentieuxConst.ParamName.OBSERVATION_LIST,
                            Tools.getListObservations(reclamation.getId().trim(),
                                    GeneralConst.Module.MOD_RECLAMATION));
                    reclamationJson.addProperty(ContentieuxConst.ParamName.ETAT, reclamation.getEtat());
                    reclamationJson.addProperty(ContentieuxConst.ParamName.ETAT_RECLAMATION,
                            reclamation.getEtatReclamation());
                    reclamationJson.addProperty(ContentieuxConst.ParamName.RECLAMATION_TRAITER,
                            reclamation.getEstTraiter());
                    reclamationJson.addProperty(ContentieuxConst.ParamName.TYPE_RECLAMATION,
                            reclamation.getTypeReclamation());

                    Personne personne = IdentificationBusiness.getPersonneByCode(
                            reclamation.getFkPersonne().getCode().trim());

                    if (personne != null) {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.AssujettiCode,
                                personne.getCode().trim());
                        reclamationJson.addProperty(ContentieuxConst.ParamName.AssujettiName,
                                personne.toString().toUpperCase().trim());

                        reclamationJson.addProperty(ContentieuxConst.ParamName.LegalFormName,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        Adresse adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                personne.getCode().trim());
                        if (adresse != null) {
                            reclamationJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                                    adresse.toString().toUpperCase().trim());
                        } else {
                            reclamationJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                                    GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.AssujettiCode,
                                GeneralConst.EMPTY_STRING);

                        reclamationJson.addProperty(ContentieuxConst.ParamName.AssujettiName,
                                GeneralConst.EMPTY_STRING);

                        reclamationJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                                GeneralConst.EMPTY_STRING);
                        reclamationJson.addProperty(ContentieuxConst.ParamName.LegalFormName,
                                GeneralConst.EMPTY_STRING);

                    }
                    reclamationJson.addProperty(ContentieuxConst.ParamName.REFERENCE_COURRIER_RECLAMATION,
                            reclamation.getReferenceCourierReclamation().toUpperCase().trim());
                    reclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_ID,
                            reclamation.getAgentCreat().getCode());
                    reclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_NAME,
                            reclamation.getAgentCreat().toString().toUpperCase().trim());

                    if (reclamation.getAgentCreat().getFonction() != null) {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_QUALITY,
                                reclamation.getAgentCreat().getFonction().getIntitule().toUpperCase().trim());
                    } else {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_QUALITY,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (reclamation.getDateReceptionCourier() != null) {
                        Date dateReception = Tools.addMonthTodate(reclamation.getDateReceptionCourier(),
                                GeneralConst.Numeric.THREE);

                        if (dateReception != null) {
                            if (Compare.after(new Date(), dateReception)) {
                                reclamationJson.addProperty(ContentieuxConst.ParamName.DELAIS_RECOURS_DEPASSE,
                                        GeneralConst.Number.ONE);
                            } else {
                                reclamationJson.addProperty(ContentieuxConst.ParamName.DELAIS_RECOURS_DEPASSE,
                                        GeneralConst.Number.ZERO);
                            }
                            String dateReceptionCour = GeneralConst.EMPTY_STRING;
                            dateReceptionCour = Tools.formatDateToString(reclamation.getDateReceptionCourier());
                            reclamationJson.addProperty(ContentieuxConst.ParamName.DATE_RECEPTION_COURRIER,
                                    dateReceptionCour);
                        } else {
                            reclamationJson.addProperty(ContentieuxConst.ParamName.DATE_RECEPTION_COURRIER,
                                    GeneralConst.EMPTY_STRING);
                            reclamationJson.addProperty(ContentieuxConst.ParamName.DELAIS_RECOURS_DEPASSE,
                                    GeneralConst.Number.ZERO);
                        }

                    } else {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.DATE_RECEPTION_COURRIER,
                                GeneralConst.EMPTY_STRING);

                        reclamationJson.addProperty(ContentieuxConst.ParamName.DELAIS_RECOURS_DEPASSE,
                                GeneralConst.Number.ZERO);
                    }

                    if (reclamation.getEtat() == 4) {

                        if (!reclamation.getRecoursJuridictionnelList().isEmpty()) {

                            RecoursJuridictionnel recoursJuridictionnel = reclamation.getRecoursJuridictionnelList().get(0);

                            reclamationJson.addProperty(ContentieuxConst.ParamName.NUMERO_ENREGISTREMENT_GREFFE,
                                    recoursJuridictionnel.getNumeroEnregistrementGreffe().toUpperCase().trim());

                            reclamationJson.addProperty(ContentieuxConst.ParamName.DATE_AUDIENCE,
                                    recoursJuridictionnel.getDateAudience());

                            reclamationJson.addProperty(ContentieuxConst.ParamName.DATE_DEPOT_RECOURS,
                                    Tools.formatDateToStringV2(recoursJuridictionnel.getDateDepotRecours()));

                            reclamationJson.addProperty(ContentieuxConst.ParamName.MOTIF_RECOURS,
                                    recoursJuridictionnel.getMotifRecours());

                            reclamationJson.addProperty(ContentieuxConst.ParamName.RECOURS_EXIST,
                                    GeneralConst.Number.ONE);

                        } else {
                            reclamationJson.addProperty(ContentieuxConst.ParamName.RECOURS_EXIST,
                                    GeneralConst.Number.ZERO);
                        }
                    } else {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.RECOURS_EXIST,
                                GeneralConst.Number.ZERO);
                    }

                    List<DetailsReclamation> listDetailsReclamations = ContentieuxBusiness.getDetailReclamationByFkReclamation(
                            reclamation.getId().trim());

                    if (!listDetailsReclamations.isEmpty()) {

                        List<JsonObject> detailReclamationJsonList = new ArrayList<>();

                        reclamationJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION_IS_EXIST,
                                GeneralConst.Number.ONE);

                        for (DetailsReclamation detailsReclamation : listDetailsReclamations) {

                            detailReclamationJson = new JsonObject();

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.RECLAMATION_ID,
                                    detailsReclamation.getFkReclamation().getId().trim());
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DETAIL_RECLAMATION_ID,
                                    detailsReclamation.getId());
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT,
                                    detailsReclamation.getReferenceDocument());
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_ADMINISTRATIVE,
                                    GeneralConst.EMPTY_STRING);

                            if (detailsReclamation.getTypeDocument() != null
                                    && !detailsReclamation.getTypeDocument().isEmpty()) {

                                // String exerciceFiscal;
                                String article;
                                SuiviComptableDeclaration sc;
                                String codeBudgetaire;
                                detailFPC = new DetailFichePriseCharge();
                                Amr amr;

                                switch (detailsReclamation.getTypeDocument()) {
                                    case DocumentConst.DocumentCode.AMR:
                                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.TYPE_DOCUMENT,
                                                DocumentConst.DocumentName.AVIS_MISE_RECOUVREMENT);

                                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.TYPE_DOCUMENT_2,
                                                DocumentConst.DocumentCode.AMR);
                                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                                detailsReclamation.getMontantContester());
                                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                                detailsReclamation.getPourcentageMontantNonContestre());

                                        amr = ContentieuxBusiness.loaderAmrByNumero_V2(detailsReclamation.getReferenceDocument().trim());
                                        if (amr != null) {
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.AMOUNT,
                                                    amr.getNetAPayer());

                                            detailFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amr.getFichePriseCharge().getCode().trim());

                                            if (detailFPC != null) {

                                                NoteCalcul nc = TaxationBusiness.getNoteCalculByNumero(
                                                        detailFPC.getNc().trim());

                                                if (nc != null) {

                                                    //exerciceFiscal = Tools.getExerciceFiscal(nc);
                                                    if (nc.getExercice() != null) {

                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                                nc.getExercice());
                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                                GeneralConst.EMPTY_STRING);
                                                    }

                                                    if (!nc.getService().isEmpty()) {
                                                        Service service = ContentieuxBusiness.getServiceByCode(nc.getService().trim());
                                                        if (service != null) {

                                                            if (service.getIntitule() != null) {
                                                                detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                        service.getIntitule().toUpperCase().trim());
                                                            } else {
                                                                detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                        GeneralConst.EMPTY_STRING);
                                                            }

                                                        } else {
                                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                    GeneralConst.EMPTY_STRING);
                                                        }
                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                GeneralConst.EMPTY_STRING);
                                                    }

                                                    article = Tools.getArticleByNC(nc);

                                                    if (article != null) {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                                article.toUpperCase());
                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                                GeneralConst.EMPTY_STRING);
                                                    }

                                                    codeBudgetaire = Tools.getCodeBudgetaireByNC(nc);
                                                    if (codeBudgetaire != null && !codeBudgetaire.isEmpty()) {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                                codeBudgetaire.toUpperCase());
                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                                GeneralConst.EMPTY_STRING);
                                                    }

                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                            GeneralConst.EMPTY_STRING);
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                            GeneralConst.EMPTY_STRING);
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                            GeneralConst.EMPTY_STRING);
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                            GeneralConst.EMPTY_STRING);
                                                }

                                                sc = ContentieuxBusiness.getSuiviComptableDeclarationByNc(detailFPC.getNc().trim());
                                                if (sc != null) {
                                                    if (sc.getDevise() != null) {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE, sc.getDevise());
                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                                GeneralConst.Devise.DEVISE_CDF);
                                                    }

                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                            GeneralConst.Devise.DEVISE_CDF);
                                                }

                                            } else {
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                        GeneralConst.Devise.DEVISE_CDF);
                                            }
                                        } else {
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                    GeneralConst.Devise.DEVISE_CDF);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.AMOUNT,
                                                    GeneralConst.Number.ZERO);
                                        }

                                        break;
                                }

                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.TYPE_DOCUMENT,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.MOTIF,
                                    detailsReclamation.getMotif());

                            if (detailsReclamation.getObservationTraitement() != null
                                    && !detailsReclamation.getObservationTraitement().isEmpty()) {

                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.OBSERVATION_TRAITEMENT,
                                        detailsReclamation.getObservationTraitement());
                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.OBSERVATION_TRAITEMENT,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.AVEC_SURCIS,
                                    detailsReclamation.getAvecSurcis());

                            if (detailsReclamation.getDateDecisionSurcis() != null) {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_DECISION_SURCIS,
                                        Tools.formatDateToString(detailsReclamation.getDateDecisionSurcis()));

                                if (detailsReclamation.getDecisionSurcis() == 1) {
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_SURCIS,
                                            GeneralConst.Number.ONE);
                                } else if (detailsReclamation.getDecisionSurcis() == 0) {
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_SURCIS,
                                            GeneralConst.Number.ZERO);
                                }

                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_DECISION_SURCIS,
                                        GeneralConst.EMPTY_STRING);
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_SURCIS,
                                        GeneralConst.EMPTY_STRING);
                            }

                            if (detailsReclamation.getDateTraitement() != null) {

                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT,
                                        Tools.formatDateToString(detailsReclamation.getDateTraitement()));

                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.EST_TRAITE,
                                        GeneralConst.Number.ONE);

                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT,
                                        GeneralConst.EMPTY_STRING);
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.EST_TRAITE,
                                        GeneralConst.Number.ZERO);
                            }
                            
                            if (detailsReclamation.getFkDecision() != null
                                    && !detailsReclamation.getFkDecision().equals(GeneralConst.EMPTY_STRING)) {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_JURIDICTIONNEL,
                                        detailsReclamation.getFkDecision().getId());
                                reclamationJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_JURIDICTIONNEL,
                                            detailsReclamation.getFkDecision().getId());
                            }

                            if (detailsReclamation.getDateTraitementJuridique() != null) {

                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.EST_TRAITE_JURIDICTIONNEL,
                                        GeneralConst.Number.ONE);

                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT_JURIDIQUE,
                                        Tools.formatDateToString(detailsReclamation.getDateTraitementJuridique()));

                                Decision decision = ContentieuxBusiness.getDecisionById(
                                        detailsReclamation.getFkDecisionJuridique());

                                if (decision != null) {
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_JURIDIQUE,
                                            decision.getLibelleDecision().toUpperCase().trim());
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_JURIDICTIONNEL,
                                            decision.getId());
                                    reclamationJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_JURIDICTIONNEL,
                                            decision.getId());

                                } else {
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_JURIDIQUE,
                                            GeneralConst.EMPTY_STRING);
                                    reclamationJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_JURIDICTIONNEL,
                                            GeneralConst.EMPTY_STRING);
                                }

                            } else {

                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.EST_TRAITE_JURIDICTIONNEL,
                                        GeneralConst.Number.ZERO);
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT_JURIDIQUE,
                                        GeneralConst.EMPTY_STRING);
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_JURIDIQUE,
                                        GeneralConst.EMPTY_STRING);
                            }

                            if (detailsReclamation.getFkDecision() != null
                                    && !detailsReclamation.getFkDecision().equals(GeneralConst.EMPTY_STRING)) {

                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_ADMINISTRATIVE,
                                        detailsReclamation.getFkDecision().getLibelleDecision().toUpperCase());
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_ADMINISTRATIVE,
                                        detailsReclamation.getFkDecision().getId());
                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_ADMINISTRATIVE,
                                        GeneralConst.EMPTY_STRING);
                            }

                            if (detailsReclamation.getMontantContester() != null) {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                        detailsReclamation.getMontantContester());
                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                        GeneralConst.EMPTY_ZERO);
                            }

                            if (detailsReclamation.getPourcentageMontantNonContestre() != null) {

                                if (detailsReclamation.getPourcentageMontantNonContestre().compareTo(BigDecimal.ZERO)
                                        > GeneralConst.Numeric.ZERO) {

                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                            detailsReclamation.getPourcentageMontantNonContestre());

                                    BonAPayer bonAPayer = PaiementBusiness.getBonAPayerByAMR(detailsReclamation.getReferenceDocument().trim());
                                    if (bonAPayer != null) {
                                        if (bonAPayer.getEtatImpression() && bonAPayer.getEnContentieux()) {
                                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DOCUMENT_IMPRIMER,
                                                    GeneralConst.Number.ONE);
                                        } else {
                                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DOCUMENT_IMPRIMER,
                                                    GeneralConst.Number.ZERO);
                                        }

                                        Journal journal = ContentieuxBusiness.getJournalByDocumentApure(bonAPayer.getCode().trim());
                                        if (journal != null) {
                                            if (journal.getEtat() == GeneralConst.Numeric.ONE) {

                                                //Payer avec apurement
                                                reclamationJson.addProperty(ContentieuxConst.ParamName.DOCUMENT_PAYER,
                                                        GeneralConst.Number.ONE);
                                            } else {

                                                // Payer sans apurement
                                                reclamationJson.addProperty(ContentieuxConst.ParamName.DOCUMENT_PAYER,
                                                        GeneralConst.Number.TWO);
                                            }
                                        } else {
                                            reclamationJson.addProperty(ContentieuxConst.ParamName.DOCUMENT_PAYER,
                                                    GeneralConst.Number.ZERO);

                                        }
                                    }
                                } else {
                                    reclamationJson.addProperty(ContentieuxConst.ParamName.DOCUMENT_PAYER,
                                            GeneralConst.Number.ONE);
                                }

                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                        GeneralConst.EMPTY_ZERO);
                            }

                            listJsonObjectArchive = new ArrayList<>();

                            List<ArchiveAccuseReception> listArchives = TaxationBusiness.getListArchiveAccuseByIdDocumenet(detailsReclamation.getFkReclamation().getId().trim());

                            for (ArchiveAccuseReception arch : listArchives) {

                                JsonObject jsonObjectArchive = new JsonObject();

                                jsonObjectArchive.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT,
                                        arch.getArchive() == null ? GeneralConst.EMPTY_STRING : arch.getArchive());
                                jsonObjectArchive.addProperty(TaxationConst.ParamName.NUMERO_NC,
                                        arch.getDocumentReference() == null ? GeneralConst.EMPTY_STRING : arch.getDocumentReference());

                                if (arch.getObservation() != null && !arch.getObservation().isEmpty()) {
                                    jsonObjectArchive.addProperty(ControleConst.ParamName.OBSERVATION_DOC, arch.getObservation());
                                } else {
                                    jsonObjectArchive.addProperty(ControleConst.ParamName.OBSERVATION_DOC, GeneralConst.EMPTY_STRING);
                                }

                                if (arch.getFkDocument() != null && !arch.getFkDocument().isEmpty()) {
                                    TypeDocument typeDocument = GeneralBusiness.getTypeDocumentByCode_V2(arch.getTypeDocument().trim());
                                    if (typeDocument != null) {
                                        jsonObjectArchive.addProperty(ContentieuxConst.ParamName.LIBELLE_DOCUMENT, typeDocument.getIntitule());
                                    } else {
                                        jsonObjectArchive.addProperty(ContentieuxConst.ParamName.LIBELLE_DOCUMENT, GeneralConst.EMPTY_STRING);
                                    }
                                } else {
                                    jsonObjectArchive.addProperty(ContentieuxConst.ParamName.LIBELLE_DOCUMENT, GeneralConst.EMPTY_STRING);
                                }

                                listJsonObjectArchive.add(jsonObjectArchive);
                            }

                            detailReclamationJson.addProperty(TaxationConst.ParamName.DECLARATION_DOCUMENT_LIST, listJsonObjectArchive.toString());

                            detailReclamationJsonList.add(detailReclamationJson);
                        }
                        reclamationJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION,
                                detailReclamationJsonList.toString());
                    } else {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION_IS_EXIST,
                                GeneralConst.Number.ZERO);
                        reclamationJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION,
                                GeneralConst.EMPTY_STRING);
                        reclamationJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_JURIDICTIONNEL,
                                GeneralConst.EMPTY_STRING);
                    }

                    reclamationJsonList.add(reclamationJson);
                }
                dataReturn = reclamationJsonList.toString();
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String validerReclamation(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result = false;
        String libelleAB = GeneralConst.EMPTY_STRING;

        try {

            String valueState = request.getParameter(ContentieuxConst.ParamName.VALUE_STATE);
            String reclamationId = request.getParameter(ContentieuxConst.ParamName.RECLAMATION_ID);
            String documentList = request.getParameter(ContentieuxConst.ParamName.DOCUMENT_LIST);
            String codeAssujetti = request.getParameter(ContentieuxConst.ParamName.CODE_ASSUJETTI);
            String numAMR = request.getParameter(ContentieuxConst.ParamName.FK_DOCUMENT);
            String userId = request.getParameter(ContentieuxConst.ParamName.USER_ID);

            JSONArray jsonArray = new JSONArray(documentList);
            List<DetailsReclamationPrint> listDetailReclamation = new ArrayList<>();

            for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                DetailsReclamationPrint detailReclamation = new DetailsReclamationPrint();

                detailReclamation.setMotif(jsonObject.getString(ContentieuxConst.ParamName.MOTIF));
                detailReclamation.setReferenceDocument(jsonObject.getString(ContentieuxConst.ParamName.REFERENCE_DOCUMENT));
                detailReclamation.setTypeDocument(jsonObject.getString(ContentieuxConst.ParamName.TYPE_DOCUMENT));
                detailReclamation.setDocumentId(jsonObject.getString(ContentieuxConst.ParamName.REFERENCE_DOCUMENT));
                detailReclamation.setMontantContester(BigDecimal.valueOf(Double.valueOf(jsonObject.getString(ContentieuxConst.ParamName.MONTANT_CONTESTER))));
                detailReclamation.setPourcentageMontantNonContster(BigDecimal.valueOf(Double.valueOf(jsonObject.getString(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER))));
                listDetailReclamation.add(detailReclamation);
            }

            Amr amrs = ContentieuxBusiness.loaderAmrByNumero(numAMR);
            if (amrs != null) {
                DetailFichePriseCharge detailsFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amrs.getFichePriseCharge().getCode().trim());
                if (detailsFPC != null) {
                    if (detailsFPC.getArticleBudgetaire() != null) {
                        libelleAB = detailsFPC.getArticleBudgetaire().getIntitule().trim();
                    }
                }
            }

            result = ContentieuxBusiness.validerReclamation(valueState, reclamationId,
                    codeAssujetti, numAMR, userId, libelleAB, listDetailReclamation);

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String traitementDeicsions(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        boolean result;
        String valueDecision;

        try {

            String decisionId = request.getParameter(ContentieuxConst.ParamName.DECISION_ID);
            String userId = request.getParameter(ContentieuxConst.ParamName.USER_ID);
            String amountDegre = request.getParameter(ContentieuxConst.ParamName.AMOUNT_DEGREVE);
            String typeDocument = request.getParameter(ContentieuxConst.ParamName.TYPE_DOCUMENT);
            String detailReclamationId = request.getParameter(ContentieuxConst.ParamName.DETAIL_RECLAMATION_ID);
            String typeRemiseGracieuse = request.getParameter(ContentieuxConst.ParamName.TYPE_REMISE_GRACIEUSE);
            String documentArchive = request.getParameter(ContentieuxConst.ParamName.DOCUMENT_ARCHIVE);
            String typeTraitement = request.getParameter(ContentieuxConst.ParamName.TYPE_TRAITEMENT);
            String twentyPercentNonCont = request.getParameter(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER);
            String fkDocument = request.getParameter(ContentieuxConst.ParamName.FK_DOCUMENT);
            String libelleAB = request.getParameter(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE);
            String codeAssujetti = request.getParameter(ContentieuxConst.ParamName.CODE_ASSUJETTI);
            String amountDu = request.getParameter(ContentieuxConst.ParamName.AMOUNT);
            String archives = request.getParameter(ContentieuxConst.ParamName.ARCHIVES);
            String reclamationId = request.getParameter(ContentieuxConst.ParamName.RECLAMATION_ID);
            String observationArch = request.getParameter(ContentieuxConst.ParamName.OBSERVATION_DOCUMENT);

            BigDecimal amount;
            BigDecimal amountPayer;
            BigDecimal amountDifference = new BigDecimal(GeneralConst.Number.ZERO);
            BigDecimal amountRestPayer = new BigDecimal(GeneralConst.Number.ZERO);
            BigDecimal twentyPourcent = new BigDecimal(GeneralConst.POURCENTAGE_PARTIE_NON_CONTESTER);
            List<String> ListArchiveDocument = new ArrayList<>();
            JSONArray jsonDocumentArray = null;
             String codeNc = null;
            propertiesConfig = new PropertiesConfig();

            String txtTraitementLibelle;
            String txtDecision;

            amount = BigDecimal.valueOf(Double.valueOf(amountDegre));
            amountPayer = BigDecimal.valueOf(Double.valueOf(amountDu));
            twentyPourcent = BigDecimal.valueOf(Double.valueOf(twentyPercentNonCont));
            amountDifference = amount.subtract(twentyPourcent);
            amountRestPayer = amountPayer.subtract(twentyPourcent);
            
            Amr amr = ContentieuxBusiness.loaderAmrByNumero_V2(fkDocument.trim());
            if (amr != null){
                DetailFichePriseCharge detailFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amr.getFichePriseCharge().getCode().trim());
                
                if (detailFPC != null){
                    NoteCalcul nc = TaxationBusiness.getNoteCalculByNumero(detailFPC.getNc().trim());
                    codeNc = nc.getNumero().trim();
                }
            }

            switch (typeTraitement) {
                case GeneralConst.Number.ONE: // Décision Administrative
                    txtTraitementLibelle = propertiesMessage.getProperty("TXT_TRAITEMENT_RECLAMATION_ADMIN");
                    txtDecision = DocumentConst.DocumentCode.DECISION_ADMINISTRATIVE;

                    switch (decisionId) {
                        case GeneralConst.Number.ONE:// Décision Administrative === Dégrevement partiel

                            valueDecision = ContentieuxConst.DecisionTraitement.DEGREVEMENT_PARTIEL;
                            int etat = GeneralConst.Numeric.TWO;

                            counter++;//UPDATE T_DETAIL_RECLAMATION
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_SIMPLE_TRAITEMENT_RECLAMATION, new Object[]{
                                Integer.valueOf(userId.trim()),
                                Integer.valueOf(valueDecision),
                                Integer.valueOf(detailReclamationId)
                            });

                            counter++; //UPDATE T_RECLAMATION
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_TRAITEMENT_RECLAMATION, new Object[]{
                                reclamationId,
                                GeneralConst.Number.THREE
                            });

                            counter++; //UPDATE T_AMR
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_UPDATE_AMR, new Object[]{
                                GeneralConst.Numeric.ZERO,
                                fkDocument
                            });

                            counter++; //GENERE BP
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.EXEC_F_NEW_BON_A_PAYER_CONTENTIEUX, new Object[]{
                                amountDifference,
                                null,
                                userId,
                                fkDocument,
                                propertiesMessage.getProperty("TXT_GENERE_BON_A_PAYER_CONTENTIEUX_ADMIN"),
                                codeAssujetti,
                                propertiesConfig.getContent(PropertiesConst.Config.TXT_ACCOUNT_BANK_BAP_CODE),
                                libelleAB.trim().trim(),
                                GeneralConst.Numeric.ZERO,
                                etat,
                                codeNc
                            });

                            if (archives != null && !archives.isEmpty()) {
                                jsonDocumentArray = new JSONArray(archives);

                                for (int i = 0; i < jsonDocumentArray.length(); i++) {
                                    JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                                    ListArchiveDocument.add(jsonObject.getString(ContentieuxConst.ParamName.PV_DOCUMENT));
                                }
                            }

                            if (ListArchiveDocument.size() > 0) {
                                for (String docu : ListArchiveDocument) {
                                    counter++;
                                    bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.ARCHIVE_ACCUSER_RECEPTION2,
                                            new Object[]{
                                                reclamationId, DocumentConst.DocumentCode.LETTRE_RECLAMATION, docu, fkDocument, observationArch.trim()
                                            });
                                }
                            }

                            result = executeQueryBulkInsert(bulkQuery);

                            if (result) {
                                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                            } else {
                                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            break;

                        case GeneralConst.Number.TWO: // Décision Administrative ==== Degrevement total

                            valueDecision = ContentieuxConst.DecisionTraitement.DEGREVEMENT_TOTAL;

                            DetailFichePriseCharge detailFP;
                            String numNC;
                            String devise = null;

                            // Suivi comptable
                            Amr amrs = ContentieuxBusiness.loaderAmrByNumero(fkDocument);
                            if (amrs != null) {
                                detailFP = ContentieuxBusiness.getArticleBudgetaireByFPC(amrs.getFichePriseCharge().getCode().trim());
                                if (detailFP != null) {
                                    numNC = detailFP.getNc().trim();
                                } else {
                                    numNC = GeneralConst.EMPTY_STRING;
                                }
                                DetailsNc detailNc = ContentieuxBusiness.getDetailNcByNc(numNC.trim());
                                if (detailNc != null) {
                                    if (detailNc.getDevise() != null) {
                                        devise = detailNc.getDevise().trim();
                                    } else {
                                        devise = GeneralConst.Devise.DEVISE_CDF;
                                    }
                                } else {
                                    devise = GeneralConst.Devise.DEVISE_CDF;
                                }
                            } else {
                                numNC = GeneralConst.EMPTY_STRING;
                            }

                            counter++; //INSERTION DANS SUIVI COMPTABLE
                            bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                                numNC,
                                fkDocument,
                                txtTraitementLibelle.concat(GeneralConst.SPACE)
                                .concat(GeneralConst.BRAKET_OPEN)
                                .concat(propertiesMessage.getProperty("TXT_DEGREVEMENT_TOTAL_ADMIN"))
                                .concat(GeneralConst.BRAKET_CLOSE),
                                GeneralConst.Numeric.ZERO,
                                GeneralConst.Numeric.ZERO,
                                propertiesMessage.getProperty("TXT_DEGREVEMENT_TOTAL_ADMIN").concat(
                                GeneralConst.SPACE).concat(fkDocument),
                                devise
                            });

                            //Crédite implôt
                            counter++;
                            bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_CREDIT_IMPOT, new Object[]{
                                codeAssujetti,
                                fkDocument,
                                propertiesMessage.getProperty("TXT_CREDIT_IMPOT_CONTENTIEUX"),
                                twentyPourcent,
                                GeneralConst.Numeric.ZERO,
                                GeneralConst.Numeric.ZERO,
                                userId,
                                GeneralConst.Numeric.ZERO,
                                GeneralConst.Numeric.ONE,
                                devise
                            });

                            counter++; //UPDATE T_AMR
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_UPDATE_AMR, new Object[]{
                                GeneralConst.Numeric.ZERO,
                                fkDocument
                            });

                            counter++;//UPDATE T_DETAIL_RECLAMATION
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_SIMPLE_TRAITEMENT_RECLAMATION, new Object[]{
                                Integer.valueOf(userId.trim()),
                                Integer.valueOf(valueDecision),
                                Integer.valueOf(detailReclamationId)
                            });

                            counter++; //UPDATE T_RECLAMATION
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_TRAITEMENT_RECLAMATION, new Object[]{
                                reclamationId,
                                GeneralConst.Number.THREE
                            });

                            if (archives != null && !archives.isEmpty()) {
                                jsonDocumentArray = new JSONArray(archives);

                                for (int i = 0; i < jsonDocumentArray.length(); i++) {
                                    JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                                    ListArchiveDocument.add(jsonObject.getString(ContentieuxConst.ParamName.PV_DOCUMENT));
                                }
                            }

                            if (ListArchiveDocument.size() > 0) {
                                for (String docu : ListArchiveDocument) {
                                    counter++;
                                    bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.ARCHIVE_ACCUSER_RECEPTION2,
                                            new Object[]{
                                                reclamationId, DocumentConst.DocumentCode.LETTRE_RECLAMATION, docu, fkDocument, observationArch.trim()
                                            });
                                }
                            }

                            result = executeQueryBulkInsert(bulkQuery);

                            if (result) {
                                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                            } else {
                                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            break;

                        case GeneralConst.Number.THREE: // Décision Administrative ==== Rejet

                            valueDecision = ContentieuxConst.DecisionTraitement.REJET_RECLAMATION;

                            counter++;//UPDATE T_DETAIL_RECLAMATION
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_SIMPLE_TRAITEMENT_RECLAMATION, new Object[]{
                                Integer.valueOf(userId.trim()),
                                Integer.valueOf(valueDecision),
                                Integer.valueOf(detailReclamationId)
                            });

                            counter++; //UPDATE T_RECLAMATION
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_TRAITEMENT_RECLAMATION, new Object[]{
                                reclamationId,
                                GeneralConst.Number.THREE
                            });

                            counter++; //UPDATE T_AMR
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_UPDATE_AMR, new Object[]{
                                GeneralConst.Numeric.ZERO,
                                fkDocument
                            });

                            counter++; //GENERE BP
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.EXEC_F_NEW_BON_A_PAYER_CONTENTIEUX, new Object[]{
                                amountRestPayer,
                                null,
                                userId,
                                fkDocument,
                                propertiesMessage.getProperty("TXT_GENERE_BON_A_PAYER_CONTENTIEUX_ADMIN"),
                                codeAssujetti,
                                propertiesConfig.getContent(PropertiesConst.Config.TXT_ACCOUNT_BANK_BAP_CODE),
                                libelleAB.trim().trim(),
                                GeneralConst.Numeric.ZERO,
                                GeneralConst.Numeric.TWO,
                                codeNc
                            });

                            if (archives != null && !archives.isEmpty()) {
                                jsonDocumentArray = new JSONArray(archives);

                                for (int i = 0; i < jsonDocumentArray.length(); i++) {
                                    JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                                    ListArchiveDocument.add(jsonObject.getString(ContentieuxConst.ParamName.PV_DOCUMENT));
                                }
                            }

                            if (ListArchiveDocument.size() > 0) {
                                for (String docu : ListArchiveDocument) {
                                    counter++;
                                    bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.ARCHIVE_ACCUSER_RECEPTION2,
                                            new Object[]{
                                                reclamationId, DocumentConst.DocumentCode.LETTRE_RECLAMATION, docu, fkDocument, observationArch.trim()
                                            });
                                }
                            }

                            result = executeQueryBulkInsert(bulkQuery);

                            if (result) {
                                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                            } else {
                                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            break;
                    }

                    break;

                case GeneralConst.Number.TWO: // Décision Juridictionnelle

                    txtTraitementLibelle = propertiesMessage.getProperty("TXT_TRAITEMENT_RECLAMATION_JURIDICTIONNEL");
                    txtDecision = DocumentConst.DocumentCode.DECISION_JURIDICTIONNELLE;

                    switch (decisionId) {
                        case GeneralConst.Number.ONE:// Décision Juridictionnelle ==== Degrevement Partiel

                            valueDecision = ContentieuxConst.DecisionTraitement.DEGREVEMENT_PARTIEL;

                            //Mise à jour T_DETAIL_RECLAMATION
                            counter++;
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_SIMPLE_TRAITEMENT_JURIDICTIONNEL, new Object[]{
                                Integer.valueOf(userId.trim()),
                                Integer.valueOf(valueDecision),
                                Integer.valueOf(detailReclamationId)
                            });

                            //UPDATE T_RECOURS
                            RecoursJuridictionnel recoursJuridictionnel = ContentieuxBusiness.getRecoursJuridictionnelByReclamation(reclamationId);
                            if (recoursJuridictionnel != null) {
                                counter++;
                                bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_RECOURS_JURIDICTIONNEL, new Object[]{
                                    recoursJuridictionnel.getId(),
                                    GeneralConst.Number.TWO
                                });
                            }
                            counter++; //UPDATE T_RECLAMATION

                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_TRAITEMENT_RECLAMATION_V2, new Object[]{
                                reclamationId,
                                GeneralConst.Number.ONE,
                                GeneralConst.Number.FOUR
                            });

                            counter++;
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_UPDATE_AMR, new Object[]{
                                GeneralConst.Numeric.ZERO,
                                fkDocument
                            });

                            Amr am = ContentieuxBusiness.getAmrCloturer(fkDocument);

                            if (am != null) {
                                BonAPayer bp = ContentieuxBusiness.getBonAPayerByAMR(am.getAmr().getNumero().trim());
                                if (bp != null) {

                                    counter++; //UPDATE T_BON_A_PAYER (DECISION JURIDICTIONNELLE)
                                    bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_BON_A_PAYER_ADMIN, new Object[]{
                                        bp.getCode().trim(),
                                        GeneralConst.Number.THREE
                                    });

                                }
                            }

                            counter++;// GENERER BON A PAYER
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.EXEC_F_NEW_BON_A_PAYER_CONTENTIEUX, new Object[]{
                                amountDifference,
                                null,
                                userId,
                                fkDocument,
                                propertiesMessage.getProperty("TXT_GENERE_BON_A_PAYER_CONTENTIEUX_JURIDIQUE"),
                                codeAssujetti,
                                propertiesConfig.getContent(PropertiesConst.Config.TXT_ACCOUNT_BANK_BAP_CODE),
                                libelleAB.trim().trim(),
                                GeneralConst.Numeric.ZERO,
                                GeneralConst.Numeric.THREE,
                                codeNc
                            });

                            result = executeQueryBulkInsert(bulkQuery);

                            if (result) {
                                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                            } else {
                                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            break;
                        case GeneralConst.Number.TWO:// Décision Juridictionnelle ==== Degrevement Total

                            valueDecision = ContentieuxConst.DecisionTraitement.DEGREVEMENT_TOTAL;

                            DetailFichePriseCharge detailFP;
                            String numNC;
                            String devise = null;

                            // Suivi comptable
                            Amr amrs = ContentieuxBusiness.loaderAmrByNumero(fkDocument);
                            if (amrs != null) {
                                detailFP = ContentieuxBusiness.getArticleBudgetaireByFPC(amrs.getFichePriseCharge().getCode().trim());
                                if (detailFP != null) {
                                    numNC = detailFP.getNc().trim();
                                } else {
                                    numNC = GeneralConst.EMPTY_STRING;
                                }
                                DetailsNc detailNc = ContentieuxBusiness.getDetailNcByNc(numNC.trim());
                                if (detailNc != null) {
                                    if (detailNc.getDevise() != null) {
                                        devise = detailNc.getDevise().trim();
                                    } else {
                                        devise = GeneralConst.Devise.DEVISE_CDF;
                                    }
                                } else {
                                    devise = GeneralConst.Devise.DEVISE_CDF;
                                }
                            } else {
                                numNC = GeneralConst.EMPTY_STRING;
                            }

                            counter++;
                            bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_SUIVI_DECLARATION, new Object[]{
                                numNC,
                                fkDocument,
                                txtTraitementLibelle.concat(GeneralConst.SPACE)
                                .concat(GeneralConst.BRAKET_OPEN)
                                .concat(propertiesMessage.getProperty("TXT_DEGREVEMENT_TOTAL_JURIDIQUE"))
                                .concat(GeneralConst.BRAKET_CLOSE),
                                GeneralConst.Numeric.ZERO,
                                GeneralConst.Numeric.ZERO,
                                propertiesMessage.getProperty("TXT_DEGREVEMENT_TOTAL_JURIDIQUE").concat(
                                GeneralConst.SPACE).concat(fkDocument),
                                devise
                            });

                            //Crédite implôt
                            counter++;
                            bulkQuery.put(counter + SQLQueryTaxation.ExecuteQuery.EXEC_F_NEW_CREDIT_IMPOT, new Object[]{
                                codeAssujetti,
                                fkDocument,
                                propertiesMessage.getProperty("TXT_CREDIT_IMPOT_CONTENTIEUX"),
                                twentyPourcent,
                                GeneralConst.Numeric.ZERO,
                                GeneralConst.Numeric.ZERO,
                                userId,
                                GeneralConst.Numeric.ZERO,
                                GeneralConst.Numeric.ONE,
                                devise
                            });

                            counter++; // UPDATE AMR
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.F_UPDATE_AMR, new Object[]{
                                GeneralConst.Numeric.ZERO,
                                fkDocument
                            });

                            //UPDATE T_RECOURS
                            RecoursJuridictionnel recoursJuridict = ContentieuxBusiness.getRecoursJuridictionnelByReclamation(reclamationId);
                            if (recoursJuridict != null) {
                                counter++;
                                bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_RECOURS_JURIDICTIONNEL, new Object[]{
                                    recoursJuridict.getId(),
                                    GeneralConst.Number.TWO
                                });
                            }

                            counter++; //UPDATE T_RECLAMATION
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_TRAITEMENT_RECLAMATION_V2, new Object[]{
                                reclamationId,
                                GeneralConst.Number.ONE,
                                GeneralConst.Number.FOUR
                            });

                            if (archives != null && !archives.isEmpty()) {
                                jsonDocumentArray = new JSONArray(archives);

                                for (int i = 0; i < jsonDocumentArray.length(); i++) {
                                    JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                                    ListArchiveDocument.add(jsonObject.getString(ContentieuxConst.ParamName.PV_DOCUMENT));
                                }
                            }

                            if (ListArchiveDocument.size() > 0) {
                                for (String docu : ListArchiveDocument) {
                                    counter++;
                                    bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.ARCHIVE_ACCUSER_RECEPTION2,
                                            new Object[]{
                                                reclamationId, DocumentConst.DocumentCode.LETTRE_RECLAMATION, docu, fkDocument, observationArch.trim()
                                            });
                                }
                            }

                            result = executeQueryBulkInsert(bulkQuery);

                            if (result) {
                                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                            } else {
                                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            break;

                        case GeneralConst.Number.THREE: // Rejet du Décision Juridictionnelle

                            //UPDATE T_RECOURS
                            RecoursJuridictionnel recoursJurid = ContentieuxBusiness.getRecoursJuridictionnelByReclamation(reclamationId);
                            if (recoursJurid != null) {
                                counter++;
                                bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_RECOURS_JURIDICTIONNEL, new Object[]{
                                    recoursJurid.getId(),
                                    GeneralConst.Number.TWO
                                });
                            }

                            counter++; //UPDATE T_RECLAMATION
                            bulkQuery.put(counter + SQLQueryContentieux.ExecuteQuery.UPDATE_TRAITEMENT_RECLAMATION_V2, new Object[]{
                                reclamationId,
                                GeneralConst.Number.ONE,
                                GeneralConst.Number.FOUR
                            });

                            result = executeQueryBulkInsert(bulkQuery);

                            if (result) {
                                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
                            } else {
                                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                            }

                            break;
                    }
                    break;

            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String printerDocumentGenerique(HttpServletRequest request) {

        bulkUpdateDoc = new HashMap<>();
        int counter = GeneralConst.Numeric.ZERO;

        String dataReturn = GeneralConst.EMPTY_STRING;

        String documentReference;
        String typeDocument;
        Archive archive;
        PrintDocument printDocument = new PrintDocument();

        try {
            documentReference = request.getParameter(ContentieuxConst.ParamName.DOCUMENT_REFERENCE);
            typeDocument = request.getParameter(ContentieuxConst.ParamName.TYPE_DOCUMENT);

            switch (typeDocument.toUpperCase().trim()) {

                case DocumentConst.DocumentCode.BON_A_PAYER:

                    List<BonAPayer> listBonAPayer = ContentieuxBusiness.getListBonAPayerByAMR_V2(documentReference.trim());

                    if (listBonAPayer.size() > 0) {
                        BonAPayer bonApayer;

                        if (listBonAPayer.size() > 1) {
                            bonApayer = listBonAPayer.get(1);
                        } else {
                            bonApayer = listBonAPayer.get(0);
                        }
                        archive = NotePerceptionBusiness.getArchiveByRefDocument(
                                bonApayer.getCode().trim());

                        if (archive != null) {
                            dataReturn = archive.getDocumentString();
                        } else {
                            if (!bonApayer.getEtatImpression()) {
                                bulkUpdateDoc.put(counter + SQLQueryNotePerception.UPDATE_BP_PRINT,
                                        new Object[]{
                                            bonApayer.getCode()
                                        });

                                NotePerceptionBusiness.executeQueryBulkInsert(bulkUpdateDoc);
                            }
                            dataReturn = printDocument.createBonAPayerContentieux(bonApayer);
                        }

                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;

                case DocumentConst.DocumentCode.NP:
                    break;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadDossierRecours(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;

        String codeAssujetti = request.getParameter(ContentieuxConst.ParamName.CODE_ASSUJETTI);
        String numeberDossier = request.getParameter(ContentieuxConst.ParamName.NUMERO_DOCUMENT);
        String idUser = request.getParameter(ContentieuxConst.ParamName.ID_USER);

        String article;
        Date receptionDate;
        String codeBudgetaire;
        String devise = GeneralConst.Devise.DEVISE_CDF;
        BigDecimal amountDu = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal amountTwentyPourcent = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal amountTwentyPourcentNonConteste = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal amountContester = new BigDecimal(GeneralConst.Number.ZERO);
        List<JsonObject> jsonAmrList = new ArrayList<>();

        try {
            objReclamation = new Reclamation();

            objReclamation = ContentieuxBusiness.loadReclamationByTitreAndAssujetti(numeberDossier, codeAssujetti);

            List<JsonObject> reclamationJsonList = new ArrayList<>();

            if (objReclamation != null) {

                reclamationJson = new JsonObject();

                reclamationJson.addProperty(ContentieuxConst.ParamName.RECLAMATION_ID,
                        objReclamation.getId());
                reclamationJson.addProperty(TaxationConst.ParamName.DATE_CREATE,
                        Tools.formatDateToString(objReclamation.getDateCreat()));
                reclamationJson.addProperty(ContentieuxConst.ParamName.DATE_RECEPTION_COURRIER,
                        Tools.formatDateToString(objReclamation.getDateReceptionCourier()));
                reclamationJson.addProperty(ContentieuxConst.ParamName.OBSERVATION,
                        objReclamation.getObservation());
                reclamationJson.addProperty(ContentieuxConst.ParamName.ETAT, objReclamation.getEtat());
                reclamationJson.addProperty(ContentieuxConst.ParamName.ETAT_RECLAMATION,
                        objReclamation.getEtatReclamation());
                reclamationJson.addProperty(ContentieuxConst.ParamName.RECLAMATION_TRAITER,
                        objReclamation.getEstTraiter());
                reclamationJson.addProperty(ContentieuxConst.ParamName.TYPE_RECLAMATION,
                        objReclamation.getTypeReclamation());

                Personne personne = IdentificationBusiness.getPersonneByCode(
                        objReclamation.getFkPersonne().getCode().trim());

                if (personne != null) {
                    reclamationJson.addProperty(ContentieuxConst.ParamName.AssujettiCode,
                            personne.getCode().trim());
                    reclamationJson.addProperty(ContentieuxConst.ParamName.AssujettiName,
                            personne.toString().toUpperCase().trim());

                    reclamationJson.addProperty(ContentieuxConst.ParamName.LegalFormName,
                            personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                    Adresse adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                            personne.getCode().trim());
                    if (adresse != null) {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                                adresse.toString().toUpperCase().trim());
                    } else {
                        reclamationJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                                GeneralConst.EMPTY_STRING);
                    }
                } else {
                    reclamationJson.addProperty(ContentieuxConst.ParamName.AssujettiCode,
                            GeneralConst.EMPTY_STRING);

                    reclamationJson.addProperty(ContentieuxConst.ParamName.AssujettiName,
                            GeneralConst.EMPTY_STRING);

                    reclamationJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                            GeneralConst.EMPTY_STRING);
                    reclamationJson.addProperty(ContentieuxConst.ParamName.LegalFormName,
                            GeneralConst.EMPTY_STRING);

                }
                reclamationJson.addProperty(ContentieuxConst.ParamName.REFERENCE_COURRIER_RECLAMATION,
                        objReclamation.getReferenceCourierReclamation().toUpperCase().trim());
                reclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_ID,
                        objReclamation.getAgentCreat().getCode());
                reclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_NAME,
                        objReclamation.getAgentCreat().toString().toUpperCase().trim());

                if (objReclamation.getAgentCreat().getFonction() != null) {
                    reclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_QUALITY,
                            objReclamation.getAgentCreat().getFonction().getIntitule().toUpperCase().trim());
                } else {
                    reclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_QUALITY,
                            GeneralConst.EMPTY_STRING);
                }

                List<DetailsReclamation> listDetailsReclamations = ContentieuxBusiness.getDetailReclamationByFkReclamation(
                        objReclamation.getId().trim());

                if (!listDetailsReclamations.isEmpty()) {

                    List<JsonObject> detailReclamationJsonList = new ArrayList<>();

                    reclamationJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION_IS_EXIST,
                            GeneralConst.Number.ONE);

                    for (DetailsReclamation detailsReclamation : listDetailsReclamations) {

                        detailReclamationJson = new JsonObject();

                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.RECLAMATION_ID,
                                detailsReclamation.getFkReclamation().getId().trim());
                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.DETAIL_RECLAMATION_ID,
                                detailsReclamation.getId());
                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.REFERENCE_DOCUMENT,
                                detailsReclamation.getReferenceDocument());

                        if (detailsReclamation.getTypeDocument() != null
                                && !detailsReclamation.getTypeDocument().isEmpty()) {

                            // String exerciceFiscal;
                            SuiviComptableDeclaration sc;
                            ///String codeBudgetaire;
                            detailFPC = new DetailFichePriseCharge();
                            Amr amr;

                            switch (detailsReclamation.getTypeDocument()) {
                                case DocumentConst.DocumentCode.AMR:
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.TYPE_DOCUMENT,
                                            DocumentConst.DocumentName.AVIS_MISE_RECOUVREMENT);

                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.TYPE_DOCUMENT_2,
                                            DocumentConst.DocumentCode.AMR);
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                            detailsReclamation.getMontantContester());
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                            detailsReclamation.getPourcentageMontantNonContestre());

                                    amr = ContentieuxBusiness.loaderAmrByNumero_V2(detailsReclamation.getReferenceDocument().trim());
                                    if (amr != null) {
                                        detailReclamationJson.addProperty(TaxationConst.ParamName.AMOUNT,
                                                amr.getNetAPayer());

                                        detailFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amr.getFichePriseCharge().getCode().trim());

                                        if (detailFPC != null) {

                                            NoteCalcul nc = TaxationBusiness.getNoteCalculByNumero(
                                                    detailFPC.getNc().trim());

                                            if (nc != null) {

                                                //exerciceFiscal = Tools.getExerciceFiscal(nc);
                                                if (nc.getExercice() != null) {

                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                            nc.getExercice());
                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                            GeneralConst.EMPTY_STRING);
                                                }

                                                if (!nc.getService().isEmpty()) {
                                                    Service service = ContentieuxBusiness.getServiceByCode(nc.getService().trim());
                                                    if (service != null) {

                                                        if (service.getIntitule() != null) {
                                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                    service.getIntitule().toUpperCase().trim());
                                                        } else {
                                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                    GeneralConst.EMPTY_STRING);
                                                        }

                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                GeneralConst.EMPTY_STRING);
                                                    }
                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                            GeneralConst.EMPTY_STRING);
                                                }

                                                article = Tools.getArticleByNC(nc);

                                                if (article != null) {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                            article.toUpperCase());

                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                            GeneralConst.EMPTY_STRING);
                                                }

                                                codeBudgetaire = Tools.getCodeBudgetaireByNC(nc);
                                                if (codeBudgetaire != null && !codeBudgetaire.isEmpty()) {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                            codeBudgetaire.toUpperCase());
                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                            GeneralConst.EMPTY_STRING);
                                                }

                                            } else {
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                        GeneralConst.EMPTY_STRING);
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                        GeneralConst.EMPTY_STRING);
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                        GeneralConst.EMPTY_STRING);
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                        GeneralConst.EMPTY_STRING);
                                            }

                                            sc = ContentieuxBusiness.getSuiviComptableDeclarationByNc(detailFPC.getNc().trim());
                                            if (sc != null) {
                                                if (sc.getDevise() != null) {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE, sc.getDevise());
                                                    devise = sc.getDevise();
                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                            GeneralConst.Devise.DEVISE_CDF);
                                                }

                                            } else {
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                        GeneralConst.Devise.DEVISE_CDF);
                                            }

                                        } else {
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                    GeneralConst.Devise.DEVISE_CDF);
                                        }
                                    }

                                    break;
                            }

                        } else {
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.TYPE_DOCUMENT,
                                    GeneralConst.EMPTY_STRING);
                        }

                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.MOTIF,
                                detailsReclamation.getMotif());

                        if (detailsReclamation.getObservationTraitement() != null
                                && !detailsReclamation.getObservationTraitement().isEmpty()) {

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.OBSERVATION_TRAITEMENT,
                                    detailsReclamation.getObservationTraitement());
                        } else {
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.OBSERVATION_TRAITEMENT,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (detailsReclamation.getDateTraitement() != null
                                && detailsReclamation.getAgentTraitement() != null) {

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT,
                                    Tools.formatDateToString(detailsReclamation.getDateTraitement()));

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_TRAITEMENT,
                                    detailsReclamation.getAgentTraitement().toString());

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_ADMINISTRATIVE,
                                    detailsReclamation.getFkDecision() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : detailsReclamation.getFkDecision().getLibelleDecision());

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.EST_TRAITE,
                                    GeneralConst.Number.ONE);

                        } else {
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT,
                                    GeneralConst.EMPTY_STRING);
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.AGENT_TRAITEMENT,
                                    GeneralConst.EMPTY_STRING);
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.EST_TRAITE,
                                    GeneralConst.Number.ZERO);
                        }

                        if (detailsReclamation.getDateTraitementJuridique() != null) {

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.EST_TRAITE_JURIDICTIONNEL,
                                    GeneralConst.Number.ONE);

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT_JURIDIQUE,
                                    Tools.formatDateToString(detailsReclamation.getDateTraitementJuridique()));

                            Decision decision = ContentieuxBusiness.getDecisionById(
                                    detailsReclamation.getFkDecisionJuridique());

                            if (decision != null) {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_JURIDIQUE,
                                        decision.getLibelleDecision().toUpperCase().trim());
                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_JURIDIQUE,
                                        GeneralConst.EMPTY_STRING);
                            }

                        } else {

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.EST_TRAITE_JURIDICTIONNEL,
                                    GeneralConst.Number.ZERO);
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT_JURIDIQUE,
                                    GeneralConst.EMPTY_STRING);
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_JURIDIQUE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (detailsReclamation.getFkDecision() != null) {
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_ADMINISTRATIVE,
                                    detailsReclamation.getFkDecision().getLibelleDecision().toUpperCase());
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_ADMINISTRATIVE,
                                    detailsReclamation.getFkDecision().getId());
                        } else {
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_ADMINISTRATIVE,
                                    GeneralConst.EMPTY_STRING);
                        }
                        if (detailsReclamation.getMontantContester() != null) {
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                    detailsReclamation.getMontantContester());
                            amountContester = detailsReclamation.getMontantContester();
                        } else {
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                    GeneralConst.Number.ZERO);
                        }

                        if (detailsReclamation.getPourcentageMontantNonContestre() != null) {

                            if (detailsReclamation.getPourcentageMontantNonContestre().compareTo(BigDecimal.ZERO)
                                    > GeneralConst.Numeric.ZERO) {

                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                        detailsReclamation.getPourcentageMontantNonContestre());
                                BigDecimal twentyPourcent = new BigDecimal(GeneralConst.Number.FIVE);
                                amountTwentyPourcentNonConteste = detailsReclamation.getPourcentageMontantNonContestre();
                                amountTwentyPourcent = detailsReclamation.getPourcentageMontantNonContestre().multiply(twentyPourcent);
                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                        GeneralConst.Number.ZERO);
                            }

                        } else {
                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                    GeneralConst.Number.ZERO);
                        }

                        amountDu = amountContester.add(amountTwentyPourcent);

                        detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_DU,
                                amountDu);

                        detailReclamationJsonList.add(detailReclamationJson);
                    }

                    reclamationJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION,
                            detailReclamationJsonList.toString());

                } else {
                    reclamationJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION_IS_EXIST,
                            GeneralConst.Number.ZERO);
                    reclamationJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION,
                            GeneralConst.EMPTY_STRING);
                }
                reclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                        devise);
                reclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_DU,
                        amountDu);
                reclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                        amountTwentyPourcentNonConteste);
                reclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                        amountContester);

                reclamationJsonList.add(reclamationJson);

                dataReturn = reclamationJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;

    }

    public String saveRecours(HttpServletRequest request) {

        String userId = request.getParameter(ContentieuxConst.ParamName.USER_ID);
        String numeroEnregistrementGreffe = request.getParameter(ContentieuxConst.ParamName.NUMERO_ENREGISTREMENT_GREFFE);
//        String listDocument = request.getParameter(ContentieuxConst.ParamName.LIST_DOCUMENT);
        String motifRecours = request.getParameter(ContentieuxConst.ParamName.MOTIF_RECOURS);
        String codeReclamation = request.getParameter(ContentieuxConst.ParamName.FK_DOCUMENT);
        String dateDepot = request.getParameter(ContentieuxConst.ParamName.DATE_DEPOT_RECOURS);

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result = false;

        try {

//            JSONArray jsonArray = new JSONArray(listDocument);
            RecoursJuridictionnel recours = new RecoursJuridictionnel();

            Date dateRecours = new Date();
            dateRecours = Tools.formatStringFullToDate(dateDepot);

            recours.setNumeroEnregistrementGreffe(numeroEnregistrementGreffe);
            recours.setFkReclamation(new Reclamation(String.valueOf(codeReclamation)));
            recours.setMotifRecours(motifRecours);
            recours.setAgentCreat(new Agent(Integer.valueOf(userId)));
            recours.setDateDepotRecours(dateRecours);

            result = ContentieuxBusiness.saveRecours(recours);

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    String loadArchiveLR(HttpServletRequest request) {
        String dataRetutn = null;
        listArchiveAccuseReception = new ArrayList<>();
        try {

            String reclamationId = request.getParameter(ContentieuxConst.ParamName.RECLAMATION_ID);
            listArchiveAccuseReception = ContentieuxBusiness.getListArchiveByReference(reclamationId);

            if (!listArchiveAccuseReception.isEmpty()) {

                listJsonObjectArchAcc = new ArrayList<>();

                for (ArchiveAccuseReception archAcc : listArchiveAccuseReception) {

                    jsonObjectArchAcc = new JsonObject();

                    jsonObjectArchAcc.addProperty(ContentieuxConst.ParamName.DECLARATION_DOCUMENT, archAcc.getArchive());
                    if (!archAcc.getObservation().isEmpty()) {
                        jsonObjectArchAcc.addProperty(ContentieuxConst.ParamName.OBSERVATION_DOC, archAcc.getObservation());
                    } else {
                        jsonObjectArchAcc.addProperty(ContentieuxConst.ParamName.OBSERVATION_DOC, GeneralConst.EMPTY_STRING);
                    }

                    jsonObjectArchAcc.addProperty(ContentieuxConst.ParamName.LIBELLE_DOCUMENT,
                            archAcc.getTypeDocument());

                    listJsonObjectArchAcc.add(jsonObjectArchAcc);
                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataRetutn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return listJsonObjectArchAcc.toString();
    }

    public String saveRecoursJuridictionnel(HttpServletRequest request) {

        String dataReturn;

        String userId = request.getParameter(ContentieuxConst.ParamName.USER_ID);
        String motifRecours = request.getParameter(ContentieuxConst.ParamName.MOTIF);
        String fkReclamation = request.getParameter(ContentieuxConst.ParamName.RECLAMATION_ID);
        String numeroEnregistrementGreffe = request.getParameter(ContentieuxConst.ParamName.NUMERO_ENREGISTREMENT_GREFFE);
        String dateDepotRecours = request.getParameter(ContentieuxConst.ParamName.DATE_DEPOT_RECOURS);
        String dateAudience = request.getParameter(ContentieuxConst.ParamName.DATE_AUDIENCE);

        try {

            RecoursJuridictionnel recoursJuridictionnel = new RecoursJuridictionnel();

            recoursJuridictionnel.setAgentCreat(new Agent(Integer.valueOf(userId)));
            recoursJuridictionnel.setFkReclamation(new Reclamation(fkReclamation));
            recoursJuridictionnel.setMotifRecours(motifRecours);
            recoursJuridictionnel.setDateAudience(dateAudience);
            recoursJuridictionnel.setNumeroEnregistrementGreffe(numeroEnregistrementGreffe);
            recoursJuridictionnel.setDateDepotRecours(Tools.formatStringFullToDate(dateDepotRecours));

            boolean result = ContentieuxBusiness.saveRecoursJuridictionnel(recoursJuridictionnel);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadRecoursJuridictionnel(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String dateDebut = GeneralConst.EMPTY_STRING;
        String dateFin = GeneralConst.EMPTY_STRING;

        String devise = GeneralConst.Devise.DEVISE_CDF;
        BigDecimal amountDu = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal amountNonConteste = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal amountTwentyPourcentNonConteste = new BigDecimal(GeneralConst.Number.ZERO);
        BigDecimal amountContester = new BigDecimal(GeneralConst.Number.ZERO);

        String valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
        String typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
        String typePage = request.getParameter(TaxationConst.ParamName.TYPE_PAGE);
        String isAdvancedSearch = request.getParameter(ContentieuxConst.ParamName.IS_ADVANCED_SEARCH);
        if (isAdvancedSearch.equals(GeneralConst.Number.ONE)) {
            dateDebut = Tools.getValidFormat(request.getParameter(GeneralConst.ParamName.DATE_DEBUT));
            dateFin = Tools.getValidFormat(request.getParameter(GeneralConst.ParamName.DATE_FIN));
        }

        String typeReclamation = request.getParameter(ContentieuxConst.ParamName.TYPE_RECLAMATION);

        try {

            listRecoursJuridictionnel = new ArrayList<>();

            listRecoursJuridictionnel = ContentieuxBusiness.loadListRecours(
                    isAdvancedSearch.trim(),
                    typeSearch.trim(),
                    valueSearch.trim(),
                    dateDebut.trim(),
                    dateFin.trim(),
                    typeReclamation,
                    typePage);

            if (!listRecoursJuridictionnel.isEmpty()) {

                List<JsonObject> recoursJuridictionnelJsonList = new ArrayList<>();
                List<JsonObject> listJsonObjectArchive;

                for (RecoursJuridictionnel recours : listRecoursJuridictionnel) {

                    recoursJson = new JsonObject();

                    recoursJson.addProperty(ContentieuxConst.ParamName.RECOURS_ID,
                            recours.getId().trim());
                    recoursJson.addProperty(ContentieuxConst.ParamName.RECLAMATION_ID,
                            recours.getFkReclamation().getId().trim());

                    recoursJson.addProperty(TaxationConst.ParamName.DATE_CREATE,
                            Tools.formatDateToString(recours.getDateCreat()));

                    recoursJson.addProperty(ContentieuxConst.ParamName.ETAT, recours.getEtat());

                    Personne personne = IdentificationBusiness.getPersonneByCode(
                            recours.getFkReclamation().getFkPersonne().getCode().trim());

                    if (personne != null) {
                        recoursJson.addProperty(ContentieuxConst.ParamName.AssujettiCode,
                                personne.getCode().trim());
                        recoursJson.addProperty(ContentieuxConst.ParamName.AssujettiName,
                                personne.toString().toUpperCase().trim());

                        recoursJson.addProperty(ContentieuxConst.ParamName.LegalFormName,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        Adresse adresse = TaxationBusiness.getAdresseDefaultByAssujetti(
                                personne.getCode().trim());
                        if (adresse != null) {
                            recoursJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                                    adresse.toString().toUpperCase().trim());
                        } else {
                            recoursJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                                    GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        recoursJson.addProperty(ContentieuxConst.ParamName.AssujettiCode,
                                GeneralConst.EMPTY_STRING);

                        recoursJson.addProperty(ContentieuxConst.ParamName.AssujettiName,
                                GeneralConst.EMPTY_STRING);

                        recoursJson.addProperty(ContentieuxConst.ParamName.AdresseName,
                                GeneralConst.EMPTY_STRING);
                        recoursJson.addProperty(ContentieuxConst.ParamName.LegalFormName,
                                GeneralConst.EMPTY_STRING);

                    }
                    recoursJson.addProperty(ContentieuxConst.ParamName.REFERENCE_COURRIER_RECLAMATION,
                            recours.getFkReclamation().getReferenceCourierReclamation().toUpperCase().trim());
                    recoursJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_ID,
                            recours.getAgentCreat().getCode());
                    recoursJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_NAME,
                            recours.getAgentCreat().toString().toUpperCase().trim());

                    if (recours.getAgentCreat().getFonction() != null) {
                        recoursJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_QUALITY,
                                recours.getAgentCreat().getFonction().getIntitule().toUpperCase().trim());
                    } else {
                        recoursJson.addProperty(ContentieuxConst.ParamName.AGENT_CREATE_QUALITY,
                                GeneralConst.EMPTY_STRING);
                    }

                    recoursJson.addProperty(ContentieuxConst.ParamName.DATE_AUDIENCE,
                            recours.getDateAudience() == null ? GeneralConst.EMPTY_STRING
                                    : recours.getDateAudience().trim());

                    recoursJson.addProperty(ContentieuxConst.ParamName.DATE_DEPOT_RECOURS,
                            recours.getDateDepotRecours() == null ? GeneralConst.EMPTY_STRING
                                    : Tools.formatDateToString(recours.getDateDepotRecours()));

                    recoursJson.addProperty(ContentieuxConst.ParamName.MOTIF_RECOURS,
                            recours.getMotifRecours() == null ? GeneralConst.EMPTY_STRING
                                    : recours.getMotifRecours().trim());

                    recoursJson.addProperty(ContentieuxConst.ParamName.NUMERO_ENREGISTREMENT_GREFFE,
                            recours.getNumeroEnregistrementGreffe() == null ? GeneralConst.EMPTY_STRING
                                    : recours.getNumeroEnregistrementGreffe().trim());

                    List<DetailsReclamation> listDetailsReclamations = ContentieuxBusiness.getDetailReclamationByFkReclamation(
                            recours.getFkReclamation().getId().trim());

                    if (!listDetailsReclamations.isEmpty()) {

                        List<JsonObject> detailReclamationJsonList = new ArrayList<>();

                        recoursJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION_IS_EXIST,
                                GeneralConst.Number.ONE);

                        for (DetailsReclamation detailsReclamation : listDetailsReclamations) {

                            detailReclamationJson = new JsonObject();
                            BigDecimal twentyPourcent = new BigDecimal(GeneralConst.Number.FIVE);

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DECISION_ADMINISTRATIVE,
                                    detailsReclamation.getFkDecision() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : detailsReclamation.getFkDecision().getLibelleDecision());
                            recoursJson.addProperty(ContentieuxConst.ParamName.DECISION_ADMINISTRATIVE,
                                    detailsReclamation.getFkDecision() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : detailsReclamation.getFkDecision().getLibelleDecision());

                            recoursJson.addProperty(ContentieuxConst.ParamName.CODE_DECISION_ADMINISTRATIVE,
                                    detailsReclamation.getFkDecision().getId());

                            if (detailsReclamation.getMontantContester() != null) {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                        detailsReclamation.getMontantContester());
                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                        GeneralConst.Number.ZERO);
                            }

                            if (detailsReclamation.getPourcentageMontantNonContestre() != null) {

                                if (detailsReclamation.getPourcentageMontantNonContestre().compareTo(BigDecimal.ZERO)
                                        > GeneralConst.Numeric.ZERO) {

                                    amountTwentyPourcentNonConteste = detailsReclamation.getPourcentageMontantNonContestre();

                                    amountContester = detailsReclamation.getMontantContester();

                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                            amountTwentyPourcentNonConteste);

                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                            amountContester);

                                    amountNonConteste = detailsReclamation.getPourcentageMontantNonContestre().multiply(twentyPourcent);

                                } else {
                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                            GeneralConst.Number.ZERO);

                                    detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                            GeneralConst.Number.ZERO);
                                }

                            } else {
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.POURCENTAGE_MONTANT_NON_CONTESTER,
                                        GeneralConst.Number.ZERO);
                                detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_CONTESTER,
                                        GeneralConst.Number.ZERO);
                            }

                            amountDu = amountContester.add(amountNonConteste);

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.MONTANT_DU,
                                    amountDu);

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DETAIL_RECLAMATION_ID,
                                    detailsReclamation.getId());

                            if (detailsReclamation.getTypeDocument() != null
                                    && !detailsReclamation.getTypeDocument().isEmpty()) {

                                SuiviComptableDeclaration sc;
                                String article;
                                String codeBudgetaire;
                                detailFPC = new DetailFichePriseCharge();
                                Amr amr;

                                switch (detailsReclamation.getTypeDocument()) {

                                    case DocumentConst.DocumentCode.AMR:
                                        amr = ContentieuxBusiness.loaderAmrByNumero_V2(detailsReclamation.getReferenceDocument().trim());
                                        if (amr != null) {

                                            detailReclamationJson.addProperty(TaxationConst.ParamName.AMOUNT,
                                                    amr.getNetAPayer());
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_AMR,
                                                    amr.getNumero().trim());

                                            recoursJson.addProperty(TaxationConst.ParamName.CODE_AMR,
                                                    amr.getNumero().trim());

                                            detailFPC = ContentieuxBusiness.getArticleBudgetaireByFPC(amr.getFichePriseCharge().getCode().trim());

                                            if (detailFPC != null) {

                                                NoteCalcul nc = TaxationBusiness.getNoteCalculByNumero(
                                                        detailFPC.getNc().trim());

                                                if (nc != null) {

                                                    if (nc.getExercice() != null) {

                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                                nc.getExercice());

                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                                GeneralConst.EMPTY_STRING);
                                                    }

                                                    if (!nc.getService().isEmpty()) {
                                                        Service service = ContentieuxBusiness.getServiceByCode(nc.getService().trim());
                                                        if (service != null) {

                                                            if (service.getIntitule() != null) {
                                                                detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                        service.getIntitule().toUpperCase().trim());
                                                            } else {
                                                                detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                        GeneralConst.EMPTY_STRING);
                                                            }

                                                        } else {
                                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                    GeneralConst.EMPTY_STRING);
                                                        }
                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                                GeneralConst.EMPTY_STRING);
                                                    }

                                                    article = Tools.getArticleByNC(nc);

                                                    if (article != null) {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                                article.toUpperCase());

                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                                GeneralConst.EMPTY_STRING);
                                                    }

                                                    codeBudgetaire = Tools.getCodeBudgetaireByNC(nc);

                                                    if (codeBudgetaire != null && !codeBudgetaire.isEmpty()) {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                                codeBudgetaire.toUpperCase());
                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                                GeneralConst.EMPTY_STRING);
                                                    }

                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                            GeneralConst.EMPTY_STRING);
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                            GeneralConst.EMPTY_STRING);
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                            GeneralConst.EMPTY_STRING);
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                            GeneralConst.EMPTY_STRING);
                                                }

                                                sc = ContentieuxBusiness.getSuiviComptableDeclarationByNc(detailFPC.getNc().trim());

                                                if (sc != null) {
                                                    if (sc.getDevise() != null) {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE, sc.getDevise());
                                                        devise = sc.getDevise();
                                                    } else {
                                                        detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                                GeneralConst.Devise.DEVISE_CDF);
                                                    }

                                                } else {
                                                    detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                            GeneralConst.Devise.DEVISE_CDF);
                                                }

                                            } else {
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                        GeneralConst.EMPTY_STRING);
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                        GeneralConst.EMPTY_STRING);
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                        GeneralConst.EMPTY_STRING);
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                        GeneralConst.EMPTY_STRING);
                                                detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                        GeneralConst.Devise.DEVISE_CDF);
                                            }

                                        } else {
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.CODE_OFFICIEL,
                                                    GeneralConst.EMPTY_STRING);
                                            detailReclamationJson.addProperty(TaxationConst.ParamName.DEVISE,
                                                    GeneralConst.Devise.DEVISE_CDF);
                                        }

                                        break;

                                }

                            }

                            if (detailsReclamation.getFkDecisionJuridique() != null) {
                                detailReclamationJson.addProperty(TaxationConst.ParamName.DECISION_JURIDICTIONNEL,
                                        detailsReclamation.getFkDecisionJuridique());
                            } else {
                                detailReclamationJson.addProperty(TaxationConst.ParamName.DECISION_JURIDICTIONNEL,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailReclamationJson.addProperty(TaxationConst.ParamName.OBSERVATION_TRAITEMENT,
                                    detailsReclamation.getObservationTraitement() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : detailsReclamation.getObservationTraitement().trim());

                            detailReclamationJson.addProperty(ContentieuxConst.ParamName.DATE_TRAITEMENT_JURIDIQUE,
                                    detailsReclamation.getDateTraitementJuridique() == null ? GeneralConst.EMPTY_STRING
                                            : Tools.formatDateToString(detailsReclamation.getDateTraitementJuridique()));

                            detailReclamationJsonList.add(detailReclamationJson);

                        }

                        recoursJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION,
                                detailReclamationJsonList.toString());

                    } else {
                        recoursJson.addProperty(ContentieuxConst.ParamName.DETAILS_RECLAMATION_IS_EXIST,
                                GeneralConst.Number.ZERO);
                    }

                    recoursJuridictionnelJsonList.add(recoursJson);
                }
                dataReturn = recoursJuridictionnelJsonList.toString();
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }
}
