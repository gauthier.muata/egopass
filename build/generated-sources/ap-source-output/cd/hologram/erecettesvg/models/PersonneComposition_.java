package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(PersonneComposition.class)
public class PersonneComposition_ { 

    public static volatile SingularAttribute<PersonneComposition, String> code;
    public static volatile SingularAttribute<PersonneComposition, String> composition;
    public static volatile SingularAttribute<PersonneComposition, String> personne;
    public static volatile SingularAttribute<PersonneComposition, Integer> numOrdre;
    public static volatile SingularAttribute<PersonneComposition, String> fonction;
    public static volatile SingularAttribute<PersonneComposition, Boolean> etat;

}