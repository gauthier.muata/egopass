/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class NotePerceptionVoirieConcentres implements Serializable{
    
    String flag;
    String codeQR;
    String nameTaxe;
    String transporteur;
    String adresse;
    String telephone;
    String produit;
    String plaque;
    String poids;
    String chiffre;
    String lettre;
    String numeroPerception;
    String banque;
    String compteBancaire;
    String lieuTaxation;
    String dateTaxation;
    String nomFonctionTaxateur;
    String modePaiement;
    String proprietaire;
    
    String montantEnChiffre;
    String montantEnLettre;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNameTaxe() {
        return nameTaxe;
    }

    public void setNameTaxe(String nameTaxe) {
        this.nameTaxe = nameTaxe;
    }

    public String getTransporteur() {
        return transporteur;
    }

    public void setTransporteur(String transporteur) {
        this.transporteur = transporteur;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getProduit() {
        return produit;
    }

    public void setProduit(String produit) {
        this.produit = produit;
    }

    public String getPlaque() {
        return plaque;
    }

    public void setPlaque(String plaque) {
        this.plaque = plaque;
    }

    public String getPoids() {
        return poids;
    }

    public void setPoids(String poids) {
        this.poids = poids;
    }

    public String getChiffre() {
        return chiffre;
    }

    public void setChiffre(String chiffre) {
        this.chiffre = chiffre;
    }

    public String getLettre() {
        return lettre;
    }

    public void setLettre(String lettre) {
        this.lettre = lettre;
    }

    

    public String getNumeroPerception() {
        return numeroPerception;
    }

    public void setNumeroPerception(String numeroPerception) {
        this.numeroPerception = numeroPerception;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }

    public String getCompteBancaire() {
        return compteBancaire;
    }

    public void setCompteBancaire(String compteBancaire) {
        this.compteBancaire = compteBancaire;
    }

    public String getLieuTaxation() {
        return lieuTaxation;
    }

    public void setLieuTaxation(String lieuTaxation) {
        this.lieuTaxation = lieuTaxation;
    }

    public String getDateTaxation() {
        return dateTaxation;
    }

    public void setDateTaxation(String dateTaxation) {
        this.dateTaxation = dateTaxation;
    }

    public String getNomFonctionTaxateur() {
        return nomFonctionTaxateur;
    }

    public void setNomFonctionTaxateur(String nomFonctionTaxateur) {
        this.nomFonctionTaxateur = nomFonctionTaxateur;
    }

    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }

    public String getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(String proprietaire) {
        this.proprietaire = proprietaire;
    }

    public String getMontantEnChiffre() {
        return montantEnChiffre;
    }

    public void setMontantEnChiffre(String montantEnChiffre) {
        this.montantEnChiffre = montantEnChiffre;
    }

    public String getMontantEnLettre() {
        return montantEnLettre;
    }

    public void setMontantEnLettre(String montantEnLettre) {
        this.montantEnLettre = montantEnLettre;
    }
    
    
    
    
}
