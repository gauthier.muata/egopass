/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var lblNameResponsible, lblAddress, lblNifResponsible, lblLegalForm, lblMontantDu;
var lbl1, lbl2, lbl3, lbl4;
var lblPartieMontantConteste, lblCinqPourcentPartieConteste, lblRestePayer;

var btnCallModalSearchResponsable, btnEnregistrerRecours, btnJoindreArchiveRecours;
var btnSearchAndAddDocument, typeTraitement, formRadioButton, btnValiderTraitement;

var inputNumeroDocument, inputNumeroEnregistrementGreffe;
var textMotifRecours, dateDepotRecours, inputDegreveAmount;
var divMontantDu, divRestPayer;

var modalTraitementJuridictinnel;

var selectDecision, referenceDocument, codeAssujetti;

var tableReclammation;
var tempDocumentList = [], tempReclamation = [];

var assujettiModal;
var codeResponsible = ''
var codeFormeJuridique = '';
var adresseId = '';
var codeReclamation = '';
var archivesReclamations, lblNbDocumentReclamation;

var typeDoc = 'RCL';
var codeDoc = undefined;
var codeTypeDocs;

$(function () {

    mainNavigationLabel.text('CONTENTIEUX');
    secondNavigationLabel.text('Gestion des décisions');

    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');
    lblLegalForm = $('#lblLegalForm');
    lblMontantDu = $('#lblMontantDu');

    formRadioButton = $('#formRadioButton');
    divMontantDu = $('#divMontantDu');
    divRestPayer = $('#divRestPayer');

    lbl1 = $('#lbl1');
    lbl2 = $('#lbl2');
    lbl3 = $('#lbl3');
    lbl4 = $('#lbl4');

    codeTypeDocs = 'RCL';

    selectDecision = $('#selectDecision');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    btnEnregistrerRecours = $('#btnEnregistrerRecours');
    btnJoindreArchiveRecours = $('#btnJoindreArchiveRecours');
    btnSearchAndAddDocument = $('#btnSearchAndAddDocument');
    btnValiderTraitement = $('#btnValiderTraitement');

    inputNumeroDocument = $('#inputNumeroDocument');
    inputNumeroEnregistrementGreffe = $('#inputNumeroEnregistrementGreffe');
    inputDegreveAmount = $('#inputDegreveAmount');
    textMotifRecours = $('#textMotifRecours');
    dateDepotRecours = $('#dateDepotRecours');

    lblPartieMontantConteste = $('#lblPartieMontantConteste');
    lblCinqPourcentPartieConteste = $('#lblCinqPourcentPartieConteste');
    lblRestePayer = $('#lblRestePayer');

    lblNbDocumentReclamation = $('#lblNbDocumentReclamation');

    assujettiModal = $('#assujettiModal');
    modalTraitementJuridictinnel = $('#modalTraitementJuridictinnel');

    tableReclammation = $('#tableReclammation');

    btnCallModalSearchResponsable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        inputNumeroEnregistrementGreffe.val('');
        textMotifRecours.val('');
        dateDepotRecours.val('');
        inputNumeroDocument.val('');
        printRecoursDocument('');
        inputNumeroDocument.val('');

        assujettiModal.modal('show');
    });

    btnJoindreArchiveRecours.click(function (e) {

        e.preventDefault();

        if (archivesReclamations == '') {
            initUpload(codeDoc, codeTypeDocs);
        } else {
            initUpload(archivesReclamations, codeTypeDocs);
        }
    });

    btnSearchAndAddDocument.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeResponsible == '') {
            alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
            return;
        }

        searchDocument();

    });

    btnEnregistrerRecours.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (tempReclamation.length === 0) {
            alertify.alert('Veuillez choisir au moins une reclamation.');
            return;
        }

        for (var i = 0; i < tempReclamation.length; i++) {


            if (codeResponsible == '') {
                alertify.alert('Veuillez d\'abord sélectionner un assujetti.');
                return;
            }

            if (inputNumeroDocument.val().trim() == '') {
                alertify.alert('Veuillez d\'abord saisir un numéro d\'une réclamation.');
                return;
            }

            if (inputNumeroEnregistrementGreffe.val() == '') {
                alertify.alert('Veuillez d\'abord saisir un numéro de l\'enregistrement greffe.');
                return;
            }
            if (dateDepotRecours.val() == '') {
                alertify.alert('Veuillez d\'abord fournir la date du dépôt de recours.');
                return;
            }

            if (tempReclamation[i].reclamationTraiter == '2') {
                alertify.alert('Ce titre est déjà en recours');
                inputNumeroDocument.val('');
                inputNumeroEnregistrementGreffe.val('0');
                dateDepotRecours.val('');
                tempReclamation.length = 0;
                printRecoursDocument('');
                return;

            }

            break;
        }

        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce recours ?', function () {

            saveRecours();

        });

    });

    selectDecision.on('change', function () {

        inputDegreveAmount.val('');

        if (selectDecision.val() == '1') {

            inputDegreveAmount.removeAttr('disabled');
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:inline');
            divRestPayer.attr('style', 'display:inline');
        }
        else if (selectDecision.val() == '2') {

            inputDegreveAmount.attr('disabled', true);
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');
        } else if (selectDecision.val() == '3') {

            inputDegreveAmount.attr('disabled', true);
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:inline');
            divRestPayer.attr('style', 'display:inline');
        } else if (selectDecision.val() == '4') {

            inputDegreveAmount.attr('disabled', true);
            formRadioButton.attr('style', 'display:none');
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');

        } else {
            formRadioButton.attr('style', 'display:inline');
            inputDegreveAmount.attr('disabled', true);
            divMontantDu.attr('style', 'display:none');
            divRestPayer.attr('style', 'display:none');

            var checkControlRadio = $("#radioDegreveTotal");

            checkControlRadio.change(function (e) {
                e.preventDefault();

                inputDegreveAmount.attr('disabled', true);
                inputDegreveAmount.val('');
                divMontantDu.attr('style', 'display:none');
                divRestPayer.attr('style', 'display:none');
            });

            var checkControlRadioPartiel = $("#radioDegrevePartiel");
            checkControlRadioPartiel.change(function (e) {
                e.preventDefault();

                inputDegreveAmount.removeAttr('disabled');
                inputDegreveAmount.val('');
                divMontantDu.attr('style', 'display:inline');
                divRestPayer.attr('style', 'display:inline');
            });
        }

    });



});

function getSelectedAssujetiData() {
    lblNameResponsible.html(selectAssujettiData.nomComplet);
    lblLegalForm.html(selectAssujettiData.categorie);
    lblAddress.html(selectAssujettiData.adresse);
    codeResponsible = selectAssujettiData.code;
    lblNifResponsible.html(selectAssujettiData.nif);
    codeFormeJuridique = selectAssujettiData.codeForme;
    adresseId = selectAssujettiData.codeAdresse;

    lbl1.show();
    lbl2.show();
    lbl3.show();
    lbl4.show();

    responsibleObject = new Object();

    responsibleObject.codeResponsible = codeResponsible;
    responsibleObject.codeFormeJuridique = codeFormeJuridique;
    responsibleObject.adresseId = adresseId;
}

function searchDocument() {

    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadDossierRecours',
            'codeAssujetti': codeResponsible,
            'numeroDocument': inputNumeroDocument.val(),
            'idUser': userData.idUser
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            if (response == '0') {
                alertify.alert('Ce titre de réclamation déjà associé à un recours ou ne fait pas l\'objet d\'un recours');
                return;
            }

            setTimeout(function () {

                var documentList = JSON.parse(JSON.stringify(response));

                if (documentList.reclamationTraiter == '2') {
                    alertify.alert('Ce titre de reclamation déjà associé à un recours qui est en cours de traitement.');
                    return;
                }

                //alert(JSON.stringify(documentList));

                printRecoursDocument(documentList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

    var paramGuideValue = JSON.parse(getTourParam());

    if (paramGuideValue === 1) {
        Tour.run([
            {
                element: $('#divEntiteTableRefObs'),
                content: '<p style="font-size:17px;color:black">Entrer la référence du courrier de réclamation, l\'observation si necessaire et enfin cliquer sur le bouton enregistrer.</p>',
                language: 'fr',
                position: 'top',
                close: 'false'
            }
        ]);
    } else {
        Tour.run();
    }

}

function printRecoursDocument(documentList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> Réclamation </th>';
    header += '<th scope="col"> Date création </th>';
    header += '<th scope="col"> Date réception Courrier </th>';
    header += '<th scope="col"> Réference courrier </th>';
//    header += '<th scope="col"> Article budgétaire </th>';
    header += '<th scope="col"> Montant dû </th>';
    header += '<th scope="col"> 20% Montant non contesté </th>';
    //header += '<th scope="col"> 5% Montant non contesté </th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody id="idBodyAMR">';

    for (var i = 0; i < documentList.length; i++) {

//        var firstLineAB = '';
//
//        if (documentList[i].libelleArticleBudgetaire.length > 75) {
//            firstLineAB = documentList[i].libelleArticleBudgetaire.substring(0, 75).toUpperCase() + ' ...';
//        } else {
//            firstLineAB = documentList[i].libelleArticleBudgetaire.toUpperCase();
//        }


        var reclamation = {};
        reclamation.reclamationId = documentList[i].reclamationId;
        reclamation.dateCreate = documentList[i].dateCreate;
        reclamation.dateReceptionCourrier = documentList[i].dateReceptionCourrier;
        reclamation.referenceCourrierReclamation = documentList[i].referenceCourrierReclamation;
        reclamation.detailsReclamation = documentList[i].detailsReclamation;
        reclamation.montantDu = documentList[i].montantDu;
        reclamation.devise = documentList[i].devise;
        reclamation.montantContester = documentList[i].montantContester;
        reclamation.pourcentageMontantNonContester = documentList[i].pourcentageMontantNonContester;
        reclamation.etatReclamation = documentList[i].etatReclamation;
        reclamation.reclamationTraiter = documentList[i].reclamationTraiter;
        reclamation.assujettiCode = documentList[i].assujettiCode;
        codeReclamation = documentList[i].reclamationId;
        tempReclamation.push(reclamation);

        body += '<tr>';
        body += '<td style="width:12%;vertical-align:middle">' + documentList[i].reclamationId + '</td>';
        body += '<td style="width:12%;vertical-align:middle">' + documentList[i].dateCreate + '</td>';
        body += '<td style="width:12%;vertical-align:middle">' + documentList[i].dateReceptionCourrier + '</td>';
        body += '<td style="width:12%;vertical-align:middle">' + documentList[i].referenceCourrierReclamation + '</td>';
//        body += '<td style="width:25%;vertical-align:middle" title="' + documentList[i].libelleArticleBudgetaire.toUpperCase() + '"><span style="font-weight:bold;">' + documentList[i].codeOfficiel + '</span><br/><br/>' + firstLineAB + '</td>';
        body += '<td style="width:10%;vertical-align:middle">' + formatNumber(documentList[i].montantDu, documentList[i].devise) + '</td>';
        // body += '<td style="width:9%;vertical-align:middle">' + formatNumber(documentList[i].montantContester, documentList[i].devise) + '</td>';
        body += '<td style="width:9%;vertical-align:middle">' + formatNumber(documentList[i].pourcentageMontantNonContester, documentList[i].devise) + '</td>';


        if (documentList[i].reclamationTraiter == '1') {
            body += '<td style="width:5%"></td>';
            btnEnregistrerRecours.attr('style', 'display:inline');
//            inputNumeroEnregistrementGreffe.attr('disabled', 'false');
        } else {
            body += '<td style="width:5%;vertical-align:middle"><a onclick="showModalTraitementDecision(\'' + documentList[i].reclamationId + '\')" class="btn btn-success" title="traitement"><i class="fa fa-check-circle"></i></a></td>';
            btnEnregistrerRecours.attr('style', 'display:none');
//            inputNumeroEnregistrementGreffe.attr('disabled', 'true');
        }

        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableContent += '</tbody>';
    tableReclammation.html(tableContent);
    tableReclammation.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        columnDefs: [
            {"visible": false, "targets": 0}
        ],
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[0, 'asc']],
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(0, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

}

//function selectDocument() {
//
//    var listDocument = [];
//
//    if (tempReclamation.length > 0) {
//        for (var i = 0; i < tempReclamation.length; i++) {
//            if (tempReclamation[i].reclamationTraiter == '1') {
//
//                var idDocument = '#checkDocument_' + tempReclamation[i].reclamationId;
//                var document = $('#tableReclammation').find(idDocument);
//
//                if (document.is(':checked')) {
//                    var documentList = {};
//
//                    documentList.reclamationId = tempReclamation[i].reclamationId;
//                    documentList.dateCreate = tempReclamation[i].dateCreate;
//                    documentList.dateReceptionCourrier = tempReclamation[i].dateReceptionCourrier;
//                    documentList.referenceCourrierReclamation = tempReclamation[i].referenceCourrierReclamation;
//                    documentList.detailsReclamation = tempReclamation[i].detailsReclamation;
//                    documentList.montantDu = tempReclamation[i].montantDu;
//                    documentList.devise = tempReclamation[i].devise;
//                    documentList.montantContester = tempReclamation[i].montantContester;
//                    documentList.pourcentageMontantNonContester = tempReclamation[i].pourcentageMontantNonContester;
//                    documentList.etatReclamation = tempReclamation[i].etatReclamation;
//                    codeReclamation = tempReclamation[i].reclamationId;
//
//                    listDocument.push(documentList);
//                } else {
//                    return;
//                }
//            }
//        }
//        listDocument = JSON.stringify(listDocument);
//
//    } else {
//        alertify.alert('Veuillez d\'abord sélectionner au moins une réclamation.')
//        return;
//    }
//    return listDocument;
//}

function saveRecours() {

    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveRecouurs',
            'userId': userData.idUser,
            'numeroEnregistrementGreffe': inputNumeroEnregistrementGreffe.val(),
            'textMotifRecours': textMotifRecours.val(),
            'dateDepotRecours': dateDepotRecours.val(),
            'fkDocument': codeReclamation
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de la réclamation en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'enregistrement du recours s\'est effectué avec succès.');
                    resetData();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement du recours.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function resetData() {
    inputNumeroEnregistrementGreffe.val('');
    textMotifRecours.val('');
    dateDepotRecours.val('');
    inputNumeroDocument.val('');
    archivesReclamations = '';
    btnSearchAndAddDocument.trigger('click');
}

function showModalTraitementDecision(reclamationId) {

//    var documentRef, typeDocument,
//            , typeRemiseGracieuse,
//            documentArchive, idDecision;
    var amount, devise, fivePercent, montantConteste, degrevValue;
    var amountDegre;
    var libelleAB, detailReclamationId;
//
    var lblMontantDu = $('#lblMontantDu');
    var lblRestePayer = $('#lblRestePayer');

    var lblPartieMontantConteste = $('#lblPartieMontantConteste');
    var lblCinqPourcentPartieConteste = $('#lblCinqPourcentPartieConteste');

    var dataDecision = '<option value ="0">-- Sélectionner une décision --</option>'
            + '<option value="1">Dégrèvement partiel</option>'
            + '<option value="2">Dégrèvement total</option>'
            + '<option value="3">Rejet recours</option>'
    /*+ '<option value="4">Admission en non-valeur</option>'*/

    selectDecision.html(dataDecision);

    for (var i = 0; i < tempReclamation.length; i++) {

        if (tempReclamation[i].reclamationId == reclamationId) {

            amount = tempReclamation[i].montantDu;
            fivePercent = tempReclamation[i].pourcentageMontantNonContester;
            montantConteste = tempReclamation[i].montantContester;
            devise = tempReclamation[i].devise;
            codeAssujetti = tempReclamation[i].assujettiCode;
//            documentArchive = '';
            idReclamation = reclamationId;
            lblMontantDu.html(formatNumber(amount, devise));
            typeTraitement = 2;

            lblPartieMontantConteste.html(formatNumber(montantConteste, devise));
            lblCinqPourcentPartieConteste.html(formatNumber(fivePercent, devise));

            lblRestePayer.html(formatNumber((parseFloat(amount) - parseFloat(fivePercent)), devise));
//            avecSurcis = tempDetailReclamationList[i].decisionSurcis;
//            dateDecisionSurcis = tempDetailReclamationList[i].dateDecisionSurcis;

            var detailReclamation = JSON.parse(tempReclamation[i].detailsReclamation);

            for (var j = 0; j < detailReclamation.length; j++) {
                if (detailReclamation[j].reclamationId == reclamationId) {
                    libelleAB = detailReclamation[j].libelleArticleBudgetaire;
                    detailReclamationId = detailReclamation[j].detailReclamationId;
                    referenceDocument = detailReclamation[j].referenceDocument;
                    break;
                }
            }


            break;
        }
    }

    modalTraitementJuridictinnel.modal('show');

    if (selectDecision.val() == '5') {
        formRadioButton.attr('style', 'display:inline');
    } else {
        formRadioButton.attr('style', 'display:none');
    }

    inputDegreveAmount.change(function () {

        degrevValue = parseFloat(inputDegreveAmount.val());

        if (degrevValue >= 0) {
            if (degrevValue > montantConteste) {
                alertify.alert('Le montant dégrevé est superieur à ' +formatNumber(montantConteste, devise)+'. Veuillez saisir un montant valide.');
                inputDegreveAmount.val('');
                return;
            } else {
                amountDegre = parseFloat(amount) - parseFloat(degrevValue) - parseFloat(fivePercent);
                lblRestePayer.html(formatNumber(amountDegre, devise));
            }
        } else {
            amountDegre = parseFloat(amount) - parseFloat(fivePercent);
            lblRestePayer.html(formatNumber(amountDegre, devise));
        }
    });



    btnValiderTraitement.click(function (e) {
        e.preventDefault();

        var selectedDecision = selectDecision.val();

        var selectState = $('input[name=radioValider]:checked').val();

        if (selectedDecision == "0") {
            alertify.alert('Veuillez sélectionner un décision.');
            return;
        }


        if (inputDegreveAmount.val() == '') {
            amountDegre = 0;
        } else {
            degrevValue = parseFloat(inputDegreveAmount.val());
            amountDegre = amount - degrevValue;
        }

        if (parseInt(amountDegre) != 0 && parseInt(inputDegreveAmount.val()) > amount) {
            alertify.alert('Le montant dégrevé est superieur au montant dû. Veuillez saisir un montant valide.');
            return;
        }

        switch (selectedDecision) {
            case "1" :
                if (inputDegreveAmount.val() == '') {
                    alertify.alert('Aucun montant dégrevé est fourni. Veuillez saisir un montant.');
                }
                break;
        }

        switch (selectedDecision) {

            case "1" :
                if (amountDegre == 0) {
                    selectedDecision = '2';
                }
                break;
        }


        alertify.confirm('Etes-vous sûr de vouloir valider ce traitement ?', function () {

            traitementJuridictionnel(
                    selectedDecision,
                    // typeDocument,
                    detailReclamationId,
                    //documentArchive,
                    amountDegre,
                    fivePercent,
                    amount,
                    libelleAB
                    );
        });

    });
}

function traitementJuridictionnel(
        idDecision,
        // typeDocument,
        detailReclamationId,
        //documentArchive,
        amountDegre,
        fivePercent,
        amount,
        libelleAB
        ) {

    $.ajax({
        type: 'POST',
        url: 'contentieux_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'traitementDecision',
            'decisionId': idDecision,
//            'typeDocument': typeDocument,
            'detailReclamationId': detailReclamationId,
            //  'archives': documentArchive,
            'amountDegre': amountDegre,
            'pourcentageMontantNonContester': fivePercent,
            'amount': amount,
            'libelleArticleBudgetaire': libelleAB,
            'typeTraitement': '1',
            'fkDocument': referenceDocument,
            'codeAssujetti': codeAssujetti,
            'userId': userData.idUser,
            'reclamationId': idReclamation


        },
        beforeSend: function () {
            modalTraitementJuridictinnel.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Traitement en cours...</h5>'});
        },
        success: function (response)
        {
            modalTraitementJuridictinnel.unblock();

            if (response == '0') {
                showResponseError();
                return;
            }

            if (response == '1') {
                alertify.alert('Le traitement de la décision s\'est effectué avec success!');

                setTimeout(function () {
                    modalTraitementJuridictinnel.modal('hide');
                    printRecoursDocument('');
                    inputNumeroDocument.val('');
                    inputNumeroEnregistrementGreffe.val('');
                    textMotifRecours.val('');
                    dateDepotRecours.val('');

                    // modalDetailReclaration.modal('hide');

//                    idReclamation = '';
//                    stateTraitement = '';

//                    if (isAdvancedSearch == '0') {
//                        btnSearch.trigger('click');
//                    } else {
//                        btnAdvancedSearch.trigger('click');
//                    }
                }
                , 1);
            }


        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalTraitementJuridictinnel.unblockUI();
            showResponseError();
        }
    });
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesReclamations = getUploadedData();

    nombre = JSON.parse(archivesReclamations).length;

    switch (nombre) {
        case 0:
            lblNbDocumentReclamation.text('');
            break;
        case 1:
            lblNbDocumentReclamation.html('1 document');
            break;
        default:
            lblNbDocumentReclamation.html(nombre + ' documents');
    }
}