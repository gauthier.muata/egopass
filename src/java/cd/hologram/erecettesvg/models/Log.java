/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_LOG")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Log.findAll", query = "SELECT l FROM Log l"),
    @NamedQuery(name = "Log.findByIdLog", query = "SELECT l FROM Log l WHERE l.idLog = :idLog"),
    @NamedQuery(name = "Log.findByTable", query = "SELECT l FROM Log l WHERE l.table = :table"),
    @NamedQuery(name = "Log.findByAction", query = "SELECT l FROM Log l WHERE l.action = :action"),
    @NamedQuery(name = "Log.findByDateAction", query = "SELECT l FROM Log l WHERE l.dateAction = :dateAction"),
    @NamedQuery(name = "Log.findByAgentCreat", query = "SELECT l FROM Log l WHERE l.agentCreat = :agentCreat"),
    @NamedQuery(name = "Log.findByPc", query = "SELECT l FROM Log l WHERE l.pc = :pc"),
    @NamedQuery(name = "Log.findByApplication", query = "SELECT l FROM Log l WHERE l.application = :application"),
    @NamedQuery(name = "Log.findByCompte", query = "SELECT l FROM Log l WHERE l.compte = :compte"),
    @NamedQuery(name = "Log.findByIdOccurence", query = "SELECT l FROM Log l WHERE l.idOccurence = :idOccurence")})
public class Log implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_LOG")
    private Integer idLog;
    @Size(max = 50)
    @Column(name = "TABLE")
    private String table;
    @Size(max = 50)
    @Column(name = "ACTION")
    private String action;
    @Column(name = "DATE_ACTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAction;
    @Size(max = 50)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 100)
    @Column(name = "PC")
    private String pc;
    @Size(max = 100)
    @Column(name = "APPLICATION")
    private String application;
    @Size(max = 50)
    @Column(name = "COMPTE")
    private String compte;
    @Size(max = 50)
    @Column(name = "ID_OCCURENCE")
    private String idOccurence;
    @OneToMany(mappedBy = "fkTLog")
    private List<LogChampsChange> logChampsChangeList;

    public Log() {
    }

    public Log(Integer idLog) {
        this.idLog = idLog;
    }

    public Integer getIdLog() {
        return idLog;
    }

    public void setIdLog(Integer idLog) {
        this.idLog = idLog;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getDateAction() {
        return dateAction;
    }

    public void setDateAction(Date dateAction) {
        this.dateAction = dateAction;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getCompte() {
        return compte;
    }

    public void setCompte(String compte) {
        this.compte = compte;
    }

    public String getIdOccurence() {
        return idOccurence;
    }

    public void setIdOccurence(String idOccurence) {
        this.idOccurence = idOccurence;
    }

    @XmlTransient
    public List<LogChampsChange> getLogChampsChangeList() {
        return logChampsChangeList;
    }

    public void setLogChampsChangeList(List<LogChampsChange> logChampsChangeList) {
        this.logChampsChangeList = logChampsChangeList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLog != null ? idLog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Log)) {
            return false;
        }
        Log other = (Log) object;
        if ((this.idLog == null && other.idLog != null) || (this.idLog != null && !this.idLog.equals(other.idLog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Log[ idLog=" + idLog + " ]";
    }
    
}
