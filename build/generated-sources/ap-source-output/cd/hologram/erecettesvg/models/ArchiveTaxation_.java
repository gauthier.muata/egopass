package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.NoteCalcul;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ArchiveTaxation.class)
public class ArchiveTaxation_ { 

    public static volatile SingularAttribute<ArchiveTaxation, NoteCalcul> fkNc;
    public static volatile SingularAttribute<ArchiveTaxation, String> observation;
    public static volatile SingularAttribute<ArchiveTaxation, String> archive;
    public static volatile SingularAttribute<ArchiveTaxation, Integer> id;
    public static volatile SingularAttribute<ArchiveTaxation, String> directory;

}