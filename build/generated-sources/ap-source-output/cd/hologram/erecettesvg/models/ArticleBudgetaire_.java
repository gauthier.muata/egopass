package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.AbComplementBien;
import cd.hologram.erecettesvg.models.ArticleGenerique;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.BanqueAb;
import cd.hologram.erecettesvg.models.CleRepartition;
import cd.hologram.erecettesvg.models.DetailBordereau;
import cd.hologram.erecettesvg.models.DetailDepotDeclaration;
import cd.hologram.erecettesvg.models.DetailFichePriseCharge;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.NatureArticleBudgetaire;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.Periodicite;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.Unite;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ArticleBudgetaire.class)
public class ArticleBudgetaire_ { 

    public static volatile SingularAttribute<ArticleBudgetaire, String> agentMaj;
    public static volatile SingularAttribute<ArticleBudgetaire, Short> periodiciteVariable;
    public static volatile ListAttribute<ArticleBudgetaire, AbComplementBien> abComplementBienList;
    public static volatile ListAttribute<ArticleBudgetaire, Assujeti> assujetiList;
    public static volatile SingularAttribute<ArticleBudgetaire, String> code;
    public static volatile SingularAttribute<ArticleBudgetaire, Unite> unite;
    public static volatile SingularAttribute<ArticleBudgetaire, String> dateDebutPenalite;
    public static volatile SingularAttribute<ArticleBudgetaire, BigDecimal> fin;
    public static volatile ListAttribute<ArticleBudgetaire, DetailDepotDeclaration> detailDepotDeclarationList;
    public static volatile SingularAttribute<ArticleBudgetaire, Short> transactionnel;
    public static volatile SingularAttribute<ArticleBudgetaire, Boolean> etat;
    public static volatile SingularAttribute<ArticleBudgetaire, Integer> nbrJourLimite;
    public static volatile SingularAttribute<ArticleBudgetaire, Date> dateMaj;
    public static volatile ListAttribute<ArticleBudgetaire, BanqueAb> banqueAbList;
    public static volatile SingularAttribute<ArticleBudgetaire, BigDecimal> debut;
    public static volatile SingularAttribute<ArticleBudgetaire, Short> proprietaire;
    public static volatile SingularAttribute<ArticleBudgetaire, Tarif> tarif;
    public static volatile SingularAttribute<ArticleBudgetaire, String> arrete;
    public static volatile SingularAttribute<ArticleBudgetaire, Periodicite> periodicite;
    public static volatile SingularAttribute<ArticleBudgetaire, String> tarifVariable;
    public static volatile ListAttribute<ArticleBudgetaire, CleRepartition> cleRepartitionList;
    public static volatile ListAttribute<ArticleBudgetaire, Palier> palierList;
    public static volatile SingularAttribute<ArticleBudgetaire, String> articleMere;
    public static volatile SingularAttribute<ArticleBudgetaire, Boolean> quantiteVariable;
    public static volatile ListAttribute<ArticleBudgetaire, DetailFichePriseCharge> detailFichePriseChargeList;
    public static volatile SingularAttribute<ArticleBudgetaire, Date> dateCreat;
    public static volatile SingularAttribute<ArticleBudgetaire, NatureArticleBudgetaire> nature;
    public static volatile SingularAttribute<ArticleBudgetaire, Boolean> assujetissable;
    public static volatile SingularAttribute<ArticleBudgetaire, String> intitule;
    public static volatile ListAttribute<ArticleBudgetaire, DetailBordereau> detailBordereauList;
    public static volatile SingularAttribute<ArticleBudgetaire, String> echeanceLegale;
    public static volatile SingularAttribute<ArticleBudgetaire, ArticleGenerique> articleGenerique;
    public static volatile ListAttribute<ArticleBudgetaire, DetailsNc> detailsNcList;
    public static volatile SingularAttribute<ArticleBudgetaire, Boolean> palier;
    public static volatile SingularAttribute<ArticleBudgetaire, Boolean> periodeEcheance;
    public static volatile SingularAttribute<ArticleBudgetaire, String> dateLimitePaiement;
    public static volatile SingularAttribute<ArticleBudgetaire, String> dateDebutPeriode;
    public static volatile SingularAttribute<ArticleBudgetaire, String> agentCreat;
    public static volatile SingularAttribute<ArticleBudgetaire, String> codeOfficiel;

}