package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(RedressementAssujetissement.class)
public class RedressementAssujetissement_ { 

    public static volatile SingularAttribute<RedressementAssujetissement, Long> code;
    public static volatile SingularAttribute<RedressementAssujetissement, String> ancienAssujetissement;
    public static volatile SingularAttribute<RedressementAssujetissement, String> nouvelAssujetissement;
    public static volatile SingularAttribute<RedressementAssujetissement, String> agentCreat;
    public static volatile SingularAttribute<RedressementAssujetissement, Boolean> etat;

}