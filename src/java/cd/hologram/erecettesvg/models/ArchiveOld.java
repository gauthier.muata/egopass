/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ARCHIVE_OLD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ArchiveOld.findAll", query = "SELECT a FROM ArchiveOld a"),
    @NamedQuery(name = "ArchiveOld.findById", query = "SELECT a FROM ArchiveOld a WHERE a.id = :id"),
    @NamedQuery(name = "ArchiveOld.findByDossier", query = "SELECT a FROM ArchiveOld a WHERE a.dossier = :dossier"),
    @NamedQuery(name = "ArchiveOld.findByIntitule", query = "SELECT a FROM ArchiveOld a WHERE a.intitule = :intitule"),
    @NamedQuery(name = "ArchiveOld.findByObservation", query = "SELECT a FROM ArchiveOld a WHERE a.observation = :observation"),
    @NamedQuery(name = "ArchiveOld.findByAgentCreat", query = "SELECT a FROM ArchiveOld a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "ArchiveOld.findByDateCreat", query = "SELECT a FROM ArchiveOld a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "ArchiveOld.findByAgentMaj", query = "SELECT a FROM ArchiveOld a WHERE a.agentMaj = :agentMaj"),
    @NamedQuery(name = "ArchiveOld.findByDateMaj", query = "SELECT a FROM ArchiveOld a WHERE a.dateMaj = :dateMaj"),
    @NamedQuery(name = "ArchiveOld.findByEtat", query = "SELECT a FROM ArchiveOld a WHERE a.etat = :etat"),
    @NamedQuery(name = "ArchiveOld.findByTypeDoc", query = "SELECT a FROM ArchiveOld a WHERE a.typeDoc = :typeDoc"),
    @NamedQuery(name = "ArchiveOld.findByReference", query = "SELECT a FROM ArchiveOld a WHERE a.reference = :reference"),
    @NamedQuery(name = "ArchiveOld.findByTypeDocument", query = "SELECT a FROM ArchiveOld a WHERE a.typeDocument = :typeDocument"),
    @NamedQuery(name = "ArchiveOld.findByDateProduction", query = "SELECT a FROM ArchiveOld a WHERE a.dateProduction = :dateProduction"),
    @NamedQuery(name = "ArchiveOld.findByApplication", query = "SELECT a FROM ArchiveOld a WHERE a.application = :application"),
    @NamedQuery(name = "ArchiveOld.findByRefDocument", query = "SELECT a FROM ArchiveOld a WHERE a.refDocument = :refDocument"),
    @NamedQuery(name = "ArchiveOld.findByTypeFormat", query = "SELECT a FROM ArchiveOld a WHERE a.typeFormat = :typeFormat"),
    @NamedQuery(name = "ArchiveOld.findByEstSynchronise", query = "SELECT a FROM ArchiveOld a WHERE a.estSynchronise = :estSynchronise")})
public class ArchiveOld implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 20)
    @Column(name = "DOSSIER")
    private String dossier;
    @Size(max = 75)
    @Column(name = "INTITULE")
    private String intitule;
    @Lob
    @Column(name = "DOCUMENT")
    private byte[] document;
    @Size(max = 255)
    @Column(name = "OBSERVATION")
    private String observation;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Column(name = "DATE_MAJ")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;
    @Column(name = "ETAT")
    private Short etat;
    @Column(name = "TYPE_DOC")
    private Short typeDoc;
    @Size(max = 25)
    @Column(name = "REFERENCE")
    private String reference;
    @Size(max = 25)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    @Column(name = "DATE_PRODUCTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateProduction;
    @Size(max = 25)
    @Column(name = "APPLICATION")
    private String application;
    @Size(max = 25)
    @Column(name = "REF_DOCUMENT")
    private String refDocument;
    @Size(max = 5)
    @Column(name = "TYPE_FORMAT")
    private String typeFormat;
    @Column(name = "EST_SYNCHRONISE")
    private Boolean estSynchronise;

    public ArchiveOld() {
    }

    public ArchiveOld(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDossier() {
        return dossier;
    }

    public void setDossier(String dossier) {
        this.dossier = dossier;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public byte[] getDocument() {
        return document;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    public Short getTypeDoc() {
        return typeDoc;
    }

    public void setTypeDoc(Short typeDoc) {
        this.typeDoc = typeDoc;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public Date getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(Date dateProduction) {
        this.dateProduction = dateProduction;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getRefDocument() {
        return refDocument;
    }

    public void setRefDocument(String refDocument) {
        this.refDocument = refDocument;
    }

    public String getTypeFormat() {
        return typeFormat;
    }

    public void setTypeFormat(String typeFormat) {
        this.typeFormat = typeFormat;
    }

    public Boolean getEstSynchronise() {
        return estSynchronise;
    }

    public void setEstSynchronise(Boolean estSynchronise) {
        this.estSynchronise = estSynchronise;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ArchiveOld)) {
            return false;
        }
        ArchiveOld other = (ArchiveOld) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ArchiveOld[ id=" + id + " ]";
    }
    
}
