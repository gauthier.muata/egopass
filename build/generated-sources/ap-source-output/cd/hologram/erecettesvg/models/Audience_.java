package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Audience;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Audience.class)
public class Audience_ { 

    public static volatile SingularAttribute<Audience, Audience> audience;
    public static volatile SingularAttribute<Audience, String> code;
    public static volatile SingularAttribute<Audience, String> composition;
    public static volatile SingularAttribute<Audience, String> dateAudience;
    public static volatile SingularAttribute<Audience, String> observation;
    public static volatile SingularAttribute<Audience, String> dossier;
    public static volatile SingularAttribute<Audience, Audience> audience1;
    public static volatile SingularAttribute<Audience, Short> etat;

}