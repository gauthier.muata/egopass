<%-- 
    Document   : documentOfficiels
    Created on : 11 mai 2020, 09:30:42
    Author     : WILLY KASHALA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Documents officiels</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row">
                    <div class="col-lg-8">
                        <form class="form-inline" role="form">
                            Filtrer par 
                            <select  class="form-control" id="cmbSearchTypeDocumenetOfficiel">
                                <option value ="0">Intitulé document</option>
                                <option value ="1">Numéro de l'arrêté</option>
                            </select>
                            <div class="input-group" >
                                <input type="text" class="form-control" style="width: 300px" id="inputSearchDocumentOfficiel" 
                                       placeholder="Intitulé du document">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary" id="btnSearchDocumentOfficiel">
                                        <i class="fa fa-search"></i> 
                                        Rechercher</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal">Intitulé du document : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="inputIntituleDocum"/>
                                            </div>
                                            <div class="col-lg-2">
                                                <label style="font-weight: normal">Numéro de l'arrêté : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="inputNumArrete"/>
                                            </div>
                                         <div class="col-lg-2">
                                                <label style="font-weight: normal">Date de l'arrêté : </label>
                                            </div>
                                            <div class="col-lg-4">
                                                <input type="Date" class="form-control" id="inputDateArrete"/>
                                            </div>
                                        </div> 
                                    
                                    <hr/>
                                        <table width="100%" 
                                               class="table table-bordered table-hover" 
                                               id="tableDocumentOfficiel">
                                        </table>
                                        <hr>
                                        <button class="btn btn-success" id="btnEnregistrerDocum" style="float: right">
                                            <i class="fa fa-save"></i>  Enregistrer 
                                        </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>            
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/articleBudgetaire/documentsOfficiels.js"></script>
    </body>
</html>
