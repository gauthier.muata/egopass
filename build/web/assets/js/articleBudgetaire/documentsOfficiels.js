/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmbSearchTypeDocumenetOfficiel;

var tableDocumentOfficiel;

var inputSearchDocumentOfficiel, inputIntituleDocum,
        inputNumArrete, inputDateArrete;

var btnSearchDocumentOfficiel, btnEnregistrerDocum;

var codeResearchDocumentOfficiel, messageEmptyValueDocumentOfficiel,
        messageAvanceeDocumentOfficiel;
var tempDocumentList;

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Documents officiels');

    removeActiveMenu();
    linkSubMenuDocumentOfficial.addClass('active');

    tableDocumentOfficiel = $('#tableDocumentOfficiel');

    cmbSearchTypeDocumenetOfficiel = $('#cmbSearchTypeDocumenetOfficiel');

    inputSearchDocumentOfficiel = $('#inputSearchDocumentOfficiel');
    inputDateArrete = $('#inputDateArrete');
    inputNumArrete = $('#inputNumArrete');
    inputIntituleDocum = $('#inputIntituleDocum');

    btnSearchDocumentOfficiel = $('#btnSearchDocumentOfficiel');
    btnEnregistrerDocum = $('#btnEnregistrerDocum');

    codeResearchDocumentOfficiel = cmbSearchTypeDocumenetOfficiel.val();

    btnSearchDocumentOfficiel.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
         loadDocumentOfficiel();
    });

    cmbSearchTypeDocumenetOfficiel.on('change', function (e) {
        codeResearchDocumentOfficiel = cmbSearchTypeDocumenetOfficiel.val();
        inputSearchDocumentOfficiel.val('');

        if (codeResearchDocumentOfficiel === '0') {
            messageEmptyValueDocumentOfficiel = 'Veuillez saisir l\'intitulé du document officiel.';
            inputSearchDocumentOfficiel.attr('placeholder', 'l\'intitulé du document officiel');
        } else {
            messageEmptyValueDocumentOfficiel = 'Veuillez saisir le numéro d\'arrêté du document.';
            inputSearchDocumentOfficiel.attr('placeholder', 'Numéro d\'arrêté du document');
        }

        printDocumentOfficiel('');
    });

    btnEnregistrerDocum.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputIntituleDocum.val().trim() === '') {
            alertify.alert('Veuillez d\'abord saisir l\'intitulé du document officiel.');
            return;
        }
        
        if (inputNumArrete.val().trim() === '') {
            alertify.alert('Veuillez d\'abord saisir le numéro du document officiel.');
            return;
        }
        if (inputDateArrete.val().trim() === '') {
            alertify.alert('Veuillez d\'abord saisir la date du document officiel.');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce document officiel ?', function () {
            saveDocumentOfficiel();
        });

    });

    loadDocumentOfficiel();

});

function loadDocumentOfficiel() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getDocument',
            'typeSearch': codeResearchDocumentOfficiel,
            'libelle': inputSearchDocumentOfficiel.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var documentList = JSON.parse(JSON.stringify(response));

                tempDocumentList = [];
                for (var i = 0; i < documentList.length; i++) {
                    var document = new Object();
                    document.code = documentList[i].code;
                    document.intituleDocumentOfficiel = documentList[i].intituleDocumentOfficiel;
                    document.numeroArrete = documentList[i].numeroArrete;
                    document.dateArrete = documentList[i].dateArrete;
                    document.etat = documentList[i].etat;

                    tempDocumentList.push(document);
                }

                printDocumentOfficiel(tempDocumentList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDocumentOfficiel(tempDocumentList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:45%" >INTITULE DU DOCUMENT</th>';
    header += '<th style="width:35%">NUMERO DE L\'ARRETE</th>';
    header += '<th style="width:15%" >DATE DE L\'ARRETE</th>';
    header += '<th style="width:5%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code agent </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < tempDocumentList.length; i++) {
        body += '<tr>';
        body += '<td>' + tempDocumentList[i].intituleDocumentOfficiel + '</td>';
        body += '<td>' + tempDocumentList[i].numeroArrete + '</td>';
        body += '<td>' + tempDocumentList[i].dateArrete + '</td>';
        body += '<td style="text-align:center"><center><input onclick="checkUser(\'' + tempDocumentList[i].code + '\')" type="checkbox" id="checkBoxSelectDocument' + tempDocumentList[i].code + '" name="checkBoxSelectDocument' + tempDocumentList[i].code + '"></center></td>';
        body += '<td hidden="true">' + tempDocumentList[i].code + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableDocumentOfficiel.html(tableContent);

    tableDocumentOfficiel.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function saveDocumentOfficiel() {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveDocumentOfficiel',
            'intituleDocumentOfficiel': inputIntituleDocum.val(),
            'numeroArrete': inputNumArrete.val(),
            'dateArrete': inputDateArrete.val()

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du document officiel ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('Le document officiel est enregistré avec succès.');
                    resetFields();

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement du document officiel.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function resetFields() {

    inputIntituleDocum.val('');
    inputNumArrete.val('');
    inputDateArrete.val('');

}

