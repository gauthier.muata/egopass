package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Division;
import cd.hologram.erecettesvg.models.Site;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(UtilisateurDivision.class)
public class UtilisateurDivision_ { 

    public static volatile SingularAttribute<UtilisateurDivision, Division> division;
    public static volatile SingularAttribute<UtilisateurDivision, Site> site;
    public static volatile SingularAttribute<UtilisateurDivision, String> code;
    public static volatile SingularAttribute<UtilisateurDivision, String> fkPersonne;
    public static volatile SingularAttribute<UtilisateurDivision, Short> etat;

}