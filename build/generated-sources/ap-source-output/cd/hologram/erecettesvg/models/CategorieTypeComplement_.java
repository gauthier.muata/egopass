package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.TypeComplement;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(CategorieTypeComplement.class)
public class CategorieTypeComplement_ { 

    public static volatile SingularAttribute<CategorieTypeComplement, String> code;
    public static volatile SingularAttribute<CategorieTypeComplement, String> intitule;
    public static volatile ListAttribute<CategorieTypeComplement, TypeComplement> typeComplementList;

}