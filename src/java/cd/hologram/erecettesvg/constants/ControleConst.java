/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class ControleConst {

    public class ParamName {

        public final static String OPERATION = "operation";
        public final static String SECTEUR_LIST = "secteurList";
        public final static String ARTICLE_LIST = "articleList";
        public final static String USER_ID = "userId";
        public final static String DATE_DEBUT = "dateDebut";
        public final static String DATE_FIN = "dateFin";
        public final static String TYPE_MISSION = "typeMission";
        public final static String OBSERVATION = "observation";
        public final static String OBSERVATION_VALIDATION = "observationValidation";
        public final static String NUMERO_MISSION = "numeroMission";
        public final static String DATE_CREATE = "dateCreate";
        public final static String MISSION_ID = "missionId";
        public final static String CONTROLEUR_ID = "controleurId";
        public final static String PERIODE_MISSION = "periodeMission";
        public final static String LIBELLE_EQUIPE = "libelleEquipe";
        public final static String EQUIPE_ID = "equipeId";
        public final static String DETAIL_MISSION_LIST = "detailMissionList";
        public final static String DETAIL_EQUIPE_MISSION_LIST = "detailEquipeMissionList";
        public final static String DETAIL_PV_MISSION_LIST = "detailPvMissionList";
        public final static String DETAIL_DECLARATION_MISSION_LIST = "detailDeclarationMissionList";
        public final static String PERIODE_DECLATION_EXIST = "periodeDeclarationExist";
        public final static String DETAILS_MISSION_EXISTS = "detailMissionExist";
        public final static String DETAILS_PV_EXISTS = "detailPvExist";
        public final static String PV_EXIST = "pvExist";
        public final static String EQUIPE_EXIST = "equipeExist";
        public final static String ARCHIVE_EXIST = "archiveExist";
        public final static String PV_HTML = "pvHtml";
        public final static String PV_ID = "pvId";
        public final static String AGENT_EQUIPE = "agentEquipe";
        public final static String IS_CHEF_EQUIPE = "isChefEquipe";
        public final static String STATE_EQUIPE = "stateEquipe";
        public final static String LIBELLE_FONCTION = "libelleFonction";
        public final static String PERIODE_DECLARATION_ID = "periodeDeclarationId";
        public final static String STATE_MISSION = "stateMission";
        public final static String PERIODE_DECLARATION_LIST = "periodeDeclarationList";
        public final static String DETAILS_NOT_EXIST = "detailsNotExist";
        public final static String VALUE_UPDATE_MISSION = "valueUpdateMission";
        public final static String EQUIPE_MISSION_LIST = "equipeMissionList";
        public final static String EQUIPE_MISSION_EXIST = "equipeMissionExist";
        public final static String FK_EQUIPE_MISSION = "fkEquipeMission";
        public final static String DATE_AFFECTATION = "dateAffectation";
        public final static String DEFAILLANT_LIST = "defaillantList";
        public final static String DEFAILLANT_LIST_EXIST = "defaillantListExist";
        public final static String EQUIPE_MISSION_ID = "equipeMissionId";
        public final static String NUMERO_PV = "numeroPv";
        public final static String PV_EXISTS = "pvExist";
        public final static String FK_PERSONNE = "fkPersonne";
        public final static String STATE_PV = "statePv";
        public final static String DATE_CREAT_PV = "dateCreatPV";
        public final static String AGENT_VALIDATE = "agentValidatePv";
        public final static String NC_PERIODE_DECLARATION = "ncPeriodeDeclaration";
        public final static String HAS_TAXATION_OFFICE = "hasTaxationOffice";
        public final static String HAS_ORDONNANCEMENT_OFFICE = "hasOrdonnancementOffice";
        public final static String DETAIL_PV_LIST = "detailPvList";
        public final static String VALUE_UPDATE_PV = "valueUpdatePv";
        public final static String NC_EXIST_TO_PERIODE_DECLARATION = "ncExistToPeriodeDeclaration";
        public final static String ID_PV = "idPv";
        public final static String OBSERVATION_PV = "observationPv";
        public final static String AGENT_NAME = "agentName";
        public final static String AGENT_CODE = "agentCode";
        public final static String AGENT_MATRICULE = "agentMatricule";
        public final static String TYPE_SEARCH = "typeSearch";
        public final static String PV_DOCUMENT_LIST = "pvDocumentList";
        public final static String TELEPHONE_ASSUJETTI = "telephone";
        public final static String ASSUJETTI_NIF = "assujettiNif";
        public final static String MEMBER_TEAM_EXIST = "memberTeamExist";
        public final static String MEMBER_TEAM_LIST = "memberTeamList";
        public final static String PV_DOCUMENT = "pvDocument";
        public final static String OBSERVATION_DOC = "observation";
        public final static String PV_ARCHIVE_ID = "pvArchiveId";
        public final static String PV_ARCHIVE_FORMAT = "pvArchiveFormat";
        public final static String PV_ARCHIVE_TYPE_DOCUMENT = "pvArchiveTypeDocument";
        public final static String PV_ARCHIVE_REFERENCE = "pvArchiveRefDocument";
        public final static String PV_ARCHIVE_BASE_64 = "pvArchiveBase64";
        public final static String DECLARATION_DOCUMENT = "declarationDocument";
        public final static String LIBELLE_DOCUMENT = "libelleDocument";
        
        public final static String NOTE_CALCUL = "noteCalcul";
        public final static String BUDGECT_ARTICLE_NAME = "budgectArticleName";
        public final static String BUDGECT_ARTICLE_CODE = "budgectArticleCode";
        public final static String EXERCICE_FISCALE = "exerciceFiscale";
        public final static String SECTEUR = "secteur";
        public final static String ALL_ASSUJETTI_AFFECTED = "allAssujettiAffected";
    }

    public class Operation {

        public final static String LOAD_LIST_BUDGET_ARTICLE = "loadListBudgetArticle";
        public final static String LOAD_DEFAILLANT_DECLARATION = "loadDefaillantDeclaration";
        public final static String LOAD_MISSION = "loadMission";
        public final static String LOAD_PV = "loadPv";
        public final static String CREATE_MISSION = "1501";
        public final static String UPDATE_MISSION = "updateMission";
        public final static String VALIDATE_MISSION = "1502";
        public final static String REJET_MISSION = "1503";
        public final static String CREATION_EQUIPE = "1506";
        public final static String AFFECTER_DEFAILLANT_EQUIPE = "affecterDefaillantToEquipe";
        public final static String GENERATE_PV = "generatePv";
        public final static String PRINT_ORDRE_MISSION = "printOrdreMission";
        public final static String CREATION_PV = "1504";
        public final static String VALIDATE_PV = "1507";
        public final static String LOAD_CONTROLEUR = "loadControleur";
        public final static String LOAD_DEFAILLANT_MISSION = "loadDefaillantMission";
        public final static String REJET_PV = "1508";
        public final static String PRINT_PV_ARCHIVE = "printPvArchive";
        public final static String PERIODE_DECLARATION_ASSUJETTI = "periodeDeclationAssujetti";
    }

    public class ParamQuery {

        public final static String MISSION_ID = "MISSION_ID";
        public final static String AGENT_CREAT = "AGENT_CREAT";
        public final static String DATE_DEBUT = "DATE_DEBUT";
        public final static String DATE_CREAT = "DATE_CREAT";
        public final static String NUMERO_MISSION = "NUMERO_MISSION";
        public final static String DATE_FIN = "DATE_FIN";
        public final static String TYPE_MISSION = "TYPE_MISSION";
        public final static String OBSERVATION = "OBSERVATION";
        public final static String MAC_ADRESSE = "MAC_ADRESSE";
        public final static String IP_ADRESSE = "IP_ADRESSE";
        public final static String HOST_NAME = "HOST_NAME";
        public final static String CONTEXT_BROWSER = "CONTEXT_BROWSER";
        public final static String FK_PERIODE_DECLARATION = "FK_PERIODE_DECLARATION";
        public final static String EQUIPE_MISSION_ID = "EQUIPE_MISSION_ID";
        public final static String LIBELLE_EQUIPE = "LIBELLE_EQUIPE";
        public final static String FK_MISSION = "FK_MISSION";
        public final static String FK_PERSONNE = "FK_PERSONNE";
        public final static String NUMERO_PV = "NUMERO_PV";
        public final static String PV_ID = "PV_ID";
    }

}
