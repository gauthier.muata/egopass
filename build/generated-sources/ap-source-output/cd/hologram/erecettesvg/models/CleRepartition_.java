package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(CleRepartition.class)
public class CleRepartition_ { 

    public static volatile SingularAttribute<CleRepartition, Integer> code;
    public static volatile SingularAttribute<CleRepartition, ArticleBudgetaire> fkArticleBudgetaire;
    public static volatile SingularAttribute<CleRepartition, BigDecimal> taux;
    public static volatile SingularAttribute<CleRepartition, String> entiteBeneficiaire;
    public static volatile SingularAttribute<CleRepartition, Boolean> isTresorPart;
    public static volatile SingularAttribute<CleRepartition, Boolean> etat;

}