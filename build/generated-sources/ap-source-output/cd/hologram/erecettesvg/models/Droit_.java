package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Module;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Droit.class)
public class Droit_ { 

    public static volatile SingularAttribute<Droit, String> agentMaj;
    public static volatile SingularAttribute<Droit, Module> fkModule;
    public static volatile SingularAttribute<Droit, String> code;
    public static volatile SingularAttribute<Droit, Date> dateCreat;
    public static volatile SingularAttribute<Droit, String> agentCreat;
    public static volatile SingularAttribute<Droit, String> intitule;
    public static volatile SingularAttribute<Droit, Short> etat;
    public static volatile SingularAttribute<Droit, Date> dateMaj;

}