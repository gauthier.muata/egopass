package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Retribution;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(CategorieRepartition.class)
public class CategorieRepartition_ { 

    public static volatile SingularAttribute<CategorieRepartition, String> code;
    public static volatile ListAttribute<CategorieRepartition, Retribution> retributionList;
    public static volatile SingularAttribute<CategorieRepartition, String> intitule;

}