/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.util.Tools;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author gauthier.muata
 */
public class AccuserReceptionBusiness {

    private static final Dao myDao = new Dao();
    static HashMap<String, Object[]> bulkUpdateAC;

    public static boolean AccuserReception(int nbrJourEcheance, HttpServletRequest request) throws Exception {

        bulkUpdateAC = new HashMap<>();
        int counter = 0;
        boolean result = false;
        String query = null;
        Object[] valueObject = null;
        String idUser = request.getParameter(GeneralConst.ID_USER);
        String dateAccuserReception = request.getParameter(GeneralConst.AccuserReception.DATE_ACCUSER_RECEPTION);
        String idDocument = request.getParameter(GeneralConst.AccuserReception.ID_DOCUMENT);
        String typeDocument = request.getParameter(GeneralConst.AccuserReception.TYPE_DOCUMENT);
        String operation = request.getParameter(GeneralConst.OPERATION);
        String archives = request.getParameter(GeneralConst.ParamName.ARCHIVES);
        String tableName = GeneralConst.EMPTY_STRING;
        String observation = request.getParameter(GeneralConst.ParamName.OBSERVATION);
        String fkDocument = request.getParameter(GeneralConst.ParamName.FK_DOCUMENT);

        Date dateAccuser = Tools.formatStringFullToDate(dateAccuserReception);

        try {

            Date dateEcheance = Tools.getEcheanceDate(dateAccuser, Integer.valueOf(nbrJourEcheance));

            List<String> documentList = new ArrayList<>();
            JSONArray jsonDocumentArray;

            if (archives != null && !archives.isEmpty()) {

                jsonDocumentArray = new JSONArray(archives);

                for (int i = 0; i < jsonDocumentArray.length(); i++) {
                    JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                    documentList.add(jsonObject.getString(GeneralConst.ParamName.PV_DOCUMENT));
                }
            }

            switch (typeDocument) {

                case DocumentConst.DocumentCode.NP:

                    tableName = GeneralConst.NameTable.T_NOTE_PERCEPTION;
                    query = SQLQueryGeneral.UPDATE_NOTE_PERCEPTION_DATE_ECHEANCE;
                    valueObject = new Object[]{
                        dateAccuser,
                        dateEcheance,
                        idDocument};
                    break;

                case DocumentConst.DocumentCode.BP:

                    tableName = GeneralConst.NameTable.T_BON_A_PAYER;
                    query = SQLQueryGeneral.UPDATE_BP_DATE_ECHEANCE;
                    valueObject = new Object[]{
                        dateEcheance,
                        idDocument};
                    break;

                case DocumentConst.DocumentCode.MEP_2:
                case DocumentConst.DocumentCode.MED_2:
                case DocumentConst.DocumentCode.INVITATION_A_PAYER:
                case "INVITATION_PAIEMENT":
                case "INVITATION_SERVICE":
                case "RELANCE":

                    tableName = GeneralConst.NameTable.T_MED;
                    query = SQLQueryGeneral.UPDATE_MED_DATE_ECHEANCE;
                    valueObject = new Object[]{
                        dateAccuser,
                        dateEcheance,
                        idDocument};
                    break;

                case DocumentConst.DocumentCode.AMR1:
                case DocumentConst.DocumentCode.AMR2:
                case DocumentConst.DocumentCode.AMR:

                    tableName = GeneralConst.NameTable.T_AMR;

                    query = SQLQueryGeneral.UPDATE_AMR_DATE_ECHEANCE;
                    valueObject = new Object[]{
                        dateAccuser,
                        dateEcheance,
                        idDocument};

                    break;
                case DocumentConst.DocumentCode.CTR:

                    tableName = GeneralConst.NameTable.T_CONTRAINTE;

                    query = SQLQueryGeneral.UPDATE_CONTRAINTE_DATE_ECHEANCE;
                    valueObject = new Object[]{
                        dateAccuser,
                        dateEcheance,
                        idDocument};
                    break;

                case DocumentConst.DocumentCode.CMDT:

                    tableName = GeneralConst.NameTable.T_COMMANDEMENT;

                    query = SQLQueryGeneral.UPDATE_COMMANDEMENT_DATE_ECHEANCE;
                    valueObject = new Object[]{
                        dateAccuser,
                        dateEcheance,
                        idDocument};
                    break;

            }

            counter++;
            bulkUpdateAC.put(counter + query,
                    valueObject);

            if (documentList.size() > 0) {
                for (String docu : documentList) {
                    counter++;
                    bulkUpdateAC.put(counter + SQLQueryNotePerception.ARCHIVE_ACCUSER_RECEPTION_V2,
                            new Object[]{
                                idDocument,
                                typeDocument,
                                docu,
                                fkDocument,
                                observation
                            });
                }
            }

            result = executeQueryBulkAccuserReception(bulkUpdateAC);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    public static boolean executeQueryBulkAccuserReception(HashMap<String, Object[]> params) {
        return myDao.getDaoImpl().executeBulkQuery(params);
    }
}
