/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var modalValiderApurementCompt;
var btnRadioAprouver;
var textaeraObservation;
var btnValiderApurementCompt;
var formRadioButton;

$(function () {

    mainNavigationLabel.text('APUREMENT');
    secondNavigationLabel.text('Registre des apurements comptable');
    modalValiderApurementCompt = $('#modalValiderApurementCompt');
    btnRadioAprouver = $('#btnRadioAprouver');
    textaeraObservation = $('#textaeraObservation');
    btnValiderApurementCompt = $('#btnValiderApurementCompt');
    formRadioButton = $('#formRadioButton');

    removeActiveMenu();
    linkMenuApurement.addClass('active');

    btnSimpleSearch.click(function (e) {
        e.preventDefault();
        if (inputResearchValue.val() === "" || inputResearchValue.val().length < SEARCH_MIN_TEXT) {
              showEmptySearchMessage();
        } else {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            jourrnalSearching(2, 1);
        }
    });
    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        jourrnalSearching(2, 2);
    });

    loadJournalTable('', 2);
    
    btnAdvencedSearch.trigger('click');
});
function ComptableApurementValidation(codeJournal) {

    modalValiderApurementCompt.modal('show');

    var etatApurement;

    btnValiderApurementCompt.click(function (e) {
        e.preventDefault();

        var selectState = $('input[name=radioValider]:checked').val();

        if (selectState == undefined) {
            alertify.alert('Veuillez sélectionner un avis');
            return;
        }

        if (selectState === "1") {
            etatApurement = 1;
        } else {
            etatApurement = 4;
        }

        if (etatApurement == 4 && textaeraObservation.val() === "") {
            alertify.alert('Veuillez d\'abord fournir l\'observation avant de rejéter');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir d\'apurer ce paiement ?', function () {

            modalValiderApurementCompt.modal('hide');

            $.ajax({
                type: 'POST',
                url: 'paiement_servlet',
                dataType: 'text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: false,
                data: {
                    'codeJournal': codeJournal,
                    'etatApurement': etatApurement,
                    'observationApurement': textaeraObservation.val(),
                    'operation': 'validationApurementCompt'
                },
                beforeSend: function () {

                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Apurement en cours...</h5>'});
                },
                success: function (response)
                {
                    $.unblockUI();
                    if (response === '-1') {
                        showResponseError();
                        return;
                    } else if (response === '0') {
                        showResponseError();
                        return;
                    } else if (response === '1') {
                        setTimeout(function () {
                            $.unblockUI();
                            alertify.alert('L\'apurement comptable a été effectué avec succès.');
                            jourrnalSearching(2, savedOperation);
                        }
                        , 1);
                    } else {
                        showResponseError();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.unblockUI();
                    showResponseError();
                }

            });
        });
    });
}

