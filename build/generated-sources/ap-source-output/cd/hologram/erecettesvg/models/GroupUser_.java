package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.AgentGu;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(GroupUser.class)
public class GroupUser_ { 

    public static volatile SingularAttribute<GroupUser, String> agentMaj;
    public static volatile SingularAttribute<GroupUser, Date> dateCreat;
    public static volatile ListAttribute<GroupUser, AgentGu> agentGuList;
    public static volatile SingularAttribute<GroupUser, String> description;
    public static volatile SingularAttribute<GroupUser, Integer> id;
    public static volatile SingularAttribute<GroupUser, String> agentCreat;
    public static volatile SingularAttribute<GroupUser, String> intitule;
    public static volatile SingularAttribute<GroupUser, Short> etat;
    public static volatile SingularAttribute<GroupUser, Date> dateMaj;

}