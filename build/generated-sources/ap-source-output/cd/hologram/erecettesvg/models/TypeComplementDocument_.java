package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TypeComplementDocument.class)
public class TypeComplementDocument_ { 

    public static volatile SingularAttribute<TypeComplementDocument, String> code;
    public static volatile SingularAttribute<TypeComplementDocument, String> ordre;
    public static volatile SingularAttribute<TypeComplementDocument, Boolean> obligatoire;
    public static volatile SingularAttribute<TypeComplementDocument, String> typeDocument;
    public static volatile SingularAttribute<TypeComplementDocument, String> typeComplement;
    public static volatile SingularAttribute<TypeComplementDocument, Boolean> etat;

}