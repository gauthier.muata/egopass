package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TauxPenalite.class)
public class TauxPenalite_ { 

    public static volatile SingularAttribute<TauxPenalite, String> agentMaj;
    public static volatile SingularAttribute<TauxPenalite, String> penalite;
    public static volatile SingularAttribute<TauxPenalite, String> code;
    public static volatile SingularAttribute<TauxPenalite, Boolean> palier;
    public static volatile SingularAttribute<TauxPenalite, Date> dateCreat;
    public static volatile SingularAttribute<TauxPenalite, String> natureArticleBudgetaire;
    public static volatile SingularAttribute<TauxPenalite, String> typePenalite;
    public static volatile SingularAttribute<TauxPenalite, String> agentCreat;
    public static volatile SingularAttribute<TauxPenalite, Date> dateMaj;
    public static volatile SingularAttribute<TauxPenalite, Date> echeance;

}