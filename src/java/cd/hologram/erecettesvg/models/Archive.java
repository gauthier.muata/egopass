/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ARCHIVE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Archive.findAll", query = "SELECT a FROM Archive a"),
    @NamedQuery(name = "Archive.findById", query = "SELECT a FROM Archive a WHERE a.id = :id"),
    @NamedQuery(name = "Archive.findByTypeFormat", query = "SELECT a FROM Archive a WHERE a.typeFormat = :typeFormat"),
    @NamedQuery(name = "Archive.findByTypeDocument", query = "SELECT a FROM Archive a WHERE a.typeDocument = :typeDocument"),
    @NamedQuery(name = "Archive.findByDateProduction", query = "SELECT a FROM Archive a WHERE a.dateProduction = :dateProduction"),
    @NamedQuery(name = "Archive.findByRefDocument", query = "SELECT a FROM Archive a WHERE a.refDocument = :refDocument"),
    @NamedQuery(name = "Archive.findByEstSynchronise", query = "SELECT a FROM Archive a WHERE a.estSynchronise = :estSynchronise"),
    @NamedQuery(name = "Archive.findByDossier", query = "SELECT a FROM Archive a WHERE a.dossier = :dossier")})
public class Archive implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Lob
    @Column(name = "DOCUMENT")
    private byte[] document;
    @Size(max = 5)
    @Column(name = "TYPE_FORMAT")
    private String typeFormat;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "APPLICATION")
    private String application;
    @Size(max = 50)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    @Column(name = "DATE_PRODUCTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateProduction;
    @Size(max = 50)
    @Column(name = "REF_DOCUMENT")
    private String refDocument;
    
    @Column(name = "DOCUMENT_STRING")
    private String documentString;
    
    @Column(name = "EST_SYNCHRONISE")
    private Boolean estSynchronise;
    @Size(max = 50)
    @Column(name = "DOSSIER")
    private String dossier;

    public Archive() {
    }

    public Archive(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocumentString() {
        return documentString;
    }

    public void setDocumentString(String documentString) {
        this.documentString = documentString;
    }
    
    

    public byte[] getDocument() {
        return document;
    }

    public void setDocument(byte[] document) {
        this.document = document;
    }

    public String getTypeFormat() {
        return typeFormat;
    }

    public void setTypeFormat(String typeFormat) {
        this.typeFormat = typeFormat;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public Date getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(Date dateProduction) {
        this.dateProduction = dateProduction;
    }

    public String getRefDocument() {
        return refDocument;
    }

    public void setRefDocument(String refDocument) {
        this.refDocument = refDocument;
    }

    public Boolean getEstSynchronise() {
        return estSynchronise;
    }

    public void setEstSynchronise(Boolean estSynchronise) {
        this.estSynchronise = estSynchronise;
    }

    public String getDossier() {
        return dossier;
    }

    public void setDossier(String dossier) {
        this.dossier = dossier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Archive)) {
            return false;
        }
        Archive other = (Archive) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Archive[ id=" + id + " ]";
    }
    
}
