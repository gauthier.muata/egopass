package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(RetraitDeclaration.class)
public class RetraitDeclaration_ { 

    public static volatile SingularAttribute<RetraitDeclaration, Integer> retraitDeclarationMere;
    public static volatile SingularAttribute<RetraitDeclaration, String> fkAssujetti;
    public static volatile SingularAttribute<RetraitDeclaration, BigDecimal> penaliteRemise;
    public static volatile SingularAttribute<RetraitDeclaration, String> codeDeclaration;
    public static volatile SingularAttribute<RetraitDeclaration, BigDecimal> montant;
    public static volatile SingularAttribute<RetraitDeclaration, Date> dateCreate;
    public static volatile SingularAttribute<RetraitDeclaration, Integer> etat;
    public static volatile SingularAttribute<RetraitDeclaration, String> fkCompteBancaire;
    public static volatile SingularAttribute<RetraitDeclaration, Integer> fkRetraitPere;
    public static volatile SingularAttribute<RetraitDeclaration, String> fkPeriode;
    public static volatile SingularAttribute<RetraitDeclaration, BigDecimal> valeurBase;
    public static volatile SingularAttribute<RetraitDeclaration, Integer> id;
    public static volatile SingularAttribute<RetraitDeclaration, Date> dateEcheancePaiement;
    public static volatile SingularAttribute<RetraitDeclaration, String> newId;
    public static volatile SingularAttribute<RetraitDeclaration, String> observationRemise;
    public static volatile SingularAttribute<RetraitDeclaration, String> fkAb;
    public static volatile SingularAttribute<RetraitDeclaration, String> fkBanque;
    public static volatile SingularAttribute<RetraitDeclaration, Integer> tauxRemise;
    public static volatile SingularAttribute<RetraitDeclaration, String> devise;
    public static volatile SingularAttribute<RetraitDeclaration, String> fkAgentCreate;
    public static volatile SingularAttribute<RetraitDeclaration, Integer> estPenalise;
    public static volatile SingularAttribute<RetraitDeclaration, String> observationControle;
    public static volatile SingularAttribute<RetraitDeclaration, String> avis;
    public static volatile SingularAttribute<RetraitDeclaration, String> fkSite;
    public static volatile SingularAttribute<RetraitDeclaration, String> requerant;
    public static volatile SingularAttribute<RetraitDeclaration, Integer> estTaxationOffice;

}