/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var fpcList = [];
var responsibleObject = new Object();

var btnSimpleSearch;
var selectTypeSearch;
var selectTypeSearchID;
var valueTypeSearch;

var modalAccuserReception;

var assujettiModal;
var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var dateStartPickerAmr;
var dateEndPickerAmr;
var btnAdvencedSearchAmr;

var lblService, lblTypeContrainte, lblStateAmr, lblDateDebut, lblDateFin;
var selectServiceID;

var btnShowAdvancedSerachModal;
var tableFiche;

var codeContrainteCurrent;
var contrainteDocumentPrint = '';
var commandementDocumentPrint = '';

var documentListModal, selectDocument, btnPrintDocument;

var documentHtmlPrint;

var labelType, lblStateAmr, selectStateAmr, lblTypeAmr;
var isAdvance;

var modalDetailFichePriseCharge;
var btnVal, btnAnn, inputObservation, tableDetailFpc, lblPenaliteDu, lblTotalDu;
var lblAdress, lblNameResponsible, lblCodeResponsible, lblNumeroFpc;

var idFpc, typeFpc;
var typeMedSelected;
//var btnClosedTaxation, btnRetaxerNc;

$(function () {

    mainNavigationLabel.text('POURSUITES');
    secondNavigationLabel.text('Registre des fiches de prise en charge');
    removeActiveMenu();
    linkMenuContentieux.addClass('active');


    btnSimpleSearch = $('#btnSimpleSearch');
    selectTypeSearch = $('#selectTypeSearch');
    valueTypeSearch = $('#valueTypeSearch');

    modalAccuserReception = $('#modalAccuserReception');
    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');
    assujettiModal = $('#assujettiModal');

    selectService = $('#selectService');
    selectTypeAmr = $('#selectTypeAmr');
    selectStateAmr = $('#selectStateAmr');
    inputDateStartAmr = $('#inputDateStartAmr');
    inputdateEndAmr = $('#inputdateEndAmr');

    dateStartPickerAmr = $('#dateStartPickerAmr');
    dateEndPickerAmr = $('#dateEndPickerAmr');
    btnAdvencedSearchAmr = $('#btnAdvencedSearchAmr');

    //btnVal = $('#btnClosedTaxation');
    //btnAnn = $('#btnRetaxerNc');

    modalRechercheAvanceeAmr = $('#modalRechercheAvanceeAmr');
    lblTypeAmr = $('#lblTypeAmr');

    isAdvance = $('#isAdvance');
    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');

    lblService = $('#lblService');
    lblTypeContrainte = $('#lblTypeContrainte');
    lblStateAmr = $('#lblStateAmr');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    labelType = $('#labelType');
    lblStateAmr = $('#lblStateAmr');

    tableFiche = $('#tableFiche');

    documentListModal = $('#documentListModal');
    selectDocument = $('#selectDocument');
    btnPrintDocument = $('#btnPrintDocument');


    modalDetailFichePriseCharge = $('#modalDetailFichePriseCharge');
    //btnVal = $('#btnVal');
    //btnAnn = $('#btnAnn');
    inputObservation = $('#inputObservation');
    lblPenaliteDu = $('#lblPenaliteDu');
    lblTotalDu = $('#lblTotalDu');
    lblAdress = $('#lblAdress');
    lblNameResponsible = $('#lblNameResponsible');
    lblCodeResponsible = $('#lblCodeResponsible');
    lblNumeroFpc = $('#lblNumeroFpc');
    tableDetailFpc = $('#tableDetailFpc');

    selectService.on('change', function (e) {
        selectServiceID = selectService.val();
    });

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    dateStartPickerAmr.datepicker("setDate", new Date());
    dateEndPickerAmr.datepicker("setDate", new Date());

    selectTypeSearch.on('change', function (e) {

        selectTypeSearchID = selectTypeSearch.val();
        valueTypeSearch.val(empty);

        switch (selectTypeSearchID) {
            case '1':
                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de la fiche');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;
            case '2':
                valueTypeSearch.attr('readonly', true);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
                break;

        }


    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeAmr.modal('show');
    });

    $('#btnValidateFpc').on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir valider cette fiche de prise en charge ?', function () {

            switch (typeMedSelected) {
                case 'DECLARATION':
                    traiterFichePriseChargeV2(2, idFpc, typeFpc);
                    break;
                default:
                    traiterFichePriseCharge(2, idFpc, typeFpc);
                    break;
            }

        });

    });

    $('#btnAnnulerFpc').on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir annuler cette fiche de prise en charge ?', function () {
            traiterFichePriseCharge(0, idFpc, typeFpc);
        });

    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '0';
        switch (selectTypeSearchID) {

            case '1':
                if (valueTypeSearch.val() === empty) {
                    alertify.alert('Veuillez saisir le numéro de la fiche');
                    return;
                } else {
                    loadFichePriseCharge(isAdvanced);
                }

                break;
            case '2':
                assujettiModal.modal('show');
                break;
            default:
                loadFichePriseCharge(isAdvanced);
                break;
        }

    });

    btnAdvencedSearchAmr.on('click', function (e) {
        e.preventDefault();

        isAdvanced = '1';
        selectTypeSearch.val('1');
        valueTypeSearch.val(empty);
        valueTypeSearch.attr('readonly', false);
        valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de la fiche');
        valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');
        loadFichePriseCharge(isAdvanced);
    });

    selectTypeAmr.attr('style', 'display:none');
    lblTypeContrainte.attr('style', 'display:none');

    lblStateAmr.text('Type :');
    lblTypeAmr.attr('style', 'display:none');
    var dataselectStateAmr = '';

    selectStateAmr.html(empty);

    dataselectStateAmr += '<option value="0">Tous</option>';
    dataselectStateAmr += '<option value="1">En attente de validation</option>';
    dataselectStateAmr += '<option value="2">Valider</option>';
    dataselectStateAmr += '<option value="3">Annuler</option>';

    selectStateAmr.html(dataselectStateAmr);

    selectTypeSearchID = selectTypeSearch.val();
    printFichePriseCharge(empty);

});

function loadFichePriseCharge(isAdvanced) {

    switch (isAdvanced) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            labelType.text($('#selectStateAmr option:selected').text().toUpperCase());
            labelType.attr('title', $('#selectStateAmr option:selected').text().toUpperCase());

            if (selectService.val() === '*') {
                lblService.text('TOUS');
                lblService.attr('title', 'TOUS');
            } else {
                lblService.text($('#selectService option:selected').text().toUpperCase());
                lblService.attr('title', $('#selectService option:selected').text().toUpperCase());
            }

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: block');
            break;
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectServiceID,
            'codeTypeFpc': selectStateAmr.val(),
            'dateDebut': inputDateStartAmr.val(),
            'dateFin': inputdateEndAmr.val(),
            'valueSearch': selectTypeSearchID === '2' ? responsibleObject.codeResponsible : valueTypeSearch.val(),
            'advancedSearch': isAdvanced,
            'typeSearch': selectTypeSearchID,
            'operation': 'loadFichePriseCharge'
        },
        beforeSend: function () {
            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeAmr.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '2':
                                printFichePriseCharge(empty);
                                alertify.alert('Cet assujetti : ' + valueTypeSearch.val() + ' n\'a pas de fiche de prise en charge');
                                break;
                            default:
                                printFichePriseCharge(empty);
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'une fiche de prise en charge');
                                break;
                        }
                        break;
                    case '1':
                        printFichePriseCharge(empty);
                        alertify.alert('Aucune fiche de prise en charge ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                fpcList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeAmr.modal('hide');
                }
                printFichePriseCharge(fpcList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeAmr.unblock();
                    break;
            }
            showResponseError();
        }

    });
}


function getSelectedAssujetiData() {

    valueTypeSearch.val(selectAssujettiData.nomComplet);
    valueTypeSearch.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadFichePriseCharge(isAdvanced);

}

function printFichePriseCharge(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:8%">NUMERO</th>';
    tableContent += '<th style="text-align:left;width:10%">DATE CREATION</th>';
    tableContent += '<th style="text-align:left;width:10%">NTD</th>';
    tableContent += '<th style="text-align:left;width:25%">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left;width:20%">FAIT GENERATEUR</th>';
    tableContent += '<th style="text-align:right;width:8%">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left;width:10%">ETAT</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amountDu = formatNumber(result[i].montantTotalDu, result[i].devise);
        var buttonCallModalDetailFpc = '<button class="btn btn-warning" onclick="callModalDetailFpc(\'' + result[i].idFpc + '\',\'' + result[i].etat + '\',\'' + result[i].typeMed + '\')"><i class="fa fa-list"></i></button>';

        var medInfo = empty;

        if (result[i].codeMed !== empty) {
            medInfo = '<hr/><span style="color:red;font-weight:normal">' + '(MED : ' + result[i].codeMed + ')' + '</span>';
        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].idFpc + medInfo + '</td>';
        tableContent += '<td style="text-align:center;width:9%;vertical-align:middle">' + result[i].dateCreateFpc + '</td>';
        tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + result[i].userName + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].articleBudgetaireName + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:normal">' + amountDu + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:normal">' + result[i].etatLibelle + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonCallModalDetailFpc + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableFiche.html(tableContent);
    tableFiche.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

}

function callModalDetailFpc(id, state, typeMed) {

    typeMedSelected = typeMed;

    switch (state) {
        case '0':
        case '2':
            $('#btnValidateFpc').attr('style', 'display: none');
            $('#btnAnnulerFpc').attr('style', 'display: none');

            $('#btnCloseModal').attr('class', 'pull-right');

            break;
        default:
            $('#btnValidateFpc').attr('style', 'display: block');
            $('#btnAnnulerFpc').attr('style', 'display: block');
            $('#btnCloseModal').attr('class', 'pull-left');
            break;
    }

    for (var i = 0; i < fpcList.length; i++) {

        var totalDu = 0;

        if (fpcList[i].idFpc == id) {
            idFpc = id;
            typeFpc = fpcList[i].typeFpc;

            totalDu = parseFloat(fpcList[i].montantDu);

            var devise = fpcList[i].devise;
            var detailFpcList = $.parseJSON(fpcList[i].detailFpcList);
            var penaliteDu = 0;

            var tableContent = '';
            tableContent += '<thead style="background-color:#0085c7;color:white">';
            tableContent += '<tr>';
            tableContent += '<th style="text-align:center;width:10%">EXERCICE</th>';
            tableContent += '<th style="text-align:left;width:15%">PENALITE</th>';
            tableContent += '<th style="text-align:center;width:10%">TAUX</th>';
            tableContent += '<th style="text-align:right;width:10%">MONTANT DÛ</th>';
            tableContent += '<th style="text-align:center;width:10%">MOIS DE RETARD</th>';
            tableContent += '<th style="text-align:right;width:10%">PENALITE DÛ</th>';
            tableContent += '</tr>';
            tableContent += '</thead>';
            tableContent += '<tbody>';

            for (var j = 0; j < detailFpcList.length; j++) {

                penaliteDu += parseFloat(detailFpcList[j].penaliteDu);

                var amountDu = formatNumber(detailFpcList[j].principalDu, devise);
                var amountPenaliteDu = formatNumber(detailFpcList[j].penaliteDu, devise);

                var tauxPenalite = detailFpcList[j].tauxPenalite + '%';

                tableContent += '<tr>';
                tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + detailFpcList[j].periodeDeclarationName + '</td>';
                tableContent += '<td style="text-align:left;width:15%;vertical-align:middle">' + detailFpcList[j].penaliteName + '</td>';
                tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + tauxPenalite + '</td>';
                tableContent += '<td style="text-align:right;width:10%;vertical-align:middle">' + amountDu + '</td>';
                tableContent += '<td style="text-align:center;width:10%;vertical-align:middle">' + detailFpcList[j].nombreMoisRetard + '</td>';
                tableContent += '<td style="text-align:right;width:10%;vertical-align:middle">' + amountPenaliteDu + '</td>';

                tableContent += '</tr>';
                tableContent += '<hr/>';
            }

            tableContent += '</tbody>';
            tableDetailFpc.html(tableContent);
            tableDetailFpc.DataTable({
                language: {
                    processing: "Traitement en cours...",
                    track: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible pour le critère sélectionné",
                    search: "Rechercher par N° document _INPUT_  ",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                info: false,
                destroy: true,
                tracking: false,
                ordering: false,
                searching: false,
                lengthChange: false,
                pageLength: 5,
                lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
                select: {
                    style: 'os',
                    blurable: true
                },
                datalength: 5
            });

            inputObservation.val(fpcList[i].motifPenalite);
            lblAdress.html(fpcList[i].adresseName);
            lblNameResponsible.html(fpcList[i].assujettiName);
            lblCodeResponsible.html(fpcList[i].assujettiCode);
            lblNumeroFpc.html(fpcList[i].idFpc);

            lblTotalDu.html(formatNumber(totalDu, devise));
            lblPenaliteDu.html(formatNumber(penaliteDu, devise));


            /*switch (fpcList[i].etat) {
             case 0:
             case 2:
             btnVal.attr('style', 'display: none');
             btnAnn.attr('style', 'display: none');
             break;
             default:
             btnVal.attr('style', 'display: block');
             btnAnn.attr('style', 'display: block');
             break;
             }*/

            modalDetailFichePriseCharge.modal('show');
        }
    }
}

function printDetailFichePriseCharge(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:8%">NUMERO</th>';
    tableContent += '<th style="text-align:left;width:10%">DATE CREATION</th>';
    tableContent += '<th style="text-align:left;width:25%">ASSUJETTI</th>';
    tableContent += '<th style="text-align:left;width:20%">FAIT GENERATEUR</th>';
    tableContent += '<th style="text-align:right;width:8%">MONTANT DÛ</th>';
    tableContent += '<th style="text-align:left;width:10%">ETAT</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var amountDu = formatNumber(result[i].montantDu, result[i].devise);
        var buttonCallModalDetailFpc = '<button class="btn btn-warning" onclick="callModalDetailFpc(\'' + result[i].idFpc + '\')"><i class="fa fa-list"></i></button>';

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].idFpc + '</td>';
        tableContent += '<td style="text-align:center;width:9%;vertical-align:middle">' + result[i].dateCreateFpc + '</td>';
        tableContent += '<td style="text-align:left;width:25%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].articleBudgetaireName + '</td>';
        tableContent += '<td style="text-align:right;width:8%;vertical-align:middle;font-weight:normal">' + amountDu + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle;font-weight:normal">' + result[i].etatLibelle + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + buttonCallModalDetailFpc + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableFiche.html(tableContent);
    tableFiche.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

}

function traiterFichePriseCharge(value, id, type) {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'value': value,
            'idFpc': id,
            'typeFpc': type,
            'userId': userData.idUser,
            'operation': 'traiterFichePriseCharge'
        },
        beforeSend: function () {

            switch (value) {
                case 2 :
                    modalDetailFichePriseCharge.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation en cours ...</h5>'});
                    break;
                case 0 :
                    modalDetailFichePriseCharge.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Annulation en cours ...</h5>'});
                    break;
            }
        },
        success: function (response)
        {

            setTimeout(function () {
                modalDetailFichePriseCharge.unblock();
                if (response == '-1' || response == '0') {
                    showResponseError();
                    return;
                } else {

                    switch (value) {
                        case 2 :
                            //alertify.alert('La validation de cette prise en charge s\'est effectuée avec succès.');


                            setTimeout(function () {
                                setDocumentContent(response);

                                setTimeout(function () {
                                }, 2000);
                                window.open('visualisation-document', '_blank');
                            }
                            , 1);

                            break;

                        case 0 :
                            alertify.alert('L\'annulation de cette prise en charge s\'est effectuée avec succès.');
                            break;
                    }

                    modalDetailFichePriseCharge.modal('hide');

                    loadFichePriseCharge(isAdvanced);
                }

            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalDetailFichePriseCharge.unblock();
            showResponseError();
        }

    });
}

function traiterFichePriseChargeV2(value, id, type) {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'value': value,
            'idFpc': id,
            'typeFpc': type,
            'userId': userData.idUser,
            'operation': 'traiterFichePriseCharge'
        },
        beforeSend: function () {

            modalDetailFichePriseCharge.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalDetailFichePriseCharge.unblock();

                if (response == '-1' || response == '0') {

                    showResponseError();
                    return;

                } else {

                    var result = JSON.parse(JSON.stringify(response));

                    setTimeout(function () {

                        setDocumentContent(result[0].amr1Html);

                        setTimeout(function () {
                        }, 2000);
                        window.open('visualisation-document', '_blank');
                    }
                    , 1);

                    setTimeout(function () {

                        setDocumentContent(result[0].amr2Html);

                        setTimeout(function () {
                        }, 2000);
                        window.open('visualisation-document', '_blank');
                    }
                    , 1);


                }

                modalDetailFichePriseCharge.modal('hide');

                loadFichePriseCharge(isAdvanced);
            }


            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalDetailFichePriseCharge.unblock();
            showResponseError();
        }

    });
}