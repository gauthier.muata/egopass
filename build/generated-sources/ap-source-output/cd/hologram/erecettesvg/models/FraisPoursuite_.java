package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Amr;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(FraisPoursuite.class)
public class FraisPoursuite_ { 

    public static volatile SingularAttribute<FraisPoursuite, String> contrainte;
    public static volatile SingularAttribute<FraisPoursuite, Amr> amr2;
    public static volatile SingularAttribute<FraisPoursuite, String> code;
    public static volatile SingularAttribute<FraisPoursuite, String> commandement;
    public static volatile SingularAttribute<FraisPoursuite, Date> dateCreat;
    public static volatile SingularAttribute<FraisPoursuite, Amr> amr1;
    public static volatile SingularAttribute<FraisPoursuite, BigDecimal> montant;

}