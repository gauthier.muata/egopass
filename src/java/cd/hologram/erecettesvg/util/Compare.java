/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.util;

import static cd.hologram.erecettesvg.util.ConvertDate.dateToInt;
import java.util.Date;

/**
 *
 * @author WILLY KASHALA
 */
public class Compare {

    public static boolean before(Date debut, Date fin) {

        return compare(debut, fin, -1);

    }

    public static boolean equals(Date debut, Date fin) {

        return compare(debut, fin, 0);

    }
    
     public static boolean after(Date debut, Date fin) {

        return compare(debut, fin, 1);

    }

    private static boolean compare(Date debut, Date fin, int comparator) {
        int nDebut = dateToInt(debut);
        int nFin = dateToInt(fin);
        boolean result;

        switch (comparator) {
            case -1:
                result = (nDebut < nFin);
                break;
            case 0:
                result = (nDebut == nFin);
                break;
            default:
                result = (nDebut > nFin);
        }
        return result;
    }

}
