/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author WILLY
 */
@Entity
@Table(name = "T_REMISE_SOUS_PROVISION_PEAGE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RemiseSousProvisionPeage.findAll", query = "SELECT r FROM RemiseSousProvisionPeage r"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findById", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.id = :id"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findByTaux", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.taux = :taux"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findByType", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.type = :type"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findByFkAxe", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.fkAxe = :fkAxe"),
    @NamedQuery(name = "RemiseSousProvisionPeage.findByEtat", query = "SELECT r FROM RemiseSousProvisionPeage r WHERE r.etat = :etat")})
public class RemiseSousProvisionPeage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Column(name = "TAUX")
    private BigDecimal taux;
    @Size(max = 50)
    @Column(name = "TYPE")
    private String type;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "FK_AXE")
    private Integer fkAxe;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkPersonne;
    @JoinColumn(name = "FK_TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif fkTarif;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getTaux() {
        return taux;
    }

    public void setTaux(BigDecimal taux) {
        this.taux = taux;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Integer getFkAxe() {
        return fkAxe;
    }

    public void setFkAxe(Integer fkAxe) {
        this.fkAxe = fkAxe;
    }

    public Personne getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(Personne fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public Tarif getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(Tarif fkTarif) {
        this.fkTarif = fkTarif;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RemiseSousProvisionPeage other = (RemiseSousProvisionPeage) obj;
        return true;
    }

    @Override
    public String toString() {
        return "RemiseSousProvisionPeage{" + "id=" + id + ", taux=" + taux + ", type=" + type + ", etat=" + etat + ", fkAxe=" + fkAxe + ", fkPersonne=" + fkPersonne + ", fkTarif=" + fkTarif + ", fkSite=" + fkSite + '}';
    } 
    
}
