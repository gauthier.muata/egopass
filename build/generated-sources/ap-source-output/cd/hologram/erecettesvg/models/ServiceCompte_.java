package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Service;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ServiceCompte.class)
public class ServiceCompte_ { 

    public static volatile SingularAttribute<ServiceCompte, String> agentMaj;
    public static volatile SingularAttribute<ServiceCompte, CompteBancaire> compteBancaire;
    public static volatile SingularAttribute<ServiceCompte, Date> dateCreat;
    public static volatile SingularAttribute<ServiceCompte, Service> service;
    public static volatile SingularAttribute<ServiceCompte, String> id;
    public static volatile SingularAttribute<ServiceCompte, String> agentCreat;
    public static volatile SingularAttribute<ServiceCompte, Short> etat;
    public static volatile SingularAttribute<ServiceCompte, Date> dateMaj;

}