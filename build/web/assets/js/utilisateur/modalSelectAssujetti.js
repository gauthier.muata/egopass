/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var codeResearch, messageEmptyValue, messageAvancee, codeGestionnaire;

var modalSelectAssujetti, modalRechercheAvanceeAssujetti, divSelectAssujetti;

var cmbSearchTypeSelect;

var inputSearchAssujettiSelect;

var btnSearchAssujettiSelect, btnCreateAssujettiSelect,
        rechercerAvancesAssujettiSelect, btnAffecterAssujetti;

var tableResultSelectAssujetti;

var tempPersonneList;


$(function () {

    modalSelectAssujetti = $('#modalSelectAssujetti');
    modalRechercheAvanceeAssujetti = $('#modalRechercheAvanceeAssujetti');
    divSelectAssujetti = $('#divSelectAssujetti');

    tableResultSelectAssujetti = $('#tableResultSelectAssujetti');

    btnSearchAssujettiSelect = $('#btnSearchAssujettiSelect');
    btnCreateAssujettiSelect = $('#btnCreateAssujettiSelect');
    rechercerAvancesAssujettiSelect = $('#rechercerAvancesAssujettiSelect');
    btnAffecterAssujetti = $('#btnAffecterAssujetti');

    inputSearchAssujettiSelect = $('#inputSearchAssujettiSelect');

    cmbSearchTypeSelect = $('#cmbSearchTypeSelect');
    codeResearch = cmbSearchTypeSelect.val();

    messageEmptyValue = 'Veuillez d\'abord saisir le nom de l\'assujetti';
    messageAvancee = 'Veuillez d\'abord saisir le critere de recherche.';

    rechercerAvancesAssujettiSelect.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeAssujetti.modal('show');
    });

    btnSearchAssujettiSelect.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadAssujettis();
    });

    cmbSearchTypeSelect.on('change', function (e) {
        codeResearch = cmbSearchTypeSelect.val();
        inputSearchAssujettiSelect.val('');

        if (codeResearch === '0') {
            messageEmptyValue = 'Veuillez saisir le nom de l\'assujetti.';
            inputSearchAssujetti.attr('placeholder', 'Nom de l\'assujetti');
        } else {
            messageEmptyValue = 'Veuillez saisir le numéro d\'identification nationale de l\'assujetti.';
            inputSearchAssujettiSelect.attr('placeholder', 'Numéro d\'identification nationale');
        }

        printPersonnes('');
    });

    btnAffecterAssujetti.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous de vouloir affecter cet assujetti ?', function () {

            var assujettiLists = preparerAffectation();
            codeGestionnaire = getCodeGestionnaire();
            affecterAssujetti(assujettiLists);
            printPersonnes('');

        });

    });

    btnCreateAssujettiSelect.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        window.location = 'identification';
    });

    loadFormeJuridique();
    printPersonnes('');

});


function loadAssujettis() {

    if (inputSearchAssujettiSelect.val().trim() === empty || inputSearchAssujettiSelect.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAssujetti',
            'typeSearch': codeResearch,
            'libelle': inputSearchAssujettiSelect.val()
        },
        beforeSend: function () {

            modalSelectAssujetti.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                modalSelectAssujetti.unblock()();
                showResponseError();
                return;
            }

            setTimeout(function () {

                modalSelectAssujetti.unblock();

                var personneList = JSON.parse(JSON.stringify(response));

                tempPersonneList = [];
                for (var i = 0; i < personneList.length; i++) {
                    var personne = new Object();
                    personne.libelleFormeJuridique = personneList[i].libelleFormeJuridique;
                    personne.nif = personneList[i].nif;
                    personne.nom = personneList[i].nom;
                    personne.postNom = personneList[i].postNom;
                    personne.prenom = personneList[i].prenom;
                    personne.telephone = personneList[i].telephone;
                    personne.email = personneList[i].email;
                    personne.chaine = personneList[i].chaine;
                    personne.codePersonne = personneList[i].codePersonne;
                    personne.nomComplet = personneList[i].nomComplet;
                    tempPersonneList.push(personne);
                }

                printPersonnes(tempPersonneList);

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            modalSelectAssujetti.unblock();;
            showResponseError();
        }

    });
}

function loadFormeJuridique() {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFormeJuridique'
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                var result = $.parseJSON(JSON.stringify(response));

                var dataFormeJuridique = '<option value ="0">-- Sélectionner --</option>';

                for (var i = 0; i < result.length; i++) {

                    dataFormeJuridique += '<option value =' + result[i].codeFormeJuridique + '>' + result[i].libelleFormeJuridique.toUpperCase() + '</option>';
                }

                cmbFormeJuridique.html(dataFormeJuridique);

            }
            , 1000);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            showResponseError();
        }
    });
}

function printPersonnes(tempPersonneList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:10%" > Nif </th>';
    header += '<th style="width:25%"> Assujetti </th>';
    header += '<th style="width:13%"> Catégorie </th>';
    header += '<th style="width:10%"> Téléphone </th>';
    header += '<th hidden="true" > Email </th>';
    header += '<th style="width:30%" > Adresse principale</th>';
    header += '<th style="width:10%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code personne </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < tempPersonneList.length; i++) {
        body += '<tr>';

        body += '<td>' + tempPersonneList[i].nif + '</td>';
        body += '<td>' + tempPersonneList[i].nomComplet + '</td>';
        body += '<td>' + tempPersonneList[i].libelleFormeJuridique + '</td>';
        body += '<td>' + tempPersonneList[i].telephone + '</td>';
        body += '<td hidden="true">' + tempPersonneList[i].email + '</td>';
        body += '<td>' + tempPersonneList[i].chaine + '</td>';
        body += '<td style="text-align:center"><center><input type="checkbox" id="checkBoxSelectAssujetti' + tempPersonneList[i].codePersonne + '" name="checkBoxSelectAssujetti' + tempPersonneList[i].codePersonne + '"></center></td>';
        body += '<td hidden="true">' + tempPersonneList[i].codePersonne + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableResultSelectAssujetti.html(tableContent);

    tableResultSelectAssujetti.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function editPersonne(codePersonne) {
    for (var i = 0; i < tempPersonneList.length; i++) {

        if (tempPersonneList[i].codePersonne === codePersonne) {
            var nomComplet = tempPersonneList[i].nomComplet;
            alertify.confirm('Voulez-vous afficher les détails de ' + nomComplet + ' ?', function () {
                window.location = 'identification?id=' + codePersonne;
            });
            break;
        }
    }
}

function preparerAffectation() {

    assujettiList = [];

    for (var i = 0; i < tempPersonneList.length; i++) {

        var selectAssujetti = '#checkBoxSelectAssujetti' + tempPersonneList[i].codePersonne;

        var select = $(selectAssujetti);

        if (select.is(':checked')) {

            var assujetti = {};

            assujetti.codePersonne = tempPersonneList[i].codePersonne;
            assujetti.nif = tempPersonneList[i].nif;

            assujettiList.push(assujetti);

        }

    }

    assujettiList = JSON.stringify(assujettiList);

    return assujettiList;
}

function affecterAssujetti(assujettiList) {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterAssujetti',
            'codeGestionnaire': codeGestionnaire,
            'assujettiList': assujettiList

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement des assujettis en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'enregistrement des assujettis s\'est effectué avec succès.');
                    loadAssujettisAffecter(codeGestionnaire);
                    modalSelectAssujetti.modal('hide');
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement des assujettis.');
                    modalSelectAssujetti.modal('hide');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}


