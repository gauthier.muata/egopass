package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.DetailFichePriseCharge;
import cd.hologram.erecettesvg.models.Etat;
import cd.hologram.erecettesvg.models.Penalite;
import cd.hologram.erecettesvg.models.Personne;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(FichePriseCharge.class)
public class FichePriseCharge_ { 

    public static volatile ListAttribute<FichePriseCharge, DetailFichePriseCharge> detailFichePriseChargeList;
    public static volatile SingularAttribute<FichePriseCharge, String> code;
    public static volatile ListAttribute<FichePriseCharge, Amr> amrList;
    public static volatile SingularAttribute<FichePriseCharge, BigDecimal> totalPrincipaldu;
    public static volatile SingularAttribute<FichePriseCharge, Date> dateCreat;
    public static volatile SingularAttribute<FichePriseCharge, Personne> personne;
    public static volatile SingularAttribute<FichePriseCharge, String> fkMed;
    public static volatile SingularAttribute<FichePriseCharge, String> devise;
    public static volatile SingularAttribute<FichePriseCharge, Etat> etat;
    public static volatile SingularAttribute<FichePriseCharge, String> numeroReference;
    public static volatile SingularAttribute<FichePriseCharge, String> detailMotif;
    public static volatile SingularAttribute<FichePriseCharge, BigDecimal> totalPenalitedu;
    public static volatile SingularAttribute<FichePriseCharge, String> fkArticleBudgetaire;
    public static volatile SingularAttribute<FichePriseCharge, Integer> avisRegularisation;
    public static volatile SingularAttribute<FichePriseCharge, String> casFpc;
    public static volatile SingularAttribute<FichePriseCharge, Date> dateEcheance;
    public static volatile SingularAttribute<FichePriseCharge, Penalite> motif;

}