/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var
        codeResearch,
        messageEmptyValue,
        advanced;

var tableCPI;

var
        inputSearch,
        inputDateDebut,
        inputdateLast;

var btnSearch, btnImprimerAcquit;

var
        cmbSearchType,
        selectSite,
        selectService;
var serviceCode,
        siteCode;

var tempAcquitLiberatoireList;

var
        datePickerDebut,
        datePickerFin;

var sumCDF = 0;
var sumUSD = 0;

var isAdvance, lblSite, lblService, lblDateDebut, lblDateFin;
var modalRechercheAvanceeModelTwo;

$(function () {

    mainNavigationLabel.text('ACQUIT LIBERATOIRE');
    secondNavigationLabel.text('Registre des acquits libératoires');

    removeActiveMenu();
    linkMenuAcquitLiberatoire.addClass('active');

    inputSearch = $('#inputSearch');

    btnImprimerAcquit = $('#btnImprimerAcquit');

    tableCPI = $('#tableCPI');

    isAdvance = $('#isAdvance');
    lblSite = $('#lblSite');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    modalRechercheAvanceeModelTwo = $('#modalRechercheAvanceeModelTwo');

    btnSearch = $('#btnSearch');
    btnSearch.click(function (e) {
        e.preventDefault();
        advanced = '0';
        loadAcquitsLiberatoires();
    });

    cmbSearchType = $('#cmbSearchType');
    codeResearch = cmbSearchType.val();
    messageEmptyValue = 'Veuillez d\'abord saisir le nom de l\'assujettii.';

    cmbSearchType.on('change', function (e) {
        codeResearch = cmbSearchType.val();
        inputSearch.val('');

        if (codeResearch === '0') {
            messageEmptyValue = 'Veuillez saisir le nom de l\'assujetti.';
            inputSearch.attr('placeholder', 'Nom de l\'assujetti');
        } else if (codeResearch === '1') {
            messageEmptyValue = 'Veuillez d\'abord saisir le numéro du titre de perception.';
            inputSearch.attr('placeholder', 'Numéro du titre');
        } else if (codeResearch === '2') {
            messageEmptyValue = 'Veuillez d\'abord saisir le numéro de l\'acquit libératoire.';
            inputSearch.attr('placeholder', 'Numéro du CPI');
        }
    });

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    selectSite = $('#selectSite');
    selectService = $('#selectService');

    datePickerDebut = $("#datePicker1");
    datePickerFin = $('#datePicker2');

    btnRechercheAvancee = $('#btnRechercheAvancee');
    btnRechercheAvancee.click(function (e) {
        e.preventDefault();

        selectSite.val(userData.SiteCode);
        selectService.val(userData.serviceCode);
        datePickerDebut.datepicker("setDate", new Date());
        datePickerFin.datepicker("setDate", new Date());

        $('#modalRechercheAvanceeModelTwo').modal('show');
    });

    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnAdvencedSearch.click(function (e) {

        e.preventDefault();

        advanced = '1';
        inputSearch.val('');
        loadAcquitsLiberatoires();

    });

    btnImprimerAcquit.click(function (e) {

        e.preventDefault();

        alertify.confirm('Etes-vous sûre de vouloir imprimer le registre des acquits libératoires ?', function () {
            if (checkSession()) {
                showSessionExpiredMessage();
                return;
            }
            printDataAcquitLiberat();
        });
    });

    printDataAcquitLiberatoire('');
    btnAdvencedSearch.trigger('click');


});

function loadAcquitsLiberatoires() {

    var
            allSite = '0',
            allService = '0';

    if (advanced == '0') {

        if (inputSearch.val().trim() === empty || inputSearch.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
            return;
        }

        serviceCode = userData.serviceCode;
        siteCode = userData.SiteCode;

    } else {
        serviceCode = selectService.val();
        siteCode = selectSite.val();
    }

    if (controlAccess('VIEW_ALL_SITES_NC')) {
        allSite = '1';
    }
    if (controlAccess('VIEW_ALL_SERVICES_NC')) {
        allService = '1';
    }

    if (advanced == '1') {

        // lblSite.html($('#selectSite option:selected').text().toUpperCase());
        //lblService.html($('#selectService option:selected').text().toUpperCase());

        if ($('#selectSite option:selected').text().toUpperCase() == '*') {
            lblSite.html('TOUS LES BUREAUX');
        } else {
            lblSite.html($('#selectSite option:selected').text().toUpperCase());
        }

        if ($('#selectService option:selected').text().toUpperCase() == '*') {
            lblService.html('TOUS LES SERVICES');
        } else {
            lblService.html($('#selectService option:selected').text().toUpperCase());
        }

        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());

        isAdvance.attr('style', 'display: block');
    } else {
        isAdvance.attr('style', 'display: none');
    }

    $.ajax({
        type: 'POST',
        url: 'acquitLiberatoire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAcquitsLiberatoires',
            'advanced': advanced,
            'typeSearch': codeResearch,
            'libelle': inputSearch.val(),
            'allSite': allSite,
            'allService': allService,
            'idUser': userData.idUser,
            'codeService': serviceCode,
            'codeSite': siteCode,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val()
        },
        beforeSend: function () {

            switch (advanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeModelTwo.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            switch (advanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeModelTwo.unblock();
                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                switch (advanced) {

                    case '0':

                        if (codeResearch == '0') {
                            alertify.alert('Cet assujetti n\'est possède pas de CPI');
                            return;
                        } else if (codeResearch == '1') {
                            alertify.alert('Ce titre de perception ne pas lié à un CPI');
                            return;

                        } else if (codeResearch == '2') {
                            alertify.alert('Ce numéro ne pas un CPI');
                            return;
                        }

                        break;

                    case '1':
                        alertify.alert('Aucune ne disponible pour cette période');
                        break;
                }

            } else {

                setTimeout(function () {

                    var acquitLiberatoireList = JSON.parse(JSON.stringify(response));

                    tempAcquitLiberatoireList = [];
                    sumCDF = 0;
                    sumUSD = 0;

                    for (var i = 0; i < acquitLiberatoireList.length; i++) {
                        var cpi = new Object();
                        cpi.date = acquitLiberatoireList[i].date;
                        cpi.reference = acquitLiberatoireList[i].reference;
                        cpi.periode = acquitLiberatoireList[i].periode;
                        cpi.articleBudgetaire = acquitLiberatoireList[i].articleBudgetaire;
                        cpi.requerant = acquitLiberatoireList[i].requerant;
                        cpi.formeJuridique = acquitLiberatoireList[i].formeJuridique;
                        cpi.notePerception = acquitLiberatoireList[i].notePerception;
                        cpi.paye = acquitLiberatoireList[i].paye;
                        cpi.isPrint = acquitLiberatoireList[i].isPrint;
                        cpi.devise = acquitLiberatoireList[i].devise;
                        cpi.userName = acquitLiberatoireList[i].userName;
                        cpi.vignette = acquitLiberatoireList[i].vignette;
                        cpi.codeArticleBudgetaire = acquitLiberatoireList[i].codeArticleBudgetaire;
                        cpi.montantPayer = acquitLiberatoireList[i].paye + ' ' + acquitLiberatoireList[i].devise;

                        if (acquitLiberatoireList[i].devise == 'CDF') {
                            sumCDF += +acquitLiberatoireList[i].paye;
                        } else {
                            sumUSD += +acquitLiberatoireList[i].paye;
                        }

                        cpi.sumUSD = sumUSD;
                        cpi.sumCDF = sumCDF;

                        tempAcquitLiberatoireList.push(cpi);
                    }

                    printDataAcquitLiberatoire(tempAcquitLiberatoireList);

                }
                , 1);

                $('#modalRechercheAvanceeModelTwo').modal('hide');
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDataAcquitLiberatoire(acquitLiberatoireList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col">EXERCICE</th>';
    header += '<th scope="col">N° NOTE </th>';
    header += '<th scope="col">DATE CREATION</th>';
    header += '<th scope="col">NTD</th>';
    header += '<th scope="col">ASSUJETTI</th>';
    header += '<th scope="col">TYPE ASSUJETTI</th>';
    header += '<th scope="col">REFERENCE</th>';
    header += '<th scope="col"> ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col" style="text-align:right">MONTANT PAYE</th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody >';

    for (var i = 0; i < acquitLiberatoireList.length; i++) {

        var firstLineAB = '';

        if (acquitLiberatoireList[i].articleBudgetaire.length > 350) {
            firstLineAB = acquitLiberatoireList[i].articleBudgetaire.substring(0, 350).toUpperCase() + ' ...';
        } else {
            firstLineAB = acquitLiberatoireList[i].articleBudgetaire.toUpperCase();
        }

        var value = '';

        if (acquitLiberatoireList[i].codeArticleBudgetaire == '00000000000002302020') {
            value = '<hr/>' + 'VIGNETTE : ' + '<span style="font-weight:bold">' + acquitLiberatoireList[i].vignette + '</span>';
        }

        body += '<tr>';
        body += '<td style="vertical-align:middle;">' + acquitLiberatoireList[i].periode + '</td>';
        body += '<td style="vertical-align:middle;">' + acquitLiberatoireList[i].notePerception.toUpperCase() + value + '</td>';
        body += '<td style="vertical-align:middle;">' + acquitLiberatoireList[i].date + '</td>';
        body += '<td style="vertical-align:middle;width:10%">' + acquitLiberatoireList[i].userName + '</td>';
        body += '<td style="vertical-align:middle;width:20%">' + acquitLiberatoireList[i].requerant.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;width:15%">' + acquitLiberatoireList[i].formeJuridique.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;width:15%">' + acquitLiberatoireList[i].reference + '</td>';
        body += '<td style="text-align:left;vertical-align:middle;width:20%; title="' + acquitLiberatoireList[i].articleBudgetaire.toUpperCase() + '"><span style="font-weight:bold;"></span>' + firstLineAB.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle;width:10%;text-align:right">' + formatNumber(acquitLiberatoireList[i].paye, acquitLiberatoireList[i].devise) + '</td>';
        body += '<td style="vertical-align:middle;"><a title="Cliquer pour ré-imprimer" style="margin-right:3px" onclick="print(\''
                + acquitLiberatoireList[i].reference
                + '\')" class="btn btn-primary"><i class="fa fa-print"></i></a></td>';
        body += '</tr>';

        var paye = formatNumberOnly(acquitLiberatoireList[i].paye);
    }

    body += '</tbody>';

    body += '<tfoot>';
    body += '<tr><th colspan="8" style="text-align:right;font-size:14px;vertical-align:middle">TOTAL MONTANT</th><th style="text-align:right;font-size:15px;color:red"></th></tr>';
    body += '</tfoot>';

    var tableContent = header + body;

    tableCPI.html(tableContent);

    var dtNoteCalcul = tableCPI.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 15,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;
            $(api.column(8).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD')
                    );
        },
        columnDefs: [
            {"visible": false, "targets": 6}
        ],
        order: [[5, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(5, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="11">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }

    });
}



function rePrinterAcquitLberatoire(reference) {

    if (reference == '') {
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'acquitLiberatoire_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'rePrintAcquitLiberatoire',
            'reference': reference
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Ré-impression en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                setDocumentContent(response);
                window.open('visualisation-document', '_blank');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function print(numeroNP) {

    alertify.confirm('Etes-vous sûre de vouloir ré-imprimer cet acquit libératoire?', function () {

        rePrinterAcquitLberatoire(numeroNP);
    });
}

function printDataAcquitLiberat() {

    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('l', 'pt');
    var docTitle, position, hauteur = 155;

    if (tempAcquitLiberatoireList.length > 0) {

        if (advanced == '1') {
            docTitle = 'REGISTRE DES ACQUIT LIBERATOIRE';
            if (selectSite.val() == '*' && selectService.val() == '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                position = 300;
            } else if (selectSite.val() !== '*' && selectService.val() == '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSITE : ' + $('#selectSite option:selected').text();
                position = 300;
                hauteur = 160;
            } else if (selectSite.val() == '*' && selectService.val() !== '*') {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSERVICE : ' + $('#selectService option:selected').text();
                position = 300;
                hauteur = 160;
            } else {
                docTitle += '(' + inputDateDebut.val() + ' au ' + inputdateLast.val() + ')';
                docTitle += '\nSITE : ' + $('#selectSite option:selected').text();
                docTitle += '\nSERVICE : ' + $('#selectService option:selected').text();
                position = 300;
                hauteur = 170;
            }
        } else {
            var critere = inputSearch.val().toUpperCase();
            docTitle = 'REGISTRE DES ACQUIT LIBERATOIRE : ' + critere;
            position = 300;
        }

    } else {
        docTitle = 'REGISTRE DES ACQUIT LIBERATOIRE';
        position = 440;
    }

    var columns = [
        {title: "EXERCICE", dataKey: "periode"},
        {title: "N° NOTE", dataKey: "notePerception"},
        {title: "DATE CREATION", dataKey: "date"},
        {title: "ASSUJETTI", dataKey: "requerant"},
        {title: "TYPE ASSUJETTI", dataKey: "formeJuridique"},
        {title: "REFERENCE", dataKey: "reference"},
        {title: "ARTICLE BUDGETAIRE", dataKey: "articleBudgetaire"},
        {title: "MONTANT", dataKey: "montantPayer"}];

    var rows = tempAcquitLiberatoireList;

    var pageContent = function (data) {

        doc.setFontSize(10);
        doc.text("REPUBLIQUE DEMOCRATIQUE DU CONGO", 40, 25);
        doc.setFontSize(9);
        doc.text("PROVINCE DU HAUT-KATANGA", 40, 40);
        doc.setFontSize(10);
        doc.text(docTitle, position, 140);

        doc.setFontSize(8);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-RECETTES 1.0', 590, 25);
        doc.text("Par : " + userData.nomComplet, 590, 35);
        doc.text("Page : " + data.pageCount, 590, 45);

    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columns, rows, {
        addPageContent: pageContent,
        margin: {top: hauteur},
        theme: 'grid',
        headerStyles: {
            fillColor: [44, 62, 80],
            fontSize: 9},
        columnStyles: {
            periode: {columnWidth: 70, fontSize: 8, overflow: 'linebreak'},
            notePerception: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            date: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            requerant: {columnWidth: 150, overflow: 'linebreak', fontSize: 9},
            formeJuridique: {columnWidth: 80, overflow: 'linebreak', fontSize: 8},
            reference: {columnWidth: 70, overflow: 'linebreak', fontSize: 8},
            articleBudgetaire: {columnWidth: 180, overflow: 'linebreak', fontSize: 9},
            montantPayer: {columnWidth: 70, overflow: 'linebreak', fontSize: 8}
        },
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 10 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(10);
    doc.setFontStyle('bold');
    doc.text("TOTAL EN FRANC CONGOLAIS  :  " + formatNumberOnly(sumCDF), 540, finalY + 40);
    doc.text("TOTAL EN DOLLAR AMERICAIN :  " + formatNumberOnly(sumUSD), 540, finalY + 60);
    window.open(doc.output('bloburl'), '_blank');
}
