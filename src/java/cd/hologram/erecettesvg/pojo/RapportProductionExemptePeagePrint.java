/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class RapportProductionExemptePeagePrint implements Serializable{
    
    String datePrint;
    String axepeage;
    String poste;
    String controleurName;
    String controleurCode;
    String controleurPhone;
    String percepteurName;
    String percepteurCode;
    String percepteurPhone;
    
    String efV;
    String efVL;
    String efCIP;
    String efBM;
    String efTOTAL;
    
    String efC2P;
    String efCR;
    String efCRTR;
    String efVIT;
    String efVLIT;
    String efCIPBM;
    String efTOTAL2;
    
    String totlGenTour;
    String observation;
    String lieuPrint;

    public String getDatePrint() {
        return datePrint;
    }

    public void setDatePrint(String datePrint) {
        this.datePrint = datePrint;
    }

    public String getAxepeage() {
        return axepeage;
    }

    public void setAxepeage(String axepeage) {
        this.axepeage = axepeage;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getControleurName() {
        return controleurName;
    }

    public void setControleurName(String controleurName) {
        this.controleurName = controleurName;
    }

    public String getControleurCode() {
        return controleurCode;
    }

    public void setControleurCode(String controleurCode) {
        this.controleurCode = controleurCode;
    }

    public String getControleurPhone() {
        return controleurPhone;
    }

    public void setControleurPhone(String controleurPhone) {
        this.controleurPhone = controleurPhone;
    }

    public String getPercepteurName() {
        return percepteurName;
    }

    public void setPercepteurName(String percepteurName) {
        this.percepteurName = percepteurName;
    }

    public String getPercepteurCode() {
        return percepteurCode;
    }

    public void setPercepteurCode(String percepteurCode) {
        this.percepteurCode = percepteurCode;
    }

    public String getPercepteurPhone() {
        return percepteurPhone;
    }

    public void setPercepteurPhone(String percepteurPhone) {
        this.percepteurPhone = percepteurPhone;
    }

    public String getEfV() {
        return efV;
    }

    public void setEfV(String efV) {
        this.efV = efV;
    }

    public String getEfVL() {
        return efVL;
    }

    public void setEfVL(String efVL) {
        this.efVL = efVL;
    }

    public String getEfCIP() {
        return efCIP;
    }

    public void setEfCIP(String efCIP) {
        this.efCIP = efCIP;
    }

    public String getEfBM() {
        return efBM;
    }

    public void setEfBM(String efBM) {
        this.efBM = efBM;
    }

    public String getEfTOTAL() {
        return efTOTAL;
    }

    public void setEfTOTAL(String efTOTAL) {
        this.efTOTAL = efTOTAL;
    }

    public String getEfC2P() {
        return efC2P;
    }

    public void setEfC2P(String efC2P) {
        this.efC2P = efC2P;
    }

    public String getEfCR() {
        return efCR;
    }

    public void setEfCR(String efCR) {
        this.efCR = efCR;
    }

    public String getEfCRTR() {
        return efCRTR;
    }

    public void setEfCRTR(String efCRTR) {
        this.efCRTR = efCRTR;
    }

    public String getEfVIT() {
        return efVIT;
    }

    public void setEfVIT(String efVIT) {
        this.efVIT = efVIT;
    }

    public String getEfVLIT() {
        return efVLIT;
    }

    public void setEfVLIT(String efVLIT) {
        this.efVLIT = efVLIT;
    }

    public String getEfCIPBM() {
        return efCIPBM;
    }

    public void setEfCIPBM(String efCIPBM) {
        this.efCIPBM = efCIPBM;
    }

    public String getEfTOTAL2() {
        return efTOTAL2;
    }

    public void setEfTOTAL2(String efTOTAL2) {
        this.efTOTAL2 = efTOTAL2;
    }

    public String getTotlGenTour() {
        return totlGenTour;
    }

    public void setTotlGenTour(String totlGenTour) {
        this.totlGenTour = totlGenTour;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }

    
    
}
