package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.DetailsRole;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Role.class)
public class Role_ { 

    public static volatile SingularAttribute<Role, Date> dateCreat;
    public static volatile SingularAttribute<Role, String> fkSite;
    public static volatile SingularAttribute<Role, BigDecimal> fraisPoursuiteGlobal;
    public static volatile SingularAttribute<Role, Date> periodeFin;
    public static volatile SingularAttribute<Role, String> articleRole;
    public static volatile ListAttribute<Role, DetailsRole> detailsRoleList;
    public static volatile SingularAttribute<Role, String> observationDirection;
    public static volatile SingularAttribute<Role, String> id;
    public static volatile SingularAttribute<Role, String> observationReceveur;
    public static volatile SingularAttribute<Role, Agent> agentCreat;
    public static volatile SingularAttribute<Role, Date> periodeDebut;
    public static volatile SingularAttribute<Role, Integer> etat;

}