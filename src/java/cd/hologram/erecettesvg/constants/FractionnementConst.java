/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author moussa.toure
 */
public class FractionnementConst {

    public class ParamName {

        public final static String NotePerception = "notePerception";
        public final static String listTitreJson = "titreList";
        public final static String LetterReference = "letterReference";
        public final static String Observation = "observation";
        public final static String UserId = "userId";
        public final static String Etat = "etat";
        public final static String CodeDemande = "codeLettre";

        public final static String ExerciceFiscal = "exerciceFiscal";
        public final static String ServiceAssietteName = "serviceAssietteName";
        public final static String DateCreateDe = "dateCreateDe";

        public final static String NoteCalculNpMere = "noteCalculNpMere";
        public final static String AccountBankCodeNpMere = "accountBankCodeNpMere";
        public final static String BankCodeNpMere = "bankCodeNpMere";

        public final static String AssujettiCode = "assujettiCode";
        public final static String AssujettiName = "assujettiName";

        public final static String AdresseName = "adresseName";
        public final static String DeviseNpMere = "devise";

        public final static String BudgetArticleName = "budgetArticleName";

        public final static String LegalFormName = "legalFormName";

        public final static String AmountDuNpMere = "amountDuNpMere";

        public final static String ValueSearch = "valueSearch";
        public final static String TypeeSearch = "typeSearch";
        public final static String ModeSearch = "modeSearch";

        public final static String ChildenExists = "childenExists";
        public final static String ListFicheCompte = "listFicheCompte";

        public final static String NumeroNpFille = "numeroIntercalaire";
        public final static String EcheanceNpFille = "echeanceNpFille";
        public final static String DetailEchelonnement = "detailEchelonnement";
        public final static String TitreEchelonnementForOrdonnancement = "titreEchelonnementForOrdonnancement";
        public final static String listIntercalaireDetail = "listIntercalaireDetail";
        public final static String EcheanceNpFilleSource = "echeanceNpFilleSource";
        public final static String EcheanceNpFilleProrogee = "echeanceNpFilleProrogee";
        public final static String Archives = "archives";
        public final static String AmountDuNpFille = "amountDuNpFille";
        public final static String DeviseNpFille = "deviseNpFille";
        public final static String StateNpFille = "stateNpFille";
        public final static String StateNpFilleBoolean = "stateNpFilleBoolean";
        public final static String EchelonnementDepasse = "echelonnementDepasse";
        public final static String EchelonnementAnnulable = "echelonnementAnnulable";

        public final static String CodeSite = "codeSite";
        public final static String CodeService = "codeService";

        public final static String DateDebut = "dateDebut";
        public final static String DateFin = "dateFin";
        public final static String ViewAllSite = "viewAllSite";
        public final static String ViewAllService = "viewAllService";

        public final static String DemandeEchelonnementCode = "demandeEchelonnementCode";
        public final static String StateDemandeEchelonnement = "stateDemandeEchelonnement";

        public final static String ObservationValidation = "observationValidation";

        public final static String OBSERVATION_LIST = "observationList";

        public final static String DateValidation = "dateValidation";
        public final static String AgentValidation = "agentValidation";
        public final static String AgentCreate = "agentCreate";

        public final static String StateCondition = "stateCondition";
        public final static String typeRegistre = "stateCondition";

        public final static String NumeroNpMere = "numeroNpMere";
        public final static String NUMERO_TITRE = "numeroTitre";
        public final static String NUMERO_TITRE_MANUEL = "numeroTitreManuel";
        public final static String TYPE_TITRE = "typeTitre";
        public final static String PREFIX_PROVINCE = "prefixProvince";
        public final static String TITRE_MANUEL_ECHELONNEMENT = "titreEchelonnement";

        public final static String INTERCALAIRE_LIST = "intercalaireList";

        public final static String EcheanceNpIntercalaire = "echeanceNpIntercalaire";
        public final static String AmountNpIntercalaire = "amountNpIntercalaire";
        public final static String NumeroNpIntercalaire = "numeroNpIntercalaire";

        public final static String EchelonnementExists = "echelonnementExists";

        public final static String EchelonnementState = "echelonnementState";

        public final static String DateLimiteEcheance = "dateLimiteEcheance";

        public final static String interetBonApayer = "interetBonAPayer";
        public final static String MontantInteret = "montantInteret";

        public final static String Archive = "archive";

        public final static String ListNpFilleProrogation = "listNpFilleProrogation";

        public final static String FK_DOCUMENT = "fkDocument";
        public final static String LIBELLE_DOCUMENT = "libelleDocument";
        public final static String OBSERVATION = "observation";
        public final static String OBSERVATION_DOCUMENT = "observationDocument";
        public final static String PV_DOCUMENT = "pvDocument";
        public final static String CodePersonne = "codePersonne";
    }

    public class Operation {

        public final static String SAVE_DEMANDE_ECHELONNEMENT = "1201";
        public final static String LOAD_TITRE_DETAIL = "loadTitreDetail";
        public final static String VALIDER_DEMANDE_ECHELONNEMENT = "1202";
        public final static String REJETER_DEMANDE_ECHELONNEMENT = "rejeterDemande";
        public final static String LOAD_DEMANDE_ECHELONNEMENT = "getListDemandeEchelonnement";
        public final static String ORDONNANCEMENT_NP_ECHELONNEMENT = "1204";
        public final static String ANNULATION_NP_FRACTIONNE = "1206";
        public final static String PRINT_NP_INTERCALAIRE = "printNpIntercalaire";
        public final static String LOAD_ARCHIVE = "loadArchive";
        public final static String PROROGER_ECHEANCE_PAIEMENT = "1207";
    }

    public class EtatPaiement {

        public final static String PAIEMENT_VALID = "PAYEE";
        public final static String PAIEMENT_INVALID = "NON PAYEE";
    }

    public class code {

        public final static String FOUR_CENT = "400";
        public final static String FIVE_CENT = "500";
        public final static String SIX_CENT = "600";
        public final static String SEVEN_CENT = "700";
        public final static String HEIGHT_CENT = "800";
        public final static String NINE_CENT = "900";
        public final static String LIGHT = "1000";
        public final static String NINE_CENT_ONE = "901";
        public final static String NINE_CENT_TWO = "902";
    }

}
