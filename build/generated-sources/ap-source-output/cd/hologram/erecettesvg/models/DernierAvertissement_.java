package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.ExtraitDeRole;
import cd.hologram.erecettesvg.models.Personne;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DernierAvertissement.class)
public class DernierAvertissement_ { 

    public static volatile SingularAttribute<DernierAvertissement, Personne> fkPersonne;
    public static volatile SingularAttribute<DernierAvertissement, Date> dateCreat;
    public static volatile SingularAttribute<DernierAvertissement, Date> dateEcheance;
    public static volatile SingularAttribute<DernierAvertissement, String> id;
    public static volatile SingularAttribute<DernierAvertissement, Agent> agentCreat;
    public static volatile SingularAttribute<DernierAvertissement, Integer> etat;
    public static volatile SingularAttribute<DernierAvertissement, ExtraitDeRole> fkExtraitRole;
    public static volatile SingularAttribute<DernierAvertissement, Date> dateReception;

}