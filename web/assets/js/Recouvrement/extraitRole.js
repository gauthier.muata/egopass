/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var typeSearch;
var btnShowAdvancedSerachModalRole;
var btnSimpleSearchRole, ResearchTypeRole, ResearchValueRole;
var btnOrdonnancerExtraitRole;
var tableRegistreExtraitRole, tableRegistreExtraitRole2, tableDetailRole;
var tempExtraitRole = [];
var lblNameAssujetti, lblArticleRole;
var paramData;
var modalDetailExtraitRole;
var tempExtraitRoleId = [];
var idExtraitRole;
var btnShowAccuserReceptionExtr;
var tableNouveauTitrePerception;
var btnDernierAvertissement, btnImprimerEtraitRole;
var idAssujetti;

var lblMinistere, lblService, lblDateDebut, lblDateFin, lblSite, lblEntite, lblProvince, lblAgent;
var isAdvance;
var urlExtraitRole;

var extraitRoleListPublic = [];

var modalTrackerLineExtraitRole;
var lblArticleRoleExtrait;
var lblNameAssujettiExtrait;
var divInfoTrackerLine;
var dataTrackerLine;

var idLevelZeroLi, idLevelOneLi, idLevelTwoLi, idLevelThreeLi, idLevelFourLi;
var idLevelZero, idLevelOne, idLevelTwo, idLevelThree, idLevelFour;

var advancedSearchParam;

var codeAssujetti = '';

$(function () {

    mainNavigationLabel.text('RECOUVREMENT');
    secondNavigationLabel.text('Registre des extraits des rôles');


    removeActiveMenu();
    linkMenuRecouvrement.addClass('active');

//    if (!controlAccess('14005')) {
//        window.location = 'dashboard';
//    }

    modalTrackerLineExtraitRole = $('#modalTrackerLineExtraitRole');
    lblArticleRoleExtrait = $('#lblArticleRoleExtrait');
    lblNameAssujettiExtrait = $('#lblNameAssujettiExtrait');
    divInfoTrackerLine = $('#divInfoTrackerLine');
    dataTrackerLine = $('#dataTrackerLine');

    tableRegistreExtraitRole = $('#tableRegistreExtraitRole');
    tableRegistreExtraitRole2 = $('#tableRegistreExtraitRole2');
    tableDetailRole = $('#tableDetailRole');
    ResearchValueRole = $('#ResearchValueRole');
    btnOrdonnancerExtraitRole = $('#btnOrdonnancerExtraitRole');
    btnSimpleSearchRole = $('#btnSimpleSearchRole');
    btnShowAdvancedSerachModalRole = $('#btnShowAdvancedSerachModalRole');
    ResearchTypeRole = $('#ResearchTypeRole');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblArticleRole = $('#lblArticleRole');
    lblProvince = $('#lblProvince');
    lblEntite = $('#lblEntite');

    modalDetailExtraitRole = $('#modalDetailExtraitRole');
    btnShowAccuserReceptionExtr = $('#btnShowAccuserReceptionExtr');
    tableNouveauTitrePerception = $('#tableNouveauTitrePerception');
    btnDernierAvertissement = $('#btnDernierAvertissement');
    btnImprimerEtraitRole = $('#btnImprimerEtraitRole');

    lblMinistere = $('#lblMinistere');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblSite = $('#lblSite');
    isAdvance = $('#isAdvance');
    lblAgent = $('#lblAgent');

    lblArticleRoleExtrait = $('#lblArticleRoleExtrait');
    lblNameAssujettiExtrait = $('#lblNameAssujettiExtrait');

    idLevelZeroLi = $('#idLevelZeroLi');
    idLevelOneLi = $('#idLevelOneLi');
    idLevelTwoLi = $('#idLevelTwoLi');
    idLevelThreeLi = $('#idLevelThreeLi');
    idLevelFourLi = $('#idLevelFourLi');

    idLevelZero = $('#idLevelZero');
    idLevelOne = $('#idLevelOne');
    idLevelTwo = $('#idLevelTwo');
    idLevelThree = $('#idLevelThree');
    idLevelFour = $('#idLevelFour');

    ResearchTypeRole.on('change', function () {

        ResearchValueRole.val('');
        var paramSearch = ResearchTypeRole.val();
        switch (paramSearch) {
            case '1':
                ResearchValueRole.attr('placeholder', 'Nom de l\'assujetti');
                ResearchValueRole.val();
                ResearchValueRole.attr('disabled', 'true');
                assujettiModal.modal('show');
                break;
            case '2':
                ResearchValueRole.attr('placeholder', 'Article du rôle');
                ResearchValueRole.val();
                ResearchValueRole.removeAttr('disabled');
        }
    });

    btnSimpleSearchRole.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (ResearchTypeRole.val() === '1') {
            allSearchAssujetti = '1';
            assujettiModal.modal('show');
        } else {
            if (ResearchValueRole.val().trim() == '') {
                alertify.alert('Veuillez d\'abord saisir un critère de recherche.');
                ResearchValueRole.val('');
                return;
            }
            paramData = '2';
            typeSearch = '1';
            isAdvance.attr('style', 'display: none');
            getCurrentSearchParam(0);
            searchExtraitRole();
        }
    });

    btnShowAdvancedSerachModalRole.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeNC.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        paramData = '2';
        typeSearch = '2';
        isAdvance.attr('style', 'display: block;');
        getCurrentSearchParam(1);
        searchExtraitRole();
        modalRechercheAvanceeNC.modal('hide');
    });

    btnShowAccuserReceptionExtr.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        initAccuseReceptionUI(idExtraitRole, 'EXTR');
    });

    btnDernierAvertissement.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        CreatDernierAvertissement();
    });

    btnImprimerEtraitRole.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        printExtraitRole();
    });

    urlExtraitRole = getUrlParameter('id');

//    setTimeout(function () {
//
//        var paramDashboard = getDashboardParam();
//
//        if (jQuery.type(urlExtraitRole) !== 'undefined') {
//            var articleRole = atob(urlExtraitRole);
//            ResearchTypeRole.val('2');
//            ResearchValueRole.val(articleRole);
//            setTimeout(function () {
//                btnSimpleSearchRole.trigger('click');
//            }, 1000);
//
//        } else {
//
//            if (paramDashboard == 'null') {
//
//                setTimeout(function () {
//                    btnAdvencedSearch.trigger('click');
//                }, 1000);
//
//            } else {
//
//                modeResearch = '2';
//                advancedSearchParam = JSON.parse(paramDashboard);
//
//                setTimeout(function () {
//                    paramData = '2';
//                    typeSearch = '2';
//                    searchExtraitRole();
//                }, 1000);
//
//            }
//        }
//
//    }, 1000);

});

function searchExtraitRole() {

    if (typeSearch == '2') {

        try {

            var service = advancedSearchParam.serviceLibelle;
            var site = advancedSearchParam.siteLibelle;
            var entite = advancedSearchParam.entiteLibelle;
            var province = advancedSearchParam.provinceLibelle;
            var agent = advancedSearchParam.agentName;

            lblProvince.text(getsearchbarText(advancedSearchParam.province.length, advancedSearchParam.provinceLibelle));
            lblProvince.attr('title', province);

            lblEntite.text(getsearchbarText(advancedSearchParam.entite.length, advancedSearchParam.entiteLibelle));
            lblEntite.attr('title', entite);

            lblService.text(getsearchbarText(advancedSearchParam.service.length, advancedSearchParam.serviceLibelle));
            lblService.attr('title', service);

            lblSite.text(getsearchbarText(advancedSearchParam.site.length, advancedSearchParam.siteLibelle));
            lblSite.attr('title', site);

            lblAgent.text(getsearchbarText(advancedSearchParam.agent.length, advancedSearchParam.agentName));
            lblAgent.attr('title', agent);

            lblDateDebut.text(advancedSearchParam.dateDebut);
            lblDateFin.text(advancedSearchParam.dateFin);

        } catch (err) {
            location.reload(true);
        }
    }

    var valueResearch = ResearchValueRole.val().trim();

    if (ResearchTypeRole.val() === '1') {
        valueResearch = codeAssujetti;
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'typeSearch': typeSearch,
            'articleRole': valueResearch,
            'operation': 'loadExtraitRole',
            'typeRegister': paramData,
            'codeService': JSON.stringify(advancedSearchParam.service),
            'codeSite': JSON.stringify(advancedSearchParam.site),
            'codeEntite': JSON.stringify(advancedSearchParam.entite),
            'codeAgent': JSON.stringify(advancedSearchParam.agent),
            'dateDebut': advancedSearchParam.dateDebut,
            'dateFin': advancedSearchParam.dateFin,
            'critereSearch': ResearchTypeRole.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                loadRegistreExtraitRole('');
                return;
            }

            setTimeout(function () {

                $.unblockUI();
                var extraitRoleList = JSON.parse(JSON.stringify(response));
                extraitRoleListPublic = extraitRoleList;
                loadRegistreExtraitRole(extraitRoleList);
            }
            , 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadRegistreExtraitRole(result) {

    var stateExtraitRole;

    tempExtraitRole = [];
    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
    header += '<th>ARTICLE DE L\'EXTRAIT DE ROLE</th>';
    header += '<th>ASSUJETTI</th>';
    header += '<th>DATE CREATION</th>';
    header += '<th>DATE RECEPTION</th>';
    header += '<th>DATE EXI.</th>';
    header += '<th>ETAPE</th>';
    header += '<th style ="text-align: right">MONTANT DÛ</th>';
    header += '<th style ="text-align: right">FARIS DE POURSUITE</th>';
    header += '<th style ="text-align: right">MONTANT PAYER</th>';
    header += '<th style ="text-align: right">RESTE A PAYER</th>';
    header += '<th></th></tr></thead>';

    var data = '';
    data += '<tbody id="bodyTable">';

    var sumMontantDuCDF = 0;
    var sumMontantPayerCDF = 0;
    var sumFraisDePoursuite = 0;

    for (var i = 0; i < result.length; i++) {

        var montantDuExtrait = getSumMontantDuExtraitById(JSON.parse(result[i].detailRoleList));
        sumMontantDuCDF += montantDuExtrait;
        var montantPayeExtrait = getSumMontantPayeExtraitById(JSON.parse(result[i].detailRoleList));
        var fraisDePoursuite = result[i].fraisPoursuite;
        sumFraisDePoursuite += fraisDePoursuite;
        sumMontantPayerCDF += montantPayeExtrait;
        var RestePayerExtrait = (montantDuExtrait + fraisDePoursuite) - montantPayeExtrait;

        var extraitRole = {};

        extraitRole.extraitRoleId = result[i].extraitRoleId;
        extraitRole.articleRole = result[i].articleRole;
        extraitRole.assujettiName = result[i].assujettiName;
        extraitRole.codeAssujetti = result[i].codeAssujetti;
        extraitRole.detailRoleList = result[i].detailRoleList;
        extraitRole.titrePerceptionList = result[i].titrePerceptionList;
        extraitRole.dateReceptionExtraitRole = result[i].dateReceptionExtraitRole;
        extraitRole.hasChildNp = result[i].hasChildNp;
        extraitRole.echeanceExtraitRoleDepasser = result[i].echeanceExtraitRoleDepasser;
        extraitRole.restePayerExtraitRole = RestePayerExtrait;
        extraitRole.etat = result[i].stateExtraitRole;

        tempExtraitRole.push(extraitRole);

        if (result[i].niveauExtraitRole == 'N5') {
            stateExtraitRole = "(Niveau : ATD)";
        } else if (result[i].niveauExtraitRole == 'N4') {
            stateExtraitRole = "(Niveau : PV DE SAISIE)";
        } else if (result[i].niveauExtraitRole == 'N3') {
            stateExtraitRole = "(Niveau : COMMANDEMENT)";
        } else if (result[i].niveauExtraitRole == 'N2') {
            stateExtraitRole = "(Niveau : CONTRAINTE)";
        } else if (result[i].niveauExtraitRole == 'N1') {
            stateExtraitRole = "(Niveau : DERNIER AVERTISSEMENT)";
        } else if (result[i].niveauExtraitRole == 'N0') {

            if (result[i].stateExtraitRole == 1) {
                stateExtraitRole = "VALIDER";
            } else if (result[i].stateExtraitRole == 2) {
                stateExtraitRole = "EN ATTENTE DE VALIDATION";
            } else {
                stateExtraitRole = "";
            }

        }

        var firstLineASS = '';
        var firstLineAB = '';

        if (result[i].assujettiName.length > 30) {
            firstLineASS = result[i].assujettiName.substring(0, 30) + ' ...';
        } else {
            firstLineASS = result[i].assujettiName;
        }

        if (result[i].articleRole.length > 40) {
            firstLineAB = result[i].articleRole.substring(0, 40) + ' ...';
        } else {
            firstLineAB = result[i].articleRole;
        }

        var color = 'color: black';

        if (result[i].echeanceExtraitRoleDepasser == 1 && RestePayerExtrait > 0) {
            color = 'color: red';
        }

        data += '<tr>';
        data += '<td style="text-align:left;width:15%;" title="' + result[i].articleRole + '">' + firstLineAB + '</td>';
        data += '<td style="text-align:left;width:15%" title="' + result[i].assujettiName + '">' + firstLineASS + '</td>';
        data += '<td style="text-align:left">' + result[i].dateCreateExtraitRole + '</td>';
        data += '<td style="text-align:left">' + result[i].dateReceptionExtraitRole + '</td>';
        data += '<td style="text-align:left;' + color + '">' + result[i].dateEcheanceExtraitRole + '</td>';
        data += '<td style="text-align:center;width:18%"><label id="idLabelTrackerLineExtraitRole" style="text-decoration:underline;color:maron" onclick="displayTimeLineExtraitRole(\'' + result[i].extraitRoleId + '\')">Voir les étapes de l\'extrait de rôle </label></td>';
        data += '<td style="text-align:right">' + formatNumber(montantDuExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(fraisDePoursuite, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(montantPayeExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(RestePayerExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:center;width:5%;vertical-align:middle;"> <a  onclick="showModalDetailRole(\'' + result[i].extraitRoleId + '\')" class="btn btn-warning"><i class="fa fa-list"></i></a></i></a></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    data += '<tfoot>';

    data += '<tr><th colspan="6" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableRegistreExtraitRole.html(TableContent);

    tableRegistreExtraitRole.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 5,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        dom: 'Bfrtip',
        columnDefs: [{
                targets: 1,
                className: 'noVis'
            }],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(6).footer()).html(
                    formatNumber(sumMontantDuCDF, 'CDF'));

            $(api.column(7).footer()).html(
                    formatNumber(sumFraisDePoursuite, 'CDF'));

            $(api.column(8).footer()).html(
                    formatNumber(sumMontantPayerCDF, 'CDF'));

            $(api.column(9).footer()).html(
                    formatNumber((sumMontantDuCDF + sumFraisDePoursuite) - sumMontantPayerCDF, 'CDF'));
        }
    });
}

function showModalDetailRole(extraitRoleId) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }


    for (var i = 0; i < tempExtraitRole.length; i++) {

        if (tempExtraitRole[i].extraitRoleId == extraitRoleId) {

            lblArticleRole.html(tempExtraitRole[i].articleRole);
            lblNameAssujetti.html(tempExtraitRole[i].assujettiName);
            idExtraitRole = extraitRoleId;
            idAssujetti = tempExtraitRole[i].codeAssujetti;

            var detailRoleList = JSON.parse(tempExtraitRole[i].detailRoleList);

            printDetailRole(detailRoleList);

            if (tempExtraitRole[i].dateReceptionExtraitRole != empty) {

                btnShowAccuserReceptionExtr.attr('style', 'display: none');
            } else {

                btnShowAccuserReceptionExtr.removeAttr('style', 'display: inline');
            }

            if (tempExtraitRole[i].etat == 3) {
                
                btnDernierAvertissement.attr('style', 'display: none');
                
            } else {

                if (controlAccess('14011') && tempExtraitRole[i].restePayerExtraitRole > 0) {
                    
                    btnDernierAvertissement.attr('style', 'display: inline');
                    
                } else {
                    
                    if (tempExtraitRole[i].echeanceExtraitRoleDepasser == 1 && tempExtraitRole[i].restePayerExtraitRole > 0) {
                       
                        btnDernierAvertissement.attr('style', 'display: inline');
                    
                    } else {
                        
                        btnDernierAvertissement.attr('style', 'display: none');
                    }
                }
            }

            modalDetailExtraitRole.modal('show');
            
            break;
        }
    }
}

function printDetailRole(listDetailRole) {

    tempExtraitRoleId = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th> </th>';
    header += '<th> EXERCICE </th>';
    header += '<th> SERVICE D\'ASSIETTE </th>';
    header += '<th> ARTICLE BUDGETAIRE </th>';
    header += '<th> TYPE </th>';
    header += '<th > TYPE DOCUMENT </th>';
    header += '<th scope="col" > TITRE DE PERCEPTION</th>';
    header += '<th scope="col" > DATE EXIGIBILITE </th>';
    header += '<th scope="col" style="text-align:right"> MONTANT DÛ </th>';
    header += '<th scope="col" style="text-align:right"> PENALITE DÛ </th>';
    header += '</tr></thead>';

    var body = '<tbody>';
    var firstLineAB = '';
    var firstLineSA = '';

    for (var i = 0; i < listDetailRole.length; i++) {

        var extraitRole = {};

        extraitRole.documentReference = listDetailRole[i].documentReference;
        extraitRole.documentReferenceManuel = listDetailRole[i].documentReferenceManuel;
        extraitRole.typeDocument = listDetailRole[i].typeDocument;
        extraitRole.amountPenalite = 0;

        tempExtraitRoleId.push(extraitRole);

        var penaliteDu = listDetailRole[i].TOTAL_PENALITE;

        if (listDetailRole[i].codeOfficiel !== '') {
            if (listDetailRole[i].libelleArticleBudgetaire.length > 40) {
                firstLineAB = listDetailRole[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
            } else {
                firstLineAB = listDetailRole[i].libelleArticleBudgetaire;
            }
        } else {
            firstLineAB = '';
        }


        if (listDetailRole[i].SERVICE.length > 40) {
            firstLineSA = listDetailRole[i].SERVICE.substring(0, 40) + ' ...';
        } else {
            firstLineSA = listDetailRole[i].SERVICE;
        }

        var etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">NON PAYE</a></center>';

        if (listDetailRole[i].isApured == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:green">PAYE</a></center>';
        } else if (listDetailRole[i].isApured == false && listDetailRole[i].isPaid == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">PAYE</a></center>';
        }
        
        body += '<tr>';
        body += '<td class="details-control" style="width:3%text-align:center;"></td>';
        body += '<td style="width:5%">' + listDetailRole[i].exerciceFiscal + '</td>';

        body += '<td style="width:8%" title="' + listDetailRole[i].SERVICE + '">'
                + firstLineSA.toUpperCase() + '</td>';

        body += '<td style="text-align:left;width:20%;" title="' + listDetailRole[i].libelleArticleBudgetaire
                + '">' + firstLineAB + '</td>';

        body += '<td style="width:7%">' + listDetailRole[i].TYPE + '</td>';
        body += '<td style="width:7%" >' + listDetailRole[i].typeDocument + '</td>';
        body += '<td style="width:9%">' + listDetailRole[i].documentReferenceManuel + '</td>';
        body += '<td style="width:9%">' + listDetailRole[i].echeance + '</td>';
        body += '<td style="text-align:right;width:9%">' + formatNumber(listDetailRole[i].MONTANTDU, listDetailRole[i].devise) + ' ' + etatPaiement + '</td>';
        body += '<td style="text-align:right;width:9%">' + formatNumber(penaliteDu, listDetailRole[i].devise) + '</td>';


        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;

    tableDetailRole.html(tableContent);

    var myTableDetailRole = tableDetailRole.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        columnDefs: [
            {"visible": false, "targets": 4}
        ],
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[4, 'asc']],
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(4, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="9">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableDetailRole tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = myTableDetailRole.row(tr);
        var dataDetail = myTableDetailRole.row(tr).data();
        var numeroTitre = dataDetail[6];

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {


            var tableTitreDependant = '<center><h4>LES TITRES DES PERCEPTIONS DEPENDANTS AU TITRE N° : ' + numeroTitre + '</h4><br/></center><table class="table table-bordered">';
            tableTitreDependant += '<thead><tr style="background-color:#e6ceac;color:black"><td>TITRE DE PERCEPTION</td><td>TYPE TITRE DE PERCEPTION</td><td>ECHEANCE</td><td>MONTANT DÛ</td><td>ETAT PAIEMENT</td></tr></thead>';

            var titreList = '';

            for (var j = 0; j < listDetailRole.length; j++) {

                if (listDetailRole[j].documentReferenceManuel == numeroTitre) {

                    titreList = JSON.parse(listDetailRole[j].TITRE_PERCEPTION_DEPENDACY);

                    for (var i = 0; i < titreList.length; i++) {

                        var etatPaiement = '<span style="font-weight:bold;color:red">NON PAYE</span>';

                        if (titreList[i].isApured == true && titreList[i].isPaid == true) {
                            etatPaiement = '<span style="font-weight:bold;color:green">PAYE</span>';
                        } else if (titreList[i].isApured == false && titreList[i].isPaid == true) {
                            etatPaiement = '<span style="font-weight:bold;;color:red">PAYE</span>';
                        }
                        
                        tableTitreDependant += '<tr>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].documentReferenceManuel + '</td>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].TYPE + '</td>';

                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].echeance + '</td>';

                        tableTitreDependant += '<td style="text-align:right;width:25%;">' + formatNumber(titreList[i].MONTANTDU, 'CDF') + '</td>';
                        tableTitreDependant += '<td style="text-align:center:width:15%;">' + etatPaiement + '</td>';
                        tableTitreDependant += '</tr>';
                    }

                }
            }

            tableTitreDependant += '<tbody>';

            tableTitreDependant += '</tbody>';
            row.child(tableTitreDependant).show();

            tr.addClass('shown');

        }

    });
}

function getSumMontantDuExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].MONTANTDU + listeDetailRole[i].TOTAL_PENALITE;
    }
    return sum;
}

function getSumMontantPayeExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].TOTAL_PAYEE_PENALITE + listeDetailRole[i].montantPaye;
    }

    return sum;
}

function CreatDernierAvertissement() {

    alertify.confirm('Etes-vous sûre de vouloir de créer un dernier avertissement ?', function () {

        $.ajax({
            type: 'POST',
            url: 'recouvrement_servlet',
            dataType: 'text',
            headers: {
                'Access-Control-Allow-Origin': '*'
            },
            crossDomain: false,
            data: {
                'extraitRoleId': idExtraitRole,
                'operation': '1403',
                'idUser': userData.idUser,
                'codeAssujetti': idAssujetti
            },
            beforeSend: function () {

                modalDetailExtraitRole.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Génération du dernier avertissement en cours...</h5>'});
            },
            success: function (response)
            {
                modalDetailExtraitRole.unblock();
                if (response === '-1') {
                    showResponseError();
                    return;
                } else if (response === '0') {
                    showResponseError();
                    return;
                } else {
                    setTimeout(function () {

                        setDocumentContent(response);
                        window.open('visualisation-document', '_blank');

                        alertify.alert('Le dernier avertissement a été généré avec succès.');
                        modalDetailExtraitRole.modal('hide');
                        searchExtraitRole();
                    }
                    , 1);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                modalDetailExtraitRole.unblock();
                showResponseError();
            }

        });
    });
}

function printExtraitRole() {
    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': idExtraitRole,
            'operation': 'printDocumentRecouvrement'
        },
        beforeSend: function () {
            modalDetailExtraitRole.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            modalDetailExtraitRole.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucun document trouvé !');
                return;
            }
            setTimeout(function () {
                modalDetailExtraitRole.unblock();
                setDocumentContent(response);
                window.open('document-paysage', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalDetailExtraitRole.unblock();
            showResponseError();
        }

    });
}

function refrechDataAfterAccuserReception() {
    modalDetailExtraitRole.modal('hide');
    searchExtraitRole();
}

function printDocumentGenerique(numeroDocument, typeDoc) {


    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': numeroDocument,
            'typeDocument': typeDoc,
            'operation': '403'
        },
        beforeSend: function () {
            modalDetailExtraitRole.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            modalDetailExtraitRole.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                showResponseError();
                return;
            }
            setTimeout(function () {
                modalDetailExtraitRole.unblock();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalDetailExtraitRole.unblock();
            showResponseError();
        }

    });

}

function initEmptyControls() {

    idLevelZeroLi.addClass('disabled');
    idLevelZeroLi.removeClass('active');

    idLevelZero.html('');

    idLevelOneLi.addClass('disabled');
    idLevelOneLi.removeClass('active');
    idLevelOne.html('');

    idLevelTwoLi.addClass('disabled');
    idLevelTwoLi.removeClass('active');
    idLevelTwo.html('');

    idLevelThreeLi.addClass('disabled');
    idLevelThreeLi.removeClass('active');
    idLevelThree.html('');

    idLevelFourLi.addClass('disabled');
    idLevelFourLi.removeClass('active');
    idLevelFour.html('');
}

function displayTimeLineExtraitRole(extraitRoleId) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    initEmptyControls();

    var trackerLine = [];
    var levelFolder = '';

    for (var i = 0; i < extraitRoleListPublic.length; i++) {

        if (extraitRoleListPublic[i].extraitRoleId == extraitRoleId) {

            levelFolder = extraitRoleListPublic[i].niveauExtraitRole;

            lblArticleRoleExtrait.html(extraitRoleListPublic[i].articleRole);
            lblNameAssujettiExtrait.html(extraitRoleListPublic[i].assujettiName);

            trackerLine = JSON.parse(extraitRoleListPublic[i].trackerLineList);

            if (levelFolder == 'N0') {

                for (var j = 0; j < trackerLine.length; j++) {
                    idLevelZeroLi.addClass('active');
                    idLevelZero.html('Extrait de rôle ' + '(Le ' + trackerLine[j].levelDate + ')');
                }

            } else if (levelFolder == 'N1') {

                for (var j = 0; j < trackerLine.length; j++) {

                    if (trackerLine[j].levelValue == 'N0') {
                        idLevelZeroLi.addClass('active');
                        idLevelZero.html('Extrait de rôle ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N1') {
                        idLevelOneLi.addClass('active');
                        idLevelOne.html('Dernier avert. ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }
                }
            } else if (levelFolder == 'N2') {

                for (var j = 0; j < trackerLine.length; j++) {

                    if (trackerLine[j].levelValue == 'N0') {
                        idLevelZeroLi.addClass('active');
                        idLevelZero.html('Extrait de rôle ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N1') {
                        idLevelOneLi.addClass('active');
                        idLevelOne.html('Dernier avert. ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N2') {
                        idLevelTwoLi.addClass('active');
                        idLevelTwo.html('Contrainte ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }
                }
            } else if (levelFolder == 'N3') {

                for (var j = 0; j < trackerLine.length; j++) {

                    if (trackerLine[j].levelValue == 'N0') {
                        idLevelZeroLi.addClass('active');
                        idLevelZero.html('Extrait de rôle ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N1') {
                        idLevelOneLi.addClass('active');
                        idLevelOne.html('Dernier avert. ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N2') {
                        idLevelTwoLi.addClass('active');
                        idLevelTwo.html('Contrainte ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N3') {
                        idLevelThreeLi.addClass('active');
                        idLevelThree.html('Commandement ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }
                }
            } else if (levelFolder == 'N4') {

                for (var j = 0; j < trackerLine.length; j++) {

                    if (trackerLine[j].levelValue == 'N0') {
                        idLevelZeroLi.addClass('active');
                        idLevelZero.html('Extrait de rôle ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N1') {
                        idLevelOneLi.addClass('active');
                        idLevelOne.html('Dernier avert. ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N2') {
                        idLevelTwoLi.addClass('active');
                        idLevelTwo.html('Contrainte ' + '(Le ' + trackerLine[j].levelDate + ')');

                    }

                    if (trackerLine[j].levelValue == 'N3') {
                        idLevelThreeLi.addClass('active');
                        idLevelThree.html('Commandement ' + '(Le ' + trackerLine[j].levelDate + ')');

                    }

                    if (trackerLine[j].levelValue == 'N4') {
                        idLevelFourLi.addClass('active');
                        idLevelFour.html('PV de Saisie ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }
                }
            } else if (levelFolder == 'N5') {
                for (var j = 0; j < trackerLine.length; j++) {

                    if (trackerLine[j].levelValue == 'N0') {
                        idLevelZeroLi.addClass('active');
                        idLevelZero.html('Extrait de rôle ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N1') {
                        idLevelOneLi.addClass('active');
                        idLevelOne.html('Dernier avert. ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N2') {
                        idLevelTwoLi.addClass('active');
                        idLevelTwo.html('Contrainte ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N3') {
                        idLevelThreeLi.addClass('active');
                        idLevelThree.html('Commandement ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }

                    if (trackerLine[j].levelValue == 'N5') {
                        idLevelFourLi.addClass('active');
                        idLevelFour.html('ATD ' + '(Le ' + trackerLine[j].levelDate + ')');
                    }
                }
            }
            modalTrackerLineExtraitRole.modal('show');
            return;
        }
    }
}

function getSelectedAssujetiData() {

    paramData = '2';
    typeSearch = '1';
    isAdvance.attr('style', 'display: none');

    var responsibleObject = {};

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.nameResponsible = selectAssujettiData.nomComplet;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;

    if (selectAssujettiData.nif !== '') {
        ResearchValueRole.val('Nif : ' + selectAssujettiData.nif + ' / Noms complet : ' + responsibleObject.nameResponsible);
    } else {
        ResearchValueRole.val('Code : ' + selectAssujettiData.code + ' / Noms complet : ' + responsibleObject.nameResponsible);
    }
    codeAssujetti = selectAssujettiData.code;

    getCurrentSearchParam(0);
    searchExtraitRole();
}
