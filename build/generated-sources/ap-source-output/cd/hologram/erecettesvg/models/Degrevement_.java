package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.BonAPayer;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Degrevement.class)
public class Degrevement_ { 

    public static volatile SingularAttribute<Degrevement, Long> code;
    public static volatile SingularAttribute<Degrevement, BigDecimal> montantDegreve;
    public static volatile SingularAttribute<Degrevement, BonAPayer> bonAPayer;
    public static volatile SingularAttribute<Degrevement, String> dossier;
    public static volatile SingularAttribute<Degrevement, String> typeDegrevement;
    public static volatile SingularAttribute<Degrevement, BigDecimal> montantPaye;

}