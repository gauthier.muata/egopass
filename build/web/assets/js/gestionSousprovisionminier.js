/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var tableSousProvision,
        btnCallModalSearchAvanded,
        selectSocieteMiniere,
        modalResearchContribuableMinier;

var dataSousprovision;

$(function () {



    mainNavigationLabel.text('ORDONNANCEMENT');
    secondNavigationLabel.text('Registre des sous-provision des miniers');

    removeActiveMenu();
    linkMenuOrdonnancement.addClass('active');

    tableSousProvision = $('#tableSousProvision');
    btnCallModalSearchAvanded = $('#btnCallModalSearchAvanded');
    modalResearchContribuableMinier = $('#modalResearchContribuableMinier');
    selectSocieteMiniere = $('#selectSocieteMiniere');

    loadMiniers();

});


function loadMiniers() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getMinier'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            setTimeout(function () {

                var dataMinierList = JSON.parse(JSON.stringify(response));
                var dataMinier = '';

                dataMinier += '<option value="*">TOUS LES MINIERS</option>';

                for (var i = 0; i < dataMinierList.length; i++) {
                    dataMinier += '<option value="' + dataMinierList[i].codeMinier + '">' + dataMinierList[i].nameMinier.toUpperCase() + '</option>';
                }

                selectSocieteMiniere.html(dataMinier);
            }
            , 1);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
        }
    });
}

function loadSousProvisionMinier() {

    $.ajax({
        type: 'POST',
        url: 'registrenoteperception',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codePersonne': selectSocieteMiniere.val(),
            'operation': 'getSousProvisionMinier'
        },
        beforeSend: function () {
            modalResearchContribuableMinier.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours ...</h5>'});
        },
        success: function (response)
        {

            modalResearchContribuableMinier.block();

            if (response == '0') {

                printSousprovision(empty);
                alertify.alert('Aucune sous provision trouvée !');
                return;

            } else if (response == '-1') {
                printSousprovision(empty);
                showResponseError();
                return;

            } else {

                setTimeout(function () {

                    dataSousprovision = JSON.parse(JSON.stringify(response));
                    printSousprovision(dataSousprovision);

                }
                , 1);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalResearchContribuableMinier.block();
            printSousprovision(empty);
            showResponseError();
        }
    });
}

function printSousprovision(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left"></th>';
    tableContent += '<th style="text-align:center;width:5%"scope="col">ID</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">N° NOTE PERC.</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">DATE DE CREATION</th>';
    tableContent += '<th style="text-align:left;width:10%"scope="col">DATE SORTIE</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">MINIER</th>';
    tableContent += '<th style="text-align:right;width:11%"scope="col">CREDIT</th>';
    tableContent += '<th style="text-align:right;width:11%"scope="col">DEBIT</th>';
    tableContent += '<th style="text-align:right;width:11%"scope="col">SOLDE</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">TONNAGE INITIAL</th>';
    tableContent += '<th style="text-align:right;width:10%"scope="col">TONNAGE RESTANT</th>';
    tableContent += '<th style="text-align:left;width:15%"scope="col">TAXE</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    var sumCDF = 0;
    var sumUSD = 0;

    for (var i = 0; i < result.length; i++) {

        if (result[i].devise === 'CDF') {
            sumCDF += result[i].credit;
        } else if (result[i].devise === 'USD') {
            sumUSD += result[i].credit;
        }

        tableContent += '<tr>';
        tableContent += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        tableContent += '<td style="vertical-align:middle;width:5%;text-align:center">' + result[i].id + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].notePerception + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].dateCreate + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%">' + result[i].dateDerniereSortie + '</td>';
        tableContent += '<td style="vertical-align:middle;width:15%">' + result[i].nameMinier + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%;text-align:right">' + formatNumber(result[i].credit, result[i].devise) + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%;text-align:right">' + formatNumber(result[i].debit, result[i].devise) + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%;text-align:right">' + formatNumberOnly()(result[i].tonnageInit) + '</td>';
        tableContent += '<td style="vertical-align:middle;width:10%;text-align:right">' + formatNumberOnly()(result[i].tonnageRestant) + '</td>';
        tableContent += '<td style="vertical-align:middle;width:15%">' + result[i].taxeName + '</td>';
        tableContent += '</tr>';

    }
    tableContent += '</tbody>';

    tableContent += '<tfoot>';

    tableContent += '<tr><th colspan="7" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL CREDIT </th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    tableContent += '</tfoot>';

    tableSousProvision.html(tableContent);

    var datatable = tableSousProvision.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        pageLength: 25,
        columnDefs: [
            {"visible": false, "targets": 5}
        ],
        order: [[5, 'asc']],
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3,
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(7).footer()).html(
                    formatNumber(sumCDF, 'CDF') +
                    '<hr/>' +
                    formatNumber(sumUSD, 'USD'));
        },
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(5, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="8">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableSousProvision tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = datatable.row(tr);
        var dataDetail = datatable.row(tr).data();
        var numeroTitre = dataDetail[11];


        var tableTitreDependant = '';

        console.log(row.child.isShown());

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {

            tableTitreDependant = '<center><h4>LES MANIFESTES LIES A LA NOTE DE PERCEPTION N° : ' + numeroTitre + '</h4><br/></center><table class="table table-bordered">';
            tableTitreDependant += '<thead><tr style="background-color:#e6ceac;color:black"><td>TRANSITAIRE</td><td>NATURE PRODUIT</td><td>N° PLAQUE</td><td>TONNAGE</td><td>MONTANT DÛ</td><td style="text-center:center">AUTRES INFOS</td><td></td style="text-center:center"></tr></thead>';

            var titreList = '';

            for (var j = 0; j < dataSousprovision.length; j++) {

                if (dataSousprovision[j].numeroNP == numeroTitre) {

                    titreList = JSON.parse(dataSousprovision[j].complementList);

                    for (var i = 0; i < titreList.length; i++) {

                        var etatPaiement = '<span style="font-weight:bold;color:red">NON PAYE</span>';

                        if (titreList[i].isPaid === '1') {
                            etatPaiement = '<span style="font-weight:bold;color:green">PAYE</span>';
                        }

                        var btn = '<button onclick="printNp(\'' + titreList[i].NUMERO + '\')" class="btn btn-warning"><i class="fa fa-print"></i></button>';

                        tableTitreDependant += '<tr>';
                        tableTitreDependant += '<td style="text-align:left;width:15%;">' + titreList[i].NUMERO + '</td>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].TYPE_DOCUMENT + '</td>';
                        tableTitreDependant += '<td style="text-align:left;width:12%;">' + titreList[i].dateCreate + '</td>';
                        tableTitreDependant += '<td style="text-align:left;width:12%;">' + titreList[i].echeance + '</td>';
                        tableTitreDependant += '<td style="text-align:right;width:20%;">' + formatNumber(titreList[i].MONTANTDU, titreList[i].DEVISE) + '</td>';
                        tableTitreDependant += '<td style="text-center:center:width:15%;">' + etatPaiement + '</td>';
                        tableTitreDependant += '<td style="text-center:center:width:5%;">' + btn + '</td>';
                        tableTitreDependant += '</tr>';
                    }

                }
            }

            tableTitreDependant += '<tbody>';

            tableTitreDependant += '</tbody>';
            row.child(tableTitreDependant).show();
            tr.addClass('shown');

        }

    });
}