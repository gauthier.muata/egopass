package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Process;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Dossier.class)
public class Dossier_ { 

    public static volatile SingularAttribute<Dossier, String> agentMaj;
    public static volatile SingularAttribute<Dossier, String> identite;
    public static volatile ListAttribute<Dossier, Process> processList;
    public static volatile SingularAttribute<Dossier, String> numero;
    public static volatile SingularAttribute<Dossier, Date> dateCreat;
    public static volatile SingularAttribute<Dossier, Date> dateImpressionNotification;
    public static volatile SingularAttribute<Dossier, String> observation;
    public static volatile SingularAttribute<Dossier, String> amr;
    public static volatile SingularAttribute<Dossier, Date> dateReceptionNotification;
    public static volatile SingularAttribute<Dossier, String> intitule;
    public static volatile SingularAttribute<Dossier, Short> etat;
    public static volatile SingularAttribute<Dossier, Boolean> etatRejet;
    public static volatile SingularAttribute<Dossier, Date> dateMaj;
    public static volatile SingularAttribute<Dossier, String> dateDecision;
    public static volatile SingularAttribute<Dossier, String> autreReference;
    public static volatile SingularAttribute<Dossier, Boolean> contentieuxJudiciaire;
    public static volatile SingularAttribute<Dossier, String> partieAdverse;
    public static volatile SingularAttribute<Dossier, String> dateDeliberation;
    public static volatile SingularAttribute<Dossier, String> adresse;
    public static volatile SingularAttribute<Dossier, Date> dateEcheanceRejet;
    public static volatile SingularAttribute<Dossier, String> agentCreat;
    public static volatile SingularAttribute<Dossier, String> natureAb;

}