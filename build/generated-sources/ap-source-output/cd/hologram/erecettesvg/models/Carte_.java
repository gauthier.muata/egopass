package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ConducteurVehicule;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Carte.class)
public class Carte_ { 

    public static volatile SingularAttribute<Carte, String> numeroAutorisation;
    public static volatile SingularAttribute<Carte, ConducteurVehicule> fkConducteurVehicule;
    public static volatile SingularAttribute<Carte, String> photoProfil;
    public static volatile SingularAttribute<Carte, String> numeroAutocollan;
    public static volatile SingularAttribute<Carte, String> commune;
    public static volatile SingularAttribute<Carte, Date> dateGeneration;
    public static volatile SingularAttribute<Carte, String> numeroCpi;
    public static volatile SingularAttribute<Carte, Integer> etat;
    public static volatile SingularAttribute<Carte, Integer> nombreImpression;
    public static volatile SingularAttribute<Carte, Date> dateLivraison;
    public static volatile SingularAttribute<Carte, String> district;
    public static volatile SingularAttribute<Carte, String> id;
    public static volatile SingularAttribute<Carte, Date> dateFinValidite;

}