package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.DetailAssujettissement;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.PrevisionCredit;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailPrevisionCredit.class)
public class DetailPrevisionCredit_ { 

    public static volatile SingularAttribute<DetailPrevisionCredit, PrevisionCredit> previsionCredit;
    public static volatile SingularAttribute<DetailPrevisionCredit, BigDecimal> montant;
    public static volatile SingularAttribute<DetailPrevisionCredit, Integer> id;
    public static volatile SingularAttribute<DetailPrevisionCredit, DetailAssujettissement> detailAssujettissement;
    public static volatile SingularAttribute<DetailPrevisionCredit, Integer> etat;
    public static volatile SingularAttribute<DetailPrevisionCredit, Devise> devise;

}