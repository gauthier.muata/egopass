package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Personne;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(PersonneExemptee.class)
public class PersonneExemptee_ { 

    public static volatile SingularAttribute<PersonneExemptee, Personne> personne;
    public static volatile SingularAttribute<PersonneExemptee, Integer> id;
    public static volatile SingularAttribute<PersonneExemptee, Integer> agentCreat;
    public static volatile SingularAttribute<PersonneExemptee, Date> dateCreate;
    public static volatile SingularAttribute<PersonneExemptee, Integer> etat;

}