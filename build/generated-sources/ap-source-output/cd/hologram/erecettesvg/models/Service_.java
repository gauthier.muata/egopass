package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.TypeBienService;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Service.class)
public class Service_ { 

    public static volatile SingularAttribute<Service, String> code;
    public static volatile ListAttribute<Service, TypeBienService> typeBienServiceList;
    public static volatile SingularAttribute<Service, Service> service;
    public static volatile ListAttribute<Service, Service> serviceList;
    public static volatile SingularAttribute<Service, String> intitule;
    public static volatile SingularAttribute<Service, Short> etat;

}