package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.DetailDepotDeclaration;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.ParamTauxVignette;
import cd.hologram.erecettesvg.models.ServiceTarif;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Tarif.class)
public class Tarif_ { 

    public static volatile SingularAttribute<Tarif, String> agentMaj;
    public static volatile ListAttribute<Tarif, Assujeti> assujetiList;
    public static volatile SingularAttribute<Tarif, String> code;
    public static volatile SingularAttribute<Tarif, BigDecimal> valeur;
    public static volatile SingularAttribute<Tarif, Date> dateCreat;
    public static volatile ListAttribute<Tarif, ServiceTarif> serviceTarifList;
    public static volatile ListAttribute<Tarif, DetailDepotDeclaration> detailDepotDeclarationList;
    public static volatile SingularAttribute<Tarif, String> intitule;
    public static volatile SingularAttribute<Tarif, Short> etat;
    public static volatile SingularAttribute<Tarif, Integer> estTarifPeage;
    public static volatile SingularAttribute<Tarif, Date> dateMaj;
    public static volatile ListAttribute<Tarif, ParamTauxVignette> paramTauxVignettesList;
    public static volatile SingularAttribute<Tarif, String> typeValeur;
    public static volatile ListAttribute<Tarif, ArticleBudgetaire> articleBudgetaireList;
    public static volatile ListAttribute<Tarif, Palier> palierList;
    public static volatile SingularAttribute<Tarif, String> agentCreat;

}