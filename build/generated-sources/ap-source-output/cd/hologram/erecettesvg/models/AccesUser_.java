package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AccesUser.class)
public class AccesUser_ { 

    public static volatile SingularAttribute<AccesUser, String> agentMaj;
    public static volatile SingularAttribute<AccesUser, String> agent;
    public static volatile SingularAttribute<AccesUser, Date> dateCreat;
    public static volatile SingularAttribute<AccesUser, String> droit;
    public static volatile SingularAttribute<AccesUser, Integer> id;
    public static volatile SingularAttribute<AccesUser, String> agentCreat;
    public static volatile SingularAttribute<AccesUser, Short> etat;
    public static volatile SingularAttribute<AccesUser, Date> dateMaj;

}