package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.CarteVoucher;
import cd.hologram.erecettesvg.models.DetailVoucher;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(CarteDetailVoucher.class)
public class CarteDetailVoucher_ { 

    public static volatile SingularAttribute<CarteDetailVoucher, CarteVoucher> fkCarte;
    public static volatile SingularAttribute<CarteDetailVoucher, DetailVoucher> fkDetailVoucher;
    public static volatile SingularAttribute<CarteDetailVoucher, Integer> id;
    public static volatile SingularAttribute<CarteDetailVoucher, Date> dateCreate;
    public static volatile SingularAttribute<CarteDetailVoucher, Integer> etat;

}