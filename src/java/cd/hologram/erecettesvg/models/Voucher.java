/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author WILLY
 */
@Entity
@Table(name = "T_VOUCHER")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Voucher.findAll", query = "SELECT v FROM Voucher v"),
    @NamedQuery(name = "Voucher.findById", query = "SELECT v FROM Voucher v WHERE v.id = :id"),
    @NamedQuery(name = "Voucher.findByQte", query = "SELECT v FROM Voucher v WHERE v.qte = :qte"),
    @NamedQuery(name = "Voucher.findByPrixUnitaire", query = "SELECT v FROM Voucher v WHERE v.prixUnitaire = :prixUnitaire"),
    @NamedQuery(name = "Voucher.findByMontant", query = "SELECT v FROM Voucher v WHERE v.montant = :montant"),
    @NamedQuery(name = "Voucher.findByRemise", query = "SELECT v FROM Voucher v WHERE v.remise = :remise"),
    @NamedQuery(name = "Voucher.findByMontantFinal", query = "SELECT v FROM Voucher v WHERE v.montantFinal = :montantFinal"),
    @NamedQuery(name = "Voucher.findByEtat", query = "SELECT v FROM Commande v WHERE v.etat = :etat")})
public class Voucher implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "FK_COMMANDE", referencedColumnName = "ID")
    @ManyToOne
    private Commande fkCommande;
    @JoinColumn(name = "FK_SITE", referencedColumnName = "CODE")
    @ManyToOne
    private Site fkSite;
    @JoinColumn(name = "FK_TARIF", referencedColumnName = "CODE")
    @ManyToOne
    private Tarif fkTarif;
    @JoinColumn(name = "FK_AXE", referencedColumnName = "ID")
    @ManyToOne
    private Axe fkAxe;
    @Column(name = "QTE")
    private Integer qte;
    @Column(name = "PRIX_UNITAIRE")
    private BigDecimal prixUnitaire;
    @Column(name = "MONTANT")
    private BigDecimal montant;
    @Column(name = "REMISE")
    private BigDecimal remise;
    @Column(name = "MONTANT_FINAL")
    private BigDecimal montantFinal;
    @JoinColumn(name = "DEVISE", referencedColumnName = "CODE")
    @ManyToOne
    private Devise devise;
    @JoinColumn(name = "AGENT_MAJ", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentMaj;
    @Column(name = "ETAT")
    private Integer etat;

    public Voucher() {
    }

    public Voucher(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Commande getFkCommande() {
        return fkCommande;
    }

    public void setFkCommande(Commande fkCommande) {
        this.fkCommande = fkCommande;
    }

    public Site getFkSite() {
        return fkSite;
    }

    public void setFkSite(Site fkSite) {
        this.fkSite = fkSite;
    }

    public Tarif getFkTarif() {
        return fkTarif;
    }

    public void setFkTarif(Tarif fkTarif) {
        this.fkTarif = fkTarif;
    }

    public Axe getFkAxe() {
        return fkAxe;
    }

    public void setFkAxe(Axe fkAxe) {
        this.fkAxe = fkAxe;
    }

    public Integer getQte() {
        return qte;
    }

    public void setQte(Integer qte) {
        this.qte = qte;
    }

    public BigDecimal getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(BigDecimal prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public BigDecimal getRemise() {
        return remise;
    }

    public void setRemise(BigDecimal remise) {
        this.remise = remise;
    }

    public BigDecimal getMontantFinal() {
        return montantFinal;
    }

    public void setMontantFinal(BigDecimal montantFinal) {
        this.montantFinal = montantFinal;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }

    public Agent getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(Agent agentMaj) {
        this.agentMaj = agentMaj;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Voucher other = (Voucher) obj;
        return true;
    }

    @Override
    public String toString() {
        return "Voucher{" + "id=" + id + ", fkCommande=" + fkCommande + ", fkSite=" + fkSite + ", fkTarif=" + fkTarif + ", qte=" + qte + ", prixUnitaire=" + prixUnitaire + ", montant=" + montant + ", remise=" + remise + ", montantFinal=" + montantFinal + ", devise=" + devise + ", agentMaj=" + agentMaj + ", etat=" + etat + '}';
    }
    

}
