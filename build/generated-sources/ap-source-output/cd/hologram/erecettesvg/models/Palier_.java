package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.Retribution;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.Unite;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Palier.class)
public class Palier_ { 

    public static volatile SingularAttribute<Palier, String> agentMaj;
    public static volatile SingularAttribute<Palier, Integer> code;
    public static volatile SingularAttribute<Palier, Date> dateCreat;
    public static volatile SingularAttribute<Palier, Unite> unite;
    public static volatile SingularAttribute<Palier, BigDecimal> taux;
    public static volatile SingularAttribute<Palier, Short> multiplierValeurBase;
    public static volatile SingularAttribute<Palier, String> fkUsage;
    public static volatile ListAttribute<Palier, Retribution> retributionList;
    public static volatile SingularAttribute<Palier, BigDecimal> borneInferieure;
    public static volatile SingularAttribute<Palier, BigDecimal> borneSuperieure;
    public static volatile SingularAttribute<Palier, Short> etat;
    public static volatile SingularAttribute<Palier, Devise> devise;
    public static volatile SingularAttribute<Palier, Date> dateMaj;
    public static volatile SingularAttribute<Palier, String> fkTypeBien;
    public static volatile SingularAttribute<Palier, ArticleBudgetaire> articleBudgetaire;
    public static volatile SingularAttribute<Palier, Tarif> tarif;
    public static volatile SingularAttribute<Palier, String> fkEntiteAdministrative;
    public static volatile SingularAttribute<Palier, String> typeTaux;
    public static volatile SingularAttribute<Palier, FormeJuridique> typePersonne;
    public static volatile SingularAttribute<Palier, String> agentCreat;

}