package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(PalierDroit.class)
public class PalierDroit_ { 

    public static volatile SingularAttribute<PalierDroit, String> agentMaj;
    public static volatile SingularAttribute<PalierDroit, Date> dateCreat;
    public static volatile SingularAttribute<PalierDroit, BigDecimal> borneInf;
    public static volatile SingularAttribute<PalierDroit, String> droit;
    public static volatile SingularAttribute<PalierDroit, Integer> id;
    public static volatile SingularAttribute<PalierDroit, String> agentCreat;
    public static volatile SingularAttribute<PalierDroit, Short> etat;
    public static volatile SingularAttribute<PalierDroit, BigDecimal> borneSup;
    public static volatile SingularAttribute<PalierDroit, Date> dateMaj;

}