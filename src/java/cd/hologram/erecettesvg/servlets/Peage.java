/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.*;
import static cd.hologram.erecettesvg.business.AssujettissementBusiness.getComplements;
import cd.hologram.erecettesvg.constants.*;
import static cd.hologram.erecettesvg.constants.TaxationOfficeConst.ParamName.codeAssujetti;
import cd.hologram.erecettesvg.etats.Anchor;
import cd.hologram.erecettesvg.models.*;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.pojo.PeriodiciteData;
import cd.hologram.erecettesvg.util.*;
import cd.hologram.erecettesvg.models.Assujettissement;
import cd.hologram.erecettesvg.pojo.CartePrint;
import cd.hologram.erecettesvg.pojo.TicketPeagePrint;
import cd.hologram.erecettesvg.properties.PropertiesConfig;
import static cd.hologram.erecettesvg.util.Tools.generateQRCode;
import com.google.gson.JsonObject;
import static com.sun.xml.wss.impl.dsig.WSSPolicyConsumerImpl.printDocument;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.*;

/**
 *
 * @author emmanuel.tsasa
 */
@WebServlet(name = "peage", urlPatterns = {"/peage_servlet"})
public class Peage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    static List<Devise> devises = null;
    static List<Unite> unites = null;

    static List<ComplementBien> infoComplementaires;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case AssujettissementConst.Operation.LOAD_TYPE_BIEN:
                result = loadTypeBiens();
                break;
            case AssujettissementConst.Operation.LOAD_TYPE_COMPLEMENT_BIEN:
                result = loadTypeComplementBiens(request);
                break;
            case AssujettissementConst.Operation.SAVE_BIEN:
                result = createBien(request);
                break;
            case AssujettissementConst.Operation.LOAD_BIENS_PERSONNE:
                result = loadBiensPersonne(request);
                break;
            case AssujettissementConst.Operation.GET_BIENS_PERSONNE:
                result = loadBiens(request);
                break;
            case AssujettissementConst.Operation.DESACTIVATE_BIEN_ACQUISITION:
                result = desactivateBienAcquisition(request);
                break;
            case AssujettissementConst.Operation.SAVE_BIEN_LOCATION:
                result = createAcquisition(request);
                break;
            case AssujettissementConst.Operation.LOAD_ARTICLES_BUDGETAIRES:
                result = loadArticlesBudgetaires(request);
                break;
            case AssujettissementConst.Operation.LOAD_DATA_ASSUJETTISSEMENT:
                result = loadPeriodiciteAndTarifs(request);
                break;
            case AssujettissementConst.Operation.GENERATE_PERIODES_DECLARATIONS:
                result = getPeriodesDeclarations(request);
                break;
            case AssujettissementConst.Operation.SAVE_ASSUJETTISSEMENT:
                result = createAssujettissement(request);
                break;
            case AssujettissementConst.Operation.GET_AB_ASSUJETTI:
                result = getABAssujetti(request);
                break;
            case AssujettissementConst.Operation.SAVE_RETRAIT:
                result = saveRetraitDeclaration(request);
                break;
            case AssujettissementConst.Operation.GET_BIEN_FOR_EDITION:
                result = getBienForEdition(request);
                break;
            case AssujettissementConst.Operation.LOAD_ARTICLE_BUDGETAIRE_ASSUJETTISSABLE:
                result = loadArticlesBudgetairesAssujettissable(request);
                break;
            case GeneralConst.ParamName.LOAD_ASSUJETTI:
                result = loadAssujettisPrevision(request);
                break;
            case GeneralConst.ParamName.LOAD_ARTICLE_OF_ASSUJETTI:
                result = loadArticlesBudgetairesOfAssujetti(request);
                break;
            case "loadArtcleOfAssujettiV2":
                result = loadArticlesBudgetairesOfAssujettiV2(request);
                break;
            case "loadBiensRemplacement":
                result = getListBiensRemplacementByAssujetti(request);
                break;
            case "loadSousProvisions":
                result = getListSousProvisionByAssujetti(request);
                break;
            case "loadCredits":
                result = getListCreditsByAssujetti(request);
                break;
            case "editPrepaiement":
                result = editPrepaiement(request);
                break;
            case "saveRenouvellementSouProvision":
                result = saveRenouvellementSouProvision(request);
                break;
            case "suspendreBienPrevisionCredit":
                result = suspendreBienPrevisionCredit(request);
                break;
            case "remplacerBienPrevisionCredit":
                result = remplacerBienPrevisionCredit(request);
                break;
            case "activateDetailProvisionCredt":
                result = activateDetailProvisionCredt(request);
                break;
            case "loadBiensAssujettiExempte":
                result = getListBienAssujettiExempte(request);
                break;
            case "activateExemption":
                result = activateExemption(request);
                break;
            case "desactivateExemption":
                result = desactivateExemption(request);
                break;
            case "activateAllExemption":
                result = activateAllExemption(request);
                break;
            case "desactivateAllExemption":
                result = desactivateAllExemption(request);
                break;
            case "printRapportProduction":
                result = printRapportProduction(request);
                break;
            case "loadSitePeage":
                result = getListSitePeage(request);
                break;
            case "loadTarifBySite":
                result = getListTarifBySitePeage(request);
                break;
            case "loadPercepteurBySite":
                result = getListPercepteurBySitePeage(request);
                break;

            case "loadControleurs":
                result = loadControleurs(request);
                break;

            case GeneralConst.ParamName.GET_DETAIL_ASSUJETTISSEMENT:
                result = getDetailsAssujettis(request);
                break;
            case AssujettissementConst.Operation.LOAD_TAXES_PEAGES:
                result = loadTaxesPeages(request);
                break;
            case GeneralConst.ParamName.SAVE_PREVISION_CREDIT:
                result = savePrevisionCredit(request);
                break;
            case AssujettissementConst.Operation.SAVE_ASSUJETTISSEMENT_PEAGE:
                result = saveAssujettissementPeage(request);
                break;
            case IdentificationConst.Operation.LOAD_ASSUJETTIS:
                result = loadAssujettis(request);
                break;
            case PeageConst.Operation.EXEMPTE_ASSUJETTI:
                result = savePersonneExemptes(request);
                break;
            case PeageConst.Operation.DISABLE_PERSONNE_EXEMPTE:
                result = disabledPersonneExemptes(request);
                break;
            case PeageConst.Operation.LOAD_PEAGES:
                result = loadPeages(request);
                break;
            case "loadTicketCanceled":
                result = loadTicketCanceled(request);
                break;
            case "canceledTicket":
                result = canceledTicket(request);
                break;
            case "loadCommandeVoucher":
                result = loadCommandeVoucher(request);
                break;
            case "updateVoucher":
                result = updateVoucher(request);
                break;
            case "loadCompteBancaireOfBank":
                result = loadCompteBancaireOfBank(request);
                break;
            case "getPreuvePaiement":
                result = getPreuvePaiement(request);
                break;
            case "validerPaiement":
                result = validerPaiementCommande(request);
                break;
            case "loadCommandeCarte":
                result = loadCommandeCarte(request);
                break;
            case "traiteCmdCarte":
                result = traiterCmdCarte(request);
                break;
            case "getDetailCommandeCarte":
                result = getDetailCmdCarte(request);
                break;
            case "printCarteNFC":
                result = imprimerCarteNFC(request);
                break;
            case "disableCarte":
                result = disableCarte(request);
                break;
            case "loadCommandeGopass":
                result = loadCommandeGopass(request);
                break;
            case "getDetailCommandeGopass":
                result = getDetailGopasse(request);
                break;

        }
        out.print(result);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public String saveRetraitDeclaration(HttpServletRequest request) {

        String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
        String idUser = request.getParameter(GeneralConst.ID_USER);
        String listRetrait = request.getParameter(AssujettissementConst.ParamName.LISTE_RETRAIT);
        String numDeclaration = request.getParameter(AssujettissementConst.ParamName.NUMERO_DECLARATION);
        String nomRequerant = request.getParameter(AssujettissementConst.ParamName.NOM_REQUERANT);
        String codeAB = request.getParameter(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE);

        List<RetraitDeclaration> retraitDeclarations = new ArrayList<>();
        String returnValue;

        RetraitDeclaration retraitdeclaration = AssujettissementBusiness.getRetraitDeclarationByCode(numDeclaration);

        if (retraitdeclaration != null) {
            return GeneralConst.Number.TWO;
        }

        try {
            JSONArray jsonRetrait = new JSONArray(listRetrait);

            for (int i = 0; i < jsonRetrait.length(); i++) {

                //récuper la valeur de base dans assuj de la periode
                RetraitDeclaration declaration = new RetraitDeclaration();

                JSONObject jsonobject = jsonRetrait.getJSONObject(i);
                declaration.setFkPeriode(jsonobject.getString("periode"));
                declaration.setMontant(new BigDecimal(Double.valueOf(jsonobject.getString("montant"))));
                declaration.setDevise(jsonobject.getString("devise"));
                declaration.setRequerant(nomRequerant);
                declaration.setFkAssujetti(codePersonne);
                declaration.setCodeDeclaration(numDeclaration);
                declaration.setFkAb(codeAB);
                declaration.setFkAgentCreate(idUser);
                declaration.setDateCreate(new Date());
                retraitDeclarations.add(declaration);
            }

            boolean result = false;//AssujettissementBusiness.saveRetraitDeclaration(retraitDeclarations);

            if (result) {
                returnValue = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                returnValue = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (JSONException | NumberFormatException e) {

            CustumException.LogException(e);
            returnValue = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return returnValue;
    }

    public String getABAssujetti(HttpServletRequest request) {

        String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
        JSONObject allDataAssujettisement = new JSONObject();
        List<JSONObject> dataAssujettisement = new ArrayList<>();
        List<JSONObject> dataAB = new ArrayList<>();

        try {

            List<Assujeti> assujettissements = AssujettissementBusiness.getAssujettissementByPersonne(codePersonne);

            List<String> codeABList = new ArrayList<>();

            for (Assujeti assujeti : assujettissements) {

                JSONObject jsonAssujetti = new JSONObject();

                if (codeABList.contains(assujeti.getArticleBudgetaire().getCode())) {
                    continue;
                }

                codeABList.add(assujeti.getArticleBudgetaire().getCode());

                jsonAssujetti.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCode());
                jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getIntitule());
                jsonAssujetti.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCodeOfficiel());

                dataAB.add(jsonAssujetti);

            }

            for (Assujeti assujeti : assujettissements) {

                JSONObject jsonAssujetti = new JSONObject();
                List<JSONObject> jsonPeriodes = new ArrayList<>();

                jsonAssujetti.put(AssujettissementConst.ParamName.ID_ASSUJETTISSEMENT, assujeti.getId());
                jsonAssujetti.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                        assujeti.getArticleBudgetaire() == null
                                ? GeneralConst.EMPTY_STRING : assujeti.getArticleBudgetaire().getCode());

                Bien bien;

                if (assujeti.getBien() != null) {

                    bien = assujeti.getBien();

                    jsonAssujetti.put(AssujettissementConst.ParamName.ID_BIEN, bien.getId());
                    jsonAssujetti.put(AssujettissementConst.ParamName.INTITULE_BIEN, bien.getIntitule().concat(GeneralConst.SPACE).concat(GeneralConst.BRAKET_OPEN).concat(assujeti.getBien().getTypeBien().getIntitule()).concat(GeneralConst.BRAKET_CLOSE));

                    if (bien.getFkAdressePersonne() != null) {

                        jsonAssujetti.put(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                bien.getFkAdressePersonne().getAdresse().toString());

                    } else {

                        jsonAssujetti.put(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                GeneralConst.EMPTY_STRING);
                    }
                }

                boolean isPalier = assujeti.getArticleBudgetaire().getPalier();

                Palier palier = AssujettissementBusiness.getPalierByAbTarifAndFJuridique(
                        assujeti.getArticleBudgetaire().getCode().trim(),
                        assujeti.getTarif().getCode().trim(),
                        assujeti.getPersonne().getFormeJuridique().getCode().trim(), assujeti.getValeur().floatValue(), isPalier);

                BigDecimal taux = new BigDecimal(BigInteger.ZERO);

                if (palier != null) {

                    if (palier.getTypeTaux().equals("F")) {

                        taux = palier.getMultiplierValeurBase() == 0 ? palier.getTaux()
                                : palier.getTaux().multiply(assujeti.getValeur());

                    } else {

                        taux = assujeti.getValeur().multiply(palier.getTaux()).divide(BigDecimal.valueOf(GeneralConst.Numeric.CENT));
                    }

                }

                jsonAssujetti.put(AssujettissementConst.ParamName.TARIF, assujeti.getTarif().getIntitule());
                jsonAssujetti.put(AssujettissementConst.ParamName.MONTANT, taux);
                jsonAssujetti.put(AssujettissementConst.ParamName.DEVISE, palier.getDevise().getCode());

                for (PeriodeDeclaration periode : assujeti.getPeriodeDeclarationList()) {

                    if (!assujeti.getEtat() && periode.getNoteCalcul() != null) {
                        continue;
                    }

                    RetraitDeclaration declaration = AssujettissementBusiness.getRetraitDeclarationByPeriode(periode.getId().toString());

                    if (declaration == null) {

                        JSONObject jsonPeriode = new JSONObject();
                        jsonPeriode.put(AssujettissementConst.ParamName.PERIODE_ID, periode.getId());
                        jsonPeriode.put(AssujettissementConst.ParamName.PERIODE, Tools.getPeriodeIntitule(periode.getDebut(),
                                periode.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode()));

                        jsonPeriodes.add(jsonPeriode);
                    }

                }

                Unite unite = AssujettissementBusiness.getUnite(palier.getUnite().getCode());
                TypeComplement complement = AssujettissementBusiness.getTypeComplementbyComplementBien(assujeti.getComplementBien());

                if (unite != null && complement != null) {

                    String valBase = !unite.getIntitule().equalsIgnoreCase("Montant") ? unite.getIntitule() : palier.getDevise().getCode();
                    jsonAssujetti.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN, complement.getIntitule() + GeneralConst.TWO_POINTS + assujeti.getValeur() + GeneralConst.SPACE + valBase);

                } else {

                    jsonAssujetti.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN, GeneralConst.EMPTY_STRING);
                }

                jsonAssujetti.put(AssujettissementConst.ParamName.LIST_PERIODES_DECLARATIONS, jsonPeriodes);

                dataAssujettisement.add(jsonAssujetti);

            }

            allDataAssujettisement.put("dataAB", dataAB.toString());
            allDataAssujettisement.put("dataAssujettisement", dataAssujettisement.toString());

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return allDataAssujettisement.toString();
    }

    public String loadTypeBiens() {

        List<JSONObject> jsonTypeBiens = new ArrayList<>();

        try {

            List<TypeBien> typeBiens = AssujettissementBusiness.getTypeBien();

            for (TypeBien typeBien : typeBiens) {
                JSONObject jsonTypeBien = new JSONObject();

                jsonTypeBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN,
                        typeBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeBien.getCode());

                jsonTypeBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                        typeBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeBien.getIntitule());

                jsonTypeBien.put(AssujettissementConst.ParamName.EST_CONTRACTUEL,
                        typeBien == null
                                ? GeneralConst.EMPTY_STRING
                                : (typeBien.getContractuel())
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO);

                jsonTypeBiens.add(jsonTypeBien);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonTypeBiens.toString();
    }

    public String loadBiensPersonne(HttpServletRequest request) {

        List<JSONObject> jsonBiens = new ArrayList<>();
        JSONObject jsonAssujettissement = new JSONObject();

        try {

            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String typeBienMobiliers = propertiesConfig.getProperty("TYPES_BIENS_MOBILIERS");
            String immatriculationTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_IMMATRICULATION");
            String numeroChassisTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_CHASSIS");

            List<Acquisition> acquisitions = AssujettissementBusiness.getBiensMobiliersOfPersonneV2(codePersonne.trim());

            for (Acquisition acquisition : acquisitions) {

                JSONObject jsonBien = new JSONObject();

                Bien bien = acquisition.getBien();

                jsonBien.put(AssujettissementConst.ParamName.ID_ACQUISITION,
                        bien == null ? GeneralConst.EMPTY_STRING : acquisition.getId());

                jsonBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN,
                        bien == null ? GeneralConst.EMPTY_STRING : bien.getTypeBien() == null
                                        ? GeneralConst.EMPTY_STRING : bien.getTypeBien().getCode());

                jsonBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getTypeBien() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getTypeBien().getIntitule().toUpperCase());

                jsonBien.put(AssujettissementConst.ParamName.ID_BIEN,
                        bien == null ? GeneralConst.EMPTY_STRING : bien.getId());

                jsonBien.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN,
                        bien.getDescription() == null ? GeneralConst.EMPTY_STRING : bien.getDescription());

                jsonBien.put(AssujettissementConst.ParamName.CHAINE_ADRESSE,
                        bien == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne() == null
                                        ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getAdresse().toString());

                jsonBien.put(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE,
                        bien == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne() == null
                                        ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getCode());

                jsonBien.put(AssujettissementConst.ParamName.PROPRIETAIRE, acquisition.getProprietaire() == null
                        ? false : acquisition.getProprietaire());

                if (acquisition.getBien().getFkTarif() != null) {

                    Tarif tarif = TaxationBusiness.getTarifByCode(acquisition.getBien().getFkTarif());

                    jsonBien.put("tarifCode", tarif.getCode());
                    jsonBien.put("tarifName", tarif.getIntitule().toUpperCase());

                } else {
                    jsonBien.put("tarifCode", GeneralConst.EMPTY_STRING);
                    jsonBien.put("tarifName", GeneralConst.EMPTY_STRING);
                }

                String responsable = GeneralConst.MY_SELF;
                String intituleBien = (bien == null) ? GeneralConst.EMPTY_STRING : bien.getIntitule().toUpperCase();

                String immatriculation = GeneralConst.EMPTY_STRING;
                List<ComplementBien> complementBienList = bien.getComplementBienList();

                String valUnity = GeneralConst.EMPTY_STRING;

                /*if (complementBienList != null) {
                    
                 for (ComplementBien cb : complementBienList) {
                        
                 TypeComplementBien tcb = cb.getTypeComplement();
                        
                 if (tcb.getComplement().getCode().equals(immatriculationTC) | tcb.getComplement().getCode().equals(numeroChassisTC)) {
                 immatriculation = cb.getValeur();
                 break;
                 }
                        
                        
                        
                 }
                 }*/
                String infosComplementaire = GeneralConst.EMPTY_STRING;

                for (ComplementBien cb : complementBienList) {

                    if (cb.getDevise() != null && !cb.getDevise().isEmpty()) {

                        switch (cb.getDevise()) {
                            case GeneralConst.Devise.DEVISE_CDF:
                            case GeneralConst.Devise.DEVISE_USD:

                                valUnity = cb.getDevise();

                                break;
                            default:
                                Unite unite = TaxationBusiness.getUnitebyCode(cb.getDevise());
                                valUnity = unite != null ? unite.getIntitule() : "";

                                break;
                        }

                    } else {
                        valUnity = GeneralConst.EMPTY_STRING;
                    }

                    ValeurPredefinie valeurPredefinie = new ValeurPredefinie();
                    String txtValeurPredefinie = GeneralConst.EMPTY_STRING;

                    if (infosComplementaire.isEmpty()) {

                        if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                            valeurPredefinie = AssujettissementBusiness.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                            txtValeurPredefinie = valeurPredefinie == null ? "" : valeurPredefinie.getValeur().toUpperCase();

                            infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                        } else {

                            infosComplementaire = cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                        }

                    } else {

                        if (cb.getTypeComplement().getComplement().getValeurPredefinie()) {

                            valeurPredefinie = AssujettissementBusiness.getValeurPredefinieByCode(cb.getValeur(), cb.getTypeComplement().getComplement().getCode());
                            txtValeurPredefinie = valeurPredefinie == null ? "" : valeurPredefinie.getValeur().toUpperCase();

                            infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(txtValeurPredefinie).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");

                        } else {
                            infosComplementaire = infosComplementaire += cb.getTypeComplement().getComplement().getIntitule().concat(GeneralConst.TWO_POINTS).concat(cb.getValeur()).concat(GeneralConst.SPACE).concat(valUnity).concat("<br/>");
                        }

                    }

                }

                jsonBien.put("complement", infosComplementaire);

                DetailAssujettissement da = AssujettissementBusiness.getDetailAssujettissementByAssujettiAndBien(codePersonne.trim(), bien.getId());

                if (da == null) {
                    jsonBien.put(AssujettissementConst.ParamName.HASTARIF, GeneralConst.Number.ZERO);
                } else {
                    jsonBien.put(AssujettissementConst.ParamName.HASTARIF, GeneralConst.Number.ONE);

                    Tarif tarif = da.getTarif();

                    JSONObject jsonDa = new JSONObject();
                    jsonDa.put(AssujettissementConst.ParamName.CODE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getCode());
                    jsonDa.put(AssujettissementConst.ParamName.INTITULE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getIntitule());
                    jsonDa.put(AssujettissementConst.ParamName.TAUX_AB, da.getTaux() == null ? GeneralConst.Numeric.ZERO : da.getTaux());
                    jsonDa.put(AssujettissementConst.ParamName.DEVISE, da.getDevise() == null ? GeneralConst.EMPTY_STRING : da.getDevise().getCode());

                    jsonBien.put(AssujettissementConst.ParamName.DATA_TARIF, jsonDa);
                }

                jsonBien.put(AssujettissementConst.ParamName.IMMATRICULATION, immatriculation);
                jsonBien.put(AssujettissementConst.ParamName.RESPONSABLE, responsable);
                jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, intituleBien);
                jsonBiens.add(jsonBien);
            }

            jsonAssujettissement.put(AssujettissementConst.ParamName.LIST_BIENS, jsonBiens);

        } catch (JSONException | NumberFormatException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAssujettissement.toString();
    }

    public String loadTypeComplementBiens(HttpServletRequest request) {

        JSONObject data = new JSONObject();

        try {

            List<JSONObject> jsonTypeComplementBiens = new ArrayList<>();

            infoComplementaires = null;

            String codeTypeBien = request.getParameter(AssujettissementConst.ParamName.CODE_TYPE_BIEN);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);

            if (idBien.equals(GeneralConst.EMPTY_STRING)) {
                data.put("bien", GeneralConst.EMPTY_STRING);
            } else {
                List<ComplementBien> complementBienList = AssujettissementBusiness.getListComplementBien(idBien);
                if (complementBienList == null || complementBienList.isEmpty()) {
                    data.put("bien", GeneralConst.EMPTY_STRING);
                } else {
                    data.put("bien", getValeurComplementBiens(complementBienList));
                }
            }

            jsonTypeComplementBiens = getTypeComplementBiens(codeTypeBien);

            data.put("typeComplementBienList", jsonTypeComplementBiens);

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return data.toString();
    }

    public List<JSONObject> getTypeComplementBiens(String codeTypeBien) {

        List<JSONObject> jsonTypeComplementBiens = new ArrayList<>();

        try {
            List<TypeComplementBien> typeComplementBiens = AssujettissementBusiness
                    .getTypeComplementBienByTypeBien(codeTypeBien);

            if (typeComplementBiens != null) {
                int counter = 0;
                for (TypeComplementBien typeComplementBien : typeComplementBiens) {

                    JSONObject jsonTypeComplementBien = new JSONObject();
                    TypeComplement typeComplement = typeComplementBien.getComplement();

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getCode());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.LIBELLE_TYPE_COMPLEMENT,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getIntitule());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.OBJET_INTERACTION,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getObjetInteraction());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.VALEUR_PREDEFINIE,
                            typeComplement == null ? GeneralConst.EMPTY_STRING : typeComplement.getValeurPredefinie());

                    jsonTypeComplementBien.put(AssujettissementConst.ParamName.INPUT_VALUE,
                            createControl(typeComplementBien, counter));

                    jsonTypeComplementBiens.add(jsonTypeComplementBien);
                    counter++;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }
        return jsonTypeComplementBiens;
    }

    public String createControl(final TypeComplementBien typeComplementBien, int counter) {

        String CONTROL_ID = GeneralConst.PREFIX_IDENTIFIANT_KEY.concat(String.valueOf(counter));
        String value = GeneralConst.EMPTY_STRING;

        TypeComplement typeComplement = typeComplementBien.getComplement();

        final String OBJET_INTERACTION = (typeComplement == null)
                ? GeneralConst.EMPTY_STRING : typeComplement.getObjetInteraction();

        final String CONTROL_VALUE = (typeComplement == null)
                ? GeneralConst.EMPTY_STRING : typeComplement.getIntitule();

        final boolean HAS_VALUES_LIST = (typeComplement == null)
                ? false : typeComplement.getValeurPredefinie();

        if (HAS_VALUES_LIST) {

            value = getControlList(
                    CONTROL_ID,
                    CONTROL_VALUE,
                    typeComplementBien,
                    GeneralConst.EMPTY_STRING);

        } else {

            switch (OBJET_INTERACTION) {
                case AssujettissementConst.ObjetInteraction.EMPTY:
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            AssujettissementConst.TypeData.TYPE_TEXT,
                            typeComplementBien);
                    break;
                case AssujettissementConst.ObjetInteraction.DATE:
                    value = getInputControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            AssujettissementConst.TypeData.TYPE_DATE,
                            typeComplementBien);
                    break;
                case AssujettissementConst.ObjetInteraction.MONTANT:
                    value = getBoxControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            AssujettissementConst.TypeData.TYPE_NUMBER,
                            typeComplementBien, true);
                    break;
                case AssujettissementConst.ObjetInteraction.NOMBRE:
                    value = getBoxControl(
                            CONTROL_ID,
                            CONTROL_VALUE,
                            AssujettissementConst.TypeData.TYPE_NUMBER,
                            typeComplementBien, false);
                    break;
            }
        }

        return value;
    }

    public static String getBoxControl(String controlId, String controlValue, String type, TypeComplementBien typeComplementBien, boolean hasDevise) {

        String selectId, options = GeneralConst.EMPTY_STRING;

        if (hasDevise) {

            selectId = controlId + GeneralConst.DASH_SEPARATOR + GeneralConst.FIRST_LETTRE_DEVISE;

            if (devises == null) {
                devises = TaxationBusiness.getListAllDevises();
            }

            if (devises != null) {
                options = "<option value=\"0\">--- Devise ---</option>";
                for (Devise devise : devises) {
                    options += String.format(AssujettissementConst.ParamControl.ITEM_OPTION,
                            devise.getCode(),
                            GeneralConst.EMPTY_STRING,
                            devise.getIntitule());
                }
            }

        } else {

            selectId = controlId + GeneralConst.DASH_SEPARATOR + GeneralConst.FIRST_LETTRE_UNITE;

            if (unites == null) {
                unites = TaxationBusiness.getListAllUnites();
            }

            if (unites != null) {
                options = "<option value=\"0\">--- Unité ---</option>";
                for (Unite unite : unites) {
                    options += String.format(AssujettissementConst.ParamControl.ITEM_OPTION,
                            unite.getCode(),
                            GeneralConst.EMPTY_STRING,
                            unite.getIntitule());
                }
            }

        }

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.Number.ZERO
                : complementData.keyName);

        String value = String.format(AssujettissementConst.ParamControl.BOX_CONTROL,
                controlId,
                controlValue,
                requiredValue,
                type,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.keyName == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.keyName),
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.complement == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.complement.getValeur()),
                selectId,
                options);

        return value;
    }

    public static String getControlList(String controlId, String controlValue, TypeComplementBien typeComplementBien, final String objetInteraction) {

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String options = String.format(AssujettissementConst.ParamControl.ITEM_OPTION,
                GeneralConst.Number.ZERO,
                GeneralConst.EMPTY_STRING,
                GeneralConst.TEXT_SELECTED_VALUE);

        TypeComplement typeComplement = (typeComplementBien == null) ? null : typeComplementBien.getComplement();
        if (typeComplement != null) {
            List<ValeurPredefinie> valeurPredefinies = typeComplement.getValeurPredefinieList();
            if (valeurPredefinies != null) {
                for (ValeurPredefinie valeurPredefinie : valeurPredefinies) {
                    options += String.format(AssujettissementConst.ParamControl.ITEM_OPTION,
                            valeurPredefinie.getCode(),
                            selectedOption(complementData == null
                                            ? null
                                            : complementData.complement, valeurPredefinie.getCode()),
                            valeurPredefinie.getValeur().toUpperCase());
                }
            }
        }

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.Number.ZERO
                : complementData.keyName);

        String value = String.format(AssujettissementConst.ParamControl.COMPLEMENT_LIST,
                controlId,
                controlValue,
                requiredValue,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING : complementData.keyName,
                options);

        return value;
    }

    public static class ComplementData {

        public String keyName;
        public ComplementBien complement;
    }

    public static ComplementData getKeyNameComplement(TypeComplementBien typeComplementBien) {

        ComplementData complementData = new ComplementData();

        complementData.complement = getCorrespondingComplement(typeComplementBien);

        String idComplement = (complementData.complement == null)
                ? GeneralConst.Number.ZERO
                : complementData.complement.getId();

        TypeComplement typeComplement = typeComplementBien.getComplement();

        complementData.keyName = idComplement
                .concat(GeneralConst.DASH_SEPARATOR)
                .concat(typeComplementBien.getCode())
                .concat(GeneralConst.DASH_SEPARATOR)
                .concat(typeComplement == null
                                ? GeneralConst.Number.ZERO
                                : (typeComplementBien.getComplement().getObligatoire()
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO));
        return complementData;
    }

    public static ComplementBien getCorrespondingComplement(TypeComplementBien typeComplementBien) {

        if (infoComplementaires != null) {
            for (ComplementBien complementBien : infoComplementaires) {
                if (complementBien.getTypeComplement().equals(typeComplementBien)) {
                    return complementBien;
                }
            }
        }

        return null;
    }

    public static String selectedOption(ComplementBien complementBien, String item) {

        String result = GeneralConst.EMPTY_STRING;
        if (complementBien != null) {
            if (complementBien.getValeur().trim().equals(item.trim())) {
                return AssujettissementConst.ParamControl.SELECTED_OPTION_ATTR;
            }
        }
        return result;
    }

    public static String construstRequiredValue(String codeTCB) {

        String requiredValue;
        String[] array = codeTCB.split(GeneralConst.DASH_SEPARATOR);

        if (array.length > 1) {
            requiredValue = array[2].equals(GeneralConst.Number.ZERO)
                    ? GeneralConst.EMPTY_STRING
                    : AssujettissementConst.ParamControl.SPAN_REQUIRED_VALUE;
        } else {
            requiredValue = GeneralConst.EMPTY_STRING;
        }
        return requiredValue;
    }

    public static String getInputControl(String controlId, String controlValue, String type, TypeComplementBien typeComplementBien) {

        ComplementData complementData = getKeyNameComplement(typeComplementBien);

        String requiredValue = construstRequiredValue(complementData == null
                ? GeneralConst.Number.ZERO
                : complementData.keyName);

        String value = String.format(AssujettissementConst.ParamControl.INPUT_CONTROL,
                controlId,
                controlValue,
                requiredValue,
                type,
                controlId,
                controlValue,
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.keyName == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.keyName),
                complementData == null ? GeneralConst.EMPTY_STRING
                        : (complementData.complement == null
                                ? GeneralConst.EMPTY_STRING
                                : complementData.complement.getValeur()));
        return value;
    }

    public String loadAdresses(HttpServletRequest request) {

        List<JSONObject> jsonAdresses = new ArrayList<>();
        try {

            String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);
            List<EntiteAdministrative> avenues = IdentificationBusiness.getEntiteAdministratives(libelle == null
                    ? GeneralConst.EMPTY_STRING
                    : libelle);

            for (EntiteAdministrative avenue : avenues) {

                JSONObject jsonAdresse = new JSONObject();
                jsonAdresse.put(IdentificationConst.ParamName.CODE_AVENUE, avenue == null
                        ? GeneralConst.EMPTY_STRING : avenue.getCode());

                jsonAdresse.put(IdentificationConst.ParamName.AVENUE, avenue == null
                        ? GeneralConst.EMPTY_STRING : avenue.getIntitule());

                if (avenue != null) {
                    EntiteAdministrative quartier = avenue.getEntiteMere();
                    jsonAdresse.put(IdentificationConst.ParamName.CODE_QUARTIER, quartier == null
                            ? GeneralConst.EMPTY_STRING : quartier.getCode());

                    jsonAdresse.put(IdentificationConst.ParamName.QUARTIER, quartier == null
                            ? GeneralConst.EMPTY_STRING : quartier.getIntitule());

                    if (quartier != null) {
                        EntiteAdministrative commune = quartier.getEntiteMere();
                        jsonAdresse.put(IdentificationConst.ParamName.CODE_COMMUNE, commune == null
                                ? GeneralConst.EMPTY_STRING : commune.getCode());

                        jsonAdresse.put(IdentificationConst.ParamName.COMMUNE, commune == null
                                ? GeneralConst.EMPTY_STRING : commune.getIntitule());

                        if (commune != null) {
                            EntiteAdministrative district = commune.getEntiteMere();
                            jsonAdresse.put(IdentificationConst.ParamName.CODE_DISTRICT, district == null
                                    ? GeneralConst.EMPTY_STRING : district.getCode());

                            jsonAdresse.put(IdentificationConst.ParamName.DISTRICT, district == null
                                    ? GeneralConst.EMPTY_STRING : district.getIntitule());

                            if (district != null) {
                                EntiteAdministrative ville = district.getEntiteMere();
                                jsonAdresse.put(IdentificationConst.ParamName.CODE_VILLE, ville == null
                                        ? GeneralConst.EMPTY_STRING : ville.getCode());

                                jsonAdresse.put(IdentificationConst.ParamName.VILLE, ville == null
                                        ? GeneralConst.EMPTY_STRING : ville.getIntitule());

                                if (ville != null) {
                                    EntiteAdministrative province = ville.getEntiteMere();
                                    jsonAdresse.put(IdentificationConst.ParamName.CODE_PROVINCE, province == null
                                            ? GeneralConst.EMPTY_STRING : province.getCode());

                                    jsonAdresse.put(IdentificationConst.ParamName.PROVINCE, province == null
                                            ? GeneralConst.EMPTY_STRING : province.getIntitule());
                                }
                            }
                        }
                    }
                }
                jsonAdresses.add(jsonAdresse);
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAdresses.toString();
    }

    public String createBien(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;

        try {
            String codeTypeBien = request.getParameter(AssujettissementConst.ParamName.CODE_TYPE_BIEN);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String intituleBien = request.getParameter(AssujettissementConst.ParamName.INTITULE_BIEN);
            String descriptionBien = request.getParameter(AssujettissementConst.ParamName.DESCRIPTION_BIEN);
            String dateAcquisition = request.getParameter(AssujettissementConst.ParamName.DATE_ACQUISITION);
            String codeAP = request.getParameter(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE);

            String idUser = request.getParameter(GeneralConst.ID_USER);

            JSONArray jsonComplementBiens = new JSONArray(request.getParameter(AssujettissementConst.ParamName.COMPLEMENTS_BIEN));

            Bien bien = new Bien();
            bien.setDescription(descriptionBien);
            bien.setIntitule(intituleBien);
            bien.setTypeBien(new TypeBien(codeTypeBien));
            bien.setFkAdressePersonne(codeAP.equals("") ? null : new AdressePersonne(codeAP));

            Acquisition acquisition = new Acquisition();
            acquisition.setDateAcquisition(dateAcquisition);
            acquisition.setPersonne(new Personne(codePersonne));

            List<ComplementBien> complementBiens = getComplementBiens(jsonComplementBiens);

            if (idBien != null && !idBien.equals(GeneralConst.EMPTY_STRING)) {
                bien.setId(idBien);
                if (AssujettissementBusiness.updateBien(bien, acquisition, complementBiens, Integer.valueOf(idUser))) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                if (AssujettissementBusiness.saveBien(bien, acquisition, complementBiens, Integer.valueOf(idUser))) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public List<ComplementBien> getComplementBiens(JSONArray complementsArray) {

        List<ComplementBien> complementBiens = new ArrayList<>();
        try {
            for (int i = 0; i < complementsArray.length(); i++) {
                JSONObject jsonobject = complementsArray.getJSONObject(i);
                String id = jsonobject.getString(AssujettissementConst.ParamName.ID_COMPLEMENT_BIEN);
                String code = jsonobject.getString(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT_BIEN);
                String valeur = jsonobject.getString(AssujettissementConst.ParamName.VALEUR_COMPLEMENT_BIEN);
                String unite = jsonobject.getString(AssujettissementConst.ParamName.UNITE_DEVISE);

                ComplementBien complementBien = new ComplementBien();
                complementBien.setId(id.equals(GeneralConst.Number.ZERO) ? GeneralConst.EMPTY_STRING : id);
                complementBien.setTypeComplement(new TypeComplementBien(code));
                complementBien.setValeur(valeur);
                complementBien.setDevise(unite.equals(GeneralConst.EMPTY_STRING) ? null : unite);
                complementBiens.add(complementBien);
            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return complementBiens;
    }

    public String loadBiens(HttpServletRequest request) {

        List<JSONObject> jsonBiens = new ArrayList<>();
        JSONObject jsonAssujettissement = new JSONObject();

        try {

            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);

            List<Acquisition> acquisitions = AssujettissementBusiness.getBiensByIdBienAndPersonne(codePersonne, idBien);

            for (Acquisition acquisition : acquisitions) {
                JSONObject jsonBien = new JSONObject();

                jsonBien.put(AssujettissementConst.ParamName.ID_ACQUISITION,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getId());

                jsonBien.put(AssujettissementConst.ParamName.CHAINE_ADRESSE,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getFkAdressePersonne() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getFkAdressePersonne().getAdresse().toString());

                jsonBien.put(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getFkAdressePersonne() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : acquisition.getBien().getFkAdressePersonne().getCode());

                jsonBien.put(AssujettissementConst.ParamName.ID_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getId());

                jsonBien.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getDescription());

                jsonBien.put(AssujettissementConst.ParamName.DATE_ACQUISITION,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getDateAcquisition());

                jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN,
                        acquisition.getBien() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getBien().getIntitule());

                String responsable = GeneralConst.MY_SELF;
                String intituleBien = acquisition.getBien() == null
                        ? GeneralConst.EMPTY_STRING
                        : acquisition.getBien().getIntitule();

                jsonBien.put(AssujettissementConst.ParamName.CODE_PERSONNE,
                        acquisition.getPersonne() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getPersonne().getCode());

                jsonBien.put(AssujettissementConst.ParamName.PERSONNE_ACQUIT,
                        acquisition.getPersonne() == null
                                ? GeneralConst.EMPTY_STRING
                                : acquisition.getPersonne().toString());

                Bien bien = acquisition.getBien();
                List<JSONObject> jsonListComplement = new ArrayList<>();

                if (bien != null) {

                    List<ComplementBien> listComplementBien = new ArrayList<>();

                    if (!acquisition.getProprietaire()) {
                        Personne personne = AssujettissementBusiness.getResponsableBien(bien.getId());
                        if (personne != null) {
                            responsable = personne.toString();
                        }
                        intituleBien += " <br/>(En location)";
                    }
                    listComplementBien = AssujettissementBusiness.getListComplementBien(bien.getId());
                    if (listComplementBien.size() > 0) {
                        for (ComplementBien cplBien : listComplementBien) {

                            JSONObject jsonComplement = new JSONObject();

                            jsonComplement.put(AssujettissementConst.ParamName.ID_BIEN,
                                    cplBien.getBien() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : cplBien.getBien().getId());

                            jsonComplement.put(AssujettissementConst.ParamName.ID_COMPLEMENT_BIEN,
                                    cplBien.getId() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : cplBien.getId());

                            jsonComplement.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT_BIEN,
                                    cplBien.getTypeComplement() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : cplBien.getTypeComplement().getCode());

                            jsonComplement.put(AssujettissementConst.ParamName.VALEUR_COMPLEMENT_BIEN,
                                    cplBien.getValeur() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : cplBien.getValeur());

                            jsonListComplement.add(jsonComplement);

                        }
                    }
                }
                jsonBien.put(AssujettissementConst.ParamName.COMPLEMENTS_BIEN, jsonListComplement);
                jsonBien.put(AssujettissementConst.ParamName.RESPONSABLE, responsable);
                jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN, intituleBien);

                jsonBiens.add(jsonBien);
            }

            jsonAssujettissement.put(AssujettissementConst.ParamName.LIST_BIENS, jsonBiens);

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonAssujettissement.toString();
    }

    public String desactivateBienAcquisition(HttpServletRequest request) {

        String result;

        try {
            String idAcquisition = request.getParameter(AssujettissementConst.ParamName.ID_ACQUISITION);

            if (AssujettissementBusiness.removeBienAcquisition(idAcquisition)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String createAcquisition(HttpServletRequest request) {

        String result;

        try {
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String dateAcquisition = request.getParameter(AssujettissementConst.ParamName.DATE_ACQUISITION);
            String referenceContrat = request.getParameter(AssujettissementConst.ParamName.REFERENCE_CONTRAT);
            String numActeNotarie = request.getParameter(AssujettissementConst.ParamName.NUM_ACTE_NOTARIE);
            String dateActeNotarie = request.getParameter(AssujettissementConst.ParamName.DATE_ACTE_NOTARIE);

            Acquisition acquisition = new Acquisition();
            acquisition.setPersonne(new Personne(codePersonne));
            acquisition.setBien(new Bien(idBien));
            acquisition.setDateAcquisition(dateAcquisition);
            acquisition.setProprietaire(false);
            acquisition.setReferenceContrat(referenceContrat);
            acquisition.setNumActeNotarie(numActeNotarie);
            acquisition.setDateActeNotarie(dateActeNotarie);

            if (AssujettissementBusiness.createAcquisition(acquisition)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadArticlesBudgetaires(HttpServletRequest request) {

        List<JSONObject> jsonArticlesBudgetaires = new ArrayList<>();

        try {

            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            List<AbComplementBien> abComplementBiens = AssujettissementBusiness.getAssujettissableAbByBien(idBien);

            for (AbComplementBien abComplementBien : abComplementBiens) {

                ArticleBudgetaire articleBudgetaire = abComplementBien.getArticleBudgetaire();
                TypeComplementBien typeComplementBien = abComplementBien.getTypeComplementBien();

                String codeTypeComplement = typeComplementBien.getComplement() == null
                        ? GeneralConst.EMPTY_STRING
                        : typeComplementBien.getComplement().getCode();

                String codeTypeComplementBien = typeComplementBien == null
                        ? GeneralConst.EMPTY_STRING
                        : typeComplementBien.getCode();

                JSONObject jsonArticleBudgetaire = new JSONObject();

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCode());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getIntitule());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCodeOfficiel());

                jsonArticlesBudgetaires.add(jsonArticleBudgetaire);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_PERIODICITE_AB,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodicite().getCode());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.LIBELLE_PERIODICITE_AB,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodicite().getIntitule());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TARIF_AB,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getTarif().getCode());

                List<JSONObject> jsonPaliers = getJsonPaliersByAB(articleBudgetaire);
                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PALIERS, jsonPaliers);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT,
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : codeTypeComplement);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.LIBELLE_TYPE_COMPLEMENT,
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeComplementBien.getComplement().getIntitule());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.BASE_VARIABLE,
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : (typeComplementBien.getComplement().getBaseVariable())
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODICITE_VARIABLE,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getPeriodiciteVariable());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.TARIF_VARIABLE,
                        articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getTarifVariable());

                ComplementBien complementBien = AssujettissementBusiness.getComplementBien(codeTypeComplementBien, idBien);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TYPE_COMPLEMENT_BIEN,
                        typeComplementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : typeComplementBien.getCode());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ID_COMPLEMENT_BIEN,
                        complementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : complementBien.getId());

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.VALEUR_COMPLEMENT_BIEN,
                        complementBien == null
                                ? GeneralConst.EMPTY_STRING
                                : complementBien.getValeur());

                if (complementBien != null) {

                    if (complementBien.getTypeComplement().getComplement().getObjetInteraction().equals(IdentificationConst.ObjetInteraction.MONTANT)) {
                        Palier palier = AssujettissementBusiness.getFistPalierByArticleBudgetaire(articleBudgetaire == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getCode());
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.UNITE, palier.getDevise().getCode());
                    } else {
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.UNITE, articleBudgetaire.getUnite().getIntitule());
                    }
                } else {
                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.UNITE, GeneralConst.EMPTY_STRING);
                }

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE,
                        articleBudgetaire == null ? GeneralConst.Number.ZERO
                                : articleBudgetaire.getNbrJourLimite());

                String echeanceLegale = articleBudgetaire == null ? GeneralConst.EMPTY_STRING
                        : articleBudgetaire.getEcheanceLegale() == null
                                ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getEcheanceLegale();

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ECHEANCE_LEGALE, echeanceLegale);

                jsonArticleBudgetaire.put(AssujettissementConst.ParamName.NBRE_JOUR_LEGALE_PAIEMENT,
                        articleBudgetaire == null ? GeneralConst.EMPTY_STRING
                                : articleBudgetaire.getDateLimitePaiement() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getDateLimitePaiement());

                if (articleBudgetaire == null) {
                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);
                } else {
                    if (articleBudgetaire.getPeriodeEcheance() != null) {
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE,
                                articleBudgetaire.getPeriodeEcheance() == true
                                        ? GeneralConst.Number.ONE
                                        : GeneralConst.Number.ZERO);
                    } else {
                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);
                    }
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticlesBudgetaires.toString();
    }

    public List<JSONObject> getJsonPaliersByAB(ArticleBudgetaire articleBudgetaire) {

        List<JSONObject> jsonPaliers = new ArrayList<>();

        try {

            List<Palier> paliers = AssujettissementBusiness.getListPaliersByAB(articleBudgetaire.getCode(), GeneralConst.ALL, GeneralConst.EMPTY_STRING);

            if (paliers != null) {

                for (Palier palier : paliers) {

                    JSONObject jsonPalier = new JSONObject();

                    jsonPalier.put(AssujettissementConst.ParamName.CODE_TARIF, palier.getTarif() == null
                            ? GeneralConst.EMPTY_STRING : palier.getTarif().getCode());

                    jsonPalier.put(AssujettissementConst.ParamName.PALIER_TYPE_TAUX, palier.getTypeTaux() == null
                            ? GeneralConst.EMPTY_STRING : palier.getTypeTaux());

                    jsonPalier.put(AssujettissementConst.ParamName.UNITE, palier.getUnite() == null
                            ? GeneralConst.EMPTY_STRING : palier.getUnite().getIntitule());

                    jsonPalier.put(AssujettissementConst.ParamName.TAUX_AB, palier.getTaux() == null
                            ? GeneralConst.Number.ZERO : palier.getTaux());

                    jsonPalier.put(AssujettissementConst.ParamName.CODE_TYPE_PERSONNE,
                            palier.getTypePersonne() == null ? GeneralConst.ALL : palier.getTypePersonne().getCode());

                    jsonPalier.put(AssujettissementConst.ParamName.DEVISE, palier.getDevise() == null
                            ? GeneralConst.Number.ZERO : palier.getDevise().getCode());

                    jsonPalier.put(AssujettissementConst.ParamName.BORNE_INFERIEUR, palier.getBorneInferieure());

                    jsonPalier.put(AssujettissementConst.ParamName.BORNE_SUPERIEUR, palier.getBorneSuperieure());

                    jsonPalier.put(AssujettissementConst.ParamName.MULTIPLIER_VALEUR_BASE,
                            palier.getMultiplierValeurBase().equals(Short.valueOf(GeneralConst.Number.ONE))
                                    ? GeneralConst.Number.ONE : palier.getTypeTaux().equals("%")
                                            ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    jsonPaliers.add(jsonPalier);
                }

            } else {
                JSONObject jsonPalier = new JSONObject();

                jsonPalier.put(AssujettissementConst.ParamName.CODE_TARIF, articleBudgetaire.getTarif() == null
                        ? GeneralConst.EMPTY_STRING : articleBudgetaire.getTarif().getCode());

                jsonPalier.put(AssujettissementConst.ParamName.TAUX_AB, articleBudgetaire.getTarif() == null
                        ? GeneralConst.EMPTY_STRING
                        : articleBudgetaire.getTarif().getValeur() + articleBudgetaire.getTarif().getTypeValeur());

                jsonPalier.put(AssujettissementConst.ParamName.MULTIPLIER_VALEUR_BASE, GeneralConst.Number.ONE);

                jsonPaliers.add(jsonPalier);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }

        return jsonPaliers;
    }

    public List<JSONObject> getJsonPaliersByAB(List<Palier> paliers) {

        List<JSONObject> jsonPaliers = new ArrayList<>();

        try {

            for (Palier palier : paliers) {

                JSONObject jsonPalier = new JSONObject();

                jsonPalier.put(AssujettissementConst.ParamName.CODE_TARIF, palier.getTarif() == null
                        ? GeneralConst.EMPTY_STRING : palier.getTarif().getCode());

                jsonPalier.put(AssujettissementConst.ParamName.PALIER_TYPE_TAUX, palier.getTypeTaux() == null
                        ? GeneralConst.EMPTY_STRING : palier.getTypeTaux());

                jsonPalier.put(AssujettissementConst.ParamName.UNITE, palier.getUnite() == null
                        ? GeneralConst.EMPTY_STRING : palier.getUnite().getIntitule());

                jsonPalier.put(AssujettissementConst.ParamName.TAUX_AB, palier.getTaux() == null
                        ? GeneralConst.Number.ZERO : palier.getTaux());

                jsonPalier.put(AssujettissementConst.ParamName.CODE_TYPE_PERSONNE,
                        palier.getTypePersonne() == null ? GeneralConst.ALL : palier.getTypePersonne().getCode());

                jsonPalier.put(AssujettissementConst.ParamName.DEVISE, palier.getDevise() == null
                        ? GeneralConst.Number.ZERO : palier.getDevise().getCode());

                jsonPalier.put(AssujettissementConst.ParamName.BORNE_INFERIEUR, palier.getBorneInferieure());

                jsonPalier.put(AssujettissementConst.ParamName.BORNE_SUPERIEUR, palier.getBorneSuperieure());

                jsonPalier.put(AssujettissementConst.ParamName.MULTIPLIER_VALEUR_BASE,
                        palier.getMultiplierValeurBase().equals(Short.valueOf(GeneralConst.Number.ONE))
                                ? GeneralConst.Number.ONE : palier.getTypeTaux().equals("%")
                                        ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                jsonPaliers.add(jsonPalier);

            }

        } catch (Exception e) {
            CustumException.LogException(e);
        }

        return jsonPaliers;
    }

    public String loadPeriodiciteAndTarifs(HttpServletRequest request) {

        JSONObject jsonDataAssujettissement = new JSONObject();
        List<JSONObject> jsonPeriodicites = new ArrayList<>();
        List<JSONObject> jsonTarifs = new ArrayList<>();

        try {

            String codeAB = request.getParameter(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            String formeJuridique = request.getParameter(AssujettissementConst.ParamName.FORME_JURIDIQUE);

            List<Periodicite> periodicites = AssujettissementBusiness.getListPeriodicites();

            for (Periodicite periodicite : periodicites) {

                JSONObject jsonPeriodicite = new JSONObject();

                jsonPeriodicite.put(AssujettissementConst.ParamName.CODE_PERIODICITE,
                        periodicite == null
                                ? GeneralConst.EMPTY_STRING
                                : periodicite.getCode());

                jsonPeriodicite.put(AssujettissementConst.ParamName.INTITULE_PERIODICITE,
                        periodicite == null
                                ? GeneralConst.EMPTY_STRING
                                : periodicite.getIntitule());

                jsonPeriodicite.put(AssujettissementConst.ParamName.NOMBRE_JOUR,
                        periodicite == null
                                ? GeneralConst.Number.ZERO
                                : periodicite.getNbrJour());

                jsonPeriodicites.add(jsonPeriodicite);
            }

            List<Palier> paliers = AssujettissementBusiness.getListPaliersByAB(codeAB, formeJuridique, GeneralConst.EMPTY_STRING);

            for (Palier palier : paliers) {

                Tarif tarif = palier.getTarif();

                JSONObject jsonTarif = new JSONObject();

                jsonTarif.put(AssujettissementConst.ParamName.CODE_TARIF,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getCode());

                jsonTarif.put(AssujettissementConst.ParamName.INTITULE_TARIF,
                        tarif == null
                                ? GeneralConst.EMPTY_STRING
                                : tarif.getIntitule());

                jsonTarifs.add(jsonTarif);
            }

            jsonDataAssujettissement.put(AssujettissementConst.ParamName.LIST_PERIODICITES, jsonPeriodicites);
            jsonDataAssujettissement.put(AssujettissementConst.ParamName.LIST_TARIFS, jsonTarifs);
            //  ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByCode(codeAB);
            List<JSONObject> jsonPaliers = getJsonPaliersByAB(paliers);
            jsonDataAssujettissement.put(AssujettissementConst.ParamName.PALIERS, jsonPaliers);

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonDataAssujettissement.toString();
    }

    public String getPeriodesDeclarations(HttpServletRequest request) {

        List<JSONObject> jsonPeriodes = new ArrayList();

        try {

            String codePeriodicite = request.getParameter(AssujettissementConst.ParamName.CODE_PERIODICITE_AB);
            String mois = request.getParameter(AssujettissementConst.ParamName.MOIS);
            String annee = request.getParameter(AssujettissementConst.ParamName.ANNEE);
            String nombreJour = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR);
            String nombreJourLimite = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE);
            String echeanceLegale = request.getParameter(AssujettissementConst.ParamName.ECHEANCE_LEGALE);
            String periodeEcheance = request.getParameter(AssujettissementConst.ParamName.PERIODE_ECHEANCE);
            String nombreJourLimitePaiement = request.getParameter(AssujettissementConst.ParamName.NBRE_JOUR_LEGALE_PAIEMENT);

            int nombreDeJours = Integer.parseInt(nombreJour.equals(GeneralConst.EMPTY_STRING)
                    ? GeneralConst.Number.ZERO
                    : nombreJour);

            jsonPeriodes = generatePeriodesDeclarations(
                    codePeriodicite,
                    GeneralConst.Number.ZERO,
                    mois,
                    annee,
                    nombreDeJours,
                    nombreJourLimite,
                    echeanceLegale,
                    nombreJourLimitePaiement,
                    false,
                    periodeEcheance
            );

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return jsonPeriodes.toString();
    }

    public List<JSONObject> generatePeriodesDeclarations(
            String codePeriodicite, String jour, String mois, String annee, int nombreJour,
            String nombreJourLimite, String echeanceLegale, String nombreJourLimitePaiement, boolean forTaxation, String periodeEcheance) {

        final String PONC = "PR0012015",
                JOUR = "PR0022015",
                MENS = "PR0032015",
                TRIME = "PR0052015",
                SEMS = "PR0072015",
                ANNEE = "PR0042015";

        List<JSONObject> jsonPeriodes = new ArrayList();

        Date dateDepart = ConvertDate.formatDate("01/" + mois + GeneralConst.SEPARATOR_SLASH_NO_SPACE + annee);
        int anneeDepart = Integer.parseInt(annee);
        int moisDepart = Integer.parseInt(mois);

        String DATE_DAY_ECHANCE = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE = GeneralConst.EMPTY_STRING;
        String DATE_DAY_ECHANCE_PAIEMENT = GeneralConst.EMPTY_STRING, DATE_MONTH_ECHEANCE_PAIEMENT = GeneralConst.EMPTY_STRING;

        if (!echeanceLegale.equals(GeneralConst.Number.ZERO) && !echeanceLegale.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = echeanceLegale.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE = dateArray[2];
            DATE_MONTH_ECHEANCE = dateArray[1];
        }

        if (!nombreJourLimitePaiement.equals(GeneralConst.Number.ZERO) && !nombreJourLimitePaiement.equals(GeneralConst.EMPTY_STRING)) {
            String[] dateArray = nombreJourLimitePaiement.split(GeneralConst.DASH_SEPARATOR);
            DATE_DAY_ECHANCE_PAIEMENT = dateArray[2];
            DATE_MONTH_ECHEANCE_PAIEMENT = dateArray[1];
        }

        switch (codePeriodicite) {
            case PONC:
            case JOUR:

                int length = 1;
                Date dateJour = new Date();
                if (forTaxation) {
                    length = 10;
                    dateJour = ConvertDate.formatDate(jour + "/" + mois + "/" + annee);
                    dateJour = ConvertDate.addDayOfDate(dateJour, 1);
                }

                jsonPeriodes = PeriodiciteData.getPeriodeJournaliere(dateJour, length, codePeriodicite, nombreJourLimitePaiement);
                break;

            case MENS:
            case "BIMEN":
            case TRIME:
            case SEMS:

                if (forTaxation) {
                    moisDepart += 1;
                    if (moisDepart > 12) {
                        moisDepart = 1;
                        anneeDepart += 1;
                    }
                }

                jsonPeriodes = PeriodiciteData.getPeriodesMensuelles(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        moisDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance);
                break;

            case ANNEE:
            case "2ANS":
            case "5ANS":

                anneeDepart = (forTaxation) ? (anneeDepart + 1) : anneeDepart;

                jsonPeriodes = PeriodiciteData.getPeriodesAnnuelles(
                        nombreJour,
                        forTaxation,
                        dateDepart,
                        anneeDepart,
                        DATE_DAY_ECHANCE,
                        DATE_MONTH_ECHEANCE,
                        DATE_DAY_ECHANCE_PAIEMENT,
                        DATE_MONTH_ECHEANCE_PAIEMENT,
                        codePeriodicite,
                        periodeEcheance);
        }

        return jsonPeriodes;
    }

    public String createAssujettissement(HttpServletRequest request) {

        String result;

        try {
            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);
            String codeArticleBudgetaire = request.getParameter(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            String dateDebut = request.getParameter(AssujettissementConst.ParamName.DATE_DEBUT);
            String dateFin = request.getParameter(AssujettissementConst.ParamName.DATE_FIN);
            String valeurBase = request.getParameter(AssujettissementConst.ParamName.VALEUR_BASE);
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            String nombreJourLimite = request.getParameter(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE);
            String codePeriodicite = request.getParameter(AssujettissementConst.ParamName.CODE_PERIODICITE);
            String codeTarif = request.getParameter(AssujettissementConst.ParamName.CODE_TARIF);
            String codeGestionnaire = request.getParameter(AssujettissementConst.ParamName.CODE_GESTIONNAIRE);
            String codeAP = request.getParameter(AssujettissementConst.ParamName.CODE_ADRESSE_PERSONNE);
            String idComplementBien = request.getParameter(AssujettissementConst.ParamName.ID_COMPLEMENT_BIEN);
            String idUser = request.getParameter(AssujettissementConst.ParamName.ID_USER);
            String nvlEcheance = request.getParameter(AssujettissementConst.ParamName.NVL_ECHEANCE);

            JSONArray jsonPeriodesDeclarations = new JSONArray(
                    request.getParameter(AssujettissementConst.ParamName.PERIODES_DECLARATIONS)
            );

            JSONArray selectedABArray = new JSONArray();

            try {

                selectedABArray = new JSONArray(
                        request.getParameter(AssujettissementConst.ParamName.SELECTED_AB)
                );
            } catch (Exception e) {
            }

            Assujeti assujettissement = AssujettissementBusiness.getExistAssujettissement(
                    idBien,
                    codeArticleBudgetaire,
                    idComplementBien,
                    codePersonne, codeTarif);

            Date dateFinAssuj, dateDebutNewAssuj;

            if (assujettissement != null) {

                dateDebutNewAssuj = ConvertDate.formatDate(dateDebut);
                dateFinAssuj = ConvertDate.formatDate(ConvertDate.getValidFormatDatePrint(assujettissement.getDateFin()));

                if (dateDebutNewAssuj.before(dateFinAssuj)) {
                    return AssujettissementConst.ResultCode.EXIST_ASSUJETTISSEMENT;
                }
            }

            Assujeti assujeti = new Assujeti();
            assujeti.setBien(new Bien(idBien));
            assujeti.setArticleBudgetaire(new ArticleBudgetaire(codeArticleBudgetaire));
            assujeti.setRenouvellement(Short.valueOf(GeneralConst.Number.ZERO));
            assujeti.setDateDebut(dateDebut);
            assujeti.setDateFin(dateFin);
            long base = Long.valueOf(valeurBase.equals(GeneralConst.EMPTY_STRING) ? GeneralConst.Number.ZERO : valeurBase);
            assujeti.setValeur(BigDecimal.valueOf(base));
            assujeti.setPersonne(new Personne(codePersonne));
            assujeti.setNbrJourLimite(Integer.valueOf(nombreJourLimite));
            //assujeti.setPeriodicite(new Periodicite(codePeriodicite));
            assujeti.setPeriodicite(codePeriodicite);
            assujeti.setTarif(new Tarif(codeTarif));
            //assujeti.setGestionnaire(new Agent(Integer.valueOf(codeGestionnaire)));
            assujeti.setGestionnaire(codeGestionnaire);
            assujeti.setFkAdressePersonne(new AdressePersonne(codeAP));
            assujeti.setComplementBien(idComplementBien);
            //assujeti.setAgentCreat(Integer.valueOf(idUser));
            //assujeti.setAgentCreat(Integer.valueOf(idUser));
//            assujeti.setNouvellesEcheances(nvlEcheance);
//            assujeti.setNouvellesEcheances(nvlEcheance);

            List<PeriodeDeclaration> periodeDeclarations = getPeriodesDeclarations(jsonPeriodesDeclarations);

            LogUser logUser = new LogUser(request, AssujettissementConst.Operation.SAVE_ASSUJETTISSEMENT);

            if (AssujettissementBusiness.saveAssujettissement(assujeti, periodeDeclarations, selectedABArray, logUser)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public List<PeriodeDeclaration> getPeriodesDeclarations(JSONArray jsonPeriodesDeclarations) {

        List<PeriodeDeclaration> periodeDeclarations = new ArrayList<>();
        try {
            for (int i = 0; i < jsonPeriodesDeclarations.length(); i++) {

                JSONObject jsonobject = jsonPeriodesDeclarations.getJSONObject(i);
                String dateDebut = jsonobject.getString(AssujettissementConst.ParamName.DATE_DEBUT);
                String dateFin = jsonobject.getString(AssujettissementConst.ParamName.DATE_FIN);
                String dateLimite = jsonobject.getString(AssujettissementConst.ParamName.DATE_LIMITE);
                String dateLimitePaiement = jsonobject.getString(AssujettissementConst.ParamName.DATE_LIMITE_PAIEMENT);

                PeriodeDeclaration periodeDeclaration = new PeriodeDeclaration();
                periodeDeclaration.setDebut(ConvertDate.formatDate(dateDebut));
                periodeDeclaration.setFin(ConvertDate.formatDate(dateFin));
                periodeDeclaration.setDateLimite(ConvertDate.formatDate(dateLimite));
                periodeDeclaration.setDateLimitePaiement(ConvertDate.formatDate(dateLimitePaiement));
                periodeDeclarations.add(periodeDeclaration);

            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return periodeDeclarations;
    }

    public JSONObject getValeurComplementBiens(List<ComplementBien> complementBiens) {

        JSONObject data = new JSONObject();

        try {

            Bien bien = complementBiens.get(0).getBien();
            data.put("idBien", bien.getId());
            data.put("intituleBien", bien.getIntitule());
            data.put("descriptionBien", bien.getDescription());
            data.put("dateAcquisition", bien.getAcquisitionList() == null ? GeneralConst.EMPTY_STRING : bien.getAcquisitionList().get(0).getDateAcquisition());
            data.put("codeTypeBien", bien.getTypeBien() == null ? GeneralConst.EMPTY_STRING : bien.getTypeBien().getCode());
            data.put("codeAdressePersonne", bien.getFkAdressePersonne() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getCode());
            data.put("chaineAdressePersonne", bien.getFkAdressePersonne() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getAdresse() == null ? GeneralConst.EMPTY_STRING : bien.getFkAdressePersonne().getAdresse().toString());

            List<JSONObject> dataList = new ArrayList<>();

            for (ComplementBien cb : complementBiens) {

                JSONObject dataCB = new JSONObject();
                dataCB.put("id", cb.getId());
                dataCB.put("codeTypeComplementBien", cb.getTypeComplement() == null ? GeneralConst.EMPTY_STRING : cb.getTypeComplement().getCode());
                dataCB.put("valeur", cb.getValeur());
                dataCB.put("uniteDevise", cb.getDevise() == null ? GeneralConst.EMPTY_STRING : cb.getDevise());
                dataList.add(dataCB);

            }

            data.put("complementList", dataList);

        } catch (Exception e) {

        }
        return data;
    }

    public String getBienForEdition(HttpServletRequest request) {

        JSONObject jsonBien = new JSONObject();

        try {

            String idBien = request.getParameter(AssujettissementConst.ParamName.ID_BIEN);

            Bien bien = AssujettissementBusiness.getBienById(idBien);

            jsonBien.put(AssujettissementConst.ParamName.CODE_TYPE_BIEN, bien == null
                    ? GeneralConst.EMPTY_STRING : bien.getTypeBien() == null ? GeneralConst.EMPTY_STRING : bien.getTypeBien().getCode());

            jsonBien.put(AssujettissementConst.ParamName.INTITULE_BIEN,
                    bien == null ? GeneralConst.EMPTY_STRING : bien.getIntitule());

            jsonBien.put(AssujettissementConst.ParamName.DESCRIPTION_BIEN,
                    bien == null ? GeneralConst.EMPTY_STRING : bien.getDescription());

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonBien.toString();
    }

    public String loadArticlesBudgetairesAssujettissable(HttpServletRequest request) {

        List<JSONObject> jsonArticlesBudgetaires = new ArrayList<>();
        List<ArticleBudgetaire> articleBudgetaires;
        //List<ArticleMere> articleBudgetairesMereList = new ArrayList<>();

        boolean getMere = false;

        try {
            String libelle = request.getParameter(AssujettissementConst.ParamName.LIBELLE);
            String codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            String codeEntite = request.getParameter(TaxationConst.ParamName.CODE_ENTITE);
            String codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            String getMereValue = request.getParameter(TaxationConst.ParamName.GET_MERE);

            if (getMereValue != null) {

                getMere = getMereValue.equals(GeneralConst.Number.ONE);
            }

            //articleBudgetaires = AssujettissementBusiness.getArticlesBudgetairesAssujettissableBySite(codeSite, codeService, codeEntite);
            articleBudgetaires = AssujettissementBusiness.getArticlesBudgetairesAssujettissable(libelle);

            if (getMere) {

                //articleBudgetairesMereList = AssujettissementBusiness.getArticlesBudgetairesAssujettissableBySiteMere(codeSite, codeService, codeEntite);
            }

            if (articleBudgetaires.isEmpty()) {

                articleBudgetaires = AssujettissementBusiness.
                        getArticlesBudgetairesAssujettissable(libelle, codeService, codeEntite);

                if (getMere) {

                    //articleBudgetairesMereList = AssujettissementBusiness.getArticlesBudgetairesAssujettissableMere(libelle, codeService, codeEntite);
                }

            }

            if (!articleBudgetaires.isEmpty()) {

                for (ArticleBudgetaire articleBudgetaire : articleBudgetaires) {

                    boolean canGo = true;

                    if (getMere) {

//                        if (articleBudgetaire.getArticleReference() != null) {
//                            for (ArticleMere mere : articleBudgetairesMereList) {
//                                if (articleBudgetaire.getArticleReference().equals(mere.getId().toString())) {
//                                    canGo = false;
//                                    break;
//                                }
//                            }
//                        }
//                        if (!canGo) {
//                            continue;
//                        }
                    }

                    JSONObject jsonArticleBudgetaire = new JSONObject();

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                            articleBudgetaire.getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getCode());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_GENERIQUE,
                            articleBudgetaire.getArticleGenerique() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getArticleGenerique().getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                            articleBudgetaire.getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                            articleBudgetaire.getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getCodeOfficiel());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_PERIODICITE_AB,
                            articleBudgetaire.getPeriodicite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getPeriodicite().getCode());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.LIBELLE_PERIODICITE_AB,
                            articleBudgetaire.getPeriodicite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getPeriodicite().getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_TARIF_AB,
                            articleBudgetaire.getTarif() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getTarif().getCode());

                    List<JSONObject> jsonPaliers = getJsonPaliersByAB(articleBudgetaire);
                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PALIERS, jsonPaliers);

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODICITE_VARIABLE,
                            articleBudgetaire.getPeriodiciteVariable() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getPeriodiciteVariable());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.TARIF_VARIABLE,
                            articleBudgetaire.getTarifVariable() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getTarifVariable());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.UNITE,
                            articleBudgetaire.getUnite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getUnite() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : articleBudgetaire.getUnite().getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.NOMBRE_JOUR_LIMITE,
                            articleBudgetaire.getNbrJourLimite() == null
                                    ? GeneralConst.Number.ZERO
                                    : articleBudgetaire.getNbrJourLimite());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ECHEANCE_LEGALE,
                            articleBudgetaire.getEcheanceLegale() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getEcheanceLegale());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.NBRE_JOUR_LEGALE_PAIEMENT,
                            articleBudgetaire.getDateLimitePaiement() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getDateLimitePaiement());

                    if (articleBudgetaire == null) {

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);

                    } else {
                        if (articleBudgetaire.getPeriodeEcheance() != null) {
                            jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE,
                                    articleBudgetaire.getPeriodeEcheance() == true
                                            ? GeneralConst.Number.ONE
                                            : GeneralConst.Number.ZERO);
                        } else {
                            jsonArticleBudgetaire.put(AssujettissementConst.ParamName.PERIODE_ECHEANCE, GeneralConst.EMPTY_STRING);
                        }
                    }

                    if (articleBudgetaire.getArticleGenerique().getServiceAssiette() != null) {

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE_CODE,
                                articleBudgetaire.getArticleGenerique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getServiceAssiette().getCode());

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE,
                                articleBudgetaire.getArticleGenerique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getServiceAssiette().getIntitule());

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.MINISTERE_LIBELLE,
                                articleBudgetaire.getArticleGenerique().getServiceAssiette().getService() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : articleBudgetaire.getArticleGenerique().getServiceAssiette().getService().getIntitule());

                    } else {

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE_CODE,
                                GeneralConst.EMPTY_STRING);

                        jsonArticleBudgetaire.put(AssujettissementConst.ParamName.SECTEUR_ACTIVITE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.IS_PALIER,
                            articleBudgetaire.getPalier() ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.TYPE_AB,
                            GeneralConst.Number.ZERO);

                    //jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ARTICLE_REFERENCE, articleBudgetaire.getArticleReference() == null ? GeneralConst.EMPTY_STRING : articleBudgetaire.getArticleReference());
                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.ARTICLE_REFERENCE, GeneralConst.EMPTY_STRING);

                    jsonArticlesBudgetaires.add(jsonArticleBudgetaire);

                }

//                for (ArticleMere articleMere : articleBudgetairesMereList) {
//
//                    JSONObject jsonArticleBudgetaire = new JSONObject();
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
//                            articleMere.getId());
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
//                            articleMere.getIntitule() == null
//                                    ? GeneralConst.EMPTY_STRING
//                                    : articleMere.getIntitule());
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
//                            articleMere.getCodeOfficiel() == null
//                                    ? GeneralConst.EMPTY_STRING
//                                    : articleMere.getCodeOfficiel());
//
//                    ArticleBudgetaire ab = TaxationBusiness.getArticleBudgetaireByMere(articleMere.getId().toString());
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_PERIODICITE_AB,
//                            ab.getPeriodicite() == null
//                                    ? GeneralConst.EMPTY_STRING
//                                    : ab.getPeriodicite().getCode());
//
//                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.TYPE_AB,
//                            GeneralConst.Number.ONE);
//
//                    jsonArticlesBudgetaires.add(jsonArticleBudgetaire);
//                }
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticlesBudgetaires.toString();
    }

    public String loadAssujettisPrevision(HttpServletRequest request) {

        List<JSONObject> jsonPersonnes = new ArrayList<>();
        String typeSearch = request.getParameter(PeageConst.ParamName.TYPE_SEARCH);
        String valueSearch = request.getParameter(PeageConst.ParamName.VALUE_SEARCH);

        try {

            List<Personne> personnes = new ArrayList<>();

            if (typeSearch.equals(GeneralConst.Number.ZERO)) {
                personnes = PeageBusiness.getAssujettiByName(valueSearch);
            } else {
                Personne personne = PeageBusiness.getAssujettiByCodeOrNif(valueSearch, false);
                if (personne != null) {
                    personnes.add(personne);
                }
            }

            for (Personne personne : personnes) {
                JSONObject jsonPersonne = new JSONObject();

                jsonPersonne.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getFormeJuridique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getFormeJuridique().getIntitule());

                jsonPersonne.put(IdentificationConst.ParamName.CODE_FORME_JURIDIQUE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getFormeJuridique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getFormeJuridique().getCode());

                jsonPersonne.put(IdentificationConst.ParamName.NIF,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNif());

                jsonPersonne.put(IdentificationConst.ParamName.NOM_COMPLET, personne.toString().toUpperCase().trim());

                jsonPersonne.put(IdentificationConst.ParamName.NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNom());

                jsonPersonne.put(IdentificationConst.ParamName.POST_NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPostnom());

                jsonPersonne.put(IdentificationConst.ParamName.PRENOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPrenoms());

                jsonPersonne.put(IdentificationConst.ParamName.TELEPHONE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getTelephone());

                jsonPersonne.put(IdentificationConst.ParamName.EMAIL,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getMail());

                jsonPersonne.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                        getDefaultAdresse(personne).toUpperCase());

                jsonPersonne.put(IdentificationConst.ParamName.CODE_PERSONNE,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getCode());

                jsonPersonnes.add(jsonPersonne);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonPersonnes.toString();
    }

    public String getDefaultAdresse(Personne personne) {

        String adresse = GeneralConst.EMPTY_STRING;
        if (personne != null) {
            if (personne.getAdressePersonneList() != null) {
                for (AdressePersonne adressePersonne : personne.getAdressePersonneList()) {
                    if (adressePersonne.getParDefaut() == true && adressePersonne.getEtat() == true) {
                        adresse = adressePersonne.getAdresse().toString();
                        break;
                    }
                }
            }
        }
        return adresse;
    }

    public String loadTaxesPeages(HttpServletRequest request) {

        String taxesPeages = propertiesConfig.getProperty("ARTICLES_BUDGETAIRES_PEAGES");
        List<JSONObject> jsonArticlesBudgetaires = new ArrayList<>();

        try {

            String siteUser = request.getParameter("siteCode");

            List<ArticleBudgetaire> articleBudgetaireList = AssujettissementBusiness.getAllTaxesPeages(taxesPeages);

            for (ArticleBudgetaire ab : articleBudgetaireList) {

                JSONObject jsonAB = new JSONObject();

                jsonAB.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                        ab == null ? GeneralConst.EMPTY_STRING : ab.getCode());

                jsonAB.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                        ab == null ? GeneralConst.EMPTY_STRING : ab.getIntitule());

                if (ab.getPalierList() == null) {

                    jsonAB.put(AssujettissementConst.ParamName.HAS_PALIER, GeneralConst.Number.ZERO);

                } else {

                    jsonAB.put(AssujettissementConst.ParamName.HAS_PALIER, GeneralConst.Number.ONE);
                }

                List<TarifSite> tarifSites = AssujettissementBusiness.getAllTaxesPeagesBySiteUser(siteUser.trim());

                List<JSONObject> tarifSiteJsonList = new ArrayList<>();

                Tarif tarifObj = new Tarif();
                Tarif tarif;

                for (TarifSite ts : tarifSites) {

                    JSONObject tarifSiteJson = new JSONObject();

                    if (tarifSiteJsonList.size() == GeneralConst.Numeric.ZERO) {

                        tarif = TaxationBusiness.getTarifByCode(ts.getFkTarif().trim());
                        tarifObj = new Tarif();
                        tarifObj = tarif;

                        tarifSiteJson.put(AssujettissementConst.ParamName.CODE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getCode());
                        tarifSiteJson.put(AssujettissementConst.ParamName.INTITULE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getIntitule());
                        tarifSiteJson.put(AssujettissementConst.ParamName.TAUX_AB, ts.getTaux());
                        tarifSiteJson.put(AssujettissementConst.ParamName.DEVISE, ts.getFkDevise() == null ? GeneralConst.EMPTY_STRING : ts.getFkDevise());

                        tarifSiteJsonList.add(tarifSiteJson);

                    } else {

                        tarif = TaxationBusiness.getTarifByCode(ts.getFkTarif().trim());

                        if (!tarifObj.equals(tarif)) {

                            tarifSiteJson.put(AssujettissementConst.ParamName.CODE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getCode());
                            tarifSiteJson.put(AssujettissementConst.ParamName.INTITULE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getIntitule());
                            tarifSiteJson.put(AssujettissementConst.ParamName.TAUX_AB, ts.getTaux());
                            tarifSiteJson.put(AssujettissementConst.ParamName.DEVISE, ts.getFkDevise() == null ? GeneralConst.EMPTY_STRING : ts.getFkDevise());

                            tarifObj = tarif;

                            tarifSiteJsonList.add(tarifSiteJson);
                        }
                    }

                }

                jsonAB.put(AssujettissementConst.ParamName.PALIER_LIST, tarifSiteJsonList);

                jsonArticlesBudgetaires.add(jsonAB);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticlesBudgetaires.toString();
    }

    public String loadArticlesBudgetairesOfAssujetti(HttpServletRequest request) {

        List<JSONObject> jsonArticlesBudgetaires = new ArrayList<>();

        try {

            String codeAssujetti = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);

            List<ArticleBudgetaire> articlbudgetaireList = PeageBusiness.getArticleBudgetaireOfAssujetti(codeAssujetti);

            if (articlbudgetaireList.size() > 0) {

                for (ArticleBudgetaire article : articlbudgetaireList) {

                    JSONObject jsonArticleBudgetaire = new JSONObject();

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                            article == null
                                    ? GeneralConst.EMPTY_STRING
                                    : article.getCode());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE,
                            article == null
                                    ? GeneralConst.EMPTY_STRING
                                    : article.getIntitule());

                    jsonArticleBudgetaire.put(AssujettissementConst.ParamName.CODE_OFFICIEL,
                            article == null
                                    ? GeneralConst.EMPTY_STRING
                                    : article.getCodeOfficiel());

                    jsonArticlesBudgetaires.add(jsonArticleBudgetaire);

                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonArticlesBudgetaires.toString();
    }

    public String getListBiensRemplacementByAssujetti(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codePersonne = request.getParameter("codePersonne");
            String bienCode = request.getParameter("bien");
            String tarifCode = request.getParameter("tarif");

            List<Bien> listBiens = PeageBusiness.getListBiensRemplacementByAssujetti(codePersonne, bienCode, tarifCode);

            if (!listBiens.isEmpty()) {

                List<JsonObject> bienJsonList = new ArrayList<>();

                for (Bien bien : listBiens) {

                    JsonObject bienJson = new JsonObject();
                    ComplementBien complementBien = PeageBusiness.getComplementBienByBienAndPlaque(
                            propertiesConfig.getProperty("COMPLEMENT_BIEN_PLAQUE_IMMATRICULATION"),
                            bien.getId());

                    bienJson.addProperty("bienPlaque", complementBien == null ? GeneralConst.EMPTY_STRING : complementBien.getValeur());
                    bienJson.addProperty("bienId", bien.getId());
                    bienJson.addProperty("bienName", bien.getIntitule());
                    bienJson.addProperty("bienType", bien.getTypeBien().getIntitule().toLowerCase());

                    bienJsonList.add(bienJson);

                }

                dataReturn = bienJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadArticlesBudgetairesOfAssujettiV2(HttpServletRequest request) {

        List<JSONObject> assujJsonList = new ArrayList<>();
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeAssujetti = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
            List<Assujettissement> assujettissements = PeageBusiness.getAssujettissementOfAssujetti(codeAssujetti.trim());

            if (!assujettissements.isEmpty()) {

                for (Assujettissement assujettissement : assujettissements) {

                    JSONObject assujJson = new JSONObject();
                    ArticleBudgetaire articleBudgetaire = new ArticleBudgetaire();

                    articleBudgetaire = TaxationBusiness.getArticleBudgetaireByCode(assujettissement.getArticleBudgetaire());

                    assujJson.put("assujetissementCode", assujettissement.getCode());
                    assujJson.put("assujetissementDate", ConvertDate.formatDateHeureToStringV2(assujettissement.getDateCreate()));
                    assujJson.put("assujetissementState", assujettissement.getEtat());

                    assujJson.put(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE, articleBudgetaire == null ? GeneralConst.EMPTY_STRING : articleBudgetaire.getCode());
                    assujJson.put(AssujettissementConst.ParamName.INTITULE_ARTICLE_BUDGETAIRE, articleBudgetaire == null ? GeneralConst.EMPTY_STRING : articleBudgetaire.getIntitule());
                    assujJson.put(AssujettissementConst.ParamName.CODE_OFFICIEL, articleBudgetaire == null ? GeneralConst.EMPTY_STRING : articleBudgetaire.getCodeOfficiel());

                    List<DetailAssujettissement> detailAssujettissements = PeageBusiness.getDetailsAssujettissementByPersonne(
                            codeAssujetti.trim());

                    List<JSONObject> dAssujJsonnList = new ArrayList<>();

                    for (DetailAssujettissement dassuj : detailAssujettissements) {

                        JSONObject dAssujJson = new JSONObject();

                        dAssujJson.put("assujetissementCode", assujettissement.getCode());
                        dAssujJson.put("taux", dassuj.getTaux());
                        dAssujJson.put("devise", dassuj.getDevise().getCode());
                        dAssujJson.put("tarifCode", dassuj.getTarif().getCode());
                        dAssujJson.put("tarifName", dassuj.getTarif().getIntitule());
                        dAssujJson.put("bienCode", dassuj.getBien().getId());
                        dAssujJson.put("bienName", dassuj.getBien().getIntitule().toUpperCase());
                        dAssujJson.put("bienDescription", dassuj.getBien().getDescription().toUpperCase());
                        dAssujJson.put("typeBien", dassuj.getBien().getTypeBien().getIntitule().toUpperCase());
                        dAssujJson.put("detailAssujId", dassuj.getId());

                        Bien bien = AssujettissementBusiness.getBienById(dassuj.getBien().getId());

                        String immatriculation = GeneralConst.EMPTY_STRING;

                        if (bien != null) {

                            List<ComplementBien> complementBienien = bien.getComplementBienList();

                            String immatriculationTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_IMMATRICULATION");
                            String numeroChassisTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_CHASSIS");

                            for (ComplementBien cb : complementBienien) {

                                TypeComplementBien tcb = cb.getTypeComplement();
                                if (tcb.getComplement().getCode().equals(immatriculationTC) | tcb.getComplement().getCode().equals(numeroChassisTC)) {
                                    immatriculation = cb.getValeur();
                                    break;
                                }
                            }
                        }

                        dAssujJson.put("bienPlaque", immatriculation);

                        dAssujJsonnList.add(dAssujJson);
                    }

                    assujJson.put("detailAssujettissementList", dAssujJsonnList.toString());

                    assujJsonList.add(assujJson);

                }

                dataReturn = assujJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String saveRenouvellementSouProvision(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String numberTour = request.getParameter("numberTour");
            String inputTotalAPayer = request.getParameter("inputTotalAPayer");
            String id = request.getParameter("id");
            String type = request.getParameter("type");
            String devise = request.getParameter("devise");
            String taux = request.getParameter("taux");
            String detailAssujId = request.getParameter("detailAssujId");
            String bien = request.getParameter("bien");
            String prePaiementCode = request.getParameter("prePaiementCode");

            BigDecimal inputTotalAPayer2 = new BigDecimal(0);
            BigDecimal taux2 = new BigDecimal(0);

            inputTotalAPayer2 = inputTotalAPayer2.add(BigDecimal.valueOf(Double.valueOf(inputTotalAPayer)));
            taux2 = taux2.add(BigDecimal.valueOf(Double.valueOf(taux)));

            if (PeageBusiness.saveRenouvellementSouProvision(Integer.valueOf(userId), Integer.valueOf(numberTour),
                    inputTotalAPayer2, Integer.valueOf(type), devise, taux2, bien,
                    Integer.valueOf(detailAssujId), prePaiementCode)) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String activateDetailProvisionCredt(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String id = request.getParameter("id");

            if (PeageBusiness.activateDP(Integer.valueOf(userId), Integer.valueOf(id))) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String suspendreBienPrevisionCredit(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String id = request.getParameter("id");

            if (PeageBusiness.suspendreBienPrevisionCredit(Integer.valueOf(userId), Integer.valueOf(id))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String activateExemption(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String id = request.getParameter("id");

            if (PeageBusiness.activateExemption(Integer.valueOf(userId), Integer.valueOf(id))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String desactivateExemption(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String id = request.getParameter("id");

            if (PeageBusiness.desactivateExemption(Integer.valueOf(userId), Integer.valueOf(id))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String activateAllExemption(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String id = request.getParameter("id");
            String list = request.getParameter("list");

            JSONArray jsonArray = null;

            if (list != null) {
                jsonArray = new JSONArray(list);
            }

            List<String> listId = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String a = jsonObject.getString("id");

                listId.add(a);

            }

            if (PeageBusiness.activateAllExemption(Integer.valueOf(userId), listId)) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String canceledTicket(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String code = request.getParameter("code");
            String observation = request.getParameter("observation");
            String archive = request.getParameter("archive");

            JSONArray jsonArray = null;

            if (archive != null) {
                jsonArray = new JSONArray(archive);
            }

            List<String> listTicket = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String ticket = jsonObject.getString(GeneralConst.ParamName.PV_DOCUMENT);

                listTicket.add(ticket);

            }

            if (PeageBusiness.canceledTicket(Integer.valueOf(userId), listTicket, observation, code)) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String desactivateAllExemption(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String id = request.getParameter("id");
            String list = request.getParameter("list");

            JSONArray jsonArray = null;

            if (list != null) {
                jsonArray = new JSONArray(list);
            }

            List<String> listId = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String a = jsonObject.getString("id");

                listId.add(a);

            }

            if (PeageBusiness.desactivateAllExemption(Integer.valueOf(userId), listId)) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getListSitePeage(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<Site> listSitePeage = PeageBusiness.getAllSitePeage();
            List<JsonObject> siteJsonList = new ArrayList<>();

            if (!listSitePeage.isEmpty()) {

                for (Site site : listSitePeage) {

                    JsonObject siteJson = new JsonObject();

                    siteJson.addProperty("siteCode", site.getCode());
                    siteJson.addProperty("siteName", site.getIntitule().toUpperCase());

                    siteJsonList.add(siteJson);

                }
            }

            dataReturn = siteJsonList.toString();

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getListTarifBySitePeage(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String site = request.getParameter("site");

            if (site.equals(GeneralConst.ALL)) {

                List<Tarif> tarifList = AssujettissementBusiness.getAllTaxesPeages();
                List<JsonObject> tarifJsonList = new ArrayList<>();

                for (Tarif tarif : tarifList) {

                    JsonObject tarifJson = new JsonObject();

                    tarifJson.addProperty(AssujettissementConst.ParamName.CODE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getCode());
                    tarifJson.addProperty(AssujettissementConst.ParamName.INTITULE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getIntitule());

                    tarifJsonList.add(tarifJson);
                }

                dataReturn = tarifJsonList.toString();

            } else {

                List<TarifSite> tarifSites = AssujettissementBusiness.getAllTaxesPeagesBySiteUser(site.trim());

                List<JSONObject> tarifSiteJsonList = new ArrayList<>();

                Tarif tarifObj = new Tarif();
                Tarif tarif;

                for (TarifSite ts : tarifSites) {

                    JSONObject tarifSiteJson = new JSONObject();

                    if (tarifSiteJsonList.size() == GeneralConst.Numeric.ZERO) {

                        tarif = TaxationBusiness.getTarifByCode(ts.getFkTarif().trim());
                        tarifObj = new Tarif();
                        tarifObj = tarif;

                        tarifSiteJson.put(AssujettissementConst.ParamName.CODE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getCode());
                        tarifSiteJson.put(AssujettissementConst.ParamName.INTITULE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getIntitule());

                        tarifSiteJsonList.add(tarifSiteJson);

                    } else {

                        tarif = TaxationBusiness.getTarifByCode(ts.getFkTarif().trim());

                        if (!tarifObj.equals(tarif)) {

                            tarifSiteJson.put(AssujettissementConst.ParamName.CODE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getCode());
                            tarifSiteJson.put(AssujettissementConst.ParamName.INTITULE_TARIF, tarif == null ? GeneralConst.EMPTY_STRING : tarif.getIntitule());

                            tarifObj = tarif;

                            tarifSiteJsonList.add(tarifSiteJson);
                        }
                    }

                }

                dataReturn = tarifSiteJsonList.toString();
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getListPercepteurBySitePeage(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String site = request.getParameter("site");
            String codeFonction = propertiesConfig.getProperty("CODE_FONCTION_PERCEPTEUR");

            List<Agent> percepteurList = AssujettissementBusiness.getAllPercepteurBySite(site.trim(), codeFonction);
            List<JsonObject> percepteurJsonList = new ArrayList<>();

            for (Agent percepteur : percepteurList) {

                JsonObject percepteurJson = new JsonObject();

                percepteurJson.addProperty("codePercepteur", percepteur.getCode());
                percepteurJson.addProperty("nomsPercepteur", percepteur.toString().toUpperCase());
                percepteurJson.addProperty("matriculePercepteur", percepteur.getMatricule() == null
                        ? GeneralConst.EMPTY_STRING : percepteur.getMatricule().toUpperCase());

                percepteurJsonList.add(percepteurJson);
            }

            dataReturn = percepteurJsonList.toString();

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadControleurs(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String site = request.getParameter("site");
            String codeFonction = propertiesConfig.getProperty("CODE_FONCTION_CONTROLEUR");

            List<Agent> controleurList = AssujettissementBusiness.getAllControleurBySite(site.trim(), codeFonction);

            List<JsonObject> controleurJsonList = new ArrayList<>();

            for (Agent percepteur : controleurList) {

                JsonObject controleurJson = new JsonObject();

                controleurJson.addProperty("contoleurId", percepteur.getMatricule());
                controleurJson.addProperty("contoleurName", percepteur.toString().toUpperCase());
                controleurJson.addProperty("siteCode", percepteur.getSite().getCode());
                controleurJson.addProperty("siteName", percepteur.getSite().getIntitule().toUpperCase());

                controleurJsonList.add(controleurJson);
            }

            dataReturn = controleurJsonList.toString();

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String remplacerBienPrevisionCredit(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String id = request.getParameter("id");
            String bien = request.getParameter("bien");

            String codePersonne = GeneralConst.EMPTY_STRING;

            PrevisionCredit previsionCredit = PeageBusiness.getPrevisionCreditByDetailId(id);

            if (previsionCredit != null) {

                codePersonne = previsionCredit.getFkPersonne();

            }

            if (PeageBusiness.remplacerBienPrevisionCredit(Integer.valueOf(userId), codePersonne, bien)) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String editPrepaiement(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String state = request.getParameter("state");
            String code = request.getParameter("code");

            if (PeageBusiness.editPrepaiement(code, Integer.valueOf(userId), state)) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getListSousProvisionByAssujetti(HttpServletRequest request) {

        List<JSONObject> prePaiementJsonList = new ArrayList<>();
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeAssujetti = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
            List<PrevisionCredit> prePaiementList = PeageBusiness.getPrepaiementListByAssujetti(codeAssujetti.trim());

            if (!prePaiementList.isEmpty()) {

                for (PrevisionCredit prePaiement : prePaiementList) {

                    JSONObject prePaiementJson = new JSONObject();

                    prePaiementJson.put("prePaiementCode", prePaiement.getCode());
                    prePaiementJson.put("prePaiementDate", ConvertDate.formatDateHeureToStringV2(prePaiement.getDateCreate()));
                    prePaiementJson.put("prePaiementState", prePaiement.getEtat());
                    prePaiementJson.put("prePaiementSiteCode", prePaiement.getFkSite());

                    Site site = TaxationBusiness.getSiteByCode(prePaiement.getFkSite().trim());

                    prePaiementJson.put("prePaiementSiteName", site == null ? GeneralConst.EMPTY_STRING : site.getIntitule().toUpperCase());
                    prePaiementJson.put("prePaiementAdresseSite", site == null ? GeneralConst.EMPTY_STRING : site.getAdresse().toString().toUpperCase());
                    prePaiementJson.put("prePaiementAssujettiCode", prePaiement.getFkPersonne().trim());

                    List<DetailsPrevisionCredit> detailsPrevisionCredits = PeageBusiness.getDetailsPrePaiements(
                            prePaiement.getCode().trim());

                    List<JSONObject> detailPrePaiementJsonList = new ArrayList<>();

                    for (DetailsPrevisionCredit detailPrePaiement : detailsPrevisionCredits) {

                        JSONObject detailPrePaiementJson = new JSONObject();

                        detailPrePaiementJson.put("prePaiementCode", prePaiement.getCode().trim());
                        detailPrePaiementJson.put("detailPrePaiementId", detailPrePaiement.getId());
                        detailPrePaiementJson.put("detailPrePaiementState", detailPrePaiement.getEtat());

                        Bien bien = AssujettissementBusiness.getBienById(detailPrePaiement.getFkBien().trim());

                        detailPrePaiementJson.put("bienCode", bien == null ? GeneralConst.EMPTY_STRING : bien.getId());
                        detailPrePaiementJson.put("bienName", bien == null ? GeneralConst.EMPTY_STRING : bien.getIntitule().toUpperCase());
                        detailPrePaiementJson.put("bienDescription", bien == null ? GeneralConst.EMPTY_STRING : bien.getDescription());
                        detailPrePaiementJson.put("typeBien", bien == null ? GeneralConst.EMPTY_STRING : bien.getTypeBien().getIntitule().toUpperCase());

                        String immatriculation = GeneralConst.EMPTY_STRING;

                        if (bien != null) {

                            List<ComplementBien> complementBienien = bien.getComplementBienList();

                            String immatriculationTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_IMMATRICULATION");
                            String numeroChassisTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_CHASSIS");

                            for (ComplementBien cb : complementBienien) {

                                TypeComplementBien tcb = cb.getTypeComplement();
                                if (tcb.getComplement().getCode().equals(immatriculationTC) | tcb.getComplement().getCode().equals(numeroChassisTC)) {
                                    immatriculation = cb.getValeur();
                                    break;
                                }
                            }
                        }

                        detailPrePaiementJson.put("bienPlaque", immatriculation);

                        detailPrePaiementJson.put("taux", detailPrePaiement.getTaux());
                        detailPrePaiementJson.put("totalTaux", detailPrePaiement.getTotalTaux());
                        detailPrePaiementJson.put("devise", detailPrePaiement.getFkDevise());

                        DetailAssujettissement detailAssujettissement = PeageBusiness.getDetailAssujettissementByID(
                                detailPrePaiement.getFkDetailAssujettissement());

                        detailPrePaiementJson.put("tarifCode", detailAssujettissement == null ? GeneralConst.EMPTY_STRING : detailAssujettissement.getTarif().getCode());
                        detailPrePaiementJson.put("tarifName", detailAssujettissement == null ? GeneralConst.EMPTY_STRING : detailAssujettissement.getTarif().getIntitule());
                        detailPrePaiementJson.put("detailAssujId", detailPrePaiement.getFkDetailAssujettissement());
                        detailPrePaiementJson.put("numberTourSoumission", detailPrePaiement.getNbreTourSouscrit());
                        detailPrePaiementJson.put("numberTourUsing", detailPrePaiement.getNbreTourUtilise());

                        detailPrePaiementJsonList.add(detailPrePaiementJson);
                    }

                    prePaiementJson.put("detailPrePaiementJsonList", detailPrePaiementJsonList.toString());

                    prePaiementJsonList.add(prePaiementJson);

                }

                dataReturn = prePaiementJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String getListCreditsByAssujetti(HttpServletRequest request) {

        List<JSONObject> prePaiementJsonList = new ArrayList<>();
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeAssujetti = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
            List<PrevisionCredit> prePaiementList = PeageBusiness.getCreditListByAssujetti(codeAssujetti.trim());

            if (!prePaiementList.isEmpty()) {

                for (PrevisionCredit credit : prePaiementList) {

                    JSONObject prePaiementJson = new JSONObject();

                    prePaiementJson.put("prePaiementCode", credit.getCode());
                    prePaiementJson.put("prePaiementDate", ConvertDate.formatDateHeureToStringV2(credit.getDateCreate()));
                    prePaiementJson.put("prePaiementState", credit.getEtat());
                    prePaiementJson.put("prePaiementSiteCode", credit.getFkSite());

                    Site site = TaxationBusiness.getSiteByCode(credit.getFkSite().trim());

                    prePaiementJson.put("prePaiementSiteName", site == null ? GeneralConst.EMPTY_STRING : site.getIntitule().toUpperCase());
                    prePaiementJson.put("prePaiementAdresseSite", site == null ? GeneralConst.EMPTY_STRING : site.getAdresse().toString().toUpperCase());
                    prePaiementJson.put("prePaiementAssujettiCode", credit.getFkPersonne().trim());

                    List<DetailsPrevisionCredit> detailsPrevisionCredits = PeageBusiness.getDetailsCredits(
                            credit.getCode().trim());

                    List<JSONObject> detailPrePaiementJsonList = new ArrayList<>();

                    for (DetailsPrevisionCredit detailPrePaiement : detailsPrevisionCredits) {

                        JSONObject detailPrePaiementJson = new JSONObject();

                        detailPrePaiementJson.put("prePaiementCode", credit.getCode().trim());
                        detailPrePaiementJson.put("detailPrePaiementId", detailPrePaiement.getId());
                        detailPrePaiementJson.put("detailPrePaiementState", detailPrePaiement.getEtat());

                        Bien bien = AssujettissementBusiness.getBienById(detailPrePaiement.getFkBien().trim());

                        detailPrePaiementJson.put("bienCode", bien == null ? GeneralConst.EMPTY_STRING : bien.getId());
                        detailPrePaiementJson.put("bienName", bien == null ? GeneralConst.EMPTY_STRING : bien.getIntitule().toUpperCase());
                        detailPrePaiementJson.put("bienDescription", bien == null ? GeneralConst.EMPTY_STRING : bien.getDescription());
                        detailPrePaiementJson.put("typeBien", bien == null ? GeneralConst.EMPTY_STRING : bien.getTypeBien().getIntitule().toUpperCase());

                        String immatriculation = GeneralConst.EMPTY_STRING;

                        if (bien != null) {

                            List<ComplementBien> complementBienien = bien.getComplementBienList();

                            String immatriculationTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_IMMATRICULATION");
                            String numeroChassisTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_CHASSIS");

                            for (ComplementBien cb : complementBienien) {

                                TypeComplementBien tcb = cb.getTypeComplement();
                                if (tcb.getComplement().getCode().equals(immatriculationTC) | tcb.getComplement().getCode().equals(numeroChassisTC)) {
                                    immatriculation = cb.getValeur();
                                    break;
                                }
                            }
                        }

                        detailPrePaiementJson.put("bienPlaque", immatriculation);

                        detailPrePaiementJson.put("taux", detailPrePaiement.getTaux());
                        detailPrePaiementJson.put("totalTaux", detailPrePaiement.getTotalTaux());
                        detailPrePaiementJson.put("devise", detailPrePaiement.getFkDevise());

                        DetailAssujettissement detailAssujettissement = PeageBusiness.getDetailAssujettissementByID(
                                detailPrePaiement.getFkDetailAssujettissement());

                        detailPrePaiementJson.put("tarifCode", detailAssujettissement == null ? GeneralConst.EMPTY_STRING : detailAssujettissement.getTarif().getCode());
                        detailPrePaiementJson.put("tarifName", detailAssujettissement == null ? GeneralConst.EMPTY_STRING : detailAssujettissement.getTarif().getIntitule());
                        detailPrePaiementJson.put("detailAssujId", detailPrePaiement.getFkDetailAssujettissement());
                        detailPrePaiementJson.put("numberTourSoumission", detailPrePaiement.getNbreTourSouscrit());
                        detailPrePaiementJson.put("numberTourUsing", detailPrePaiement.getNbreTourUtilise());

                        detailPrePaiementJsonList.add(detailPrePaiementJson);
                    }

                    prePaiementJson.put("detailPrePaiementJsonList", detailPrePaiementJsonList.toString());

                    prePaiementJsonList.add(prePaiementJson);

                }

                dataReturn = prePaiementJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String getDetailsAssujettis(HttpServletRequest request) {

        List<JSONObject> jsonDetailsAssujettis = new ArrayList<>();

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeArticle = request.getParameter(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            String codeAssujetti = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);

            List<DetailAssujettissement> detailsAssujetti = PeageBusiness.getDetailsAssujettissementByArticle(codeArticle, codeAssujetti);

            if (detailsAssujetti.size() > 0) {

                for (DetailAssujettissement dlrsAssujet : detailsAssujetti) {

                    JSONObject jsonDetailsPrevision = new JSONObject();

                    jsonDetailsPrevision.put(PeageConst.ParamName.CODE_DETAIL_ASSUJETTISSEMENT,
                            dlrsAssujet == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dlrsAssujet.getId());

                    jsonDetailsPrevision.put(PeageConst.ParamName.VALEUR,
                            dlrsAssujet == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dlrsAssujet.getTaux());

                    jsonDetailsPrevision.put(PeageConst.ParamName.CODE_BIEN,
                            dlrsAssujet.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dlrsAssujet.getBien().getId());

                    jsonDetailsPrevision.put(PeageConst.ParamName.INTITULE_BIEN,
                            dlrsAssujet.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dlrsAssujet.getBien().getIntitule().concat(GeneralConst.SPACE).concat(GeneralConst.BRAKET_OPEN).concat(dlrsAssujet.getBien().getTypeBien().getIntitule().concat(GeneralConst.BRAKET_CLOSE)));

                    jsonDetailsPrevision.put(PeageConst.ParamName.DESCRIPTION_BIEN,
                            dlrsAssujet.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dlrsAssujet.getBien().getDescription());

                    jsonDetailsPrevision.put(PeageConst.ParamName.CHAINE_ADRESSE,
                            dlrsAssujet.getBien() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dlrsAssujet.getBien().getFkAdressePersonne().getAdresse().toString());

                    jsonDetailsPrevision.put(PeageConst.ParamName.DEVISE,
                            dlrsAssujet.getDevise() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dlrsAssujet.getDevise().getCode());

                    jsonDetailsAssujettis.add(jsonDetailsPrevision);

                }

                dataReturn = jsonDetailsAssujettis.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String savePrevisionCredit(HttpServletRequest request) {

        String codeAssujetti = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
        String userID = request.getParameter("userID");
        String detailPrevisionCredit = request.getParameter(PeageConst.ParamName.DEATIL_PREVISION_CREDIT_LIST);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            JSONArray jsonArray = new JSONArray(detailPrevisionCredit);
            PrevisionCredit previsionCredit = new PrevisionCredit();
            List<DetailsPrevisionCredit> listDetailPrevisionCredit = new ArrayList<>();

            previsionCredit.setFkPersonne(codeAssujetti.trim());
            previsionCredit.setAgentCreate(Integer.valueOf(userID));
            previsionCredit.setEtat(GeneralConst.Numeric.ONE);

            for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                BigDecimal taux = new BigDecimal(GeneralConst.Number.ZERO);
                BigDecimal totalTaux = new BigDecimal(GeneralConst.Number.ZERO);

                JSONObject jsonObject = jsonArray.getJSONObject(i);

                if (jsonObject.getString("typePaidExist").equals(GeneralConst.Number.ONE)) {

                    DetailsPrevisionCredit dtlPrevCred = new DetailsPrevisionCredit();

                    taux = taux.add(BigDecimal.valueOf(Double.valueOf(jsonObject.getString("taux"))));
                    totalTaux = totalTaux.add(BigDecimal.valueOf(Double.valueOf(jsonObject.getString("totalPaid"))));

                    dtlPrevCred.setTaux(taux);
                    dtlPrevCred.setTotalTaux(totalTaux);
                    dtlPrevCred.setType(jsonObject.getInt("type"));
                    dtlPrevCred.setFkBien(jsonObject.getString("bienId"));
                    dtlPrevCred.setFkDevise(jsonObject.getString("devise"));
                    dtlPrevCred.setNbreTourSouscrit(jsonObject.getInt("numberTour"));
                    dtlPrevCred.setFkDetailAssujettissement(jsonObject.getInt("detailAssujettissementId"));

                    listDetailPrevisionCredit.add(dtlPrevCred);

                }

            }

            String result = PeageBusiness.savePrevisionCredit(listDetailPrevisionCredit, previsionCredit);

            if (result.isEmpty()) {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveAssujettissementPeage(HttpServletRequest request) {

        String result;

        try {

            String codeArticleBudgetaire = request.getParameter(AssujettissementConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            String codePersonne = request.getParameter(AssujettissementConst.ParamName.CODE_PERSONNE);
            //String idUser = request.getParameter(AssujettissementConst.ParamName.ID_USER);

            JSONArray jsonDetailAssujettissements = new JSONArray(request.getParameter(AssujettissementConst.ParamName.TARIFS));

            List<DetailAssujettissement> detailAssujettissements = getDetailAssujettissements(jsonDetailAssujettissements);

            Assujettissement assujettissement = new Assujettissement();

            assujettissement.setPersonne(codePersonne);
            assujettissement.setArticleBudgetaire(codeArticleBudgetaire);
            assujettissement.setEtat(1);
            assujettissement.setDetailAssujettissementList(detailAssujettissements);

            if (AssujettissementBusiness.saveAssujettissementPeage(assujettissement)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public List<DetailAssujettissement> getDetailAssujettissements(JSONArray jsonDetailAssujettissements) {

        List< DetailAssujettissement> detailAssujettissementList = new ArrayList<>();
        try {
            for (int i = 0; i < jsonDetailAssujettissements.length(); i++) {

                JSONObject jsonobject = jsonDetailAssujettissements.getJSONObject(i);
                String idBien = jsonobject.getString(AssujettissementConst.ParamName.ID_BIEN);
                String codeTarif = jsonobject.getString(AssujettissementConst.ParamName.CODE_TARIF);
                String tauxAB = jsonobject.getString(AssujettissementConst.ParamName.TAUX_AB);
                String devise = jsonobject.getString(AssujettissementConst.ParamName.DEVISE);

                DetailAssujettissement detailAssujettissement = new DetailAssujettissement();
                detailAssujettissement.setBien(new Bien(idBien));
                detailAssujettissement.setTarif(new Tarif(codeTarif));
                detailAssujettissement.setTaux(BigDecimal.valueOf(Long.valueOf(tauxAB)));
                detailAssujettissement.setDevise(new Devise(devise));
                detailAssujettissement.setEtat(1);
                detailAssujettissementList.add(detailAssujettissement);

            }
        } catch (JSONException e) {
            CustumException.LogException(e);
        }
        return detailAssujettissementList;
    }

    public String loadAssujettis(HttpServletRequest request) {

        List<JSONObject> jsonPersonnes = new ArrayList<>();
        String typeSearch = request.getParameter(IdentificationConst.ParamName.TYPE_SEARCH);
        String libelle = request.getParameter(IdentificationConst.ParamName.LIBELLE);

        try {

            List<Personne> personnes = new ArrayList<>();

            if (typeSearch.equals(GeneralConst.Number.ZERO)) {
                personnes = PeageBusiness.getPersonnesByName(libelle);
            } else {
                Personne personne = PeageBusiness.getPersonneByCodeOrNif(libelle, false);
                if (personne != null) {
                    personnes.add(personne);
                }
            }

            for (Personne personne : personnes) {

                JSONObject jsonPersonne = new JSONObject();

                jsonPersonne.put(IdentificationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getFormeJuridique() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getFormeJuridique().getIntitule());

                jsonPersonne.put(IdentificationConst.ParamName.NIF,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNif());

                jsonPersonne.put(IdentificationConst.ParamName.NOM_COMPLET, personne.toString().toUpperCase().trim());

                jsonPersonne.put(IdentificationConst.ParamName.NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getNom());

                jsonPersonne.put(IdentificationConst.ParamName.POST_NOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPostnom());

                jsonPersonne.put(IdentificationConst.ParamName.PRENOM,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getPrenoms());

                jsonPersonne.put(IdentificationConst.ParamName.TELEPHONE,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getTelephone());

                jsonPersonne.put(IdentificationConst.ParamName.EMAIL,
                        personne == null
                                ? GeneralConst.EMPTY_STRING
                                : personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getMail());

                jsonPersonne.put(IdentificationConst.ParamName.CHAINE_ADRESSE,
                        getDefaultAdresse(personne).toUpperCase());

                jsonPersonne.put(IdentificationConst.ParamName.CODE_PERSONNE,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getCode());

                jsonPersonne.put(IdentificationConst.ParamName.ETAT,
                        personne == null ? GeneralConst.EMPTY_STRING : personne.getEtat());

                PersonneExemptee personneExmpte = PeageBusiness.checkPersonneExemptes(personne.getCode().trim());

                if (personneExmpte != null) {
                    jsonPersonne.put(PeageConst.ParamName.EST_EXEMPTE, GeneralConst.Number.ONE);
                    jsonPersonne.put(PeageConst.ParamName.CODE_PERSONNE_EXEMPTE, personneExmpte.getId());
                } else {
                    jsonPersonne.put(PeageConst.ParamName.EST_EXEMPTE, GeneralConst.Number.ZERO);
                    jsonPersonne.put(PeageConst.ParamName.CODE_PERSONNE_EXEMPTE, GeneralConst.EMPTY_STRING);
                }

                jsonPersonnes.add(jsonPersonne);
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonPersonnes.toString();
    }

    public String savePersonneExemptes(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;

        try {
            String codePersonne = request.getParameter(IdentificationConst.ParamName.CODE_PERSONNE);
            String user = request.getParameter(PeageConst.ParamName.USER_ID);

            PersonneExemptee personneExempte = new PersonneExemptee();

            personneExempte.setPersonne(new Personne(codePersonne));
            personneExempte.setAgentCreat(Integer.valueOf(user));
            personneExempte.setDateCreate(new Date());
            personneExempte.setEtat(GeneralConst.Numeric.ONE);

            if (PeageBusiness.exempteAssujettis(personneExempte)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String disabledPersonneExemptes(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        try {

            String idPersonneExempte = request.getParameter(PeageConst.ParamName.CODE_PERSONNE_EXEMPTE);

            if (idPersonneExempte != null) {
                if (PeageBusiness.disabledPersonneExemptes(idPersonneExempte)) {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }
        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String loadPeages(HttpServletRequest request) {

        List<JSONObject> jsonTicketPeages = new ArrayList<>();

        try {

            List<TicketPeage> ticketPeageList;

            String tcImm = propertiesConfig.getProperty("TYPE_COMPLEMENT_IMMATRICULATION");

            String isAdvanced = request.getParameter("isAdvanced");

            if (isAdvanced.equals(GeneralConst.Number.ZERO)) {

                //String typeSearch = request.getParameter("typeSearch");
                String valueSearch = request.getParameter("valueSearch");
                String codeSite = request.getParameter("codeSite");
                String userId = request.getParameter("userId");

                ticketPeageList = PeageBusiness.getTicketPeages(valueSearch, codeSite, Integer.valueOf(userId));

            } else {

                String siteCode = request.getParameter("siteCode");
                String typePaiement = request.getParameter("type");

                String tarif = request.getParameter("tarif");
                String percepteur = request.getParameter("percepteur");

                String dateDebut = request.getParameter("dateDebut");
                String dateFin = request.getParameter("dateFin");
                String shift = request.getParameter("shift");

                ticketPeageList = PeageBusiness.getTicketPeages(siteCode, typePaiement,
                        ConvertDate.getValidFormatDate(dateDebut), ConvertDate.getValidFormatDate(dateFin),
                        tarif.trim(), percepteur.trim(), shift);
            }

            for (TicketPeage ticket : ticketPeageList) {

                Bien bien = ticket.getBien();
                ComplementBien cb = new ComplementBien();
                Tarif tarif = new Tarif();

                if (bien != null) {
                    cb = PeageBusiness.getComplementBienOfImm(bien.getId(), tcImm);
                } else {
                    cb = null;
                    bien = null;
                }

                ArticleBudgetaire taxe = new ArticleBudgetaire();

                if (ticket.getArticleBudgetaire() != null) {
                    taxe = ticket.getArticleBudgetaire();
                } else {
                    taxe = TaxationBusiness.getArticleBudgetaireByCode(propertiesConfig.getProperty("ARTICLES_BUDGETAIRES_PEAGESS"));
                }

                Devise devise = ticket.getDevise();

                Site site = ticket.getSite();

                Personne personne = ticket.getPersonne();
                String codePersonne = personne == null ? "" : personne.getCode();
                String nom = personne == null ? "" : personne.getNom();
                String postnom = personne == null ? "" : personne.getPostnom();
                String prenom = personne == null ? "" : personne.getPrenoms();

                Agent agent = ConnexionBusiness.getAgentByCode(ticket.getAgentCreat());

                String nomAgent = GeneralConst.EMPTY_STRING;

                if (agent != null) {
                    nomAgent = agent.getMatricule() + " - " + agent.toString().toUpperCase();
                }

                if (ticket.getFkTarif() != null) {
                    tarif = TaxationBusiness.getTarifByCode(ticket.getFkTarif());
                }

                String numeroPlaque = "--";
                String shift = "--";
                String stateCar = "Véhicule non identifié".toUpperCase();

                if (ticket.getPlaqueChassis() != null) {
                    numeroPlaque = ticket.getPlaqueChassis().toUpperCase();
                    stateCar = "Véhicule identifié".toUpperCase();
                }

                if (ticket.getShiftTravail() != null) {
                    shift = ticket.getShiftTravail().toUpperCase();
                }

                JSONObject data = new JSONObject();
                data.put("code", ticket.getCode());
                data.put("bien", bien == null ? stateCar : bien.getIntitule());
                data.put("numPlaque", cb == null ? numeroPlaque : cb.getValeur() + GeneralConst.SPACE + "SHIFT" + shift);

                if (!nom.isEmpty()) {
                    data.put("personne", nom + " " + postnom + " " + prenom);
                } else {
                    data.put("personne", "SPONTANE");
                }

                data.put("taxe", taxe == null ? "" : taxe.getIntitule());
                data.put("tarif", tarif == null ? "" : tarif.getIntitule());
                data.put("montant", ticket.getMontant());
                data.put("devise", devise == null ? "" : devise.getCode());
                data.put("typePaiement", ticket.getTypePaiement() == null ? "" : ticket.getTypePaiement());
                data.put("motifAnnulation", ticket.getMotifAnnulation() == null ? "" : ticket.getMotifAnnulation());
                data.put("dateProd", ticket.getDateProd() == null ? "" : Tools.formatDateWithTimeToString(ticket.getDateProd()));
                data.put("dateSync", ticket.getDateSync() == null ? "" : Tools.formatDateWithTimeToString(ticket.getDateSync()));
                data.put("agentCreate", nomAgent);
                data.put("site", site == null ? "" : site.getIntitule().toUpperCase());
                data.put("shift", ticket.getShiftTravail() == null ? "" : "(SHIFT : ".concat(ticket.getShiftTravail().concat(")")));

                jsonTicketPeages.add(data);

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonTicketPeages.toString();
    }

    public String loadTicketCanceled(HttpServletRequest request) {

        List<JSONObject> jsonTicketPeages = new ArrayList<>();

        try {

            List<TicketPeage> ticketPeageList;

            String tcImm = propertiesConfig.getProperty("TYPE_COMPLEMENT_IMMATRICULATION");

            String isAdvanced = request.getParameter("isAdvanced");

            if (isAdvanced.equals(GeneralConst.Number.ZERO)) {

                //String typeSearch = request.getParameter("typeSearch");
                String valueSearch = request.getParameter("valueSearch");
                String codeSite = request.getParameter("codeSite");
                String userId = request.getParameter("userId");

                ticketPeageList = PeageBusiness.getTicketCanceledPeages(valueSearch, codeSite, Integer.valueOf(userId));

            } else {

                String siteCode = request.getParameter("siteCode");
                String typePaiement = request.getParameter("type");

                String tarif = request.getParameter("tarif");
                String percepteur = request.getParameter("percepteur");

                String dateDebut = request.getParameter("dateDebut");
                String dateFin = request.getParameter("dateFin");

                ticketPeageList = PeageBusiness.getTicketCanceledPeages(siteCode, typePaiement,
                        ConvertDate.getValidFormatDate(dateDebut), ConvertDate.getValidFormatDate(dateFin), tarif.trim(), percepteur.trim());
            }

            for (TicketPeage ticket : ticketPeageList) {

                Bien bien = ticket.getBien();
                ComplementBien cb = new ComplementBien();
                Tarif tarif = new Tarif();

                if (bien != null) {
                    cb = PeageBusiness.getComplementBienOfImm(bien.getId(), tcImm);
                } else {
                    cb = null;
                    bien = null;
                }

                ArticleBudgetaire taxe = new ArticleBudgetaire();

                if (ticket.getArticleBudgetaire() != null) {
                    taxe = ticket.getArticleBudgetaire();
                } else {
                    taxe = TaxationBusiness.getArticleBudgetaireByCode(propertiesConfig.getProperty("ARTICLES_BUDGETAIRES_PEAGESS"));
                }

                Devise devise = ticket.getDevise();

                Site site = ticket.getSite();

                Personne personne = ticket.getPersonne();
                String codePersonne = personne == null ? "" : personne.getCode();
                String nom = personne == null ? "" : personne.getNom();
                String postnom = personne == null ? "" : personne.getPostnom();
                String prenom = personne == null ? "" : personne.getPrenoms();

                Agent agent = ConnexionBusiness.getAgentByCode(ticket.getAgentCreat());

                String nomAgent = GeneralConst.EMPTY_STRING;

                if (agent != null) {
                    nomAgent = agent.getMatricule() + " - " + agent.toString().toUpperCase();
                }

                String nomAgentCanceled = GeneralConst.EMPTY_STRING;

                if (ticket.getAgentMaj() != null) {

                    agent = ConnexionBusiness.getAgentByCode(ticket.getAgentMaj());

                    if (agent != null) {
                        nomAgentCanceled = agent.getMatricule().toUpperCase() + " - " + agent.toString().toUpperCase();
                    }
                }

                if (ticket.getFkTarif() != null) {
                    tarif = TaxationBusiness.getTarifByCode(ticket.getFkTarif());
                }

                JSONObject data = new JSONObject();
                data.put("code", ticket.getCode());
                data.put("bien", bien == null ? "Véhicule non identifié".toUpperCase() : bien.getIntitule());
                data.put("numPlaque", cb == null ? "--" : cb.getValeur());

                if (!nom.isEmpty()) {
                    data.put("personne", nom + " " + postnom + " " + prenom);
                } else {
                    data.put("personne", "SPONTANE");
                }

                data.put("taxe", taxe == null ? "" : taxe.getIntitule());
                data.put("tarif", tarif == null ? "" : tarif.getIntitule());
                data.put("montant", ticket.getMontant());
                data.put("devise", devise == null ? "" : devise.getCode());
                data.put("typePaiement", ticket.getTypePaiement() == null ? "" : ticket.getTypePaiement());
                data.put("motifAnnulation", ticket.getMotifAnnulation() == null ? "" : ticket.getMotifAnnulation());
                data.put("dateProd", ticket.getDateProd() == null ? "" : Tools.formatDateWithTimeToString(ticket.getDateProd()));
                data.put("dateSync", ticket.getDateSync() == null ? "" : Tools.formatDateWithTimeToString(ticket.getDateSync()));
                data.put("dateCanceled", ticket.getDateMaj() == null ? "" : Tools.formatDateWithTimeToString(ticket.getDateMaj()));
                data.put("agentCreate", nomAgent);
                data.put("agentCanceled", nomAgentCanceled);
                data.put("observationCanceled", ticket.getObservation() == null ? "" : ticket.getObservation());
                data.put("ticketCanceled", ticket.getTicketDocument() == null ? "" : ticket.getTicketDocument());
                data.put("site", site == null ? "" : site.getIntitule().toUpperCase());

                jsonTicketPeages.add(data);

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonTicketPeages.toString();
    }

    public String getListBienAssujettiExempte(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codePersonne = request.getParameter("codePersonne");

            List<DetailAssujettissement> listDetailAssujettissements = PeageBusiness.getDetailAssujettissementOfAssujetti(codePersonne);

            if (!listDetailAssujettissements.isEmpty()) {

                List<JsonObject> bienJsonList = new ArrayList<>();

                for (DetailAssujettissement detailAssujettissement : listDetailAssujettissements) {

                    JsonObject bienJson = new JsonObject();
                    Bien bien = detailAssujettissement.getBien();

                    bienJson.addProperty("id", detailAssujettissement.getId());
                    bienJson.addProperty("bienId", bien.getId());
                    bienJson.addProperty("bienName", bien.getIntitule().toUpperCase());
                    bienJson.addProperty("descriptionBien", bien.getDescription());
                    bienJson.addProperty("bienType", bien.getTypeBien().getIntitule().toUpperCase());
                    bienJson.addProperty("bienStateName", detailAssujettissement.getEtat() == 1 ? "NON EXEMPTE" : "EXEMPTE");
                    bienJson.addProperty("bienStateId", detailAssujettissement.getEtat());

                    bienJson.addProperty("tarifName", detailAssujettissement.getTarif().getIntitule().toUpperCase());
                    bienJson.addProperty("taux", detailAssujettissement.getTaux());
                    bienJson.addProperty("devise", detailAssujettissement.getDevise().getCode());

                    String immatriculation = GeneralConst.EMPTY_STRING;

                    List<ComplementBien> complementBienien = bien.getComplementBienList();

                    String immatriculationTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_IMMATRICULATION");
                    String numeroChassisTC = propertiesConfig.getProperty("TYPE_COMPLEMENT_CHASSIS");

                    for (ComplementBien cb : complementBienien) {

                        TypeComplementBien tcb = cb.getTypeComplement();
                        if (tcb.getComplement().getCode().equals(immatriculationTC) | tcb.getComplement().getCode().equals(numeroChassisTC)) {
                            immatriculation = cb.getValeur();
                            break;
                        }
                    }

                    bienJson.addProperty("bienPlaque", immatriculation);

                    bienJsonList.add(bienJson);
                }

                dataReturn = bienJsonList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printRapportProduction(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String userId = request.getParameter("userID");
            String data = request.getParameter("data");

            String percepeurCode = request.getParameter("percepeurCode");
            String percepeurName = request.getParameter("percepeurName");
            String controleurCode = request.getParameter("controleurCode");
            String controleurName = request.getParameter("controleurName");
            String poste = request.getParameter("poste");
            String typeRapport = request.getParameter("typeRapport");
            String axe = request.getParameter("axe");
            String observation = request.getParameter("observation");

            JSONArray jsonArray = null;
            List<TicketPeage> listTicketPeage = new ArrayList<>();

            if (data != null) {
                jsonArray = new JSONArray(data);
            }

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = jsonArray.getJSONObject(i);
                TicketPeage ticketPeage = PeageBusiness.getTicketPeageByCode(jsonObject.getString("ticketId"));

                listTicketPeage.add(ticketPeage);
            }

            List<TicketPeagePrint> listTicketPeagePrint = new ArrayList<>();

            String current = GeneralConst.EMPTY_STRING;
            int tourInt = 0;

            for (TicketPeage tp : listTicketPeage) {

                //System.out.println("ticket : " + tp.getCode());
                TicketPeagePrint ticketPeagePrint = new TicketPeagePrint();

                if (current.isEmpty()) {

                    current = tp.getCode();

                    tourInt++;

                } else {

                    if (current.equals(tp.getCode())) {

                        tourInt++;

                    } else {

                    }
                }

                //ticketPeagePrint.set
                listTicketPeagePrint.add(ticketPeagePrint);
            }

            PrintDocument printDocument = new PrintDocument();
            dataReturn = printDocument.createRapportProductionPeage(listTicketPeage, percepeurCode, percepeurName, controleurCode,
                    controleurName, poste, typeRapport, axe, observation, userId);

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadCommandeVoucher(HttpServletRequest request) {

        List<JSONObject> jsonCmdVoucherList = new ArrayList<>();

        JSONObject jsonCmdVoucher;
        String valueSearch, typeSearch;

        try {
            List<Commande> cmdVoucherList;
            List<Voucher> voucherList;

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            String dateDebut = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_DEBUT);
            String dateFin = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_FIN);
            Boolean isAdvancedSearch = Boolean.valueOf(request.getParameter(TaxationConst.ParamName.IS_AVANCED_SEARCH));

            dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            dateFin = ConvertDate.getValidFormatDate(dateFin);

            Boolean checkRemise = Boolean.valueOf(propertiesConfig.getProperty("checkRemise"));

            if (isAdvancedSearch) {
                cmdVoucherList = PeageBusiness.getCommandeVoucherByDate(dateDebut, dateFin);
            } else {
                cmdVoucherList = PeageBusiness.getCommandeVouche(valueSearch, Integer.valueOf(typeSearch));
            }

            if (cmdVoucherList.size() > 0) {

                for (Commande cmdVch : cmdVoucherList) {

                    jsonCmdVoucher = new JSONObject();

                    jsonCmdVoucher.put(AssujettissementConst.ParamName.CODE_PERSONNE, cmdVch.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getFkPersonne().getCode());
                    jsonCmdVoucher.put(AssujettissementConst.ParamName.RESPONSABLE, cmdVch.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getFkPersonne().toString());
                    jsonCmdVoucher.put(AssujettissementConst.ParamName.FORME_JURIDIQUE, cmdVch.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getFkPersonne().getFormeJuridique() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : cmdVch.getFkPersonne().getFormeJuridique().getIntitule());
                    jsonCmdVoucher.put("id", cmdVch.getId());
                    jsonCmdVoucher.put("dateCreat", cmdVch.getDateCreat() == null
                            ? GeneralConst.EMPTY_STRING
                            : ConvertDate.formatDateHeureToString(cmdVch.getDateCreat()));
                    jsonCmdVoucher.put("montant", cmdVch.getMontant() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getMontant());
                    jsonCmdVoucher.put("etat", cmdVch.getEtat());
                    jsonCmdVoucher.put("fkArticle", cmdVch.getFkAb() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getFkAb().getCode());
                    jsonCmdVoucher.put("intituleArticle", cmdVch.getFkAb() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getFkAb().getCode());
                    jsonCmdVoucher.put("devise", cmdVch.getDevise() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getDevise().getCode());
                    jsonCmdVoucher.put("intituleDevise", cmdVch.getDevise() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getDevise().getIntitule());
                    jsonCmdVoucher.put("reference", cmdVch.getReference() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdVch.getReference());
                    jsonCmdVoucher.put("preuvePaiement", cmdVch.getPreuvePaiement() == null
                            ? false
                            : true);
                    jsonCmdVoucher.put("checkRemise", checkRemise);

                    voucherList = PeageBusiness.getVoucherByCommande(String.valueOf(cmdVch.getId()));

                    if (voucherList.size() > 0) {

                        List<JSONObject> jsonDetailCmdVoucheList = new ArrayList<>();
                        JSONObject jsonDetailCmdVouche;

                        for (Voucher vcher : voucherList) {
                            jsonDetailCmdVouche = new JSONObject();

                            jsonDetailCmdVouche.put("id", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getId());
                            jsonDetailCmdVouche.put("idCmd", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getFkCommande() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : vcher.getFkCommande().getId());
                            jsonDetailCmdVouche.put("codeSite", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getFkSite() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : vcher.getFkSite().getCode());
                            jsonDetailCmdVouche.put("intituleSite", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getFkSite() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : vcher.getFkSite().getIntitule());
                            jsonDetailCmdVouche.put("codeTarif", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getFkTarif() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : vcher.getFkTarif().getCode());
                            jsonDetailCmdVouche.put("intituleTarif", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getFkTarif() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : vcher.getFkTarif().getIntitule());
                            jsonDetailCmdVouche.put("codeAxe", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getFkAxe() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : vcher.getFkAxe().getId());
                            jsonDetailCmdVouche.put("intituleAxe", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getFkAxe() == null
                                            ? GeneralConst.EMPTY_STRING
                                            : vcher.getFkAxe().getIntitule());
                            jsonDetailCmdVouche.put("qte", vcher == null
                                    ? GeneralConst.Numeric.ZERO
                                    : vcher.getQte());
                            jsonDetailCmdVouche.put("prixUnitaire", vcher == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getPrixUnitaire());
                            jsonDetailCmdVouche.put("montantVoucher", vcher == null
                                    ? GeneralConst.Numeric.ZERO
                                    : vcher.getMontant());
                            if (vcher.getRemise() == null) {
                                jsonDetailCmdVouche.put("remise", GeneralConst.Numeric.ZERO);
                            } else {
                                jsonDetailCmdVouche.put("remise", vcher.getRemise());
                            }
                            if (vcher.getMontantFinal() == null) {
                                jsonDetailCmdVouche.put("montantFinal", GeneralConst.Numeric.ZERO);
                            } else {
                                jsonDetailCmdVouche.put("montantFinal", vcher.getMontantFinal());
                            }
                            jsonDetailCmdVouche.put("codeDevise", vcher.getDevise() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getDevise().getCode());
                            jsonDetailCmdVouche.put("intituleDevise", vcher.getDevise() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getDevise().getIntitule());
                            jsonDetailCmdVouche.put("codeAgentMj", vcher.getAgentMaj() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getAgentMaj().getCode());
                            jsonDetailCmdVouche.put("nameAgentMj", vcher.getAgentMaj() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getAgentMaj().toString());
                            jsonDetailCmdVouche.put("etatVoucher", vcher.getEtat());
                            jsonDetailCmdVouche.put("reference", vcher.getFkCommande() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : vcher.getFkCommande().getReference());
                            jsonDetailCmdVouche.put("codeAssujetti", vcher.getFkCommande().getFkPersonne().getCode());
                            jsonDetailCmdVouche.put("checkRemise", checkRemise);

                            jsonDetailCmdVoucheList.add(jsonDetailCmdVouche);
                        }

                        jsonCmdVoucher.put("detailCmdVoucheList", jsonDetailCmdVoucheList.toString());

                    } else {
                        jsonCmdVoucher.put("detailCmdVoucheList", GeneralConst.Number.ZERO);
                    }

                    jsonCmdVoucherList.add(jsonCmdVoucher);
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonCmdVoucherList.toString();
    }

    public String updateVoucher(HttpServletRequest request) {

        String result = null;
        String codepersonne = GeneralConst.EMPTY_STRING;
        String reference = GeneralConst.EMPTY_STRING;
        String codeCmd = null;

        try {
            List<Voucher> voucherList = new ArrayList<>();
            String resp;
            String agentMaj = request.getParameter("agentMaj");
            String codeCompteBancaire = request.getParameter("codeCompteBancaire");
            Boolean checkOperation = Boolean.valueOf(request.getParameter("checkOperation"));
            JSONArray jsondetailCmdVoucheList = new JSONArray(request.getParameter("detailCmdVoucheList"));

            BigDecimal sumMonantFinal = new BigDecimal(BigInteger.ZERO);

            for (int i = 0; i < jsondetailCmdVoucheList.length(); i++) {
                JSONObject jsonobject = jsondetailCmdVoucheList.getJSONObject(i);

                Voucher vche = new Voucher();

                BigDecimal montantFinal = BigDecimal.valueOf(Double.valueOf(jsonobject.getString("montantFinal")));
                BigDecimal remise = BigDecimal.valueOf(Double.valueOf(jsonobject.getString("remise")));
                String id = jsonobject.getString("id");
                String idCmd = jsonobject.getString("idCmd");
                codepersonne = jsonobject.getString("codeAssujetti");
                reference = jsonobject.getString("reference");
                codeCmd = idCmd;

                sumMonantFinal = sumMonantFinal.add(montantFinal);

                vche.setMontantFinal(montantFinal);
                vche.setRemise(remise);
                vche.setId(Integer.valueOf(id));
                vche.setFkCommande(new Commande(Integer.valueOf(idCmd)));
                vche.setAgentMaj(new Agent(Integer.valueOf(agentMaj)));

                voucherList.add(vche);
            }
            Personne personne = IdentificationBusiness.getPersonneByCode(codepersonne);
            if (checkOperation) {
                if (PeageBusiness.updateVoucher(voucherList, sumMonantFinal, codeCompteBancaire)) {
                    if (!codepersonne.equals(GeneralConst.EMPTY_STRING)) {
                        if (personne != null) {
                            resp = sendNotification(personne, sumMonantFinal, reference);
                            if (resp == "1") {
                                result = GeneralConst.ResultCode.SUCCES_OPERATION;
                            } else {
                                result = GeneralConst.ResultCode.VALUE_EXIST;
                            }
                        }
                    } else {
                        result = GeneralConst.ResultCode.VALUE_EXIST;
                    }

                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            } else {
                resp = sendCodeVoucherEmail(personne, codeCmd);
                if (resp == "1") {
                    result = GeneralConst.ResultCode.SUCCES_OPERATION;
                } else {
                    result = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String sendNotification(Personne personne, BigDecimal montantFinal, String reference) throws IOException {

        String service_sms = propertiesConfig.getProperty("SMS_SERVICE");
        String mailjetUser = propertiesConfig.getProperty("MAILJET_USERNAME");
        String mailjetPassword = propertiesConfig.getProperty("MAILJET_PASSWORD");
        String mailjetURL = propertiesConfig.getProperty("MAILJET_URL");
        String mailSmtp = propertiesConfig.getProperty("MAIL_SMTP");

        String smsURL;

        if (service_sms.equals(GeneralConst.Number.ONE)) {
            smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL);
        } else {
            smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL2);
        }
        String smsSubscribe = propertiesConfig.getProperty("SMS_SUBSCRIBE_PEAGE");
        String mailSubscribeContent = propertiesConfig.getProperty("MAIL_SUBSCRIBE_CLIENT_CONTENT");
        String mailSubscribeSubjet = propertiesConfig.getProperty("MAIL_SUBSCRIBE_CLIENT_SUBJECT");
        String urlErecettes = propertiesConfig.getProperty("ERECETTES_LINK_PEAGE");

        String mailContent = String.format(mailSubscribeContent, personne.toString(), reference, montantFinal, urlErecettes);

        MailSender mailSender = new MailSender();
        mailSender.setDate(new Date());
        mailSender.setHost(mailjetURL);
        mailSender.setSubject(mailSubscribeSubjet);
        mailSender.setMessage(mailContent);
        mailSender.setReciever(personne.getLoginWeb().getMail().trim());
        mailSender.setSender(mailSmtp);

        mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);

        String mail = personne.getLoginWeb() == null ? GeneralConst.EMPTY_STRING : personne.getLoginWeb().getMail() == null
                ? GeneralConst.EMPTY_STRING : personne.getLoginWeb().getMail();
        String telephone = personne.getLoginWeb() == null ? GeneralConst.EMPTY_STRING : personne.getLoginWeb().getTelephone() == null
                ? GeneralConst.EMPTY_STRING : personne.getLoginWeb().getTelephone();

        if (!telephone.equals(GeneralConst.EMPTY_STRING)) {
            String smsContent = String.format(smsSubscribe, personne.getNom(), reference, mail);
            smsURL = String.format(smsURL, personne.getLoginWeb().getTelephone().trim(), smsContent);
            SMSSender.sendSMS(smsURL);
        }

        return "1";
    }

    public String sendCodeVoucherEmail(Personne personne, String codeCmd) throws IOException {

        List<DetailVoucher> detailvcherList = new ArrayList<>();

        String service_sms = propertiesConfig.getProperty("SMS_SERVICE");
        String mailjetUser = propertiesConfig.getProperty("MAILJET_USERNAME");
        String mailjetPassword = propertiesConfig.getProperty("MAILJET_PASSWORD");
        String mailjetURL = propertiesConfig.getProperty("MAILJET_URL");
        String mailSmtp = propertiesConfig.getProperty("MAIL_SMTP");

        String mailSubscribeSubjet = propertiesConfig.getProperty("MAIL_SUBSCRIBE_CLIENT_SUBJECT");

        String smsURL;

        if (service_sms.equals(GeneralConst.Number.ONE)) {
            smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL);
        } else {
            smsURL = propertiesConfig.getProperty(PropertiesConst.Config.SMS_URL2);
        }
        detailvcherList = PeageBusiness.getDetailVoucherOfVoucher(codeCmd);

        StringBuilder mailContent = null;
        mailContent = new StringBuilder();

        mailContent.append("<html><head> <style>body{background-color: #f6f6f6; font-family: sans-serif; font-size: 14px}.main{background-color: #fff; box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 20px; border-radius: 3px;margin-top: 40px; box-shadow: 8px 10px 0px rgba(0, 0, 0, 0.1)}.footer{clear: both; margin-top: 10px; text-align: center; color: #999; font-size: 12px}</style></head><body>");
        mailContent.append("<div class=main> <p><br/> Bonjour <b>" + personne.toString() + "</b>, <br/> <br/>Voici vos codes vouchers,</p>");
        mailContent.append("<table width=\\\"225\\\" border=\\\"1\\\">");
        if (detailvcherList.size() > 0) {
            for (DetailVoucher dtvch : detailvcherList) {
                mailContent.append("<tr><td><div align=\\\"center\\\"><a/>&nbsp;<span style=\\\"font-size: 5px; text-align: center;\\\"></span><b>" + dtvch.getCodeVoucher() + "</b></div></td></tr>");
            }
        }
        mailContent.append("</table><p><br/> Merci, l'équipe erecettes Haut KATANGA<br/> <br/></p></div><div class=footer> <br/> <span>&copy; Copyright 2019&nbsp;&nbsp;</span><a>Hologram Identification Services</a></div></body></html>");

        MailSender mailSender = new MailSender();
        mailSender.setDate(new Date());
        mailSender.setHost(mailjetURL);
        mailSender.setSubject(mailSubscribeSubjet);
        mailSender.setMessage(mailContent.toString());
        mailSender.setReciever(personne.getLoginWeb().getMail().trim());
        mailSender.setSender(mailSmtp);

        mailSender.sendMailWithAPI(mailjetUser, mailjetPassword);

        return "1";
    }

    private String loadCompteBancaireOfBank(HttpServletRequest request) {

        String result;

        List<JSONObject> jSONObject = new ArrayList<>();

        try {
            String codeBanque = request.getParameter("codeBanque");

            List<CompteBancaire> cptBankList = GestionBanqueBusiness.getCompteBancaireListOfBank(codeBanque);

            if (!cptBankList.isEmpty()) {
                for (CompteBancaire cptBank : cptBankList) {

                    JSONObject object = new JSONObject();
                    object.put("code", cptBank.getCode());
                    object.put("intitule", cptBank.getIntitule());
                    object.put("devise", cptBank.getDevise() == null
                            ? GeneralConst.EMPTY_STRING
                            : cptBank.getDevise().getCode());
                    object.put("codeBank", cptBank.getBanque() == null
                            ? GeneralConst.EMPTY_STRING
                            : cptBank.getBanque().getCode());
                    jSONObject.add(object);
                }
                result = jSONObject.toString();
            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    private String getPreuvePaiement(HttpServletRequest request) {

        String result;

        try {
            String idCommande = request.getParameter("idCommande");

            Commande commande = GestionBanqueBusiness.getPreuvePaiement(idCommande);

            List<JSONObject> jSONObject = new ArrayList<>();

            if (commande != null) {
                JSONObject object = new JSONObject();
                object.put("name", "Preuve paiement");
                object.put("pvDocument", commande.getPreuvePaiement());

                jSONObject.add(object);
                result = jSONObject.toString();

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (Exception e) {
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return result;
    }

    public String validerPaiementCommande(HttpServletRequest request) {

        String result = null;

        try {

            String idCommande = request.getParameter("idCommande");

            if (PeageBusiness.validerPaiementVoucher(idCommande)) {
                result = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String loadCommandeCarte(HttpServletRequest request) {

        List<JSONObject> jsonCmdCarteList = new ArrayList<>();

        JSONObject jsonCmdCarte;
        String valueSearch, typeSearch;

        try {
            List<CommandeCarte> cmdCarteList;

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            String dateDebut = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_DEBUT);
            String dateFin = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_FIN);
            Boolean isAdvancedSearch = Boolean.valueOf(request.getParameter(TaxationConst.ParamName.IS_AVANCED_SEARCH));

            dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            dateFin = ConvertDate.getValidFormatDate(dateFin);

//            Boolean checkRemise = Boolean.valueOf(propertiesConfig.getProperty("checkRemise"));
            if (isAdvancedSearch) {
                cmdCarteList = PeageBusiness.getCommandeCarteByDate(dateDebut, dateFin);
            } else {
                cmdCarteList = PeageBusiness.getCommandeCarte(valueSearch, Integer.valueOf(typeSearch));
            }

            if (cmdCarteList.size() > 0) {

                for (CommandeCarte cmdCarte : cmdCarteList) {

                    jsonCmdCarte = new JSONObject();

                    jsonCmdCarte.put(AssujettissementConst.ParamName.CODE_PERSONNE, cmdCarte.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdCarte.getFkPersonne().getCode());
                    jsonCmdCarte.put(AssujettissementConst.ParamName.RESPONSABLE, cmdCarte.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdCarte.getFkPersonne().toString());
                    jsonCmdCarte.put(AssujettissementConst.ParamName.FORME_JURIDIQUE, cmdCarte.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdCarte.getFkPersonne().getFormeJuridique() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : cmdCarte.getFkPersonne().getFormeJuridique().getIntitule());
                    jsonCmdCarte.put("id", cmdCarte.getId());
                    jsonCmdCarte.put("dateCreat", cmdCarte.getDateCreate() == null
                            ? GeneralConst.EMPTY_STRING
                            : ConvertDate.formatDateHeureToString(cmdCarte.getDateCreate()));
                    jsonCmdCarte.put("dateUpdate", cmdCarte.getDateUpdate() == null
                            ? GeneralConst.EMPTY_STRING
                            : ConvertDate.formatDateHeureToString(cmdCarte.getDateUpdate()));
                    jsonCmdCarte.put("etat", cmdCarte.getEtat());
                    jsonCmdCarte.put("qte", cmdCarte.getQte() == null
                            ? GeneralConst.Numeric.ZERO
                            : cmdCarte.getQte());
                    jsonCmdCarte.put("prixUnitaire", cmdCarte.getPrixUnitaire() == null
                            ? GeneralConst.Numeric.ZERO
                            : cmdCarte.getPrixUnitaire());
                    jsonCmdCarte.put("prixTotal", cmdCarte.getPrixTotal() == null
                            ? GeneralConst.Numeric.ZERO
                            : cmdCarte.getPrixTotal());
                    jsonCmdCarte.put("codeAgentMj", cmdCarte.getAgentUpdate() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdCarte.getAgentUpdate().getCode());
                    jsonCmdCarte.put("nameAgentMj", cmdCarte.getAgentUpdate() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdCarte.getAgentUpdate().toString());
                    jsonCmdCarte.put("reference", cmdCarte.getReference() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdCarte.getReference());
                    jsonCmdCarte.put("codeDevise", cmdCarte.getDevise() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdCarte.getDevise().getCode());

                    jsonCmdCarteList.add(jsonCmdCarte);
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonCmdCarteList.toString();
    }

    public String traiterCmdCarte(HttpServletRequest request) {

        String result = null;
        String codepersonne = GeneralConst.EMPTY_STRING;
        String sigle = GeneralConst.EMPTY_STRING;

        try {
            List<String> codeCmdCarteList = new ArrayList<>();
            String resp;
            String Observation = request.getParameter("observation");
            codepersonne = request.getParameter("codeAssujetti");
            int codeCmd = Integer.valueOf(request.getParameter("codeCmd"));
            int qteCodeCmd = Integer.valueOf(request.getParameter("qteCodeCmd"));
            int agentMaj = Integer.valueOf(request.getParameter("agentMaj"));

            int numberZeroOfCarte = Integer.valueOf(propertiesConfig.getProperty("NUMBER_ZERO_CODE_CARTE"));

            Personne assujetti = IdentificationBusiness.getPersonneByCode(codepersonne);

            if (assujetti != null) {
                sigle = assujetti.getPostnom().toUpperCase().trim();
            }
            codeCmdCarteList = Tools.getCodeList(numberZeroOfCarte, qteCodeCmd, sigle);

            if (PeageBusiness.traiterCmdCodeCarte(codeCmdCarteList, codepersonne, Observation, codeCmd, agentMaj)) {

                result = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                result = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            result = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return result;
    }

    public String getDetailCmdCarte(HttpServletRequest request) {

        List<JSONObject> jsonDetailCmdCarteList = new ArrayList<>();

        JSONObject jsonDetailCmdCarte;

        try {
            List<CarteVoucher> DetailCmdCarteList;

            int codeCmd = Integer.valueOf(request.getParameter("codeCmd"));

            DetailCmdCarteList = PeageBusiness.getCarteVoucherByCmde(codeCmd);

            if (DetailCmdCarteList.size() > 0) {

                for (CarteVoucher carteVchr : DetailCmdCarteList) {

                    jsonDetailCmdCarte = new JSONObject();

                    jsonDetailCmdCarte.put(AssujettissementConst.ParamName.CODE_PERSONNE, carteVchr.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : carteVchr.getFkPersonne().getCode());
                    jsonDetailCmdCarte.put(AssujettissementConst.ParamName.RESPONSABLE, carteVchr.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : carteVchr.getFkPersonne().toString());
                    jsonDetailCmdCarte.put(AssujettissementConst.ParamName.FORME_JURIDIQUE, carteVchr.getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : carteVchr.getFkPersonne().getFormeJuridique() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : carteVchr.getFkPersonne().getFormeJuridique().getIntitule());
                    jsonDetailCmdCarte.put("id", carteVchr.getId());
                    jsonDetailCmdCarte.put("etat", carteVchr.getEtat());
                    jsonDetailCmdCarte.put("codeCmd", carteVchr.getFkCommandeCarte() == null
                            ? GeneralConst.EMPTY_STRING
                            : carteVchr.getFkCommandeCarte().getId());
                    jsonDetailCmdCarte.put("observation", carteVchr.getObservation() == null
                            ? GeneralConst.EMPTY_STRING
                            : carteVchr.getObservation());
                    jsonDetailCmdCarte.put("numeroCarte", carteVchr.getNumeroCarte() == null
                            ? GeneralConst.EMPTY_STRING
                            : carteVchr.getNumeroCarte());

                    jsonDetailCmdCarteList.add(jsonDetailCmdCarte);
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonDetailCmdCarteList.toString();
    }

    public String imprimerCarteNFC(HttpServletRequest request) {

        String result = GeneralConst.EMPTY_STRING;
        String documentHtml = null;
        PrintDocument printDocument = new PrintDocument();
        CartePrint cartePrint = new CartePrint();

        try {

            String numeroReference = request.getParameter("referenceCarte");
            String isToTracking = request.getParameter("isToTracking");
            String idUser = request.getParameter(LoginConst.ParamName.ID_USER);

            CarteVoucher carteVchr = PeageBusiness.getCarteVoucherById(String.valueOf(numeroReference));

            if (carteVchr != null) {

                cartePrint.setNomSociete(carteVchr.getFkPersonne().toString());
                cartePrint.setNumCarteNFC(carteVchr.getNumeroCarte());

                documentHtml = getDocumentHtmlNFC(carteVchr, idUser);

                if (isToTracking.equals(GeneralConst.Number.ZERO)) {
                    documentHtml = documentHtml.replace("hidden=\"true\"", "");
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return documentHtml;
    }

    String getDocumentHtmlNFC(CarteVoucher carteNfc, String idUser) {

        String documentHtml = GeneralConst.EMPTY_STRING;
        String qrCode = GeneralConst.EMPTY_STRING;
        Boolean result = false;

        String url = propertiesConfig.getProperty("URL_TRACKING_DOC").concat(carteNfc.getNumeroCarte());
        String logoHVK = propertiesConfig.getProperty("LOGO_RVA");
        String codeQR = Tools.generateQRCodeV2(url);

        if (codeQR != null && !codeQR.isEmpty()) {
            qrCode = codeQR;
        }

        try {

            documentHtml = propertiesConfig.getProperty("HTML_CARTE_GOPASS");

            Anchor anchor = new Anchor();

            List<Anchor> listAnchor = anchor.getAnchorList(documentHtml);

            for (Anchor monAnchor : listAnchor) {

                String resultatTraitement = Traitement_CarteNFC(monAnchor, carteNfc, qrCode, logoHVK);

                documentHtml = documentHtml.replace(monAnchor.getAnchorTxt(), resultatTraitement);
            }

            result = PeageBusiness.updateCarteNFC(carteNfc);

        } catch (Exception e) {
            throw e;
        }

        return documentHtml;

    }

    public String Traitement_CarteNFC(Anchor anchor, CarteVoucher carteNfc, String qrCode, String logoHVK) {

        String textRetourner = GeneralConst.EMPTY_STRING;

//        Agent agent = ConnexionBusiness.getAgentByCode(Integer.valueOf(idUser));
        switch (anchor.getField().trim()) {
            case GeneralConst.FormatComplement.LOGO_HK:
                textRetourner = logoHVK;
                break;
            case GeneralConst.FormatComplement.NOM_SOCIETE:
                textRetourner = carteNfc.getFkPersonne().toString();
                break;
            case GeneralConst.FormatComplement.NUM_CARTE_NFC:
                textRetourner = carteNfc.getNumeroCarte();
                break;
            case GeneralConst.FormatComplement.QRCODE:
                textRetourner = qrCode;
                break;

        }

        if (!textRetourner.isEmpty()) {
            return textRetourner;
        }

        return textRetourner;
    }

    public String disableCarte(HttpServletRequest request) {

        int idCarte = Integer.valueOf(request.getParameter("idCarte"));
        int etat = Integer.valueOf(request.getParameter("etat"));
        String idUser = request.getParameter(GeneralConst.ID_USER);

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            boolean result = PeageBusiness.disableCarteNFC(idCarte, etat);

            if (result) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadCommandeGopass(HttpServletRequest request) {

        List<JSONObject> jsonCmdVoucherList = new ArrayList<>();

        JSONObject jsonCmdGopass;
        String valueSearch, typeSearch;

        try {
            List<GoPasse> cmdGoPasseList;
//            List<GoPasse> detailGoPasseList;

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            String dateDebut = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_DEBUT);
            String dateFin = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_FIN);
            Boolean isAdvancedSearch = Boolean.valueOf(request.getParameter(TaxationConst.ParamName.IS_AVANCED_SEARCH));

            dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            dateFin = ConvertDate.getValidFormatDate(dateFin);

//            Boolean checkRemise = Boolean.valueOf(propertiesConfig.getProperty("checkRemise"));
            if (isAdvancedSearch) {
                cmdGoPasseList = PeageBusiness.getCommandeGoPasseByDate(dateDebut, dateFin);
            } else {
                cmdGoPasseList = PeageBusiness.getCommandeGoPasse(valueSearch, Integer.valueOf(typeSearch));
            }

            if (cmdGoPasseList.size() > 0) {

                for (GoPasse cmdGpasse : cmdGoPasseList) {

                    jsonCmdGopass = new JSONObject();

                    jsonCmdGopass.put(AssujettissementConst.ParamName.CODE_PERSONNE, cmdGpasse.getFkCommande() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getFkCommande().getFkPersonne());
                    jsonCmdGopass.put(AssujettissementConst.ParamName.RESPONSABLE, cmdGpasse.getFkCommande().getFkPersonne() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getFkCommande().getFkPersonne().toString());
                    jsonCmdGopass.put(AssujettissementConst.ParamName.FORME_JURIDIQUE, cmdGpasse.getFkCommande() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getFkCommande().getFkPersonne().getFormeJuridique() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : cmdGpasse.getFkCommande().getFkPersonne().getFormeJuridique().getIntitule());
                    jsonCmdGopass.put("id", cmdGpasse.getIdGopasse());
                    jsonCmdGopass.put("montant", cmdGpasse.getMontant() == null
                            ? GeneralConst.Number.ZERO
                            : cmdGpasse.getMontant());
                    jsonCmdGopass.put("etat", cmdGpasse.getEtat());
                    jsonCmdGopass.put("devise", cmdGpasse.getDevise() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getDevise());
                    jsonCmdGopass.put("typeGopasse", cmdGpasse.getTypeGopasse() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getTypeGopasse());
                    jsonCmdGopass.put("categorie", cmdGpasse.getFkTarif() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getFkTarif().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : cmdGpasse.getFkTarif().getIntitule());
                    jsonCmdGopass.put("codeAgentMaj", cmdGpasse.getAgentMaj() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getAgentMaj().getCode());
                    jsonCmdGopass.put("nomAgentCreat", cmdGpasse.getAgentMaj() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getAgentMaj().getNom());
                    jsonCmdGopass.put("reference", cmdGpasse.getNumeroGopasse() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getNumeroGopasse());
                    jsonCmdGopass.put("quantite", cmdGpasse.getQuantite() == null
                            ? GeneralConst.Number.ZERO
                            : cmdGpasse.getNumeroGopasse());
                    jsonCmdGopass.put("idNew", cmdGpasse.getNewId() == null
                            ? GeneralConst.EMPTY_STRING
                            : cmdGpasse.getNumeroGopasse());

                    jsonCmdVoucherList.add(jsonCmdGopass);
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonCmdVoucherList.toString();
    }

    public String getDetailGopasse(HttpServletRequest request) {

        List<JSONObject> jsonDetailGopasseList = new ArrayList<>();

        JSONObject jsonDetailGopasse;

        try {
            List<DetailsGopasse> DetailGopassList;

            int codeCmd = Integer.valueOf(request.getParameter("codeGopasse"));

            DetailGopassList = PeageBusiness.getDetailGopasse(codeCmd);

            if (DetailGopassList.size() > 0) {

                for (DetailsGopasse dtlGopass : DetailGopassList) {

                    jsonDetailGopasse = new JSONObject();

                    jsonDetailGopasse.put(AssujettissementConst.ParamName.ID_DETAIL_GOPASS, dtlGopass.getIdDetails());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.ID_GOPASS, dtlGopass.getFkGopasse() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getFkGopasse().getIdGopasse());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.NAME_PASSAGER, dtlGopass.getPassager() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getPassager());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.PIECE_IDENTITE, dtlGopass.getPieceIdentite() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getPieceIdentite());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.SEXE_PASSAGER, dtlGopass.getSexePassager() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getSexePassager());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.DATE_GENERATION, dtlGopass.getDateGeneration() == null
                            ? GeneralConst.EMPTY_STRING
                            : ConvertDate.formatDateHeureToString(dtlGopass.getDateGeneration()));
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.DATE_UTILISATION, 
                            dtlGopass.getDateUtilisation() == null
                            ? GeneralConst.EMPTY_STRING
                            : ConvertDate.formatDateHeureToString(dtlGopass.getDateUtilisation()));
                    jsonDetailGopasse.put("etat", dtlGopass.getEtat());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.CODE_TICKET_GENERATE, dtlGopass.getCodeTicketGenerate() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getCodeTicketGenerate());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.CODE_COMPANIE, dtlGopass.getFkCompagnie() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getFkCompagnie().getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dtlGopass.getFkCompagnie().getCode());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.LIBELLE_COMPANIE, dtlGopass.getFkCompagnie() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getFkCompagnie().toString() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dtlGopass.getFkCompagnie().toString());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.LIBELLE_AEROPORT_PROVENANCE, dtlGopass.getFkAeroportProvenance() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getFkAeroportProvenance().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dtlGopass.getFkAeroportProvenance().getIntitule());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.CODE_AEROPORT_PROVENANCE, dtlGopass.getFkAeroportProvenance() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getFkAeroportProvenance().getCodeAeroport() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dtlGopass.getFkAeroportProvenance().getCodeAeroport());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.CODE_AEROPORT_DESTINATION, dtlGopass.getFkAeroportDestination() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getFkAeroportDestination().getCodeAeroport() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dtlGopass.getFkAeroportDestination().getCodeAeroport());
                    jsonDetailGopasse.put(AssujettissementConst.ParamName.LIBELLE_AEROPORT_DESTINATION, dtlGopass.getFkAeroportDestination() == null
                            ? GeneralConst.EMPTY_STRING
                            : dtlGopass.getFkAeroportDestination().getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : dtlGopass.getFkAeroportDestination().getIntitule());

                    jsonDetailGopasseList.add(jsonDetailGopasse);
                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return jsonDetailGopasseList.toString();
    }
}
