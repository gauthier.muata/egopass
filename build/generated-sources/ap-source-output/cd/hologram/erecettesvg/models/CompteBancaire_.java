package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Banque;
import cd.hologram.erecettesvg.models.BanqueAb;
import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.ServiceCompte;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(CompteBancaire.class)
public class CompteBancaire_ { 

    public static volatile SingularAttribute<CompteBancaire, String> agentMaj;
    public static volatile SingularAttribute<CompteBancaire, String> code;
    public static volatile SingularAttribute<CompteBancaire, Date> dateCreat;
    public static volatile ListAttribute<CompteBancaire, Bordereau> bordereauList;
    public static volatile ListAttribute<CompteBancaire, ServiceCompte> serviceCompteList;
    public static volatile SingularAttribute<CompteBancaire, String> intitule;
    public static volatile SingularAttribute<CompteBancaire, Short> etat;
    public static volatile SingularAttribute<CompteBancaire, Devise> devise;
    public static volatile SingularAttribute<CompteBancaire, Date> dateMaj;
    public static volatile ListAttribute<CompteBancaire, BanqueAb> banqueAbList;
    public static volatile ListAttribute<CompteBancaire, Journal> journalList;
    public static volatile SingularAttribute<CompteBancaire, Banque> banque;
    public static volatile ListAttribute<CompteBancaire, BonAPayer> bonAPayerList;
    public static volatile SingularAttribute<CompteBancaire, String> agentCreat;

}