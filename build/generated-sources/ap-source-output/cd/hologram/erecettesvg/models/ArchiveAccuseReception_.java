package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ArchiveAccuseReception.class)
public class ArchiveAccuseReception_ { 

    public static volatile SingularAttribute<ArchiveAccuseReception, String> fkDocument;
    public static volatile SingularAttribute<ArchiveAccuseReception, String> observation;
    public static volatile SingularAttribute<ArchiveAccuseReception, Date> datecreat;
    public static volatile SingularAttribute<ArchiveAccuseReception, String> documentReference;
    public static volatile SingularAttribute<ArchiveAccuseReception, String> typeDocument;
    public static volatile SingularAttribute<ArchiveAccuseReception, String> archive;
    public static volatile SingularAttribute<ArchiveAccuseReception, Integer> id;
    public static volatile SingularAttribute<ArchiveAccuseReception, Integer> etat;

}