package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ComptePaiement.class)
public class ComptePaiement_ { 

    public static volatile SingularAttribute<ComptePaiement, String> compteBancaire;
    public static volatile SingularAttribute<ComptePaiement, String> typeDocument;
    public static volatile SingularAttribute<ComptePaiement, Integer> id;
    public static volatile SingularAttribute<ComptePaiement, Boolean> etat;

}