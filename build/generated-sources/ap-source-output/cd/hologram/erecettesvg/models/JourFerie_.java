package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(JourFerie.class)
public class JourFerie_ { 

    public static volatile SingularAttribute<JourFerie, Date> dateJourFerie;
    public static volatile SingularAttribute<JourFerie, Boolean> estActif;
    public static volatile SingularAttribute<JourFerie, String> description;
    public static volatile SingularAttribute<JourFerie, Integer> id;
    public static volatile SingularAttribute<JourFerie, String> type;

}