/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalUpdateEcheanceDate;
var dateEcheance, selectTypeDocument, btnUpdateEcheance, referenceDocument;

$(function () {

    mainNavigationLabel.text('MISE A JOUR');
    secondNavigationLabel.text('Echéance paiement');

    removeActiveMenu();
    linkMenuMajEcheance.addClass('active');


    dateEcheance = $('#dateEcheance');
    selectTypeDocument = $('#selectTypeDocument');
    btnUpdateEcheance = $('#btnUpdateEcheance');
    referenceDocument = $('#referenceDocument');

    modalUpdateEcheanceDate = $('#modalUpdateEcheanceDate');
    modalUpdateEcheanceDate.modal('show');


    btnUpdateEcheance.click(function (e) {
        e.preventDefault();

        if (selectTypeDocument.val() === '0') {
            alertify.alert('Veuillez d\'abord sélectionner le document');
            return;
        }

        if (referenceDocument.val() == '') {
            alertify.alert('Veuillez d\'abord saisiir la référence du document');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir mettre à jour l\'échéance de paiement de ce document ?', function () {
            updateEcheancePayment();
        });
    });

});

function updateEcheancePayment() {

    $.ajax({
        type: 'POST',
        url: 'taxation_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'documentCode': selectTypeDocument.val(),
            'dateEcheance': dateEcheance.val(),
            'reference': referenceDocument.val(),
            'operation': 'updateEcheanePayment'
        },
        beforeSend: function () {
            modalUpdateEcheanceDate.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Mise à jour en cours ...</h5>'});
        },
        success: function (response)
        {

            modalUpdateEcheanceDate.unblock();

            setTimeout(function () {

                if (response == '-1') {
                    showResponseError();
                    return;

                } else if (response == '0') {

                    alertify.alert('La référence fournie : ' + referenceDocument.val() + ', n\'existe pas dans la table de : ' + $('#selectTypeDocument option:selected').text());

                } else if (response == '-2') {
                    
                    alertify.alert('La nouvelle échéance est supérieure à la date du jour');
                    return;
                    
                } else {
                    alertify.alert('La mise à jour de l\'échéance de paiement du document');
                    //modalUpdateEcheanceDate.modal('hide');

                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalUpdateEcheanceDate.unblock();
            showResponseError();
        }
    });

}