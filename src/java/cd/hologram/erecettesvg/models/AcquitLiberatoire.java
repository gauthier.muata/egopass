/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ACQUIT_LIBERATOIRE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AcquitLiberatoire.findAll", query = "SELECT a FROM AcquitLiberatoire a"),
    @NamedQuery(name = "AcquitLiberatoire.findByCode", query = "SELECT a FROM AcquitLiberatoire a WHERE a.code = :code"),
    @NamedQuery(name = "AcquitLiberatoire.findByNumeroPapier", query = "SELECT a FROM AcquitLiberatoire a WHERE a.numeroPapier = :numeroPapier"),
    @NamedQuery(name = "AcquitLiberatoire.findByNumeroTimbre", query = "SELECT a FROM AcquitLiberatoire a WHERE a.numeroTimbre = :numeroTimbre"),
    @NamedQuery(name = "AcquitLiberatoire.findByAgentCreat", query = "SELECT a FROM AcquitLiberatoire a WHERE a.agentCreat = :agentCreat"),
    @NamedQuery(name = "AcquitLiberatoire.findByDateCreat", query = "SELECT a FROM AcquitLiberatoire a WHERE a.dateCreat = :dateCreat"),
    @NamedQuery(name = "AcquitLiberatoire.findByAgentMaj", query = "SELECT a FROM AcquitLiberatoire a WHERE a.agentMaj = :agentMaj"),
    @NamedQuery(name = "AcquitLiberatoire.findByDateMaj", query = "SELECT a FROM AcquitLiberatoire a WHERE a.dateMaj = :dateMaj"),
    @NamedQuery(name = "AcquitLiberatoire.findByEtat", query = "SELECT a FROM AcquitLiberatoire a WHERE a.etat = :etat"),
    @NamedQuery(name = "AcquitLiberatoire.findByEtatImpression", query = "SELECT a FROM AcquitLiberatoire a WHERE a.etatImpression = :etatImpression"),
    @NamedQuery(name = "AcquitLiberatoire.findBySite", query = "SELECT a FROM AcquitLiberatoire a WHERE a.site = :site"),
    @NamedQuery(name = "AcquitLiberatoire.findByUtilise", query = "SELECT a FROM AcquitLiberatoire a WHERE a.utilise = :utilise"),
    @NamedQuery(name = "AcquitLiberatoire.findByObservation", query = "SELECT a FROM AcquitLiberatoire a WHERE a.observation = :observation")})
public class AcquitLiberatoire implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 25)
    @Column(name = "NUMERO_PAPIER")
    private String numeroPapier;
    @Size(max = 25)
    @Column(name = "NUMERO_TIMBRE")
    private String numeroTimbre;
    @Size(max = 20)
    @Column(name = "AGENT_CREAT")
    private String agentCreat;
    @Size(max = 10)
    @Column(name = "DATE_CREAT")
    private String dateCreat;
    @Size(max = 20)
    @Column(name = "AGENT_MAJ")
    private String agentMaj;
    @Size(max = 10)
    @Column(name = "DATE_MAJ")
    private String dateMaj;
    @Column(name = "ETAT")
    private Boolean etat;
    @Column(name = "ETAT_IMPRESSION")
    private Boolean etatImpression;
    @Size(max = 25)
    @Column(name = "SITE")
    private String site;
    @Column(name = "UTILISE")
    private Boolean utilise;
    @Size(max = 50)
    @Column(name = "OBSERVATION")
    private String observation;
    @JoinColumn(name = "NOTE_PERCEPTION", referencedColumnName = "NUMERO")
    @ManyToOne
    private NotePerception notePerception;
    @JoinColumn(name = "RECEPTIONNISTE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne receptionniste;

    public AcquitLiberatoire() {
    }

    public AcquitLiberatoire(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNumeroPapier() {
        return numeroPapier;
    }

    public void setNumeroPapier(String numeroPapier) {
        this.numeroPapier = numeroPapier;
    }

    public String getNumeroTimbre() {
        return numeroTimbre;
    }

    public void setNumeroTimbre(String numeroTimbre) {
        this.numeroTimbre = numeroTimbre;
    }

    public String getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(String agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(String dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(String agentMaj) {
        this.agentMaj = agentMaj;
    }

    public String getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(String dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Boolean getEtatImpression() {
        return etatImpression;
    }

    public void setEtatImpression(Boolean etatImpression) {
        this.etatImpression = etatImpression;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Boolean getUtilise() {
        return utilise;
    }

    public void setUtilise(Boolean utilise) {
        this.utilise = utilise;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public NotePerception getNotePerception() {
        return notePerception;
    }

    public void setNotePerception(NotePerception notePerception) {
        this.notePerception = notePerception;
    }

    public Personne getReceptionniste() {
        return receptionniste;
    }

    public void setReceptionniste(Personne receptionniste) {
        this.receptionniste = receptionniste;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AcquitLiberatoire)) {
            return false;
        }
        AcquitLiberatoire other = (AcquitLiberatoire) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.AcquitLiberatoire[ code=" + code + " ]";
    }
    
}
