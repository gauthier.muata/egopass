package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.TypeComplementBien;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AbComplementBien.class)
public class AbComplementBien_ { 

    public static volatile SingularAttribute<AbComplementBien, ArticleBudgetaire> articleBudgetaire;
    public static volatile SingularAttribute<AbComplementBien, String> code;
    public static volatile SingularAttribute<AbComplementBien, Date> dateCreat;
    public static volatile SingularAttribute<AbComplementBien, TypeComplementBien> typeComplementBien;
    public static volatile SingularAttribute<AbComplementBien, Integer> agentCreat;
    public static volatile SingularAttribute<AbComplementBien, BigInteger> etat;

}