package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.CategorieTypeComplement;
import cd.hologram.erecettesvg.models.ComplementForme;
import cd.hologram.erecettesvg.models.TypeComplementBien;
import cd.hologram.erecettesvg.models.ValeurPredefinie;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TypeComplement.class)
public class TypeComplement_ { 

    public static volatile ListAttribute<TypeComplement, ComplementForme> complementFormeList;
    public static volatile SingularAttribute<TypeComplement, Boolean> valeurPredefinie;
    public static volatile SingularAttribute<TypeComplement, String> code;
    public static volatile SingularAttribute<TypeComplement, Boolean> taxable;
    public static volatile SingularAttribute<TypeComplement, Boolean> baseVariable;
    public static volatile SingularAttribute<TypeComplement, Boolean> obligatoire;
    public static volatile SingularAttribute<TypeComplement, String> type;
    public static volatile SingularAttribute<TypeComplement, String> intitule;
    public static volatile SingularAttribute<TypeComplement, Boolean> etat;
    public static volatile ListAttribute<TypeComplement, ValeurPredefinie> valeurPredefinieList;
    public static volatile SingularAttribute<TypeComplement, Boolean> proprietaire;
    public static volatile SingularAttribute<TypeComplement, CategorieTypeComplement> fkCategorieTypeCompelement;
    public static volatile SingularAttribute<TypeComplement, String> objetInteraction;
    public static volatile ListAttribute<TypeComplement, TypeComplementBien> typeComplementBienList;

}