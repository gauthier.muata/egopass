package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(InteretMoratoire.class)
public class InteretMoratoire_ { 

    public static volatile SingularAttribute<InteretMoratoire, String> moisInteret;
    public static volatile SingularAttribute<InteretMoratoire, Date> dateCreat;
    public static volatile SingularAttribute<InteretMoratoire, BigDecimal> soldeTotal;
    public static volatile SingularAttribute<InteretMoratoire, String> libelle;
    public static volatile SingularAttribute<InteretMoratoire, BigDecimal> interetGenere;
    public static volatile SingularAttribute<InteretMoratoire, Integer> id;
    public static volatile SingularAttribute<InteretMoratoire, String> fkAmr;

}