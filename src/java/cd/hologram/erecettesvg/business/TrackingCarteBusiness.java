/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.Carte;
import cd.hologram.erecettesvg.sql.SQLQueryTrackingCarte;
import cd.hologram.erecettesvg.util.Casting;
import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class TrackingCarteBusiness {

    private static final Dao myDao = new Dao();

    public static List<Carte> getDetailsCarte(String numero) {
        try {
            String query = SQLQueryTrackingCarte.SELECT_CARTE;
            List<Carte> cartes = (List<Carte>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Carte.class), false, numero);
            return cartes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static String checkValidateValue(String value, String sw) {

        String queryStr = "";

        queryStr = "SELECT DBO.F_CHECK_VALIDATE_VALUE('" + value.trim() + "','" + sw + "')";

        String result = myDao.getDaoImpl().getSingleResultToString(queryStr);
        return result;
    }

    public static Carte getCarteByFkConducteurVehicule(String id) {
        try {

            String query = "SELECT C.* FROM T_CARTE C WITH (READPAST) WHERE C.FK_CONDUCTEUR_VEHICULE = ?1 AND C.ETAT = 1";

            List<Carte> cartes = (List<Carte>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Carte.class), false, id);
            return cartes.isEmpty() ? null : cartes.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

}
