/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var data;
var _id;
var _montant;
var _devise;

var cmbMinistere, cmbService;

var inputExercice, inputMontantAssign;

var tableSecteurDactivite, tableArticleBudgetaire;
var cmbFaitgenerateur;
var modalMinistereService, exerciceFiscal;
var btnSearchArt, btnModifyAssignation;

$(function () {

    mainNavigationLabel.text('ASSIGNATION BUDGETAIRE');

    secondNavigationLabel.text('Registre assignation');

    btnSearchArt = $('#btnSearchArt');
    btnModifyAssignation = $('#btnModifyAssignation');
    exerciceFiscal = $('#exerciceFiscal');
    cmbService = $('#cmbService');

    inputMontantAssign = $('#inputMontantAssign');

    btnSearchArt.on('click', function (e) {
        e.preventDefault();
        loadAssignationBudgetaire();
    });

    btnModifyAssignation.on('click', function (e) {
        e.preventDefault();

        if (inputMontantAssign.val() == '' || inputMontantAssign.val() == 0) {
            alertify.alert('Veuillez saisir un montant correct.');
            return;
        }

        alertify.confirm('Etes-vous sûrs de vouloir modifier le montant ?', function () {

            _montant = inputMontantAssign.val();
            //_montant = _montant.replaceAll(',', '');
            _montant = replaceAll(_montant, ',', '');

            data = {};
            data.operation = 'updateAssignationMontant';
            data.id = _id;
            data.montant = _montant;
            send(data.operation);
        });

    });

    //inputExercice = $('#inputExercice');

    //modalMinistereService = $('#modalMinistereService');

    tableArticleBudgetaire = $('#tableArticleBudgetaire');

    //loadMinistere();

    printArticleBudgetaire('');
    loadExercice();
    loadAllService();

});

function send(operation) {

    $.ajax({
        type: 'POST',
        url: 'assignationBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: data,
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            $.unblockUI();

            if (response == '-1') {
                showResponseError();
                return;
            }

            callback(JSON.parse(JSON.stringify(response)), operation);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function callback(response, operation) {

    var content;
    switch (operation) {
        case 'loadAssignationByExercice':
            printArticleBudgetaire(response);
            break;
        case 'loadAllservices':
            content = '<option value="0">--- Sélectionner ---</option>';
            for (var i = 0; i < response.length; i++) {
                content += '<option value="' + response[i].code + '">' + response[i].intitule + '</option>';
            }
            cmbService.html(content);
            break;
        case 'updateAssignationMontant':
            if (response == '1') {
                var montant = inputMontantAssign.val();
                //montant = +montant.replaceAll(',', '');
                montant = +replaceAll(montant, ',', '');
                $('#amountId_' + _id).html(montant);

                montant = formatNumber(montant, _devise);
                $('#amountAssign_' + _id).html(montant);

                $('#td_edition_' + _id).html('<a onclick="editAmountAssignation(\'' + _id + '\',\'' + _devise + '\')" class="btn btn-success"><i class="fa fa-edit"></i></a>');
                $('#modalModifyAssignation').modal('hide');
            } else {
                alertify.alert('Impossible de modifier le montant.');
            }
            break;
    }
}

function loadAssignationBudgetaire() {

    data = {};
    data.operation = 'loadAssignationByExercice';
    data.secteur = cmbService.val();
    data.exerciceFiscal = exerciceFiscal.val();

    send(data.operation);
}

function printArticleBudgetaire(articleBudgetaireList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    //header += '<th style="width:10%" scope="col"> Code officiel</th>';
    header += '<th scope="col"> Fait générateur </th>';
    header += '<th scope="col"> Article budégtaire </th>';
    header += '<th scope="col" > Date Assignation </th>';
    //header += '<th scope="col"> Montant ordonancé </th>';
    //header += '<th scope="col" style="text-align: right;"> Montant recouvré </th>';
    header += '<th scope="col" style="text-align: right;"> Assignation </th>';
    header += '<th scope="col" style="text-align: right;display:none"> Assignation </th>';

    //header += '<th scope="col"> Période </th>';
    header += '<th ></th>';
    header += '</tr></thead>';

    var body = '<tbody>';
    //var totRecouvre = 0;
    var totAssignation = 0;

    for (var i = 0; i < articleBudgetaireList.length; i++) {
        ecart = 0;

        //ecart = articleBudgetaireList[i].totalRealisation - articleBudgetaireList[i].assignation;
        //totRecouvre += articleBudgetaireList[i].totalRealisation;
        totAssignation += articleBudgetaireList[i].assignation;

        //totEcart += ecart;

        body += '<tr >';
        //body += '<td>' + articleBudgetaireList[i].codeofficiel + '</td>';
        body += '<td>' + articleBudgetaireList[i].faitgenerateur + '</td>';
        body += '<td>' + articleBudgetaireList[i].intitule + '</td>';
        body += '<td >' + articleBudgetaireList[i].dateassignation + '</td>';
        //body += '<td></td>';
        //body += '<td style="text-align: right;">' + formatNumber(articleBudgetaireList[i].totalRealisation, 'USD') + '</td>';
        body += '<td style="text-align: right;" id="amountAssign_' + articleBudgetaireList[i].id + '">' + formatNumber(articleBudgetaireList[i].assignation, articleBudgetaireList[i].devise) + '</td>';
        body += '<td style="text-align: right;display:none" id="amountId_' + articleBudgetaireList[i].id + '">' + articleBudgetaireList[i].assignation + '</td>';

        //body += '<td>' + articleBudgetaireList[i].intituleperiode + '</td>';
        body += '<td style="vertical-align:middle;text-align:center" id="td_edition_' + articleBudgetaireList[i].id + '"><a onclick="editAmountAssignation(\'' + articleBudgetaireList[i].id + '\',\'' + articleBudgetaireList[i].devise + '\')" class="btn btn-success"><i class="fa fa-edit"></i></a></td>';
        body += '</tr>';
    }

    body += '<tfoot>';

    body += '<tr><th colspan="3" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red" id="lblTotalGeneral"></th><th style="text-align:right;font-size:18px;color:red" id="lblTotalGeneral"></th></tr>';

    body += '</tfoot>';

    body += '</tbody>';
    var tableContent = header + body;
    tableArticleBudgetaire.html(tableContent);
    var dtArticleBudgetaire = tableArticleBudgetaire.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        columnDefs: [
            {"visible": false, "targets": 0}
        ],
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(0, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="9">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        },
        footerCallback: function (row, data, start, end, display) {

            var api = this.api(), data;

            $(api.column(3).footer()).html(
                    formatNumber(totAssignation, 'CDF'));
        }
    });
    $('#tableArticleBudgetaire tbody').on('click', 'tr', function () {
        var data = dtArticleBudgetaire.row(this).data();
    });
}

function loadExercice() {

    var anneeDebut = 2000;
    var date = new Date();
    var anneeFin = date.getFullYear();
    var dataAnnee = '<option value ="0">-- Année --</option>';
    while (anneeDebut <= anneeFin) {
        dataAnnee += '<option value =' + anneeDebut + '> ' + anneeDebut + '</option>';
        anneeDebut++;
    }
    exerciceFiscal.html(dataAnnee);
}

function loadAllService() {
    data = {};
    data.operation = 'loadAllservices';
    //data.codeMinistere = codeMinistere;
    send(data.operation);
}

function editAmountAssignation(id, devise) {

    var mont = $('#amountId_' + id).text();
    $('#inputMontantAssign').val(mont);
    formatting($('#inputMontantAssign'));

    $('#labelModifyAssign').html('Montant assigné (' + devise + ') <span style="color:red"> *</span>');
    $('#modalModifyAssignation').modal('show');

    _id = id;
    _devise = devise;
}

function activaTab(tab) {
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
}