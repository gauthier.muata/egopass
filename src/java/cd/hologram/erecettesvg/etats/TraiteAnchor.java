package cd.hologram.erecettesvg.etats;

/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/

import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import java.util.Map;

/**
*
* @author hids
*/
public class TraiteAnchor {

    public TraiteAnchor() {
    }

    public String Traitement(Anchor anchor, Map dic) {
        String textRetourner = GeneralConst.EMPTY_STRING;
        if (!anchor.getTable().equals(GeneralConst.EMPTY_STRING)) {
            if (!anchor.getTable().isEmpty()) {
                String[] table = anchor.getTable().split(GeneralConst.VIRGULE);

                String[] field = anchor.getField().split(GeneralConst.VIRGULE);
                if (table.length > 1) {
                    try {
                        Class cl = dic.get(table[1]).getClass();
                        for (int i = 0; i < field.length; i++) {
                            String string = field[i];
                            string = String.valueOf(string.charAt(0)).toUpperCase().concat(string.substring(1));
                            try {
                                java.lang.reflect.Method m = cl.getMethod(DocumentConst.DocumentParamName.GET.concat(string.trim()));
                                try {
                                    textRetourner = textRetourner + GeneralConst.SPACE.concat(String.valueOf(m.invoke(dic.get(table[1]))));
                                } catch (Exception ex) {
                                }
                            } catch (NoSuchMethodException ex) {
                                textRetourner = textRetourner + GeneralConst.SPACE.concat(string);
                            } catch (java.lang.NullPointerException ex) {
                                return GeneralConst.EMPTY_STRING;
                            }
                        }
                    } catch (Exception e) {
                        textRetourner = "<input title=\"" + anchor.getTitre() + "\" type=\"text\" size=\"" + anchor.getTitre().length() + "\" name=\"input" + anchor.getIndex() + "\" id=\"input" + anchor.getIndex() + "\" >";
                    }
                }
                textRetourner = textRetourner + GeneralConst.EMPTY_STRING;
            }
        } else if (anchor.getAttribut().equalsIgnoreCase(DocumentConst.DocumentParamName.DEFAULT_TEXT)) {
            textRetourner = "<textarea name=\"area" + anchor.getIndex() + "\" title=\"" + anchor.getTitre() + "\" cols=\"100\" rows=\"\">" + anchor.getField() + "</textarea>";
        } else {
            textRetourner = "<input type= \"text\" placeholder=\"" + anchor.getTitre() + "\" title=\"" + anchor.getTitre() + "\" name=\"input" + anchor.getIndex() + "\" >";
        }
        return textRetourner;
    }
}
