/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var cbxResearchTypeCommandeVoucher;

var btnSearchCommandeVoucher, btnShowAdvancedSearchModal,
        btnAdvencedSearch, btnSelectedCompteBancaire;

var inputSearchCommandeVoucher, inputDateDebut, inputdateLast;

var codeResearchCommande;

var tableCommandeVoucher;

var modalRechercheAvanceeModelTwo, modalSelectedCompteBancaire;

var tempCmdVoucherList, tempDetailCmdVoucheList, tempCmdGopassList, tempDetailGopass;

var checkLoad = true;

var checkUpdate = false;

var checkRemise;

var codePersonne, cmdId, codeCompteBancaire,
        codeBancaire, codeCmdGopass;

var lblDateDebut, lblDateFin, isAdvance;

var cmbBanques, cmbCompteBancaires;
var dataBankList;

var voucherList = [];

$(function () {

    mainNavigationLabel.text('PEAGE');
    secondNavigationLabel.text('Registre des commandes go-pass');

    tableCommandeVoucher = $('#tableCommandeVoucher');
    modalRechercheAvanceeModelTwo = $('#modalRechercheAvanceeModelTwo');
    modalSelectedCompteBancaire = $('#modalSelectedCompteBancaire');

    cbxResearchTypeCommandeVoucher = $('#cbxResearchTypeCommandeVoucher');

    cmbCompteBancaires = $('#cmbCompteBancaires');
    cmbBanques = $('#cmbBanques');

    btnSearchCommandeVoucher = $('#btnSearchCommandeVoucher');
    btnShowAdvancedSearchModal = $('#btnShowAdvancedSearchModal');
    btnAdvencedSearch = $('#btnAdvencedSearch');
    btnSelectedCompteBancaire = $('#btnSelectedCompteBancaire');

    inputSearchCommandeVoucher = $('#inputSearchCommandeVoucher');
    inputdateLast = $('#inputdateLast');
    inputDateDebut = $('#inputDateDebut');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    isAdvance = $('#isAdvance');

    btnShowAdvancedSearchModal.click(function (e) {
        e.preventDefault();
        checkUpdate = true;
        modalRechercheAvanceeModelTwo.modal('show');
    });

    btnSearchCommandeVoucher.click(function (e) {
        e.preventDefault();
        if (inputSearchCommandeVoucher.val() === empty) {
            alertify.alert('Veuillez fournir un critère de recherche ');
            return;
        }
//        loadCommandeVoucher(false);
        loadCommandeGopass(false);
    });

    cbxResearchTypeCommandeVoucher.on('change', function (e) {

        e.preventDefault();

        codeResearchCommande = cbxResearchTypeCommandeVoucher.val();
        checkLoad = true;

        if (codeResearchCommande === "1") {
            inputSearchCommandeVoucher.attr('placeholder', 'Le nom de l\'assujetti');
            inputSearchCommandeVoucher.val('');
        } else {
            inputSearchCommandeVoucher.attr('placeholder', 'Reference commande');
            inputSearchCommandeVoucher.val('');
        }
    });

    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeModelTwo.modal('hide');
//        loadCommandeVoucher(true);
        loadCommandeGopass(true);
    });

    btnSelectedCompteBancaire.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (cmbCompteBancaires.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner un compte bancire');
            return;
        }
        modalSelectedCompteBancaire.modal('hide');
        if (checkRemise) {
            checkVaucher();
        } else {
            ajustValidation(cmdId);
            checkVaucher();
        }

    });

    cmbBanques.on('change', function () {
        codeBancaire = cmbBanques.val();
        getCompteBancairfeOfBank(codeBancaire);
    });

    cmbCompteBancaires.on('change', function () {
        codeCompteBancaire = cmbCompteBancaires.val();
    });

    if (checkLoad) {
        codeResearchCommande = cbxResearchTypeCommandeVoucher.val();
        checkLoad = false;
    }

//    printCommandeVoucher('');
    printCommandeGopass('');
//    loadCommandeVoucher(false);
    loadCommandeGopass(false);
    getBankList();
});

function loadCommandeVoucher(checkAdvance) {

    if (checkAdvance) {

        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());
        isAdvance.attr('style', 'display: block');

    } else {

        isAdvance.attr('style', 'display: none');
    }


    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'valueSearch': inputSearchCommandeVoucher.val(),
            'typeSearch': codeResearchCommande,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'isAdvancedSearch': checkAdvance,
            'operation': 'loadCommandeVoucher'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();

                tempCmdVoucherList = null;

                tempCmdVoucherList = JSON.parse(JSON.stringify(response));

                printCommandeVoucher(tempCmdVoucherList);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}


//function printCommandeVoucher(tempCmdVouchList) {
//
//    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
//    header += '<th scope="col"> </th>';
//    header += '<th scope="col" style="width:15%"> REFERENCE </th>';
//    header += '<th scope="col" style="width:20%"> ASSUJETTI </th>';
//    header += '<th scope="col" style="width:15%"> TYPE ASSUJETTI </th>';
//    header += '<th scope="col" style="width:15%"> DATE </th>';
//    header += '<th scope="col" style="width:15%"> ETAT </th>';
//    header += '<th scope="col" style="width:15%"> MONTANT </th>';
//    header += '<th hidden="true" scope="col"> Code Personne </th>';
//    header += '<th scope="col"> </th>';
//    header += '</tr></thead>';
//
//    var body = '<tbody id="tbodyBien">';
//
//    for (var i = 0; i < tempCmdVouchList.length; i++) {
//
//        var etatLibelle;
//        var btnPreuvePaiement = '';
//        var btnValiderPaiement = '';
//
//        switch (tempCmdVouchList[i].etat) {
//            case 1 :
//                etatLibelle = 'Payé';
//                if (tempCmdVouchList[i].preuvePaiement) {
//                    btnPreuvePaiement = '<button class="btn btn-primary" onclick="showPreuve(\'' + tempCmdVouchList[i].id + '\')"><i class="fa fa-eye"></i></button>'
//                }
//                break;
//            case 2 :
//                etatLibelle = 'Validé';
//                if (tempCmdVouchList[i].preuvePaiement) {
//                    btnValiderPaiement = '<button class="btn btn-primary" onclick="validerPaiement(\'' + tempCmdVouchList[i].id + '\')"><i class="fa fa-check"></i></button>'
//                }
//                break;
//            case 3 :
//                etatLibelle = 'En attente de validation';
//                break;
//            case 4 :
//                etatLibelle = 'Livré';
//                break;
//        }
//
//        body += '<tr>';
//        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
//        body += '<td style="vertical-align:middle;">' + tempCmdVouchList[i].reference + '</td>';
//        body += '<td style="vertical-align:middle;">' + tempCmdVouchList[i].responsable + '</td>';
//        body += '<td style="vertical-align:middle;">' + tempCmdVouchList[i].formeJuridique + '</td>';
//        body += '<td style="vertical-align:middle;">' + tempCmdVouchList[i].dateCreat + '</td>';
//        body += '<td style="vertical-align:middle;">' + etatLibelle + '</td>';
//        body += '<td style="vertical-align:middle;" id="totalCmd_' + tempCmdVouchList[i].id + '">' + formatNumber(tempCmdVouchList[i].montant, tempCmdVouchList[i].devise) + '</td>';
//        body += '<td hidden="true">' + tempCmdVouchList[i].id + '</td>';
//        body += '<td style="text-align:center;vertical-align:middle;width:4%">' + btnPreuvePaiement + btnValiderPaiement+'</td>';
//        body += '</tr>';
//    }
//
//    body += '</tbody>';
//
//    var tableContent = header + body;
//
//    tableCommandeVoucher.html(tableContent);
//
//    var dataDetailCmdVcher = tableCommandeVoucher.DataTable({
//        language: {
//            processing: "Traitement en cours...",
//            track: "Rechercher&nbsp;:",
//            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
//            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
//            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
//            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
//            infoPostFix: "",
//            loadingRecords: "Chargement en cours...",
//            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
//            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
//            search: "Rechercher par le nom _INPUT_  ",
//            paginate: {
//                first: "Premier",
//                previous: "Pr&eacute;c&eacute;dent",
//                next: "Suivant",
//                last: "Dernier"
//            },
//            aria: {
//                sortAscending: ": activer pour trier la colonne par ordre croissant",
//                sortDescending: ": activer pour trier la colonne par ordre décroissant"
//            }
//        },
//        info: false,
//        destroy: true,
//        searching: false,
//        paging: true,
//        lengthChange: false,
//        tracking: false,
//        ordering: false,
//        pageLength: 50,
//        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
//        select: {
//            style: 'os',
//            blurable: true
//        },
//        datalength: 3
//    });
//    $('#tableCommandeVoucher tbody').on('click', 'td.details-control', function () {
//
//        var tr = $(this).closest('tr');
//        var row = dataDetailCmdVcher.row(tr);
//        var dataDetail = dataDetailCmdVcher.row(tr).data();
//
//        cmdId = dataDetail[7];
//
//        if (row.child.isShown()) {
//
//            row.child.hide();
//            tr.removeClass('shown');
//        }else {
//
//            var detailTable = '<center><h4>LES DETAILS COMMANDES </h4><br/></center>';
//            detailTable += '<table class="table table-bordered">';
//            detailTable += '<thead><tr style="background-color:#e6ceac;color:black">';
//            detailTable += '<td style="text-align:center">AXE</td>';
//            detailTable += '<td style="text-align:center">TARIF</td>';
//            detailTable += '<td style="text-align:center">QUANTITE</td>';
//            detailTable += '<td style="text-align:center">PRIX UNITAIRE</td>';
//            detailTable += '<td style="text-align:center">MONTANT</td>';
//            detailTable += '<td style="text-align:center">REMISE</td>';
//            detailTable += '<td style="text-align:center">MONTANT FINAL</td>';
//            detailTable += '<th scope="col"> </th>';
//            detailTable += '</tr></thead>';
//            detailTable += '<tbody>';
//
//            var etatCmd = 0;
//            var idCmd;
//
//            for (var i = 0; i < tempCmdVoucherList.length; i++) {
//
//                if (tempCmdVoucherList[i].id == cmdId) {
//
//                    voucherList = [];
//
//                    etatCmd = tempCmdVoucherList[i].etat;
//                    idCmd = tempCmdVoucherList[i].id;
//                    checkRemise = tempCmdVoucherList[i].checkRemise;
//
//                    tempDetailCmdVoucheList = JSON.parse(tempCmdVoucherList[i].detailCmdVoucheList);
//
//                    for (var j = 0; j < tempDetailCmdVoucheList.length; j++) {
//
//                        var inputNumber = "inputNumber_" + j;
//                        var finalMount = "mountFinal_" + j;
//
//                        detailTable += '<tr>';
//                        detailTable += '<td style="text-align:center;width:18%;">' + tempDetailCmdVoucheList[j].intituleAxe + '</td>';
//                        detailTable += '<td style="text-align:center;width:18%;">' + tempDetailCmdVoucheList[j].intituleTarif + '</td>';
//                        detailTable += '<td style="text-align:center;width:13%;">' + tempDetailCmdVoucheList[j].qte + '</td>';
//                        detailTable += '<td style="text-align:center;width:13%;">' + tempDetailCmdVoucheList[j].prixUnitaire + '</td>';
//                        detailTable += '<td style="text-align:center;width:13%;">' + formatNumber(tempDetailCmdVoucheList[j].montantVoucher, tempDetailCmdVoucheList[j].codeDevise) + '</td>';
//                        if (tempDetailCmdVoucheList[j].checkRemise) {
//                            if (etatCmd == 3) {
//                                detailTable += '<td style="text-align:center;width:13%;"><input id="' + inputNumber + '" onchange="ajustMontant(\'' + j + '\')" type="number" min="0" max="100" style="width : 100%"></input></td>';
//                            } else {
//                                detailTable += '<td style="text-align:center;width:13%;">' + tempDetailCmdVoucheList[j].remise + ' % </td>';
//                            }
//                        } else {
//                            detailTable += '<td style="text-align:center;width:13%;">' + tempDetailCmdVoucheList[j].remise + ' % </td>';
//                        }
//
//
//                        detailTable += '<td style="text-align:center;width:13%;" id="' + finalMount + '">' + formatNumber(tempDetailCmdVoucheList[j].montantFinal, tempDetailCmdVoucheList[j].codeDevise) + '</td>';
//                        detailTable += '</tr>';
//                    }
//
//                    break;
//                }
//
//            }
//
//            detailTable += '</tbody>';
//
//            if (etatCmd == 3) {
//
//                detailTable += '<tfoot>';
//                detailTable += '<tr><th colspan="7" style="vertical-align:middle;text-align:right"><button class="btn btn-success" type="submit" id="btnvalidateCmd"><i class="fa fa-check-circle"></i> Valider la commande</button></th></tr>';
//                detailTable += '</tfoot>';
//            }
////            if (etatCmd == 1) {
////
////                detailTable += '<tfoot>';
////                detailTable += '<tr><th colspan="7" style="vertical-align:middle;text-align:right"><a onclick="preparationSend()" class="btn btn-success" type="submit" id="btnEnvoyerVchr">Envoyer code voucher <i class="fa fa-arrow-circle-right"></i></a></th></tr>';
////                detailTable += '</tfoot>';
////            }
//
//            detailTable += '</table>';
//            row.child(detailTable).show();
//            tr.addClass('shown');
//
//            btnvalidateCmd = $('#btnvalidateCmd');
//
//            btnvalidateCmd.click(function (e) {
//                e.preventDefault();
//                modalSelectedCompteBancaire.modal('show');
//            });
//
//            btnEnvoyerVchr = $('#btnEnvoyerVchr');
//
////            btnEnvoyerVchr.click(function (e) {
////                e.preventDefault();
////                alertify.confirm('Etes-vous sûre d\'envoyer ces codes vouchers ?', function () {
////                    updateVoucher(false);
////                });
////            });
//        }
//
//    });
//}

function ajustMontant(indice) {

    for (var i = 0; i < tempDetailCmdVoucheList.length; i++) {

        if (indice == i) {

            var row = tempDetailCmdVoucheList[i];

            var montVoucher = parseFloat(row.montantVoucher);
            var percentRemise = $("#inputNumber_" + i).val();
            var montantFinal = row.montantVoucher - (parseFloat(percentRemise) * montVoucher / 100);
            $("#mountFinal_" + i).html(formatNumber(montantFinal, row.codeDevise));

            voucher = {};
            voucher.id = row.id;
            voucher.codeAssujetti = row.codeAssujetti;
            voucher.montantFinal = montantFinal;
            voucher.remise = percentRemise;
            voucher.idCmd = row.idCmd;
            voucher.reference = row.reference;
            voucher.checkRemise = row.checkRemise;
            voucherList.push(voucher);

            break;
        }

    }

}

function checkVaucher() {
    if (checkRemise) {
        if (voucherList.length < tempDetailCmdVoucheList.length) {
            alertify.alert('Veuillez définir le(s) remise(s)');
            return;
        }
    }
    alertify.confirm('Etes-vous sûre de valider cette commande ?', function () {

        updateVoucher(true);

    });
}

function updateVoucher(checkOperation) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'updateVoucher',
            'agentMaj': userData.idUser,
            'codeCompteBancaire': codeCompteBancaire,
            'checkOperation': checkOperation,
            'detailCmdVoucheList': JSON.stringify(voucherList)
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation de la commande ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\'opération effectuée avec succès.');

                    if (checkUpdate) {
                        loadCommandeVoucher(true);
                    } else {
                        loadCommandeVoucher(false);
                    }
                    codeCompteBancaire = '';

                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de cette opération.');
                } else if (response == '3') {
                    alertify.alert('L\'opération effectuée avec succès sans notification.');
                    if (checkUpdate) {
                        loadCommandeVoucher(true);
                    } else {
                        loadCommandeVoucher(false);
                    }
                    codeCompteBancaire = '';
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function preparationSend() {

    for (var i = 0; i < tempDetailCmdVoucheList.length; i++) {

        voucher = {};
        voucher.id = tempDetailCmdVoucheList[i].id;
        voucher.codeAssujetti = tempDetailCmdVoucheList[i].codeAssujetti;
        voucher.montantFinal = tempDetailCmdVoucheList[i].montantFinal;
        voucher.remise = tempDetailCmdVoucheList[i].remise;
        voucher.idCmd = tempDetailCmdVoucheList[i].idCmd;
        voucher.reference = tempDetailCmdVoucheList[i].reference;
        voucherList.push(voucher);
    }
    alertify.confirm('Etes-vous sûre d\'envoyer ces codes vouchers ?', function () {
        updateVoucher(false);
    });
}

function getBankList() {
    var dataBank = '<option value ="0">-- Sélectionner --</option>';
    dataBankList = JSON.parse(userData.banqueUserList);

    for (var i = 0; i < dataBankList.length; i++) {
        dataBank += '<option value="' + dataBankList[i].banqueCode + '" >' + dataBankList[i].banqueName + '</option>';
    }
    cmbBanques.html(dataBank);
}

function getCompteBancairfeOfBank(codeBanque) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'codeBanque': codeBanque,
            'operation': 'loadCompteBancaireOfBank'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {
                    var dataCompteBancaireList = $.parseJSON(JSON.stringify(response));

                    if (dataCompteBancaireList.length > 0) {
                        var dataCompteBancaire = '<option value ="0">-- Sélectionner --</option>';
                        for (var i = 0; i < dataCompteBancaireList.length; i++) {
                            dataCompteBancaire += '<option value =' + dataCompteBancaireList[i].code + '>' + dataCompteBancaireList[i].intitule + '</option>';
                        }
                        cmbCompteBancaires.html(dataCompteBancaire);
                        codeBancaire = '';
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function showPreuve(idCommande) {
    getPreuvePaiement(idCommande);
}


function getPreuvePaiement(idCommande) {
    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getPreuvePaiement',
            'idCommande': idCommande

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche de la preuve de paiement en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de cette opération.');
                } else if (response == '-1') {
                    showResponseError();
                } else {
                    var dataPreuve = JSON.stringify(response);

                    initGeneralPrintData(dataPreuve);
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function ajustValidation(idCmd) {

    for (var i = 0; i < tempCmdVoucherList.length; i++) {

        if (idCmd == tempCmdVoucherList[i].id) {

            voucherList = [];

            tempDetailCmdVoucheList = JSON.parse(tempCmdVoucherList[i].detailCmdVoucheList);

            for (var j = 0; j < tempDetailCmdVoucheList.length; j++) {

                var row = tempDetailCmdVoucheList[j];

                voucher = {};
                voucher.id = row.id;
                voucher.codeAssujetti = row.codeAssujetti;
                voucher.montantFinal = row.montantFinal;
                voucher.remise = row.remise;
                voucher.idCmd = row.idCmd;
                voucher.reference = row.reference;
                voucher.checkRemise = row.checkRemise;
                voucherList.push(voucher);
            }
            break;
        }

    }

}

function validerPaiement(idCommande) {

    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'validerPaiement',
            'idCommande': idCommande
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation de la commande ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\'opération effectuée avec succès.');

                    if (checkUpdate) {
                        loadCommandeVoucher(true);
                    } else {
                        loadCommandeVoucher(false);
                    }
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de cette opération.');
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });

}

function loadCommandeGopass(checkAdvance) {

    if (checkAdvance) {

        lblDateDebut.html(inputDateDebut.val());
        lblDateFin.html(inputdateLast.val());
        isAdvance.attr('style', 'display: block');

    } else {

        isAdvance.attr('style', 'display: none');
    }


    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'valueSearch': inputSearchCommandeVoucher.val(),
            'typeSearch': codeResearchCommande,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val(),
            'isAdvancedSearch': checkAdvance,
            'operation': 'loadCommandeGopass'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();

                tempCmdGopassList = null;

                tempCmdGopassList = JSON.parse(JSON.stringify(response));

                printCommandeGopass(tempCmdGopassList);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printCommandeGopass(tempCmdGopassList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> </th>';
    header += '<th scope="col" style="width:15%"> REFERENCE </th>';
    header += '<th scope="col" style="width:20%"> ASSUJETTI </th>';
    header += '<th scope="col" style="width:15%"> TYPE ASSUJETTI </th>';
    header += '<th scope="col" style="width:10%"> TYPE GO-PASS </th>';
    header += '<th scope="col" style="width:10%"> CATEGORIE GO-PASS </th>';
//    header += '<th scope="col" style="width:10%"> DATE </th>';
    header += '<th scope="col" style="width:10%"> ETAT </th>';
    header += '<th scope="col" style="width:10%"> MONTANT </th>';
    header += '<th hidden="true" scope="col"> Code Personne </th>';
    header += '<th scope="col"> </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyBien">';

    for (var i = 0; i < tempCmdGopassList.length; i++) {

        var etatLibelle;
        var btnPreuvePaiement = '';
        var btnValiderPaiement = '';

        switch (tempCmdGopassList[i].etat) {
            case 1 :
                etatLibelle = 'Payé';
                if (tempCmdGopassList[i].preuvePaiement) {
                    btnPreuvePaiement = '<button class="btn btn-primary" onclick="showPreuve(\'' + tempCmdGopassList[i].id + '\')"><i class="fa fa-eye"></i></button>'
                }
                break;
            case 2 :
                etatLibelle = 'Validé';
                if (tempCmdGopassList[i].preuvePaiement) {
                    btnValiderPaiement = '<button class="btn btn-primary" onclick="validerPaiement(\'' + tempCmdGopassList[i].id + '\')"><i class="fa fa-check"></i></button>'
                }
                break;
            case 3 :
                etatLibelle = 'En attente de validation';
                break;
            case 4 :
                etatLibelle = 'Livré';
                break;
        }

        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="vertical-align:middle;">' + tempCmdGopassList[i].reference + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdGopassList[i].responsable + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdGopassList[i].formeJuridique + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdGopassList[i].typeGopasse + '</td>';
        body += '<td style="vertical-align:middle;">' + tempCmdGopassList[i].categorie + '</td>';
//        body += '<td style="vertical-align:middle;">' + tempCmdGopassList[i].dateCreat + '</td>';
        body += '<td style="vertical-align:middle;">' + etatLibelle + '</td>';
        body += '<td style="vertical-align:middle;" id="totalCmd_' + tempCmdGopassList[i].id + '">' + formatNumber(tempCmdGopassList[i].montant, tempCmdGopassList[i].devise) + '</td>';
        body += '<td hidden="true">' + tempCmdGopassList[i].id + '</td>';
        body += '<td style="text-align:center;vertical-align:middle;width:4%">' + btnPreuvePaiement + btnValiderPaiement + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableCommandeVoucher.html(tableContent);

    var dataDetailCmdGopass = tableCommandeVoucher.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par le nom _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableCommandeVoucher tbody').on('click', 'td.details-control', function () {

        codeCmdGopass = ''
        var tr = $(this).closest('tr');
        var row = dataDetailCmdGopass.row(tr);
        var dataDetail = dataDetailCmdGopass.row(tr).data();

        codeCmdGopass = dataDetail[8];

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');
        } else {
            var detailTable = '<center><h4>LES DETAILS COMMANDES GO-PASS </h4><br/></center>';
            detailTable += '<div id="childContent_' + codeCmdGopass + '"></div>';

            getDetailCommandeGopass(codeCmdGopass);

            row.child(detailTable).show();
            tr.addClass('shown');

        }
    });
}

function getDetailCommandeGopass(idCmdGopass) {


    $.ajax({
        type: 'POST',
        url: 'peage_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'codeGopasse': idCmdGopass,
            'operation': 'getDetailCommandeGopass'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();

                var detailCmdGopassList = null;
                tempDetailGopass = [];

                detailCmdGopassList = JSON.parse(JSON.stringify(response));
                childTableGopass(detailCmdGopassList, idCmdGopass);
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function childTableGopass(tempDetailCmdGopass, idCmdGopass) {

    var detailTable = '<table class="table table-bordered">';
    detailTable += '<thead><tr style="background-color:#e6ceac;color:black">';
    detailTable += '<td style="text-align:center">PASSAGER</td>';
    detailTable += '<td style="text-align:center">IDENTITE</td>';
    detailTable += '<td style="text-align:center">AER. DESTINATION</td>';
    detailTable += '<td style="text-align:center">AER. PROVENANCE</td>';
    detailTable += '<td style="text-align:center">COMPANIE</td>';
    detailTable += '<td style="text-align:center">DATE GERAT</td>';
    detailTable += '<td style="text-align:center">DATE UTILIS</td>';
    detailTable += '<td style="text-align:center">ETAT</td>';
    detailTable += '<th scope="col"> </th>';
    detailTable += '</tr></thead>';
    detailTable += '<tbody>';

    var etatDetailCmd;



    if (tempDetailCmdGopass.length) {

        for (var j = 0; j < tempDetailCmdGopass.length; j++) {

            etatDetailCmd = tempDetailCmdGopass[j].etat;
//            var bntActions = '';


            switch (tempDetailCmdGopass[j].etat) {
                case 0 :
                    etatLibelle = 'Gopass désactivée';
//                    bntActions = '<a onclick="desactiverCarte(\'' + tempDetailCmdGopass[j].id + '\', 3)" class="btn btn-warning" title="Activer carte"><i class="fa fa-trash-o"></i>&nbsp;Activer</a>';
                    break;
                case 1 :
                    etatLibelle = 'Gopass valide';
//                    bntActions = '<a onclick="desactiverCarte(\'' + tempDetailCmdGopass[j].id + '\', 0)" class="btn btn-danger" title="Désactiver carte"><i class="fa fa-trash-o"></i>&nbsp;Désactiver</a>';
                    break;
            }

            detailTable += '<tr>';
            detailTable += '<td style="text-align:center;width:18%;">' + tempDetailCmdGopass[j].namePassager + '</td>';
            detailTable += '<td style="text-align:center;width:18%;">' + tempDetailCmdGopass[j].pieceIdentite + '</td>';
            detailTable += '<td style="text-align:center;width:15%;">' + tempDetailCmdGopass[j].libelleAeroportDestination + '</td>';
            detailTable += '<td style="text-align:center;width:15%;">' + tempDetailCmdGopass[j].libelleAeroportProvenance + '</td>';
            detailTable += '<td style="text-align:center;width:15%;">' + tempDetailCmdGopass[j].libelleCompanie + '</td>';
            detailTable += '<td style="text-align:center;width:10%;">' + tempDetailCmdGopass[j].dateGeneration + '</td>';
            detailTable += '<td style="text-align:center;width:10%;">' + tempDetailCmdGopass[j].dateUtilisattion + '</td>';
            detailTable += '<td style="text-align:center;vertical-align:middle;width:10%">' + etatLibelle + '</td>';
            detailTable += '</tr>';
        }
    }
    detailTable += '</tbody>';

    detailTable += '</table>';

    var contentChildDiv = $('#childContent_' + idCmdGopass + '');
    contentChildDiv.html(detailTable);
//    

//    btnPrintCarteCmde = $('#btnPrintCarteCmde');
//
//    btnPrintCarteCmde.click(function (e) {
//        e.preventDefault();
//    });
}