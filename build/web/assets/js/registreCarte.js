/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var
        codeResearch,
        messageEmptyValue,
        advanced;

var tableCarte;

var
        inputSearch,
        inputDateDebut,
        inputdateLast;

var btnSearch;

var
        cmbSearchType,
        selectSite,
        selectService;

var tempAcquitLiberatoireList;

var
        datePickerDebut,
        datePickerFin;

var assujettiModal;
var responsibleObject = new Object();

var cpiParam, urlExists;

$(function () {

    mainNavigationLabel.text('IDENTIFICATION CONDUCTEUR MOTO');
    secondNavigationLabel.text('Registre des cartes');

    inputSearch = $('#inputSearch');
    tableCarte = $('#tableCarte');
    btnSearch = $('#btnSearch');

    btnSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        switch (codeResearch) {
            case '0':
                inputSearch.attr('placeholder', 'Numéro de la quittance. EX: RECUX17AA00007');
                inputSearch.attr('readonly', false);

                if (inputSearch.val() == empty) {
                    alertify.alert('Veuillez saisir le numéro de la preuve de preuve de paiement (Quittance)');
                    return;
                } else {
                    loadCarte();
                }

                break;
            case '1':
                inputSearch.attr('placeholder', 'Nom du conducteur. EX: Gauthier MUATA');
                inputSearch.attr('readonly', true);
                assujettiModal.modal('show');

                break;
            case '2':
                inputSearch.attr('placeholder', 'Nom du propriétaire. EX: AKIM');
                inputSearch.attr('readonly', true);
                assujettiModal.modal('show');

                break;
        }
    });

    assujettiModal = $('#assujettiModal');
    cmbSearchType = $('#cmbSearchType');

    printDataCarte(empty);

    cmbSearchType.on('change', function (e) {

        codeResearch = cmbSearchType.val();
        inputSearch.val(empty);
        inputSearch.attr('style', 'font-weight:bold;width: 380px');

        switch (codeResearch) {
            case '0':
                inputSearch.attr('placeholder', 'Numéro de la quittance. EX: RECUX17AA00007');
                inputSearch.attr('style', 'font-weight:normal');
                inputSearch.attr('readonly', false);
                break;
            case '1':
                inputSearch.attr('placeholder', 'Nom du conducteur. EX: Gauthier MUATA');
                inputSearch.attr('style', 'font-weight:normal');
                inputSearch.attr('readonly', true);
                break;
            case '2':
                inputSearch.attr('placeholder', 'Nom du propriétaire. EX: AKIM');
                inputSearch.attr('style', 'font-weight:normal');
                inputSearch.attr('readonly', true);
                break;
        }
    });

    urlExists = '0';
    var urlCpiParam = getUrlParameter('recu');

    if (jQuery.type(urlCpiParam) !== 'undefined') {
        urlExists = '1';
        cpiParam = atob(urlCpiParam);
        inputSearch.val(cpiParam);
        inputSearch.attr('disabled', true);
        cmbSearchType.attr('disabled', true);
        codeResearch = '0';
        btnSearch.trigger('click');
    }

    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');

    selectSite = $('#selectSite');
    selectService = $('#selectService');

    datePickerDebut = $("#datePicker1");
    datePickerFin = $('#datePicker2');

    btnRechercheAvancee = $('#btnRechercheAvancee');

    btnRechercheAvancee.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        switch (codeResearch) {
            case '0':
                inputSearch.attr('placeholder', 'Numéro de la quittance. EX: RECUX17AA00007');

                if (urlExists == '0') {
                    inputSearch.attr('readonly', false);
                }


                if (inputSearch.val() == empty) {
                    alertify.alert('Veuillez saisir le numéro de la preuve de preuve de paiement (Reçu)');
                    return;
                } else {
                    loadCarte();
                }

                break;
            case '1':
                inputSearch.attr('placeholder', 'Nom du conducteur. EX: Gauthier MUATA');
                inputSearch.attr('readonly', true);
                assujettiModal.modal('show');

                break;
            case '2':
                inputSearch.attr('placeholder', 'Nom du propriétaire. EX: AKIM');
                inputSearch.attr('readonly', true);
                assujettiModal.modal('show');

                break;
        }
    });

    codeResearch = cmbSearchType.val();

});

function loadCarte() {

    var valueSearch = empty;

    switch (cmbSearchType.val()) {
        case '0':
            valueSearch = inputSearch.val();
            break;
        case '1':
        case '2':
            valueSearch = responsibleObject.codeResponsible;
            break;
    }

    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadCarte',
            'advanced': advanced,
            'typeSearch': codeResearch,
            'libelle': valueSearch,
            'idUser': userData.idUser,
            'dateDebut': inputDateDebut.val(),
            'dateFin': inputdateLast.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});

        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {
                printDataCarte(empty);
                $.unblockUI();
                showResponseError();
                return;

            } else if (response == '0') {

                printDataCarte(empty);

                switch (codeResearch) {
                    case '0':
                        alertify.alert('Désoler, ce reçu : ' + inputSearch.val() + ' n\'est pas lié à une demande de carte');
                        break;
                    case '1':
                        alertify.alert('Désoler, ce conducter : ' + inputSearch.val() + ' n\'est pas lié à une demande de carte');
                        break;
                    case '2':
                        alertify.alert('Désoler, ce propriétaire : ' + inputSearch.val() + ' n\'est pas lié à une demande de carte');
                        break;
                }
            } else {
                var carteList = JSON.parse(JSON.stringify(response));
                printDataCarte(carteList);
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function printDataCarte(carteListList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th>ID CARTE</th>';
    header += '<th>PROPRIETAIRE</th>';
    header += '<th>CONDUCTEUR</th>';
    header += '<th>MOTO</th>';
    header += '<th>RECU</th>';
    header += '<th>DATE CREATION</th>';
    header += '<th>DATE EXPIRATION</th>';
    header += '<th></th>';
    header += '</tr></thead>';

    var body = '<tbody >';

    for (var i = 0; i < carteListList.length; i++) {

        var printBtn = '';

        if (controlAccess('PRINT_CARD_MOTOCYCLE')) {
            if (parseInt(carteListList[i].stateCard) == 2) {
                printBtn = '';
            } else {
                printBtn = '<button title="Cliquez ici pour imprimer la carte" style="margin-right:3px" onclick="print(\'' + carteListList[i].idCarte + '\')" class="btn btn-warning"><i class="fa fa-print"></i></button>';
            }

        } else {

            if (parseInt(carteListList[i].stateCard) == 2) {
                printBtn = '';
            } else {
                printBtn = '<button disabled="true" onclick="print(\'' + carteListList[i].idCarte + '\')" class="btn btn-warning"><i class="fa fa-print"></i></button>';
            }

        }

        var color = ';color:black';

        if (carteListList[i].cardExpired === '1') {
            color = ';color:red';
        }

        body += '<tr>';
        body += '<td style="text-align:left;width:9%;vertical-align:middle">' + carteListList[i].idCarte + '</td>';
        body += '<td style="text-align:left;width:22%;vertical-align:middle">' + carteListList[i].nomProprietaire + '</td>';
        body += '<td style="text-align:left;width:22%;vertical-align:middle">' + carteListList[i].nomConducteur + '</td>';
        body += '<td style="text-align:left;width:20%;vertical-align:middle">' + carteListList[i].nomCompositeBien + '</td>';
        body += '<td style="text-align:left;width:8%;vertical-align:middle;font-weight: bold">' + carteListList[i].numeroCPI + '</td>';
        body += '<td style="text-align:left;width:7%;vertical-align:middle">' + carteListList[i].dateCreation + '</td>';
        body += '<td style="text-align:left;width:7%;vertical-align:middle;font-weight: bold' + color + '">' + carteListList[i].dateExpiration + '</td>';
        body += '<td style="text-align:center;width:5%;vertical-align:middle">' + printBtn + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableCarte.html(tableContent);

    var dtNoteCalcul = tableCarte.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

    $('#tableCarte tbody').on('click', 'tr', function () {
        var data = dtNoteCalcul.row(this).data();
    });
}

function printerCarte(reference) {

    if (reference == '') {
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'printCarte',
            'reference': reference,
            'isToTracking': '0',
            'idUser': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression de la carte en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                setDocumentContent(response);
                window.open('impression-carte ', '_blank');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function print(ref) {

    alertify.confirm('Etes-vous sûre de vouloir imprimer cette carte ?', function () {
        printerCarte(ref);
    });
}

function dissociateBien(reference) {

    if (reference == '') {
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'dissociateBien',
            'reference': reference
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                alertify.alert("ok");

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            alert(xhr.status);
            $.unblockUI();
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }

    inputSearch.val(selectAssujettiData.nomComplet);
    inputSearch.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadCarte();

}
