/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_SUIVI_COMPTABLE_DECLARATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SuiviComptableDeclaration.findAll", query = "SELECT s FROM SuiviComptableDeclaration s"),
    @NamedQuery(name = "SuiviComptableDeclaration.findByCode", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.code = :code"),
    @NamedQuery(name = "SuiviComptableDeclaration.findByNc", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.nc = :nc"),
    @NamedQuery(name = "SuiviComptableDeclaration.findByDebit", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.debit = :debit"),
    @NamedQuery(name = "SuiviComptableDeclaration.findByCredit", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.credit = :credit"),
    @NamedQuery(name = "SuiviComptableDeclaration.findBySolde", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.solde = :solde"),
    @NamedQuery(name = "SuiviComptableDeclaration.findByDocumentReference", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.documentReference = :documentReference"),
    @NamedQuery(name = "SuiviComptableDeclaration.findByLibelleOperation", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.libelleOperation = :libelleOperation"),
    @NamedQuery(name = "SuiviComptableDeclaration.findByDateCreat", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.dateCreat = :dateCreat"),
    @NamedQuery(name = "SuiviComptableDeclaration.findByDevise", query = "SELECT s FROM SuiviComptableDeclaration s WHERE s.devise = :devise")})
public class SuiviComptableDeclaration implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CODE")
    private Integer code;
    @Size(max = 25)
    @Column(name = "NC")
    private String nc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "DEBIT")
    private BigDecimal debit;
    @Column(name = "CREDIT")
    private BigDecimal credit;
    @Column(name = "SOLDE")
    private BigDecimal solde;
    @Size(max = 25)
    @Column(name = "DOCUMENT_REFERENCE")
    private String documentReference;
    @Size(max = 100)
    @Column(name = "LIBELLE_OPERATION")
    private String libelleOperation;
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "OBSERVATION")
    private String observation;
    @Size(max = 5)
    @Column(name = "DEVISE")
    private String devise;

    public SuiviComptableDeclaration() {
    }

    public SuiviComptableDeclaration(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getNc() {
        return nc;
    }

    public void setNc(String nc) {
        this.nc = nc;
    }

    public BigDecimal getDebit() {
        return debit;
    }

    public void setDebit(BigDecimal debit) {
        this.debit = debit;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal credit) {
        this.credit = credit;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

    public String getDocumentReference() {
        return documentReference;
    }

    public void setDocumentReference(String documentReference) {
        this.documentReference = documentReference;
    }

    public String getLibelleOperation() {
        return libelleOperation;
    }

    public void setLibelleOperation(String libelleOperation) {
        this.libelleOperation = libelleOperation;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getDevise() {
        return devise;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SuiviComptableDeclaration)) {
            return false;
        }
        SuiviComptableDeclaration other = (SuiviComptableDeclaration) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.SuiviComptableDeclaration[ code=" + code + " ]";
    }

}
