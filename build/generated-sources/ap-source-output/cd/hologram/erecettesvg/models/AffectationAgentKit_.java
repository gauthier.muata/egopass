package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AffectationAgentKit.class)
public class AffectationAgentKit_ { 

    public static volatile SingularAttribute<AffectationAgentKit, String> agent;
    public static volatile SingularAttribute<AffectationAgentKit, Long> code;
    public static volatile SingularAttribute<AffectationAgentKit, Integer> kit;
    public static volatile SingularAttribute<AffectationAgentKit, Boolean> etat;

}