/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_ASSIGNATION_BUDGETAIRE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AssignationBudgetaire.findAll", query = "SELECT a FROM AssignationBudgetaire a"),
    @NamedQuery(name = "AssignationBudgetaire.findById", query = "SELECT a FROM AssignationBudgetaire a WHERE a.id = :id"),
    @NamedQuery(name = "AssignationBudgetaire.findByFkExercice", query = "SELECT a FROM AssignationBudgetaire a WHERE a.fkExercice = :fkExercice"),
    @NamedQuery(name = "AssignationBudgetaire.findByMontantGlobal", query = "SELECT a FROM AssignationBudgetaire a WHERE a.montantGlobal = :montantGlobal"),
    @NamedQuery(name = "AssignationBudgetaire.findByMontantReel", query = "SELECT a FROM AssignationBudgetaire a WHERE a.montantReel = :montantReel"),
    @NamedQuery(name = "AssignationBudgetaire.findByFkDevise", query = "SELECT a FROM AssignationBudgetaire a WHERE a.fkDevise = :fkDevise"),
    @NamedQuery(name = "AssignationBudgetaire.findByDateCreate", query = "SELECT a FROM AssignationBudgetaire a WHERE a.dateCreate = :dateCreate"),
    @NamedQuery(name = "AssignationBudgetaire.findByAgentCreate", query = "SELECT a FROM AssignationBudgetaire a WHERE a.agentCreate = :agentCreate"),
    @NamedQuery(name = "AssignationBudgetaire.findByEtat", query = "SELECT a FROM AssignationBudgetaire a WHERE a.etat = :etat")})
public class AssignationBudgetaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 10)
    @Column(name = "FK_EXERCICE")
    private String fkExercice;
    @Column(name = "MONTANT_GLOBAL")
    private Double montantGlobal;
    @Column(name = "MONTANT_REEL")
    private Double montantReel;
    @Size(max = 5)
    @Column(name = "FK_DEVISE")
    private String fkDevise;
    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;
    @Column(name = "AGENT_CREATE")
    private Integer agentCreate;
    @Column(name = "ARCHIVE")
    private String archive;
    @Column(name = "ETAT")
    private Integer etat;
//    @JoinColumn(name = "FK_SERVICE", referencedColumnName = "CODE")
//    @ManyToOne
//    private Service fkService;

    public AssignationBudgetaire() {
    }

    public AssignationBudgetaire(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFkExercice() {
        return fkExercice;
    }

    public void setFkExercice(String fkExercice) {
        this.fkExercice = fkExercice;
    }

    public Double getMontantGlobal() {
        return montantGlobal;
    }

    public void setMontantGlobal(Double montantGlobal) {
        this.montantGlobal = montantGlobal;
    }

    public Double getMontantReel() {
        return montantReel;
    }

    public void setMontantReel(Double montantReel) {
        this.montantReel = montantReel;
    }

    public String getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(String fkDevise) {
        this.fkDevise = fkDevise;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Integer getAgentCreate() {
        return agentCreate;
    }

    public void setAgentCreate(Integer agentCreate) {
        this.agentCreate = agentCreate;
    }

    public String getArchive() {
        return archive;
    }

    public void setArchive(String archive) {
        this.archive = archive;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

//    public Service getFkService() {
//        return fkService;
//    }
//
//    public void setFkService(Service fkService) {
//        this.fkService = fkService;
//    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AssignationBudgetaire)) {
            return false;
        }
        AssignationBudgetaire other = (AssignationBudgetaire) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.AssignationBudgetaire[ id=" + id + " ]";
    }

}
