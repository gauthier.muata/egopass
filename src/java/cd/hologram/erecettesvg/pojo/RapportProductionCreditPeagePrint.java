/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class RapportProductionCreditPeagePrint implements Serializable{
    
    String datePrint;
    String axepeage;
    String poste;
    String controleurName;
    String controleurCode;
    String controleurPhone;
    String percepteurName;
    String percepteurCode;
    String percepteurPhone;
    
    String cfV;
    String cfVL;
    String cfCIP;
    String cfBM;
    String cfTOTAL;
    
    String cfC2P;
    String cfCR;
    String cfCRTR;
    String cfVIT;
    String cfVLIT;
    String cfCIPBM;
    String cfTOTAL2;
    
    String totlGenTour;
    String observation;
    String lieuPrint;

    public String getDatePrint() {
        return datePrint;
    }

    public void setDatePrint(String datePrint) {
        this.datePrint = datePrint;
    }

    public String getAxepeage() {
        return axepeage;
    }

    public void setAxepeage(String axepeage) {
        this.axepeage = axepeage;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getControleurName() {
        return controleurName;
    }

    public void setControleurName(String controleurName) {
        this.controleurName = controleurName;
    }

    public String getControleurCode() {
        return controleurCode;
    }

    public void setControleurCode(String controleurCode) {
        this.controleurCode = controleurCode;
    }

    public String getControleurPhone() {
        return controleurPhone;
    }

    public void setControleurPhone(String controleurPhone) {
        this.controleurPhone = controleurPhone;
    }

    public String getPercepteurName() {
        return percepteurName;
    }

    public void setPercepteurName(String percepteurName) {
        this.percepteurName = percepteurName;
    }

    public String getPercepteurCode() {
        return percepteurCode;
    }

    public void setPercepteurCode(String percepteurCode) {
        this.percepteurCode = percepteurCode;
    }

    public String getPercepteurPhone() {
        return percepteurPhone;
    }

    public void setPercepteurPhone(String percepteurPhone) {
        this.percepteurPhone = percepteurPhone;
    }

    public String getCfV() {
        return cfV;
    }

    public void setCfV(String cfV) {
        this.cfV = cfV;
    }

    public String getCfVL() {
        return cfVL;
    }

    public void setCfVL(String cfVL) {
        this.cfVL = cfVL;
    }

    public String getCfCIP() {
        return cfCIP;
    }

    public void setCfCIP(String cfCIP) {
        this.cfCIP = cfCIP;
    }

    public String getCfBM() {
        return cfBM;
    }

    public void setCfBM(String cfBM) {
        this.cfBM = cfBM;
    }

    public String getCfTOTAL() {
        return cfTOTAL;
    }

    public void setCfTOTAL(String cfTOTAL) {
        this.cfTOTAL = cfTOTAL;
    }

    public String getCfC2P() {
        return cfC2P;
    }

    public void setCfC2P(String cfC2P) {
        this.cfC2P = cfC2P;
    }

    public String getCfCR() {
        return cfCR;
    }

    public void setCfCR(String cfCR) {
        this.cfCR = cfCR;
    }

    public String getCfCRTR() {
        return cfCRTR;
    }

    public void setCfCRTR(String cfCRTR) {
        this.cfCRTR = cfCRTR;
    }

    public String getCfVIT() {
        return cfVIT;
    }

    public void setCfVIT(String cfVIT) {
        this.cfVIT = cfVIT;
    }

    public String getCfVLIT() {
        return cfVLIT;
    }

    public void setCfVLIT(String cfVLIT) {
        this.cfVLIT = cfVLIT;
    }

    public String getCfCIPBM() {
        return cfCIPBM;
    }

    public void setCfCIPBM(String cfCIPBM) {
        this.cfCIPBM = cfCIPBM;
    }

    public String getCfTOTAL2() {
        return cfTOTAL2;
    }

    public void setCfTOTAL2(String cfTOTAL2) {
        this.cfTOTAL2 = cfTOTAL2;
    }

    public String getTotlGenTour() {
        return totlGenTour;
    }

    public void setTotlGenTour(String totlGenTour) {
        this.totlGenTour = totlGenTour;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getLieuPrint() {
        return lieuPrint;
    }

    public void setLieuPrint(String lieuPrint) {
        this.lieuPrint = lieuPrint;
    }

    
    
}
