package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Med.class)
public class Med_ { 

    public static volatile SingularAttribute<Med, String> agentMaj;
    public static volatile SingularAttribute<Med, Date> dateCreat;
    public static volatile SingularAttribute<Med, String> notePerception;
    public static volatile SingularAttribute<Med, String> personne;
    public static volatile SingularAttribute<Med, String> noteCalcul;
    public static volatile SingularAttribute<Med, String> exerciceFiscal;
    public static volatile SingularAttribute<Med, BigDecimal> montant;
    public static volatile SingularAttribute<Med, Short> etat;
    public static volatile SingularAttribute<Med, Integer> periodeDeclaration;
    public static volatile SingularAttribute<Med, Date> dateMaj;
    public static volatile SingularAttribute<Med, String> medMere;
    public static volatile SingularAttribute<Med, String> articleBudgetaire;
    public static volatile SingularAttribute<Med, Date> dateImpression;
    public static volatile SingularAttribute<Med, String> numeroDocument;
    public static volatile SingularAttribute<Med, String> typeMed;
    public static volatile SingularAttribute<Med, String> fkSite;
    public static volatile SingularAttribute<Med, String> adresse;
    public static volatile SingularAttribute<Med, BigDecimal> montantPenalite;
    public static volatile SingularAttribute<Med, Short> etatImpression;
    public static volatile SingularAttribute<Med, Date> dateEcheance;
    public static volatile SingularAttribute<Med, String> id;
    public static volatile SingularAttribute<Med, String> agentCreat;
    public static volatile SingularAttribute<Med, Date> dateReception;

}