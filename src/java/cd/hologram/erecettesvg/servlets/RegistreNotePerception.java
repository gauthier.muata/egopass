/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import static cd.hologram.erecettesvg.business.IdentificationBusiness.getPersonneByCode;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.NotePerceptionConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Archive;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.ComplementInfoTaxe;
import cd.hologram.erecettesvg.models.Jdossier;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.SuiviComptableDeclaration;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.sql.SQLQueryGeneral;
import cd.hologram.erecettesvg.sql.SQLQueryNotePerception;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.PrintDocument;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author : WILLY KASHALA Tel : 00243 81 27 20 560
 */
@WebServlet(name = "RegistreNotePerception", urlPatterns = {"/registrenoteperception"})
public class RegistreNotePerception extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE);
    Properties propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case NotePerceptionConst.Operation.LOAD_NOTE_PERCEPTION:
                result = getNotePerception(request);
                break;
            case NotePerceptionConst.Operation.PRINT_NOTE_PERCEPTION:
                result = printNP(request);
                break;
            case "printNpFille":
                result = printNPFille(request);
                break;
            case NotePerceptionConst.Operation.UPDATE_DATE_ECHEANCE:
                result = accuserReception(request);
                break;
            case "getMinier":
                result = loadMiniers(request);
                break;
            case "getSousProvisionMinier":
                result = loadSousProvisionMinier(request);
                break;
            default:

        }
        out.print(result);
    }

    NotePerception notePerception;
    HashMap<String, Object[]> bulkUpdateNP;

    public String printNP(HttpServletRequest request) {

        bulkUpdateNP = new HashMap<>();
        PrintDocument printDocument = new PrintDocument();
        BigDecimal netAPayerValue, negativeNetAPayerValue;
        int counter = 0;
        boolean result;

        try {

            String numeroNP = request.getParameter(NotePerceptionConst.ParamName.NUMERO);
            String notecalcul = request.getParameter(NotePerceptionConst.ParamName.NOTE_CALCUL);
            String compteBancaire = request.getParameter(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE);
            String devise = request.getParameter(NotePerceptionConst.ParamName.DEVISE);
            String initialDevise = request.getParameter(NotePerceptionConst.ParamName.INITIAL_DEVISE);
            String timbre = request.getParameter(NotePerceptionConst.ParamName.TIMBRE);
            String papier = request.getParameter(NotePerceptionConst.ParamName.PAPIER_SECURISE);
            String netApayer = request.getParameter(NotePerceptionConst.ParamName.NET_A_PAYER);
            String tauxApplique = request.getParameter(NotePerceptionConst.ParamName.TAUX_APPLIQUE);
            String initialAmount = request.getParameter(NotePerceptionConst.ParamName.MONTANT_INITIAL);
            String receptionniste = request.getParameter(NotePerceptionConst.ParamName.RECEPTIONNISTE);
            Integer printCount = Integer.valueOf(request.getParameter(NotePerceptionConst.ParamName.PRINT_COUNT));
            Integer addEcheance = Integer.valueOf(request.getParameter(NotePerceptionConst.ParamName.ADD_ECHEANCE));

            notePerception = new NotePerception();
            SuiviComptableDeclaration suiviComptable = new SuiviComptableDeclaration();
            netAPayerValue = BigDecimal.valueOf(Float.valueOf(netApayer));
            negativeNetAPayerValue = BigDecimal.valueOf(Float.valueOf(netApayer) * -1);
            notePerception.setNumero(numeroNP);

            boolean addEchanceDate = (addEcheance == GeneralConst.Numeric.ONE);

            notePerception = NotePerceptionBusiness.getNotePerceptionByCode(notePerception.getNumero());

            if (addEchanceDate) {

                String DAY_NP_ECHEANCE = propertiesConfig.getProperty("NBRE_DAY_NP_ECHEANCE");
                Date dateEcheance = Tools.getEcheanceDate(Tools.formatStringFullToDate(notePerception.getDateCreat()), Integer.valueOf(DAY_NP_ECHEANCE));
                Personne p = IdentificationBusiness.getPersonneByCode(receptionniste);
                if (p != null) {
                    notePerception.setReceptionniste(p);
                } else {
                    notePerception.setReceptionniste(new Personne());
                }
                notePerception.setDateReception(Tools.formatDateToString(new Date()));
                notePerception.setDateEcheancePaiement(Tools.formatDateToString(dateEcheance));

            } else {
                notePerception.setReceptionniste(null);
                notePerception.setDateReception(null);
                notePerception.setDateEcheancePaiement(null);
            }

            if (printCount == 0) {

                notePerception.setNetAPayer(netAPayerValue);
                notePerception.setSolde(netAPayerValue);
                notePerception.setCompteBancaire(compteBancaire);
                notePerception.setTauxApplique(BigDecimal.valueOf(Float.valueOf(tauxApplique)));
                notePerception.setMontantInitial(BigDecimal.valueOf(Float.valueOf(initialAmount)));
                notePerception.setDevise(devise);
                notePerception.setPapierSecurise(papier);
                notePerception.setTimbre(timbre);

                counter++;
                bulkUpdateNP.put(counter + SQLQueryNotePerception.UPDATE_NOTE_PERCEPTION_PRINT_CASE_2,
                        new Object[]{
                            notePerception.getCompteBancaire(),
                            notePerception.getDateReception(),
                            notePerception.getReceptionniste() == null ? null : notePerception.getReceptionniste().getCode(),
                            notePerception.getTimbre(),
                            notePerception.getPapierSecurise(),
                            notePerception.getSolde(),
                            notePerception.getDevise(),
                            notePerception.getTauxApplique(),
                            notePerception.getMontantInitial(),
                            notePerception.getNetAPayer(),
                            notePerception.getDateEcheancePaiement(),
                            notePerception.getNumero()
                        });

                if (!initialDevise.equals(devise)) {

                    suiviComptable.setDebit(netAPayerValue);
                    suiviComptable.setSolde(negativeNetAPayerValue);
                    suiviComptable.setDevise(devise);

                    counter++;
                    bulkUpdateNP.put(counter + SQLQueryNotePerception.UPDATE_SUIVI_COMPTABLE_PRINT,
                            new Object[]{
                                suiviComptable.getDebit(),
                                suiviComptable.getSolde(),
                                suiviComptable.getDevise(),
                                notecalcul
                            });

                }

            }

            counter++;
            printCount++;
            suiviComptable.setNc(notecalcul);
            suiviComptable.setDebit(new BigDecimal(BigInteger.ZERO));
            suiviComptable.setCredit(new BigDecimal(BigInteger.ZERO));
            suiviComptable.setDocumentReference(notePerception.getNumero());
            suiviComptable.setDevise(devise);
            suiviComptable.setLibelleOperation(propertiesMessage.getProperty("MESSAGE_PRINT_NP").concat(GeneralConst.SPACE).concat(printCount.toString()));
            suiviComptable.setObservation(propertiesMessage.getProperty("MESSAGE_PRINT_NP_SUCCESS"));

            bulkUpdateNP.put(counter + SQLQueryGeneral.EXEC_F_NEW_SUIVI_DECLARATION,
                    new Object[]{
                        suiviComptable.getNc(),
                        suiviComptable.getDocumentReference(),
                        suiviComptable.getLibelleOperation(),
                        suiviComptable.getDebit(),
                        suiviComptable.getCredit(),
                        suiviComptable.getObservation(),
                        suiviComptable.getDevise()
                    });

            result = NotePerceptionBusiness.executeQueryBulkInsert(bulkUpdateNP);

            if (result) {
                String dataReturn = printDocument.createNP(notePerception);
                return dataReturn;

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NumberFormatException | InvocationTargetException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public String printNPFille(HttpServletRequest request) {

        PrintDocument printDocument = new PrintDocument();
        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String NUMERO = request.getParameter(NotePerceptionConst.ParamName.NUMERO);

            notePerception = new NotePerception();

            Archive archive = NotePerceptionBusiness.getArchiveByRefDocument(NUMERO.trim());

            if (archive != null) {

                dataReturn = archive.getDocumentString();

            } else {

                notePerception = NotePerceptionBusiness.getNotePerceptionByCode(NUMERO.trim());

                if (notePerception != null) {

                    dataReturn = printDocument.createNP(notePerception);

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }
            }

        } catch (NumberFormatException | InvocationTargetException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String getNotePerception(HttpServletRequest request) {

        String valueSearch, typeSearch,
                viewAllSite, viewAllService,
                codeSite, codeService, userId, typeRegister, isAvancedSearch,
                datedebut, dateFin;

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            viewAllSite = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SITE);
            isAvancedSearch = request.getParameter(TaxationConst.ParamName.IS_AVANCED_SEARCH);
            viewAllService = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SERVICE);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            datedebut = Tools.getValidFormat(request.getParameter(GeneralConst.ParamName.DATE_DEBUT));
            dateFin = Tools.getValidFormat(request.getParameter(GeneralConst.ParamName.DATE_FIN));

            List<NotePerception> listNotePerception;

            if (Integer.valueOf(isAvancedSearch) == 1) {

                listNotePerception = NotePerceptionBusiness.getListNotePerceptionsBySearchAvanced(codeSite, codeService, datedebut, dateFin);

            } else {

                listNotePerception = NotePerceptionBusiness.getListNotePerceptions(
                        valueSearch,
                        Integer.valueOf(typeSearch),
                        typeRegister,
                        viewAllSite.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                        viewAllService.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                        codeSite,
                        codeService,
                        userId);
            }

            List<JsonObject> jsonNotePerceptionList = new ArrayList<>();

            JsonObject jsonNP;

            if (listNotePerception != null && !listNotePerception.isEmpty()) {

                for (NotePerception np : listNotePerception) {

                    jsonNP = new JsonObject();

                    Personne personne;
                    Personne personneReception;
                    Adresse adresse;

                    String codeAB = np.getNoteCalcul().getDetailsNcList().get(0).getArticleBudgetaire().getCode();

                    switch (codeAB) {

                        case "00000000000001212016": //VOIRIE
                        case "00000000000001072016": //CONCENTRES

                            ComplementInfoTaxe complementInfoTaxe = TaxationBusiness.getComplementInfoTaxeByNP(np.getNumero());

                            if (complementInfoTaxe != null) {

                                jsonNP.addProperty("estTaxeMine", true);
                                jsonNP.addProperty("transiteur", complementInfoTaxe.getTransporteur().toUpperCase());
                                jsonNP.addProperty("produit", complementInfoTaxe.getNatureProduit().toUpperCase());
                                jsonNP.addProperty("tonage", np.getNoteCalcul().getDetailsNcList().get(0).getValeurBase());

                                float amountTotal = (np.getNoteCalcul().getDetailsNcList().get(0).getValeurBase().floatValue()
                                        * np.getNoteCalcul().getDetailsNcList().get(0).getTauxArticleBudgetaire().floatValue());

                                jsonNP.addProperty("amountTotal", amountTotal);
                                jsonNP.addProperty("tauxUnit", np.getNoteCalcul().getDetailsNcList().get(0).getTauxArticleBudgetaire());

                                if (codeAB.equals("00000000000001072016")) {
                                    jsonNP.addProperty("typeAB", "concentrés".toUpperCase());
                                    jsonNP.addProperty("typeAB2", "C");
                                } else {
                                    jsonNP.addProperty("typeAB", "voirie".toUpperCase());
                                    jsonNP.addProperty("typeAB2", "V");
                                }

                            } else {
                                jsonNP.addProperty("estTaxeMine", true);
                                jsonNP.addProperty("transiteur", "");
                                jsonNP.addProperty("produit", "");
                                jsonNP.addProperty("tonage", "");
                                jsonNP.addProperty("tauxUnit", "");
                                jsonNP.addProperty("amountTotal", "");
                                jsonNP.addProperty("typeAB", "");
                                jsonNP.addProperty("typeAB2", "");
                            }

                            break;
                        default:
                            jsonNP.addProperty("estTaxeMine", false);
                            jsonNP.addProperty("transiteur", "");
                            jsonNP.addProperty("produit", "");
                            jsonNP.addProperty("tonage", "");
                            jsonNP.addProperty("tauxUnit", "");
                            jsonNP.addProperty("amountTotal", "");
                            jsonNP.addProperty("typeAB", "");
                            jsonNP.addProperty("typeAB2", "");
                            break;
                    }

                    Date dateCreateNP;
                    String dateEcheanceNP = GeneralConst.EMPTY_STRING;

                    if (np.getNoteCalcul().getFkAdressePersonne() != null
                            && !np.getNoteCalcul().getFkAdressePersonne().isEmpty()) {

                        adresse = TaxationBusiness.getAdressByCode(np.getNoteCalcul().getFkAdressePersonne().trim());

                        jsonNP.addProperty(TaxationConst.ParamName.CODE_ADRESSE_ASSUJETTI, np.getNoteCalcul().getFkAdressePersonne().trim());

                        if (adresse != null) {
                            jsonNP.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI, adresse.toString().toUpperCase().trim());
                        } else {
                            jsonNP.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI, GeneralConst.EMPTY_STRING);
                        }
                    } else {
                        jsonNP.addProperty(TaxationConst.ParamName.ADRESSE_ASSUJETTI, GeneralConst.EMPTY_STRING);
                    }

                    Date dateCreate = Tools.formatStringFullToDate(np.getDateCreat());

                    jsonNP.addProperty(TaxationConst.ParamName.DATE_CREATE, Tools.formatDateToStringV2(dateCreate));
                    jsonNP.addProperty(NotePerceptionConst.ParamName.NUMERO, np.getNumero());
                    jsonNP.addProperty(TaxationConst.ParamName.CODE_SERVICE, np.getNoteCalcul().getService());
                    jsonNP.addProperty(TaxationConst.ParamName.EXERCICE_FISCAL, np.getNoteCalcul().getExercice());

                    dateEcheanceNP = GeneralConst.EMPTY_STRING;

                    if (np.getDateEcheancePaiement() != null) {

                        dateEcheanceNP = Tools.formatDateToStringV2(Tools.formatStringFullToDate(
                                np.getDateEcheancePaiement()));
                    }

                    jsonNP.addProperty(TaxationConst.ParamName.ECHEANCE, dateEcheanceNP);

                    String libelleAB = GeneralConst.EMPTY_STRING;

                    if (!np.getNoteCalcul().getDetailsNcList().isEmpty()) {

                        Integer size = np.getNoteCalcul().getDetailsNcList().size();

                        for (int i = 0; i < size; i++) {

                            String tempAb;
                            String libelleTarif = GeneralConst.EMPTY_STRING;

                            Tarif tarif = TaxationBusiness.getTarifByCode(np.getNoteCalcul().getDetailsNcList().get(i).getTarif().trim());
                            if (tarif != null) {
                                libelleTarif = tarif.getIntitule();
                            }
                            tempAb = np.getNoteCalcul().getDetailsNcList().get(i).getArticleBudgetaire().getIntitule() + " : " + libelleTarif;
                            if (i == 0) {
                                libelleAB = tempAb;
                            } else {
                                libelleAB += GeneralConst.AB_SEPARTOR.concat(tempAb);
                            }
                        }
                    }

                    jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, libelleAB);
                    Service service = TaxationBusiness.getServiceByCode(np.getNoteCalcul().getService());

                    if (service != null) {
                        jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE, service.getIntitule().toUpperCase().trim());
                    } else {
                        jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE, GeneralConst.EMPTY_STRING);
                    }

                    personne = IdentificationBusiness.getPersonneByCode(np.getNoteCalcul().getPersonne().trim());
                    personneReception = np.getReceptionniste();
                    jsonNP.addProperty(NotePerceptionConst.ParamName.CODE_ASSUJETTI, np.getNoteCalcul().getPersonne());

                    if (personne != null) {
                        jsonNP.addProperty(NotePerceptionConst.ParamName.ASSUJETTI, personne.toString().toUpperCase().trim());

                        jsonNP.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE, personne.getFormeJuridique().getCode().trim());
                        jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE, personne.getFormeJuridique().getIntitule().toUpperCase().trim());
                        jsonNP.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null
                                        ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());
                    } else {
                        jsonNP.addProperty(NotePerceptionConst.ParamName.ASSUJETTI, GeneralConst.EMPTY_STRING);
                        jsonNP.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE, GeneralConst.EMPTY_STRING);
                        jsonNP.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE, GeneralConst.EMPTY_STRING);
                        jsonNP.addProperty(IdentificationConst.ParamName.USER_NAME, GeneralConst.EMPTY_STRING);
                    }

                    if (personneReception != null) {
                        jsonNP.addProperty(NotePerceptionConst.ParamName.CODE_ASSUJETTI_RECEPTION, personneReception.getCode());
                        jsonNP.addProperty(NotePerceptionConst.ParamName.RECEPTIONNISTE, personneReception.toString());

                    } else {
                        jsonNP.addProperty(NotePerceptionConst.ParamName.CODE_ASSUJETTI_RECEPTION, GeneralConst.EMPTY_STRING);
                        jsonNP.addProperty(NotePerceptionConst.ParamName.RECEPTIONNISTE, GeneralConst.EMPTY_STRING);
                    }

                    jsonNP.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, np.getNetAPayer());

                    String deviseInitial = GeneralConst.EMPTY_STRING;

                    if (!np.getNoteCalcul().getDetailsNcList().isEmpty()) {
                        deviseInitial = np.getNoteCalcul().getDetailsNcList().get(0).getDevise() != null ? np.getNoteCalcul().getDetailsNcList().get(0).getDevise() : GeneralConst.Devise.DEVISE_CDF;
                        jsonNP.addProperty(NotePerceptionConst.ParamName.INITIAL_DEVISE, deviseInitial);
                    }

                    jsonNP.addProperty(NotePerceptionConst.ParamName.DEVISE, np.getDevise() == null ? deviseInitial : np.getDevise());

                    jsonNP.addProperty(NotePerceptionConst.ParamName.NOTE_CALCUL, np.getNoteCalcul().getNumero().trim());
                    jsonNP.addProperty(NotePerceptionConst.ParamName.NBR_IMPRESSION, np.getNbrImpression());

                    jsonNP.addProperty(NotePerceptionConst.ParamName.MONTANT_INITIAL, np.getDevise() == null ? np.getNetAPayer() : np.getMontantInitial());

                    if (np.getNbrImpression() > 0) {

                        jsonNP.addProperty(NotePerceptionConst.ParamName.COMPTE_BANCAIRE_CODE, np.getCompteBancaire());
                    }

                    if (np.getFractionnee() == GeneralConst.Numeric.ONE || !np.getNotePerceptionList().isEmpty()) {

                        jsonNP.addProperty("estFractionnee", GeneralConst.Number.ONE);

                        List<NotePerception> npFilleList = np.getNotePerceptionList();
                        List<JsonObject> jsonNPFilleList = new ArrayList<>();

                        for (NotePerception npFille : npFilleList) {

                            JsonObject jsonNPFille = new JsonObject();

                            dateCreateNP = Tools.formatStringFullToDate(npFille.getDateCreat());
                            dateEcheanceNP = GeneralConst.EMPTY_STRING;

                            if (npFille.getDateEcheancePaiement() != null) {

                                dateEcheanceNP = Tools.formatDateToStringV2(Tools.formatStringFullToDate(
                                        npFille.getDateEcheancePaiement()));
                            }

                            jsonNPFille.addProperty("npMere", npFille.getNpMere().getNumero());
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.NUMERO, npFille.getNumero());
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, "NOTE DE PERCEPTION FILLE");
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.DEVISE, npFille.getDevise());
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.NBR_IMPRESSION, npFille.getNbrImpression());
                            jsonNPFille.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, npFille.getNetAPayer());
                            jsonNPFille.addProperty(TaxationConst.ParamName.DATE_CREATE, Tools.formatDateToStringV2(dateCreateNP));
                            jsonNPFille.addProperty(TaxationConst.ParamName.ECHEANCE, dateEcheanceNP);

                            Journal journal = PaiementBusiness.getJournalByDocument(npFille.getNumero());

                            jsonNPFille.addProperty("isPaid", journal != null ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                            jsonNPFilleList.add(jsonNPFille);

                        }

                        jsonNP.addProperty("npFilleList", jsonNPFilleList.toString());

                    } else {
                        jsonNP.addProperty("estFractionnee", GeneralConst.Number.ZERO);
                        jsonNP.addProperty("npFilleList", GeneralConst.EMPTY_STRING);
                    }

                    jsonNotePerceptionList.add(jsonNP);

                }
                return jsonNotePerceptionList.toString();
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

    }

    public String accuserReception(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {
            String typeDocument = request.getParameter(GeneralConst.AccuserReception.TYPE_DOCUMENT);

            String DAY_NP_ECHEANCE = GeneralConst.EMPTY_STRING;

            switch (typeDocument) {
                case DocumentConst.DocumentCode.NP:
                    DAY_NP_ECHEANCE = propertiesMessage.getProperty(NotePerceptionConst.ParamName.NBRE_DAY_NP_ECHEANCE);
                    break;
                case DocumentConst.DocumentCode.AMR:
                    DAY_NP_ECHEANCE = propertiesMessage.getProperty(NotePerceptionConst.ParamName.NBRE_DAY_AMR_ECHEANCE);
                    break;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadMiniers(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            List<Personne> listPersonneMinier = TaxationBusiness.getListPersonneMinier();

            if (!listPersonneMinier.isEmpty()) {

                List<JsonObject> jsonObjectList = new ArrayList<>();

                for (Personne personneMinier : listPersonneMinier) {

                    JsonObject jsonObject = new JsonObject();

                    jsonObject.addProperty("codeMinier", personneMinier.getCode());
                    jsonObject.addProperty("nameMinier", personneMinier.toString().toUpperCase());

                    jsonObjectList.add(jsonObject);

                }

                dataReturn = jsonObjectList.toString();

            }

        } catch (Exception e) {
            dataReturn = "";
        }

        return dataReturn;
    }

    public String loadSousProvisionMinier(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codePersonne = request.getParameter("codePersonne");

            List<Jdossier> listSousProvision = TaxationBusiness.getListSousprovisionByMinier(codePersonne);

            if (!listSousProvision.isEmpty()) {

                List<JsonObject> jsonObjectList = new ArrayList<>();

                for (Jdossier jdossier : listSousProvision) {

                    JsonObject jsonObject = new JsonObject();
                    Personne personne = getPersonneByCode(jdossier.getDossier());

                    jsonObject.addProperty("id", jdossier.getId());
                    jsonObject.addProperty("nameMinier", personne.toString().toUpperCase());
                    jsonObject.addProperty("credit", jdossier.getCredit());
                    jsonObject.addProperty("debit", jdossier.getDebit());
                    jsonObject.addProperty("solde", jdossier.getSolde());
                    jsonObject.addProperty("devise", jdossier.getDevise());
                    jsonObject.addProperty("tonnageInit", jdossier.getTonnageInitial());
                    jsonObject.addProperty("tonnageRestant", jdossier.getTonnageRestant());
                    jsonObject.addProperty("dateCreate", ConvertDate.formatDateHeureToString(jdossier.getDateCreat()));
                    jsonObject.addProperty("dateDerniereSortie", ConvertDate.formatDateHeureToString(jdossier.getDateMaj()));
                    jsonObject.addProperty("notePerception", jdossier.getReference());

                    jsonObject.addProperty("taxeCode", jdossier.getFkAb());

                    if (jdossier.getFkAb().equals("00000000000001072016")) {
                        jsonObject.addProperty("taxeName", "concentré".toUpperCase());
                    } else if (jdossier.getFkAb().equals("00000000000001212016")) {
                        jsonObject.addProperty("taxeName", "VOIRIE");
                    } else {
                        jsonObject.addProperty("taxeName", "");
                    }

                    List<ComplementInfoTaxe> listComplementInfoTaxe = TaxationBusiness.getListComplementInfoTaxeByNP(jdossier.getReference().trim());

                    if (!listComplementInfoTaxe.isEmpty()) {

                        List<JsonObject> jsonComplementInfoTaxeList = new ArrayList<>();

                        for (ComplementInfoTaxe complementInfoTaxe : listComplementInfoTaxe) {

                            JsonObject jsonComplementInfoTaxe = new JsonObject();

                            jsonComplementInfoTaxe.addProperty("numeroNP", jdossier.getReference());
                            jsonComplementInfoTaxe.addProperty("transporteur", complementInfoTaxe.getTransporteur().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("produit", complementInfoTaxe.getNatureProduit().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("plaque", complementInfoTaxe.getNumeroPlaque().toUpperCase());
                            jsonComplementInfoTaxe.addProperty("modePaiement", complementInfoTaxe.getModePaiement().toUpperCase());

                            jsonComplementInfoTaxe.addProperty("tonnageSortie", complementInfoTaxe.getTonnageSortie() == null ? 0 : complementInfoTaxe.getTonnageSortie());
                            jsonComplementInfoTaxe.addProperty("montantTonnageSortie", complementInfoTaxe.getMontantTonnageSortie() == null ? 0 : complementInfoTaxe.getMontantTonnageSortie());

                            String dateValidation = GeneralConst.EMPTY_STRING;

                            if (complementInfoTaxe.getAgentValidate() != null) {

                                Agent agentValidate = new Agent();
                                agentValidate = GeneralBusiness.getAgentByCode(complementInfoTaxe.getAgentValidate() + "");

                                jsonComplementInfoTaxe.addProperty("agentValidate", agentValidate.toString().toUpperCase());

                            } else {
                                jsonComplementInfoTaxe.addProperty("agentValidate", "");
                            }

                            if (complementInfoTaxe.getDateValidation() != null) {

                                dateValidation = ConvertDate.formatDateHeureToString(complementInfoTaxe.getDateValidation());

                                jsonComplementInfoTaxe.addProperty("dateValidation", dateValidation);

                            } else {
                                jsonComplementInfoTaxe.addProperty("dateValidation", "");

                            }

                            jsonComplementInfoTaxeList.add(jsonComplementInfoTaxe);
                        }

                        jsonObject.addProperty("complementList", jsonComplementInfoTaxeList.toString());

                    } else {
                        jsonObject.addProperty("complementExist", GeneralConst.Number.ZERO);
                        jsonObject.addProperty("complementList", "");
                    }

                    jsonObjectList.add(jsonObject);

                }

                dataReturn = jsonObjectList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
