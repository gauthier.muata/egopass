package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AgentService.class)
public class AgentService_ { 

    public static volatile SingularAttribute<AgentService, Integer> fkAgent;
    public static volatile SingularAttribute<AgentService, Integer> id;
    public static volatile SingularAttribute<AgentService, Date> dateCreate;
    public static volatile SingularAttribute<AgentService, Integer> etat;
    public static volatile SingularAttribute<AgentService, String> fkService;

}