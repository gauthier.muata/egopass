<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : emmanuel.tsasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des bureaux</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label style="margin-left: 5px">Bureau / Site :</label>
                            <input class="form-control" id="inputSite" style="margin-left: 5px">

                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="input-group-btn">
                            <button class="btn btn-warning" id="btnCleanFields" style="margin-top: 25px"><i class="fa fa-close"></i> Nettoyer champs</button>
                        </div>

                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label style="margin-left: 5px">Adresse :</label>
                            <textarea class="form-control" rows="2" readonly="true"
                                      id="inputAdresseSite" style="margin-left: 5px"></textarea>

                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="input-group-btn">
                            <button class="btn btn-primary" id="btnSelectAdresse" style="margin-top: 35px"><i class="fa fa-search"></i> Sélectionner</button>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label style="margin-left: 5px">Responsable :</label>
                            <input class="form-control" readonly="true" id="inputResponsableSite" style="margin-left: 5px">

                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="input-group-btn">
                            <button class="btn btn-primary" id="btnCallModalSearchResponsable" style="margin-top: 25px"><i class="fa fa-search"></i> Rechercher</button>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <div class="form-group">

                            <p class="control-label col-sm-1" for="cmbDivisionBureau">
                                Division 
                            <div class="col-sm-5">
                                <select class="form-control" id="cmbDivisionBureau"></select>
                            </div>

                            

                            <div class="col-lg-4">
                                <input class="form-control" placeholder="Sigle bureau" style="width: 100%" id="inputSigleBureau"/>
                            </div>


                        </div>   

                        <div class="col-lg-2">
                            <div class="input-group-btn">
                                <button  
                                    id="btnSaveSite"
                                    class="btn btn-success"
                                    style="margin-left: 10px">
                                    <i class="fa fa-save"></i>  
                                    Enregistrer
                                </button>
                            </div>

                        </div>

                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <p class="control-label col-sm-2" for="selectTypeSite">
                                Type bureau/site 
                            <div class="col-sm-4">
                                <select class="form-control" id="selectTypeSite">
                                    <option value="0">--</option>
                                    <option value="1">BUREAU DRHKAT</option>
                                    <option value="2">BUREAU PEAGE</option>
                                </select>
                            </div>
                        
                        <p class="control-label col-sm-1" for="cmdSelectTypeEntite">
                                Type entité 
                            <div class="col-sm-3">
                                <select class="form-control" id="cmdSelectTypeEntite">
                                    <option value="0">--</option>
                                    <option value="1">COMMUNE</option>
                                    <option value="2">LOCALITE</option>
                                    <option value="3">AUTRES</option>
                                </select>
                            </div>

                    </div>
                </div>
                <hr/>
                
                <div class="journal" style="margin-left: 5px">
                    <table id="tableSites" style="width: 100%" class="table-bordered responsive">

                    </table>
                </div>

            </div>

        </div>

        <div class="modal fade" id="agentModal" 
             role="dialog"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" 
                            id="exampleModalLongTitle">Rechercher et s&eacute;lectionner un agent responsable</h5>
                    </div>
                    <div class="modal-body" id="divAssujetti">
                        <div class="navbar-form" role="search">
                            <div class="row">
                                <div class="col-lg-6 divInputGroupSearch">
                                    <div class="input-group">
                                        <input 
                                            type="text" class="form-control" 
                                            id="inputValueResearchAgent" 
                                            placeholder="Crit&egrave;re de recherche"/>
                                        <div class="input-group-btn">
                                            <button 
                                                class="btn btn-primary" 
                                                id="btnSearchResponsable">
                                                <i class="fa fa-search"></i>
                                                <span class="spanModalSearchAssuj">Rechercher</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div>
                                <table class="table table-bordered table-hover table-responsive" id="tableResultSearchAgent"></table>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" 
                                id="btnSelectAgent"
                                style="display: none">
                            <i class="fa fa-check-circle"></i> &nbsp;
                            S&eacute;lectionner</button>
                        <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalAdresses" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" id="exampleModalLongTitle">Registre des adresses</h5>
                    </div>
                    <div class="modal-body" id="divAdresses">
                        <div class="navbar-form" role="search">
                            <div class="row">
                                <div class="col-lg-6 divInputGroupSearch">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="inputLibelle" 
                                               placeholder="Veuillez saisir l'adresse">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary" id="btnRechercherAdresses">
                                                <i class="fa fa-search-plus"></i>
                                                <span class="spanModalSearchAssuj">Rechercher</span>                                                                    
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <a id="btnAddEntite"
                                       class="btn btn-warning pull-right">
                                        <i class="fa fa-plus-circle"></i>  
                                        <span class="spanModalSearchAssuj">Nouvelle adresse</span>                                                        
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="divAdressModal">
                            <table 
                                width="100%" 
                                class="table table-bordered table-hover" 
                                id="tableAdresses">                                                       
                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <div class="navbar-form" >
                            <label id="labelSelectedAdresse" style="float:left;font-weight: normal"></label>
                            <div class="input-group" >
                                <input type="text" 
                                       class="form-control" 
                                       id="inputNumber" 
                                       placeholder="Numéro" 
                                       size="5"
                                       style="visibility: hidden">

                                <div class="input-group-btn">
                                    <button 
                                        class="btn btn-success" 
                                        id="btnValiderAdresse"
                                        style="visibility: hidden">
                                        <i class="fa fa-check-circle"></i>
                                        Valider
                                    </button>
                                    <button class="btn btn-default" 
                                            id="btnValiderAdresse"
                                            data-dismiss="modal">
                                        Fermer
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <%@include file="assets/include/modal/addEntiteAdministrative.xhtml" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>

        <script type="text/javascript" src="assets/js/entityAdministrative.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchAdresse.js"></script>
        <script type="text/javascript" src="assets/js/modalSearchPersonneAgent.js"></script>
        <script type="text/javascript" src="assets/js/gestionSite/editionsite.js"></script>

    </body>
</html>
