/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author gauthier.muata
 */
public class PaiementConst {

    public class ParamName {

        public final static String SESSION = "session";
        public final static String OPERATION = "operation";
        public final static String TYPE_SEARCH = "typeSearch";
        public final static String VALUE_SEACH = "valueSearch";
        public final static String VALUE_PAGE = "page";

        public final static String SITE_VALUE = "siteValue";
        public final static String SITE_VALUE_LIST = "siteValueList";
        public final static String BANQUE_VALUE = "banqueValue";
        public final static String BANQUE_NAME = "banqueName";
        public final static String COMPTE_BANCAIRE_VALUE = "compteBancaireValue";
        public final static String PERIODE_DEBUT_VALUE = "periodeDebutValue";
        public final static String PERIODE_FIN_VALUE = "periodeFinValue";

        public final static String DATE_PAIEMENT = "datePaiement";
        public final static String DATE_VALIDATION_PAIEMENT = "dateValidationPaiement";
        public final static String AGENT_VALIDATION_PAIEMENT = "agentValidationPaiement";
        public final static String DOCUMENT = "document";
        public final static String REQUERANT = "requerant";
        public final static String REFERENCE = "reference";
        public final static String CODE_JOURNAL = "codeJournal";
        public final static String DOCUMENT_APURE = "documentApure";
        public final static String BORDEREAU = "bordereau";
        public final static String COMPTE_BANCAIRE_BORDEREAU = "compteBancaireBordereau";
        public final static String DATE_BORDERAU = "dateBordereau";
        public final static String LIBELLE_BANQUE = "banque";
        public final static String COMPTE_BANCAIRE = "compteBancaire";
        public final static String DEVISE = "devise";
        public final static String MONTANT_PAYE = "montantPaye";
        public final static String ETAT_PAIEMENT = "etatPaiement";
        public final static String ID_ETAT_PAIEMENT = "idEtatPaiement";
        public final static String AGENT_PAIEMENT = "agentPaiement";
        public final static String NUMERO_ATTESTATION = "numeroAttestation";
        public final static String NUMERO_DECLARATION = "numeroDeclaration";
        public final static String CENTRE = "centre";
        public final static String NOM_COMPLET = "nomComplet";
        public final static String DETAIL_BORDEREAU_LIST = "detailBordereauList";
        public final static String MESSAGE_PAIEMENT = "messagePaiement";

        //public final static String CODE_JOURNAL = "codeJournal";
        //public final static String DOCUMENT_APURE = "documentApure";
        
        public final static String AGENT_APUREMENT = "agentApurement";
        public final static String MONTANT_APURE = "montantApure";
        public final static String NUMERO_RELEVE_BANCAIRE = "numeroReleveBancaire";
        public final static String DATE_RELEVE_BANCAIRE = "dateReleveBancaire";
        public final static String ETAT_APUREMENT = "etatApurement";
        public final static String OBSERVATION_APUREMENT = "observationApurement";
        public final static String REFERENCE_PAIEMENT = "referencePaiement";
        
        public final static String PAIEMENT_EXISTING = "paiementExisting";
        public final static String TYPE_DOCUMENT = "typeDocument";
        public final static String TYPE_DOCUMENT_CODE = "typeDocumentCode";
        public final static String ALL_SITE = "allSite";
        public final static String ADVANCED_SEARCH = "advancedSearch";
        public final static String AGENT_OR_SITE = "agentOrSite";
        public final static String FROM_RESEARCH_PAYMENT = "fromResearch";
        
        public final static String ARTICLE_BUDGETAIRE_NAME = "articleBudgetaireName";
        public final static String MONTANT_PERCU = "montantPercu";
        public final static String PERIODE_NAME = "periodeName";
        public final static String BORDEREAU_ID = "bordereauId";
        public final static String REFERENCE_RECORD = "referenceRecord";
        public final static String OBSERVATION = "observation";
        public final static String VALUE_TRAITEMENT = "valueTraitement";


    }

    public class Operation {

        public final static String OPERATION = "operation";
        public final static String SEARCH_PAYMENT_FROM_BANK = "searchPaymentFromBank";
        public final static String SEARCH_PAYMENT_FROM_ADMINISTRATION = "searchPaymentFromAdministration";
        public final static String SEARCH_PAYMENT_ADVENCED = "searchPaymentAdvenced";
        public final static String VALIDATION_APUREMENT_ADMIN = "validationApurementAdmin";
        public final static String VALIDATION_APUREMENT_COMPT = "validationApurementCompt";
        public final static String SAVE_BORDEREAU = "saveBordereau";
        public final static String SAVE_JOURNAL = "saveJournal";
        public final static String SAVE_DECLARATION = "saveDeclaration";
        public final static String TRAITEMENT_PAIEMENT = "traitementPaiement";
        public final static String SEARCH_PAYMENT = "searchPayment";

    }

    public class ParamNameDocument {

        public static final String NP = "NP";
        public static final String NOTE_PERCEPTION = "NOTE DE PERCEPTION";
        public static final String BAP = "BAP";
        public static final String BP = "BP";
        public static final String BON_A_PAYE = "BON A PAYE";
        public static final String AMR_ONE = "AMR1";
        public static final String AMR_TWO = "AMR2";
        public static final String AMR = "AMR";
        public static final String DEC = "DEC";
        public static final String DECLARATION = "NOTE DE TAXATION";

    }

    public class ParamQuery {

        public static final String PAYEMENT = "PAYEMENT";
        public static final String CODE_JOURNAL = "CODE_JOURNAL";
        public static final String DOCUMENT_APURE = "DOCUMENT_APURE";
        public static final String AGENT_APUREMENT = "AGENT_APUREMENT";
        public static final String MONTANT_APURRE = "MONTANT_APURRE";
        public static final String NUMERO_RELEVE_BANCAIRE = "NUMERO_RELEVE_BANCAIRE";
        public static final String DATE_RELEVE_BANCAIRE = "DATE_RELEVE_BANCAIRE";

    }

    public class Format {

        public static final String FORMAT_DATE = "dd/MMM/yyyy";

    }

    public class ResultCode {

        public final static String BORDEREAU_EXISTING = "700";

    }

}
