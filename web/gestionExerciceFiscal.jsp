<%-- 
    Document   : gestionExerciceFiscal
    Created on : 27 janvier 2020, 13:13:41
    Author     : juslin.tshiamua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion exercice fiscal</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >
                    <div class="row" style="margin-bottom: 5px;">
                        <div class="col-lg-12">                         
                            <label style="font-weight: normal">Champs obligatoire <span style="color:red">(*)</span></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> Informations de base</h4>
                                </div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">                

                                        <div class="form-group" style="display: none" id="div_code">
                                            <p class="control-label col-sm-4" for="code">
                                                Code
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="code" disabled="true"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <p class="control-label col-sm-4" for="inputNom">
                                                Intitul&eacute; <span style="color:red"> *</span></p>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="inputTitre" placeholder="Intitulé"/>
                                            </div>
                                        </div>

                                        <div class="form-group" >
                                            <p class="control-label col-sm-4" for="dateDebut">
                                                D&eacute;but
                                            <div class="col-sm-8">
                                                <div class="input-group date" id="datePicker1" style="width: 100%;">
                                                    <input type="text" class="form-control" id="dateDebut">
                                                    <div class="input-group-addon">
                                                        <span class="fa fa-th"></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        
                                        <div class="form-group" >
                                            <p class="control-label col-sm-4" for="dateDebut">
                                                Cl&ocirc;ture
                                            <div class="col-sm-8">
                                                <div class="input-group date" id="datePicker2" style="width: 100%;">
                                                    <input type="text" class="form-control" id="dateCloture" disabled="true">
                                                    <div class="input-group-addon">
                                                        <span class="fa fa-th"></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group" >
                                            <button class="btn btn-success" id="btnEnregistrer" style="float: right; margin-right: 15px">
                                                <i class="fa fa-save"></i>  Enregistrer
                                            </button>
                                        </div>

                                    </div>


                                </div>

                            </div>

                            <div class='table-responsive'>
                                <table 
                                    class="table table-bordered table-hover" 
                                    id="registreExerciceFiscal">
                                </table>
                            </div>

                        </div>
                    </div>
                </form> 

            </div>

        </div>

        <%@include file="assets/include/modal/header.html" %>
        <%@include file="assets/include/modal/addEntiteAdministrative.xhtml" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/articleBudgetaire/gestionExerciceFiscal.js"></script>
    </body>
</html>
