package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.SiteBanque;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Banque.class)
public class Banque_ { 

    public static volatile SingularAttribute<Banque, String> agentMaj;
    public static volatile SingularAttribute<Banque, String> codeSuift;
    public static volatile SingularAttribute<Banque, String> code;
    public static volatile SingularAttribute<Banque, Date> dateCreat;
    public static volatile ListAttribute<Banque, SiteBanque> siteBanqueList;
    public static volatile SingularAttribute<Banque, String> intitule;
    public static volatile SingularAttribute<Banque, Short> etat;
    public static volatile SingularAttribute<Banque, Date> dateMaj;
    public static volatile SingularAttribute<Banque, String> sigle;
    public static volatile SingularAttribute<Banque, String> token;
    public static volatile SingularAttribute<Banque, Boolean> estConnecter;
    public static volatile SingularAttribute<Banque, String> agentCreat;
    public static volatile ListAttribute<Banque, CompteBancaire> compteBancaireList;

}