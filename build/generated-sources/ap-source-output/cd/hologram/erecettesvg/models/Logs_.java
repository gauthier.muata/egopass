package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Logs.class)
public class Logs_ { 

    public static volatile SingularAttribute<Logs, String> level;
    public static volatile SingularAttribute<Logs, String> logger;
    public static volatile SingularAttribute<Logs, Date> dateD;
    public static volatile SingularAttribute<Logs, Integer> logId;
    public static volatile SingularAttribute<Logs, Integer> id;
    public static volatile SingularAttribute<Logs, String> message;
    public static volatile SingularAttribute<Logs, String> userId;

}