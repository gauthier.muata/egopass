/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class BapPrint implements Serializable {

    String logo;
    String codeQR;
    String numImpot;
    String nomAssujetti;
    String adresseAssujetti;
    String telephoneAssujetti;
    String emailAssujetti;
    String dateImpression;

    String userPrintDocument;
    String functionUserDocument;
    
    String numeroBap;
    String compteBancaireBap;
    String faitGenerateur;
    String motif;
    String montantEnChiffre;
    String montantEnLettre;
    String nomReceveur;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNumImpot() {
        return numImpot;
    }

    public void setNumImpot(String numImpot) {
        this.numImpot = numImpot;
    }

    public String getNomAssujetti() {
        return nomAssujetti;
    }

    public void setNomAssujetti(String nomAssujetti) {
        this.nomAssujetti = nomAssujetti;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTelephoneAssujetti() {
        return telephoneAssujetti;
    }

    public void setTelephoneAssujetti(String telephoneAssujetti) {
        this.telephoneAssujetti = telephoneAssujetti;
    }

    public String getEmailAssujetti() {
        return emailAssujetti;
    }

    public void setEmailAssujetti(String emailAssujetti) {
        this.emailAssujetti = emailAssujetti;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getUserPrintDocument() {
        return userPrintDocument;
    }

    public void setUserPrintDocument(String userPrintDocument) {
        this.userPrintDocument = userPrintDocument;
    }

    public String getFunctionUserDocument() {
        return functionUserDocument;
    }

    public void setFunctionUserDocument(String functionUserDocument) {
        this.functionUserDocument = functionUserDocument;
    }

    public String getNumeroBap() {
        return numeroBap;
    }

    public void setNumeroBap(String numeroBap) {
        this.numeroBap = numeroBap;
    }

    public String getCompteBancaireBap() {
        return compteBancaireBap;
    }

    public void setCompteBancaireBap(String compteBancaireBap) {
        this.compteBancaireBap = compteBancaireBap;
    }

    public String getFaitGenerateur() {
        return faitGenerateur;
    }

    public void setFaitGenerateur(String faitGenerateur) {
        this.faitGenerateur = faitGenerateur;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public String getMontantEnChiffre() {
        return montantEnChiffre;
    }

    public void setMontantEnChiffre(String montantEnChiffre) {
        this.montantEnChiffre = montantEnChiffre;
    }

    public String getMontantEnLettre() {
        return montantEnLettre;
    }

    public void setMontantEnLettre(String montantEnLettre) {
        this.montantEnLettre = montantEnLettre;
    }

    public String getNomReceveur() {
        return nomReceveur;
    }

    public void setNomReceveur(String nomReceveur) {
        this.nomReceveur = nomReceveur;
    }
    
    
    
}
