package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AvisRegularisation.class)
public class AvisRegularisation_ { 

    public static volatile SingularAttribute<AvisRegularisation, Date> dateCreat;
    public static volatile SingularAttribute<AvisRegularisation, String> personne;
    public static volatile SingularAttribute<AvisRegularisation, Date> dateEcheance;
    public static volatile SingularAttribute<AvisRegularisation, Integer> id;
    public static volatile SingularAttribute<AvisRegularisation, String> agentCreat;
    public static volatile SingularAttribute<AvisRegularisation, Integer> etat;
    public static volatile SingularAttribute<AvisRegularisation, Date> dateReception;

}