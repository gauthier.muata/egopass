/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var miseEnDemeureList = [];

var selectTypeSearch;
var valueTypeSearch;
var selectTypeMiseEnDemeure;

var selectTypeSearchID;
var selectTypeMiseEnDemeureID;
var selectStateMedID;
var btnSimpleSearch;
var isAdvanced;

var responsibleObject = new Object();
var tableMiseEnDemeure;
var modalAccuserReception;

var codeTypeDoc = 'MEP';
var codeDoc = undefined;

var tempDocumentList = [];
var archivesMep = '';
var lblNbDocumentEchelonnement;

var modalRechercheAvanceeMed;
var selectService;
var selectStateMed;

var inputDateDebutMed;
var inputdateLastMed;

var datePickerMed1;
var datePickerMed2;

var btnShowAdvancedSerachModal;
var btnAdvencedSearchMed;
var codeMedCurrent;

var isAdvance;

var lblService, lblTypeMed, lblStateMed, lblDateDebut, lblDateFin;

var modalFichePriseEnCharge;

var periodeIDSelected;
var fromCall;

var penalitieList = [];

var amountPenalite;
var amountPenaliteFormat;
var medIDSelected;
var numberRows;
var deviseSelected;
var dateDebuts, dateFins;
     

$(function () {

    fromCall = '0';
    numberRows = empty;

    mainNavigationLabel.text('POURSUITES');
    secondNavigationLabel.text('Registre des mises en demeure');
    removeActiveMenu();
    linkMenuContentieux.addClass('active');

    selectTypeSearch = $('#selectTypeSearch');
    valueTypeSearch = $('#valueTypeSearch');
    selectTypeMiseEnDemeure = $('#selectTypeMiseEnDemeure');
    btnSimpleSearch = $('#btnSimpleSearch');
    tableMiseEnDemeure = $('#tableMiseEnDemeure');
    modalAccuserReception = $('#modalAccuserReception');

    lblNbDocumentEchelonnement = $('#lblNbDocumentEchelonnement');

    modalRechercheAvanceeMed = $('#modalRechercheAvanceeMed');
    selectService = $('#selectService');
    selectStateMed = $('#selectStateMed');

    inputDateDebutMed = $('#inputDateDebutMed');
    inputdateLastMed = $('#inputdateLastMed');

    datePickerMed1 = $('#datePickerMed1');
    datePickerMed2 = $('#datePickerMed2');
    btnAdvencedSearchMed = $('#btnAdvencedSearchMed');
    isAdvance = $('#isAdvance');

    lblService = $('#lblService');
    lblTypeMed = $('#lblTypeMed');
    lblStateMed = $('#lblStateMed');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');

    modalFichePriseEnCharge = $('#modalFichePriseEnCharge');

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    datePickerMed1.datepicker("setDate", new Date());
    datePickerMed2.datepicker("setDate", new Date());

    btnShowAdvancedSerachModal = $('#btnShowAdvancedSerachModal');

    selectTypeMiseEnDemeure.on('change', function (e) {

        selectTypeMiseEnDemeureID = selectTypeMiseEnDemeure.val();

        if (selectTypeMiseEnDemeureID === '0') {

            alertify.alert('Veuillez sélectionner un type de mise en demeure valide.');
            return;

        }
    });

    selectStateMed.on('change', function (e) {
        selectStateMedID = selectStateMed.val();
    });

    selectTypeSearch.on('change', function (e) {

        selectTypeSearchID = selectTypeSearch.val();
        valueTypeSearch.val(empty);

        switch (selectTypeSearchID) {
            case '1':
                valueTypeSearch.attr('readonly', true);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
                break;
            case '2':
                valueTypeSearch.attr('readonly', false);
                valueTypeSearch.attr('placeholder', 'Veuillez saisir le numéro de la mise en demeure');
                valueTypeSearch.attr('style', 'font-weight:normal;width: 380px');

                break;
        }
    });

    btnShowAdvancedSerachModal.on('click', function (e) {
        e.preventDefault();
        modalRechercheAvanceeMed.modal('show');
    });

    btnAdvencedSearchMed.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '1';

        valueTypeSearch.val(empty);
        valueTypeSearch.attr('readonly', true);
        valueTypeSearch.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
        loadMiseEnDemeure(isAdvanced);
    });

    btnSimpleSearch.on('click', function (e) {
        e.preventDefault();
        isAdvanced = '0';
        switch (selectTypeSearchID) {
            case '1':
                assujettiModal.modal('show');
                break;
            case '2':

                if (valueTypeSearch.val() === empty) {

                    alertify.alert('Veuillez saisir le numéro de mise en demeure avant de lancer la recheche');
                    return;

                } else {
                    loadMiseEnDemeure(isAdvanced);
                }

                break;
        }

    });

    selectTypeSearchID = selectTypeSearch.val();
    selectTypeMiseEnDemeureID = selectTypeMiseEnDemeure.val();
    selectStateMedID = selectStateMed.val();

    getMED();

});

function loadMiseEnDemeure(isAdvanced) {

    switch (isAdvanced) {
        case '0':
            isAdvance.attr('style', 'display: none');
            break;
        case '1':

            lblTypeMed.text($('#selectTypeMiseEnDemeure option:selected').text().toUpperCase());
            lblTypeMed.attr('title', $('#selectTypeMiseEnDemeure option:selected').text().toUpperCase());

            lblStateMed.text($('#selectStateMed option:selected').text().toUpperCase());
            lblStateMed.attr('title', $('#selectStateMed option:selected').text().toUpperCase());

            if (selectService.val() === '*') {
                lblService.text('TOUS');
                lblService.attr('title', 'TOUS');
            } else {
                lblService.text($('#selectService option:selected').text().toUpperCase());
                lblService.attr('title', $('#selectService option:selected').text().toUpperCase());
            }

            lblDateDebut.text(inputDateDebut.val());
            lblDateDebut.attr('title', inputDateDebut.val());

            lblDateFin.text(inputdateLast.val());
            lblDateFin.attr('title', inputdateLast.val());

            isAdvance.attr('style', 'display: inline');

            break;
    }

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': selectService.val(),
            'codeTypeMiseEnDemeure': selectTypeMiseEnDemeureID,
            'codeStateMiseEnDemeure': selectStateMedID,
            'dateDebut': inputDateDebutMed.val(),
            'dateFin': inputdateLastMed.val(),
            'valueSearch': selectTypeSearchID === '1' ? responsibleObject.codeResponsible : valueTypeSearch.val(),
            'advancedSearch': isAdvanced,
            'typeSearch': selectTypeSearchID,
            'operation': 'loadMiseEnDemeure'

        },
        beforeSend: function () {
            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeMed.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeMed.unblock();
                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeMed.modal('hide');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '0':
                                printMiseEnDemeure('');
                                alertify.alert('Cet assujetti : ' + valueTypeSearch.val() + ' n\'a pas de mise en demeure');
                                break;
                            case '1':
                                printMiseEnDemeure('');
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'une mise en demeure');
                                break;
                        }
                        break;
                    case '1':
                        alertify.alert('Aucune mise en demeure ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                miseEnDemeureList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeMed.modal('hide');
                }

                printMiseEnDemeure(miseEnDemeureList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeMed.unblock();
                    break;
            }
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {

    valueTypeSearch.val(selectAssujettiData.nomComplet);
    valueTypeSearch.attr('style', 'font-weight:bold;width: 380px');

    responsibleObject = new Object();

    responsibleObject.codeResponsible = selectAssujettiData.code;
    responsibleObject.codeFormeJuridique = selectAssujettiData.codeForme;
    responsibleObject.adresseId = selectAssujettiData.codeAdresse;

    loadMiseEnDemeure(isAdvanced);

}

function printMiseEnDemeure(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left">NUMERO</th>';
    tableContent += '<th style="text-align:center">TYPE</th>';
    tableContent += '<th style="text-align:center">EXERCICE</th>';
    tableContent += '<th style="text-align:left">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:left">NTD</th>';
    tableContent += '<th style="text-align:left">ASSUJETTI</th>';

    tableContent += '<th style="text-align:left">DATE RECEPTION</th>';

    tableContent += '<th style="text-align:left">DATE ECHEANCE</th>';

    tableContent += '<th style="text-align:right">MONTANT DÛ</th>';

    tableContent += '<th style="text-align:right">PENALITE DÛ</th>';

    tableContent += '<th style="text-align:center"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var buttonCallNextStap = '';
        var caseMed;

        switch (result[i].typeMed) {

            case 'DECLARATION':
                caseMed = parseInt(3);
                break;

            case 'PAIEMENT':
                caseMed = parseInt(2);

                break;
        }

        generatePenalies(result[i].referenceID, result[i].amountMed, result[i].deviseMed, result[i].isRecidiviste, result[i].nombreMois, caseMed);

        var amount = formatNumber(result[i].amountMed, result[i].deviseMed);
        var buttonMed = '';

        var buttonCallSaveFichePriseEnCharge = '';
        var buttonCallFichePriseEnCharge = ''

        amountPenalite = result[i].amountPenaliteMed;

        amountPenaliteFormat = formatNumber(amountPenalite, result[i].deviseMed);

        if (result[i].fpcExist == '0') {

            amountPenalite = getSumByReference(result[i].referenceID);
            amountPenaliteFormat = formatNumber(amountPenalite, result[i].deviseMed);

            buttonCallSaveFichePriseEnCharge = '&nbsp;<button class="btn btn-success" onclick="callSaveFichePriseEnCharge(\'' + result[i].numeroMed + '\',\'' + result[i].amountMed + '\',\'' + result[i].referenceID + '\',\'' + result[i].deviseMed + '\',\'' + result[i].isRecidiviste + '\',\'' + result[i].nombreMois + '\',\'' + amountPenalite + '\',\'' + caseMed + '\')"><i class="fa fa-save"></i></button>';
            buttonCallFichePriseEnCharge = '&nbsp;<button class="btn btn-primary" onclick="callFichePriseEnCharge(\'' + result[i].numeroMed + '\',\'' + result[i].amountMed + '\',\'' + result[i].referenceID + '\',\'' + result[i].deviseMed + '\',\'' + result[i].isRecidiviste + '\',\'' + result[i].nombreMois + '\',\'' + result[i].referenceName + '\',\'' + caseMed + '\')"><i class="fa fa-list"></i></button>';
        }

        var buttonCallTaxationOffice = '&nbsp;<button class="btn btn-primary" onclick="callTaxationOffice(\'' + result[i].numeroMed + '\',\'' + result[i].amountMed + '\',\'' + result[i].referenceID + '\',\'' + result[i].referenceName + '\',\'' + result[i].deviseMed + '\',\'' + result[i].isRecidiviste + '\',\'' + result[i].nombreMois + '\')"><i class="fa fa-list"></i></button>'

        if (result[i].dateEcheanceMed === empty) {

            echeanceMedExist = '1';

            buttonCallSaveFichePriseEnCharge = empty;
            buttonCallFichePriseEnCharge = empty;

        } else {
            echeanceMedExist = '2';
        }

        var size1 = '';
        var size2 = '';
        var color = '';
        var fpcInfo = '';

        if (result[i].activateNextStap === '1') {

            size1 = '15%';
            size2 = '10%';
            color = 'red';

            switch (result[i].typeMed) {

                case 'DECLARATION':
                    buttonCallNextStap = buttonCallTaxationOffice;
                    break;

                case 'PAIEMENT':
                    buttonCallNextStap = buttonCallFichePriseEnCharge;
                    break;
            }

        } else {

            size1 = '15%';
            size2 = '10%';

            if (result[i].fpcExist == '1') {
                var txt = 'La fiche prise en chage est générée';
                fpcInfo = '<hr/><span style="color:red;font-weight:normal">' + txt + '</span>';
                color = 'red';
                size2 = '5%';
            } else {
                color = 'green';
            }



            buttonCallSaveFichePriseEnCharge = empty;
            buttonCallFichePriseEnCharge = empty;
            buttonCallNextStap = empty;//buttonCallFichePriseEnCharge;
            buttonCallTaxationOffice = empty;
        }

        buttonMed = '<button class="btn btn-warning" onclick="accuserReception(\'' + echeanceMedExist + '\',\'' + result[i].numeroMed + '\')"><i class="fa fa-print"></i></button>' + buttonCallNextStap;

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].numeroMed + '</td>';
        tableContent += '<td style="text-align:center;width:5%;vertical-align:middle">' + result[i].typeMed + '</td>';
        tableContent += '<td style="text-align:center;width:8%;vertical-align:middle">' + result[i].exerciceMed + '</td>';
        tableContent += '<td style="text-align:left;width:' + size1 + ';vertical-align:middle">' + result[i].articleBudgetaireName + '</td>';
        tableContent += '<td style="text-align:left;width:10%;vertical-align:middle">' + result[i].userName + '</td>';
        tableContent += '<td style="text-align:left;width:20%;vertical-align:middle">' + result[i].assujettiNameComposite + '</td>';

        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle">' + result[i].dateReceptionMed + '</td>';

        tableContent += '<td style="text-align:left;width:8%;vertical-align:middle;color:' + color + '">' + result[i].dateEcheanceMed + '</td>';

        tableContent += '<td style="text-align:right;width:9%;vertical-align:middle;font-weight:bold">' + amount + '</td>';

        //tableContent += '<td style="text-align:right;width:9%;vertical-align:middle;font-weight:bold">' + formatNumber(amountPenalite, result[i].deviseMed) + '</td>';
        tableContent += '<td style="text-align:right;width:9%;vertical-align:middle;font-weight:bold"><span id="lblPenalite_' + result[i].numeroMed + '">' + amountPenaliteFormat + fpcInfo + '</span></td>';
        tableContent += '<td style="text-align:center;width:' + size2 + ';vertical-align:middle">' + buttonMed + buttonCallSaveFichePriseEnCharge + '</td>';
        tableContent += '</tr>';
        tableContent += '<hr/>';

    }
    tableContent += '</tbody>';
    tableMiseEnDemeure.html(tableContent);
    tableMiseEnDemeure.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: false,
        lengthChange: false,
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 4
    });

}

function accuserReception(echeanceMedExist, medID) {

    var typeMed = '';

    for (var i = 0; i < miseEnDemeureList.length; i++) {

        if (miseEnDemeureList[i].numeroMed === medID) {
            codeMedCurrent = miseEnDemeureList[i].numeroMed;
            medDocumentPrint = miseEnDemeureList[i].documentPrint;
            typeMed = miseEnDemeureList[i].typeMed;
            codeTypeDoc = typeMed;
        }
    }

    if (echeanceMedExist == '1') {

        initAccuseReceptionUI(codeMedCurrent, typeMed);

        modalAccuserReception.modal('show');

    } else {

        var msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure ?';

        switch (selectTypeMiseEnDemeure.val()) {
            case '1':
                msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure de déclaration ? ';
                break;
            case '2':
                msg = 'Etes-vous sûre de vouloir imprimer une mise en demeure au paiement ? ';
                break;
        }

        alertify.confirm(msg, function () {

            setTimeout(function () {
                setDocumentContent(medDocumentPrint);

                setTimeout(function () {
                }, 2000);
                window.open('visualisation-document', '_blank');
            }
            , 1);
        });
    }
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesMep = getUploadedData();
    archivesAccuser = archivesMep;

    nombre = JSON.parse(archivesMep).length;

    switch (nombre) {
        case 0:
            lblNbDocumentEchelonnement.text('');
            break;
        case 1:
            lblNbDocumentEchelonnement.html('1 document');
            break;
        default:
            lblNbDocumentEchelonnement.html(nombre + ' documents');
    }
}

function refrechDataAfterAccuserReception() {
    loadMiseEnDemeure(isAdvanced);
}

function callTaxationOffice(medID, montant, periodeID, periodeName, devise, isRecidiviste, monthLate) {

    periodeIDSelected = periodeID
    medIDSelected = medID;
    deviseSelected = devise;

    var obj = {};

    obj.amount = parseFloat(montant);
    obj.reference = periodeID;
    obj.libelleReference = periodeName;
    obj.currency = devise;
    obj.isRecidiviste = isRecidiviste == 1 ? true : false;
    obj.monthLate = parseInt(monthLate);
    obj.case = 3;

    initUIPenality(obj);
}

function callFichePriseEnCharge(medID, montant, periodeID, devise, isRecidiviste, monthLate, periodeName, typeMed) {

    periodeIDSelected = periodeID
    medIDSelected = medID;
    deviseSelected = devise;

    var obj = {};

    obj.amount = parseFloat(montant);
    obj.reference = typeMed == 2 ? periodeID : periodeName;
    obj.libelleReference = periodeName;
    obj.currency = devise;
    obj.isRecidiviste = isRecidiviste == 1 ? true : false;
    obj.monthLate = parseInt(monthLate);
    obj.case = 2;

    initUIPenality(obj);
}


function callSaveFichePriseEnCharge(medID, amountPrincipal, periodeID, devise, isRecidiviste, monthLate, amountPenalite, typeFpc) {

    if (penalitieList.length > 0) {

        alertify.confirm('Etes-vous sûre de vouloir créer la fiche de prise en charge ?', function () {
            fromCall = '1';
            periodeIDSelected = periodeID;

            penalitieList = validateFPC();

            $.ajax({
                type: 'POST',
                url: 'poursuites_servlet',
                dataType: 'Text',
                headers: {
                    'Access-Control-Allow-Origin': '*'
                },
                crossDomain: false,
                data: {
                    'principalDu': amountPrincipal,
                    'penaliteDu': amountPenalite,
                    'caseFpc': typeFpc,
                    'deviseFpc': devise,
                    'reference': periodeID,
                    'isRecidiviste': isRecidiviste,
                    'monthLate': monthLate,
                    'penalitieList': JSON.stringify(penalitieList),
                    'medID': medID,
                    'operation': 'saveFpc'

                },
                beforeSend: function () {
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Création de la fiche en cours ...</h5>'});

                },
                success: function (response)
                {

                    $.unblockUI();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {

                        alertify.alert('La création de la fiche de prise en charge n°' + '<span style = "font-weight:bold">' + response + '</span>' + ' s\'est effectuée avec succès.');
                        loadMiseEnDemeure(isAdvanced);
                    }


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $.unblockUI();
                    showResponseError();
                }

            });
        });

    } else {

        alertify.alert('Veuillez d\'abord définir la pénalité à appliquer pour cette mise en demeure');

    }
}

function validateFPC() {

    switch (fromCall) {
        case '0':
            alertify.confirm('Etes-vous sûre de vouloir valider prendre en compte ces pénalités ?', function () {

                penalitieList = getPenalitiesReference(periodeIDSelected);

                var idColPenalite = $("#lblPenalite_" + medIDSelected);
                idColPenalite.html(formatNumber(getSumByReference(periodeIDSelected), deviseSelected));

                modalFichePriseEnCharge.modal('hide');
            });
            break;
        case '1':
            penalitieList = getPenalitiesReference(periodeIDSelected);
            break;
    }


    return penalitieList;
}

function getMiseEnDemeure(isAdvanced) {

    $.ajax({
        type: 'POST',
        url: 'poursuites_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'codeService': userData.serviceCode,
            'codeTypeMiseEnDemeure': 2,
            'codeStateMiseEnDemeure': 3,
            'valueSearch': 1,
            'advancedSearch': 2,
            'typeSearch': 1,
            'operation': 'loadMiseEnDemeure'

        },
        beforeSend: function () {
            switch (isAdvanced) {
                case '0':
                    $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
                case '1':
                    modalRechercheAvanceeMed.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
                    break;
            }

        },
        success: function (response)
        {

            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeMed.unblock();
                    break;
            }

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {

                if (isAdvanced === '1') {
                    modalRechercheAvanceeMed.modal('hide');
                }

                switch (isAdvanced) {
                    case '0':
                        switch (selectTypeSearchID) {
                            case '0':
                                printMiseEnDemeure('');
                                alertify.alert('Cet assujetti : ' + valueTypeSearch.val() + ' n\'a pas de mise en demeure');
                                break;
                            case '1':
                                printMiseEnDemeure('');
                                alertify.alert('Ce numéro : ' + valueTypeSearch.val() + ' ne pas une référence valide d\'une mise en demeure');
                                break;
                        }
                        break;
                    case '1':
                        alertify.alert('Aucune mise en demeure ne correspond au critère de recherche fournis');
                        break;
                }
            } else {

                miseEnDemeureList = JSON.parse(JSON.stringify(response));

                if (isAdvanced === '1') {
                    modalRechercheAvanceeMed.modal('hide');
                }

                printMiseEnDemeure(miseEnDemeureList);
            }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            switch (isAdvanced) {
                case '0':
                    $.unblockUI();
                    break;
                case '1':
                    modalRechercheAvanceeMed.unblock();
                    break;
            }
            showResponseError();
        }

    });
}

function getMED() {

    var date = new Date();
    var day = dateFormat(date.getDate());
    var month = dateFormat(date.getMonth() + 1);
    var year = date.getFullYear();

    var dateDuJour = day + "-" + month + "-" + year;

    dateDebuts = dateDuJour;
    dateFins = dateDuJour;

    getMiseEnDemeure(1);

}