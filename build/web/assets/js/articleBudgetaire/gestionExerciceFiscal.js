/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var dateDebut, dateCloture, registreExerciceFiscal, inputTitre, annee, code_ef, id_code;
var btnEnregistrer;
var id_maj = '0';

$(function () {

    mainNavigationLabel.text('GESTION DES ACTES GENERATEURS');
    secondNavigationLabel.text('Exercice fiscal');

    removeActiveMenu();
    linkMenuGestionArticleBudgetaire.addClass('active');

    btnEnregistrer = $('#btnEnregistrer');
    dateDebut = $('#dateDebut');
    dateCloture = $('#dateCloture');
    inputTitre = $('#inputTitre');
    registreExerciceFiscal = $('#registreExerciceFiscal');
    btnEnregistrer = $('#btnEnregistrer');
    code_ef = $('#code');

    loadExerciceFiscal('loadExerciceFiscal', 'Chargement en cours ...');

    btnEnregistrer.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (inputTitre.val() == '') {
            alertify.alert('Veuillez saisir l\'intitulé de cet exercice fiscal avant de continuer.');
            return;
        }

        if (dateDebut.val() == '') {
            alertify.alert('Veuillez sélectionner une date pour cet exercice fiscal avant de continuer.');
            return;
        }

        var code = dateDebut.val().substr(6, 4);

        if (id_maj == '0') {
            alertify.confirm('Etes-vous sûr de vouloir enregistrer cet exercice fiscal ?', function () {

                loadExerciceFiscal('addExerciceFiscal', 'Enregistrement en cours ...');

            });

        } else {
            if (code == id_code) {
                annee = code_ef.val();
                alertify.confirm('Etes-vous sûr de vouloir modifier cet exercice fiscal ?', function () {

                    loadExerciceFiscal('updateExerciceFiscal', 'Modification en cours ...');

                });

            } else {
                alertify.alert('Impossible de mettre à jour cet exercice fiscal. Vérifier la cohérence entre le code et le début.');
            }
        }
    });

    $.fn.datepicker.dates['en'] = {
        days: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"],
        daysShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
        daysMin: ["D", "L", "Ma", "Me", "J", "V", "S", "D"],
        months: ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"],
        monthsShort: ["Jan", "Fev", "Mar", "Avr", "Mai", "Jui", "Jul", "Aou", "Sep", "Oct", "Nov", "Dec"],
        today: "Aujourd'hui",
        suffix: [],
        meridiem: ["am", "pm"],
        weekStart: 1,
        format: "dd-mm-yyyy"
    };

    $('#datePicker2').datepicker("setDate", new Date());
    $('#datePicker2').datepicker("setDate", new Date());
    dateDebut.datepicker("setDate", new Date());

    var lastDate = getCloseDateByYear(dateDebut.val());
    dateCloture.val(lastDate);

    dateDebut.on('change', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var lastDate = getCloseDateByYear(dateDebut.val());
        dateCloture.val(lastDate);
    });

});

function loadExerciceFiscal(operation, message_excepting) {

    $.ajax({
        type: 'POST',
        url: 'articleBudgetaire_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': operation,
            'userId': userData.idUser,
            'annee': annee,
            'intitule': inputTitre.val(),
            'dateDebut': dateDebut.val(),
            'dateFin': dateCloture.val()
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>' + message_excepting + '</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            if (operation == 'updateExerciceFiscal') {
                if (response == '1') {
                    alertify.alert('L\'exercice fiscal ' + inputTitre.val() + ' vient d\'être mis à jour avec succès.');
                } else {
                    alertify.alert('La modification de l\'exercice fiscal ' + inputTitre.val() + ' a échoué. Veuillez réessayer.');
                }
                loadExerciceFiscal('loadExerciceFiscal');
            } else if (operation == 'addExerciceFiscal') {
                if (response == '1') {
                    alertify.alert('L\'exercice fiscal ' + inputTitre.val() + ' vient d\'être ajouté avec succès.');
                } else {
                    alertify.alert('L\'ajout de l\'exercice fiscal ' + inputTitre.val() + ' a échoué. Veuillez réessayer.');
                }
                loadExerciceFiscal('loadExerciceFiscal');
            } else if (operation == 'deleteExerciceFiscal') {
                if (response == '1') {
                    alertify.alert('L\'exercice fiscal ' + inputTitre.val() + ' vient d\'être supprimé avec succès.');
                } else {
                    alertify.alert('La suppression de l\'exercice fiscal ' + inputTitre.val() + ' a échoué. Veuillez réessayer.');
                }
                loadExerciceFiscal('loadExerciceFiscal');
            } else if (operation == 'updateExerciceFiscal') {
                loadExerciceFiscal('loadExerciceFiscal');
            }

            printExerciceFiscal(response);
            $('#div_code').attr('style', 'display:none');

        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printExerciceFiscal(efsList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> ANNEE </th>';
    header += '<th scope="col"> INTITULE </th>';
    header += '<th scope="col"> DEBUT </th>';
    header += '<th scope="col"> FIN </th>';
    header += '<th scope="col"> AGENT CREATION </th>';
    header += '<th scope="col"> DATE CREATION </th>';
    header += '<th scope="col"> AGENT MAJ </th>';
    header += '<th scope="col"> DATE MAJ </th>';
    header += '<th scope="col"></th>';
    header += '<th scope="col"></th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < efsList.length; i++) {
        body += '<tr>';
        body += '<td>' + efsList[i].code + '</td>';
        body += '<td>' + efsList[i].intitule + '</td>';
        body += '<td>' + efsList[i].debut + '</td>';
        body += '<td>' + efsList[i].fin + '</td>';
        body += '<td>' + efsList[i].agent_create.toUpperCase() + '</td>';
        body += '<td>' + efsList[i].date_create.toUpperCase() + '</td>';
        body += '<td>' + efsList[i].agent_maj.toUpperCase() + '</td>';
        body += '<td>' + efsList[i].date_maj + '</td>';
        body += '<td><button type="button" onclick="deleteExerciceFiscal(\'' + efsList[i].code + '\',\'' + i + '\')" class="btn btn-danger" title="Suppression"><i class="fa fa-trash"></i></button></td>';
        body += '<td><button type="button" onclick="updateExerciceFiscal(\'' + efsList[i].code + '\',\'' + efsList[i].intitule + '\',\'' + efsList[i].debut + '\',\'' + i + '\')" class="btn btn-success" title="Modification"><i class="fa fa-edit"></i></button></td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    registreExerciceFiscal.html(tableContent);

    registreExerciceFiscal.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function getCloseDateByYear(selectDateText) {

    annee = selectDateText.substr(6, 4);

    return '31-12-' + annee + '';
}

function deleteExerciceFiscal(code, i) {
    annee = code;
    alertify.confirm('Etes-vous sûr de vouloir supprimer cet exercice fiscal ?', function () {

        loadExerciceFiscal('deleteExerciceFiscal');

    });

}

function updateExerciceFiscal(code, intitule, debut, i) {
    id_code = code;
    $('#div_code').attr('style', 'display:block');
    code_ef.val(code);
    inputTitre.val(intitule);
    dateDebut.val(debut);

    var lastDate = getCloseDateByYear(dateDebut.val());
    dateCloture.val(lastDate);

    id_maj = '1';
}
