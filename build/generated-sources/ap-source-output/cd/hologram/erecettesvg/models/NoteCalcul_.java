package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.BonAPayer;
import cd.hologram.erecettesvg.models.DepotDeclaration;
import cd.hologram.erecettesvg.models.DetailsNc;
import cd.hologram.erecettesvg.models.NotePerception;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(NoteCalcul.class)
public class NoteCalcul_ { 

    public static volatile SingularAttribute<NoteCalcul, String> observationOrdonnancement;
    public static volatile SingularAttribute<NoteCalcul, String> numero;
    public static volatile SingularAttribute<NoteCalcul, Date> dateCreat;
    public static volatile SingularAttribute<NoteCalcul, DepotDeclaration> depotDeclaration;
    public static volatile SingularAttribute<NoteCalcul, String> observation;
    public static volatile SingularAttribute<NoteCalcul, String> personne;
    public static volatile SingularAttribute<NoteCalcul, String> agentCloture;
    public static volatile SingularAttribute<NoteCalcul, Short> etat;
    public static volatile SingularAttribute<NoteCalcul, Date> dateCloture;
    public static volatile SingularAttribute<NoteCalcul, String> site;
    public static volatile SingularAttribute<NoteCalcul, String> avis;
    public static volatile ListAttribute<NoteCalcul, DetailsNc> detailsNcList;
    public static volatile SingularAttribute<NoteCalcul, String> service;
    public static volatile ListAttribute<NoteCalcul, BonAPayer> bonAPayerList;
    public static volatile SingularAttribute<NoteCalcul, String> exercice;
    public static volatile SingularAttribute<NoteCalcul, Boolean> estTaxationOffice;
    public static volatile SingularAttribute<NoteCalcul, String> fkAdressePersonne;
    public static volatile SingularAttribute<NoteCalcul, String> agentCreat;
    public static volatile SingularAttribute<NoteCalcul, Date> dateValidation;
    public static volatile SingularAttribute<NoteCalcul, String> agentValidation;
    public static volatile SingularAttribute<NoteCalcul, String> bien;
    public static volatile ListAttribute<NoteCalcul, NotePerception> notePerceptionList;

}