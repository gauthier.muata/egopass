/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.AcquitLiberatoireBusiness;
import cd.hologram.erecettesvg.business.AssujettissementBusiness;
import cd.hologram.erecettesvg.business.DeclarationBusiness;
import cd.hologram.erecettesvg.business.GeneralBusiness;
import cd.hologram.erecettesvg.business.IdentificationBusiness;
import cd.hologram.erecettesvg.business.NotePerceptionBusiness;
import static cd.hologram.erecettesvg.business.NotePerceptionBusiness.getArchiveByRefDocument;
import cd.hologram.erecettesvg.business.PaiementBusiness;
import cd.hologram.erecettesvg.business.PoursuiteBusiness;
import cd.hologram.erecettesvg.business.RecouvrementBusiness;
import cd.hologram.erecettesvg.business.TaxationBusiness;
import cd.hologram.erecettesvg.constants.AssujettissementConst;
import cd.hologram.erecettesvg.constants.DeclarationConst;
import cd.hologram.erecettesvg.constants.DocumentConst;
import cd.hologram.erecettesvg.constants.FractionnementConst;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.RecouvrementConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.constants.NotePerceptionConst;
import cd.hologram.erecettesvg.constants.TaxationConst;
import cd.hologram.erecettesvg.models.Acquisition;
import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.Archive;
import cd.hologram.erecettesvg.models.ArchiveAccuseReception;
import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.Avis;
import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.ComplementBien;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.DepotDeclaration;
import cd.hologram.erecettesvg.models.DetailBordereau;
import cd.hologram.erecettesvg.models.DetailDepotDeclaration;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.IcmParam;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.Med;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.ParamTauxVignette;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.RetraitDeclaration;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.Tarif;
import cd.hologram.erecettesvg.models.TypeBienService;
import cd.hologram.erecettesvg.models.TypeDocument;
import cd.hologram.erecettesvg.models.UsageBien;
import cd.hologram.erecettesvg.models.ValeurPredefinie;
import cd.hologram.erecettesvg.pojo.LogUser;
import cd.hologram.erecettesvg.pojo.NoteCalculData;
import cd.hologram.erecettesvg.pojo.RecepissePrint;
import cd.hologram.erecettesvg.properties.PropertiesConfig;
import cd.hologram.erecettesvg.util.Compare;
import cd.hologram.erecettesvg.util.ConvertDate;
import cd.hologram.erecettesvg.util.CustumException;
import cd.hologram.erecettesvg.util.PrintDocument;
import cd.hologram.erecettesvg.util.Property;
import cd.hologram.erecettesvg.util.Tools;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author WILLY KASHALA
 */
@WebServlet(name = "Declaration", urlPatterns = {"/declaration_servlet"})
public class Declaration extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    //Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE);
    //static PropertiesConfig propertiesConfig;
    Properties propertiesMessage = Property.getProperties(Property.FileData.FR_MESSAGE),
            propertiesConfig = Property.getProperties(Property.FileData.APP_CONFIG);

    JsonObject jsonObject;
    JsonObject detailBordJson;
    JsonObject detailDepotDeclJson;
    LogUser logUser;
    List<Personne> listPersonneAssujetti = new ArrayList<>();
    List<ArticleBudgetaire> listArticleBudgetaire = new ArrayList<>();
    List<Tarif> listTarifs = new ArrayList<>();
    List<DepotDeclaration> listDepotDeclaration;
    List<RetraitDeclaration> listRetraitDeclaration;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, Exception {

        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        //operation = "researchArticleBudgetaireAssujettissable";
        switch (operation) {
            case DeclarationConst.Operation.SEARCH_DOCUMENT_DEPOT_DECLARATION:
                result = loadInfoDocument(request);
                break;
            case DeclarationConst.Operation.SAVE_DEPOT_DECLARATION:
                result = saveDepotDeclaration(request);
                break;
            case DeclarationConst.Operation.SEARCH_DEPOT_DECLARATION:
                result = loadDepotdeclaration(request);
                break;
            case DeclarationConst.Operation.SEARCH_AVANCED_DEPOT_DECLARATION:
                result = loadDepotDeclarationBySearchAvanced(request);
                break;
            case DeclarationConst.Operation.SEARCH_RETRAIT_DECLARATION:
                result = loadRetraitDeclaration(request);
                break;

            case "searchRetraitDeclarationDefaillant":
                result = loadRetraitDeclarationDefaillant(request);
                break;

            case DeclarationConst.Operation.SEARCH_AVANCED_RETRAIT_DECLARATION:
                result = loadRetraitDeclarationBySearchAvanced(request);
                break;

            case "SearchAvancedRetraitDeclarationDefaillant":
                result = loadRetraitDeclarationDefaillantBySearchAvanced(request);
                break;

            case DeclarationConst.Operation.PRINT_RECEPISSE:
                result = printRecepisse(request);
                break;
            case "printNoteTaxation":
                result = printNoteTaxation(request);
                break;
            case DeclarationConst.Operation.SEARCH_ARTICLE_BUDGETAIRE:
                result = loadArticleBudgetaire(request);
                break;
            case DeclarationConst.Operation.SEARCH_DEPOT_OF_ARTICLE_BUDGETAIRE:
                result = getDepotByArticleBudgetaire(request);
                break;
            case DeclarationConst.Operation.SEARCH_ARTICLE_BUDGETAIRE_ASSUJETTISSABLE:
                result = loadArticlesBudgetairesAssujettissable(request);
                break;
            case DeclarationConst.Operation.REPRINT_RECEPISSE:
                result = reprintRecepisse(request);
                break;
            case "saveInvitationApayer":
                result = saveAndPrintInvitationApayer(request);
                break;
            case "controleTaxation":
                result = controleTaxation(request);
                break;
            case "getAvis":
                result = loadAvis();
                break;

        }
        out.print(result);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Declaration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(Declaration.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    public String loadInfoDocument(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String typeDocument = request.getParameter(DeclarationConst.ParamName.TYPE_DOCUMENT);
            String numeroDocument = request.getParameter(DeclarationConst.ParamName.NUMERO_DOCUMENT);

            DepotDeclaration depotDecl = DeclarationBusiness.checkDepotDeclaration(numeroDocument, typeDocument);

            if (depotDecl != null) {

                dataReturn = GeneralConst.ResultCode.DECLARATION_EXIST;
            } else {

                jsonObject = new JsonObject();

                Personne person;

                Bordereau bordereau = DeclarationBusiness.getBordereau(numeroDocument);

                if (bordereau != null) {

                    RetraitDeclaration retraitDecl = DeclarationBusiness.getRetraiteclarationByNumero(numeroDocument);

                    String periodeName = "";

                    PeriodeDeclaration p;
                    ArticleBudgetaire ab;

                    if (retraitDecl != null) {

                        if (retraitDecl.getNewId() != null) {

                            List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID_V2(retraitDecl.getNewId());

                            if (!retraitDeclarations.isEmpty()) {

                                for (RetraitDeclaration rd : retraitDeclarations) {

                                    p = new PeriodeDeclaration();
                                    ab = new ArticleBudgetaire();

                                    p = RecouvrementBusiness.getPeriodeDeclarationByIdV2(Integer.valueOf(rd.getFkPeriode()));
                                    ab = DeclarationBusiness.getArticleBudgetaireByCodeV2(rd.getFkAb());

                                    if (periodeName.isEmpty()) {

                                        periodeName = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                    } else {
                                        periodeName += periodeName = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                    }
                                }
                            }

                        } else {

                            p = new PeriodeDeclaration();
                            ab = new ArticleBudgetaire();

                            p = RecouvrementBusiness.getPeriodeDeclarationByIdV2(Integer.valueOf(retraitDecl.getFkPeriode()));
                            ab = DeclarationBusiness.getArticleBudgetaireByCodeV2(retraitDecl.getFkAb());

                            periodeName = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                        }

                        person = DeclarationBusiness.getPersonneByCode(retraitDecl.getFkAssujetti().trim());

                        if (person != null) {
                            jsonObject.addProperty(DeclarationConst.ParamName.FORME_JURIDIQUE,
                                    person.getFormeJuridique().getIntitule());
                            jsonObject.addProperty(DeclarationConst.ParamName.CODE_FORME_JURIDIQUE,
                                    person.getFormeJuridique().getCode());
                            jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                    person.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                            jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                    person.toString().toUpperCase());
                            jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                    person.getAdressePersonneList().get(0).getCode().trim());
                            jsonObject.addProperty(DeclarationConst.ParamName.CODE_PERSONNE,
                                    person.getCode().trim());
                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.FORME_JURIDIQUE,
                                    GeneralConst.EMPTY_STRING);
                            jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                    GeneralConst.EMPTY_STRING);
                            jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                    GeneralConst.EMPTY_STRING);
                            jsonObject.addProperty(DeclarationConst.ParamName.CODE_FORME_JURIDIQUE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_BORDEREAU,
                                bordereau.getCode());

                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_BORDEREAU,
                                bordereau.getCode());

                        jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_BORDEREAU,
                                bordereau.getNumeroBordereau());

                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_PAIEMENT,
                                Tools.formatDateToString(bordereau.getDatePaiement()));
                        jsonObject.addProperty(DeclarationConst.ParamName.TOTAL_MONTANT_PERCU,
                                bordereau.getTotalMontantPercu());
                        jsonObject.addProperty(DeclarationConst.ParamName.COMPTE_BANCAIRE,
                                bordereau.getCompteBancaire().getCode());
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_BANQUE,
                                bordereau.getCompteBancaire().getBanque().getIntitule());
                        jsonObject.addProperty(DeclarationConst.ParamName.AGENT_CREATE,
                                bordereau.getAgent().toString());
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_MISE_A_JOUR,
                                Tools.formatDateToString(bordereau.getDateMaj()));
                        jsonObject.addProperty(DeclarationConst.ParamName.TOTAL_MONTANT_PERCU_TO_STRING,
                                bordereau.getTotalMontantPercuToString() != null
                                        ? bordereau.getTotalMontantPercuToString() : GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DECLARATION,
                                bordereau.getNumeroDeclaration() != null
                                        ? bordereau.getNumeroDeclaration() : GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_ATTESTATION_PAIEMENT,
                                bordereau.getNumeroAttestationPaiement() != null
                                        ? bordereau.getNumeroAttestationPaiement() : GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.IMPRESSION_RECU,
                                bordereau.getImpressionRecu() != null
                                        ? String.valueOf(bordereau.getImpressionRecu()) : GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CENTRE_BORDEREAU,
                                bordereau.getCentre() != null
                                        ? bordereau.getCentre() : GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.TYPE_DECLARATION, GeneralConst.EMPTY_STRING);

                        if (bordereau.getDetailBordereauList().size() > 0) {

                            List<JsonObject> listDetailBorJson = new ArrayList<>();
                            ArticleBudgetaire article;
                            PeriodeDeclaration periodeDeclarat;

                            for (DetailBordereau dtlBor : bordereau.getDetailBordereauList()) {

                                detailBordJson = new JsonObject();

                                detailBordJson.addProperty(DeclarationConst.ParamName.CODE_DETAIL_BORDEREAU,
                                        dtlBor.getBordereau().getCode() != null
                                                ? String.valueOf(dtlBor.getCode()) : GeneralConst.EMPTY_STRING);

                                detailBordJson.addProperty(DeclarationConst.ParamName.CODE_BORDEREAU,
                                        dtlBor.getBordereau().getCode() != null ? String.valueOf(dtlBor.getBordereau().getCode())
                                                : GeneralConst.EMPTY_STRING);
                                detailBordJson.addProperty(DeclarationConst.ParamName.NUMERO_BORDEREAU,
                                        dtlBor.getBordereau().getNumeroBordereau() != null ? String.valueOf(dtlBor.getBordereau().getNumeroBordereau())
                                                : GeneralConst.EMPTY_STRING);
                                detailBordJson.addProperty(DeclarationConst.ParamName.DEVISE,
                                        dtlBor.getDevise() != null
                                                ? dtlBor.getDevise() : GeneralConst.EMPTY_STRING);
                                detailBordJson.addProperty(DeclarationConst.ParamName.COMPTE_BANCAIRE,
                                        bordereau.getCompteBancaire() != null
                                                ? bordereau.getCompteBancaire().getCode() : GeneralConst.EMPTY_STRING);
                                detailBordJson.addProperty(DeclarationConst.ParamName.NUMERO_BORDEREAU,
                                        bordereau.getNumeroBordereau());
                                detailBordJson.addProperty(DeclarationConst.ParamName.DATE_PAIEMENT,
                                        Tools.formatDateToString(bordereau.getDatePaiement()));

                                periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(dtlBor.getPeriodicite().trim());

                                if (periodeDeclarat != null) {

                                    if (periodeDeclarat.getDebut() != null) {

                                        /*detailBordJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                         Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                                         periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim()));*/
                                        detailBordJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE, periodeName);
                                    } else {
                                        detailBordJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                                GeneralConst.EMPTY_STRING);
                                    }
                                } else {
                                    detailBordJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                            GeneralConst.EMPTY_STRING);
                                }

                                detailBordJson.addProperty(DeclarationConst.ParamName.PERIODICITE,
                                        dtlBor.getPeriodicite() != null ? String.valueOf(dtlBor.getPeriodicite())
                                                : GeneralConst.EMPTY_STRING);

                                if (dtlBor.getPeriodicite() != null) {

                                    Assujeti assujeti = DeclarationBusiness.getAssujettissementByPeriode(dtlBor.getPeriodicite());

                                    if (assujeti != null) {

                                        int _type = 0;

                                        TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(
                                                assujeti.getBien().getTypeBien().getCode());

                                        _type = typeBienService.getType();

                                        detailBordJson.addProperty("type", _type);

                                        if (assujeti.getBien().getFkCommune() != null) {

                                            EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(assujeti.getBien().getFkCommune());

                                            String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                                            detailBordJson.addProperty("communeCode", ea.getCode());
                                            detailBordJson.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                                        } else {
                                            detailBordJson.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                                            detailBordJson.addProperty("communeName", GeneralConst.EMPTY_STRING);
                                        }

                                        if (assujeti.getBien().getFkQuartier() != null) {

                                            EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(
                                                    assujeti.getBien().getFkQuartier());

                                            detailBordJson.addProperty("quartierCode", eaQuartier.getCode());
                                            detailBordJson.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                                        } else {
                                            detailBordJson.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                                            detailBordJson.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                                        }

                                        if (assujeti.getBien().getFkTarif() != null) {

                                            Tarif tarif = TaxationBusiness.getTarifByCode(assujeti.getBien().getFkTarif());

                                            detailBordJson.addProperty("tarifCode", tarif.getCode());
                                            detailBordJson.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                                        } else {
                                            detailBordJson.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                                            detailBordJson.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                                        }

                                        if (assujeti.getBien().getFkUsageBien() != null) {

                                            UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(assujeti.getBien().getFkUsageBien());

                                            detailBordJson.addProperty("usageCode", usageBien.getId());
                                            detailBordJson.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                                            detailBordJson.addProperty("isImmobilier", GeneralConst.Number.ONE);

                                        } else {

                                            detailBordJson.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                                            detailBordJson.addProperty("usageName", GeneralConst.EMPTY_STRING);
                                            detailBordJson.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                                        }

                                        detailBordJson.addProperty("libelleTypeBien", assujeti.getBien().getTypeBien().getIntitule().toUpperCase());

                                        //----------------------------------------------------------------------------
                                        detailBordJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                                assujeti.getBien().getIntitule().toUpperCase());

                                        if (assujeti.getBien().getFkAdressePersonne() != null) {
                                            detailBordJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                    assujeti.getBien().getFkAdressePersonne().getAdresse().toString());
                                        } else {
                                            detailBordJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                    GeneralConst.EMPTY_STRING);
                                        }

                                    } else {
                                        detailBordJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                                GeneralConst.EMPTY_STRING);
                                        detailBordJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                GeneralConst.EMPTY_STRING);
                                    }
                                } else {
                                    detailBordJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                            GeneralConst.EMPTY_STRING);
                                    detailBordJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                            GeneralConst.EMPTY_STRING);
                                }

                                detailBordJson.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                        dtlBor.getMontantPercu() != null ? String.valueOf(dtlBor.getMontantPercu())
                                                : GeneralConst.EMPTY_STRING);

                                jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                                        dtlBor.getDevise() != null
                                                ? dtlBor.getDevise() : GeneralConst.EMPTY_STRING);

                                article = DeclarationBusiness.getArticleBudgetaireByCode(dtlBor.getArticleBudgetaire().getCode().trim());

                                if (article != null) {
                                    detailBordJson.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                            article.getCode());
                                    detailBordJson.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                            article.getIntitule());
                                    detailBordJson.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                            article.getCodeOfficiel());
                                } else {
                                    detailBordJson.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                            GeneralConst.EMPTY_STRING);
                                    detailBordJson.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                            GeneralConst.EMPTY_STRING);
                                }

                                listDetailBorJson.add(detailBordJson);

                            }
                            jsonObject.addProperty(DeclarationConst.ParamName.LIST_DETAIL_BORDEREAU, listDetailBorJson.toString());
                        }

                        dataReturn = jsonObject.toString();

                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }

                } else {
                    dataReturn = GeneralConst.ResultCode.PAIEMENT_NON_EXIST;
                }
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveDepotDeclaration(HttpServletRequest request) throws JSONException, Exception {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String userId = request.getParameter(DeclarationConst.ParamName.USER_ID);
        String observation = request.getParameter(DeclarationConst.ParamName.OBSERVATION);
        String codeAssujetti = request.getParameter(DeclarationConst.ParamName.CODE_PERSONNE);
        String numeroBordereau = request.getParameter(DeclarationConst.ParamName.NUMERO_BORDEREAU);
        String listDetailsBordereau = request.getParameter(DeclarationConst.ParamName.LIST_DETAIL_BORDEREAU);
        String codeFormeJuridique = request.getParameter(DeclarationConst.ParamName.CODE_FORME_JURIDIQUE);
        String codeAdressePersonne = request.getParameter(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE);
        String numeroDeclaration = request.getParameter(DeclarationConst.ParamName.NUMERO_DECLARATION);
        String service = request.getParameter(DeclarationConst.ParamName.SERVICE);
        String site = request.getParameter(DeclarationConst.ParamName.SITE);
        String totalMontantPercu = request.getParameter(DeclarationConst.ParamName.TOTAL_MONTANT_PERCU);
        String archives = request.getParameter(DeclarationConst.ParamName.ARCHIVES);

        try {

            DepotDeclaration depotDecl = DeclarationBusiness.getDepotDeclarationByNumDepot(numeroDeclaration.trim());

            if (depotDecl != null) {

                dataReturn = GeneralConst.ResultCode.DECLARATION_EXIST;

            } else {

                DepotDeclaration depotDeclar = new DepotDeclaration();
                Bordereau bord = DeclarationBusiness.getBordereau(numeroBordereau);
                logUser = new LogUser(request, DeclarationConst.Operation.SAVE_DEPOT_DECLARATION);

                BigDecimal amountTotalPercu = new BigDecimal("0");
                amountTotalPercu = amountTotalPercu.add(BigDecimal.valueOf(Double.valueOf(totalMontantPercu)));

                depotDeclar.setAgent(new Agent(Integer.valueOf(userId)));
                depotDeclar.setNumeroDepot(numeroDeclaration);
                depotDeclar.setPersonne(new Personne(codeAssujetti));
                depotDeclar.setBordereau(BigInteger.valueOf(bord.getCode()));
                depotDeclar.setService(new Service(service));

                //logUser = new LogUser(request, DeclarationConst.Operation.SAVE_DEPOT_DECLARATION);
                JSONArray jsonArray = new JSONArray(listDetailsBordereau);

                List<DetailDepotDeclaration> listDetailsDepotDecl = new ArrayList<>();
                List<NoteCalculData> listNcData = new ArrayList<>();
                List<String> ListArchiveDocument = new ArrayList<>();

                JSONArray jsonDocumentArray;

                for (int i = GeneralConst.Numeric.ZERO; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    DetailDepotDeclaration detaildepotDecl = new DetailDepotDeclaration();
                    NoteCalculData nc = new NoteCalculData();
                    PeriodeDeclaration periodeDeclarat;
                    Palier palier;
                    BigDecimal tauxATCom = new BigDecimal("0");
                    String exercice;

                    detaildepotDecl.setArticleBudgetaire(new ArticleBudgetaire(jsonObject.getString(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE)));

                    nc.setCodePersonne(codeAssujetti);
                    nc.setAgentCreate(userId);
                    nc.setCodeService(service);
                    nc.setCodeAdresse(codeAdressePersonne);
                    nc.setNumeroDepotDeclaration(numeroDeclaration);

                    periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(jsonObject.getString(DeclarationConst.ParamName.PERIODICITE));

                    if (periodeDeclarat != null) {

                        if (periodeDeclarat.getDebut() != null) {
                            exercice = Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                                    periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim());
                        } else {
                            exercice = GeneralConst.EMPTY_STRING;
                        }
                    } else {
                        exercice = GeneralConst.EMPTY_STRING;
                    }

                    nc.setExerciceFiscal(exercice);
                    nc.setCodeSite(site);
                    nc.setCodeArticleBudgetaire(jsonObject.getString(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE));
                    BigDecimal amountDu = BigDecimal.valueOf(Double.valueOf(jsonObject.getString(DeclarationConst.ParamName.MONTANT_PERCU)));
                    nc.setMontantDu(amountDu);
                    nc.setPenaliser(Short.valueOf(GeneralConst.Number.ZERO));
                    nc.setPeriodeDeclaration(Integer.valueOf(jsonObject.getString(DeclarationConst.ParamName.PERIODICITE)));
                    nc.setDevise(jsonObject.getString(DeclarationConst.ParamName.DEVISE));
                    nc.setValeurBase(BigDecimal.ONE);
                    nc.setCompteBancaire(jsonObject.getString(DeclarationConst.ParamName.COMPTE_BANCAIRE));
                    nc.setNumeroBordereau(jsonObject.getString(DeclarationConst.ParamName.NUMERO_BORDEREAU));
                    nc.setDatePaiementBordereau(jsonObject.getString(DeclarationConst.ParamName.DATE_PAIEMENT));

                    Assujeti assujeti = DeclarationBusiness.getAssujettissementByPeriode(
                            jsonObject.getString(DeclarationConst.ParamName.PERIODICITE));

                    if (assujeti != null) {

                        switch (assujeti.getArticleBudgetaire().getCode()) {

                            case "00000000000002312020": // ICM
                            case "00000000000002312021": // ICM

                                IcmParam icmParam = AssujettissementBusiness.getIcmParamByID(Integer.valueOf(periodeDeclarat.getObservation()));

                                if (icmParam != null) {

                                    nc.setTauxArticleBudgetaire(icmParam.getTaux());
                                    nc.setTypeTaux("F");

                                } else {
                                    nc.setTauxArticleBudgetaire(null);
                                    nc.setTypeTaux(null);
                                }

                                break;

                            default:

                                boolean isPalier = assujeti.getArticleBudgetaire().getPalier();

                                palier = AssujettissementBusiness.getPalierForBienImmobilier(
                                        assujeti.getArticleBudgetaire().getCode().trim(),
                                        assujeti.getBien().getFkTarif(),
                                        assujeti.getPersonne().getFormeJuridique().getCode(),
                                        assujeti.getBien().getFkQuartier(),
                                        assujeti.getValeur().floatValue(), isPalier,
                                        assujeti.getBien().getTypeBien().getCode());

                                if (palier != null) {

                                    if (assujeti.getBien().getFkUsageBien() > 1 && (assujeti.getBien().getTypeBien().getCode().equals("TB00000027")
                                            || assujeti.getBien().getTypeBien().getCode().equals("TB00000029"))
                                            && assujeti.getArticleBudgetaire().getCode().equals(propertiesConfig.getProperty("CODE_IMPOT_FONCIER"))) {

                                        tauxATCom = BigDecimal.valueOf(Double.valueOf(
                                                Tools.getTauxPalierATCommercial(assujeti.getPersonne().getFormeJuridique().getCode(),
                                                        assujeti.getBien().getFkTarif())));

                                        palier.setTaux(tauxATCom);
                                    }

                                    nc.setTauxArticleBudgetaire(palier.getTaux());
                                    nc.setTypeTaux(palier.getTypeTaux().trim());
                                } else {
                                    nc.setTauxArticleBudgetaire(null);
                                    nc.setTypeTaux(null);
                                }
                                break;

                        }

                        detaildepotDecl.setTarif(new Tarif(assujeti.getTarif().getCode().trim()));
                        BigDecimal amountBase = assujeti.getValeur();
                        detaildepotDecl.setValeurBase(amountBase);
                        nc.setTarif(assujeti.getTarif().getCode().trim());
                        nc.setCodeBien(assujeti.getBien().getId().trim());
                    } else {
                        detaildepotDecl.setTarif(new Tarif(null));
                        detaildepotDecl.setValeurBase(null);
                        nc.setTarif(null);
                        nc.setCodeBien(null);
                    }
                    detaildepotDecl.setDevise(new Devise(jsonObject.getString(DeclarationConst.ParamName.DEVISE)));
                    detaildepotDecl.setTaux(BigDecimal.ZERO);
                    detaildepotDecl.setFkPeriode(jsonObject.getString(DeclarationConst.ParamName.PERIODICITE));

                    listDetailsDepotDecl.add(detaildepotDecl);
                    listNcData.add(nc);
                }

                if (archives != null && !archives.isEmpty()) {

                    jsonDocumentArray = new JSONArray(archives);

                    for (int i = 0; i < jsonDocumentArray.length(); i++) {
                        JSONObject jsonObject = jsonDocumentArray.getJSONObject(i);
                        ListArchiveDocument.add(jsonObject.getString(DeclarationConst.ParamName.ARCHIVES_DOCUMENT));
                    }
                }

                boolean result = DeclarationBusiness.saveDepotDeclaration(logUser,
                        depotDeclar, listDetailsDepotDecl, ListArchiveDocument,
                        observation, numeroDeclaration, listNcData);

                if (result) {

                    dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

                } else {
                    dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                }

            }

        } catch (Exception e) {
            throw e;
        }

        return dataReturn;

    }

    public String loadDepotdeclaration(HttpServletRequest request) {

        String valueSearch, typeSearch, typeRegister, allSite, allService,
                codeSite, codeService, userId = GeneralConst.EMPTY_STRING;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            allSite = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SITE);
            allService = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SERVICE);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();
            Bordereau bordereau;

            listDepotDeclaration = new ArrayList<>();

            listDepotDeclaration = DeclarationBusiness.getListDepotDeclaration(
                    valueSearch.trim(),
                    Integer.valueOf(typeSearch),
                    typeRegister.trim(),
                    false,
                    allService.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                    codeSite.trim(),
                    codeService.trim(),
                    userId);

            if (listDepotDeclaration != null && !listDepotDeclaration.isEmpty()) {

                for (DepotDeclaration depotDecl : listDepotDeclaration) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();

                    RetraitDeclaration retaDeclaration = AssujettissementBusiness.getRetraitDeclarationByCode(depotDecl.getNumeroDepot());

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(retaDeclaration.getFkBanque());

                    jsonObject.addProperty("banqueName2", banque == null ? "" : banque.getIntitule().toUpperCase());

                    CompteBancaire compteBancaire = GeneralBusiness.getCompteBancaireByCode(retaDeclaration.getFkCompteBancaire());

                    jsonObject.addProperty("compteBancaireName2", compteBancaire == null ? "" : compteBancaire.getIntitule().toUpperCase());

                    jsonObject.addProperty("noteTaxation", retaDeclaration == null ? "" : retaDeclaration.getId() + "");
                    jsonObject.addProperty("numeroDeclaration", retaDeclaration == null ? "" : retaDeclaration.getCodeDeclaration().toUpperCase());

                    jsonObject.addProperty(DeclarationConst.ParamName.CODE_DEPOT_DECLARATION,
                            depotDecl.getCode().trim());

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            depotDecl.getNumeroDepot() != null ? depotDecl.getNumeroDepot().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(depotDecl.getDateCreat(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    bordereau = DeclarationBusiness.getBordereauByCode(String.valueOf(depotDecl.getBordereau()));

                    if (bordereau != null) {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                bordereau.getTotalMontantPercu());
                        jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_BORDEREAU,
                                bordereau.getNumeroBordereau());
                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                GeneralConst.Numeric.ZERO);
                        jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_BORDEREAU,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.CODE_BORDEREAU,
                            depotDecl.getBordereau());

                    service = TaxationBusiness.getServiceByCode(depotDecl.getService().getCode().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE,
                            depotDecl.getService().getCode().trim());
                    if (service != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                service.getIntitule().toUpperCase().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.NOMBRE_IMPRESSION,
                            depotDecl.getNbrImpression());

                    jsonObject.addProperty(DeclarationConst.ParamName.ETAT,
                            depotDecl.getEtat());

                    personne = IdentificationBusiness.getPersonneByCode(depotDecl.getPersonne().getCode().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                personne.getLoginWeb() == null ? GeneralConst.EMPTY_STRING
                                        : personne.getLoginWeb().getUsername());

                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(IdentificationConst.ParamName.USER_NAME,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (depotDecl.getDetailDepotDeclarationList().size() > 0) {

                        List<JsonObject> listDetailDepotDeclJson = new ArrayList<>();
                        ArticleBudgetaire article;
                        DetailBordereau detailBordereau;

                        for (DetailDepotDeclaration dtldepotDecl : depotDecl.getDetailDepotDeclarationList()) {

                            detailDepotDeclJson = new JsonObject();

                            article = DeclarationBusiness.getArticleBudgetaireByCode(dtldepotDecl.getArticleBudgetaire().getCode().trim());

                            if (article != null) {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                        article.getCode());
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                        article.getIntitule());
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                        article.getCodeOfficiel());
                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                                    dtldepotDecl.getDepotDeclaration().getNumeroDepot() != null
                                            ? dtldepotDecl.getDepotDeclaration().getNumeroDepot().trim()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.TAUX,
                                    dtldepotDecl.getTaux() != null ? dtldepotDecl.getTaux()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.QUANTITE,
                                    dtldepotDecl.getQuantite() != null ? dtldepotDecl.getQuantite()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                                    dtldepotDecl.getValeurBase() != null ? dtldepotDecl.getValeurBase()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.DEVISE,
                                    dtldepotDecl.getDevise().getCode() != null
                                            ? dtldepotDecl.getDevise().getCode()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ETAT,
                                    dtldepotDecl.getEtat());
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_TARIF,
                                    dtldepotDecl.getTarif().getCode() != null
                                            ? dtldepotDecl.getTarif().getCode()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_TARIF,
                                    dtldepotDecl.getTarif().getIntitule() != null
                                            ? dtldepotDecl.getTarif().getIntitule()
                                            : GeneralConst.EMPTY_STRING);

                            if (dtldepotDecl.getFkPeriode() != null) {

                                PeriodeDeclaration periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(dtldepotDecl.getFkPeriode().trim());
                                Assujeti assujeti = DeclarationBusiness.getAssujettissementByPeriode(dtldepotDecl.getFkPeriode().trim());

                                if (periodeDeclarat != null) {

                                    if (periodeDeclarat.getDebut() != null) {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                                Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                                                        periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim()));
                                    } else {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                                GeneralConst.EMPTY_STRING);
                                    }

                                } else {
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                            GeneralConst.EMPTY_STRING);
                                }

                                if (assujeti != null) {

                                    int _type = 0;

                                    TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(assujeti.getBien().getTypeBien().getCode());

                                    _type = typeBienService.getType();

                                    detailDepotDeclJson.addProperty("type", _type);

                                    if (assujeti.getBien().getFkCommune() != null) {

                                        EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(assujeti.getBien().getFkCommune());

                                        String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                                        detailDepotDeclJson.addProperty("communeCode", ea.getCode());
                                        detailDepotDeclJson.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                                    } else {
                                        detailDepotDeclJson.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("communeName", GeneralConst.EMPTY_STRING);
                                    }

                                    if (assujeti.getBien().getFkQuartier() != null) {

                                        EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(assujeti.getBien().getFkQuartier());

                                        detailDepotDeclJson.addProperty("quartierCode", eaQuartier.getCode());
                                        detailDepotDeclJson.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                                    } else {
                                        detailDepotDeclJson.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                                    }

                                    if (assujeti.getBien().getFkTarif() != null) {

                                        Tarif tarif = TaxationBusiness.getTarifByCode(assujeti.getBien().getFkTarif());

                                        detailDepotDeclJson.addProperty("tarifCode", tarif.getCode());
                                        detailDepotDeclJson.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                                    } else {
                                        detailDepotDeclJson.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                                    }

                                    //infosComplementaire = GeneralConst.EMPTY_STRING;
                                    if (assujeti.getBien().getFkUsageBien() != null) {

                                        UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(assujeti.getBien().getFkUsageBien());

                                        detailDepotDeclJson.addProperty("usageCode", usageBien.getId());
                                        detailDepotDeclJson.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                                        detailDepotDeclJson.addProperty("isImmobilier", GeneralConst.Number.ONE);

                                    } else {
                                        detailDepotDeclJson.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("usageName", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                                    }

                                    detailDepotDeclJson.addProperty("libelleTypeBien", assujeti.getBien().getTypeBien().getIntitule().toUpperCase());

                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                            assujeti.getBien().getIntitule() == null ? "" : assujeti.getBien().getIntitule().toUpperCase());

                                    if (assujeti.getBien().getFkAdressePersonne() != null) {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                assujeti.getBien().getFkAdressePersonne().getAdresse().toString());
                                    } else {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                GeneralConst.EMPTY_STRING);
                                    }

                                } else {
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                            GeneralConst.EMPTY_STRING);
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                            GeneralConst.EMPTY_STRING);
                                }

                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailBordereau = DeclarationBusiness.getDetailBordereauByPeriodicite(dtldepotDecl.getFkPeriode());

                            if (detailBordereau != null) {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                        detailBordereau.getMontantPercu());
                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                        GeneralConst.Numeric.ZERO);
                            }

                            jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                                    dtldepotDecl.getDevise().getCode() != null
                                            ? dtldepotDecl.getDevise().getCode()
                                            : GeneralConst.EMPTY_STRING);

                            listDetailDepotDeclJson.add(detailDepotDeclJson);
                        }

                        jsonObject.addProperty(DeclarationConst.ParamName.LIST_DETAIL_DEPOT_DECLARATION,
                                listDetailDepotDeclJson.toString());
                    }

                    listJsonObjects.add(jsonObject);

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            dataReturn = listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadDepotDeclarationBySearchAvanced(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String codeSite = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SITE_CODE);
            String codeAb = request.getParameter("codeAb");
            String mois = request.getParameter("mois");
            String annee = request.getParameter("annee");
            boolean isMensuel = Boolean.valueOf(request.getParameter("isMensuel"));
            String declarationOnlineOnly = request.getParameter(TaxationConst.ParamName.DECLARATION_ONLINE);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();
            Bordereau bordereau;
            DetailBordereau detailBordereau;

            List<DepotDeclaration> listDepotDeclaration = new ArrayList<>();

            listDepotDeclaration = DeclarationBusiness.getListDepotDeclarationBySearchAvanced(
                    codeSite.trim(), codeAb, mois, annee, isMensuel, declarationOnlineOnly);

            if (!listDepotDeclaration.isEmpty()) {

                for (DepotDeclaration depotDecl : listDepotDeclaration) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();

                    RetraitDeclaration retaDeclaration = AssujettissementBusiness.getRetraitDeclarationByCode(depotDecl.getNumeroDepot());

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(retaDeclaration.getFkBanque());

                    jsonObject.addProperty("banqueName2", banque == null ? "" : banque.getIntitule().toUpperCase());

                    CompteBancaire compteBancaire = GeneralBusiness.getCompteBancaireByCode(retaDeclaration.getFkCompteBancaire());

                    jsonObject.addProperty("compteBancaireName2", compteBancaire == null ? "" : compteBancaire.getIntitule().toUpperCase());

                    jsonObject.addProperty("noteTaxation", retaDeclaration == null ? "" : retaDeclaration.getId() + "");
                    jsonObject.addProperty("numeroDeclaration", retaDeclaration == null ? "" : retaDeclaration.getCodeDeclaration().toUpperCase());

                    jsonObject.addProperty(DeclarationConst.ParamName.CODE_DEPOT_DECLARATION,
                            depotDecl.getCode().trim());

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            depotDecl.getNumeroDepot() != null ? depotDecl.getNumeroDepot().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(depotDecl.getDateCreat(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    bordereau = DeclarationBusiness.getBordereauByCode(String.valueOf(depotDecl.getBordereau()));

                    if (bordereau != null) {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                bordereau.getTotalMontantPercu());
                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                GeneralConst.Numeric.ZERO);
                    }

                    service = TaxationBusiness.getServiceByCode(depotDecl.getService().getCode().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE,
                            depotDecl.getService().getCode().trim());
                    if (service != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                service.getIntitule().toUpperCase().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_BORDEREAU,
                            depotDecl.getBordereau());

                    jsonObject.addProperty(DeclarationConst.ParamName.NOMBRE_IMPRESSION,
                            depotDecl.getNbrImpression());

                    jsonObject.addProperty(DeclarationConst.ParamName.ETAT,
                            depotDecl.getEtat());

                    personne = IdentificationBusiness.getPersonneByCode(depotDecl.getPersonne().getCode().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.USER_NAME,
                                personne.getLoginWeb().getUsername());
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (depotDecl.getDetailDepotDeclarationList().size() > 0) {

                        List<JsonObject> listDetailDepotDeclJson = new ArrayList<>();
                        ArticleBudgetaire article;

                        for (DetailDepotDeclaration dtldepotDecl : depotDecl.getDetailDepotDeclarationList()) {

                            detailDepotDeclJson = new JsonObject();

                            article = DeclarationBusiness.getArticleBudgetaireByCode(dtldepotDecl.getArticleBudgetaire().getCode().trim());

                            if (article != null) {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                        article.getCode());
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                        article.getIntitule());
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                        article.getCodeOfficiel());
                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                                    dtldepotDecl.getDepotDeclaration().getNumeroDepot() != null
                                            ? dtldepotDecl.getDepotDeclaration().getNumeroDepot().trim()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.TAUX,
                                    dtldepotDecl.getTaux() != null ? dtldepotDecl.getTaux()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.QUANTITE,
                                    dtldepotDecl.getQuantite() != null ? dtldepotDecl.getQuantite()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                                    dtldepotDecl.getValeurBase() != null ? dtldepotDecl.getValeurBase()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.DEVISE,
                                    dtldepotDecl.getDevise().getCode() != null
                                            ? dtldepotDecl.getDevise().getCode()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ETAT,
                                    dtldepotDecl.getEtat());
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_TARIF,
                                    dtldepotDecl.getTarif().getCode() != null
                                            ? dtldepotDecl.getTarif().getCode()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_TARIF,
                                    dtldepotDecl.getTarif().getIntitule() != null
                                            ? dtldepotDecl.getTarif().getIntitule()
                                            : GeneralConst.EMPTY_STRING);
                            if (dtldepotDecl.getFkPeriode() != null) {

                                PeriodeDeclaration periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(dtldepotDecl.getFkPeriode().trim());
                                Assujeti assujeti = DeclarationBusiness.getAssujettissementByPeriode(dtldepotDecl.getFkPeriode().trim());

                                if (periodeDeclarat != null) {

                                    if (periodeDeclarat.getDebut() != null) {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                                Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                                                        periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim()));
                                    } else {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                                GeneralConst.EMPTY_STRING);
                                    }

                                } else {
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                            GeneralConst.EMPTY_STRING);
                                }

                                if (assujeti != null) {

                                    int _type = 0;

                                    TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(assujeti.getBien().getTypeBien().getCode());

                                    _type = typeBienService.getType();

                                    detailDepotDeclJson.addProperty("type", _type);

                                    if (assujeti.getBien().getFkCommune() != null) {

                                        EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(assujeti.getBien().getFkCommune());

                                        String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                                        detailDepotDeclJson.addProperty("communeCode", ea.getCode());
                                        detailDepotDeclJson.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                                    } else {
                                        detailDepotDeclJson.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("communeName", GeneralConst.EMPTY_STRING);
                                    }

                                    if (assujeti.getBien().getFkQuartier() != null) {

                                        EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(assujeti.getBien().getFkQuartier());

                                        detailDepotDeclJson.addProperty("quartierCode", eaQuartier.getCode());
                                        detailDepotDeclJson.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                                    } else {
                                        detailDepotDeclJson.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                                    }

                                    if (assujeti.getBien().getFkTarif() != null) {

                                        Tarif tarif = TaxationBusiness.getTarifByCode(assujeti.getBien().getFkTarif());

                                        detailDepotDeclJson.addProperty("tarifCode", tarif.getCode());
                                        detailDepotDeclJson.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                                    } else {
                                        detailDepotDeclJson.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                                    }

                                    //infosComplementaire = GeneralConst.EMPTY_STRING;
                                    if (assujeti.getBien().getFkUsageBien() != null) {

                                        UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(assujeti.getBien().getFkUsageBien());

                                        detailDepotDeclJson.addProperty("usageCode", usageBien.getId());
                                        detailDepotDeclJson.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                                        detailDepotDeclJson.addProperty("isImmobilier", GeneralConst.Number.ONE);

                                    } else {
                                        detailDepotDeclJson.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("usageName", GeneralConst.EMPTY_STRING);
                                        detailDepotDeclJson.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                                    }

                                    detailDepotDeclJson.addProperty("libelleTypeBien", assujeti.getBien().getTypeBien().getIntitule().toUpperCase());

                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                            assujeti.getBien().getIntitule() == null ? "" : assujeti.getBien().getIntitule().toUpperCase());

                                    if (assujeti.getBien().getFkAdressePersonne() != null) {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                assujeti.getBien().getFkAdressePersonne().getAdresse().toString());
                                    } else {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                GeneralConst.EMPTY_STRING);
                                    }

                                } else {
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                            GeneralConst.EMPTY_STRING);
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                            GeneralConst.EMPTY_STRING);
                                }

                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailBordereau = DeclarationBusiness.getDetailBordereauByPeriodicite(dtldepotDecl.getFkPeriode());

                            if (bordereau != null) {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                        detailBordereau.getMontantPercu());
                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                        GeneralConst.Numeric.ZERO);
                            }

                            jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                                    dtldepotDecl.getDevise().getCode() != null
                                            ? dtldepotDecl.getDevise().getCode()
                                            : GeneralConst.EMPTY_STRING);

                            listDetailDepotDeclJson.add(detailDepotDeclJson);
                        }

                        jsonObject.addProperty(DeclarationConst.ParamName.LIST_DETAIL_DEPOT_DECLARATION,
                                listDetailDepotDeclJson.toString());
                    }

                    listJsonObjects.add(jsonObject);

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            dataReturn = listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadRetraitDeclaration(HttpServletRequest request) {

        String valueSearch, typeSearch, typeRegister, allSite, allService,
                codeSite, codeService, userId, dateDebut, dateFin;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            allSite = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SITE);
            allService = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SERVICE);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);

            List<JsonObject> listJsonObjects = new ArrayList<>();

            listRetraitDeclaration = new ArrayList<>();

            listRetraitDeclaration = DeclarationBusiness.getListRetraitDeclaration(
                    valueSearch.trim(),
                    Integer.valueOf(typeSearch),
                    typeRegister.trim(),
                    false,
                    allService.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                    codeSite.trim(),
                    codeService.trim(),
                    userId);

            if (!listRetraitDeclaration.isEmpty()) {

                List<ArchiveAccuseReception> archiveAccuseReceptions = new ArrayList<>();

                for (RetraitDeclaration retraitDecl : listRetraitDeclaration) {

                    JsonObject obj = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire article;
                    PeriodeDeclaration periodeDeclarat;
                    String intitulePeriode;
                    //ArchiveAccuseReception archiveAccuseReception = new ArchiveAccuseReception();

                    Site site = null;

                    if (retraitDecl.getFkSite() != null) {
                        site = TaxationBusiness.getSiteByCode(retraitDecl.getFkSite());
                    }

                    obj.addProperty("siteName",
                            site != null ? site.getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);

                    Bordereau bordereau = DeclarationBusiness.getBordereau(retraitDecl.getCodeDeclaration().trim());

                    if (bordereau != null) {

                        obj.addProperty(DeclarationConst.ParamName.MONTANT_PAYER,
                                bordereau.getTotalMontantPercu());

                        obj.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST,
                                GeneralConst.Number.ONE);

                    } else {
                        obj.addProperty(DeclarationConst.ParamName.MONTANT_PAYER,
                                GeneralConst.Numeric.ZERO);

                        obj.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST,
                                GeneralConst.Number.ZERO);

                    }

                    BigDecimal amountRD = new BigDecimal("0");
                    String periodeNames = "";

                    if (retraitDecl.getNewId() != null) {

                        obj.addProperty("detailExist", GeneralConst.Number.ONE);

                        List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID(retraitDecl.getNewId());

                        if (!retraitDeclarations.isEmpty()) {

                            for (RetraitDeclaration rd : retraitDeclarations) {

                                amountRD = amountRD.add(rd.getMontant());

                                PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationById(Integer.valueOf(rd.getFkPeriode()));
                                ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCode(rd.getFkAb());

                                if (periodeNames.isEmpty()) {

                                    periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode());
                                } else {
                                    periodeNames += periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                }
                            }
                        }

                    } else {

                        obj.addProperty("detailExist", GeneralConst.Number.ZERO);

                        amountRD = amountRD.add(retraitDecl.getMontant());

                        PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationById(Integer.valueOf(retraitDecl.getFkPeriode()));
                        ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb());

                        periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode());
                    }

                    archiveAccuseReceptions = DeclarationBusiness.getListArchivePreuvePaiementByDeclaration(
                            retraitDecl.getCodeDeclaration().trim());

                    if (!archiveAccuseReceptions.isEmpty()) {

                        obj.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                                GeneralConst.Number.ONE);

                        List<JsonObject> jsonArchiveList = new ArrayList<>();

                        for (ArchiveAccuseReception aar : archiveAccuseReceptions) {

                            JsonObject jsonArchive = new JsonObject();

                            jsonArchive.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT,
                                    aar.getArchive());

                            jsonArchive.addProperty(DeclarationConst.ParamName.NUMERO_NC,
                                    aar.getDocumentReference());

                            jsonArchive.addProperty(DeclarationConst.ParamName.OBSERVATION_DOC,
                                    aar.getObservation() == null ? GeneralConst.EMPTY_STRING : aar.getObservation().trim());

                            TypeDocument typeDocument = DeclarationBusiness.getTypeDocumentByCode(aar.getTypeDocument());

                            if (typeDocument != null) {
                                jsonArchive.addProperty(DeclarationConst.ParamName.LIBELLE_DOCUMENT,
                                        typeDocument.getIntitule());
                            } else {
                                jsonArchive.addProperty(DeclarationConst.ParamName.LIBELLE_DOCUMENT,
                                        GeneralConst.EMPTY_STRING);
                            }

                            jsonArchiveList.add(jsonArchive);

                        }

                        obj.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                jsonArchiveList.toString());

                    } else {

                        obj.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                                GeneralConst.Number.ZERO);

                        obj.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_DOCUMENT,
                                GeneralConst.EMPTY_STRING);

                        obj.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                GeneralConst.EMPTY_STRING);

                    }

                    Agent agentTaxateur = PaiementBusiness.getAgentByCode(Integer.valueOf(retraitDecl.getFkAgentCreate()));

                    obj.addProperty("agentTaxateur", agentTaxateur.toString().toUpperCase());

                    obj.addProperty(DeclarationConst.ParamName.ID_RETRAIT_DECLARATION,
                            retraitDecl.getId());

                    if (retraitDecl.getEstTaxationOffice() != null) {

                        obj.addProperty("typeTaxation", retraitDecl.getEstTaxationOffice() == 1 ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    } else {
                        obj.addProperty("typeTaxation", GeneralConst.Number.ZERO);
                    }

                    if (retraitDecl.getRetraitDeclarationMere() == null) {
                        obj.addProperty("estTaxationPenalite", GeneralConst.Number.ZERO);
                    } else {
                        obj.addProperty("estTaxationPenalite", GeneralConst.Number.ONE);

                        RetraitDeclaration principal = AssujettissementBusiness.getRetraitDeclarationByID(retraitDecl.getRetraitDeclarationMere());
                        obj.addProperty("codeNTPricipal", principal.getCodeDeclaration());
                    }

                    obj.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            retraitDecl.getCodeDeclaration() != null ? retraitDecl.getCodeDeclaration().trim() : GeneralConst.EMPTY_STRING);

                    obj.addProperty(TaxationConst.ParamName.OBSERVATION_CONTROLE,
                            retraitDecl.getObservationControle() != null ? retraitDecl.getObservationControle().trim() : GeneralConst.EMPTY_STRING);

                    obj.addProperty(TaxationConst.ParamName.ID_AVIS,
                            retraitDecl.getAvis() != null ? retraitDecl.getAvis().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateCreate(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        obj.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        obj.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    String dateEcheance = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateEcheancePaiement(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateEcheance != null && !dateEcheance.isEmpty()) {

                        obj.addProperty("dateEcheance", dateEcheance);

                        if (Compare.before(retraitDecl.getDateEcheancePaiement(), new Date())) {

                            obj.addProperty("echeanceDepasse", GeneralConst.Number.ONE);

                        } else {
                            obj.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                        }

                    } else {
                        obj.addProperty("dateEcheance", GeneralConst.EMPTY_STRING);
                        obj.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                    }

                    obj.addProperty(DeclarationConst.ParamName.ETAT,
                            retraitDecl.getEtat());
                    obj.addProperty(DeclarationConst.ParamName.REQUERANT,
                            retraitDecl.getRequerant());

                    personne = IdentificationBusiness.getPersonneByCode(retraitDecl.getFkAssujetti().trim());

                    if (personne != null) {

                        obj.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        obj.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        obj.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        obj.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        obj.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        obj.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        obj.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        obj.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        obj.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        obj.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        obj.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        obj.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL"))
                            || retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL_22M"))
                            || retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL_22A"))) {

                        article = TaxationBusiness.getArticleBudgetaireByCode(propertiesConfig.getProperty("CODE_IMPOT_RL"));

                    } else {

                        article = TaxationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb());
                    }

                    if (article != null) {
                        obj.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                article.getCode());
                        obj.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                article.getIntitule());
                        obj.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                article.getCodeOfficiel());
                    } else {
                        obj.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                        obj.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                    }

                    obj.addProperty(DeclarationConst.ParamName.DEVISE,
                            retraitDecl.getDevise() != null
                                    ? retraitDecl.getDevise()
                                    : GeneralConst.EMPTY_STRING);
                    obj.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                            retraitDecl.getValeurBase() != null ? retraitDecl.getValeurBase()
                                    : GeneralConst.EMPTY_ZERO);
                    obj.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                            retraitDecl.getMontant() != null ? amountRD
                                    : GeneralConst.EMPTY_ZERO);

                    obj.addProperty("codePeriodeDeclaration", retraitDecl.getFkPeriode());

                    if (retraitDecl.getPenaliteRemise().floatValue() > 0) {

                        obj.addProperty("resmiseExist", GeneralConst.Number.ONE);
                        obj.addProperty("amountRemise", retraitDecl.getPenaliteRemise());
                        obj.addProperty("tauxRemise", retraitDecl.getTauxRemise());
                        obj.addProperty("observationRemise", retraitDecl.getObservationRemise());

                        long moisRetard = 0;

                        PeriodeDeclaration periode = TaxationBusiness.getPeriodeDeclarationId(retraitDecl.getFkPeriode());

                        if (periode.getDateLimite() != null) {

                            if (Compare.before(periode.getDateLimite(), new Date())) {

                                moisRetard = ConvertDate.getMonthsBetweenV2(periode.getDateLimite(), new Date(), periode.getId());

                            } else {
                                moisRetard = 0;
                            }

                        } else {

                            if (Compare.before(periode.getFin(), new Date())) {

                                moisRetard = ConvertDate.getMonthsBetweenV2(periode.getFin(), new Date(), periode.getId());

                            } else {
                                moisRetard = 0;
                            }
                        }

                        obj.addProperty(TaxationConst.ParamName.NBRE_MOIS, moisRetard);

                    } else {
                        obj.addProperty("resmiseExist", GeneralConst.Number.ZERO);
                        obj.addProperty("amountRemise", 0);
                        obj.addProperty("tauxRemise", 0);
                        obj.addProperty("observationRemise", GeneralConst.EMPTY_STRING);
                        obj.addProperty(TaxationConst.ParamName.NBRE_MOIS, 0);
                    }

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(retraitDecl.getFkBanque());

                    obj.addProperty("banqueName", banque == null ? GeneralConst.EMPTY_STRING : banque.getIntitule().toUpperCase());

                    CompteBancaire compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(retraitDecl.getFkCompteBancaire());

                    obj.addProperty("compteBancaireName", compteBancaire == null ? GeneralConst.EMPTY_STRING : compteBancaire.getIntitule().toUpperCase());

                    periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(retraitDecl.getFkPeriode().trim());

                    if (periodeDeclarat != null) {

                        if (periodeDeclarat.getDebut() != null) {

                            /*obj.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                             Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                             periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim()));*/
                            obj.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE, periodeNames.toUpperCase());
                        } else {
                            obj.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (periodeDeclarat.getAssujetissement().getBien() != null) {

                            obj.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getIntitule().toUpperCase());

                            if (periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne() != null) {

                                obj.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                        periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString());

                            } else {

                                obj.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                            }
                            if (periodeDeclarat.getAssujetissement().getBien().getDescription() != null && !periodeDeclarat.getAssujetissement().getBien().getDescription().isEmpty()) {

                                obj.addProperty("descriptionBien", periodeDeclarat.getAssujetissement().getBien().getDescription());

                            } else {
                                obj.addProperty("descriptionBien", GeneralConst.EMPTY_STRING);
                            }

                            obj.addProperty(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getTypeBien().getIntitule().toUpperCase());

                            int _type = 0;

                            TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(periodeDeclarat.getAssujetissement().getBien().getTypeBien().getCode());

                            _type = typeBienService.getType();

                            obj.addProperty("type", _type);

                            if (periodeDeclarat.getAssujetissement().getBien().getFkCommune() != null) {

                                EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkCommune());

                                String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                                obj.addProperty("communeCode", ea.getCode());
                                obj.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                            } else {
                                obj.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                                obj.addProperty("communeName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkQuartier() != null) {

                                EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkQuartier());

                                obj.addProperty("quartierCode", eaQuartier.getCode());
                                obj.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                            } else {
                                obj.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                                obj.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkTarif() != null) {

                                Tarif tarif = TaxationBusiness.getTarifByCode(periodeDeclarat.getAssujetissement().getBien().getFkTarif());

                                obj.addProperty("tarifCode", tarif.getCode());
                                obj.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                            } else {
                                obj.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                                obj.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                            }

                            //infosComplementaire = GeneralConst.EMPTY_STRING;
                            if (periodeDeclarat.getAssujetissement().getBien().getFkUsageBien() != null) {

                                UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(periodeDeclarat.getAssujetissement().getBien().getFkUsageBien());

                                obj.addProperty("usageCode", usageBien.getId());
                                obj.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                                obj.addProperty("isImmobilier", GeneralConst.Number.ONE);

                            } else {
                                obj.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                                obj.addProperty("usageName", GeneralConst.EMPTY_STRING);
                                obj.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                            }

                        } else {
                            obj.addProperty(DeclarationConst.ParamName.INTITULE_BIEN, GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        obj.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                GeneralConst.EMPTY_STRING);
                    }
                    obj.addProperty(DeclarationConst.ParamName.PERIODICITE,
                            retraitDecl.getFkPeriode() != null ? retraitDecl.getFkPeriode()
                                    : GeneralConst.EMPTY_STRING);

                    List<RetraitDeclaration> listTaxationDependances = DeclarationBusiness.getListRetraitDeclarationDepandances(retraitDecl.getId());

                    List<JsonObject> titreIntercalaireJsonList = new ArrayList<>();

                    if (!listTaxationDependances.isEmpty()) {

                        for (RetraitDeclaration rdEchelonnement : listTaxationDependances) {

                            boolean isEcheanceEchus = false;

                            JsonObject intercalaireJsonObject = new JsonObject();
                            Personne personneEchelonnement = IdentificationBusiness.getPersonneByCode(rdEchelonnement.getFkAssujetti());

                            if (Compare.before(rdEchelonnement.getDateEcheancePaiement(), new Date())) {
                                isEcheanceEchus = true;
                            }

                            Journal journal = PaiementBusiness.getJournalByDocument(rdEchelonnement.getId() + "", false);

                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.NT);
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ASSUJETTI, personneEchelonnement.toString().toUpperCase());

                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_INTERET, false);
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO, rdEchelonnement.getId());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO_MANUEL, rdEchelonnement.getId());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, rdEchelonnement.getMontant());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, rdEchelonnement.getDevise());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE, rdEchelonnement.getRetraitDeclarationMere());

                            intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                            intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_SITE, rdEchelonnement.getFkSite());
                            intercalaireJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, GeneralConst.EMPTY_STRING);
                            intercalaireJsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE, rdEchelonnement.getFkSite());

                            compteBancaire = new CompteBancaire();
                            compteBancaire = GeneralBusiness.getCompteBancaireByCode(rdEchelonnement.getFkCompteBancaire());

                            intercalaireJsonObject.addProperty("COMPTE_BANCAIRE_PENALITE", compteBancaire == null ? "" : compteBancaire.getIntitule().toUpperCase());
                            intercalaireJsonObject.addProperty("BANQUE_PENALITE", compteBancaire == null ? "" : compteBancaire.getBanque().getIntitule().toUpperCase());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE_MANUEL, rdEchelonnement.getRetraitDeclarationMere());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ETAT_ECHELONNEMENT, rdEchelonnement.getEtat());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceEchus);
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, Tools.formatDateToString(rdEchelonnement.getDateEcheancePaiement()));
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT_V2, Tools.formatDateToString(rdEchelonnement.getDateEcheancePaiement()).replace("/", "_"));
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_PAID, journal != null);
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal != null ? journal.getEtat() == GeneralConst.Numeric.ONE : false);

                            titreIntercalaireJsonList.add(intercalaireJsonObject);
                        }
                    }

                    obj.addProperty(FractionnementConst.ParamName.listIntercalaireDetail, titreIntercalaireJsonList.toString());

                    listJsonObjects.add(obj);

                }

                dataReturn = listJsonObjects.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            //dataReturn = listJsonObjects.toString();
        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadRetraitDeclarationDefaillant(HttpServletRequest request) {

        String valueSearch, typeSearch, typeRegister, allSite, allService,
                codeSite, codeService, userId = GeneralConst.EMPTY_STRING;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            valueSearch = request.getParameter(TaxationConst.ParamName.VALUE_SEACH);
            typeSearch = request.getParameter(TaxationConst.ParamName.TYPE_SEACH);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            allSite = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SITE);
            allService = request.getParameter(TaxationConst.ParamName.VIEW_ALL_SERVICE);
            codeSite = request.getParameter(TaxationConst.ParamName.CODE_SITE);
            codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            userId = request.getParameter(TaxationConst.ParamName.USER_ID);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();

            listRetraitDeclaration = new ArrayList<>();

            List<RetraitDeclaration> listRetraitDeclaration1 = DeclarationBusiness.getListRetraitDeclarationDefaillant(
                    valueSearch.trim(),
                    Integer.valueOf(typeSearch),
                    typeRegister.trim(),
                    false,
                    allService.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                    codeSite.trim(),
                    codeService.trim(),
                    userId);

            List<RetraitDeclaration> listRetraitDeclaration2 = DeclarationBusiness.getListRetraitDeclarationDefaillantPaidRetard(
                    valueSearch.trim(),
                    Integer.valueOf(typeSearch),
                    typeRegister.trim(),
                    false,
                    allService.equals("true") ? Boolean.TRUE : Boolean.FALSE,
                    codeSite.trim(),
                    codeService.trim(),
                    userId);

            listRetraitDeclaration.addAll(listRetraitDeclaration1);
            listRetraitDeclaration.addAll(listRetraitDeclaration2);

            if (!listRetraitDeclaration.isEmpty()) {

                for (RetraitDeclaration retraitDecl : listRetraitDeclaration) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire article;
                    PeriodeDeclaration periodeDeclarat;

                    Bordereau bordereau = null;

                    if (retraitDecl.getCodeDeclaration() != null) {
                        bordereau = GeneralBusiness.getBordereauByDeclaration(retraitDecl.getCodeDeclaration());
                    }

                    jsonObject.addProperty("bordereauExist", bordereau == null ? "0" : "1");
                    jsonObject.addProperty("bordereauDate", bordereau == null ? "" : ConvertDate.formatDateToStringOfFormat(
                            bordereau.getDateCreate(),
                            GeneralConst.Format.FORMAT_DATE));

                    Site site = null;

                    if (retraitDecl.getFkSite() != null) {
                        site = TaxationBusiness.getSiteByCode(retraitDecl.getFkSite());
                    }

                    jsonObject.addProperty("siteName",
                            site != null ? site.getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);

                    jsonObject.addProperty(DeclarationConst.ParamName.ID_RETRAIT_DECLARATION,
                            retraitDecl.getId());

                    BigDecimal amountRD = new BigDecimal("0");
                    String periodeNames = "";

                    if (retraitDecl.getNewId() != null) {

                        List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID_V2(retraitDecl.getNewId());

                        if (!retraitDeclarations.isEmpty()) {

                            for (RetraitDeclaration rd : retraitDeclarations) {

                                amountRD = amountRD.add(rd.getMontant());

                                PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationByIdV2(Integer.valueOf(rd.getFkPeriode()));
                                ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCodeV2(rd.getFkAb());

                                if (periodeNames.isEmpty()) {

                                    periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                } else {
                                    periodeNames += periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                }
                            }
                        }
                    } else {

                        amountRD = amountRD.add(retraitDecl.getMontant());

                        PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationById(Integer.valueOf(retraitDecl.getFkPeriode()));
                        ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb());

                        periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode());
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PAYER, amountRD);

                    Med med = RecouvrementBusiness.getMedByPerideDeclaration(Integer.valueOf(retraitDecl.getFkPeriode()));

                    jsonObject.addProperty("medExist", med == null ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                    if (med != null) {

                        if (med.getNumeroDocument() != null) {
                            jsonObject.addProperty("numeroMed", med.getNumeroDocument());
                            jsonObject.addProperty("medId", med.getId());
                        } else {
                            jsonObject.addProperty("numeroMed", med.getId());
                            jsonObject.addProperty("medId", med.getId());
                        }

                    } else {
                        jsonObject.addProperty("numeroMed", "");
                        jsonObject.addProperty("medId", "");
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            retraitDecl.getCodeDeclaration() != null ? retraitDecl.getCodeDeclaration().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateCreate(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty("codePeriodeDeclaration", retraitDecl.getFkPeriode());

                    if (retraitDecl.getEstTaxationOffice() != null) {

                        jsonObject.addProperty("typeTaxation", retraitDecl.getEstTaxationOffice() == 1 ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    } else {
                        jsonObject.addProperty("typeTaxation", GeneralConst.Number.ZERO);
                    }

                    String dateEcheance = ConvertDate.formatDateToStringOfFormat(
                            retraitDecl.getDateEcheancePaiement(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateEcheance != null && !dateEcheance.isEmpty()) {

                        jsonObject.addProperty("dateEcheance", dateEcheance);

                        if (Compare.before(retraitDecl.getDateEcheancePaiement(), new Date())) {

                            jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ONE);

                        } else {
                            jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                        }

                    } else {
                        jsonObject.addProperty("dateEcheance", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.ETAT,
                            retraitDecl.getEtat());
                    jsonObject.addProperty(DeclarationConst.ParamName.REQUERANT,
                            retraitDecl.getRequerant());

                    personne = IdentificationBusiness.getPersonneByCode(retraitDecl.getFkAssujetti().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }

                    article = DeclarationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb().trim());

                    if (article != null) {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                article.getCode());
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                article.getIntitule());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                article.getCodeOfficiel());
                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                            retraitDecl.getDevise() != null
                                    ? retraitDecl.getDevise()
                                    : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                            retraitDecl.getValeurBase() != null ? retraitDecl.getValeurBase()
                                    : GeneralConst.EMPTY_ZERO);
                    jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                            retraitDecl.getMontant() != null ? amountRD
                                    : GeneralConst.EMPTY_ZERO);

                    long moisRetard = 0;
                    String echeanceDate = GeneralConst.EMPTY_STRING;

                    echeanceDate = ConvertDate.formatDateToString(retraitDecl.getDateEcheancePaiement());
                    moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(echeanceDate);
                    jsonObject.addProperty(TaxationConst.ParamName.NBRE_MOIS, moisRetard);

                    if (retraitDecl.getPenaliteRemise().floatValue() > 0) {

                        jsonObject.addProperty("resmiseExist", GeneralConst.Number.ONE);
                        jsonObject.addProperty("amountRemise", retraitDecl.getPenaliteRemise());
                        jsonObject.addProperty("tauxRemise", retraitDecl.getTauxRemise());
                        jsonObject.addProperty("observationRemise", retraitDecl.getObservationRemise());

                    } else {
                        jsonObject.addProperty("resmiseExist", GeneralConst.Number.ZERO);
                        jsonObject.addProperty("amountRemise", 0);
                        jsonObject.addProperty("tauxRemise", 0);
                        jsonObject.addProperty("observationRemise", GeneralConst.EMPTY_STRING);

                    }

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(retraitDecl.getFkBanque());

                    jsonObject.addProperty("banqueName", banque == null ? GeneralConst.EMPTY_STRING : banque.getIntitule().toUpperCase());

                    CompteBancaire compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(retraitDecl.getFkCompteBancaire());

                    jsonObject.addProperty("compteBancaireName", compteBancaire == null ? GeneralConst.EMPTY_STRING : compteBancaire.getIntitule().toUpperCase());

                    periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(retraitDecl.getFkPeriode().trim());

                    if (periodeDeclarat != null) {

                        if (periodeDeclarat.getDebut() != null) {

                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE, periodeNames);
                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        int _type = 0;

                        TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(
                                periodeDeclarat.getAssujetissement().getBien().getTypeBien().getCode());

                        _type = typeBienService.getType();

                        jsonObject.addProperty("type", _type);

                        if (periodeDeclarat.getAssujetissement().getBien() != null) {

                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getIntitule().toUpperCase());

                            if (periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne() != null) {

                                jsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                        periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString());

                            } else {

                                jsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                            }
                            if (periodeDeclarat.getAssujetissement().getBien().getDescription() != null && !periodeDeclarat.getAssujetissement().getBien().getDescription().isEmpty()) {

                                jsonObject.addProperty("descriptionBien", periodeDeclarat.getAssujetissement().getBien().getDescription());

                            } else {
                                jsonObject.addProperty("descriptionBien", GeneralConst.EMPTY_STRING);
                            }

                            jsonObject.addProperty(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getTypeBien().getIntitule().toUpperCase());

                            if (periodeDeclarat.getAssujetissement().getBien().getFkCommune() != null) {

                                EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkCommune());

                                String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                                jsonObject.addProperty("communeCode", ea.getCode());
                                jsonObject.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                            } else {
                                jsonObject.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("communeName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkQuartier() != null) {

                                EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkQuartier());

                                jsonObject.addProperty("quartierCode", eaQuartier.getCode());
                                jsonObject.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                            } else {
                                jsonObject.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkTarif() != null) {

                                Tarif tarif = TaxationBusiness.getTarifByCode(periodeDeclarat.getAssujetissement().getBien().getFkTarif());

                                jsonObject.addProperty("tarifCode", tarif.getCode());
                                jsonObject.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                            } else {
                                jsonObject.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                            }

                            //infosComplementaire = GeneralConst.EMPTY_STRING;
                            if (periodeDeclarat.getAssujetissement().getBien().getFkUsageBien() != null) {

                                UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(periodeDeclarat.getAssujetissement().getBien().getFkUsageBien());

                                jsonObject.addProperty("usageCode", usageBien.getId());
                                jsonObject.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                                jsonObject.addProperty("isImmobilier", GeneralConst.Number.ONE);

                            } else {
                                jsonObject.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("usageName", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                            }

                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN, GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                GeneralConst.EMPTY_STRING);
                    }
                    jsonObject.addProperty(DeclarationConst.ParamName.PERIODICITE,
                            retraitDecl.getFkPeriode() != null ? retraitDecl.getFkPeriode()
                                    : GeneralConst.EMPTY_STRING);

                    listJsonObjects.add(jsonObject);

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            dataReturn = listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadRetraitDeclarationBySearchAvanced2(HttpServletRequest request) {

        String codeSite, codeService, dateDebut, dateFin, typeRegister, declarationOnlineOnly = GeneralConst.EMPTY_STRING;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            codeSite = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SITE_CODE);
            codeService = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SERVICE_CODE);
            dateDebut = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_FIN);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            declarationOnlineOnly = request.getParameter(TaxationConst.ParamName.DECLARATION_ONLINE);
            String codeAgent = request.getParameter("codeAgent");

            dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            dateFin = ConvertDate.getValidFormatDate(dateFin);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();

            listRetraitDeclaration = new ArrayList<>();

            listRetraitDeclaration = DeclarationBusiness.getListRetraitDeclarationBySearchAvanced(
                    codeSite.trim(),
                    codeService,
                    dateDebut, dateFin, declarationOnlineOnly, Integer.valueOf(codeAgent));

            if (!listRetraitDeclaration.isEmpty()) {

                List<ArchiveAccuseReception> archiveAccuseReceptions = new ArrayList<>();

                for (RetraitDeclaration retraitDecl : listRetraitDeclaration) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire article;
                    PeriodeDeclaration periodeDeclarat;
                    String intitulePeriode;
                    Site site = null;

                    if (retraitDecl.getFkSite() != null) {
                        site = TaxationBusiness.getSiteByCode(retraitDecl.getFkSite());
                    }

                    //ArchiveAccuseReception archiveAccuseReception = new ArchiveAccuseReception();
                    Bordereau bordereau = DeclarationBusiness.getBordereau(retraitDecl.getCodeDeclaration().trim());

                    if (bordereau != null) {

                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PAYER,
                                bordereau.getTotalMontantPercu());

                        jsonObject.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST,
                                GeneralConst.Number.ONE);

                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PAYER,
                                GeneralConst.Numeric.ZERO);

                        jsonObject.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST,
                                GeneralConst.Number.ZERO);

                    }

                    archiveAccuseReceptions = DeclarationBusiness.getListArchivePreuvePaiementByDeclaration(
                            retraitDecl.getCodeDeclaration().trim());

                    if (!archiveAccuseReceptions.isEmpty()) {

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                                GeneralConst.Number.ONE);

                        List<JsonObject> jsonArchiveList = new ArrayList<>();

                        for (ArchiveAccuseReception aar : archiveAccuseReceptions) {

                            JsonObject jsonArchive = new JsonObject();

                            jsonArchive.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT,
                                    aar.getArchive());

                            jsonArchive.addProperty(DeclarationConst.ParamName.NUMERO_NC,
                                    aar.getDocumentReference());

                            jsonArchive.addProperty(DeclarationConst.ParamName.OBSERVATION_DOC,
                                    aar.getObservation() == null ? GeneralConst.EMPTY_STRING : aar.getObservation().trim());

                            TypeDocument typeDocument = DeclarationBusiness.getTypeDocumentByCode(aar.getTypeDocument());

                            if (typeDocument != null) {
                                jsonArchive.addProperty(DeclarationConst.ParamName.LIBELLE_DOCUMENT,
                                        typeDocument.getIntitule());
                            } else {
                                jsonArchive.addProperty(DeclarationConst.ParamName.LIBELLE_DOCUMENT,
                                        GeneralConst.EMPTY_STRING);
                            }

                            jsonArchiveList.add(jsonArchive);

                        }

                        jsonObject.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                jsonArchiveList.toString());

                    } else {

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST,
                                GeneralConst.Number.ZERO);

                        jsonObject.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_DOCUMENT,
                                GeneralConst.EMPTY_STRING);

                        jsonObject.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST,
                                GeneralConst.EMPTY_STRING);

                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.ID_RETRAIT_DECLARATION,
                            retraitDecl.getId());

                    if (retraitDecl.getEstTaxationOffice() != null) {

                        jsonObject.addProperty("typeTaxation", retraitDecl.getEstTaxationOffice() == 1 ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    } else {
                        jsonObject.addProperty("typeTaxation", GeneralConst.Number.ZERO);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            retraitDecl.getCodeDeclaration() != null ? retraitDecl.getCodeDeclaration().trim() : GeneralConst.EMPTY_STRING);

                    jsonObject.addProperty("siteName",
                            site != null ? site.getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);

                    jsonObject.addProperty(TaxationConst.ParamName.OBSERVATION_CONTROLE,
                            retraitDecl.getObservationControle() != null ? retraitDecl.getObservationControle().trim() : GeneralConst.EMPTY_STRING);

                    jsonObject.addProperty(TaxationConst.ParamName.ID_AVIS,
                            retraitDecl.getAvis() != null ? retraitDecl.getAvis().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateCreate(),
                            GeneralConst.Format.FORMAT_DATE);

                    jsonObject.addProperty("codePeriodeDeclaration", retraitDecl.getFkPeriode());

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    String dateEcheance = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateEcheancePaiement(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateEcheance != null && !dateEcheance.isEmpty()) {

                        jsonObject.addProperty("dateEcheance", dateEcheance);

                        if (Compare.before(retraitDecl.getDateEcheancePaiement(), new Date())) {

                            jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ONE);

                        } else {
                            jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                        }

                    } else {
                        jsonObject.addProperty("dateEcheance", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.ETAT,
                            retraitDecl.getEtat());
                    jsonObject.addProperty(DeclarationConst.ParamName.REQUERANT,
                            retraitDecl.getRequerant());

                    personne = IdentificationBusiness.getPersonneByCode(retraitDecl.getFkAssujetti().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL"))
                            || retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL_22M"))
                            || retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL_22A"))) {

                        article = TaxationBusiness.getArticleBudgetaireByCode(propertiesConfig.getProperty("CODE_IMPOT_RL"));

                    } else {

                        article = TaxationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb());
                    }

                    if (article != null) {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                article.getCode());
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                article.getIntitule());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                article.getCodeOfficiel());
                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                            retraitDecl.getDevise() != null
                                    ? retraitDecl.getDevise()
                                    : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                            retraitDecl.getValeurBase() != null ? retraitDecl.getValeurBase()
                                    : GeneralConst.EMPTY_ZERO);
                    jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                            retraitDecl.getMontant() != null ? retraitDecl.getMontant()
                                    : GeneralConst.EMPTY_ZERO);

                    if (retraitDecl.getPenaliteRemise().floatValue() > 0) {

                        jsonObject.addProperty("resmiseExist", GeneralConst.Number.ONE);
                        jsonObject.addProperty("amountRemise", retraitDecl.getPenaliteRemise());
                        jsonObject.addProperty("tauxRemise", retraitDecl.getTauxRemise());
                        jsonObject.addProperty("observationRemise", retraitDecl.getObservationRemise());

                        long moisRetard = 0;

                        PeriodeDeclaration periode = TaxationBusiness.getPeriodeDeclarationId(retraitDecl.getFkPeriode());

                        if (periode.getDateLimite() != null) {

                            if (Compare.before(periode.getDateLimite(), new Date())) {

                                moisRetard = ConvertDate.getMonthsBetweenV2(periode.getDateLimite(), new Date(), periode.getId());

                            } else {
                                moisRetard = 0;
                            }

                        } else {

                            if (Compare.before(periode.getFin(), new Date())) {

                                moisRetard = ConvertDate.getMonthsBetweenV2(periode.getFin(), new Date(), periode.getId());

                            } else {
                                moisRetard = 0;
                            }
                        }

                        jsonObject.addProperty(TaxationConst.ParamName.NBRE_MOIS, moisRetard);

                    } else {
                        jsonObject.addProperty("resmiseExist", GeneralConst.Number.ZERO);
                        jsonObject.addProperty("amountRemise", 0);
                        jsonObject.addProperty("tauxRemise", 0);
                        jsonObject.addProperty("observationRemise", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.NBRE_MOIS, 0);
                    }

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(retraitDecl.getFkBanque());

                    jsonObject.addProperty("banqueName", banque == null ? GeneralConst.EMPTY_STRING : banque.getIntitule().toUpperCase());

                    CompteBancaire compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(retraitDecl.getFkCompteBancaire());

                    jsonObject.addProperty("compteBancaireName", compteBancaire == null ? GeneralConst.EMPTY_STRING : compteBancaire.getIntitule().toUpperCase());

                    periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(retraitDecl.getFkPeriode().trim());

                    if (periodeDeclarat != null) {

                        if (periodeDeclarat.getDebut() != null) {

                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                                            periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim()));
                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (periodeDeclarat.getAssujetissement().getBien() != null) {

                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getIntitule().toUpperCase());

                            if (periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne() != null) {

                                jsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                        periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString());

                            } else {

                                jsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                            }
                            if (periodeDeclarat.getAssujetissement().getBien().getDescription() != null && !periodeDeclarat.getAssujetissement().getBien().getDescription().isEmpty()) {

                                jsonObject.addProperty("descriptionBien", periodeDeclarat.getAssujetissement().getBien().getDescription());

                            } else {
                                jsonObject.addProperty("descriptionBien", GeneralConst.EMPTY_STRING);
                            }

                            jsonObject.addProperty(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getTypeBien().getIntitule().toUpperCase());

                            if (periodeDeclarat.getAssujetissement().getBien().getFkCommune() != null) {

                                EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkCommune());

                                String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                                jsonObject.addProperty("communeCode", ea.getCode());
                                jsonObject.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                            } else {
                                jsonObject.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("communeName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkQuartier() != null) {

                                EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkQuartier());

                                jsonObject.addProperty("quartierCode", eaQuartier.getCode());
                                jsonObject.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                            } else {
                                jsonObject.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkTarif() != null) {

                                Tarif tarif = TaxationBusiness.getTarifByCode(periodeDeclarat.getAssujetissement().getBien().getFkTarif());

                                jsonObject.addProperty("tarifCode", tarif.getCode());
                                jsonObject.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                            } else {
                                jsonObject.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                            }

                            //infosComplementaire = GeneralConst.EMPTY_STRING;
                            if (periodeDeclarat.getAssujetissement().getBien().getFkUsageBien() != null) {

                                UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(periodeDeclarat.getAssujetissement().getBien().getFkUsageBien());

                                jsonObject.addProperty("usageCode", usageBien.getId());
                                jsonObject.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                                jsonObject.addProperty("isImmobilier", GeneralConst.Number.ONE);

                            } else {
                                jsonObject.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("usageName", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                            }

                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN, GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                GeneralConst.EMPTY_STRING);
                    }
                    jsonObject.addProperty(DeclarationConst.ParamName.PERIODICITE,
                            retraitDecl.getFkPeriode() != null ? retraitDecl.getFkPeriode()
                                    : GeneralConst.EMPTY_STRING);

                    listJsonObjects.add(jsonObject);

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            dataReturn = listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadRetraitDeclarationBySearchAvanced(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        String tarifCode = GeneralConst.EMPTY_STRING;
        String formJuridiqueCode = GeneralConst.EMPTY_STRING;

        try {

            String codeSite = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SITE_CODE);
            String codeAb = request.getParameter("codeAb");
            String mois = request.getParameter("mois");
            String codeAgentTaxateur = request.getParameter("codeAgentTaxateur");
            String annee = request.getParameter("annee");
            boolean isMensuel = Boolean.valueOf(request.getParameter("isMensuel"));
            String dateDebut = ConvertDate.getValidFormatDate(request.getParameter("dateDebut"));
            String dateFin = ConvertDate.getValidFormatDate(request.getParameter("dateFin"));
            String declarationOnlineOnly = request.getParameter(TaxationConst.ParamName.DECLARATION_ONLINE);

            List<JsonObject> listJsonObjects = new ArrayList<>();

            List<RetraitDeclaration> listRetraitDeclaration = new ArrayList<>();

            listRetraitDeclaration = DeclarationBusiness.getListRetraitDeclarationFromSearchAvanced(
                    codeSite.trim(), codeAb, mois, annee, isMensuel, dateDebut, dateFin, declarationOnlineOnly, codeAgentTaxateur);

            if (!listRetraitDeclaration.isEmpty()) {

                //List<ArchiveAccuseReception> archiveAccuseReceptions = new ArrayList<>();
                for (RetraitDeclaration retraitDecl : listRetraitDeclaration) {

                    JsonObject obj = new JsonObject();
                    Personne personne = new Personne();
                    ArticleBudgetaire article;
                    PeriodeDeclaration periodeDeclarat;
                    Site site = null;

                    if (retraitDecl.getFkSite() != null) {
                        site = TaxationBusiness.getSiteByCode(retraitDecl.getFkSite());
                    }

                    Bordereau bordereau = DeclarationBusiness.getBordereau(retraitDecl.getCodeDeclaration().trim());

                    if (bordereau != null) {

                        obj.addProperty(DeclarationConst.ParamName.MONTANT_PAYER, bordereau.getTotalMontantPercu());
                        obj.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST, GeneralConst.Number.ONE);

                    } else {
                        obj.addProperty(DeclarationConst.ParamName.MONTANT_PAYER, GeneralConst.Numeric.ZERO);
                        obj.addProperty(DeclarationConst.ParamName.BORDEREAU_EXIST, GeneralConst.Number.ZERO);

                    }

                    BigDecimal amountRD = new BigDecimal("0");
                    String periodeNames = "";

                    if (retraitDecl.getNewId() != null) {

                        obj.addProperty("detailExist", GeneralConst.Number.ONE);

                        List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID_V2(retraitDecl.getNewId());

                        if (!retraitDeclarations.isEmpty()) {

                            //List<JsonObject> rdJsonList = new ArrayList<>();
                            for (RetraitDeclaration rd : retraitDeclarations) {

                                amountRD = amountRD.add(rd.getMontant());

                                PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationByIdV2(Integer.valueOf(rd.getFkPeriode()));
                                ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCodeV2(rd.getFkAb());

                                if (periodeNames.isEmpty()) {

                                    periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                } else {
                                    periodeNames += periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                }

                                //JsonObject rdJson = new JsonObject();
                            }
                        }
                    } else {
                        obj.addProperty("detailExist", GeneralConst.Number.ZERO);
                        amountRD = amountRD.add(retraitDecl.getMontant());

                        PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationById(Integer.valueOf(retraitDecl.getFkPeriode()));
                        ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb());

                        periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode());
                    }

                    obj.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_EXIST, GeneralConst.Number.ZERO);
                    obj.addProperty(DeclarationConst.ParamName.PREUVE_PAIEMENT_DOCUMENT, GeneralConst.EMPTY_STRING);
                    obj.addProperty(DeclarationConst.ParamName.DECLARATION_DOCUMENT_LIST, GeneralConst.EMPTY_STRING);

                    obj.addProperty(DeclarationConst.ParamName.ID_RETRAIT_DECLARATION, retraitDecl.getId());

                    Agent agentTaxateur = PaiementBusiness.getAgentByCode(Integer.valueOf(retraitDecl.getFkAgentCreate()));

                    obj.addProperty("agentTaxateur", agentTaxateur.toString().toUpperCase());

                    if (retraitDecl.getEstTaxationOffice() != null) {

                        obj.addProperty("typeTaxation", retraitDecl.getEstTaxationOffice() == 1 ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    } else {
                        obj.addProperty("typeTaxation", GeneralConst.Number.ZERO);
                    }

                    if (retraitDecl.getRetraitDeclarationMere() == null) {
                        obj.addProperty("estTaxationPenalite", GeneralConst.Number.ZERO);
                    } else {
                        obj.addProperty("estTaxationPenalite", GeneralConst.Number.ONE);

                        RetraitDeclaration principal = AssujettissementBusiness.getRetraitDeclarationByID_V2(retraitDecl.getRetraitDeclarationMere());
                        obj.addProperty("codeNTPricipal", principal.getCodeDeclaration());
                    }

                    obj.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            retraitDecl.getCodeDeclaration() != null ? retraitDecl.getCodeDeclaration().trim() : GeneralConst.EMPTY_STRING);
                    obj.addProperty("siteName", site != null ? site.getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);
                    obj.addProperty(TaxationConst.ParamName.OBSERVATION_CONTROLE,
                            retraitDecl.getObservationControle() != null ? retraitDecl.getObservationControle().trim() : GeneralConst.EMPTY_STRING);
                    obj.addProperty(TaxationConst.ParamName.ID_AVIS, retraitDecl.getAvis() != null ? retraitDecl.getAvis().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateCreate(), GeneralConst.Format.FORMAT_DATE);
                    obj.addProperty("codePeriodeDeclaration", retraitDecl.getFkPeriode());

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        obj.addProperty(DeclarationConst.ParamName.DATE_CREATE, dateCreate);
                    } else {
                        obj.addProperty(TaxationConst.ParamName.DATE_CREATE, GeneralConst.EMPTY_STRING);
                    }

                    String dateEcheance = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateEcheancePaiement(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateEcheance != null && !dateEcheance.isEmpty()) {

                        obj.addProperty("dateEcheance", dateEcheance);

                        if (Compare.before(retraitDecl.getDateEcheancePaiement(), new Date())) {

                            obj.addProperty("echeanceDepasse", GeneralConst.Number.ONE);

                        } else {
                            obj.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                        }

                    } else {
                        obj.addProperty("dateEcheance", GeneralConst.EMPTY_STRING);
                        obj.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                    }

                    obj.addProperty(DeclarationConst.ParamName.ETAT, retraitDecl.getEtat());
                    obj.addProperty(DeclarationConst.ParamName.REQUERANT, retraitDecl.getRequerant());

                    personne = IdentificationBusiness.getPersonneByCode(retraitDecl.getFkAssujetti().trim());

                    if (personne != null) {

                        obj.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI, personne.toString().toUpperCase().trim());
                        obj.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE, personne.getFormeJuridique().getCode().trim());
                        obj.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE, personne.getFormeJuridique().getIntitule().toUpperCase().trim());
                        obj.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE, personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        obj.addProperty(DeclarationConst.ParamName.NOM_COMPLET, personne.toString().toUpperCase());
                        obj.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE, personne.getAdressePersonneList().get(0).getCode().trim());
                        formJuridiqueCode = personne.getFormeJuridique().getCode().trim();
                    } else {
                        obj.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI, GeneralConst.EMPTY_STRING);
                        obj.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE, GeneralConst.EMPTY_STRING);
                        obj.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE, GeneralConst.EMPTY_STRING);
                        obj.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE, GeneralConst.EMPTY_STRING);
                        obj.addProperty(DeclarationConst.ParamName.NOM_COMPLET, GeneralConst.EMPTY_STRING);
                        obj.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE, GeneralConst.EMPTY_STRING);
                    }

                    if (retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL"))
                            || retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL_22M"))
                            || retraitDecl.getFkAb().equals(propertiesConfig.getProperty("CODE_IMPOT_RL_22A"))) {

                        article = TaxationBusiness.getArticleBudgetaireByCode(propertiesConfig.getProperty("CODE_IMPOT_RL"));

                    } else {

                        article = TaxationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb());
                    }

                    //article = DeclarationBusiness.getArticleBudgetaireByCodeV2(retraitDecl.getFkAb().trim());
                    if (article != null) {
                        obj.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE, article.getCode());
                        obj.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, article.getIntitule());
                        obj.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL, article.getCodeOfficiel());
                    } else {
                        obj.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE, GeneralConst.EMPTY_STRING);
                        obj.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE, GeneralConst.EMPTY_STRING);
                    }

                    obj.addProperty(DeclarationConst.ParamName.DEVISE, retraitDecl.getDevise() != null
                            ? retraitDecl.getDevise() : GeneralConst.EMPTY_STRING);
                    obj.addProperty(DeclarationConst.ParamName.VALEUR_BASE, retraitDecl.getValeurBase() != null
                            ? retraitDecl.getValeurBase() : GeneralConst.EMPTY_ZERO);
                    obj.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                            retraitDecl.getMontant() != null ? amountRD : GeneralConst.EMPTY_ZERO);

                    if (retraitDecl.getPenaliteRemise().floatValue() > 0) {

                        obj.addProperty("resmiseExist", GeneralConst.Number.ONE);
                        obj.addProperty("amountRemise", retraitDecl.getPenaliteRemise());
                        obj.addProperty("tauxRemise", retraitDecl.getTauxRemise());
                        obj.addProperty("observationRemise", retraitDecl.getObservationRemise());

                        long moisRetard = 0;

                        PeriodeDeclaration periode = TaxationBusiness.getPeriodeDeclarationId(retraitDecl.getFkPeriode());

                        if (periode.getDateLimite() != null) {

                            if (Compare.before(periode.getDateLimite(), new Date())) {

                                moisRetard = ConvertDate.getMonthsBetweenV2(periode.getDateLimite(), new Date(), periode.getId());

                            } else {
                                moisRetard = 0;
                            }

                        } else {

                            if (Compare.before(periode.getFin(), new Date())) {

                                moisRetard = ConvertDate.getMonthsBetweenV2(periode.getFin(), new Date(), periode.getId());

                            } else {
                                moisRetard = 0;
                            }
                        }

                        obj.addProperty(TaxationConst.ParamName.NBRE_MOIS, moisRetard);

                    } else {
                        obj.addProperty("resmiseExist", GeneralConst.Number.ZERO);
                        obj.addProperty("amountRemise", 0);
                        obj.addProperty("tauxRemise", 0);
                        obj.addProperty("observationRemise", GeneralConst.EMPTY_STRING);
                        obj.addProperty(TaxationConst.ParamName.NBRE_MOIS, 0);
                    }

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(retraitDecl.getFkBanque());

                    obj.addProperty("banqueName", banque == null ? GeneralConst.EMPTY_STRING : banque.getIntitule().toUpperCase());

                    CompteBancaire compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(retraitDecl.getFkCompteBancaire());

                    obj.addProperty("compteBancaireName", compteBancaire == null ? GeneralConst.EMPTY_STRING : compteBancaire.getIntitule().toUpperCase());

                    periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(retraitDecl.getFkPeriode().trim());

                    if (periodeDeclarat != null) {

                        if (periodeDeclarat.getDebut() != null) {

                            obj.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE, periodeNames.toUpperCase());
                        } else {
                            obj.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        if (periodeDeclarat.getAssujetissement().getBien() != null) {

                            obj.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getIntitule().toUpperCase());

                            ComplementBien cb = DeclarationBusiness.getComplementBien(periodeDeclarat.getAssujetissement().getBien().getTypeBien().getCode());
                            if (cb != null) {
                                String codeTypeCplBien = periodeDeclarat.getAssujetissement().getBien().getComplementBienList().get(0).getTypeComplement().getComplement().getCode();
                                if (null != codeTypeCplBien) {
                                    switch (codeTypeCplBien) {
                                        case "00000000000001482015": {
                                            ValeurPredefinie vp = DeclarationBusiness.getValeurPredefinie(cb.getValeur());
                                            if (vp != null) {
                                                obj.addProperty("marque", vp.getValeur());
                                            } else {
                                                obj.addProperty("marque", "");
                                            }
                                            break;
                                        }
                                        case "00000000000001492015": {
                                            ValeurPredefinie vp = DeclarationBusiness.getValeurPredefinie(cb.getValeur());
                                            if (vp != null) {
                                                obj.addProperty("numeroPlaque", vp.getValeur());
                                            }
                                            break;
                                        }
                                    }
                                }
                            }

                            switch (periodeDeclarat.getAssujetissement().getArticleBudgetaire().getCode()) {

                                case "00000000000002282020": //RL;
                                case "00000000000002292020": //IRL;
                                case "00000000000002352021": // RL 22% MOIS
                                case "00000000000002362021": // RL 22% L'AN

                                    for (Acquisition acquisition : periodeDeclarat.getAssujetissement().getBien().getAcquisitionList()) {

                                        if (acquisition.getEtat() == 1) {

                                            Adresse a = TaxationBusiness.getAdresseDefaultByAssujetti(acquisition.getPersonne().getCode());

                                            //System.out.println("Adresse bien : " + a.toString().toUpperCase());
                                            if (acquisition.getProprietaire()) {

                                                obj.addProperty("proprietaire", acquisition.getPersonne().toString().toUpperCase());
                                                obj.addProperty("adresseProprietaire", a == null ? "" : a.toString().toUpperCase());

                                                //System.out.println("Adresse bailleur : " + a == null ? "" : a.toString().toUpperCase());
                                            } else {
                                                obj.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN, a == null ? "" : a.toString().toUpperCase());
                                            }

                                        }

                                    }

                                    break;

                                default:

                                    if (periodeDeclarat.getAssujetissement().getBien().getAcquisitionList().get(0).getProprietaire()) {

                                        obj.addProperty("proprietaire",
                                                periodeDeclarat.getAssujetissement().getBien().getAcquisitionList().get(0).getPersonne().toString());
                                        obj.addProperty("adresseProprietaire",
                                                periodeDeclarat.getAssujetissement().getBien().getAcquisitionList().get(0).getPersonne().getAdressePersonneList().get(0).getAdresse().getChaine());
                                    } else {
                                        obj.addProperty("adresseProprietaire", "");
                                    }

                                    if (periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne() != null) {

                                        obj.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                                periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString());

                                        //System.out.println("Adresse bien : " + periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString());
                                    } else {

                                        obj.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                                GeneralConst.EMPTY_STRING);
                                    }
                                    break;
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getDescription() != null && !periodeDeclarat.getAssujetissement().getBien().getDescription().isEmpty()) {

                                obj.addProperty("descriptionBien", periodeDeclarat.getAssujetissement().getBien().getDescription());

                            } else {
                                obj.addProperty("descriptionBien", GeneralConst.EMPTY_STRING);
                            }

                            obj.addProperty(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getTypeBien().getIntitule().toUpperCase());

                            int _type = 0;

                            TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(
                                    periodeDeclarat.getAssujetissement().getBien().getTypeBien().getCode());

                            _type = typeBienService.getType();

                            obj.addProperty("type", _type);

                            if (periodeDeclarat.getAssujetissement().getBien().getFkCommune() != null) {

                                EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkCommune());

                                String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                                obj.addProperty("communeCode", ea.getCode());
                                obj.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                            } else {
                                obj.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                                obj.addProperty("communeName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkQuartier() != null) {

                                EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkQuartier());

                                obj.addProperty("quartierCode", eaQuartier.getCode());
                                obj.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                            } else {
                                obj.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                                obj.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkTarif() != null) {

                                Tarif tarif = TaxationBusiness.getTarifByCode(periodeDeclarat.getAssujetissement().getBien().getFkTarif());

                                obj.addProperty("tarifCode", tarif.getCode());
                                obj.addProperty("tarifName", tarif.getIntitule().toUpperCase());
                                tarifCode = tarif.getCode();

                            } else {
                                obj.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                                obj.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                            }

                            ParamTauxVignette ptv = DeclarationBusiness.getTauxVignette(tarifCode, formJuridiqueCode);
                            if (ptv != null) {
                                obj.addProperty("montantImpot", ptv.getMontantImpot());
                                obj.addProperty("montantTscr", ptv.getMontantTscr());
                                obj.addProperty("montantTotal", ptv.getMontantTotal());
                            } else {
                                obj.addProperty("montantImpot", 0);
                                obj.addProperty("montantTscr", 0);
                                obj.addProperty("montantTotal", 0);
                            }

                            //infosComplementaire = GeneralConst.EMPTY_STRING;
                            if (periodeDeclarat.getAssujetissement().getBien().getFkUsageBien() != null) {

                                UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(periodeDeclarat.getAssujetissement().getBien().getFkUsageBien());

                                obj.addProperty("usageCode", usageBien.getId());
                                obj.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                                obj.addProperty("isImmobilier", GeneralConst.Number.ONE);

                            } else {
                                obj.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                                obj.addProperty("usageName", GeneralConst.EMPTY_STRING);
                                obj.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                            }

                        } else {
                            obj.addProperty(DeclarationConst.ParamName.INTITULE_BIEN, GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        obj.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                GeneralConst.EMPTY_STRING);
                    }
                    obj.addProperty(DeclarationConst.ParamName.PERIODICITE,
                            retraitDecl.getFkPeriode() != null ? retraitDecl.getFkPeriode()
                                    : GeneralConst.EMPTY_STRING);

                    List<RetraitDeclaration> listTaxationDependances = DeclarationBusiness.getListRetraitDeclarationDepandances(retraitDecl.getId());

                    List<JsonObject> titreIntercalaireJsonList = new ArrayList<>();

                    if (!listTaxationDependances.isEmpty()) {

                        for (RetraitDeclaration rdEchelonnement : listTaxationDependances) {

                            boolean isEcheanceEchus = false;

                            JsonObject intercalaireJsonObject = new JsonObject();
                            Personne personneEchelonnement = IdentificationBusiness.getPersonneByCode(rdEchelonnement.getFkAssujetti());

                            if (Compare.before(rdEchelonnement.getDateEcheancePaiement(), new Date())) {
                                isEcheanceEchus = true;
                            }

                            Journal journal = PaiementBusiness.getJournalByDocument(rdEchelonnement.getId() + "", false);

                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.TYPE_DOCUMENT, DocumentConst.DocumentCode.NT);
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ASSUJETTI, personneEchelonnement.toString().toUpperCase());

                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_INTERET, false);
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO, rdEchelonnement.getId());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NUMERO_MANUEL, rdEchelonnement.getId());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.MONTANT_DU, rdEchelonnement.getMontant());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DEVISE, rdEchelonnement.getDevise());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE, rdEchelonnement.getRetraitDeclarationMere());

                            intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_ENTITE, GeneralConst.EMPTY_STRING);
                            intercalaireJsonObject.addProperty(GeneralConst.ParamName.CODE_SITE, rdEchelonnement.getFkSite());
                            intercalaireJsonObject.addProperty(GeneralConst.ParamName.PROVINCE_CODE, GeneralConst.EMPTY_STRING);
                            intercalaireJsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE, rdEchelonnement.getFkSite());

                            compteBancaire = new CompteBancaire();
                            compteBancaire = GeneralBusiness.getCompteBancaireByCode(rdEchelonnement.getFkCompteBancaire());

                            intercalaireJsonObject.addProperty("COMPTE_BANCAIRE_PENALITE", compteBancaire == null ? "" : compteBancaire.getIntitule().toUpperCase());
                            intercalaireJsonObject.addProperty("BANQUE_PENALITE", compteBancaire == null ? "" : compteBancaire.getBanque().getIntitule().toUpperCase());

                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.NP_MERE_MANUEL, rdEchelonnement.getRetraitDeclarationMere());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.ETAT_ECHELONNEMENT, rdEchelonnement.getEtat());
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_ECHEANCE_ECHUS, isEcheanceEchus);
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT, Tools.formatDateToString(rdEchelonnement.getDateEcheancePaiement()));
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.DATE_ECHEANCE_PAIEMENT_V2, Tools.formatDateToString(rdEchelonnement.getDateEcheancePaiement()).replace("/", "_"));
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_PAID, journal != null);
                            intercalaireJsonObject.addProperty(NotePerceptionConst.ParamName.IS_APURED, journal != null ? journal.getEtat() == GeneralConst.Numeric.ONE : false);

                            titreIntercalaireJsonList.add(intercalaireJsonObject);
                        }
                    }

                    obj.addProperty(FractionnementConst.ParamName.listIntercalaireDetail, titreIntercalaireJsonList.toString());

                    listJsonObjects.add(obj);

                }

                dataReturn = listJsonObjects.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            //dataReturn = listJsonObjects.toString();
        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadRetraitDeclarationDefaillantBySearchAvanced(HttpServletRequest request) {

        String codeSite, codeService, dateDebut, dateFin, typeRegister, declarationOnlineOnly = GeneralConst.EMPTY_STRING;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            codeSite = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SITE_CODE);
            codeService = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.SERVICE_CODE);
            dateDebut = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_DEBUT);
            dateFin = request.getParameter(cd.hologram.erecettesvg.constants.GeneralConst.ParamName.DATE_FIN);
            typeRegister = request.getParameter(TaxationConst.ParamName.TYPE_REGISTER);
            declarationOnlineOnly = request.getParameter(TaxationConst.ParamName.DECLARATION_ONLINE);
            String codeAgent = request.getParameter("codeAgent");

            dateDebut = ConvertDate.getValidFormatDate(dateDebut);
            dateFin = ConvertDate.getValidFormatDate(dateFin);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();

            listRetraitDeclaration = new ArrayList<>();

            List<RetraitDeclaration> listRetraitDeclaration1 = DeclarationBusiness.getListRetraitDeclarationDefaillantBySearchAvanced(
                    codeSite.trim(),
                    codeService,
                    dateDebut, dateFin, declarationOnlineOnly, Integer.valueOf(codeAgent));

            List<RetraitDeclaration> listRetraitDeclaration2 = DeclarationBusiness.getListRetraitDeclarationDefaillantPaidBySearchAvanced(
                    codeSite.trim(),
                    codeService,
                    dateDebut, dateFin, declarationOnlineOnly, Integer.valueOf(codeAgent));

            listRetraitDeclaration.addAll(listRetraitDeclaration1);
            listRetraitDeclaration.addAll(listRetraitDeclaration2);

            if (!listRetraitDeclaration.isEmpty()) {

                for (RetraitDeclaration retraitDecl : listRetraitDeclaration) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();
                    ArticleBudgetaire article;
                    PeriodeDeclaration periodeDeclarat;

                    Bordereau bordereau = null;

                    if (retraitDecl.getCodeDeclaration() != null) {
                        bordereau = GeneralBusiness.getBordereauByDeclaration(retraitDecl.getCodeDeclaration());
                    }

                    jsonObject.addProperty("bordereauExist", bordereau == null ? "0" : "1");
                    jsonObject.addProperty("bordereauDate", bordereau == null ? "" : ConvertDate.formatDateToStringOfFormat(
                            bordereau.getDateCreate(),
                            GeneralConst.Format.FORMAT_DATE));

                    Site site = null;

                    if (retraitDecl.getFkSite() != null) {
                        site = TaxationBusiness.getSiteByCode(retraitDecl.getFkSite());
                    }

                    jsonObject.addProperty("siteName",
                            site != null ? site.getIntitule().toUpperCase() : GeneralConst.EMPTY_STRING);

                    jsonObject.addProperty(DeclarationConst.ParamName.ID_RETRAIT_DECLARATION,
                            retraitDecl.getId());

                    BigDecimal amountRD = new BigDecimal("0");
                    String periodeNames = "";

                    if (retraitDecl.getNewId() != null) {

                        List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID_V2(retraitDecl.getNewId());

                        if (!retraitDeclarations.isEmpty()) {

                            for (RetraitDeclaration rd : retraitDeclarations) {

                                amountRD = amountRD.add(rd.getMontant());

                                PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationByIdV2(Integer.valueOf(rd.getFkPeriode()));
                                ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCodeV2(rd.getFkAb());

                                if (periodeNames.isEmpty()) {

                                    periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                } else {
                                    periodeNames += periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                }
                            }
                        }
                    } else {

                        amountRD = amountRD.add(retraitDecl.getMontant());

                        PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationById(Integer.valueOf(retraitDecl.getFkPeriode()));
                        ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb());

                        periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode());
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PAYER, amountRD);

                    Med med = RecouvrementBusiness.getMedByPerideDeclaration(Integer.valueOf(retraitDecl.getFkPeriode()));

                    jsonObject.addProperty("medExist", med == null ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE);

                    if (med != null) {

                        if (med.getNumeroDocument() != null) {
                            jsonObject.addProperty("numeroMed", med.getNumeroDocument());
                            jsonObject.addProperty("medId", med.getId());
                        } else {
                            jsonObject.addProperty("numeroMed", med.getId());
                            jsonObject.addProperty("medId", med.getId());
                        }

                    } else {
                        jsonObject.addProperty("numeroMed", "");
                        jsonObject.addProperty("medId", "");
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            retraitDecl.getCodeDeclaration() != null ? retraitDecl.getCodeDeclaration().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(retraitDecl.getDateCreate(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty("codePeriodeDeclaration", retraitDecl.getFkPeriode());

                    if (retraitDecl.getEstTaxationOffice() != null) {

                        jsonObject.addProperty("typeTaxation", retraitDecl.getEstTaxationOffice() == 1 ? GeneralConst.Number.ONE : GeneralConst.Number.ZERO);

                    } else {
                        jsonObject.addProperty("typeTaxation", GeneralConst.Number.ZERO);
                    }

                    String dateEcheance = ConvertDate.formatDateToStringOfFormat(
                            retraitDecl.getDateEcheancePaiement(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateEcheance != null && !dateEcheance.isEmpty()) {

                        jsonObject.addProperty("dateEcheance", dateEcheance);

                        if (Compare.before(retraitDecl.getDateEcheancePaiement(), new Date())) {

                            jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ONE);

                        } else {
                            jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                        }

                    } else {
                        jsonObject.addProperty("dateEcheance", GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty("echeanceDepasse", GeneralConst.Number.ZERO);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.ETAT,
                            retraitDecl.getEtat());
                    jsonObject.addProperty(DeclarationConst.ParamName.REQUERANT,
                            retraitDecl.getRequerant());

                    personne = IdentificationBusiness.getPersonneByCode(retraitDecl.getFkAssujetti().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }

                    article = DeclarationBusiness.getArticleBudgetaireByCode(retraitDecl.getFkAb().trim());

                    if (article != null) {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                article.getCode());
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                article.getIntitule());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                article.getCodeOfficiel());
                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                            retraitDecl.getDevise() != null
                                    ? retraitDecl.getDevise()
                                    : GeneralConst.EMPTY_STRING);
                    jsonObject.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                            retraitDecl.getValeurBase() != null ? retraitDecl.getValeurBase()
                                    : GeneralConst.EMPTY_ZERO);
                    jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                            retraitDecl.getMontant() != null ? amountRD
                                    : GeneralConst.EMPTY_ZERO);

                    long moisRetard = 0;
                    String echeanceDate = GeneralConst.EMPTY_STRING;

                    echeanceDate = ConvertDate.formatDateToString(retraitDecl.getDateEcheancePaiement());
                    moisRetard = TaxationBusiness.getDateDiffBetwenTwoDates(echeanceDate);
                    jsonObject.addProperty(TaxationConst.ParamName.NBRE_MOIS, moisRetard);

                    if (retraitDecl.getPenaliteRemise().floatValue() > 0) {

                        jsonObject.addProperty("resmiseExist", GeneralConst.Number.ONE);
                        jsonObject.addProperty("amountRemise", retraitDecl.getPenaliteRemise());
                        jsonObject.addProperty("tauxRemise", retraitDecl.getTauxRemise());
                        jsonObject.addProperty("observationRemise", retraitDecl.getObservationRemise());

                    } else {
                        jsonObject.addProperty("resmiseExist", GeneralConst.Number.ZERO);
                        jsonObject.addProperty("amountRemise", 0);
                        jsonObject.addProperty("tauxRemise", 0);
                        jsonObject.addProperty("observationRemise", GeneralConst.EMPTY_STRING);

                    }

                    cd.hologram.erecettesvg.models.Banque banque = GeneralBusiness.getBanqueByCode(retraitDecl.getFkBanque());

                    jsonObject.addProperty("banqueName", banque == null ? GeneralConst.EMPTY_STRING : banque.getIntitule().toUpperCase());

                    CompteBancaire compteBancaire = AcquitLiberatoireBusiness.getCompteBancaireByCode(retraitDecl.getFkCompteBancaire());

                    jsonObject.addProperty("compteBancaireName", compteBancaire == null ? GeneralConst.EMPTY_STRING : compteBancaire.getIntitule().toUpperCase());

                    periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(retraitDecl.getFkPeriode().trim());

                    if (periodeDeclarat != null) {

                        if (periodeDeclarat.getDebut() != null) {

                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE, periodeNames);
                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                    GeneralConst.EMPTY_STRING);
                        }

                        int _type = 0;

                        TypeBienService typeBienService = AssujettissementBusiness.getTypeBienServiceByTypeBien(
                                periodeDeclarat.getAssujetissement().getBien().getTypeBien().getCode());

                        _type = typeBienService.getType();

                        jsonObject.addProperty("type", _type);

                        if (periodeDeclarat.getAssujetissement().getBien() != null) {

                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getIntitule().toUpperCase());

                            if (periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne() != null) {

                                jsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                        periodeDeclarat.getAssujetissement().getBien().getFkAdressePersonne().getAdresse().toString());

                            } else {

                                jsonObject.addProperty(AssujettissementConst.ParamName.ADRESSE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                            }
                            if (periodeDeclarat.getAssujetissement().getBien().getDescription() != null && !periodeDeclarat.getAssujetissement().getBien().getDescription().isEmpty()) {

                                jsonObject.addProperty("descriptionBien", periodeDeclarat.getAssujetissement().getBien().getDescription());

                            } else {
                                jsonObject.addProperty("descriptionBien", GeneralConst.EMPTY_STRING);
                            }

                            jsonObject.addProperty(AssujettissementConst.ParamName.LIBELLE_TYPE_BIEN,
                                    periodeDeclarat.getAssujetissement().getBien().getTypeBien().getIntitule().toUpperCase());

                            if (periodeDeclarat.getAssujetissement().getBien().getFkCommune() != null) {

                                EntiteAdministrative ea = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkCommune());

                                String ville = " (Ville : <span style='font-weight:bold'>".concat(ea.getEntiteMere().getIntitule().toUpperCase()).concat("</span>)");

                                jsonObject.addProperty("communeCode", ea.getCode());
                                jsonObject.addProperty("communeName", ea.getIntitule().toUpperCase().concat(ville));

                            } else {
                                jsonObject.addProperty("communeCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("communeName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkQuartier() != null) {

                                EntiteAdministrative eaQuartier = IdentificationBusiness.getEntiteAdministrativeByCode(periodeDeclarat.getAssujetissement().getBien().getFkQuartier());

                                jsonObject.addProperty("quartierCode", eaQuartier.getCode());
                                jsonObject.addProperty("quartierName", eaQuartier.getIntitule().toUpperCase());

                            } else {
                                jsonObject.addProperty("quartierCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("quartierName", GeneralConst.EMPTY_STRING);
                            }

                            if (periodeDeclarat.getAssujetissement().getBien().getFkTarif() != null) {

                                Tarif tarif = TaxationBusiness.getTarifByCode(periodeDeclarat.getAssujetissement().getBien().getFkTarif());

                                jsonObject.addProperty("tarifCode", tarif.getCode());
                                jsonObject.addProperty("tarifName", tarif.getIntitule().toUpperCase());

                            } else {
                                jsonObject.addProperty("tarifCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("tarifName", GeneralConst.EMPTY_STRING);
                            }

                            //infosComplementaire = GeneralConst.EMPTY_STRING;
                            if (periodeDeclarat.getAssujetissement().getBien().getFkUsageBien() != null) {

                                UsageBien usageBien = AssujettissementBusiness.getUsageBienByCode(periodeDeclarat.getAssujetissement().getBien().getFkUsageBien());

                                jsonObject.addProperty("usageCode", usageBien.getId());
                                jsonObject.addProperty("usageName", usageBien.getIntitule().toUpperCase());
                                jsonObject.addProperty("isImmobilier", GeneralConst.Number.ONE);

                            } else {
                                jsonObject.addProperty("usageCode", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("usageName", GeneralConst.EMPTY_STRING);
                                jsonObject.addProperty("isImmobilier", GeneralConst.Number.ZERO);

                            }

                        } else {
                            jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_BIEN, GeneralConst.EMPTY_STRING);
                        }

                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                GeneralConst.EMPTY_STRING);
                    }
                    jsonObject.addProperty(DeclarationConst.ParamName.PERIODICITE,
                            retraitDecl.getFkPeriode() != null ? retraitDecl.getFkPeriode()
                                    : GeneralConst.EMPTY_STRING);

                    listJsonObjects.add(jsonObject);

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            dataReturn = listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printNoteTaxation(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String numero = request.getParameter("numero");
            String typeDoc = request.getParameter("typeDoc");

            /*Archive archive = getArchiveByRefDocument(numero);

             if (archive != null) {
                
             dataReturn = archive.getDocumentString();

             } else {*/
            if (typeDoc != null) {

                PrintDocument printDocument = new PrintDocument();
                RetraitDeclaration retaDeclaration;
                switch (typeDoc) {

                    case "NT":
                    case "NT2":

                        retaDeclaration = DeclarationBusiness.getRetraiteclarationByID(Integer.valueOf(numero));

                        BigDecimal amountPD = new BigDecimal("0");
                        String periodeName = "";

                        PeriodeDeclaration p;
                        ArticleBudgetaire ab;

                        if (retaDeclaration != null) {

                            if (retaDeclaration.getNewId() != null) {

                                List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID_V2(retaDeclaration.getNewId());

                                if (!retraitDeclarations.isEmpty()) {

                                    for (RetraitDeclaration rd : retraitDeclarations) {

                                        p = new PeriodeDeclaration();
                                        ab = new ArticleBudgetaire();

                                        p = RecouvrementBusiness.getPeriodeDeclarationByIdV2(Integer.valueOf(rd.getFkPeriode()));
                                        ab = DeclarationBusiness.getArticleBudgetaireByCodeV2(rd.getFkAb());

                                        if (periodeName.isEmpty()) {

                                            periodeName = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                        } else {
                                            periodeName += periodeName = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                        }

                                        amountPD = amountPD.add(rd.getMontant());
                                    }
                                }
                            } else {
                                p = new PeriodeDeclaration();
                                ab = new ArticleBudgetaire();

                                p = RecouvrementBusiness.getPeriodeDeclarationByIdV2(Integer.valueOf(retaDeclaration.getFkPeriode()));
                                ab = DeclarationBusiness.getArticleBudgetaireByCodeV2(retaDeclaration.getFkAb());

                                periodeName = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);

                                amountPD = amountPD.add(retaDeclaration.getMontant());
                            }

                            dataReturn = printDocument.createNoteTaxationDeclaration(retaDeclaration, retaDeclaration.getFkAgentCreate(), amountPD, periodeName);
                        }

                        break;

                    case "AMR":

                        Amr amr = PoursuiteBusiness.getAmrByMedV2(numero);
                        Med med = RecouvrementBusiness.getMedById(amr.getFkMed());

                        RetraitDeclaration retraitPenalite = AssujettissementBusiness.getRetraitDeclarationByCodeMere(
                                amr.getFkRetraitDeclaration());
                        BigDecimal penaliteDu = new BigDecimal("0");

                        if (retraitPenalite.getPenaliteRemise().floatValue() > 0) {
                            penaliteDu = penaliteDu.add(retraitPenalite.getPenaliteRemise());
                        } else {
                            penaliteDu = penaliteDu.add(retraitPenalite.getMontant());
                        }

                        dataReturn = printDocument.createAmrOfInvitationApayer(amr, amr.getAgentCreat(), med, 2, penaliteDu);

                        break;

                }
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            //}
        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String printRecepisse(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        Personne personne = new Personne();
        RecepissePrint recepisse = new RecepissePrint();
        PrintDocument printDocument = new PrintDocument();

        try {

            String numeroBordereau = request.getParameter(DeclarationConst.ParamName.NUMERO_BORDEREAU);

            Bordereau bord = DeclarationBusiness.getBordereau(numeroBordereau);

            if (bord != null) {

                DepotDeclaration depotDeclaration = DeclarationBusiness.getDepotDeclarationByBordereau(bord.getCode());

                Archive archive = getArchiveByRefDocument(depotDeclaration.getCode());

                RetraitDeclaration retraitDeclaration = AssujettissementBusiness.getRetraitDeclarationByCode(bord.getNumeroDeclaration());

                if (archive != null) {

                    recepisse.setNumDepotDeclaration(archive.getDossier().trim());

                    dataReturn = printDocument.createRecepisse(recepisse, true);

                } else {

                    PeriodeDeclaration pd = AssujettissementBusiness.getPeriodeDeclarationByCode(retraitDeclaration.getFkPeriode());

                    recepisse.setNoteTaxation(retraitDeclaration.getId() + "");

                    String periodeNames = "";

                    if (retraitDeclaration.getNewId() != null) {

                        List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID_V2(retraitDeclaration.getNewId());

                        if (!retraitDeclarations.isEmpty()) {

                            //List<JsonObject> rdJsonList = new ArrayList<>();
                            for (RetraitDeclaration rd : retraitDeclarations) {

                                PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationByIdV2(Integer.valueOf(rd.getFkPeriode()));
                                ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCodeV2(rd.getFkAb());

                                if (periodeNames.isEmpty()) {

                                    periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                } else {
                                    periodeNames += periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode()).concat(GeneralConst.SPACE);
                                }

                                //JsonObject rdJson = new JsonObject();
                            }
                        }
                    } else {

                        PeriodeDeclaration p = RecouvrementBusiness.getPeriodeDeclarationById(Integer.valueOf(retraitDeclaration.getFkPeriode()));
                        ArticleBudgetaire ab = DeclarationBusiness.getArticleBudgetaireByCode(retraitDeclaration.getFkAb());

                        periodeNames = Tools.getPeriodeIntitule(p.getDebut(), ab.getPeriodicite().getCode());
                    }

                    recepisse.setPeriodeDeclaration(periodeNames);

                    recepisse.setImpotName(pd.getAssujetissement().getArticleBudgetaire().getIntitule().toUpperCase());

                    recepisse.setCompteBancaire(bord.getCompteBancaire().getIntitule().trim());
                    recepisse.setReferenceDocument(bord.getNumeroBordereau().trim());
                    recepisse.setMontantPayer(String.valueOf(bord.getTotalMontantPercu()).concat(GeneralConst.SPACE).concat(GeneralConst.BRAKET_OPEN).concat(bord.getDetailBordereauList().get(0).getDevise().concat(GeneralConst.BRAKET_CLOSE)));
                    String datePaiement = Tools.formatDateToString(bord.getDateCreate());
                    recepisse.setDatePaiement(datePaiement);
                    recepisse.setTypeTitre(DeclarationConst.ParamName.BORDEREAU);
                    recepisse.setBanque(bord.getCompteBancaire().getBanque().getIntitule().trim());

                    recepisse.setDateImpression(Tools.formatDateToString(new Date()));

                    DepotDeclaration depotDecl = DeclarationBusiness.getDepotDeclarationByBordereau(numeroBordereau);

                    if (depotDecl != null) {

                        recepisse.setDateDepot(Tools.formatDateToString(depotDecl.getDateCreat()));

                        personne = IdentificationBusiness.getPersonneByCode(depotDecl.getPersonne().getCode().trim());

                        if (personne != null) {

                            recepisse.setNomAssujetti(personne.toString().toUpperCase().trim());
                            recepisse.setAdresseAssujetti(personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                            if (personne.getLoginWeb() != null) {
                                recepisse.setEmailAssujetti(personne.getLoginWeb().getMail().trim());
                                recepisse.setTelephoneAssujetti(personne.getLoginWeb().getTelephone().trim());
                            } else {
                                recepisse.setEmailAssujetti(null);
                                recepisse.setTelephoneAssujetti(null);
                            }

                        }

                        recepisse.setNomValidateur(depotDecl.getAgent().toString().toUpperCase().trim());
                        recepisse.setFonctionValidateur(depotDecl.getAgent().getFonction().getIntitule().toUpperCase());

                        if (depotDecl.getAgent().getSite() != null) {

                            if (depotDecl.getAgent().getSite().getAdresse() != null) {

                                recepisse.setLieuImpression(
                                        depotDecl.getAgent().getSite().getAdresse().getVille().getIntitule().toUpperCase());

                                recepisse.setAdresseSite(depotDecl.getAgent().getSite().getAdresse().toString().toUpperCase());

                            } else {
                                recepisse.setLieuImpression(GeneralConst.EMPTY_STRING);
                                recepisse.setAdresseSite(GeneralConst.EMPTY_STRING);
                            }

                        } else {
                            recepisse.setLieuImpression(GeneralConst.EMPTY_STRING);
                            recepisse.setAdresseSite(GeneralConst.EMPTY_STRING);
                        }

                        recepisse.setCodeDepotDeclaration(depotDecl.getCode());
                        recepisse.setNumRecepisse(depotDecl.getCode());
                        recepisse.setNumDepotDeclaration(depotDecl.getNumeroDepot().trim());
                        recepisse.setFaxAssujetti(GeneralConst.EMPTY_STRING);
                        recepisse.setNumImpot(GeneralConst.EMPTY_STRING);
                        recepisse.setSigle(GeneralConst.EMPTY_STRING);
                        recepisse.setBoitePostal(GeneralConst.EMPTY_STRING);
                        recepisse.setNumPage(GeneralConst.Number.ONE);
                        recepisse.setCodeDeclaration(depotDecl.getCode().trim());

                    }

                    dataReturn = printDocument.createRecepisse(recepisse, false);

                }

            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadArticleBudgetaire(HttpServletRequest request) {

        String valueSearch, typeSearch = GeneralConst.EMPTY_STRING;
        try {

            valueSearch = request.getParameter(DeclarationConst.ParamName.VALUE_SEACH);

            JsonObject jsonObject = new JsonObject();

            List<JsonObject> listJsonObjects = new ArrayList<>();

            listArticleBudgetaire = DeclarationBusiness.getListArticleBudgetaireAssujettisable(valueSearch.trim());

            if (listArticleBudgetaire != null && !listArticleBudgetaire.isEmpty()) {

                for (ArticleBudgetaire articleBudgetaire : listArticleBudgetaire) {

                    jsonObject = new JsonObject();

                    jsonObject.addProperty(TaxationConst.ParamName.CODE, articleBudgetaire.getCode().toUpperCase().trim());
                    jsonObject.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                            articleBudgetaire.getIntitule());
                    jsonObject.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                            articleBudgetaire.getCodeOfficiel());

                    listJsonObjects.add(jsonObject);
                }

            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

            return listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String getDepotByArticleBudgetaire(HttpServletRequest request) {

        String codeArticleBudg = GeneralConst.EMPTY_STRING;
        String periodicite = GeneralConst.EMPTY_STRING;
        String annee = GeneralConst.EMPTY_STRING;
        String mois = GeneralConst.EMPTY_STRING;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            codeArticleBudg = request.getParameter(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE);
            periodicite = request.getParameter(DeclarationConst.ParamName.CODE_PERIODICITE);
            annee = request.getParameter(DeclarationConst.ParamName.ANNEE);
            mois = request.getParameter(DeclarationConst.ParamName.MOIS);

            JsonObject jsonObject = new JsonObject();
            List<JsonObject> listJsonObjects = new ArrayList<>();
            Bordereau bordereau;

            listDepotDeclaration = new ArrayList<>();

            listDepotDeclaration = DeclarationBusiness.loadDepotDeclarationByArticleBudgetaire(codeArticleBudg,
                    periodicite, annee, mois);

            if (listDepotDeclaration != null && !listDepotDeclaration.isEmpty()) {

                for (DepotDeclaration depotDecl : listDepotDeclaration) {

                    jsonObject = new JsonObject();
                    Service service = new Service();
                    Personne personne = new Personne();
                    Adresse adresse = new Adresse();

                    jsonObject.addProperty(DeclarationConst.ParamName.CODE_DEPOT_DECLARATION,
                            depotDecl.getCode().trim());

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                            depotDecl.getNumeroDepot() != null ? depotDecl.getNumeroDepot().trim() : GeneralConst.EMPTY_STRING);

                    String dateCreate = ConvertDate.formatDateToStringOfFormat(depotDecl.getDateCreat(),
                            GeneralConst.Format.FORMAT_DATE);

                    if (dateCreate != null && !dateCreate.isEmpty()) {
                        jsonObject.addProperty(DeclarationConst.ParamName.DATE_CREATE,
                                dateCreate);
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.DATE_CREATE,
                                GeneralConst.EMPTY_STRING);
                    }

                    bordereau = DeclarationBusiness.getBordereauByCode(String.valueOf(depotDecl.getBordereau()));

                    if (bordereau != null) {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                bordereau.getTotalMontantPercu());
                    } else {
                        jsonObject.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                GeneralConst.Numeric.ZERO);
                    }

                    service = TaxationBusiness.getServiceByCode(depotDecl.getService().getCode().trim());

                    jsonObject.addProperty(TaxationConst.ParamName.CODE_SERVICE,
                            depotDecl.getService().getCode().trim());
                    if (service != null) {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                service.getIntitule().toUpperCase().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_SERVICE,
                                GeneralConst.EMPTY_STRING);
                    }

                    jsonObject.addProperty(DeclarationConst.ParamName.NUMERO_BORDEREAU,
                            depotDecl.getBordereau());

                    jsonObject.addProperty(DeclarationConst.ParamName.NOMBRE_IMPRESSION,
                            depotDecl.getNbrImpression());

                    jsonObject.addProperty(DeclarationConst.ParamName.ETAT,
                            depotDecl.getEtat());

                    personne = IdentificationBusiness.getPersonneByCode(depotDecl.getPersonne().getCode().trim());

                    if (personne != null) {

                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                personne.toString().toUpperCase().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getCode().trim());
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                personne.getFormeJuridique().getIntitule().toUpperCase().trim());

                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getAdresse().toString().toUpperCase().trim());
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                personne.toString().toUpperCase());
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                personne.getAdressePersonneList().get(0).getCode().trim());
                    } else {
                        jsonObject.addProperty(TaxationConst.ParamName.NOM_ASSUJETTI,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.CODE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(TaxationConst.ParamName.LIBELLE_FORME_JURIDIQUE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.NOM_COMPLET,
                                GeneralConst.EMPTY_STRING);
                        jsonObject.addProperty(DeclarationConst.ParamName.CODE_ADRESSE_PERSONNE,
                                GeneralConst.EMPTY_STRING);
                    }

                    if (depotDecl.getDetailDepotDeclarationList().size() > 0) {

                        List<JsonObject> listDetailDepotDeclJson = new ArrayList<>();
                        ArticleBudgetaire article;
                        DetailBordereau detailBordereau;

                        for (DetailDepotDeclaration dtldepotDecl : depotDecl.getDetailDepotDeclarationList()) {

                            detailDepotDeclJson = new JsonObject();

                            article = DeclarationBusiness.getArticleBudgetaireByCode(dtldepotDecl.getArticleBudgetaire().getCode().trim());

                            if (article != null) {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                        article.getCode());
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                        article.getIntitule());
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_OFFICIEL,
                                        article.getCodeOfficiel());
                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.NUMERO_DEPOT_DECLARATION,
                                    dtldepotDecl.getDepotDeclaration().getNumeroDepot() != null
                                            ? dtldepotDecl.getDepotDeclaration().getNumeroDepot().trim()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.TAUX,
                                    dtldepotDecl.getTaux() != null ? dtldepotDecl.getTaux()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.QUANTITE,
                                    dtldepotDecl.getQuantite() != null ? dtldepotDecl.getQuantite()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.VALEUR_BASE,
                                    dtldepotDecl.getValeurBase() != null ? dtldepotDecl.getValeurBase()
                                            : GeneralConst.EMPTY_ZERO);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.DEVISE,
                                    dtldepotDecl.getDevise().getCode() != null
                                            ? dtldepotDecl.getDevise().getCode()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ETAT,
                                    dtldepotDecl.getEtat());
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.CODE_TARIF,
                                    dtldepotDecl.getTarif().getCode() != null
                                            ? dtldepotDecl.getTarif().getCode()
                                            : GeneralConst.EMPTY_STRING);
                            detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_TARIF,
                                    dtldepotDecl.getTarif().getIntitule() != null
                                            ? dtldepotDecl.getTarif().getIntitule()
                                            : GeneralConst.EMPTY_STRING);
                            if (dtldepotDecl.getFkPeriode() != null) {

                                PeriodeDeclaration periodeDeclarat = DeclarationBusiness.getPeriodeDeclarationById(dtldepotDecl.getFkPeriode().trim());
                                Assujeti assujeti = DeclarationBusiness.getAssujettissementByPeriode(dtldepotDecl.getFkPeriode().trim());

                                if (periodeDeclarat != null) {

                                    if (periodeDeclarat.getDebut() != null) {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                                Tools.getPeriodeIntitule(periodeDeclarat.getDebut(),
                                                        periodeDeclarat.getAssujetissement().getArticleBudgetaire().getPeriodicite().getCode().trim()));
                                    } else {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                                GeneralConst.EMPTY_STRING);
                                    }

                                } else {
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                            GeneralConst.EMPTY_STRING);
                                }

                                if (assujeti != null) {
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                            assujeti.getBien().getIntitule().concat(GeneralConst.SPACE).concat(GeneralConst.BRAKET_OPEN).concat(assujeti.getBien().getTypeBien().getIntitule().concat(GeneralConst.BRAKET_CLOSE)));

                                    if (assujeti.getBien().getFkAdressePersonne() != null) {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                assujeti.getBien().getFkAdressePersonne().getAdresse().toString());
                                    } else {
                                        detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                                GeneralConst.EMPTY_STRING);
                                    }

                                } else {
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                            GeneralConst.EMPTY_STRING);
                                    detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                            GeneralConst.EMPTY_STRING);
                                }

                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_PERIODICITE,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.INTITULE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.ADRESSE_BIEN,
                                        GeneralConst.EMPTY_STRING);
                            }

                            detailBordereau = DeclarationBusiness.getDetailBordereauByPeriodicite(dtldepotDecl.getFkPeriode());

                            if (detailBordereau != null) {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                        detailBordereau.getMontantPercu());
                            } else {
                                detailDepotDeclJson.addProperty(DeclarationConst.ParamName.MONTANT_PERCU,
                                        GeneralConst.Numeric.ZERO);
                            }

                            jsonObject.addProperty(DeclarationConst.ParamName.DEVISE,
                                    dtldepotDecl.getDevise().getCode() != null
                                            ? dtldepotDecl.getDevise().getCode()
                                            : GeneralConst.EMPTY_STRING);

                            listDetailDepotDeclJson.add(detailDepotDeclJson);
                        }

                        jsonObject.addProperty(DeclarationConst.ParamName.LIST_DETAIL_DEPOT_DECLARATION,
                                listDetailDepotDeclJson.toString());
                    }

                    listJsonObjects.add(jsonObject);

                }

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

            dataReturn = listJsonObjects.toString();

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String loadArticlesBudgetairesAssujettissable(HttpServletRequest request) {

        List<JSONObject> jsonArticlesBudgetaires = new ArrayList<>();
        List<ArticleBudgetaire> articleBudgetaires;

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            //String codeService = request.getParameter(TaxationConst.ParamName.CODE_SERVICE);
            //codeService = "00048";
            //articleBudgetaires = DeclarationBusiness.getArticlesBudgetairesAssujettissableByService(codeService);
            articleBudgetaires = DeclarationBusiness.getArticlesBudgetairesAssujettissableImpot(
                    propertiesConfig.getProperty("CODE_AB_IMPOT_LIST"));

            if (!articleBudgetaires.isEmpty()) {

                for (ArticleBudgetaire articleBudgetaire : articleBudgetaires) {

                    JSONObject jsonArticleBudgetaire = new JSONObject();

                    jsonArticleBudgetaire.put(DeclarationConst.ParamName.CODE_ARTICLE_BUDGETAIRE,
                            articleBudgetaire.getCode() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getCode());
                    jsonArticleBudgetaire.put(DeclarationConst.ParamName.LIBELLE_ARTICLE_BUDGETAIRE,
                            articleBudgetaire.getIntitule() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getIntitule());

                    jsonArticleBudgetaire.put(DeclarationConst.ParamName.CODE_PERIODICITE,
                            articleBudgetaire.getPeriodicite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getPeriodicite().getCode());

                    jsonArticleBudgetaire.put(DeclarationConst.ParamName.PERIODICITE,
                            articleBudgetaire.getPeriodicite() == null
                                    ? GeneralConst.EMPTY_STRING
                                    : articleBudgetaire.getPeriodicite().getIntitule());

                    jsonArticlesBudgetaires.add(jsonArticleBudgetaire);
                }

                dataReturn = jsonArticlesBudgetaires.toString();
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;

    }

    public String reprintRecepisse(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        RecepissePrint recepisse = new RecepissePrint();
        PrintDocument printDocument = new PrintDocument();

        try {

            String declaration = request.getParameter(DeclarationConst.ParamName.CODE_DEPOT_DECLARATION);

            if (declaration != null) {

                Archive archive = getArchiveByRefDocument(declaration);

                if (archive != null) {

                    recepisse.setNumDepotDeclaration(archive.getDossier().trim());

                    dataReturn = printDocument.createRecepisse(recepisse, true);

                }
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return dataReturn;
    }

    public String saveAndPrintInvitationApayer(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        String codeArticleBudgetaire = GeneralConst.EMPTY_STRING;
        String libelleArticleBudgetaire = GeneralConst.EMPTY_STRING;
        String assujettiCode = GeneralConst.EMPTY_STRING;
        String assujettiName = GeneralConst.EMPTY_STRING;
        String periodeDeclaration = GeneralConst.EMPTY_STRING;
        String echeanceDeclaration = GeneralConst.EMPTY_STRING;
        String adresseCode = GeneralConst.EMPTY_STRING;
        String adresseName = GeneralConst.EMPTY_STRING;
        String agentCreat = GeneralConst.EMPTY_STRING;
        String periodeDeclarationId = GeneralConst.EMPTY_STRING;
        String printExist = GeneralConst.EMPTY_STRING;
        String numeroMed = GeneralConst.EMPTY_STRING;

        try {

            agentCreat = request.getParameter(RecouvrementConst.ParamName.USER_ID);
            String id = request.getParameter("id");

            RetraitDeclaration retraitDeclaration = DeclarationBusiness.getRetraiteclarationByID(Integer.valueOf(id));

            BigDecimal amountRD = new BigDecimal("0");

            if (retraitDeclaration.getNewId() != null) {

                List<RetraitDeclaration> retraitDeclarations = DeclarationBusiness.getListRetraitDeclarationByNewID_V2(
                        retraitDeclaration.getNewId());

                if (!retraitDeclarations.isEmpty()) {

                    for (RetraitDeclaration rd : retraitDeclarations) {
                        amountRD = amountRD.add(rd.getMontant());
                    }
                }

            } else {
                amountRD = amountRD.add(retraitDeclaration.getMontant());
            }

            ArticleBudgetaire articleBudgetaire = DeclarationBusiness.getArticleBudgetaireByCode(retraitDeclaration.getFkAb());

            PeriodeDeclaration pd = null;
            Med med = null;

            if (retraitDeclaration != null) {

                codeArticleBudgetaire = retraitDeclaration.getFkAb();
                libelleArticleBudgetaire = articleBudgetaire.getIntitule().toUpperCase();

                med = RecouvrementBusiness.getMedByPerideDeclaration(Integer.valueOf(retraitDeclaration.getFkPeriode()));

                printExist = med == null ? GeneralConst.Number.ZERO : GeneralConst.Number.ONE;

                assujettiCode = retraitDeclaration.getFkAssujetti();
                periodeDeclarationId = retraitDeclaration.getFkPeriode();

                pd = DeclarationBusiness.getPeriodeDeclarationById(periodeDeclarationId);

                periodeDeclaration = Tools.getPeriodeIntitule(pd.getDebut(), articleBudgetaire.getPeriodicite().getCode());

            }

            Personne personne = IdentificationBusiness.getPersonneByCode(retraitDeclaration.getFkAssujetti());

            if (personne != null) {
                assujettiName = personne.toString().toUpperCase();

                Adresse adresse = TaxationBusiness.getAdresseDefaultByAssujetti(personne.getCode());

                adresseCode = adresse == null ? GeneralConst.EMPTY_STRING : adresse.getId();
                adresseName = adresse == null ? GeneralConst.EMPTY_STRING : adresse.toString().toUpperCase();
            }

            switch (printExist) {

                case GeneralConst.Number.ZERO:

                    if (RecouvrementBusiness.saveMed(
                            codeArticleBudgetaire,
                            assujettiCode,
                            periodeDeclaration,
                            echeanceDeclaration,
                            adresseCode, agentCreat,
                            amountRD.floatValue(),
                            Integer.valueOf(periodeDeclarationId), "INVITATION_PAIEMENT",
                            GeneralConst.EMPTY_STRING, GeneralConst.EMPTY_STRING, 0)) {

                        med = RecouvrementBusiness.getMedByPerideDeclaration(Integer.valueOf(periodeDeclarationId));

                        if (med != null) {

                            PrintDocument printDocument = new PrintDocument();

                            ArticleBudgetaire articleBudgetaire1 = DeclarationBusiness.getArticleBudgetaireByCode(retraitDeclaration.getFkAb());

                            dataReturn = printDocument.createInvitationApayer(med, Integer.valueOf(agentCreat), retraitDeclaration.getId() + "",
                                    ConvertDate.formatDateToStringV2(retraitDeclaration.getDateCreate()),
                                    retraitDeclaration.getDateEcheancePaiement(), retraitDeclaration.getDevise(),
                                    articleBudgetaire1.getIntitule().toUpperCase());

                        } else {
                            dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                        }

                    } else {
                        dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
                    }
                    break;
                case GeneralConst.Number.ONE:

                    Archive archive = NotePerceptionBusiness.getArchiveByRefDocument(numeroMed);

                    if (archive != null) {
                        dataReturn = archive.getDocumentString();
                    } else {
                        dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
                    }
                    break;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String controleTaxation(HttpServletRequest request) {

        String avisControle = request.getParameter(TaxationConst.ParamName.ID_AVIS);
        String codeDeclaration = request.getParameter(TaxationConst.ParamName.NUMERO_DEPOT);
        String observation = request.getParameter(TaxationConst.ParamName.OBSERVATION_ORDONNANCEMENT);

        try {
            boolean result = NotePerceptionBusiness.updateRetraiteDeclaration(avisControle, Integer.valueOf(codeDeclaration), observation);

            if (result) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }
        } catch (NumberFormatException e) {
            CustumException.LogException(e);
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

    public String loadAvis() {

        List<Avis> listAvis = TaxationBusiness.getListAllAvis();

        List<JSONObject> listAVisJson = new ArrayList<>();

        try {
            if (!listAvis.isEmpty()) {
                for (Avis avis : listAvis) {

                    JSONObject jsonAvisObject = new JSONObject();

                    jsonAvisObject.put(TaxationConst.ParamName.ID_AVIS, avis.getId().trim());
                    jsonAvisObject.put(TaxationConst.ParamName.CODE_AVIS, avis.getCode().trim());
                    jsonAvisObject.put(TaxationConst.ParamName.LIBELLE_AVIS, avis.getIntitule().trim());

                    listAVisJson.add(jsonAvisObject);

                }
            } else {
                return GeneralConst.EMPTY_STRING;
            }

        } catch (JSONException ex) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
        return listAVisJson.toString();

    }

}
