package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Procedure.class)
public class Procedure_ { 

    public static volatile SingularAttribute<Procedure, String> agentMaj;
    public static volatile SingularAttribute<Procedure, String> code;
    public static volatile SingularAttribute<Procedure, Date> dateCreat;
    public static volatile SingularAttribute<Procedure, Short> enCreation;
    public static volatile SingularAttribute<Procedure, String> agentCreat;
    public static volatile SingularAttribute<Procedure, String> intitule;
    public static volatile SingularAttribute<Procedure, Short> etat;
    public static volatile SingularAttribute<Procedure, Date> dateMaj;

}