package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Bordereau;
import cd.hologram.erecettesvg.models.FichePriseCharge;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Etat.class)
public class Etat_ { 

    public static volatile SingularAttribute<Etat, Integer> code;
    public static volatile ListAttribute<Etat, Bordereau> bordereauList;
    public static volatile SingularAttribute<Etat, String> libelleEtat;
    public static volatile ListAttribute<Etat, FichePriseCharge> fichePriseChargeList;

}