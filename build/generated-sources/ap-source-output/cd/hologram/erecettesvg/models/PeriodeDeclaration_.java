package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.DetailsNc;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(PeriodeDeclaration.class)
public class PeriodeDeclaration_ { 

    public static volatile SingularAttribute<PeriodeDeclaration, Integer> agentMaj;
    public static volatile SingularAttribute<PeriodeDeclaration, String> observation;
    public static volatile SingularAttribute<PeriodeDeclaration, String> amr;
    public static volatile SingularAttribute<PeriodeDeclaration, String> noteCalcul;
    public static volatile SingularAttribute<PeriodeDeclaration, Date> fin;
    public static volatile SingularAttribute<PeriodeDeclaration, Short> etat;
    public static volatile SingularAttribute<PeriodeDeclaration, Date> dateMaj;
    public static volatile SingularAttribute<PeriodeDeclaration, Date> debut;
    public static volatile ListAttribute<PeriodeDeclaration, DetailsNc> detailsNcList;
    public static volatile SingularAttribute<PeriodeDeclaration, Assujeti> assujetissement;
    public static volatile SingularAttribute<PeriodeDeclaration, Date> dateLimitePaiement;
    public static volatile SingularAttribute<PeriodeDeclaration, Integer> id;
    public static volatile SingularAttribute<PeriodeDeclaration, String> agentCreat;
    public static volatile SingularAttribute<PeriodeDeclaration, Date> dateLimite;

}