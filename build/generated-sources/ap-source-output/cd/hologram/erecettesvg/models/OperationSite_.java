package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(OperationSite.class)
public class OperationSite_ { 

    public static volatile SingularAttribute<OperationSite, Date> dateOuvertureOperation;
    public static volatile SingularAttribute<OperationSite, Date> dateFermetureOperation;
    public static volatile SingularAttribute<OperationSite, String> fkSite;
    public static volatile SingularAttribute<OperationSite, Integer> fkAgent;
    public static volatile SingularAttribute<OperationSite, Integer> id;
    public static volatile SingularAttribute<OperationSite, Integer> etat;

}