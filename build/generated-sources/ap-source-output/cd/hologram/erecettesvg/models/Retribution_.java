package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.CategorieRepartition;
import cd.hologram.erecettesvg.models.Palier;
import cd.hologram.erecettesvg.models.Repartition;
import cd.hologram.erecettesvg.models.Service;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Retribution.class)
public class Retribution_ { 

    public static volatile SingularAttribute<Retribution, CategorieRepartition> categorieRepartition;
    public static volatile SingularAttribute<Retribution, String> code;
    public static volatile SingularAttribute<Retribution, Palier> palier;
    public static volatile SingularAttribute<Retribution, BigDecimal> valeur;
    public static volatile SingularAttribute<Retribution, Service> service;
    public static volatile SingularAttribute<Retribution, String> type;
    public static volatile SingularAttribute<Retribution, Boolean> etat;
    public static volatile ListAttribute<Retribution, Repartition> repartitionList;

}