/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_JOUR_FERIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "JourFerie.findAll", query = "SELECT j FROM JourFerie j"),
    @NamedQuery(name = "JourFerie.findById", query = "SELECT j FROM JourFerie j WHERE j.id = :id"),
    @NamedQuery(name = "JourFerie.findByDateJourFerie", query = "SELECT j FROM JourFerie j WHERE j.dateJourFerie = :dateJourFerie"),
    @NamedQuery(name = "JourFerie.findByDescription", query = "SELECT j FROM JourFerie j WHERE j.description = :description"),
    @NamedQuery(name = "JourFerie.findByType", query = "SELECT j FROM JourFerie j WHERE j.type = :type"),
    @NamedQuery(name = "JourFerie.findByEstActif", query = "SELECT j FROM JourFerie j WHERE j.estActif = :estActif")})
public class JourFerie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATE_JOUR_FERIE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateJourFerie;
    @Size(max = 50)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "TYPE")
    private String type;
    @Column(name = "EST_ACTIF")
    private Boolean estActif;

    public JourFerie() {
    }

    public JourFerie(Integer id) {
        this.id = id;
    }

    public JourFerie(Integer id, Date dateJourFerie, String type) {
        this.id = id;
        this.dateJourFerie = dateJourFerie;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateJourFerie() {
        return dateJourFerie;
    }

    public void setDateJourFerie(Date dateJourFerie) {
        this.dateJourFerie = dateJourFerie;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getEstActif() {
        return estActif;
    }

    public void setEstActif(Boolean estActif) {
        this.estActif = estActif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof JourFerie)) {
            return false;
        }
        JourFerie other = (JourFerie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.models.JourFerie[ id=" + id + " ]";
    }
    
}
