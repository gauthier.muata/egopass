package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Dossier;
import cd.hologram.erecettesvg.models.Process;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Process.class)
public class Process_ { 

    public static volatile SingularAttribute<Process, String> agentMaj;
    public static volatile SingularAttribute<Process, String> process;
    public static volatile SingularAttribute<Process, Date> dateCreat;
    public static volatile SingularAttribute<Process, String> ordre;
    public static volatile SingularAttribute<Process, String> uaKo;
    public static volatile SingularAttribute<Process, String> observation;
    public static volatile SingularAttribute<Process, Dossier> dossier;
    public static volatile SingularAttribute<Process, String> description;
    public static volatile SingularAttribute<Process, BigDecimal> montant;
    public static volatile SingularAttribute<Process, String> ua;
    public static volatile SingularAttribute<Process, Short> controlSolde;
    public static volatile SingularAttribute<Process, Short> etat;
    public static volatile SingularAttribute<Process, Date> dateMaj;
    public static volatile SingularAttribute<Process, BigInteger> workFlow;
    public static volatile SingularAttribute<Process, Process> process1;
    public static volatile SingularAttribute<Process, Process> process2;
    public static volatile SingularAttribute<Process, Integer> duree;
    public static volatile SingularAttribute<Process, Long> id;
    public static volatile SingularAttribute<Process, String> agentCreat;
    public static volatile SingularAttribute<Process, String> uaOk;
    public static volatile SingularAttribute<Process, String> echeance;
    public static volatile SingularAttribute<Process, String> metier;

}