/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {

    mainNavigationLabel.text('PAIEMENT');
    secondNavigationLabel.text('Registre des paiements');

    removeActiveMenu();
    linkMenuPaiement.addClass('active');

    btnSimpleSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (inputResearchValue.val() === empty || inputResearchValue.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
        } else {
            jourrnalSearching(0, 1);
        }
    });
    btnAdvencedSearch.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        jourrnalSearching(0, 2);
    });

    loadJournalTable('', 0);
});