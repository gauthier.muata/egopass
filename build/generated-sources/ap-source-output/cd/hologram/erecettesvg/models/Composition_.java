package cd.hologram.erecettesvg.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Composition.class)
public class Composition_ { 

    public static volatile SingularAttribute<Composition, Boolean> permanant;
    public static volatile SingularAttribute<Composition, String> code;
    public static volatile SingularAttribute<Composition, Boolean> etat;
    public static volatile SingularAttribute<Composition, String> chambre;

}