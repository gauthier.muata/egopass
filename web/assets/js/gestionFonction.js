/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Initialisation des paramètres
var intitule = $('#intitule'),
        codeInput = $('input#code'),
        tableFonction = $('#tableFonction'),
        btnEdit = $('#tableFonction .btn-edit'),
        btnRemove = $('#tableFonction .btn-remove'),
        btnAdd = $('.btn-add-fonction'),
        modal = $('#myModal'),
        formulaire = $('.formulaire-edition-fonction'),
        fonctions;

var btnUpdateFonction;
var codeFonctionSelected;
var btnAddFonction;

(function ($) {

    btnUpdateFonction = $('#btnUpdateFonction');
    btnAddFonction = $('#btnAddFonction');

    $(document).ready(function () {
        // Recuperer la liste des fonctions
        recupererFonction();

        // Ouverture du modal d'ajout
        btnAdd.on('click', function (event) {
            event.preventDefault();
            modal.modal('show');
        });

        btnUpdateFonction.on('click', function (event) {
            event.preventDefault();

            if (intitule.val() == empty) {
                alertify.alert('Veuillez d\'abord saisir le libellé de la fonction');
                return;
            }

            alertify.confirm('Etes-vous de vouloir enregistrer cette fonction ?', function () {
                saveFonction();
            });

        });

        btnAddFonction.on('click', function (event) {
            event.preventDefault();

            intitule.val(empty);
            codeFonctionSelected = empty;
            modal.modal('show');
        });

        // Lors du submit du formulaire d'ajout
        formulaire.on('submit', function (event) {
            event.preventDefault();
            if (intitule.val() == '') {
                alertify.alert("Veuillez remplir le champ");
                return;
            }
            ajouterFonction(intitule.val(), codeInput.val());

            // reset form
            intitule.val('');

            modal.modal('hide');

        });

    });

})(jQuery)

function recupererFonction() {
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'listeFonction'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '-1') {

                showResponseError();
                loadDataTable(empty);
                return;

            } else if (response == '0') {

                alertify.alert('Il n\'y a aucune fonction disponible');
                loadDataTable(empty);
                return;
            } else {
                var data = JSON.parse(JSON.stringify(response));
                fonctions = data;
                loadDataTable(data);
            }



        },
        complete: function () {

        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function saveFonction() {

    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'Text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveFonctionUser',
            'libelle': intitule.val(),
            'code': codeFonctionSelected

        },
        beforeSend: function () {

            $('#myModal').block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1' || response == '0') {
                $('#myModal').unblock();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $('#myModal').unblock();

                if (response == '1') {
                    $('#myModal').modal('hide');
                    recupererFonction();
                    alertify.alert('L\'enregistrement s\'est effectué avec succès.');
                    
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $('#myModal').unblock();
            showResponseError();
        }
    });
}

// Cette fonction permet d'ajouter une fonction
function ajouterFonction(intitule, code) {
    var operation = 'ajoutFonction';
    if (code) {
        operation = 'modifierFonction';
    }
    if (operation == 'ajoutFonction') {
        var allFonctions = fonctions.map(function (item) {
            return (item.libelleFonction) ? item.libelleFonction.toLowerCase() : "";
        });
        if (allFonctions.indexOf(intitule.toLowerCase()) != -1) {
            alertify.alert('Cette fonction existe déjà');
            return;
        }
    }
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'intitule': intitule,
            'code': code,
            'operation': operation
        },
        beforeSend: function () {
            modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
        },
        success: function (response)
        {
            if (response == 1) {
                window.location.reload();
            } else {
                alert.alertify("L'opération a échoué");
            }
        },
        complete: function () {

        },
        error: function () {
            showResponseError();
        }

    });
}
// Lors de la modification d'une fonction
function modifierFonction(code, intituleFonction) {
    $('#myModal .modal-title').html('Modifier une fonction');
    intitule.val(intituleFonction);
    codeInput.val(code);
    codeFonctionSelected = code;
    $('#myModal').modal('show');
}

// Initilaisation de la liste des fonctions
function loadDataTable(result) {
    var tr = '';
    $.each(result, function (index, item, array) {
        tr += '<tr>';
        tr += '<td>' + item.libelleFonction.toUpperCase() + '</td>';
        if (item.etat) {
            tr += '<td class="text-center"><span class="label label-success">Activé</span></td>';
            tr += '<td class="text-center pr-20"><button class="btn btn-success btn-edit" onclick="modifierFonction(\'' + item.codeFonction + '\',\'' + item.libelleFonction.toUpperCase() + '\')"><i class="fa fa-edit"></i> </button> <button class="btn btn-danger btn-remove" onclick="supprimerFonction(\'' + item.codeFonction + '\')"><i class="fa fa-close"></i></button></td>';
        } else {
            tr += '<td class="text-center"><span class="label label-danger">Désactivé</span></td>';
            tr += '<td class="text-center pr-20"><button class="btn btn-success btn-edit" onclick="modifierFonction(\'' + item.codeFonction + '\',\'' + item.libelleFonction.toUpperCase() + '\')"><i class="fa fa-edit"></i> </button> <button class="btn btn-primary btn-activate" onclick="activerFonction(\'' + item.codeFonction + '\')"><i class="fa fa-check"></i></button></td>';
        }
        tr += '</tr>';
    });
    $('#tableFonction').find('tbody').html(tr);

    $('#tableFonction').DataTable({
        "order": [[1, "asc"]],
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
}

// Supprimer une fonction 
function supprimerFonction(code) {
    alertify.confirm(
            "Désactivation de la fonction",
            "Etes vous sûre de vouloir désactiver cette fonction",
            function () {
                desactiverFonction(code);
                //alertify.success('Success'); 
            }
    , function () {
        alertify.error('Cancel')
    }
    );
}

// Désactiver une fonction 
function desactiverFonction(code) {
    var operation = "desactiverFonction";
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'code': code,
            'operation': operation
        },
        beforeSend: function () {
            modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
        },
        success: function (response)
        {
            if (response == 1) {
                window.location.reload();
            } else {
                alert.alertify("L'opération a échoué");
            }
        },
        complete: function () {

        },
        error: function () {
            showResponseError();
        }

    });
}

function activerFonction(code) {
    alertify.confirm(
            "Activation de la fonction",
            "Etes vous sûre de vouloir activer cette fonction",
            function () {
                activerlaFonction(code);
            }
    , function () {
        alertify.error('Cancel')
    }
    );
}

function activerlaFonction(code) {
    var operation = "activerFonction";
    $.ajax({
        type: 'POST',
        url: 'utilisateur_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'code': code,
            'operation': operation
        },
        beforeSend: function () {
            modalUpdateUserPassword.block({message: '<img src="assets/images/loading.gif" /> <h5 style="color:#0085c7">Modification du mot de passe...</h5>'});
        },
        success: function (response)
        {
            if (response == 1) {
                window.location.reload();
            } else {
                alert.alertify("L'opération a échoué");
            }
        },
        complete: function () {

        },
        error: function () {
            showResponseError();
        }

    });
}