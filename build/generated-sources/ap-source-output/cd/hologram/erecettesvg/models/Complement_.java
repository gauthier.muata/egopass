package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ComplementForme;
import cd.hologram.erecettesvg.models.Personne;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Complement.class)
public class Complement_ { 

    public static volatile SingularAttribute<Complement, String> valeur;
    public static volatile SingularAttribute<Complement, Personne> personne;
    public static volatile SingularAttribute<Complement, ComplementForme> fkComplementForme;
    public static volatile SingularAttribute<Complement, String> id;
    public static volatile SingularAttribute<Complement, Boolean> etat;

}