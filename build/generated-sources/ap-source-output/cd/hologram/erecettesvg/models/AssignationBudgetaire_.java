package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AssignationBudgetaire.class)
public class AssignationBudgetaire_ { 

    public static volatile SingularAttribute<AssignationBudgetaire, Integer> agentCreate;
    public static volatile SingularAttribute<AssignationBudgetaire, String> fkExercice;
    public static volatile SingularAttribute<AssignationBudgetaire, String> fkDevise;
    public static volatile SingularAttribute<AssignationBudgetaire, Double> montantReel;
    public static volatile SingularAttribute<AssignationBudgetaire, String> archive;
    public static volatile SingularAttribute<AssignationBudgetaire, Integer> id;
    public static volatile SingularAttribute<AssignationBudgetaire, Date> dateCreate;
    public static volatile SingularAttribute<AssignationBudgetaire, Integer> etat;
    public static volatile SingularAttribute<AssignationBudgetaire, Double> montantGlobal;

}