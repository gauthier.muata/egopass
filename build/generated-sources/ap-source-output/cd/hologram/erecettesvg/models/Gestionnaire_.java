package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Personne;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Gestionnaire.class)
public class Gestionnaire_ { 

    public static volatile SingularAttribute<Gestionnaire, Agent> agent;
    public static volatile SingularAttribute<Gestionnaire, Integer> code;
    public static volatile SingularAttribute<Gestionnaire, Date> dateCreat;
    public static volatile SingularAttribute<Gestionnaire, Personne> personne;
    public static volatile SingularAttribute<Gestionnaire, Short> etat;

}