package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.TypeComplementBien;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ComplementBien.class)
public class ComplementBien_ { 

    public static volatile SingularAttribute<ComplementBien, String> valeur;
    public static volatile SingularAttribute<ComplementBien, String> id;
    public static volatile SingularAttribute<ComplementBien, TypeComplementBien> typeComplement;
    public static volatile SingularAttribute<ComplementBien, Boolean> etat;
    public static volatile SingularAttribute<ComplementBien, String> devise;
    public static volatile SingularAttribute<ComplementBien, Bien> bien;

}