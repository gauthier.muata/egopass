/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.AssujettissementBusiness.propertiesConfig;
import static cd.hologram.erecettesvg.business.IdentificationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.constants.IdentificationConst;
import cd.hologram.erecettesvg.dao.Dao;
import cd.hologram.erecettesvg.models.AccesUser;
import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Division;
import cd.hologram.erecettesvg.models.Droit;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.Fonction;
import cd.hologram.erecettesvg.models.Gestionnaire;
import cd.hologram.erecettesvg.models.GroupUser;
import cd.hologram.erecettesvg.models.LoginWeb;
import cd.hologram.erecettesvg.models.Module;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Service;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.SitePeageUser;
import cd.hologram.erecettesvg.models.Ua;
import cd.hologram.erecettesvg.sql.SQLQueryIdentification;
import cd.hologram.erecettesvg.util.Casting;
import java.util.ArrayList;
import java.util.Date;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author juslin.tshiamua
 */
public class GestionUtilisateurBusiness {

    private static final Dao myDao = new Dao();

    public static List<Fonction> getFonctionList(String codeFonction) {
        try {

            String query = GeneralConst.EMPTY_STRING;
            List<Fonction> fonctions = new ArrayList<>();

            if (codeFonction.equals(propertiesConfig.getProperty("CODE_FONCTION_ADMIN_PEAGE"))) {

                query = "SELECT * FROM T_FONCTION WITH (READPAST) WHERE %s AND ETAT = 1 ORDER BY INTITULE";
                query = String.format(query, propertiesConfig.getProperty("CODE_LIST_FONCTION_USER_PEAGE"));

                fonctions = (List<Fonction>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Fonction.class), false);

            } else if (codeFonction.equals(propertiesConfig.getProperty("CODE_FONCTION_ADMIN_BANK"))) {

                query = "SELECT * FROM T_FONCTION WITH (READPAST) WHERE  CODE = '%s' AND ETAT = 1 ORDER BY INTITULE";
                query = String.format(query, propertiesConfig.getProperty("CODE_LIST_FONCTION_USER_BANK"));

                fonctions = (List<Fonction>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Fonction.class), false);

            } else {

                query = "SELECT * FROM T_FONCTION WITH (READPAST) WHERE NOT %s AND ETAT = 1 ORDER BY INTITULE";
                query = String.format(query, propertiesConfig.getProperty("CODE_LIST_FONCTION_USER_PEAGE2"));

                fonctions = (List<Fonction>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Fonction.class), false);

                /*query = SQLQueryIdentification.SELECT_FONCTION;
                 fonctions = (List<Fonction>) myDao.getDaoImpl().find(query,
                 Casting.getInstance().convertIntoClassType(Fonction.class), false);*/
            }

            return fonctions;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Fonction> getFonctionList2() {
        try {

            String query = GeneralConst.EMPTY_STRING;

            query = SQLQueryIdentification.SELECT_FONCTION;
            List<Fonction> fonctions = (List<Fonction>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Fonction.class), false);

            return fonctions;

        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean createFonction(Fonction fonction) {
        boolean result;
        if (fonction.getCode() == null) {
            String query = SQLQueryIdentification.F_NEW_FONCTION;
            result = myDao.getDaoImpl().executeStoredProcedure(
                    query, fonction.getIntitule());
        } else {
            String query = SQLQueryIdentification.EDIT_FONCTION;
            result = myDao.getDaoImpl().executeStoredProcedure(
                    query, fonction.getIntitule(), fonction.getCode());
        }

        return result;
    }

    public static boolean desableFonction(Fonction fonction) {

        boolean result;

        String query = SQLQueryIdentification.DESACTIVER_FONCTION;
        result = myDao.getDaoImpl().executeStoredProcedure(
                query, fonction.getCode());

        return result;
    }

    public static boolean enableFonction(Fonction fonction) {
        boolean result;

        String query = SQLQueryIdentification.ACTIVER_FONCTION;
        result = myDao.getDaoImpl().executeStoredProcedure(
                query, fonction.getCode());

        return result;
    }

    public static List<Site> getSiteList(String codeFonction) {
        try {

            String query = GeneralConst.EMPTY_STRING;
            List<Site> sites = new ArrayList<>();

            if (codeFonction.equals(propertiesConfig.getProperty("CODE_FONCTION_ADMIN_PEAGE"))) {

                query = "SELECT * FROM T_SITE WITH (READPAST) WHERE ETAT = 1 and PEAGE = 1 ORDER BY INTITULE";
                sites = (List<Site>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Site.class), false);

            } else {
                query = "SELECT * FROM T_SITE WITH (READPAST) WHERE ETAT = 1 and PEAGE = 0 AND CODE NOT IN ('*') ORDER BY INTITULE";
                sites = (List<Site>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Site.class), false);
            }

            return sites;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Site> getSitePeageList() {
        try {

            String query = SQLQueryIdentification.SELECT_SITE_PEAGE;
            List<Site> sites = (List<Site>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Site.class), false);
            return sites;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<EntiteAdministrative> getDistrinctList() {
        try {
            String query = SQLQueryIdentification.SELECT_DISTRINCT;
            List<EntiteAdministrative> provinces = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false);
            return provinces;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Service> getServiceList(String codeService) {
        try {

            String query = GeneralConst.EMPTY_STRING;
            List<Service> services = new ArrayList<>();

            if (codeService.equals(propertiesConfig.getProperty("CODE_SERVICE_TRANSPORT"))) {

                query = "SELECT * FROM T_SERVICE WITH(READPAST) WHERE ETAT = 1 and code = ?1";
                services = (List<Service>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Service.class), false, codeService);

            } else {
                query = "SELECT * FROM T_SERVICE WITH(READPAST) WHERE ETAT = 1 and code NOT IN ('*') ORDER BY INTITULE";
                services = (List<Service>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Service.class), false, codeService);
            }

            return services;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Division> getListDivisionByCode(String codeDivision) {
        try {

            String query = GeneralConst.EMPTY_STRING;
            List<Division> divisions = new ArrayList<>();

            query = "SELECT * FROM T_DIVISION WHERE ETAT = 1 AND CODE = ?1";
            divisions = (List<Division>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Division.class), false, codeDivision);

            return divisions;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Division> getDivisionList(String codeDivision) {
        try {

            String query = GeneralConst.EMPTY_STRING;
            List<Division> divisions = new ArrayList<>();

            if (codeDivision.equals(propertiesConfig.getProperty("CODE_DIVISION_PEAGE"))) {

                query = "SELECT * FROM T_DIVISION WHERE ETAT = 1 AND CODE = ?1";
                divisions = (List<Division>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Division.class), false, codeDivision);

            } else {
                query = SQLQueryIdentification.SELECT_DIVISION;
                divisions = (List<Division>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Division.class), false);
            }

            return divisions;

        } catch (Exception e) {
            throw e;
        }
    }

    public static GroupUser getGroupe(String code) {
        try {

            String query = SQLQueryIdentification.SELECT_SINGLE_GROUPE;

            List<GroupUser> groups = (List<GroupUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(GroupUser.class), false, code);

            GroupUser group = groups.isEmpty() ? null : groups.get(0);

            return group;

        } catch (Exception e) {
            throw e;
        }
    }

    public static SitePeageUser getSitePeageUserByUser(int userId) {
        try {

            String query = "SELECT * from T_SITE_PEAGE_USER WITH (READPAST) WHERE FK_AGENT = ?1 AND ETAT = 1";

            List<SitePeageUser> sitePeageUsers = (List<SitePeageUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(SitePeageUser.class), false, userId);

            SitePeageUser sitePeageUser = sitePeageUsers.isEmpty() ? null : sitePeageUsers.get(0);

            return sitePeageUser;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Droit> getAllDroits(String code) {
        try {

            String query = SQLQueryIdentification.SELECT_DROITS;
            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, code);
            return droits;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Agent> getAllMembres(String code) {
        try {

            String query = SQLQueryIdentification.SELECT_AGENTS_BY_GROUPE;
            List<Agent> membres = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, code);
            return membres;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Ua> getUaList() {
        try {
            String query = SQLQueryIdentification.SELECT_UA;
            List<Ua> uadm = (List<Ua>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Ua.class), false);
            return uadm;

        } catch (Exception e) {
            throw e;
        }
    }

    public static Agent checkLogin(String login) {
        try {
            String query = SQLQueryIdentification.CHECK_LOGIN;
            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, login);
            return agents.isEmpty() ? null : agents.get(0);
        } catch (Exception e) {
            throw e;
        }
    }

    public static List<GroupUser> getGroupList() {
        try {

            String query = SQLQueryIdentification.SELECT_GROUPE;
            List<GroupUser> groups = (List<GroupUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(GroupUser.class), false);
            return groups;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Droit> getAllOtherDroits(String code) {
        try {

            String query = SQLQueryIdentification.SELECT_AUTRES_DROITS;
            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, code);
            return droits;

        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean editGroupe(GroupUser groupe) {
        boolean result;

        // Si l'id est non défini nous procédons à l'ajout d'un groupe
        // Sinon nous passons à la modification
        if (groupe.getId() == null) {
            String query = SQLQueryIdentification.AJOUT_GROUPE;
            result = myDao.getDaoImpl()
                    .executeStoredProcedure(
                            query,
                            groupe.getIntitule(),
                            groupe.getAgentCreat(),
                            groupe.getDateCreat(),
                            groupe.getEtat(),
                            groupe.getDescription()
                    );

        } else {
            String query = SQLQueryIdentification.EDIT_GROUPE;
            result = myDao.getDaoImpl()
                    .executeStoredProcedure(
                            query,
                            groupe.getIntitule(),
                            groupe.getAgentMaj(),
                            groupe.getDateMaj(),
                            groupe.getDescription(),
                            groupe.getId()
                    );
        }

        return result;
    }

    public static Integer countMembersByGroup(String code) {
        Integer count;

        String query = SQLQueryIdentification.COUNT_ROW_MEMBER_BY_GROUPE;
        String number = myDao.getDaoImpl().getSingleResultToString(query, code);

        count = Integer.parseInt(number);

        return count;
    }

    public static String enableDisableGroupe(String code, String operation) {

        String result;
        boolean res;

        if (operation.equals(GeneralConst.ParamName.ACTIVER_GROUPE)) {
            String query = SQLQueryIdentification.UPDATE_ACTIVER_GROUPE;
            res = myDao.getDaoImpl().executeStoredProcedure(query, Integer.parseInt(code));
            result = (res) ? GeneralConst.ResultCode.SUCCES_OPERATION : GeneralConst.ResultCode.FAILED_OPERATION;
        } else {
            if (countMembersByGroup(code) > 0) {
                result = GeneralConst.ResultCode.LOGIN_EMAIL_EXIST;
            } else {
                String query = SQLQueryIdentification.UPDATE_DESACTIVER_GROUPE;
                res = myDao.getDaoImpl().executeStoredProcedure(query, code);
                result = (res) ? GeneralConst.ResultCode.SUCCES_OPERATION : GeneralConst.ResultCode.FAILED_OPERATION;
            }

        }

        return result;
    }

    public static boolean insertToGroupe(String code, String droits, String user_id) {

        boolean result = false;

        try {

            String[] droitArray = droits.split(",");

            int counter = 0;
            HashMap<String, Object[]> bulkQuery = new HashMap<>();
            Date currentDate = new Date();
            for (int i = 0; i < droitArray.length; i++) {
                String droit = droitArray[i];
                bulkQuery.put(counter + SQLQueryIdentification.INSERT_GROUPE_USER, new Object[]{
                    code,
                    droit,
                    user_id,});
                counter++;
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw e;
        }

        return result;

    }

    public static Boolean saveUser(
            Agent agent,
            List<AdressePersonne> adressePersonnes,
            LoginWeb loginWeb,
            String sitePeage) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {
            if (agent.getCode() == null) {
                // NOUVEL USER
                String codeAgent = createUser(agent);
                if (codeAgent != null && !codeAgent.isEmpty()) {

                    //INSERT INTO ADRESSE AND ADRESSE_PERSONNE
                    for (AdressePersonne adressePersonne : adressePersonnes) {
                        bulkQuery.put(counter + SQLQueryIdentification.ADD_ADRESSE_WEB, new Object[]{
                            //adressePersonne.getAdresse().getProvince().getCode().trim(),
                            adressePersonne.getAdresse().getCommune().getEntiteMere().getEntiteMere().getCode(),
                            //adressePersonne.getAdresse().getVille().getCode().trim(),
                            adressePersonne.getAdresse().getCommune().getEntiteMere().getCode(),
                            adressePersonne.getAdresse().getDistrict().getCode().trim(),
                            adressePersonne.getAdresse().getCommune().getCode().trim(),
                            adressePersonne.getAdresse().getQuartier().getCode().trim(),
                            adressePersonne.getAdresse().getAvenue().getCode().trim(),
                            adressePersonne.getAdresse().getNumero(),
                            agent.getAgentCreat(),
                            codeAgent,
                            adressePersonne.getParDefaut(),
                            0,
                            adressePersonne.getCode(),
                            true
                        });
                        counter++;
                    }
                    //INSERT INTO LOGIN_WEB
                    bulkQuery.put(counter + SQLQueryIdentification.F_CREATE_LOGIN_WEB, new Object[]{
                        codeAgent,
                        loginWeb.getMail(),
                        loginWeb.getPassword(),
                        loginWeb.getTelephone(),
                        loginWeb.getMail(),
                        agent.getAgentCreat()});
                    counter++;

                    if (!sitePeage.isEmpty()) {

                        Agent agentUser = GeneralBusiness.getAgentByPersonne(codeAgent);

                        //INSERT INTO SITE_PEAGE_USER
                        bulkQuery.put(counter + ":INSERT INTO T_SITE_PEAGE_USER (FK_AGENT,FK_SITE_PEAGE,ETAT) VALUES (?1,?2,1)", new Object[]{
                            agentUser.getCode(),
                            sitePeage});
                        counter++;
                    }

                    result = executeQueryBulkInsert(bulkQuery);
                }
            } else {
                // EDITION AGENT
                String codePersonne = updateUser(agent);

                if (!sitePeage.isEmpty()) {

                    SitePeageUser sitePeageUser = GestionUtilisateurBusiness.getSitePeageUserByUser(agent.getCode());

                    if (sitePeageUser != null) {
                        //UPDATE SITE_PEAGE_USER
                        bulkQuery.put(counter + ":UPDATE T_SITE_PEAGE_USER SET FK_SITE_PEAGE = ?1,ETAT = 1 WHERE FK_AGENT = ?2", new Object[]{
                            sitePeage,
                            agent.getCode()});
                        counter++;
                    } else {
                        //INSERT INTO SITE_PEAGE_USER
                        bulkQuery.put(counter + ":INSERT INTO T_SITE_PEAGE_USER (FK_AGENT,FK_SITE_PEAGE,ETAT) VALUES (?1,?2,1)", new Object[]{
                            agent.getCode(),
                            sitePeage});
                        counter++;
                    }

                }

                if (codePersonne != null && !codePersonne.isEmpty()) {
                    //ADRESSE PERSONNE
                    for (AdressePersonne adressePersonne : adressePersonnes) {
                        if (adressePersonne.getCode() != null && !adressePersonne.getCode().isEmpty()) {
                            bulkQuery.put(counter + SQLQueryIdentification.ADD_ADRESSE_WEB, new Object[]{
                                adressePersonne.getAdresse().getCommune().getEntiteMere().getEntiteMere().getCode(),
                                adressePersonne.getAdresse().getCommune().getEntiteMere().getCode(),
                                adressePersonne.getAdresse().getDistrict().getCode().trim(),
                                adressePersonne.getAdresse().getCommune().getCode().trim(),
                                adressePersonne.getAdresse().getQuartier().getCode().trim(),
                                adressePersonne.getAdresse().getAvenue().getCode().trim(),
                                adressePersonne.getAdresse().getNumero(),
                                agent.getAgentCreat(),
                                codePersonne,
                                adressePersonne.getParDefaut(),
                                1,
                                adressePersonne.getCode(),
                                adressePersonne.getEtat()
                            });
                        } else {
                            bulkQuery.put(counter + SQLQueryIdentification.ADD_ADRESSE_WEB, new Object[]{
                                adressePersonne.getAdresse().getProvince().getCode().trim(),
                                adressePersonne.getAdresse().getVille().getCode().trim(),
                                adressePersonne.getAdresse().getDistrict().getCode().trim(),
                                adressePersonne.getAdresse().getCommune().getCode().trim(),
                                adressePersonne.getAdresse().getQuartier().getCode().trim(),
                                adressePersonne.getAdresse().getAvenue().getCode().trim(),
                                adressePersonne.getAdresse().getNumero(),
                                agent.getAgentCreat(),
                                codePersonne,
                                adressePersonne.getParDefaut(),
                                0,
                                GeneralConst.EMPTY_STRING,
                                true
                            });
                        }
                        counter++;
                    }
                    bulkQuery.put(counter + SQLQueryIdentification.F_CREATE_LOGIN_WEB, new Object[]{
                        codePersonne,
                        loginWeb.getMail(),
                        loginWeb.getPassword(),
                        loginWeb.getTelephone(),
                        loginWeb.getMail(),
                        agent.getAgentCreat()});
                    counter++;

                    result = executeQueryBulkInsert(bulkQuery);
                }
            }
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static String createUser(Agent agent) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = IdentificationConst.ParamQUERY.PERSONNE;
        try {
            String query = SQLQueryIdentification.F_CREATE_AGENT;

            params.put(IdentificationConst.ParamQUERY.NOM, agent.getNom().trim());
            params.put(IdentificationConst.ParamQUERY.PRENOMS, agent.getPrenoms().trim());
            params.put(IdentificationConst.ParamQUERY.FONCTION, agent.getFonction().getCode().trim());
            params.put(IdentificationConst.ParamQUERY.SERVICE, agent.getService().getCode().trim());
            params.put(IdentificationConst.ParamQUERY.UA, null);
            params.put(IdentificationConst.ParamQUERY.LOGIN, agent.getLogin().trim());
            params.put(IdentificationConst.ParamQUERY.MPD, agent.getMdp().trim());
            params.put(IdentificationConst.ParamQUERY.AGENT_CREAT, agent.getAgentCreat().trim());
            params.put(IdentificationConst.ParamQUERY.ETAT, GeneralConst.Number.ONE);
            params.put(IdentificationConst.ParamQUERY.MATRICULE, agent.getMatricule().trim());
            params.put(IdentificationConst.ParamQUERY.GRADE, agent.getGrade().trim());
            params.put(IdentificationConst.ParamQUERY.COMPTE_EXPIRE, agent.getCompteExpire());
            params.put(IdentificationConst.ParamQUERY.DATE_EXPIRATION, agent.getDateExpiration());
            params.put(IdentificationConst.ParamQUERY.DISTRICT, null);
            params.put(IdentificationConst.ParamQUERY.SITE, agent.getSite().getCode().trim());
            params.put(IdentificationConst.ParamQUERY.CHANGE_PWD, agent.getChangePwd().trim());

            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static String updateUser(Agent agent) {

        HashMap<String, Object> params = new HashMap<>();

        String returnValue = IdentificationConst.ParamQUERY.PERSONNE;
        try {
            String query = SQLQueryIdentification.F_UPDATE_AGENT_WEB;
            params.put(IdentificationConst.ParamQUERY.NOM, agent.getNom().trim());
            params.put(IdentificationConst.ParamQUERY.PRENOMS, agent.getPrenoms().trim());
            params.put(IdentificationConst.ParamQUERY.FONCTION, agent.getFonction().getCode().trim());
            params.put(IdentificationConst.ParamQUERY.SERVICE, agent.getService().getCode().trim());
            params.put(IdentificationConst.ParamQUERY.UA, null);
            params.put(IdentificationConst.ParamQUERY.LOGIN, agent.getLogin().trim());
            params.put(IdentificationConst.ParamQUERY.MPD, agent.getMdp().trim());
            params.put(IdentificationConst.ParamQUERY.AGENT_CREAT, agent.getAgentCreat().trim());
            params.put(IdentificationConst.ParamQUERY.ETAT, GeneralConst.Number.ONE);
            params.put(IdentificationConst.ParamQUERY.MATRICULE, agent.getMatricule().trim());
            params.put(IdentificationConst.ParamQUERY.GRADE, agent.getGrade().trim());
            params.put(IdentificationConst.ParamQUERY.COMPTE_EXPIRE, agent.getCompteExpire());
            params.put(IdentificationConst.ParamQUERY.DATE_EXPIRATION, agent.getDateExpiration());
            params.put(IdentificationConst.ParamQUERY.DISTRICT, null);
            params.put(IdentificationConst.ParamQUERY.SITE, agent.getSite().getCode().trim());
            params.put(IdentificationConst.ParamQUERY.CHANGE_PWD, agent.getChangePwd().trim());
            params.put(IdentificationConst.ParamQUERY.CODE, agent.getCode());
            String result = myDao.getDaoImpl().getStoredProcedureOutput(query, returnValue, params);

            if (!result.isEmpty()) {
                returnValue = result;
            }
        } catch (Exception e) {
            throw (e);
        }
        return returnValue;
    }

    public static List<Agent> getListGestionnaireByCriterion(String valueSearch) {
        String sqlQuerySecondary = cd.hologram.erecettesvg.constants.GeneralConst.EMPTY_STRING;

        sqlQuerySecondary = SQLQueryIdentification.SQL_QUERY_LIST_GESTIONNAIRE_WITH_LIKE;
        String sqlQueryMaster = SQLQueryIdentification.SELECT_MASTER_LIST_GESTIONNAIRE_BY_CRITERION;
        sqlQueryMaster = String.format(sqlQueryMaster, sqlQuerySecondary);

        List<Agent> listGestionnaire = (List<Agent>) myDao.getDaoImpl().find(sqlQueryMaster,
                Casting.getInstance().convertIntoClassType(Agent.class), true, valueSearch.trim());
        return listGestionnaire;
    }

    public static Boolean affecterAssujetti(
            List<Gestionnaire> listAssujetti) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (listAssujetti.size() > 0) {

                for (Gestionnaire gestionnaire : listAssujetti) {

                    bulkQuery.put(counter + SQLQueryIdentification.AFFECTER_ASSUJETTIS, new Object[]{
                        gestionnaire.getAgent().getCode(),
                        gestionnaire.getPersonne().getCode().trim(),
                        gestionnaire.getDateCreat(),
                        gestionnaire.getEtat()
                    });

                    counter++;

                }

                result = executeQueryBulkInsert(bulkQuery);
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Personne> getAssujettiByName(String libelle) {
        try {
            String query = SQLQueryIdentification.SELECT_ASSUJETTI_BY_NAME;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), true, libelle);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Personne getAssujettiByCodeOrNif(String nif, boolean byCode) {
        try {
            String query = byCode
                    ? SQLQueryIdentification.SELECT_ASSUJETTI_BY_CODE
                    : SQLQueryIdentification.SELECT_ASSUJETTI_BY_NIF;

            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, nif);
            return personnes.isEmpty() ? null : personnes.get(0);
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Personne> getAssujettiByGestionnaire(String codeGestionnaire) {
        try {
            String query = SQLQueryIdentification.SELECT_ASSUJETTI_BY_GESTIONNAIRE;
            List<Personne> personnes = (List<Personne>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Personne.class), false, codeGestionnaire);
            return personnes;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean desaffecterAssujetti(
            List<Gestionnaire> listAssujetti) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (listAssujetti.size() > 0) {

                for (Gestionnaire gestionnaire : listAssujetti) {

                    bulkQuery.put(counter + SQLQueryIdentification.DESAFFECTER_ASSUJETTIS, new Object[]{
                        gestionnaire.getAgent().getCode(),
                        gestionnaire.getPersonne().getCode().trim()
                    });

                    counter++;

                }

                result = executeQueryBulkInsert(bulkQuery);
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Agent> getUserByName(String libelle) {
        try {
            String query = SQLQueryIdentification.SELECT_USER_BY_NAME;
            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), true, libelle);
            return agent;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Agent> getUserBySimpleSearch(String type, String value, boolean isPeage) {
        try {

            boolean isLike = false;
            String sqlParam = GeneralConst.EMPTY_STRING;

            switch (type) {
                case GeneralConst.Number.ONE:
                    sqlParam = " AND a.LOGIN = '%s'";
                    sqlParam = String.format(sqlParam, value);
                    break;
                case GeneralConst.Number.TWO:
                    isLike = true;
                    sqlParam = " AND (a.NOM LIKE ?1 OR a.PRENOMS LIKE ?1)";
                    break;
            }

            String query = GeneralConst.EMPTY_STRING;

            if (isPeage) {
                query = "SELECT * from T_AGENT a WITH (READPAST) WHERE a.ETAT IN (0,1) %s AND a.CODE <> 9115 AND a.SITE IN (SELECT CODE FROM T_SITE WITH (READPAST) WHERE PEAGE = 1) ORDER BY a.NOM,a.PRENOMS";
            } else {
                query = "SELECT * from T_AGENT a WITH (READPAST) WHERE a.ETAT IN (0,1) %s AND a.CODE <> 1871 AND a.SITE IN (SELECT CODE FROM T_SITE WITH (READPAST) WHERE PEAGE = 0) ORDER BY a.NOM,a.PRENOMS";
            }

            query = String.format(query, sqlParam);

            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), isLike, value.trim());
            return agent;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Agent> getUserByAdvancedSearch(String service, String site, String dateDebut, String dateFin) {
        try {

            String sqlServiceParam = GeneralConst.EMPTY_STRING;
            String sqlSiteParam = GeneralConst.EMPTY_STRING;
            String sqlDatesParam = GeneralConst.EMPTY_STRING;

            if (!service.equals("*")) {
                sqlServiceParam = " AND a.SERVICE = '%s'";
                sqlServiceParam = String.format(sqlServiceParam, service);

            }

            if (!site.equals("*")) {
                sqlSiteParam = " AND a.SITE = '%s'";
                sqlSiteParam = String.format(sqlSiteParam, site);

            }

            sqlDatesParam = " AND DBO.FDATE(a.DATE_CREAT) BETWEEN '%s' AND '%s'";
            sqlDatesParam = String.format(sqlDatesParam, dateDebut, dateFin);

            String query = "SELECT * from T_AGENT a WITH (READPAST) WHERE a.ETAT = 1 %s %s %s ORDER BY a.NOM,a.PRENOMS";
            query = String.format(query, sqlServiceParam, sqlSiteParam, sqlDatesParam);

            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false);
            return agent;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Agent> getUserByFonction(String libelle) {
        try {
            String query = SQLQueryIdentification.SELECT_USER_BY_FONCTION;
            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, libelle);
            return agent;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Agent> getUserByFonction(String libelle, String codeFonction, String idUser) {
        try {
            String code_Fonction = propertiesConfig.getProperty("ADMINISTRATEUR_BANQUE");

            String query = GeneralConst.EMPTY_STRING;

            if (code_Fonction.equals(codeFonction)) {
                query = SQLQueryIdentification.SELECT_USER_BY_FONCTION_ADMIN_BANK;

                List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Agent.class), false, libelle, idUser);
                return agent;

            } else {
                query = SQLQueryIdentification.SELECT_USER_BY_FONCTION;

                List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                        Casting.getInstance().convertIntoClassType(Agent.class), false, libelle);
                return agent;

            }

        } catch (Exception e) {
            throw (e);
        }
    }

    public static EntiteAdministrative getDistrictByCode(String code) {

        try {
            String query = SQLQueryIdentification.SELECT_DISTRINCT_BY_CODE;

            List<EntiteAdministrative> district = (List<EntiteAdministrative>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(EntiteAdministrative.class), false, code);
            return district.isEmpty() ? null : district.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static Agent getUserByCode(String code) {

        try {
            String query = SQLQueryIdentification.SELECT_USER_BY_CODE;

            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, code);
            return agent.isEmpty() ? null : agent.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean disabledUser(String disabledUser) {

        boolean result = false;
        int idCodeUser = Integer.valueOf(disabledUser);
        try {
            String query = SQLQueryIdentification.DISABLED_USER;

            result = myDao.getDaoImpl().executeStoredProcedure(query, idCodeUser);

            return result;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> getDroitrByIntitule(String libelle, String codeUser, String codeAgent, String codeDroit) {
        String primaryQuery = "";
        String secondaryQuery = "";
        String threryQuery = "";
        boolean check = false;
        try {
            primaryQuery = "SELECT D.* FROM T_DROIT D WITH (READPAST) WHERE D.ETAT = 1 "
                    + "AND D.CODE NOT IN (SELECT U.DROIT FROM T_ACCES_USER U WITH (READPAST)"
                    + "  WHERE U.AGENT ='" + codeAgent + "' AND U.ETAT = 1) %s %s";

            if (codeDroit != null) {
                if (codeDroit.equals("0")) {
                    threryQuery = "";
                } else {
                    threryQuery = "AND D.FK_MODULE ='" + codeDroit + "'";
                }
            }

            if (!libelle.equals(GeneralConst.EMPTY_STRING)) {
                secondaryQuery = " AND D.INTITULE LIKE ?1";
                check = true;
            }

            primaryQuery = String.format(primaryQuery, secondaryQuery, threryQuery);

            List<Droit> droit = (List<Droit>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(Droit.class), check, libelle);
            return droit;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> getDroitrByCode(String codeDroit, String codeUser, String codeModule) {

        String primaryQuery = "";
        String secondaryQuery = "";

        try {

            primaryQuery = "SELECT D.CODE, D.INTITULE, D.FK_MODULE FROM T_DROIT D WITH(READPAST) "
                    + "WHERE D.CODE NOT IN (SELECT A.DROIT FROM T_ACCES_USER A WITH(READPAST)"
                    + " WHERE A.AGENT = '" + codeUser + "' AND A.ETAT = 1 ) AND D.ETAT = 1 "
                    + "AND D.CODE = '" + codeDroit + "' %s";

            if (!codeDroit.equals(GeneralConst.EMPTY_STRING)) {
                if (codeModule.equals("0")) {
                    secondaryQuery = "";
                } else {
                    secondaryQuery = "AND D.FK_MODULE ='" + codeModule + "'";
                }
            }

            primaryQuery = String.format(primaryQuery, secondaryQuery);

            primaryQuery = String.format(primaryQuery, secondaryQuery);
            List<Droit> droit = (List<Droit>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, codeUser, codeDroit);
            return droit;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> getDroitrAffecterUserByCode(String codeDroit, String codeUser) {
        try {
            String query = SQLQueryIdentification.SELECT_DROIT_BY_CODE;
            List<Droit> droit = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, codeUser, codeDroit);
            return droit;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> getDroitrAffecterUserByIntitule(String intitule, String codeUser, String codeModule) {
        String primaryQuery = "";
        String secondaryQuery = "";
        String thirdaryQuery = "";
        boolean check = false;
        try {
            primaryQuery = "SELECT D.CODE, D.INTITULE FROM T_DROIT D WITH(READPAST)  "
                    + " WHERE D.CODE IN (SELECT A.DROIT FROM T_ACCES_USER A WITH(READPAST)"
                    + " WHERE A.AGENT = '" + codeUser + "' AND A.ETAT = 1 ) AND D.ETAT = 1 %s %s";

            if (!intitule.equals(GeneralConst.EMPTY_STRING)) {
                secondaryQuery = " AND D.INTITULE LIKE ?1";
                check = true;
            }

            if (codeModule != null) {
                if (codeModule.equals("0")) {
                    thirdaryQuery = "";
                } else {
                    thirdaryQuery = " AND D.FK_MODULE = '" + codeModule + "'";
                }
            }

            primaryQuery = String.format(primaryQuery, secondaryQuery, thirdaryQuery);

            List<Droit> droit = (List<Droit>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(Droit.class), check, intitule);
            return droit;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean affecterDroits(
            List<AccesUser> listAccesUser) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (listAccesUser.size() > 0) {

                for (AccesUser accesUser : listAccesUser) {

                    bulkQuery.put(counter + SQLQueryIdentification.AFFECTER_DROIT, new Object[]{
                        accesUser.getAgent(),
                        accesUser.getDroit(),
                        accesUser.getAgentCreat()
                    });

                    counter++;

                }

                result = executeQueryBulkInsert(bulkQuery);
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean desaffecterroits(
            List<String> accesUserList, String codeUser) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (accesUserList.size() > 0) {

                for (String accesUser : accesUserList) {

                    bulkQuery.put(counter + SQLQueryIdentification.DESAFFECTER_DROIT, new Object[]{
                        accesUser,
                        codeUser
                    });

                    counter++;

                }

                result = executeQueryBulkInsert(bulkQuery);
            }

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean removeRightUser(int accessUserId) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            bulkQuery.put(counter + ":DELETE T_ACCES_USER WHERE ID = ?1", new Object[]{
                accessUserId
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean activateRightUser(int accessUserId) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            bulkQuery.put(counter + ":UPDATE T_ACCES_USER set ETAT = 1 WHERE ID = ?1", new Object[]{
                accessUserId
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean saveFonctionUser(String libelle, String code) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            if (code.isEmpty()) {
                bulkQuery.put(counter + ":EXEC F_NEW_FONCTION ?1", new Object[]{
                    libelle.trim()
                });
            } else {
                bulkQuery.put(counter + ":UPDATE T_FONCTION SET INTITULE = ?1 WHERE CODE = ?2", new Object[]{
                    libelle.trim(), code
                });
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean resetPasswordUser(int userId, int agentMaj) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            String query = ":UPDATE T_AGENT SET MDP = HASHBYTES('MD5','123456'), CHANGE_PWD = 1,CONNECTE = 0,DATE_DERNIERE_DECONNEXION = GETDATE(),AGENT_MAJ = ?1,DATE_MAJ = GETDATE() WHERE CODE = ?2";

            bulkQuery.put(counter + query, new Object[]{
                agentMaj, userId
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean deleteOrActivateUser(int userId, int agentMaj, int state, String observation) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            String query = ":UPDATE T_AGENT SET ETAT = ?1, OBSERVATION_SUPRESSION = ?2, DATE_SUPPRESSION = GETDATE(),AGENT_SUPPRESSION = ?3 WHERE CODE = ?4";

            bulkQuery.put(counter + query, new Object[]{
                state, observation, agentMaj, userId
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean addRightUser(int userId, String droitId) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            bulkQuery.put(counter + ":INSERT INTO T_ACCES_USER (AGENT,DROIT,DATE_CREAT,ETAT) VALUES (?1,?2,GETDATE(),1)", new Object[]{
                userId, droitId
            });

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<AccesUser> getDroitAccesUserByUser(List<String> listDroit, String codeUser) {
        try {
            String query = SQLQueryIdentification.SELECT_DROIT_ACCES_USER_BY_USER;
            List<AccesUser> droit = (List<AccesUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AccesUser.class), false, listDroit, codeUser);
            return droit;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> loadDroits() {
        try {
            //String query = SQLQueryIdentification.LOAD_DROIT;
            String query = SQLQueryIdentification.LOAD_DROIT_V2;
            List<Droit> droit = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false);
            return droit;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Module> loadModules() {
        try {
            String query = SQLQueryIdentification.LOAD_MODULE;
            List<Module> module = (List<Module>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Module.class), false);
            return module;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Boolean saveDroit(Droit droit) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {

            bulkQuery.put(counter + SQLQueryIdentification.NEW_DROIT, new Object[]{
                droit.getCode().trim(),
                droit.getIntitule().trim(),
                droit.getAgentCreat().trim(),
                droit.getDateCreat(),
                droit.getAgentMaj(),
                droit.getDateMaj(),
                droit.getEtat(),
                droit.getFkModule().getCode()});
            counter++;

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static Boolean saveDroitSupp(List<String> droitList, int codeUserBeneficiaire, int codeUserCreate) {

        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;

        try {

            for (String droit : droitList) {

                counter++;

                bulkQuery.put(counter + ":INSERT INTO T_ACCES_USER (AGENT,DROIT,AGENT_CREAT,DATE_CREAT,ETAT) VALUES (?1,?2,?3,GETDATE(),1)", new Object[]{
                    codeUserBeneficiaire,
                    droit.trim(),
                    codeUserCreate});
            }

            result = executeQueryBulkInsert(bulkQuery);

        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static List<Droit> getDroitAffecterByCode(String codeDroit, String codeUser, String codeModule) {

        String primaryQuery = "";
        String secondaryQuery = "";

        try {

            primaryQuery = "SELECT D.CODE, D.INTITULE, D.FK_MODULE FROM T_DROIT D WITH(READPAST) "
                    + "WHERE D.CODE IN (SELECT A.DROIT FROM T_ACCES_USER A WITH(READPAST)"
                    + " WHERE A.AGENT = '" + codeUser + "' AND A.ETAT = 1 ) AND D.ETAT = 1 "
                    + "AND D.CODE = '" + codeDroit + "' %s";

            if (!codeDroit.equals(GeneralConst.EMPTY_STRING)) {
                if (codeModule.equals("0")) {
                    secondaryQuery = "";
                } else {
                    secondaryQuery = "AND D.FK_MODULE ='" + codeModule + "'";
                }
            }

            primaryQuery = String.format(primaryQuery, secondaryQuery);

            primaryQuery = String.format(primaryQuery, secondaryQuery);
            List<Droit> droit = (List<Droit>) myDao.getDaoImpl().find(primaryQuery,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, codeUser, codeDroit);
            return droit;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<AccesUser> loadAccessByUser(int userId) {
        try {
            String query = "SELECT * from T_ACCES_USER WITH (READPAST) WHERE AGENT = ?1";
            List<AccesUser> accesUsers = (List<AccesUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AccesUser.class), false, userId);
            return accesUsers;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static Droit getDroitByCode(String code) {

        try {
            String query = "SELECT * FROM T_DROIT WHERE CODE = ?1";

            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, code);
            return droits.isEmpty() ? null : droits.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Droit> loadDroitNonAccorderByUserV1(int userId, String filter) {
        try {

            String query = "Select d.* from t_droit d WITH (READPAST) Join T_Module m WITH (READPAST) \n"
                    + " on d.fk_Module = m.code where d.code not in (SELECT droit from T_ACCES_USER WITH (READPAST) \n"
                    + " WHERE AGENT = ?1 AND etat = 0) and not d.fk_Module is null and d.etat = 1 and d.fk_Module IN (12) order by m.intitule";

            query = String.format(query, filter);

            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, userId);
            return droits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> loadDroitNonAccorderByUserV2(int userId, String filter) {
        try {

            String query = "Select d.* from t_droit d WITH (READPAST) Join T_Module m WITH (READPAST) \n"
                    + " on d.fk_Module = m.code where d.code  not in (SELECT droit from T_ACCES_USER WITH (READPAST) \n"
                    + " WHERE AGENT = ?1) and not d.fk_Module is null and d.etat = 1 %s order by m.intitule";

            query = String.format(query, filter);

            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, userId);
            return droits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> loadDroitNonAccorderByUserV3(int userId, String filter) {
        try {

            String query = "Select d.* from t_droit d WITH (READPAST) where d.code not in (SELECT droit from T_ACCES_USER WITH (READPAST) \n"
                    + " WHERE AGENT = ?1) and d.etat = 1 %s order by d.code";

            query = String.format(query, filter);

            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, userId);
            return droits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> loadDroitAccorderByUser(int userId) {
        try {

            String query = "Select d.* from t_droit d WITH (READPAST) Join T_Module m WITH (READPAST) on d.fk_Module = m.code where d.code "
                    + " in (SELECT droit from T_ACCES_USER WITH (READPAST) WHERE AGENT = ?1 and etat = 1) and "
                    + " not d.fk_Module is null and d.etat = 1 order by m.intitule";

            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, userId);
            return droits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> loadDroitAccorderByUserV2(int userId) {
        try {

            String query = "Select d.* from t_droit d WITH (READPAST) Join T_Module m WITH (READPAST) on d.fk_Module = m.code where d.code "
                    + " in (SELECT droit from T_ACCES_USER WITH (READPAST) WHERE AGENT = ?1 and etat = 1) and "
                    + " not d.fk_Module is null and d.etat = 1 order by m.intitule,d.code";

            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, userId);
            return droits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> loadDroitNonAccorderByUser(int userId) {
        try {

            String query = "Select d.* from t_droit d WITH (READPAST) Join T_Module m WITH (READPAST) on d.fk_Module = m.code where d.code "
                    + " not in (SELECT droit from T_ACCES_USER WITH (READPAST) WHERE AGENT = ?1 and etat = 1) and "
                    + " not d.fk_Module is null and d.etat = 1 order by m.intitule,d.code";

            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, userId);
            return droits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Droit> loadDroitNonAccorderByUserV2(int userId) {
        try {

            String query = "Select d.* from t_droit d WITH (READPAST) Join T_Module m WITH (READPAST) on d.fk_Module = m.code where d.code "
                    + " not in (SELECT droit from T_ACCES_USER WITH (READPAST) WHERE AGENT = ?1 and etat = 1) and "
                    + " not d.fk_Module is null and d.etat = 1 and d.fk_Module <> 10 order by m.intitule,d.code";

            List<Droit> droits = (List<Droit>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Droit.class), false, userId);
            return droits;
        } catch (Exception e) {
            throw (e);
        }
    }

    public static AccesUser getAccesByUserAndRight(int userId, String droitId) {
        try {

            String query = "SELECT au.* FROM T_ACCES_USER au WHERE au.AGENT = ?1 AND au.DROIT = ?2";

            List<AccesUser> accesUsers = (List<AccesUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AccesUser.class), false, userId, droitId);

            return accesUsers.isEmpty() ? null : accesUsers.get(0);

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<AccesUser> getListAccesRightByUser(int userId) {
        try {

            String query = "SELECT au.* FROM T_ACCES_USER au WHERE au.AGENT = ?1 ";

            List<AccesUser> accesUsers = (List<AccesUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AccesUser.class), false, userId);

            return accesUsers;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<AccesUser> getAccesUserPeageByUser(int userId) {
        try {

            String query = "SELECT * FROM T_ACCES_USER WHERE AGENT = ?1 AND DROIT IN "
                    + "(SELECT CODE FROM T_DROIT WHERE FK_MODULE = 10 AND ETAT = 1) AND ETAT = 1";

            List<AccesUser> accesUserList = (List<AccesUser>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(AccesUser.class), false, userId);

            return accesUserList;

        } catch (Exception e) {
            throw e;
        }
    }

    public static List<Agent> getUserByAdvancedSearch(String service, String site, String dateDebut, String dateFin, String division, boolean isPeage) {
        try {

            String sqlServiceParam = GeneralConst.EMPTY_STRING;
            String sqlSiteParam = GeneralConst.EMPTY_STRING;
            String sqlDatesParam = GeneralConst.EMPTY_STRING;
            String sqlDivisionParam = GeneralConst.EMPTY_STRING;

            if (!service.equals("*")) {
                sqlServiceParam = " AND a.SERVICE = '%s'";
                sqlServiceParam = String.format(sqlServiceParam, service);

            }

            if (!site.equals("*")) {
                sqlSiteParam = " AND a.SITE = '%s'";
                sqlSiteParam = String.format(sqlSiteParam, site);

            }

            if (!division.equals("*")) {
                sqlDivisionParam = "AND a.SITE IN (SELECT CODE FROM T_SITE WHERE FK_DIVISION = '%s')";
                sqlDivisionParam = String.format(sqlDivisionParam, division);

            }

            sqlDatesParam = " AND DBO.FDATE(a.DATE_CREAT) BETWEEN '%s' AND '%s'";
            sqlDatesParam = String.format(sqlDatesParam, dateDebut, dateFin);

            String query = GeneralConst.EMPTY_STRING;

            if (isPeage) {
                query = "SELECT * from T_AGENT a WITH (READPAST) WHERE a.ETAT IN (0,1) AND a.CODE <> 9115 %s %s %s %s AND a.SITE IN (SELECT CODE FROM T_SITE WHERE PEAGE = 1)ORDER BY a.NOM,a.PRENOMS";
            } else {
                query = "SELECT * from T_AGENT a WITH (READPAST) WHERE a.ETAT IN (0,1) AND a.CODE <> 1871 %s %s %s %s AND a.SITE IN (SELECT CODE FROM T_SITE WHERE PEAGE = 0)ORDER BY a.NOM,a.PRENOMS";
            }

            query = String.format(query, sqlServiceParam, sqlSiteParam, sqlDatesParam, sqlDivisionParam);

            List<Agent> agent = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false);
            return agent;

        } catch (Exception e) {
            throw (e);
        }
    }

    public static List<Agent> getListAgentBySite(String codeSite) {
        try {
            String query = "SELECT A.* FROM T_AGENT A WITH (READPAST) WHERE A.ETAT = 1 AND A.SITE = ?1 ORDER BY A.PRENOMS,A.NOM";
            List<Agent> agents = (List<Agent>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(Agent.class), false, codeSite.trim());
            return agents;
        } catch (Exception e) {
            throw (e);
        }
    }

}
