package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(GuAcces.class)
public class GuAcces_ { 

    public static volatile SingularAttribute<GuAcces, String> agentMaj;
    public static volatile SingularAttribute<GuAcces, Date> dateCreat;
    public static volatile SingularAttribute<GuAcces, String> droits;
    public static volatile SingularAttribute<GuAcces, Integer> id;
    public static volatile SingularAttribute<GuAcces, Integer> groupe;
    public static volatile SingularAttribute<GuAcces, String> agentCreat;
    public static volatile SingularAttribute<GuAcces, Short> etat;
    public static volatile SingularAttribute<GuAcces, Date> dateMaj;

}