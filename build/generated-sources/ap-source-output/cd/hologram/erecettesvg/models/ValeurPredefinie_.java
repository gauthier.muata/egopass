package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.TypeComplement;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ValeurPredefinie.class)
public class ValeurPredefinie_ { 

    public static volatile SingularAttribute<ValeurPredefinie, TypeComplement> fkTypeComplement;
    public static volatile SingularAttribute<ValeurPredefinie, String> code;
    public static volatile SingularAttribute<ValeurPredefinie, String> valeur;
    public static volatile SingularAttribute<ValeurPredefinie, Boolean> etat;

}