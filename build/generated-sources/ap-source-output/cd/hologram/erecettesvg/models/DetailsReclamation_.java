package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Decision;
import cd.hologram.erecettesvg.models.Reclamation;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailsReclamation.class)
public class DetailsReclamation_ { 

    public static volatile SingularAttribute<DetailsReclamation, String> ncPartieNonConteste;
    public static volatile SingularAttribute<DetailsReclamation, Date> dateDecisionSurcis;
    public static volatile SingularAttribute<DetailsReclamation, Date> dateTraitementJuridique;
    public static volatile SingularAttribute<DetailsReclamation, Integer> decisionSurcis;
    public static volatile SingularAttribute<DetailsReclamation, Reclamation> fkReclamation;
    public static volatile SingularAttribute<DetailsReclamation, String> referenceDocument;
    public static volatile SingularAttribute<DetailsReclamation, Date> dateTraitement;
    public static volatile SingularAttribute<DetailsReclamation, String> ncDecisionJuridique;
    public static volatile SingularAttribute<DetailsReclamation, Decision> fkDecision;
    public static volatile SingularAttribute<DetailsReclamation, String> observationTraitement;
    public static volatile SingularAttribute<DetailsReclamation, BigDecimal> pourcentageMontantNonContestre;
    public static volatile SingularAttribute<DetailsReclamation, BigDecimal> montantContester;
    public static volatile SingularAttribute<DetailsReclamation, Agent> agentTraitement;
    public static volatile SingularAttribute<DetailsReclamation, Integer> avecSurcis;
    public static volatile SingularAttribute<DetailsReclamation, String> ncDecisionAdmin;
    public static volatile SingularAttribute<DetailsReclamation, String> typeDocument;
    public static volatile SingularAttribute<DetailsReclamation, Integer> id;
    public static volatile SingularAttribute<DetailsReclamation, String> motif;
    public static volatile SingularAttribute<DetailsReclamation, Integer> fkDecisionJuridique;
    public static volatile SingularAttribute<DetailsReclamation, Integer> agentTraitementJuridique;

}