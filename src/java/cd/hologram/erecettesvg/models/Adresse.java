/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.constants.GeneralConst;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_ADRESSE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adresse.findAll", query = "SELECT a FROM Adresse a"),
    @NamedQuery(name = "Adresse.findById", query = "SELECT a FROM Adresse a WHERE a.id = :id"),
    @NamedQuery(name = "Adresse.findByNumero", query = "SELECT a FROM Adresse a WHERE a.numero = :numero"),
    @NamedQuery(name = "Adresse.findByChaine", query = "SELECT a FROM Adresse a WHERE a.chaine = :chaine"),
    @NamedQuery(name = "Adresse.findByEtat", query = "SELECT a FROM Adresse a WHERE a.etat = :etat")})
public class Adresse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 20)
    @Column(name = "NUMERO")
    private String numero;
    @Column(name = "Chaine")
    private String chaine;
    @Column(name = "ETAT")
    private Boolean etat;
    @JoinColumn(name = "DISTRICT", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative district;
    @JoinColumn(name = "VILLE", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative ville;
    @JoinColumn(name = "COMMUNE", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative commune;
    @JoinColumn(name = "QUARTIER", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative quartier;
    @JoinColumn(name = "AVENUE", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative avenue;
    @JoinColumn(name = "PROVINCE", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative province;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adresse")
    private List<AdressePersonne> adressePersonneList;
    @OneToMany(mappedBy = "adresse")
    private List<Site> siteList;

    public Adresse() {
    }

    public Adresse(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getEtat() {
        return etat;
    }

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public String getChaine() {
        return chaine;
    }

    public void setChaine(String chaine) {
        this.chaine = chaine;
    }

    public EntiteAdministrative getDistrict() {
        return district;
    }

    public void setDistrict(EntiteAdministrative district) {
        this.district = district;
    }

    public EntiteAdministrative getVille() {
        return ville;
    }

    public void setVille(EntiteAdministrative ville) {
        this.ville = ville;
    }

    public EntiteAdministrative getCommune() {
        return commune;
    }

    public void setCommune(EntiteAdministrative commune) {
        this.commune = commune;
    }

    public EntiteAdministrative getQuartier() {
        return quartier;
    }

    public void setQuartier(EntiteAdministrative quartier) {
        this.quartier = quartier;
    }

    public EntiteAdministrative getAvenue() {
        return avenue;
    }

    public void setAvenue(EntiteAdministrative avenue) {
        this.avenue = avenue;
    }

    public EntiteAdministrative getProvince() {
        return province;
    }

    public void setProvince(EntiteAdministrative province) {
        this.province = province;
    }

    @XmlTransient
    public List<AdressePersonne> getAdressePersonneList() {
        return adressePersonneList;
    }

    public void setAdressePersonneList(List<AdressePersonne> adressePersonneList) {
        this.adressePersonneList = adressePersonneList;
    }

    @XmlTransient
    public List<Site> getSiteList() {
        return siteList;
    }

    public void setSiteList(List<Site> siteList) {
        this.siteList = siteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adresse)) {
            return false;
        }
        Adresse other = (Adresse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        
        String myVille = ville != null ? ville.getIntitule() : GeneralConst.EMPTY_STRING;
        String myCommune = commune != null ? commune.getIntitule() : GeneralConst.EMPTY_STRING;
        String myQuartier = quartier != null ? quartier.getIntitule() : GeneralConst.EMPTY_STRING;
        String myAvenue = avenue != null ? avenue.getIntitule() : GeneralConst.EMPTY_STRING;
        String myNumero = numero != null ? numero : GeneralConst.EMPTY_STRING;

        return myVille + "--> C/" + myCommune
                + " - Q/" + myQuartier + " - Av. " + myAvenue + " n° " + myNumero;
    }

}
