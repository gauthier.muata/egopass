/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var tableAssujettiValider;

var cmbSearchTypeAssujettisValider;

var inputSearchAssujettisValider;

var btnSearchAssujettisValider;

var codeResearchAssujettis, messageEmptyValueAssujettis, messageAvanceeAssujettis;

var tempAssujettisList;
var nomComplet = '';

$(function () {

    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Registre des demandes d\'inscriptions à la télé-procédure');

    removeActiveMenu();
    linkSubMenuValiderAssujettis.addClass('active');

    tableAssujettiValider = $('#tableAssujettiValider');

    cmbSearchTypeAssujettisValider = $('#cmbSearchTypeAssujettisValider');

    inputSearchAssujettisValider = $('#inputSearchAssujettisValider');

    btnSearchAssujettisValider = $('#btnSearchAssujettisValider');

    codeResearchAssujettis = cmbSearchTypeAssujettisValider.val();
    messageEmptyValueAssujettis = 'Veuillez d\'abord saisir le nom de l\'assujetti';
    messageAvanceeAssujettis = 'Veuillez d\'abord saisir le critere de recherche.';

    btnSearchAssujettisValider.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadAssujettis();
    });

    cmbSearchTypeAssujettisValider.on('change', function (e) {
        codeResearchAssujettis = cmbSearchTypeAssujettisValider.val();
        inputSearchAssujettisValider.val('');

        if (codeResearchAssujettis === '0') {
            messageEmptyValueAssujettis = 'Veuillez saisir le nom de l\'assujetti.';
            inputSearchAssujettisValider.attr('placeholder', 'Nom de l\'assujetti');
        } else {
            messageEmptyValueAssujettis = 'Veuillez saisir le numéro d\'identification nationale de l\'assujetti.';
            inputSearchAssujettisValider.attr('placeholder', 'Numéro d\'identification nationale');
        }

        printAssujettis('');
    });

    btnSearchAssujettisValider.trigger('click');

    printAssujettis('');
});

function loadAssujettis() {

    if (inputSearchAssujettisValider.val().trim() !== empty) {

        if (inputSearchAssujettisValider.val().length < SEARCH_MIN_TEXT) {
            showEmptySearchMessage();
            return;
        }

    } else {
        inputSearchAssujettisValider.val('');
    }

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAssujettisValider',
            'typeSearch': codeResearchAssujettis,
            'libelle': inputSearchAssujettisValider.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var personneList = JSON.parse(JSON.stringify(response));

                if (personneList.length > 0) {

                    tempAssujettisList = [];

                    for (var i = 0; i < personneList.length; i++) {

                        var personne = new Object();
                        personne.libelleFormeJuridique = personneList[i].libelleFormeJuridique;
                        personne.nif = personneList[i].nif;
                        personne.nom = personneList[i].nom;
                        personne.postNom = personneList[i].postNom;
                        personne.prenom = personneList[i].prenom;
                        personne.telephone = personneList[i].telephone;
                        personne.email = personneList[i].email;
                        personne.chaine = personneList[i].chaine;
                        personne.codePersonne = personneList[i].codePersonne;
                        personne.nomComplet = personneList[i].nomComplet;
                        personne.userName = personneList[i].userName;
                        personne.state = personneList[i].STATUS;
                        tempAssujettisList.push(personne);
                    }

                    printAssujettis(tempAssujettisList);

                } else {

                    printAssujettis('');
                    alertify.alert('Désolé, pour l\'instant, il n\'y a pas des demandes d\'inscription à la plate-forme de télé-procédure');
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}


function printAssujettis(tempAssujettisList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:9%" >NIF</th>';
    header += '<th style="width:10%" >NTD</th>';
    header += '<th style="width:23%">ASSUJETTI</th>';
    header += '<th style="width:10%">TYPE PERSONNE</th>';
    header += '<th style="width:12%">CONTACTS</th>';
    header += '<th style="width:10%">ETAT</th>';
    header += '<th style="width:30%" >ADRESSE PRINCIPALE</th>';
    header += '<th style="width:5%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code personne </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < tempAssujettisList.length; i++) {

        var buttonValidateDemande = '<center><button class="btn btn-success" onclick="activerAssujettis(\'' + tempAssujettisList[i].codePersonne + '\')"><i class="fa fa-check-circle"></i>&nbsp;Valider la demande</button></center>';
        var buttonRejetDemande = '<br/><center><button class="btn btn-danger" onclick="rejectDemandeInscription(\'' + tempAssujettisList[i].codePersonne + '\')"><i class="fa fa-trash-o"></i>&nbsp;Rejeter la demande</button></center>';

        var phoneInfo = 'Téléphone : ' + '<span style="font-weight:bold">' + tempAssujettisList[i].telephone + '</span>';
        var mailInfo = 'E-mail : ' + '<span style="font-weight:bold">' + tempAssujettisList[i].email + '</span>';

        var contactInfo = phoneInfo + '<br/><br/>' + mailInfo;

        var stateAccount = '';

        if (tempAssujettisList[i].state == 3) {

            stateAccount = 'En attente du traitement';

        } else if (tempAssujettisList[i].state == 4) {

            buttonRejetDemande = '';
            buttonValidateDemande = '';
            stateAccount = 'Demande validée';

        } else if (tempAssujettisList[i].state == 2) {

            buttonRejetDemande = '';
            buttonValidateDemande = '';
            stateAccount = 'Demande rejetée';

        }

        body += '<tr>';
        body += '<td style="vertical-align:middle">' + tempAssujettisList[i].nif + '</td>';
        body += '<td style="vertical-align:middle">' + tempAssujettisList[i].userName + '</td>';
        body += '<td style="vertical-align:middle">' + tempAssujettisList[i].nomComplet + '</td>';
        body += '<td style="vertical-align:middle">' + tempAssujettisList[i].libelleFormeJuridique + '</td>';
        body += '<td style="vertical-align:middle">' + contactInfo + '</td>';
        body += '<td style="vertical-align:middle;font-weight:bold">' + stateAccount.toUpperCase() + '</td>';
        body += '<td style="vertical-align:middle">' + tempAssujettisList[i].chaine + '</td>';
        body += '<td style="text-align:center;vertical-align:middle">' + buttonValidateDemande + buttonRejetDemande + '</td>';
        body += '<td hidden="true" style="vertical-align:middle">' + tempAssujettisList[i].codePersonne + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableAssujettiValider.html(tableContent);

    tableAssujettiValider.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 8,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function activerAssujettis(codeAssujettis) {

    for (var i = 0; i < tempAssujettisList.length; i++) {

        if (tempAssujettisList[i].codePersonne === codeAssujettis) {
            nomComplet = '<span style="font-weight:bold">' + tempAssujettisList[i].nomComplet + '</span>';
            alertify.confirm('Voulez-vous valider la demande d\inscription à la télé-procédure pour l\'assujetti : ' + nomComplet + ' ?', function () {
                validateAssujettis(codeAssujettis);
            });
            break;
        }
    }
}

function validateAssujettis(codeAssujettis) {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'validateAssujetti',
            'codePersonne': codeAssujettis
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Validation de la demande en cours...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('La demande d\'inscripton de l\'assujetti : ' + nomComplet + ', à la télé-procédure est valider avec succès');
                    btnSearchAssujettisValider.trigger('click');

                } else if (response == '0') {
                    alertify.alert('Echec de la validation de la demande d\'inscription');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function rejectedAssujettis(codeAssujettis) {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'rejectedAssujettis',
            'codePersonne': codeAssujettis
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Rejet de la demande en cours...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('La demande d\'inscripton de l\'assujetti : ' + nomComplet + ', à la télé-procédure est rejeter avec succès');
                    btnSearchAssujettisValider.trigger('click');

                } else if (response == '0') {
                    alertify.alert('Echec du rejet de la demande d\'inscription');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function rejectDemandeInscription(codePersonne) {

    for (var i = 0; i < tempAssujettisList.length; i++) {

        if (tempAssujettisList[i].codePersonne === codePersonne) {
            var nomComplet = '<span style="font-weight:bold">' + tempAssujettisList[i].nomComplet + '</span>';
            alertify.confirm('Voulez-vous rejeter la demande d\inscription à la télé-procédure pour l\'assujetti : ' + nomComplet + ' ?', function () {
                rejectedAssujettis(codePersonne);
            });
            break;
        }
    }
}

