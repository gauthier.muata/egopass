package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.DetailDepotDeclaration;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Service;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DepotDeclaration.class)
public class DepotDeclaration_ { 

    public static volatile SingularAttribute<DepotDeclaration, String> numeroAttestationPaiement;
    public static volatile SingularAttribute<DepotDeclaration, Agent> agent;
    public static volatile SingularAttribute<DepotDeclaration, String> code;
    public static volatile SingularAttribute<DepotDeclaration, Date> dateCreat;
    public static volatile SingularAttribute<DepotDeclaration, String> numeroDepot;
    public static volatile SingularAttribute<DepotDeclaration, Personne> personne;
    public static volatile SingularAttribute<DepotDeclaration, Service> service;
    public static volatile SingularAttribute<DepotDeclaration, BigInteger> bordereau;
    public static volatile ListAttribute<DepotDeclaration, DetailDepotDeclaration> detailDepotDeclarationList;
    public static volatile SingularAttribute<DepotDeclaration, Boolean> etat;
    public static volatile ListAttribute<DepotDeclaration, NoteCalcul> noteCalculList;
    public static volatile SingularAttribute<DepotDeclaration, Integer> nbrImpression;

}