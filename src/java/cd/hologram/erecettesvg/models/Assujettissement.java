/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_ASSUJETTISSEMENT")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Assujettissement.findAll", query = "SELECT a FROM Assujettissement a"),
    @NamedQuery(name = "Assujettissement.findByCode", query = "SELECT a FROM Assujettissement a WHERE a.code = :code"),
    @NamedQuery(name = "Assujettissement.findByDateCreate", query = "SELECT a FROM Assujettissement a WHERE a.dateCreate = :dateCreate"),
    @NamedQuery(name = "Assujettissement.findByArticleBudgetaire", query = "SELECT a FROM Assujettissement a WHERE a.articleBudgetaire = :articleBudgetaire"),
    @NamedQuery(name = "Assujettissement.findByPersonne", query = "SELECT a FROM Assujettissement a WHERE a.personne = :personne"),
    @NamedQuery(name = "Assujettissement.findByEtat", query = "SELECT a FROM Assujettissement a WHERE a.etat = :etat")})
public class Assujettissement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "CODE")
    private String code;
    @Size(max = 25)
    @Column(name = "ARTICLE_BUDGETAIRE")
    private String articleBudgetaire;
    @Size(max = 25)
    @Column(name = "PERSONNE")
    private String personne;

    @Column(name = "DATE_CREATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreate;

    @Column(name = "ETAT")
    private Integer etat;
    @OneToMany(mappedBy = "assujettissement")
    private List<PrevisionCredit> previsionCreditList;
    @OneToMany(mappedBy = "assujettissement")
    private List<DetailAssujettissement> detailAssujettissementList;

    public Assujettissement() {
    }

    public Assujettissement(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }
    
    
    

    public String getArticleBudgetaire() {
        return articleBudgetaire;
    }

    public void setArticleBudgetaire(String articleBudgetaire) {
        this.articleBudgetaire = articleBudgetaire;
    }

    public String getPersonne() {
        return personne;
    }

    public void setPersonne(String personne) {
        this.personne = personne;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    @XmlTransient
    public List<PrevisionCredit> getPrevisionCreditList() {
        return previsionCreditList;
    }

    public void setPrevisionCreditList(List<PrevisionCredit> previsionCreditList) {
        this.previsionCreditList = previsionCreditList;
    }

    @XmlTransient
    public List<DetailAssujettissement> getDetailAssujettissementList() {
        return detailAssujettissementList;
    }

    public void setDetailAssujettissementList(List<DetailAssujettissement> detailAssujettissementList) {
        this.detailAssujettissementList = detailAssujettissementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (code != null ? code.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Assujettissement)) {
            return false;
        }
        Assujettissement other = (Assujettissement) object;
        if ((this.code == null && other.code != null) || (this.code != null && !this.code.equals(other.code))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Assujettissement[ code=" + code + " ]";
    }

}
