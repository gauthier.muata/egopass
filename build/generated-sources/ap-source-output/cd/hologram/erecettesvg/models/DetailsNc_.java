package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.DetailsAmr;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.PeriodeDeclaration;
import cd.hologram.erecettesvg.models.Repartition;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailsNc.class)
public class DetailsNc_ { 

    public static volatile ListAttribute<DetailsNc, DetailsAmr> detailsAmrList;
    public static volatile SingularAttribute<DetailsNc, Integer> qte;
    public static volatile SingularAttribute<DetailsNc, Short> penaliser;
    public static volatile SingularAttribute<DetailsNc, NotePerception> notePerception;
    public static volatile SingularAttribute<DetailsNc, NoteCalcul> noteCalcul;
    public static volatile SingularAttribute<DetailsNc, Boolean> etatRepartie;
    public static volatile SingularAttribute<DetailsNc, BigDecimal> tauxArticleBudgetaire;
    public static volatile SingularAttribute<DetailsNc, Short> etat;
    public static volatile SingularAttribute<DetailsNc, String> devise;
    public static volatile SingularAttribute<DetailsNc, PeriodeDeclaration> periodeDeclaration;
    public static volatile ListAttribute<DetailsNc, Repartition> repartitionList;
    public static volatile SingularAttribute<DetailsNc, String> tarif;
    public static volatile SingularAttribute<DetailsNc, ArticleBudgetaire> articleBudgetaire;
    public static volatile SingularAttribute<DetailsNc, String> typeTaux;
    public static volatile SingularAttribute<DetailsNc, String> id;
    public static volatile SingularAttribute<DetailsNc, BigDecimal> valeurBase;
    public static volatile SingularAttribute<DetailsNc, BigDecimal> montantDu;

}