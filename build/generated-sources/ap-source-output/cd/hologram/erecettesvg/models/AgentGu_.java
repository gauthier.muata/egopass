package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.GroupUser;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AgentGu.class)
public class AgentGu_ { 

    public static volatile SingularAttribute<AgentGu, Integer> fkAgentCreat;
    public static volatile SingularAttribute<AgentGu, Date> dateCreat;
    public static volatile SingularAttribute<AgentGu, Integer> fkAgent;
    public static volatile SingularAttribute<AgentGu, Integer> id;
    public static volatile SingularAttribute<AgentGu, GroupUser> fkGu;
    public static volatile SingularAttribute<AgentGu, Boolean> etat;

}