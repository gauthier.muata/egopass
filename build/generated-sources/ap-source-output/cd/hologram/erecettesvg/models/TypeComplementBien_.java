package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.AbComplementBien;
import cd.hologram.erecettesvg.models.ComplementBien;
import cd.hologram.erecettesvg.models.TypeBien;
import cd.hologram.erecettesvg.models.TypeComplement;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TypeComplementBien.class)
public class TypeComplementBien_ { 

    public static volatile ListAttribute<TypeComplementBien, ComplementBien> complementBienList;
    public static volatile ListAttribute<TypeComplementBien, AbComplementBien> abComplementBienList;
    public static volatile SingularAttribute<TypeComplementBien, String> code;
    public static volatile SingularAttribute<TypeComplementBien, TypeComplement> complement;
    public static volatile SingularAttribute<TypeComplementBien, Boolean> etat;
    public static volatile SingularAttribute<TypeComplementBien, TypeBien> typeBien;

}