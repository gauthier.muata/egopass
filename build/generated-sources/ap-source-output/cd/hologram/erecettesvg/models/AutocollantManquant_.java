package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AutocollantManquant.class)
public class AutocollantManquant_ { 

    public static volatile SingularAttribute<AutocollantManquant, Integer> numeroAutocollant;
    public static volatile SingularAttribute<AutocollantManquant, String> agent;
    public static volatile SingularAttribute<AutocollantManquant, Integer> qte;
    public static volatile SingularAttribute<AutocollantManquant, String> observation;
    public static volatile SingularAttribute<AutocollantManquant, Boolean> recuperable;
    public static volatile SingularAttribute<AutocollantManquant, Integer> borneInferieure;
    public static volatile SingularAttribute<AutocollantManquant, Integer> borneSuperieure;
    public static volatile SingularAttribute<AutocollantManquant, Boolean> etat;
    public static volatile SingularAttribute<AutocollantManquant, Date> dateRapport;
    public static volatile SingularAttribute<AutocollantManquant, Integer> idSerie;
    public static volatile SingularAttribute<AutocollantManquant, Integer> id;
    public static volatile SingularAttribute<AutocollantManquant, String> motif;
    public static volatile SingularAttribute<AutocollantManquant, Boolean> recupere;

}