package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Degrevement;
import cd.hologram.erecettesvg.models.NoteCalcul;
import cd.hologram.erecettesvg.models.Personne;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(BonAPayer.class)
public class BonAPayer_ { 

    public static volatile SingularAttribute<BonAPayer, String> motifPenalite;
    public static volatile SingularAttribute<BonAPayer, String> code;
    public static volatile SingularAttribute<BonAPayer, Date> dateCreat;
    public static volatile SingularAttribute<BonAPayer, NoteCalcul> fkNoteCalcul;
    public static volatile SingularAttribute<BonAPayer, Integer> typeBp;
    public static volatile SingularAttribute<BonAPayer, Boolean> enContentieux;
    public static volatile SingularAttribute<BonAPayer, BigDecimal> montant;
    public static volatile SingularAttribute<BonAPayer, Boolean> estFraction;
    public static volatile ListAttribute<BonAPayer, Degrevement> degrevementList;
    public static volatile SingularAttribute<BonAPayer, Boolean> etat;
    public static volatile SingularAttribute<BonAPayer, Amr> fkAmr;
    public static volatile SingularAttribute<BonAPayer, CompteBancaire> fkCompte;
    public static volatile SingularAttribute<BonAPayer, Personne> fkPersonne;
    public static volatile SingularAttribute<BonAPayer, Boolean> etatImpression;
    public static volatile SingularAttribute<BonAPayer, Date> dateEcheance;
    public static volatile SingularAttribute<BonAPayer, String> agentCreat;
    public static volatile SingularAttribute<BonAPayer, String> acteGenerateur;

}