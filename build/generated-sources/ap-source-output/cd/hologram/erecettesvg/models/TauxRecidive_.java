package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(TauxRecidive.class)
public class TauxRecidive_ { 

    public static volatile SingularAttribute<TauxRecidive, String> agentMaj;
    public static volatile SingularAttribute<TauxRecidive, String> code;
    public static volatile SingularAttribute<TauxRecidive, String> formeJuridique;
    public static volatile SingularAttribute<TauxRecidive, Date> dateCreat;
    public static volatile SingularAttribute<TauxRecidive, BigDecimal> taux;
    public static volatile SingularAttribute<TauxRecidive, String> tauxPenalite;
    public static volatile SingularAttribute<TauxRecidive, Boolean> estPourcentage;
    public static volatile SingularAttribute<TauxRecidive, String> agentCreat;
    public static volatile SingularAttribute<TauxRecidive, Date> dateMaj;

}