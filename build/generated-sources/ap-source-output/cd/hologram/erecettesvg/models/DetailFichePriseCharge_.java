package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.FichePriseCharge;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailFichePriseCharge.class)
public class DetailFichePriseCharge_ { 

    public static volatile SingularAttribute<DetailFichePriseCharge, String> penalite;
    public static volatile SingularAttribute<DetailFichePriseCharge, String> nbreRetardMois;
    public static volatile SingularAttribute<DetailFichePriseCharge, Long> code;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> amende;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> majoration;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> taux;
    public static volatile SingularAttribute<DetailFichePriseCharge, Integer> periodeDeclaration;
    public static volatile SingularAttribute<DetailFichePriseCharge, Boolean> etat;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> penalitedu;
    public static volatile SingularAttribute<DetailFichePriseCharge, ArticleBudgetaire> articleBudgetaire;
    public static volatile SingularAttribute<DetailFichePriseCharge, String> nc;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> degrevement;
    public static volatile SingularAttribute<DetailFichePriseCharge, String> exercice;
    public static volatile SingularAttribute<DetailFichePriseCharge, FichePriseCharge> fichePriseCharge;
    public static volatile SingularAttribute<DetailFichePriseCharge, String> typeTaux;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> tauxPenalite;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> principaldu;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> base;
    public static volatile SingularAttribute<DetailFichePriseCharge, BigDecimal> montantDu;

}