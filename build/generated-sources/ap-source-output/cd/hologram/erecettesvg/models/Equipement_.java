package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ProfilEquipement;
import cd.hologram.erecettesvg.models.Site;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Equipement.class)
public class Equipement_ { 

    public static volatile ListAttribute<Equipement, ProfilEquipement> profilEquipementList;
    public static volatile SingularAttribute<Equipement, String> dateCreat;
    public static volatile SingularAttribute<Equipement, Site> fkSite;
    public static volatile SingularAttribute<Equipement, String> macAdresse;
    public static volatile SingularAttribute<Equipement, String> description;
    public static volatile SingularAttribute<Equipement, String> model;
    public static volatile SingularAttribute<Equipement, Integer> id;
    public static volatile SingularAttribute<Equipement, String> sn;
    public static volatile SingularAttribute<Equipement, String> ipAdresse;
    public static volatile SingularAttribute<Equipement, String> agentCreat;
    public static volatile SingularAttribute<Equipement, Boolean> etat;
    public static volatile SingularAttribute<Equipement, String> marque;

}