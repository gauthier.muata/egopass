package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Adresse;
import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.AgentSite;
import cd.hologram.erecettesvg.models.Division;
import cd.hologram.erecettesvg.models.Equipement;
import cd.hologram.erecettesvg.models.Journal;
import cd.hologram.erecettesvg.models.SiteBanque;
import cd.hologram.erecettesvg.models.UaSite;
import cd.hologram.erecettesvg.models.UtilisateurDivision;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Site.class)
public class Site_ { 

    public static volatile SingularAttribute<Site, Integer> fkTypeEntite;
    public static volatile SingularAttribute<Site, String> code;
    public static volatile ListAttribute<Site, UaSite> uaSiteList;
    public static volatile SingularAttribute<Site, String> personne;
    public static volatile ListAttribute<Site, SiteBanque> siteBanqueList;
    public static volatile ListAttribute<Site, UtilisateurDivision> utilisateurDivisionList;
    public static volatile SingularAttribute<Site, String> centre;
    public static volatile SingularAttribute<Site, String> intitule;
    public static volatile SingularAttribute<Site, Short> etat;
    public static volatile SingularAttribute<Site, String> sigle;
    public static volatile SingularAttribute<Site, Division> division;
    public static volatile SingularAttribute<Site, Boolean> peage;
    public static volatile ListAttribute<Site, Journal> journalList;
    public static volatile ListAttribute<Site, AgentSite> agentSiteList;
    public static volatile ListAttribute<Site, Equipement> equipementList;
    public static volatile SingularAttribute<Site, Adresse> adresse;
    public static volatile ListAttribute<Site, Agent> agentList;

}