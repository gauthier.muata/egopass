package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Contrainte;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ParamContrainte.class)
public class ParamContrainte_ { 

    public static volatile SingularAttribute<ParamContrainte, String> agentMaj;
    public static volatile SingularAttribute<ParamContrainte, Date> dateCreat;
    public static volatile SingularAttribute<ParamContrainte, BigDecimal> fraisPoursuiteAtd;
    public static volatile SingularAttribute<ParamContrainte, BigDecimal> pourcentageInteretMoratoire;
    public static volatile SingularAttribute<ParamContrainte, String> comptePenalite;
    public static volatile SingularAttribute<ParamContrainte, String> agentCmdt;
    public static volatile SingularAttribute<ParamContrainte, Boolean> etat;
    public static volatile SingularAttribute<ParamContrainte, String> compteFonctionnement;
    public static volatile SingularAttribute<ParamContrainte, Date> dateMaj;
    public static volatile ListAttribute<ParamContrainte, Contrainte> contrainteList;
    public static volatile SingularAttribute<ParamContrainte, String> compteTresorUrbain;
    public static volatile SingularAttribute<ParamContrainte, String> agentSignataire;
    public static volatile SingularAttribute<ParamContrainte, BigDecimal> pourcentageFraisPoursuiteCtrCmdt;
    public static volatile SingularAttribute<ParamContrainte, String> id;
    public static volatile SingularAttribute<ParamContrainte, String> refFaitGenerateur;
    public static volatile SingularAttribute<ParamContrainte, String> agentCreat;
    public static volatile SingularAttribute<ParamContrainte, BigDecimal> pourcentagePenalite;

}