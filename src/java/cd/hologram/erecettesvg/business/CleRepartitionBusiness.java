/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.business;

import static cd.hologram.erecettesvg.business.TaxationBusiness.executeQueryBulkInsert;
import cd.hologram.erecettesvg.constants.*;
import cd.hologram.erecettesvg.dao.Dao;
import java.util.List;
import cd.hologram.erecettesvg.models.CleRepartition;
import cd.hologram.erecettesvg.sql.SQLQueryCleRepartition;
import cd.hologram.erecettesvg.sql.SQLQueryGestionArticleBudgetaire;
import cd.hologram.erecettesvg.util.Casting;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author enochbukasa
 */
public class CleRepartitionBusiness {

    private static final Dao myDao = new Dao();

    public static List<CleRepartition> getLisCleRepartitionByCode(String valueSearch, BigDecimal amountnet) {

        try {
            String sqlQuery = GeneralConst.EMPTY_STRING;
            List<CleRepartition> listCleRep = new ArrayList<>();

            sqlQuery = SQLQueryCleRepartition.SELECT_CLE_REPARTITION_BY_CODE;

            List<CleRepartition> listCleRepartition = (List<CleRepartition>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(CleRepartition.class), false, valueSearch);

            if (listCleRepartition.size() > 0) {
                BigDecimal taux = new BigDecimal(GeneralConst.Number.ZERO);
                BigDecimal amountRepart = new BigDecimal(GeneralConst.Number.ZERO);
                BigDecimal cleRepart = new BigDecimal(GeneralConst.Number.ZERO);
                BigDecimal pourcentage = new BigDecimal(GeneralConst.CENT_POURCENT);

                for (CleRepartition listCl : listCleRepartition) {

                    CleRepartition objCleRep = new CleRepartition();

                    taux = listCl.getTaux();
                    amountRepart = amountnet.multiply(taux);
                    cleRepart = amountRepart.divide(pourcentage);

                    objCleRep.setCode(listCl.getCode());
                    objCleRep.setEntiteBeneficiaire(listCl.getEntiteBeneficiaire());
                    objCleRep.setEtat(listCl.getEtat());
                    objCleRep.setTaux(cleRepart);
                    objCleRep.setFkArticleBudgetaire(listCl.getFkArticleBudgetaire());
                    objCleRep.setIsTresorPart(listCl.getIsTresorPart());

                    listCleRep.add(objCleRep);

                }
            }
            return listCleRep;
        } catch (Exception e) {
            throw e;
        }
    }

    public static CleRepartition getCleTresor(String codeAB) {
        String sqlQuery = SQLQueryCleRepartition.SELECT_CLE_REPARTITION_BY_CODE_TRESOR;

        List<CleRepartition> listCleRepartition;
        CleRepartition cleRep = null;

        try {
            listCleRepartition = (List<CleRepartition>) myDao.getDaoImpl().find(sqlQuery,
                    Casting.getInstance().convertIntoClassType(CleRepartition.class), false, codeAB);
        } catch (Exception e) {
            throw e;
        }
        if (listCleRepartition.size() > 0) {
            for (CleRepartition cl : listCleRepartition) {
                if (cl.getIsTresorPart()) {
                    cleRep = cl;
                    break;
                }
            }
        }
        return cleRep;
    }

    // save cle repartition
    public static boolean savecleRepartition(List<CleRepartition> clerepList) {
        
        HashMap<String, Object[]> bulkQuery = new HashMap<>();
        int counter = 0;
        Boolean result = false;
        try {
            for (CleRepartition repartition : clerepList) {
                bulkQuery.put(counter + ":INSERT INTO T_CLE_REPARTITION(FK_ARTICLE_BUDGETAIRE,ENTITE_BENEFICIAIRE,IS_TRESOR_PART,TAUX,ETAT) VALUES (?1,?2,?3,?4,?5)", new Object[]{
                    repartition.getFkArticleBudgetaire().getCode(),
                    repartition.getEntiteBeneficiaire(),
                    repartition.getIsTresorPart(),
                    repartition.getTaux(),
                    repartition.getEtat()
                });
                counter++;
            }

            result = executeQueryBulkInsert(bulkQuery);
        } catch (Exception e) {
            throw (e);
        }
        return result;
    }

    public static boolean DeleteCleRepartition(String code) {
        boolean result;
        String sqlQuery = SQLQueryCleRepartition.DELETE_CLE_REPARTITION;
        result = myDao.getDaoImpl().executeStoredProcedure(sqlQuery, code);
        return result;
    }

    public static List<CleRepartition> loadCleRepartition(String code) {

        try {
            String query = SQLQueryGestionArticleBudgetaire.SELECT_ALL_CLE_REPARTITION;
            List<CleRepartition> clerepartition = (List<CleRepartition>) myDao.getDaoImpl().find(query,
                    Casting.getInstance().convertIntoClassType(CleRepartition.class), false, code);
            return clerepartition;

        } catch (Exception e) {
            throw e;
        }

    }
}
