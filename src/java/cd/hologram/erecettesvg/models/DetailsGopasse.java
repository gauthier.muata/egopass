/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author KASHALA
 */
@Entity
@Cacheable(false)
@Table(name = "T_DETAILS_GOPASSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsGopasse.findAll", query = "SELECT d FROM DetailsGopasse d"),
    @NamedQuery(name = "DetailsGopasse.findByIdDetails", query = "SELECT d FROM DetailsGopasse d WHERE d.idDetails = :idDetails"),
    @NamedQuery(name = "DetailsGopasse.findByPassager", query = "SELECT d FROM DetailsGopasse d WHERE d.passager = :passager"),
    @NamedQuery(name = "DetailsGopasse.findByPieceIdentite", query = "SELECT d FROM DetailsGopasse d WHERE d.pieceIdentite = :pieceIdentite"),
    @NamedQuery(name = "DetailsGopasse.findBySexePassager", query = "SELECT d FROM DetailsGopasse d WHERE d.sexePassager = :sexePassager"),
    @NamedQuery(name = "DetailsGopasse.findByDateGeneration", query = "SELECT d FROM DetailsGopasse d WHERE d.dateGeneration = :dateGeneration"),
    @NamedQuery(name = "DetailsGopasse.findByDateUtilisation", query = "SELECT d FROM DetailsGopasse d WHERE d.dateUtilisation = :dateUtilisation"),
    @NamedQuery(name = "DetailsGopasse.findByCodeTicketGenerate", query = "SELECT d FROM DetailsGopasse d WHERE d.codeTicketGenerate = :codeTicketGenerate"),
    @NamedQuery(name = "DetailsGopasse.findByEtat", query = "SELECT d FROM DetailsGopasse d WHERE d.etat = :etat")})
public class DetailsGopasse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdDetails")
    private Integer idDetails;
    @Size(max = 250)
    @Column(name = "Passager")
    private String passager;
    @Size(max = 50)
    @Column(name = "PieceIdentite")
    private String pieceIdentite;
    @Size(max = 50)
    @Column(name = "Sexe_Passager")
    private String sexePassager;
    @Column(name = "Date_Generation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateGeneration;
    @Column(name = "Date_Utilisation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUtilisation;
    @Column(name = "ETAT")
    private Integer etat;
    @Size(max = 50)
    @Column(name = "CodeTicket_Generate")
    private String codeTicketGenerate;
    @JoinColumn(name = "Fk_Gopasse", referencedColumnName = "IdGopasse")
    @ManyToOne
    private GoPasse fkGopasse;
    @JoinColumn(name = "Agent_Creat", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentCreat;
    @JoinColumn(name = "Agent_Utilisation", referencedColumnName = "CODE")
    @ManyToOne
    private Agent agentUtilisateur;
    @JoinColumn(name = "Fk_Compagnie", referencedColumnName = "CODE")
    @ManyToOne
    private Personne fkCompagnie;
    @JoinColumn(name = "Fk_Aeroport_Provenance", referencedColumnName = "IdAeroport")
    @ManyToOne
    private Aeroport fkAeroportProvenance;
    @JoinColumn(name = "Fk_Aeroport_Destination", referencedColumnName = "IdAeroport")
    @ManyToOne
    private Aeroport fkAeroportDestination;

    public DetailsGopasse() {
    }

    public DetailsGopasse(Integer idDetails) {
        this.idDetails = idDetails;
    }

    public Integer getIdDetails() {
        return idDetails;
    }

    public void setIdDetails(Integer idDetails) {
        this.idDetails = idDetails;
    }

    public String getPassager() {
        return passager;
    }

    public void setPassager(String passager) {
        this.passager = passager;
    }

    public String getPieceIdentite() {
        return pieceIdentite;
    }

    public void setPieceIdentite(String pieceIdentite) {
        this.pieceIdentite = pieceIdentite;
    }

    public String getSexePassager() {
        return sexePassager;
    }

    public void setSexePassager(String sexePassager) {
        this.sexePassager = sexePassager;
    }

    public Date getDateGeneration() {
        return dateGeneration;
    }

    public void setDateGeneration(Date dateGeneration) {
        this.dateGeneration = dateGeneration;
    }

    public Date getDateUtilisation() {
        return dateUtilisation;
    }

    public void setDateUtilisation(Date dateUtilisation) {
        this.dateUtilisation = dateUtilisation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public String getCodeTicketGenerate() {
        return codeTicketGenerate;
    }

    public void setCodeTicketGenerate(String codeTicketGenerate) {
        this.codeTicketGenerate = codeTicketGenerate;
    }

    public GoPasse getFkGopasse() {
        return fkGopasse;
    }

    public void setFkGopasse(GoPasse fkGopasse) {
        this.fkGopasse = fkGopasse;
    }

    public Agent getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Agent agentCreat) {
        this.agentCreat = agentCreat;
    }

    public Agent getAgentUtilisateur() {
        return agentUtilisateur;
    }

    public void setAgentUtilisateur(Agent agentUtilisateur) {
        this.agentUtilisateur = agentUtilisateur;
    }

    public Personne getFkCompagnie() {
        return fkCompagnie;
    }

    public void setFkCompagnie(Personne fkCompagnie) {
        this.fkCompagnie = fkCompagnie;
    }

    public Aeroport getFkAeroportProvenance() {
        return fkAeroportProvenance;
    }

    public void setFkAeroportProvenance(Aeroport fkAeroportProvenance) {
        this.fkAeroportProvenance = fkAeroportProvenance;
    }

    public Aeroport getFkAeroportDestination() {
        return fkAeroportDestination;
    }

    public void setFkAeroportDestination(Aeroport fkAeroportDestination) {
        this.fkAeroportDestination = fkAeroportDestination;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetailsGopasse other = (DetailsGopasse) obj;
        return true;
    }

    @Override
    public String toString() {
        return "DetailsGopasse{" + "IdDetails=" + idDetails + '}';
    }

}
