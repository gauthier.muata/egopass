package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.Commandement;
import cd.hologram.erecettesvg.models.ParamContrainte;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Contrainte.class)
public class Contrainte_ { 

    public static volatile SingularAttribute<Contrainte, String> agentMaj;
    public static volatile SingularAttribute<Contrainte, BigDecimal> montantFraisPoursuite;
    public static volatile SingularAttribute<Contrainte, Date> dateCreat;
    public static volatile SingularAttribute<Contrainte, BigDecimal> montantRecouvrement;
    public static volatile ListAttribute<Contrainte, Commandement> commandementList;
    public static volatile SingularAttribute<Contrainte, Short> etat;
    public static volatile SingularAttribute<Contrainte, Date> dateMaj;
    public static volatile SingularAttribute<Contrainte, String> numeroDocument;
    public static volatile SingularAttribute<Contrainte, String> faitGenerateur;
    public static volatile SingularAttribute<Contrainte, BigDecimal> montantPenalite;
    public static volatile SingularAttribute<Contrainte, Boolean> etatImpression;
    public static volatile SingularAttribute<Contrainte, Date> dateEcheance;
    public static volatile SingularAttribute<Contrainte, ParamContrainte> idParam;
    public static volatile SingularAttribute<Contrainte, String> id;
    public static volatile SingularAttribute<Contrainte, BigDecimal> montantTresorUrbain;
    public static volatile SingularAttribute<Contrainte, BigDecimal> montantDirectionRecettes;
    public static volatile SingularAttribute<Contrainte, String> agentCreat;
    public static volatile SingularAttribute<Contrainte, String> agentCommandement;
    public static volatile SingularAttribute<Contrainte, Amr> idAmr;
    public static volatile SingularAttribute<Contrainte, Date> dateReception;

}