/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_MODELE_DOCUMENT_IMPRESSION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModeleDocumentImpression.findAll", query = "SELECT m FROM ModeleDocumentImpression m"),
    @NamedQuery(name = "ModeleDocumentImpression.findById", query = "SELECT m FROM ModeleDocumentImpression m WHERE m.id = :id"),
    @NamedQuery(name = "ModeleDocumentImpression.findByTypeDocument", query = "SELECT m FROM ModeleDocumentImpression m WHERE m.typeDocument = :typeDocument"),
    @NamedQuery(name = "ModeleDocumentImpression.findByModele", query = "SELECT m FROM ModeleDocumentImpression m WHERE m.modele = :modele"),
    @NamedQuery(name = "ModeleDocumentImpression.findByDescription", query = "SELECT m FROM ModeleDocumentImpression m WHERE m.description = :description"),
    @NamedQuery(name = "ModeleDocumentImpression.findByEtat", query = "SELECT m FROM ModeleDocumentImpression m WHERE m.etat = :etat")})
public class ModeleDocumentImpression implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 20)
    @Column(name = "TYPE_DOCUMENT")
    private String typeDocument;
    @Size(max = 50)
    @Column(name = "MODELE")
    private String modele;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ETAT")
    private Short etat;

    public ModeleDocumentImpression() {
    }

    public ModeleDocumentImpression(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Short getEtat() {
        return etat;
    }

    public void setEtat(Short etat) {
        this.etat = etat;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModeleDocumentImpression)) {
            return false;
        }
        ModeleDocumentImpression other = (ModeleDocumentImpression) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.ModeleDocumentImpression[ id=" + id + " ]";
    }
    
}
