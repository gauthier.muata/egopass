package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Complement;
import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.TypeComplement;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ComplementForme.class)
public class ComplementForme_ { 

    public static volatile SingularAttribute<ComplementForme, String> code;
    public static volatile SingularAttribute<ComplementForme, FormeJuridique> formeJuridique;
    public static volatile ListAttribute<ComplementForme, Complement> complementList;
    public static volatile SingularAttribute<ComplementForme, TypeComplement> complement;
    public static volatile SingularAttribute<ComplementForme, Boolean> etat;

}