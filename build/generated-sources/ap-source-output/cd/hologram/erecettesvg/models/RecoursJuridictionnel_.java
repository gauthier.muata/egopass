package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Reclamation;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(RecoursJuridictionnel.class)
public class RecoursJuridictionnel_ { 

    public static volatile SingularAttribute<RecoursJuridictionnel, Date> dateCreat;
    public static volatile SingularAttribute<RecoursJuridictionnel, String> motifRecours;
    public static volatile SingularAttribute<RecoursJuridictionnel, String> dateAudience;
    public static volatile SingularAttribute<RecoursJuridictionnel, Reclamation> fkReclamation;
    public static volatile SingularAttribute<RecoursJuridictionnel, String> id;
    public static volatile SingularAttribute<RecoursJuridictionnel, String> numeroEnregistrementGreffe;
    public static volatile SingularAttribute<RecoursJuridictionnel, Agent> agentCreat;
    public static volatile SingularAttribute<RecoursJuridictionnel, Integer> etat;
    public static volatile SingularAttribute<RecoursJuridictionnel, Date> dateDepotRecours;

}