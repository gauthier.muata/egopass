/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_LOGIN_WEB")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LoginWeb.findAll", query = "SELECT l FROM LoginWeb l"),
    @NamedQuery(name = "LoginWeb.findByFkPersonne", query = "SELECT l FROM LoginWeb l WHERE l.fkPersonne = :fkPersonne"),
    @NamedQuery(name = "LoginWeb.findByUsername", query = "SELECT l FROM LoginWeb l WHERE l.username = :username"),
    @NamedQuery(name = "LoginWeb.findByPassword", query = "SELECT l FROM LoginWeb l WHERE l.password = :password"),
    @NamedQuery(name = "LoginWeb.findByEtat", query = "SELECT l FROM LoginWeb l WHERE l.etat = :etat"),
    @NamedQuery(name = "LoginWeb.findByDateCreation", query = "SELECT l FROM LoginWeb l WHERE l.dateCreation = :dateCreation"),
    @NamedQuery(name = "LoginWeb.findByAgentCreat", query = "SELECT l FROM LoginWeb l WHERE l.agentCreat = :agentCreat"),
    @NamedQuery(name = "LoginWeb.findByMail", query = "SELECT l FROM LoginWeb l WHERE l.mail = :mail"),
    @NamedQuery(name = "LoginWeb.findByTelephone", query = "SELECT l FROM LoginWeb l WHERE l.telephone = :telephone")})
public class LoginWeb implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "FK_PERSONNE")
    private String fkPersonne;
    @Size(max = 50)
    @Column(name = "USERNAME")
    private String username;
    @Size(max = 250)
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "ETAT")
    private Integer etat;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "AGENT_CREAT")
    private Integer agentCreat;
    @Size(max = 30)
    @Column(name = "MAIL")
    private String mail;
    @Size(max = 15)
    @Column(name = "TELEPHONE")
    private String telephone;
    @JoinColumn(name = "FK_PERSONNE", referencedColumnName = "CODE", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Personne personne;

    public LoginWeb() {
    }

    public LoginWeb(String fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public String getFkPersonne() {
        return fkPersonne;
    }

    public void setFkPersonne(String fkPersonne) {
        this.fkPersonne = fkPersonne;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Integer getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(Integer agentCreat) {
        this.agentCreat = agentCreat;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Personne getPersonne() {
        return personne;
    }

    public void setPersonne(Personne personne) {
        this.personne = personne;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fkPersonne != null ? fkPersonne.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoginWeb)) {
            return false;
        }
        LoginWeb other = (LoginWeb) object;
        if ((this.fkPersonne == null && other.fkPersonne != null) || (this.fkPersonne != null && !this.fkPersonne.equals(other.fkPersonne))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.LoginWeb[ fkPersonne=" + fkPersonne + " ]";
    }
    
}
