/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var modalSearchDocumentDepotDeclaration;

var ResearchTypeDocument;

var lblFormeJuridique, lblNomAssujetti, lblAddresse, lblNumeroBordereau,
        lblNumeroDeclaration, lblMontantPercu, lblBanque,
        lblNbDocumentDepotDeclation, textObservDoc;

var ResearchValueDepotDeclaration;

var btnSearchEditDepotDeclaration, btnJoindreDocument, btnEnregistrerDeclaration;

var codeResearch, idDocument, archivages, codeFormeJurid,
        CodeAdressePersonne, numBordereau, numDeclaration;

var codeTypeDoc, archivesDepotDeclar, codeAssujetti;

var documentDeclare, archivagesDepotDecl, totalMontantPercu;

var tableDetailBordereau;

var codeDoc = undefined;

var detailDepotDecl;

var textObservation;


$(function () {

    mainNavigationLabel.text('DECLARATION');
    secondNavigationLabel.text('Edition d\'un depôt de déclaration');

    removeActiveMenu();
    linkSubMenuGestionDepotDeclaration.addClass('active');


    modalSearchDocumentDepotDeclaration = $('#modalSearchDocumentDepotDeclaration');

    lblFormeJuridique = $('#lblFormeJuridique');
    lblNomAssujetti = $('#lblNomAssujetti');
    lblAddresse = $('#lblAddresse');
    lblNumeroBordereau = $('#lblNumeroBordereau');
    lblNumeroDeclaration = $('#lblNumeroDeclaration');
    lblMontantPercu = $('#lblMontantPercu');
    lblBanque = $('#lblBanque');
    lblNbDocumentDepotDeclation = $('#lblNbDocumentDepotDeclation');

    textObservDoc = $('#textObservDoc');

    tableDetailBordereau = $('#tableDetailBordereau');

    ResearchValueDepotDeclaration = $('#ResearchValueDepotDeclaration');

    ResearchTypeDocument = $('#ResearchTypeDocument');

    btnSearchEditDepotDeclaration = $('#btnSearchEditDepotDeclaration');
    btnJoindreDocument = $('#btnJoindreDocument');
    btnEnregistrerDeclaration = $('#btnEnregistrerDeclaration');

    codeTypeDoc = 'TD00000035';

    ResearchTypeDocument.on('change', function (e) {
        codeResearch = ResearchTypeDocument.val();

        if (codeResearch === "BR") {
            ResearchValueDepotDeclaration.attr('placeholder', 'Numero du bordereau');
            ResearchValueDepotDeclaration.val('');
        } else if (codeResearch === "DEC") {
            ResearchValueDepotDeclaration.attr('placeholder', 'Numero de la note de taxation');
            ResearchValueDepotDeclaration.val('');
            typeReseach = '2';
        }
    });

    btnSearchEditDepotDeclaration.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (ResearchValueDepotDeclaration.val() === '') {
            alertify.alert('Veuillez d\'abord fournir le numéro du document');
        } else {

            loadInfoDocumentDepotDeclaration();
        }
    });

    btnEnregistrerDeclaration.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir enregistrer ce dépôt déclaration ?', function () {

            var listDetailBordereau = prepareDetailDepotDeclaration();

            saveDepotDeclaration(listDetailBordereau);

        });

    });

    btnJoindreDocument.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (archivesDepotDeclar == '') {
            initUpload(codeDoc, codeTypeDoc);
        } else {
            initUpload(archivesDepotDeclar, codeTypeDoc);
        }

    });


    modalSearchDocumentDepotDeclaration.modal('show');

    displayDetailBordereau('');

});




function loadInfoDocumentDepotDeclaration() {

    var typeDoc = ResearchTypeDocument.val();
    var numeroDocument = ResearchValueDepotDeclaration.val().trim();

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'typeDocument': typeDoc,
            'numeroDocument': numeroDocument,
            'operation': 'searchDocumentDepotDeclaration'
        },
        beforeSend: function () {
            modalSearchDocumentDepotDeclaration.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            modalSearchDocumentDepotDeclaration.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '-2') {
                alertify.alert('Veuillez faire un dépôt dans le système ou Valider votre paiement.');
                return;
            } else if (response == '-3') {
                alertify.alert('Cette déclaration a été déjà déposée.');
                return;
            } else if (response == '-4') {
                alertify.alert('Cette déclaration n\'est pas encore payée.');
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne corresponde au critère de recherche fournie.');
                return;
            } else {
                setTimeout(function () {
                    documentDeclare = null;
                    documentDeclare = JSON.parse(JSON.stringify(response));
                    displayInfoDocumentDeclarer(documentDeclare);
                    modalSearchDocumentDepotDeclaration.modal('hide');
                }
                , 1);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalSearchDocumentDepotDeclaration.unblock();
            showResponseError();
        }
    });
}

function displayInfoDocumentDeclarer(document) {

    var firstLineAB = '';

    lblFormeJuridique.html(document.formeJuridique);
    lblNomAssujetti.html(document.nomComplet);
    lblAddresse.html(document.adressePersonne);
    lblNumeroBordereau.html(document.numeroBordereau);
    lblNumeroDeclaration.html(document.numeroDeclaration);
    lblMontantPercu.html(formatNumber(document.totalMontantPercu, document.devise));
    lblBanque.html(document.libelleBanque);
    numBordereau = document.numeroBordereau;
    numDeclaration = document.numeroDeclaration;
    codeAssujetti = document.codePersonne;
    codeFormeJurid = document.codeFormeJuridique;
    codeAdressePersonne = document.codeAdressePersonne;
    totalMontantPercu = document.totalMontantPercu;

    btnEnregistrerDeclaration.attr('style', 'display: inline;float: right');


    detailDepotDecl = JSON.parse(document.listDetailBordereau);
    if (detailDepotDecl.length > 0) {
        displayDetailBordereau(detailDepotDecl);
    }

}

function displayDetailBordereau(detailBordereau) {

    var firstLineAB = '';

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:20%">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:center;width:12%">PERIODE DECLARATION</th>';
    tableContent += '<th style="text-align:left;width:40%">BIEN</th>';
    tableContent += '<th style="text-align:center;width:15%;text-align:right" scope="col">MONTANT PAYE</th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < detailBordereau.length; i++) {

        if (detailBordereau[i].libelleArticleBudgetaire > 250) {
            firstLineAB = detailBordereau[i].libelleArticleBudgetaire.substring(0, 250) + ' ...';
        } else {
            firstLineAB = detailBordereau[i].libelleArticleBudgetaire;
        }

        var descriptionBien = '';

        if (detailBordereau[i].isImmobilier === '1') {

            var natureInfo = 'Nature : ' + '<span style="font-weight:bold">' + detailBordereau[i].libelleTypeBien + '</span>';
            var usageInfo = 'Usage : ' + '<span style="font-weight:bold">' + detailBordereau[i].usageName + '</span>';
            var categorieInfo = 'Catégorie : ' + '<span style="font-weight:bold">' + detailBordereau[i].tarifName + '</span>';
            var communeInfo = 'Commune : ' + '<span style="font-weight:bold">' + detailBordereau[i].communeName + '</span>';
            var quartierInfo = 'Quartier : ' + '<span style="font-weight:bold">' + detailBordereau[i].quartierName + '</span>';

            var bienInfo = '<span style="font-weight:bold">' + detailBordereau[i].intituleBien + '</span>';

            var adresseInfo = detailBordereau[i].adresseBien.toUpperCase();

            descriptionBien = bienInfo + '<br/><br/>' + natureInfo + '<br/>' + usageInfo + '<br/>' + categorieInfo + '<br/>' + communeInfo + '<br/>' + quartierInfo + '<hr/>' + adresseInfo;

        } else {

            var bienInfo2, natureInfo2, categorieInfo2, adresseInfo2 = '';

            if (detailBordereau[i].type == 1) {

                bienInfo2 = '<span style="font-weight:bold">' + detailBordereau[i].intituleBien + '</span>';
                natureInfo2 = 'Nature : ' + '<span style="font-weight:bold">' + detailBordereau[i].libelleTypeBien + '</span>';
                categorieInfo2 = 'Catégorie : ' + '<span style="font-weight:bold">' + detailBordereau[i].tarifName + '</span>';
                adresseInfo2 = detailBordereau[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;

            } else if (detailBordereau[i].type == 3) {

                bienInfo2 = '<span style="font-weight:bold">' + detailBordereau[i].intituleBien + '</span>';
                natureInfo2 = 'Nature : ' + '<span style="font-weight:bold">' + detailBordereau[i].libelleTypeBien + '</span>';
                categorieInfo2 = 'Destination : ' + '<span style="font-weight:bold">' + detailBordereau[i].tarifName + '</span>';
                adresseInfo2 = detailBordereau[i].adresseBien.toUpperCase();

                descriptionBien = bienInfo2 + '<br/><br/>' + natureInfo2 + '<br/>' + categorieInfo2 + '<hr/>' + adresseInfo2;
            }


        }

        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;font-weight:bold;width:20%;vertical-align:middle">' + detailBordereau[i].codeOfficiel.toUpperCase() + ' - ' + firstLineAB.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:center;vertical-align:middle;width:12%">' + detailBordereau[i].IntitulePeriodicite + '</td>';
        tableContent += '<td style="text-align:left;vertical-align:middle;width:40%">' + descriptionBien + '</td>';
        //tableContent += '<td style="text-align:left;vertical-align:middle">' + detailBordereau[i].adresseBien.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:right;vertical-align:middle;font-weight:bold;color:green;width:15%">' + formatNumber(detailBordereau[i].montantPercu, detailBordereau[i].devise) + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableDetailBordereau.html(tableContent);

    var myDataTable = tableDetailBordereau.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des détails de bordereau est vide",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: false,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function getDocumentsToUpload() {

    var nombre = 0;

    archivesDepotDeclar = getUploadedData();

    nombre = JSON.parse(archivesDepotDeclar).length;

    switch (nombre) {
        case 0:
            lblNbDocumentDepotDeclation.text('');
            break;
        case 1:
            lblNbDocumentDepotDeclation.html('1 documents');
            break;
        default:
            lblNbDocumentDepotDeclation.html(nombre + ' documents');
    }
}

function prepareDetailDepotDeclaration() {

    var listDetailBordereau = [];

    if (detailDepotDecl.length > 0) {

        for (var i = 0; i < detailDepotDecl.length; i++) {

            var detailBordereau = {};
            detailBordereau.devise = detailDepotDecl[i].devise;
            detailBordereau.codeArticleBudgetaire = detailDepotDecl[i].codeArticleBudgetaire;
            detailBordereau.periodicite = detailDepotDecl[i].periodicite;
            detailBordereau.montantPercu = detailDepotDecl[i].montantPercu;
            detailBordereau.intituleBien = detailDepotDecl[i].intituleBien;
            detailBordereau.compteBancaire = detailDepotDecl[i].compteBancaire;
            detailBordereau.numeroBordereau = detailDepotDecl[i].numeroBordereau;
            detailBordereau.datePaiement = detailDepotDecl[i].datePaiement;
            listDetailBordereau.push(detailBordereau);
        }

        listDetailBordereau = JSON.stringify(listDetailBordereau);

    }

    return listDetailBordereau;
}

function saveDepotDeclaration(listDocument) {

    if (listDocument == 0) {
        alertify.alert('Veuillez sélectionner un assujetti');
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'saveDepotDeclaration',
            'userId': userData.idUser,
            'observation': textObservDoc.val(),
            'codePersonne': codeAssujetti,
            'numeroBordereau': numBordereau,
            'listDetailBordereau': listDocument,
            'codeFormeJuridique': codeFormeJurid,
            'codeAdressePersonne': codeAdressePersonne,
            'totalMontantPercu': totalMontantPercu,
            'numeroDeclaration': numDeclaration,
            'service': userData.serviceCode,
            'site': userData.SiteCode,
            'archives': archivesDepotDeclar
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du dépôt déclaration en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'enregistrement du dépôt déclaration s\'est effectué avec succès.');
                    printRecepisse(numBordereau);
                    resetData();
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement de la déclaration.');
                } else if (response == '-3') {
                    resetData();
                    alertify.alert('Ce retrait a déjà une déclaration.');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });

}

function resetData() {

    displayDetailBordereau('');
    textObservDoc.val('');
    codeAssujetti = '';
    numBordereau = '';
    codeFormeJurid = '';
    numDeclaration = '';
    archivesDepotDeclar = '';
    lblNbDocumentDepotDeclation.text('');


    lblFormeJuridique.html('');
    lblNomAssujetti.html('');
    lblAddresse.html('');
    lblNumeroBordereau.html('');
    lblNumeroDeclaration.html('');
    lblMontantPercu.html('');
    lblBanque.html('');

}

function printRecepisse(numeroBordereau) {

    if (numeroBordereau == '') {
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'declaration_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'numeroBordereau': numeroBordereau,
            'operation': 'printRecepisse'
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {
                $.unblockUI();
                setDocumentContent(response);
                window.open('visualisation-document', '_blank');

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}