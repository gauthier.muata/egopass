/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.util;

import cd.hologram.erecettesvg.constants.GeneralConst;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author moussa.toure
 */
public class MailSender {

    private String sender;
    private String reciever;
    private String host;
    private String subject;
    private String message;
    private String password;
    private String cc;
    private Date date;
    private String name;

    public MailSender() {

    }

    public MailSender(String host, String sender, String password, String reciever, String subject, String message) {

        this.sender = sender;
        this.reciever = reciever;
        this.host = host;
        this.subject = subject;
        this.message = message;
        this.password = password;
    }

    public MailSender(String host, String sender, String reciever, String subject, String message) {

        this.sender = sender;
        this.reciever = reciever;
        this.host = host;
        this.subject = subject;
        this.message = message;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReciever() {
        return reciever;
    }

    public void setReciever(String reciever) {
        this.reciever = reciever;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void sendMail() throws AddressException, MessagingException {

        try {

            Properties properties = System.getProperties();

            properties.setProperty("mail.smtp.host", this.host);

            Session session = Session.getDefaultInstance(properties);

            MimeMessage mimeMessage = new MimeMessage(session);

            mimeMessage.setFrom(new InternetAddress(this.sender));

            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(this.reciever));

            mimeMessage.setSentDate(date);

            mimeMessage.setSubject(this.subject, "UTF-8");

            mimeMessage.setText(this.message, "UTF-8");

            Transport transport = session.getTransport("smtp");

            transport.connect(this.host, this.sender, this.password);

            transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());

            transport.close();

        } catch (Exception e) {

            throw e;

        }

    }
    
    public String sendMailWithAPIV2(String username, String passwword) {

        try {

            SSLContext sc = SSLContext.getInstance(GeneralConst.Mail.CRYPTO_TLSv1);

            TrustManager[] trustAllCerts = {new InsecureTrustManager()};
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

            String usernameMailjet = username;
            String passwordMailjet = passwword;

            Client client = ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier(allHostsValid).build();

            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(usernameMailjet, passwordMailjet);

            client.register(feature);

            WebTarget webTarget = client.target(this.host);

            String body = String.format(GeneralConst.Mail.MAIL_PARAM_JSON,
                    this.sender,
                    this.reciever,
                    this.reciever,
                    this.subject,
                    this.subject,
                    this.message);
            
            //body = "{\"Messages\":[{\"From\":{\"Email\":\"info@snedac.com\",\"Name\":\"e-Mairie\"},\"To\":[{\"Email\":\"gauthier.muata@hologram.cd\",\"Name\":\"gauthier.muata@hologram.cd\"}],\"Subject\":\"Bienvenue à ERECETTES HAUT-KATANGA\",\"TextPart\":\"Bienvenue à ERECETTES HAUT-KATANGA\",\"HTMLPart\":\"<html><head> <style>body{background-color: #f6f6f6; font-family: sans-serif; font-size: 14px}.main{background-color: #fff; box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 20px; border-radius: 3px; margin-top: 40px; box-shadow: 8px 10px 0px rgba(0, 0, 0, 0.1)}.footer{clear: both; margin-top: 10px; text-align: center; color: #999; font-size: 12px}</style></head><body> <div class=main> <br/> Bonjour <b>MUTUNDA  ASLINE</b>, <br/> <br/> Nous vous confirmons l'activation de votre compte pour la télé declaration. <br/> <br/> Dorénavant, vous pouvez faire vos déclarations et paiements en ligne. <br/> <br/> Ci-dessous vos informations de connexion à l'application. <br/> <br/> Numéro de télédeclaration  : <b>NTD-001107-2021</b> <br/> Mot de passe par défaut :122<b></b> <br/> <br/> Cliquez sur ce lien pour vous connectez <b>https://demo.hologram.cd:8080/teledeclaration/</b> <br/> <br/> Merci, l'équipe erecettes <br/> <br/> </div><div class=footer> <br/> <span>&copy; Copyright 2019&nbsp;&nbsp;</span><a>Hologram Identification Services</a></div></body></html>\"}]}";

            String response = webTarget.request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(body), String.class);

            JSONObject jSONObject = new JSONObject(response);

            JSONArray array = jSONObject.getJSONArray(GeneralConst.Mail.MESSAGE);

            String status = array.getJSONObject(GeneralConst.Numeric.ZERO)
                    .getString(GeneralConst.Mail.STATUS);

            if (status.equals(GeneralConst.Mail.SUCCESS)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NoSuchAlgorithmException | KeyManagementException | JSONException e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }


    public String sendMailWithAPI(String username, String passwword) {

        try {

            SSLContext sc = SSLContext.getInstance(GeneralConst.Mail.CRYPTO_TLSv1);

            TrustManager[] trustAllCerts = {new InsecureTrustManager()};
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HostnameVerifier allHostsValid = new InsecureHostnameVerifier();

            String usernameMailjet = username;
            String passwordMailjet = passwword;

            Client client = ClientBuilder.newBuilder().sslContext(sc).hostnameVerifier(allHostsValid).build();

            HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(usernameMailjet, passwordMailjet);

            client.register(feature);

            WebTarget webTarget = client.target(this.host);

            String body = String.format(GeneralConst.Mail.MAIL_PARAM_JSON,
                    this.sender,
                    this.reciever,
                    this.reciever,
                    this.subject,
                    this.subject,
                    this.message);

            String response = webTarget.request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(body), String.class);

            JSONObject jSONObject = new JSONObject(response);

            JSONArray array = jSONObject.getJSONArray(GeneralConst.Mail.MESSAGE);

            String status = array.getJSONObject(GeneralConst.Numeric.ZERO)
                    .getString(GeneralConst.Mail.STATUS);

            if (status.equals(GeneralConst.Mail.SUCCESS)) {
                return GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                return GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (NoSuchAlgorithmException | KeyManagementException | JSONException e) {
            return GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }
    }

}
