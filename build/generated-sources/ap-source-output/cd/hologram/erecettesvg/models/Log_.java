package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.LogChampsChange;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Log.class)
public class Log_ { 

    public static volatile SingularAttribute<Log, String> pc;
    public static volatile SingularAttribute<Log, Date> dateAction;
    public static volatile SingularAttribute<Log, String> application;
    public static volatile ListAttribute<Log, LogChampsChange> logChampsChangeList;
    public static volatile SingularAttribute<Log, String> action;
    public static volatile SingularAttribute<Log, String> idOccurence;
    public static volatile SingularAttribute<Log, String> agentCreat;
    public static volatile SingularAttribute<Log, String> table;
    public static volatile SingularAttribute<Log, Integer> idLog;
    public static volatile SingularAttribute<Log, String> compte;

}