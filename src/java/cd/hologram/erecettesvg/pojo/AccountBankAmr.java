/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author PC
 */
public class AccountBankAmr implements Serializable {

    public String banqueName;
    public String compteCode;
    public String deviseName;

    public String getBanqueName() {
        return banqueName;
    }

    public void setBanqueName(String banqueName) {
        this.banqueName = banqueName;
    }

    public String getCompteCode() {
        return compteCode;
    }

    public void setCompteCode(String compteCode) {
        this.compteCode = compteCode;
    }

    public String getDeviseName() {
        return deviseName;
    }

    public void setDeviseName(String deviseName) {
        this.deviseName = deviseName;
    }

   

}
