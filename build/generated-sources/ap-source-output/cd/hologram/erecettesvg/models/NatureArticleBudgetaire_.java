package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(NatureArticleBudgetaire.class)
public class NatureArticleBudgetaire_ { 

    public static volatile SingularAttribute<NatureArticleBudgetaire, String> code;
    public static volatile ListAttribute<NatureArticleBudgetaire, ArticleBudgetaire> articleBudgetaireList;
    public static volatile SingularAttribute<NatureArticleBudgetaire, String> intitule;
    public static volatile SingularAttribute<NatureArticleBudgetaire, Short> etat;

}