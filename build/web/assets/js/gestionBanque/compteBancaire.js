/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnAddAccountBank;
var modalUpdateAccountBank;
var tableBanques;

var accountBankList = [];

var codeAccountBank, libelleAccountBank, selectDevise, selectBanque;

var codeAccount;

var btnDeleteBank;
var modalAccoutBank, tableAccountBank, idBank;

var btnDeleteAccountBank, btnSaveAccountBank, tableAccountBanks;

$(function () {

    mainNavigationLabel.text('Gestion des comptes bancaires');
    secondNavigationLabel.text('Edition d\'un compte bancaire');
    removeActiveMenu();
    linkMenuTaxation.addClass('active');

    codeBank = empty;

    btnAddAccountBank = $('#btnAddAccountBank');
    modalUpdateAccountBank = $('#modalUpdateAccountBank');
    tableBanques = $('#tableBanques');

    codeAccountBank = $('#codeAccountBank');
    libelleAccountBank = $('#libelleAccountBank');
    selectDevise = $('#selectDevise');
    selectBanque = $('#selectBanque');

    btnDeleteAccountBank = $('#btnDeleteAccountBank');
    btnSaveAccountBank = $('#btnSaveAccountBank');

    tableAccountBanks = $('#tableAccountBanks');
    idBank = $('#idBank');

    btnAddAccountBank.on('click', function (e) {
        e.preventDefault();

        codeAccount = empty;
        codeAccountBank.val(empty);
        libelleAccountBank.val(empty);

        btnDeleteAccountBank.attr('style', 'display:none');
        modalUpdateAccountBank.modal('show');

    });

    btnSaveAccountBank.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        checkFields();

    });

    btnDeleteAccountBank.on('click', function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        deleteAccountBank();

    });

    loadingBanks();
    loadCompteBanques();
});

function deleteAccountBank() {

    alertify.confirm('Etes-vous sûre de vouloir supprimer ce compte bancaire ?', function () {
        $.ajax({
            type: 'POST',
            url: 'banque_servlet',
            dataType: 'text',
            crossDomain: false,
            data: {
                'code': codeAccountBank.val(),
                'libelle': libelleAccountBank.val(),
                'devise': selectDevise.val(),
                'banque': selectBanque.val(),
                'userId': userData.idUser,
                'operation': 'deleteAccountBank'
            },
            beforeSend: function () {
                modalUpdateAccountBank.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Suppression compte bancaire en cours ...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {

                    modalUpdateAccountBank.unblock();

                    if (response == '-1' | response == '0') {

                        showResponseError();
                        return;

                    } else {
                        alertify.alert('La suppression du compte bancaire s\'est effectuée avec succès');
                        resetFields();
                        modalUpdateAccountBank.modal('hide');
                        loadCompteBanques();
                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                modalUpdateAccountBank.unblock();
                showResponseError();
            }
        });
    });
}

function resetFields() {

    codeAccount = empty;
    codeAccountBank.val(empty);
    libelleAccountBank.val(empty);
    selectDevise.val('0');
    selectBanque.val('0');
}

function checkFields() {

    if (codeAccountBank.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir le code du compte bancaire');
        codeAccountBank.focus()
        return;
    }

    if (libelleAccountBank.val() == empty) {
        alertify.alert('Veuillez d\'abord saisir le libellé du compte');
        libelleAccountBank.focus()
        return;
    }

    if (selectDevise.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner la devise du compte');
        selectDevise.focus()
        return;
    }

    if (selectBanque.val() == '0') {
        alertify.alert('Veuillez d\'abord saisir sélectionner la banque du compte');
        selectBanque.focus()
        return;
    }

    alertify.confirm('Etes-vous sûr de vouloir enregistrer ce compte bancaire ?', function () {
        saveAccountBank();
    });
}

function saveAccountBank() {

    $.ajax({
        type: 'POST',
        url: 'banque_servlet',
        dataType: 'text',
        crossDomain: false,
        data: {
            'code': codeAccountBank.val(),
            'libelle': libelleAccountBank.val(),
            'devise': selectDevise.val(),
            'banque': selectBanque.val(),
            'userId': userData.idUser,
            'operation': 'saveAccountBank'
        },
        beforeSend: function () {
            modalUpdateAccountBank.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement compte bancaire en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                modalUpdateAccountBank.unblock();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {
                    alertify.alert('L\'enregistrement du compte bancaire s\'est effectué avec succès');
                    resetFields();
                    modalUpdateAccountBank.modal('hide');
                    loadCompteBanques();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalUpdateAccountBank.unblock();
            showResponseError();
        }
    });
}

function loadCompteBanques() {

    $.ajax({
        type: 'POST',
        url: 'banque_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadAccountBank'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des comptes en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    accountBankList = JSON.parse(JSON.stringify(response));
                    printTableCompteBancaire(accountBankList);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printTableCompteBancaire(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:15%">CODE</th>';
    tableContent += '<th style="text-align:left;width:25%">LIBELLE</th>';
    tableContent += '<th style="text-align:left;width:10%">DEVISE</th>';
    tableContent += '<th style="text-align:left;width:30%">BANQUE</th>';
    tableContent += '<th style="text-align:center;width:5%"></th>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        var btnEditAccountBank = '<button class="btn btn-success" onclick="editAccountBank(\'' + result[i].accountBankCode + '\')"><i class="fa fa-edit"></i></button>';


        tableContent += '<tr>';
        tableContent += '<td style="text-align:left;width:15%">' + result[i].accountBankCode + '</td>';
        tableContent += '<td style="text-align:left;width:25%">' + result[i].accountBankLibelle + '</td>';
        tableContent += '<td style="text-align:left;width:10%">' + result[i].accountBankDevise + '</td>';
        tableContent += '<td style="text-align:left;width:30%">' + result[i].accountBankBanqueName + '</td>';
        tableContent += '<td style="text-align:center;width:5%">' + btnEditAccountBank + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableAccountBanks.html(tableContent);

    tableAccountBanks.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Le panier des articles budgétaires est vide",
            search: "Rechercher par intitulé _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 10,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3
    });
}

function editAccountBank(code) {

    for (var i = 0; i < accountBankList.length; i++) {

        if (accountBankList[i].accountBankCode == code) {

            codeAccount = code;

            codeAccountBank.val(accountBankList[i].accountBankCode);
            libelleAccountBank.val(accountBankList[i].accountBankLibelle);
            selectDevise.val(accountBankList[i].accountBankDevise);
            selectBanque.val(accountBankList[i].accountBankBanqueCode);

            btnDeleteAccountBank.attr('style', 'display:inline');

            modalUpdateAccountBank.modal('show');

            break;
        }
    }

}

function loadingBanks() {

    $.ajax({
        type: 'POST',
        url: 'banque_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadBankv2'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement des banques en cours ...</h5>'});
        },
        success: function (response)
        {

            setTimeout(function () {

                $.unblockUI();

                if (response == '-1' | response == '0') {

                    showResponseError();
                    return;

                } else {

                    var banqueUserList = JSON.parse(JSON.stringify(response));
                    var dataBanque;

                    dataBanque += '<option value="0">--</option>';

                    for (var i = 0; i < banqueUserList.length; i++) {
                        dataBanque += '<option value="' + banqueUserList[i].bankCode + '">' + banqueUserList[i].bankNameLong + '</option>';
                    }

                    selectBanque.html(dataBanque);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }
    });

}