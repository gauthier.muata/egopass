/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var
        codeResearch,
        messageEmptyValue,
        messageAvancee,
        codeServiceAssiette,
        codeFormeJuridique,
        modalRechercheAvanceeAssujetti;

var tableAssujetti;

var inputSearchAssujetti,
        inputRechercheAvenceeAssujetti;

var btnSearchAssujetti,
        btnRechercheAvancee,
        btnAdvencedSearchAssujet,
        btnCreateAssujetti;

var cmbSearchType,
        cmbServiceAssiette,
        cmbFormeJuridique;

var tempPersonneList;
var modalChooseModule, selectModule, btnDisplay;
var codeAssujetti;

$(function () {

    mainNavigationLabel.text('REPERTOIRE');
    secondNavigationLabel.text('Répertoire des contribuables');

    removeActiveMenu();
    linkMenuRepertoire.addClass('active');

    inputSearchAssujetti = $('#inputSearchAssujetti');
    inputRechercheAvenceeAssujetti = $('#inputSearchAssujettiAvancee');

    modalRechercheAvanceeAssujetti = $('#modalRechercheAvanceeAssujetti');

    cmbServiceAssiette = $('#selectServiceAssiette');
    cmbFormeJuridique = $('#cmbFormeJuridique');

    tableAssujetti = $('#tableAssujetti');

    modalChooseModule = $('#modalChooseModule');
    selectModule = $('#selectModule');
    btnDisplay = $('#btnDisplay');

    btnSearchAssujetti = $('#btnSearchAssujetti');
    btnRechercheAvancee = $('#idRechercerAvancesAssujetti');
    btnAdvencedSearchAssujet = $('#btnAdvencedSearchAssujetti');
    btnCreateAssujetti = $('#btnCreateAssujetti');

    btnSearchAssujetti.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        loadAssujettis();
    });

    btnDisplay.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectModule.val() == '0') {
            alertify.alert('Veuillez d\'abord sélectionner le module valid !');
            return;
        } else {
            loadCompteCourant(codeAssujetti, selectModule.val());
        }

    });

    btnRechercheAvancee.click(function (e) {
        e.preventDefault();
        modalRechercheAvanceeAssujetti.modal('show');
    });

    btnAdvencedSearchAssujet.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        modalRechercheAvanceeAssujetti.modal('hide');
        loadAssujettisAvancee();
    });


    btnCreateAssujetti.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        window.location = 'identification';
    });

    cmbSearchType = $('#cmbSearchType');
    codeResearch = cmbSearchType.val();
    messageEmptyValue = 'Veuillez d\'abord saisir le nom de l\'assujetti';
    messageAvancee = 'Veuillez d\'abord saisir le critere de recherche.';

    loadFormeJuridique();
    printPersonnes('');

    cmbSearchType.on('change', function (e) {
        codeResearch = cmbSearchType.val();
        inputSearchAssujetti.attr('style', 'width:100%');
        inputSearchAssujetti.attr('class', 'form-control');
        inputSearchAssujetti.val('');

        if (codeResearch === '0') {
            messageEmptyValue = 'Veuillez saisir le nom de l\'assujetti.';
            inputSearchAssujetti.attr('placeholder', 'Veuillez saisir le nom de l\'assujetti');
        } else if (codeResearch === '1') {
            messageEmptyValue = 'Veuillez saisir le nif de l\'assujetti.';
            inputSearchAssujetti.attr('placeholder', 'Veuillez saisir le nif de l\'assujetti');
        }else if (codeResearch === '2') {
            messageEmptyValue = 'Veuillez saisir le numéro télédéclaration de l\'assujetti.';
            inputSearchAssujetti.attr('placeholder', 'Veuillez saisir le ntd de l\'assujetti');
        }else{
            messageEmptyValue = 'Veuillez saisir le numéro de téléphone de l\'assujetti.';
            inputSearchAssujetti.attr('placeholder', 'Veuillez saisir le téléphone de l\'assujetti');
        }

        //printPersonnes('');
    });

    var dataService = '';

    var AllServiceList = JSON.parse(userData.allServiceList);

    if (!controlAccess('SEARCH_FOR_ALL_SERVICES')) {

        dataService += '<option value="' + userData.serviceCode + '">' + userData.serviceAssiette + '</option>';
        var selectServiceContent = dataService;

        cmbServiceAssiette.html(selectServiceContent);

    } else {

        dataService += '<option value="*">*</option>';

        for (var i = 0; i < AllServiceList.length; i++) {
            dataService += '<option value="' + AllServiceList[i].serviceCode + '">' + AllServiceList[i].serviceName.toUpperCase() + '</option>';

        }
        var selectServiceContent = dataService;

        cmbServiceAssiette.html(selectServiceContent);
    }

    cmbServiceAssiette.val(userData.serviceCode);
    inputSearchAssujetti.attr('style', 'width:100%');
    inputSearchAssujetti.attr('class', 'form-control');
});

function loadFormeJuridique() {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadFormeJuridique'
        },
        beforeSend: function () {
        },
        success: function (response)
        {
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                var result = $.parseJSON(JSON.stringify(response));

                var dataFormeJuridique = '<option value ="*" style="font-weight:bold;font-style: italic">TOUS TYPE</option>';

                for (var i = 0; i < result.length; i++) {

                    dataFormeJuridique += '<option value =' + result[i].codeFormeJuridique + '>' + result[i].libelleFormeJuridique.toUpperCase() + '</option>';
                }

                cmbFormeJuridique.html(dataFormeJuridique);

            }
            , 1000);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            showResponseError();
        }
    });
}

function loadAssujettis() {

    if (inputSearchAssujetti.val().trim() === empty || inputSearchAssujetti.val().length < SEARCH_MIN_TEXT) {
        showEmptySearchMessage();
        return;
    }

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAssujettis',
            'typeSearch': codeResearch,
            'libelle': inputSearchAssujetti.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var personneList = JSON.parse(JSON.stringify(response));

                if (personneList.length > 0) {

                    tempPersonneList = [];
                    for (var i = 0; i < personneList.length; i++) {
                        var personne = new Object();
                        personne.libelleFormeJuridique = personneList[i].libelleFormeJuridique;
                        personne.nif = personneList[i].nif;
                        personne.nom = personneList[i].nom;
                        personne.postNom = personneList[i].postNom;
                        personne.prenom = personneList[i].prenom;
                        personne.telephone = personneList[i].telephone;
                        personne.email = personneList[i].email;
                        personne.chaine = personneList[i].chaine;
                        personne.codePersonne = personneList[i].codePersonne;
                        personne.nomComplet = personneList[i].nomComplet;
                        personne.userName = personneList[i].userName;
                        tempPersonneList.push(personne);
                    }

                    printPersonnes(tempPersonneList);

                } else {

                    printPersonnes('');
                    
                    var value = '<span style="font-weight:bold">' + inputSearchAssujetti.val() + '</span>';
                    
                    switch (codeResearch) {
                        case '0':
                            alertify.alert('Ce nom : ' + value + ', n\'existe pas dans le répertoire des assujettis');
                            break;
                        case '1':
                            alertify.alert('Ce nif : ' + value + ', n\'existe pas dans le répertoire des assujettis');
                            break;
                        case '2':
                            alertify.alert('Ce téléphone : ' + value + ', n\'existe pas dans le répertoire des assujettis');
                            break;
                    }
                    btnCreateAssujetti.attr('style', 'margin-left: 5px;display:inline');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function printPersonnes(tempPersonneList) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="width:9%" >NIF</th>';
    header += '<th style="width:10%" >NTD</th>';
    header += '<th style="width:23%">CONTRIBUABLE</th>';
    header += '<th style="width:13%">TYPE</th>';
    header += '<th style="width:10%">CONTACT</th>';
    header += '<th hidden="true" >EMAIL</th>';
    header += '<th style="width:27%" >ADRESSE PRINCIPALE</th>';
    header += '<th style="width:8%;text-align:center"> </th>';
    header += '<th hidden="true" scope="col"> Code personne </th>';
    header += '</tr></thead>';

    var body = '<tbody id="tbodyPersonnes">';

    for (var i = 0; i < tempPersonneList.length; i++) {
        
        var phoneInfo = 'Téléphone : ' + '<span style="font-weight:bold">' + tempPersonneList[i].telephone + '</span>';
        var mailInfo = 'E-mail : ' + '<span style="font-weight:bold">' + tempPersonneList[i].email + '</span>';

        var contactInfo = phoneInfo + '<br/><br/>' + mailInfo;
        
        body += '<tr>';
        body += '<td style="vertical-align:middle">' + tempPersonneList[i].nif + '</td>';
        body += '<td style="vertical-align:middle">' + tempPersonneList[i].userName + '</td>';
        body += '<td style="vertical-align:middle">' + tempPersonneList[i].nomComplet + '</td>';
        body += '<td style="vertical-align:middle">' + tempPersonneList[i].libelleFormeJuridique + '</td>';
        body += '<td style="vertical-align:middle">' + contactInfo + '</td>';
        body += '<td hidden="true">' + tempPersonneList[i].email + '</td>';
        body += '<td style="vertical-align:middle">' + tempPersonneList[i].chaine + '</td>';
        body += '<td style="text-align:center;vertical-align:middle"><a onclick="editPersonne(\'' + tempPersonneList[i].codePersonne + '\')" class="btn btn-success"><i class="fa fa-edit"></i></a>&nbsp;<a onclick="disabledPersonne(\'' + tempPersonneList[i].codePersonne + '\')" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>&nbsp;<a style="display:none" onclick="callModalCompteCourant(\'' + tempPersonneList[i].codePersonne + '\')" class="btn btn-warning"><i class="fa fa-edit"></i></a></td>';
        body += '<td hidden="true">' + tempPersonneList[i].codePersonne + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableAssujetti.html(tableContent);

    tableAssujetti.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par le nom _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 50,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
        /*,columnDefs: [
            {"visible": false, "targets": 2}
        ],
        order: [[2, 'asc']],
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(2, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="6">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }*/
    });
}

function editPersonne(codePersonne) {
    for (var i = 0; i < tempPersonneList.length; i++) {

        if (tempPersonneList[i].codePersonne === codePersonne) {
            var nomComplet = tempPersonneList[i].nomComplet;
            alertify.confirm('Voulez-vous afficher les détails de ' + '<span style="font-weight:bold">' + nomComplet +'</span>'+ ' ?', function () {
                window.location = 'identification?id=' + btoa(codePersonne);
            });
            break;
        }
    }
}

function disabledPersonne(codePersonne) {

    for (var i = 0; i < tempPersonneList.length; i++) {

        if (tempPersonneList[i].codePersonne === codePersonne) {
            var nomComplet = tempPersonneList[i].nomComplet;
            alertify.confirm('Voulez-vous désactiver ' + nomComplet + ' ?', function () {
                validateDisabledAssujetti(codePersonne);
            });
            break;
        }
    }
}

function validateDisabledAssujetti(codePersonne) {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'disabledAssujetti',
            'codePersonne': codePersonne
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement de l\'assujetti ...</h5>'})

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {

                    alertify.alert('L\'assujetti est désactivé avec succès');

                    for (var i = 0; i < tempPersonneList.length; i++) {
                        if (tempPersonneList[i].codePersonne === codePersonne) {
                            tempPersonneList.splice(i, 1);
                            printPersonnes(tempPersonneList);
                            break;
                        }
                    }

                } else if (response == '0') {
                    alertify.alert('Echec désactivation de l\'assujetti.');
                } else {
                    showResponseError();
                }

            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function loadAssujettisAvancee() {

    $.ajax({
        type: 'POST',
        url: 'identification_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'loadAssujettiAvancee',
            'valueSearch': inputRechercheAvenceeAssujetti.val(),
            'codeService': cmbServiceAssiette.val(),
            'codeFormeJuridique': cmbFormeJuridique.val()
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var personneList = JSON.parse(JSON.stringify(response));

                if (personneList.length > 0) {

                    tempPersonneList = [];

                    for (var i = 0; i < personneList.length; i++) {
                        var personne = new Object();
                        personne.libelleFormeJuridique = personneList[i].libelleFormeJuridique;
                        personne.nif = personneList[i].nif;
                        personne.nom = personneList[i].nom;
                        personne.postNom = personneList[i].postNom;
                        personne.prenom = personneList[i].prenom;
                        personne.telephone = personneList[i].telephone;
                        personne.email = personneList[i].email;
                        personne.chaine = personneList[i].chaine;
                        personne.codePersonne = personneList[i].codePersonne;
                        personne.nomComplet = personneList[i].nomComplet;

                        tempPersonneList.push(personne);
                    }

                    printPersonnes(tempPersonneList);

                } else {
                    printPersonnes('');
                    btnCreateAssujetti.attr('style', 'margin-left: 5px;display:inline');
                }
            }
            , 1000);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function callModalCompteCourant(codePersonne) {

    alertify.confirm('Etes-vous sûr de vouloir afficher le compte courant fiscal de cet assujetti ?', function () {

        codeAssujetti = codePersonne;
        modalChooseModule.modal('show');

    });
}
