/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.pojo;

import java.io.Serializable;

/**
 *
 * @author gauthier.muata
 */
public class CommandementPrint implements Serializable {

    String logo;
    String codeQR;
    String numImpot;
    String nomAssujetti;
    String adresseAssujetti;
    String telephoneAssujetti;
    String emailAssujetti;
    String dateImpression;

    String datePrintDocument;
    String userPrintDocument;
    String functionUserDocument;
    
    String numeroCommandement;
    String dateAndHeurePrintDocumentToText;
    String agentHuissier;
    String faitGenerateur;
    String dateEmission;
    String dateExigibilite;
    String montantPoursuivi;
    String fraisPoursuite;
    String delaiCommandement;
    
    String compteBancaireDgrk;
    String dateEmissionContrainte;
    String nomReceptionniste;
    String qualiteReceptionniste;
    String dateReception;

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCodeQR() {
        return codeQR;
    }

    public void setCodeQR(String codeQR) {
        this.codeQR = codeQR;
    }

    public String getNumImpot() {
        return numImpot;
    }

    public void setNumImpot(String numImpot) {
        this.numImpot = numImpot;
    }

    public String getNomAssujetti() {
        return nomAssujetti;
    }

    public void setNomAssujetti(String nomAssujetti) {
        this.nomAssujetti = nomAssujetti;
    }

    public String getAdresseAssujetti() {
        return adresseAssujetti;
    }

    public void setAdresseAssujetti(String adresseAssujetti) {
        this.adresseAssujetti = adresseAssujetti;
    }

    public String getTelephoneAssujetti() {
        return telephoneAssujetti;
    }

    public void setTelephoneAssujetti(String telephoneAssujetti) {
        this.telephoneAssujetti = telephoneAssujetti;
    }

    public String getEmailAssujetti() {
        return emailAssujetti;
    }

    public void setEmailAssujetti(String emailAssujetti) {
        this.emailAssujetti = emailAssujetti;
    }

    public String getDateImpression() {
        return dateImpression;
    }

    public void setDateImpression(String dateImpression) {
        this.dateImpression = dateImpression;
    }

    public String getDatePrintDocument() {
        return datePrintDocument;
    }

    public void setDatePrintDocument(String datePrintDocument) {
        this.datePrintDocument = datePrintDocument;
    }

    public String getUserPrintDocument() {
        return userPrintDocument;
    }

    public void setUserPrintDocument(String userPrintDocument) {
        this.userPrintDocument = userPrintDocument;
    }

    public String getFunctionUserDocument() {
        return functionUserDocument;
    }

    public void setFunctionUserDocument(String functionUserDocument) {
        this.functionUserDocument = functionUserDocument;
    }

    public String getNumeroCommandement() {
        return numeroCommandement;
    }

    public void setNumeroCommandement(String numeroCommandement) {
        this.numeroCommandement = numeroCommandement;
    }

    public String getDateAndHeurePrintDocumentToText() {
        return dateAndHeurePrintDocumentToText;
    }

    public void setDateAndHeurePrintDocumentToText(String dateAndHeurePrintDocumentToText) {
        this.dateAndHeurePrintDocumentToText = dateAndHeurePrintDocumentToText;
    }

    public String getAgentHuissier() {
        return agentHuissier;
    }

    public void setAgentHuissier(String agentHuissier) {
        this.agentHuissier = agentHuissier;
    }

    public String getFaitGenerateur() {
        return faitGenerateur;
    }

    public void setFaitGenerateur(String faitGenerateur) {
        this.faitGenerateur = faitGenerateur;
    }

    public String getDateEmission() {
        return dateEmission;
    }

    public void setDateEmission(String dateEmission) {
        this.dateEmission = dateEmission;
    }

    public String getDateExigibilite() {
        return dateExigibilite;
    }

    public void setDateExigibilite(String dateExigibilite) {
        this.dateExigibilite = dateExigibilite;
    }

    public String getMontantPoursuivi() {
        return montantPoursuivi;
    }

    public void setMontantPoursuivi(String montantPoursuivi) {
        this.montantPoursuivi = montantPoursuivi;
    }

    public String getFraisPoursuite() {
        return fraisPoursuite;
    }

    public void setFraisPoursuite(String fraisPoursuite) {
        this.fraisPoursuite = fraisPoursuite;
    }

    public String getDelaiCommandement() {
        return delaiCommandement;
    }

    public void setDelaiCommandement(String delaiCommandement) {
        this.delaiCommandement = delaiCommandement;
    }

    public String getNomReceptionniste() {
        return nomReceptionniste;
    }

    public void setNomReceptionniste(String nomReceptionniste) {
        this.nomReceptionniste = nomReceptionniste;
    }

    public String getQualiteReceptionniste() {
        return qualiteReceptionniste;
    }

    public void setQualiteReceptionniste(String qualiteReceptionniste) {
        this.qualiteReceptionniste = qualiteReceptionniste;
    }

    public String getDateReception() {
        return dateReception;
    }

    public void setDateReception(String dateReception) {
        this.dateReception = dateReception;
    }

    public String getCompteBancaireDgrk() {
        return compteBancaireDgrk;
    }

    public void setCompteBancaireDgrk(String compteBancaireDgrk) {
        this.compteBancaireDgrk = compteBancaireDgrk;
    }

    public String getDateEmissionContrainte() {
        return dateEmissionContrainte;
    }

    public void setDateEmissionContrainte(String dateEmissionContrainte) {
        this.dateEmissionContrainte = dateEmissionContrainte;
    }
    
    
    
}
