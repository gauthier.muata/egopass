package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.Ua;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(UaSite.class)
public class UaSite_ { 

    public static volatile SingularAttribute<UaSite, String> agentMaj;
    public static volatile SingularAttribute<UaSite, Site> site;
    public static volatile SingularAttribute<UaSite, Date> dateCreat;
    public static volatile SingularAttribute<UaSite, Integer> id;
    public static volatile SingularAttribute<UaSite, String> agentCreat;
    public static volatile SingularAttribute<UaSite, Ua> ua;
    public static volatile SingularAttribute<UaSite, Short> etat;
    public static volatile SingularAttribute<UaSite, Date> dateMaj;

}