package cd.hologram.erecettesvg.models;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DocWorkFlow.class)
public class DocWorkFlow_ { 

    public static volatile SingularAttribute<DocWorkFlow, String> agentMaj;
    public static volatile SingularAttribute<DocWorkFlow, String> process;
    public static volatile SingularAttribute<DocWorkFlow, Date> dateCreat;
    public static volatile SingularAttribute<DocWorkFlow, String> ordre;
    public static volatile SingularAttribute<DocWorkFlow, String> document;
    public static volatile SingularAttribute<DocWorkFlow, Short> obligatoire;
    public static volatile SingularAttribute<DocWorkFlow, String> ua;
    public static volatile SingularAttribute<DocWorkFlow, Short> etat;
    public static volatile SingularAttribute<DocWorkFlow, Short> archivageObligatoire;
    public static volatile SingularAttribute<DocWorkFlow, Date> dateMaj;
    public static volatile SingularAttribute<DocWorkFlow, BigInteger> workFlow;
    public static volatile SingularAttribute<DocWorkFlow, Long> id;
    public static volatile SingularAttribute<DocWorkFlow, String> agentCreat;

}