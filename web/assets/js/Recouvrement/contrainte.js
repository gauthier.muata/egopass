/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var inputSearchValue;
var selectSearchContrainte;
var tableRegistreContrainte, tableRegistreContrainte2;
var btnShowAdvancedContrainte;
var btnSimpleSearchContrainte;
var btnAdvencedSearchRole;
var typeSearch = '1';
var isAdvanced;
var inputDateDebut, inputdateLast, inputNomTiers, inputFunctionTiers;
var selectService, selectSite;
var tempContrainte = [];
var lblNameAssujetti, lblArticleRole;
var idContrainte;
var modalDetailContrainte, modalTiersPersonnes;
var btnCreateCommandement, btnImprimerContrainte;
var tableNouveauTitrePerception;
var idAssujetti;
var btnShowAccuserReception, btnValiderTiers;
var restPaiyerContrainte;

var lblMinistere, lblService, lblDateDebut, lblDateFin, lblSite, lblProvince, lblEntite, lblAgent;
var isAdvance;

var advancedSearchParam;
var codeAssujetti = '';
var idHuissier = '';
var param;

$(function () {

    mainNavigationLabel.text('POURSUITE');
    secondNavigationLabel.text('Registre des contraintes');

    removeActiveMenu();
    linkMenuPoursuite.addClass('active');

    if (!controlAccess('10002')) {
        window.location = 'dashboard';
    }

    inputSearchValue = $('#inputSearchValue');
    selectSearchContrainte = $('#selectSearchContrainte');
    tableRegistreContrainte = $('#tableRegistreContrainte');
    tableRegistreContrainte2 = $('#tableRegistreContrainte2');
    btnSimpleSearchContrainte = $('#btnSimpleSearchContrainte');
    btnShowAdvancedContrainte = $('#btnShowAdvancedContrainte');
    btnAdvencedSearchRole = $('#btnAdvencedSearchRole');
    inputDateDebut = $('#inputDateDebut');
    inputdateLast = $('#inputdateLast');
    inputNomTiers = $('#inputNomTiers');
    inputFunctionTiers = $('#inputFunctionTiers');
    selectService = $('#selectService');
    selectSite = $('#selectSite');
    lblNameAssujetti = $('#lblNameAssujetti');
    lblArticleRole = $('#lblArticleRole');
    modalDetailContrainte = $('#modalDetailContrainte');
    btnCreateCommandement = $('#btnCreateCommandement');
    modalTiersPersonnes = $('#modalTiersPersonnes');
    tableNouveauTitrePerception = $('#tableNouveauTitrePerception');
    btnImprimerContrainte = $('#btnImprimerContrainte');
    btnShowAccuserReception = $('#btnShowAccuserReception');
    btnValiderTiers = $('#btnValiderTiers');

    lblMinistere = $('#lblMinistere');
    lblService = $('#lblService');
    lblDateDebut = $('#lblDateDebut');
    lblDateFin = $('#lblDateFin');
    lblSite = $('#lblSite');
    isAdvance = $('#isAdvance');
    lblEntite = $('#lblEntite');
    lblProvince = $('#lblProvince');
    lblAgent = $('#lblAgent');


    var data = '';
    if (userData.isHuissier == '1') {
        var data = '<option value="2" >Assujetti</option>';
        data += '<option value="1" >Article du rôle</option>';
    } else {
        var data = '<option value="2" >Assujetti</option>';
        data += '<option value="1" >Article du rôle</option>';
        data += '<option value="3" >Huissier</option>';
    }
    selectSearchContrainte.html(data);

    selectSearchContrainte.on('change', function () {

        var critere = selectSearchContrainte.val();
        if (critere == '1') {
            typeSearch = '1';
            inputSearchValue.attr('placeholder', 'Article du rôle');
            inputSearchValue.removeAttr('disabled');
            inputSearchValue.val('');
        } else if (critere == '2') {
            typeSearch = '2';
            inputSearchValue.attr('placeholder', 'Assujetti');
            inputSearchValue.attr('disabled', 'true');
            inputSearchValue.val('');
            assujettiModal.modal('show');
        } else {
            if (userData.isHuissier == '1') {
                typeSearch = '3';
            } else {
                typeSearch = '3';
                inputSearchValue.attr('placeholder', 'Huissier');
                inputSearchValue.attr('disabled', 'true');
                inputSearchValue.val('');
                modalAdvancedSearchAgent.modal('show');
            }
        }
    });

    btnSimpleSearchContrainte.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectSearchContrainte.val() === '2') {
            assujettiModal.modal('show');
        } else if (selectSearchContrainte.val() === '3') {
            modalAdvancedSearchAgent.modal('show');
        } else {
            isAdvanced = '0';
            isAdvance.attr('style', 'display: none');
            getCurrentSearchParam(0);
            getContrainte();
        }
    });

    btnShowAdvancedContrainte.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        modalRechercheAvanceeNC.modal('show');
    });

    btnAdvencedSearch.click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        isAdvanced = '1';
        modalRechercheAvanceeNC.modal('hide');
        isAdvance.attr('style', 'display: block;');
        getCurrentSearchParam(1);
        getContrainte();
    });

    btnCreateCommandement.click(function (e) {
        
        e.preventDefault();

      if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir générer un commandement ?', function () {

            generateCommandement();
            
        });

    });

    btnValiderTiers.click(function (e) {
        e.preventDefault();
    });

    btnShowAccuserReception.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        initAccuseReceptionUI(idContrainte, 'CTR');
    });

    btnImprimerContrainte.click(function (e) {
        
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        
        printContrainte();
    });

    $('#btnValidateSearchAgent').click(function (e) {
       
        e.preventDefault();
        
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (selectAgentHuissier.val() == null) {
            alertify.alert('Veuillez sélectionner un huissier');
            return;
        }

        modalAdvancedSearchAgent.modal('hide');

        inputSearchValue.val($('#selectAgentHuissier option:selected').text());

        idHuissier = selectAgentHuissier.val();

        isAdvanced = '0';
        isAdvance.attr('style', 'display: none');
        getCurrentSearchParam(0);
        getContrainte();

    });

    setTimeout(function () {

        var paramDashboard = getDashboardParam();

        if (paramDashboard == 'null') {

            setTimeout(function () {
                btnAdvencedSearch.trigger('click');
            }, 1000);

        } else {

            modeResearch = '2';
            advancedSearchParam = JSON.parse(paramDashboard);

            setTimeout(function () {
                isAdvanced = '1';
                getContrainte();
            }, 1000);

        }

    }, 1000);

});

function loadContrainte(result) {

    var etatContrainte = "";
    tempContrainte = [];

    var header = '<thead style="background-color:#0085c7;color:white" id="headerTable"><tr>';
    header += '<th>ASUUJETI</th>';
    header += '<th>ARTICLE EXTRAIT DE RÔLE</th>';
    header += '<th>DATE CREATION</th>';
    header += '<th>DATE RECEPTION</th>';
    header += '<th>DATE EXI.</th>';
    header += '<th>HUISSIER</th>';
    header += '<th>ETAT</th>';
    header += '<th style="text-align:right">MONTANT DÛ</th>';
    header += '<th style="text-align:right">MONTANT PAYE</th>';
    header += '<th style="text-align:right">RESTE A PAYER</th>';
    header += '<th></th>';
    header += '</tr></thead>';

    var data = '';

    var sumMontantDuCDF = 0;
    var sumMontantPayerCDF = 0;

    for (var i = 0; i < result.length; i++) {

        var montantDuExtrait = getSumMontantDuExtraitById(JSON.parse(result[i].titrePerceptionList));
        sumMontantDuCDF += montantDuExtrait;
        var montantPayeExtrait = getSumMontantPayeExtraitById(JSON.parse(result[i].titrePerceptionList));
        sumMontantPayerCDF += montantPayeExtrait;
        var RestePayerExtrait = montantDuExtrait - montantPayeExtrait;

        var contrainte = {};

        contrainte.contrainteId = result[i].contrainteId;
        contrainte.articleRole = result[i].articleRole;
        contrainte.assujettiName = result[i].assujettiName;
        contrainte.codeAssujetti = result[i].assujettiCode;
        contrainte.titrePerceptionList = result[i].titrePerceptionList;
        contrainte.dateReceptionContrainte = result[i].dateReceptionContrainte;
        contrainte.hasTitrePerception = result[i].hasTitrePerception;
        contrainte.echeanceContrainteDepasser = result[i].echeanceContrainteDepasser;
        contrainte.nameHuissier = result[i].nameHuissier;
        contrainte.restePayerContrainte = RestePayerExtrait;
        contrainte.etat = result[i].stateContrainte;
        contrainte.codeOfficiel = result[i].codeOfficiel;
        contrainte.serviceAssiette = result[i].serviceAssiette;
        tempContrainte.push(contrainte);

        if (result[i].niveauExtraitRole == 'N5') {
            etatContrainte = "(Niveau : ATD)";
        } else if (result[i].niveauExtraitRole == 'N4') {
            etatContrainte = "(Niveau : PV DE SAISIE)";
        } else if (result[i].niveauExtraitRole == 'N3') {
            etatContrainte = "(Niveau : COMMANDEMENT)";
        } else if (result[i].niveauExtraitRole == 'N2') {
            etatContrainte = "(Niveau : CONTRAINTE)";
        } else if (result[i].niveauExtraitRole == 'N1') {
            etatContrainte = "(Niveau : DERNIER AVERTISSEMENT)";
        } else if (result[i].niveauExtraitRole == 'N0') {

            if (result[i].stateContrainte == 1) {
                etatContrainte = "VALIDER";
            } else if (result[i].stateContrainte == 2) {
                etatContrainte = "EN ATTENTE DE VALIDATION";
            } else {
                etatContrainte = "";
            }

        }

        var firstLineASS = '';
        var firstLineAB = '';

        if (result[i].assujettiName.length > 30) {
            firstLineASS = result[i].assujettiName.substring(0, 30) + ' ...';
        } else {
            firstLineASS = result[i].assujettiName;
        }

        if (result[i].articleRole.length > 40) {
            firstLineAB = result[i].articleRole.substring(0, 40) + ' ...';
        } else {
            firstLineAB = result[i].articleRole;
        }

        data += '<tbody id="bodyTable">';
        data += '<tr>';
        data += '<td style="text-align:left;width:20%" title="' + result[i].assujettiName + '">' + firstLineASS + '</td>';
        data += '<td style="text-align:left;width:12%" title="' + result[i].articleRole + '">' + firstLineAB + '</td>';
        data += '<td style="text-align:left;">' + result[i].dateCreateContrainte + '</td>';
        data += '<td style="text-align:left">' + result[i].dateReceptionContrainte + '</td>';
        data += '<td style="text-align:left">' + result[i].dateEcheanceContrainte + '</td>';
        data += '<td style="text-align:left">' + result[i].nameHuissier + '</td>';
        data += '<td style="text-align:left">' + etatContrainte + '</td>';
        data += '<td style="text-align:right">' + formatNumber(montantDuExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(montantPayeExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:right">' + formatNumber(RestePayerExtrait, 'CDF') + '</td>';
        data += '<td style="text-align:center;width:6%;vertical-align:middle;"> <a  onclick="showModalDetailRole(\'' + result[i].contrainteId + '\')" class="btn btn-warning"><i class="fa fa-list"></i></a></td>';
        data += '</tr>';
    }
    data += '</tbody>';

    data += '<tfoot>';

    data += '<tr><th colspan="7" style="text-align:right;font-size:16px;vertical-align:middle">TOTAL GENERAL </th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th style="text-align:right;font-size:18px;color:red"></th><th></th></tr>';

    data += '</tfoot>';

    var TableContent = header + data;

    tableRegistreContrainte.html(TableContent);

    tableRegistreContrainte.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        order: [[5, 'asc']],
        searching: false,
        pageLength: 5,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 10,
        dom: 'Bfrtip',
        columnDefs: [{
                targets: 1,
                className: 'noVis'
            }],
        footerCallback: function (row, data, start, end, display) {
            var api = this.api(), data;

            $(api.column(7).footer()).html(
                    formatNumber(sumMontantDuCDF, 'CDF'));

            $(api.column(8).footer()).html(
                    formatNumber(sumMontantPayerCDF, 'CDF'));

            $(api.column(9).footer()).html(
                    formatNumber(sumMontantDuCDF - sumMontantPayerCDF, 'CDF'));
        }
    });
}

function getContrainte() {

    if (isAdvanced == '1') {

        try {

            var service = advancedSearchParam.serviceLibelle;
            var site = advancedSearchParam.siteLibelle;
            var entite = advancedSearchParam.entiteLibelle;
            var province = advancedSearchParam.provinceLibelle;
            var agent = advancedSearchParam.agentName;

            lblProvince.text(getsearchbarText(advancedSearchParam.province.length, advancedSearchParam.provinceLibelle));
            lblProvince.attr('title', province);

            lblEntite.text(getsearchbarText(advancedSearchParam.entite.length, advancedSearchParam.entiteLibelle));
            lblEntite.attr('title', entite);

            lblService.text(getsearchbarText(advancedSearchParam.service.length, advancedSearchParam.serviceLibelle));
            lblService.attr('title', service);

            lblSite.text(getsearchbarText(advancedSearchParam.site.length, advancedSearchParam.siteLibelle));
            lblSite.attr('title', site);

            lblAgent.text(getsearchbarText(advancedSearchParam.agent.length, advancedSearchParam.agentName));
            lblAgent.attr('title', agent);

            lblDateDebut.text(advancedSearchParam.dateDebut);
            lblDateFin.text(advancedSearchParam.dateFin);

        } catch (err) {
            location.reload(true);
        }
    }

    var valueSearch = inputSearchValue.val().trim();

    if (selectSearchContrainte.val() === '2') {
        valueSearch = codeAssujetti;
    } else if (selectSearchContrainte.val() === '3') {
        valueSearch = idHuissier;
    }

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': 'loadContrainte',
            'typeSearch': selectSearchContrainte.val(),
            'isAdvanced': isAdvanced,
            'dateDebut': advancedSearchParam.dateDebut,
            'dateFin': advancedSearchParam.dateFin,
            'valueSearch': valueSearch,
            'codeService': JSON.stringify(advancedSearchParam.service),
            'codeSite': JSON.stringify(advancedSearchParam.site),
            'codeEntite': JSON.stringify(advancedSearchParam.entite),
            'codeAgent': JSON.stringify(advancedSearchParam.agent),
            'isHuissier': userData.isHuissier,
            'idUser': userData.idUser
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Recherche en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                alertify.alert('Aucune donnée ne correspond au critère de recherche fourni.');
                loadContrainte('');
                return;
            }

            setTimeout(function () {
                var ContrainteList = JSON.parse(JSON.stringify(response));
                loadContrainte(ContrainteList);
            }
            , 1);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function showModalDetailRole(contrainteId) {

    if (checkSession()) {
        showSessionExpiredMessage();
        return;
    }


    for (var i = 0; i < tempContrainte.length; i++) {

        if (tempContrainte[i].contrainteId == contrainteId) {

            lblArticleRole.html(tempContrainte[i].articleRole);
            lblNameAssujetti.html(tempContrainte[i].assujettiName);
            idContrainte = contrainteId;
            idAssujetti = tempContrainte[i].codeAssujetti;
            restPaiyerContrainte = tempContrainte[i].restePayerContrainte;

            var detailTitrePerception;

            if (tempContrainte[i].hasTitrePerception != 0) {

                detailTitrePerception = JSON.parse(tempContrainte[i].titrePerceptionList);
                printTitrePerception(detailTitrePerception);

            } else {

                printTitrePerception('');
            }

            if (tempContrainte[i].dateReceptionContrainte != empty) {

                btnShowAccuserReception.attr('style', 'display: none');

            } else {

                btnShowAccuserReception.removeAttr('style', 'display: inline');
            }

            if (tempContrainte[i].etat == 3) {

                btnCreateCommandement.attr('style', 'display: none');

            } else {

                if (tempContrainte[i].dateReceptionContrainte != empty) {

                    btnCreateCommandement.removeAttr('style', 'display: inline');

                } else {

                    btnCreateCommandement.attr('style', 'display: none');
                }

                if (tempContrainte[i].restePayerContrainte == 0) {

                    btnCreateCommandement.attr('style', 'display: none');
                }
            }


        }
    }

    modalDetailContrainte.modal('show');

}

function printTitrePerception(listDetailRole) {

    tempExtraitRoleId = [];

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th scope="col"> </th>';
    header += '<th scope="col" > EXERCICE </th>';
    header += '<th scope="col" > SERVICE D\'ASSIETTE </th>';
    header += '<th scope="col" > ARTICLE BUDGETAIRE </th>';
    header += '<th scope="col" > TYPE </th>';
    header += '<th scope="col" > TYPE DOCUMENT </th>';
    header += '<th scope="col" > TITRE DE PERCEPTION</th>';
    header += '<th scope="col" > DATE EXIGIBILITE </th>';
    header += '<th scope="col" style="text-align:right"> MONTANT DÛ </th>';
    header += '<th scope="col" style="text-align:right"> PENALITE DÛ </th>';
    header += '</tr></thead>';

    var body = '<tbody>';
    var firstLineAB = '';
    var firstLineSA = '';

    for (var i = 0; i < listDetailRole.length; i++) {

        var extraitRole = {};

        extraitRole.documentReference = listDetailRole[i].documentReference;
        extraitRole.documentReferenceManuel = listDetailRole[i].documentReferenceManuel;
        extraitRole.typeDocument = listDetailRole[i].typeDocument;
        extraitRole.amountPenalite = 0;

        tempExtraitRoleId.push(extraitRole);

        var penaliteDu = listDetailRole[i].TOTAL_PENALITE;

        if (listDetailRole[i].libelleArticleBudgetaire.length > 40) {
            firstLineAB = listDetailRole[i].libelleArticleBudgetaire.substring(0, 40) + ' ...';
        } else {
            firstLineAB = listDetailRole[i].libelleArticleBudgetaire.toUpperCase();
        }

        if (listDetailRole[i].SERVICE.length > 40) {
            firstLineSA = listDetailRole[i].SERVICE.substring(0, 40) + ' ...';
        } else {
            firstLineSA = listDetailRole[i].SERVICE;
        }

        var etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">NON PAYE</a></center>';

        if (listDetailRole[i].isApured == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:green">PAYE </a></center>';
        } else if (listDetailRole[i].isApured == false && listDetailRole[i].isPaid == true) {
            etatPaiement = '<br/><center><a href="#" style="font-weight:bold;color:red">PAYE </a></center>';
        }

        body += '<tr>';
        body += '<td class="details-control" style="width:3%;text-align:center;"></td>';
        body += '<td style="width:5%">' + listDetailRole[i].exerciceFiscal + '</td>';

        body += '<td style="width:8%" title="' + listDetailRole[i].SERVICE + '">'
                + firstLineSA.toUpperCase() + '</td>';

        body += '<td style="text-align:left;width:20%;" title="' + listDetailRole[i].libelleArticleBudgetaire
                + '">' + firstLineAB + '</td>';

        body += '<td style="width:7%;">' + listDetailRole[i].TYPE + '</td>';
        body += '<td style="width:7%;">' + listDetailRole[i].typeDocument + '</td>';
        body += '<td style="width:9%;">' + listDetailRole[i].documentReferenceManuel + '</td>';
        body += '<td style="width:9%;">' + listDetailRole[i].echeance + '</td>';
        body += '<td style="text-align:right;width:9%;">' + formatNumber(listDetailRole[i].MONTANTDU, listDetailRole[i].devise) + ' ' + etatPaiement + '</td>';
        body += '<td style="text-align:right;width:9%;">' + formatNumber(penaliteDu, listDetailRole[i].devise) + '</td>';


        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;

    tableNouveauTitrePerception.html(tableContent);

    var myTableDetailRole = tableNouveauTitrePerception.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        columnDefs: [
            {"visible": false, "targets": 4}
        ],
        paging: true,
        lengthChange: false,
        tracking: false,
        order: [[4, 'asc']],
        pageLength: 4,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        datalength: 3,
        drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;
            api.column(4, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                            '<tr style="background-color:#e6e6e6; font-weight: bold"><td colspan="9">' + group + '</td></tr>'
                            );

                    last = group;
                }
            });
        }
    });

    $('#tableNouveauTitrePerception tbody').on('click', 'td.details-control', function () {

        var tr = $(this).closest('tr');

        var row = myTableDetailRole.row(tr);
        var dataDetail = myTableDetailRole.row(tr).data();
        var numeroTitre = dataDetail[6];

        if (row.child.isShown()) {

            row.child.hide();
            tr.removeClass('shown');

        } else {


            var tableTitreDependant = '<center><h4>LES TITRES DES PERCEPTIONS DEPENDANTS AU TITRE N° : ' + numeroTitre + '</h4><br/></center><table class="table table-bordered">';
            tableTitreDependant += '<thead><tr style="background-color:#e6ceac;color:black"><td>TITRE DE PERCEPTION</td><td>TYPE TITRE DE PERCEPTION</td><td>ECHEANCE</td><td>MONTANT DÛ</td><td>ETAT PAIEMENT</td></tr></thead>';

            var titreList = '';

            for (var j = 0; j < listDetailRole.length; j++) {

                if (listDetailRole[j].documentReferenceManuel == numeroTitre) {

                    titreList = JSON.parse(listDetailRole[j].TITRE_PERCEPTION_DEPENDACY);

                    for (var i = 0; i < titreList.length; i++) {

                        var etatPaiement = '<span style="font-weight:bold;color:red">NON PAYE</span>';

                        if (titreList[i].isApured == true) {
                            etatPaiement = '<span style="font-weight:bold;color:green">PAYE</span>';
                        } else if (titreList[i].isApured == false && titreList[i].isPaid == true) {
                            etatPaiement = '<span style="font-weight:bold;color:red">PAYE</span>';
                        }

                        tableTitreDependant += '<tr>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].documentReferenceManuel + '</td>';
                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].TYPE + '</td>';

                        tableTitreDependant += '<td style="text-align:left;width:20%;">' + titreList[i].echeance + '</td>';

                        tableTitreDependant += '<td style="text-align:right;width:25%;">' + formatNumber(titreList[i].MONTANTDU, 'CDF') + '</td>';
                        tableTitreDependant += '<td style="text-align:center:width:15%;">' + etatPaiement + '</td>';
                        tableTitreDependant += '</tr>';
                    }

                }
            }

            tableTitreDependant += '<tbody>';

            tableTitreDependant += '</tbody>';
            row.child(tableTitreDependant).show();

            tr.addClass('shown');

        }

    });
}

function generateCommandement() {

    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: false,
        data: {
            'operation': '1405',
            'idUser': userData.idUser,
            'codeAssujetti': idAssujetti,
            'contrainteId': idContrainte,
            'MONTANTDU': restPaiyerContrainte,
            'nomPersonneTiers': '...............',
            'fonctionPersonneTiers': '...............'
        },
        beforeSend: function () {

            modalDetailContrainte.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5 style="color:#286090" >Génération du commandement...</h5>'});
        },
        success: function (response)
        {
            modalDetailContrainte.unblock();
            if (response == '-1' || response == '0') {
                showResponseError();
                return;
            } else {
                setDocumentContent(response);
                window.open('visualisation-documents', '_blank');

                alertify.alert('Le commandement est généré avec succès.');
                idAssujetti = '';
                idContrainte = '';
                inputNomTiers.val('');
                inputFunctionTiers.val('');
                refrechDataAfterAccuserReception();
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            modalDetailContrainte.unblock();
            showResponseError();
        }

    });
}

function refrechDataAfterAccuserReception() {
    if (isAdvanced == '0') {
        //btnSimpleSearchContrainte.trigger('click');
        btnSimpleSearchContrainte.trigger('click');
    } else {
        btnAdvencedSearch.trigger('click');
    }

    modalDetailContrainte.modal('hide');
}

function printContrainte() {
    $.ajax({
        type: 'POST',
        url: 'recouvrement_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'documentReference': idContrainte,
            'operation': 'printDocumentRecouvrement'
        },
        beforeSend: function () {
            modalDetailContrainte.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Impression en cours ...</h5>'});
        },
        success: function (response)
        {
            modalDetailContrainte.unblock();

            if (response == '-1') {
                showResponseError();
                return;
            }
            if (response == '0') {
                alertify.alert('Aucun document(Contrainte) trouvé.');
                return;
            }
            setTimeout(function () {
                modalDetailContrainte.unblock();
                setDocumentContent(response);
                //window.open('document-paysage', '_blank');
                window.open('visualisation-documents', '_blank');
                setTimeout(function () {
                }, 2000);
            }
            , 1);

        },
        complete: function () {

        },
        error: function (xhr) {
            modalDetailContrainte.unblock();
            showResponseError();
        }

    });
}

function getSelectedAssujetiData() {
    isAdvanced = '0';
    isAdvance.attr('style', 'display: none');

    inputResearchValue.val(selectAssujettiData.nomComplet);
    codeAssujetti = selectAssujettiData.code;

    getCurrentSearchParam(0);
    getContrainte();

}

function getCurrentBudgetArticle() {

    isAdvanced = '0';
    isAdvance.attr('style', 'display: none');

    dataArrayBudgetArticles = infoBudgetArticle;
    inputResearchValue.val(dataArrayBudgetArticles[5]);
    codeBdgetArticle = dataArrayBudgetArticles[3];

    getCurrentSearchParam(0);
    getContrainte();

    budgetArticleModal.modal('hide');
}

function getSumMontantDuExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].MONTANTDU + listeDetailRole[i].TOTAL_PENALITE;
    }
    return sum;
}

function getSumMontantPayeExtraitById(listeDetailRole) {

    var sum = 0;
    for (var i = 0; i < listeDetailRole.length; i++) {

        sum += listeDetailRole[i].TOTAL_PAYEE_PENALITE + listeDetailRole[i].montantPaye;
    }

    return sum;
}
