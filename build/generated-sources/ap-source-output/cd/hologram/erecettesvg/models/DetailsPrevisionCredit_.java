package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailsPrevisionCredit.class)
public class DetailsPrevisionCredit_ { 

    public static volatile SingularAttribute<DetailsPrevisionCredit, Integer> agentMaj;
    public static volatile SingularAttribute<DetailsPrevisionCredit, BigDecimal> totalTaux;
    public static volatile SingularAttribute<DetailsPrevisionCredit, String> fkBien;
    public static volatile SingularAttribute<DetailsPrevisionCredit, BigDecimal> taux;
    public static volatile SingularAttribute<DetailsPrevisionCredit, String> fkDevise;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Integer> type;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Date> dateCreate;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Integer> fkDetailAssujettissement;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Integer> nbreTourSouscrit;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Integer> etat;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Date> dateMaj;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Integer> nbreTourUtilise;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Integer> agentCreate;
    public static volatile SingularAttribute<DetailsPrevisionCredit, String> fkPrevisionCredit;
    public static volatile SingularAttribute<DetailsPrevisionCredit, Integer> id;

}