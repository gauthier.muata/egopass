package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Agent;
import cd.hologram.erecettesvg.models.Axe;
import cd.hologram.erecettesvg.models.Commande;
import cd.hologram.erecettesvg.models.Devise;
import cd.hologram.erecettesvg.models.Site;
import cd.hologram.erecettesvg.models.Tarif;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Voucher.class)
public class Voucher_ { 

    public static volatile SingularAttribute<Voucher, Agent> agentMaj;
    public static volatile SingularAttribute<Voucher, BigDecimal> prixUnitaire;
    public static volatile SingularAttribute<Voucher, Tarif> fkTarif;
    public static volatile SingularAttribute<Voucher, Integer> qte;
    public static volatile SingularAttribute<Voucher, Axe> fkAxe;
    public static volatile SingularAttribute<Voucher, BigDecimal> montant;
    public static volatile SingularAttribute<Voucher, BigDecimal> remise;
    public static volatile SingularAttribute<Voucher, BigDecimal> montantFinal;
    public static volatile SingularAttribute<Voucher, Devise> devise;
    public static volatile SingularAttribute<Voucher, Integer> etat;
    public static volatile SingularAttribute<Voucher, Commande> fkCommande;
    public static volatile SingularAttribute<Voucher, Site> fkSite;
    public static volatile SingularAttribute<Voucher, Integer> id;

}