<%-- 
    Document   : gestionPeriodicite
    Created on : 24 sept. 2020, 09:18:35
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des periodicités</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <form class="form-horizontal" >

                    <div class="row">

                        <div class="col-lg-12">

                            <div class="form-group">

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"> Edition d'une périodicité</h4>
                                    </div>
                                </div>

                                <div class="panel panel-primary">

                                    <div class="panel-wrapper collapse in">

                                        <div class="panel-body">

                                            <div class="form-group">
                                                <p class="control-label col-sm-2" for="inputNbrJour">
                                                    Nombre des jours <span style="color:red"> *</span></p>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="inputNbrJour" placeholder="Nombre des jours"/>
                                                </div>

                                                <p class="control-label col-sm-2" for="inputPeriodeRecidive">
                                                    Période recidive <span style="color:red"> *</span></p>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="inputPeriodeRecidive" placeholder="Période recidive"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <p class="control-label col-sm-2" for="inputIntitulePeriodicite">
                                                    Intitulé <span style="color:red"> *</span></p>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="inputIntitulePeriodicite" placeholder="Intitulé periodicité"/>
                                                </div>

                                                <div class="input-group-btn">
                                                    <button class="btn btn-default pull-right" id="btnEnregistrerPeriodicite" style="right: 7%">
                                                        <i class="fa fa-save"></i> Enregistrer
                                                    </button>
                                                    <button class="btn btn-default pull-right" id="btnModifierPeriodicite" style="display: none; right: 9%">
                                                        <i class="fa fa-edit"></i> Modifier
                                                    </button>
                                                </div>

                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>

                            <table width="100%" 
                                   class="table table-bordered table-hover" 
                                   id="tableResultPeriodicite">
                            </table>

                        </div>
                    </div>
                </form>
            </div>
        </div>

        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>

        <script type="text/javascript" src="assets/js/articleBudgetaire/gestionPeriodicite.js"></script>

    </body>
</html>
