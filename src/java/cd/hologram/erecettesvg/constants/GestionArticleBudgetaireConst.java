/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author juslin.tshiamua
 */
public class GestionArticleBudgetaireConst {

    public class ParamName {

        public final static String INTITULE_DOCUMENT_OFFICIEL = "intituleDocumentOfficiel";
        public final static String CODE_DOCUMENT_OFFICIEL = "codeDocumentOfficiel";
        public final static String NUMERO_ARRETE = "numeroArrete";
        public final static String DATE_ARRETE = "dateArrete";
        public final static String CODE = "code";
        public final static String ETAT = "etat";
        public final static String CODE_ARTICLE_GENERIQUE = "codeArticleGenerique";
        public final static String INTITULE_ARTICLE_GENERIQUE = "intituleArticleGenerique";
        public final static String CODE_SERVICE = "codeService";
        public final static String INTITULE_SERVICE = "intituleService";

        public final static String CODE_FORME_JURIDIQUE = "codeFormeJuridique";
        public final static String INTITULE_FORME_JURIDIQUE = "intituleFormeJurique";
        public final static String FORME_JURIDIQUE = "formeJurique";

        public final static String CODE_PERIODICITE = "codePeriodicite";
        public final static String INTITULE_PERIODICITE = "intitulePeriodicite";
        public final static String PERIODICITE = "periodicite";

        public final static String CODE_DEVISE = "codeDevise";
        public final static String INTITULE_DEVISE = "intituleDevise";
        public final static String DEVISE = "devise";

        public final static String CODE_UNITE = "codeUnite";
        public final static String CODE_UNITE_ARTICLE = "codeUniteArticle";
        public final static String INTITULE_UNITE = "intituleUnite";
        public final static String UNITE = "unite";
        public final static String ENTITE_ADMINISTRATIVE = "entiteAdministrative";

        public final static String CODE_TARIF = "codeTarif";
        public final static String INTITULE_TARIF = "intituleTarif";
        public final static String VALEUR_TARIF = "valeurTarif";
        public final static String TYPE_VALEUR_TARIF = "typeValeurTarif";

        public final static String PALIER_LIST = "palierList";

        public final static String BORNE_MINIMALE = "borneMinimale";
        public final static String BORNE_MAXIMALE = "borneMaximale";
        public final static String TAUX = "taux";
        public final static String TYPE_TAUX = "typeTaux";
        public final static String AGENT_CREAT = "agentCreat";
        public final static String CHECKED_VAL_BASE = "checkedValBase";
        public final static String CHECKED_PALIER = "checkedPalier";
        public final static String CHECKED_ASSUJETTISSABLE = "checkedAssujettissable";
        public final static String CHECKED_PERIODE_VARIABLE = "checkPeriodeVariable";
        public final static String CHECKED_PROPRIETAIRE = "checkProprietaire";
        public final static String CHECKED_TAUX_TRANSACTIONNEL = "checkTauxTransactionnel";
        public final static String CHECKED_QTE_VARIABLE = "checkQteVariable";
        public final static String INTITULE_ARTICLE = "intituleArticle";

        public final static String CODE_TARIF_ARTICLE = "codeTarifArticle";
        public final static String CODE_OFFICIEL = "codeOfficiel";
        public final static String CODE_NATURE_ARTICLE = "codeNatureArticle";
        public final static String PRISE_CHARGE_PENALITE = "priseChargePenalite";
        public final static String PRISE_CHARGE_DECLARATION = "priseChargeDeclaration";
        public final static String NBR_JOUR_LIMITE = "nbrJourLimite";
        public final static String ARTICLE_BUDGETAIRE = "ARTICLE_BUDGETAIRE";
        
        public final static String CODE_SITE = "codeSite";
        
        public final static String BANQUE_ARTICLE_CODE = "banqueArticleCode";
        public final static String CODE_COMPTE_BANCAIRE = "codeCompteBancaire";
        public final static String CODE_ARTICLE = "codeArticle";
        public final static String CODE_ARTICLE_MERE = "codeArticleMere";
        
        public final static String CODE_ENTITE = "codeEntite";
        public final static String INTITULE_ENTITE = "intituleEntite";
        
        public final static String VALEUR_PALIER = "valeurPalier";
        public final static String ASSUJETTISABLE = "assujettisable";
        public final static String INTITULE_TARIF_ARTICLE = "intituleTarifArticle";
        public final static String TARIF_VARIABLE = "tarifVariable";
        public final static String DATE_CREAT = "dateCreat";
        public final static String DATE_MAJ = "dateMaj";
        public final static String AGENT_MAJ = "agentMaj";
        public final static String PROPRIETAIRE = "proprietaire";
        public final static String TRANSACTIONNEL = "transactionnel";
        public final static String PERIODE_DEBUT = "periodeDebut";
        public final static String PERIODE_FIN = "periodeFin";
        public final static String PERIODICITE_VARIABLE = "periodiciteVariable";
        public final static String INTITULE_NATURE_ARTICLE = "intituleNatureArticle";
        public final static String QUANTITE_VARIABLE = "quantiteVariable";
        public final static String ARRETE = "arrete";
        public final static String DATE_DEBUT_PERIODE = "dateDebutPeriode";
        public final static String DATE_DEBUT_PENALITE = "dateDebutPenalite";
        public final static String ECHEANCE_LEGALE = "echeanceLegale";
        public final static String DATE_LIMITE_PAIEMENT = "dateLimiteLegale";
        public final static String PERIODE_ECHEANCE = "periodeEcheance";
        public final static String VALEUR_TARIF_ARTICLE = "valeurTarifArticle";
        
        public final static String TARIF = "tarif";
        public final static String ETAT_REMOVE = "etatRemove";
        
        public final static String CHECK_NATURE_ARTICLE = "checkNatureArticle";
        
        public final static String CODE_COMMUNE = "codeCommune";
        public final static String INTITULE_COMMUNE = "intituleCommune";
        public final static String CODE_QUARTIER = "codeQuartier";
        public final static String INTITULE_QUARTIER = "intituleQuartier";
        
    }

    public class Operation {

        public final static String LOAD_EXERCICE_FISCAL = "loadExerciceFiscal";

        public final static String ADD_EXERCICE_FISCAL = "addExerciceFiscal";

        public final static String ADD_CLE_REPARTITION = "addclerepartition";

        public final static String DELETE_EXERCICE_FISCAL = "deleteExerciceFiscal";

        public final static String UPDATE_EXERCICE_FISCAL = "updateExerciceFiscal";

        public final static String LOAD_MINISTERE = "loadMinistere";

        public final static String LOAD_DEVISE = "loadDevise";

        public final static String SAVE_ASSIGNATION_BUDGETAIRE = "saveAssignationBudgetaire";

        public final static String UPDATE_ASSIGNATION_MONTANT = "updateAssignationMontant";

        public final static String LOAD_SERVICES = "loadServices";

        public final static String LOAD_SERVICES_BY_CODE = "loadServicesByCode";

        public final static String LOAD_FAITS_GENERATEURS_SERVICE = "loadFaitGenerateurs";
        public final static String LOAD_FORME_JURIDIQUE = "loadFormeJuridique";
        public final static String CODE_FORME_JURIDIQUE = "codeFormeJuridique";
        public final static String LIBELLE_FORME_JURIDIQUE = "libelleFormeJuridique";
        public final static String SAVE_FAIT_GENERATEUR = "saveFaitGenerateur";

        public final static String ADD_SERVICE = "addService";

        public final static String UPDATE_SERVICE = "updateService";

        public final static String ACTIVATE_DEACTIVATE_SERVICE = "activeAndDeactiveService";
        public final static String LOAD_DOCUMENT_OFFICIEL = "loadDocumentOfficiel";
        public final static String LOAD_NATURE_ARTICLE_BUDGETAIRE = "loadNatureArticleBudgetaire";

        public final static String LOAD_ARTICLE_BUDGETAIRE = "loadArticleBudgetaire";
        public final static String LOAD_ARTICLE_BUDGETAIRE_CLEREP_BY_MINISTERE = "loadArticleBudgetaireCleRep";
        public final static String DELETE_CLE_REPARTITION = "deleteclerepartition";
        public final static String LOAD_CLE_REPARTITIION = "loadCleRepartition";

        public final static String LOAD_SERVICES_ASSIETTES = "load_services_assiettes";

        public final static String SAVE_DOCUMENT_OFFICIEL = "saveDocumentOfficiel";
        public final static String GET_DOCUMENT_OFFICIEL = "getDocument";

        public final static String LOAD_ARTICLE_BUDGETAIRE_BY_EXERCICE = "loadArticleBudgetaireByExercice";
        public final static String LOAD_ASSIGNATION_BY_EXERCICE = "loadAssignationByExercice";
        public final static String LOAD_ALL_SERVICE = "loadAllservices";

        public final static String GET_ARTICLE_GENERIQUE = "getArticleGenerique";
        public final static String GET_DOCUMENT_OFFICIELS = "getDocumentOfficiel";
        public final static String INIT_DATA_ARTICLE = "initDataArticle";

        public final static String LOAD_TARIF = "loadtarif";
        public final static String SAVE_ARTICLE_BUDGETAIRE = "saveArticlebudgetaire";

        public final static String LOAD_ASSIGN_BY_EXERCICE = "loadAssignByExercice";

        public final static String SAVE_DETAILS_ASSIGNATION_BUDGETAIRE = "saveDetailsAssignationBudgetaire";

        
        public final static String LOAD_BANQUE = "loadBanque";
        public final static String LOAD_COMPTE_BANCAIRE = "loadCompteBancaire";
        public final static String LOAD_ARTICLE_AFFECTRE = "loadArticleAffecter";
        public final static String GET_SERVICE_ASSIETTE = "getServiceAssiette";
        public final static String LOAD_ARTICLE_SERVICE = "loadArticleByService";
        public final static String DESAFFECTER_ARTICLE_COMPTE = "desaffecterArticleCompte";
        public final static String AFFECTER_ARTICLE_COMPTE = "affecterArticleCompte";
        public final static String INTITULE_ENTITE_ADMINISTRATVE = "intituleEntiteAdministrative";
        
    }

    public class ParamQUERY {

        public final static String CODE = "CODE";
        public final static String INTITULE = "INTITULE";
        public final static String DATE_ARRETE = "DATE_ARRETE";
        public final static String NUMERO_ARRETE = "NUMERO";
        public final static String ETAT = "ETAT";

        public final static String ARTICLE_GENERIQUE = "ARTICLE_GENERIQUE";
        public final static String UNITE = "UNITE";
        public final static String PALIER = "PALIER";
        public final static String PERIODICITE = "PERIODICITE";
        public final static String ASSUJETISSABLE = "ASSUJETISSABLE";
        public final static String ARTICLE_MERE = "ARTICLE_MERE";
        public final static String TRANSACTIONNEL = "TRANSACTIONNEL";
        public final static String DEBUT = "DEBUT";
        public final static String FIN = "FIN";
        public final static String AGENT_CREAT = "AGENT_CREAT";
        public final static String PERIODICITE_VARIABLE = "PERIODICITE_VARIABLE";
        public final static String NBR_JOUR_LIMITE = "NBR_JOUR_LIMITE";
        public final static String TARIF_VARIABLE = "TARIF_VARIABLE";
        public final static String TARIF = "TARIF";
        public final static String PROPRIETAIRE = "PROPRIETAIRE";
        public final static String NATURE = "NATURE";
        public final static String ARRETE = "ARRETE";
        public final static String QUANTITE_VARIABLE = "QUANTITE_VARIABLE";
        public final static String CODE_OFFICIEL = "CODE_OFFICIEL";
        public final static String DATE_DEBUT_PENALITE = "DATE_DEBUT_PENALITE";
        public final static String DATE_DEBUT_PERIODE = "DATE_DEBUT_PERIODE";
        public final static String DATE_ECHEANCE_LEGALE= "DATE_ECHEANCE_LEGALE";
        public final static String DATE_LIMITE_PAIEMENT= "DATE_LIMITE_PAIEMENT";
    }
}
