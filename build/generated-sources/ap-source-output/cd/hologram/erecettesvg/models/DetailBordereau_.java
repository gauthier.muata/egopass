package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import cd.hologram.erecettesvg.models.Bordereau;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailBordereau.class)
public class DetailBordereau_ { 

    public static volatile SingularAttribute<DetailBordereau, ArticleBudgetaire> articleBudgetaire;
    public static volatile SingularAttribute<DetailBordereau, Long> code;
    public static volatile SingularAttribute<DetailBordereau, String> periodicite;
    public static volatile SingularAttribute<DetailBordereau, BigDecimal> montantPercu;
    public static volatile SingularAttribute<DetailBordereau, Bordereau> bordereau;
    public static volatile SingularAttribute<DetailBordereau, String> devise;
    public static volatile SingularAttribute<DetailBordereau, Character> statut;

}