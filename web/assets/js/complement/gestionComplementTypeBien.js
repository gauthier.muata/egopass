/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var cmbComplementTypeBiens;

var codeComplementTypeBien, codeTypeComplementBien;

var btnAffecterTypeBien, btnAssocierArticleComplementBien,
        btnLauchSearchArticleGenerique, btnValiderAffectationArticle,
        btnLauchSearchArticleComplementTypeBien, btnValiderAffectationTypeBien;

var tableComplementTypeBien, tableResultArticleGenerique,
        tableResultSelectedComplementBien;

var modalSelectedArticleBudgetaire, modalComplementTypeBien;

var inputValueResearchArticleGenerique, inputValueResearchComplementTypeBien;

var selectArticleGeneriqueData;

var listTypeComplementTypeBien;

var complementTypeBienList;

var complementObjetListBien = [];

var articleObjetList = [];

var codeArticleAffecter = '';

var codeTypeComplement = '';

var codeAbComplement = '';

var listComplementBien = '';

$(function () {

    mainNavigationLabel.text('GESTION DES COMPLEMENTS');
    secondNavigationLabel.text('affecter un complément à un type bien');

    removeActiveMenu();

    modalSelectedArticleBudgetaire = $('#modalSelectedArticleBudgetaire');
    modalComplementTypeBien = $('#modalComplementTypeBien');

    tableComplementTypeBien = $('#tableComplementTypeBien');
    tableResultArticleGenerique = $('#tableResultArticleGenerique');
    tableResultSelectedComplementBien = $('#tableResultSelectedComplementBien');

    cmbComplementTypeBiens = $('#cmbComplementTypeBiens');

    btnAffecterTypeBien = $('#btnAffecterTypeBien');
    btnAssocierArticleComplementBien = $('#btnAssocierArticleComplementBien');
    btnLauchSearchArticleGenerique = $('#btnLauchSearchArticleGenerique');
    btnValiderAffectationArticle = $('#btnValiderAffectationArticle');
    btnLauchSearchArticleComplementTypeBien = $('#btnLauchSearchArticleComplementTypeBien');
    btnLauchSearchArticleComplementTypeBien = $('#btnLauchSearchArticleComplementTypeBien');

    inputValueResearchArticleGenerique = $('#inputValueResearchArticleGenerique');
    btnValiderAffectationTypeBien = $('#btnValiderAffectationTypeBien');
    inputValueResearchComplementTypeBien = $('#inputValueResearchComplementTypeBien');

    inputValueResearchComplementTypeBien.val('');
    inputValueResearchArticleGenerique.val('');

    cmbComplementTypeBiens.on('change', function () {
        codeComplementTypeBien = cmbComplementTypeBiens.val();
        getComplementTypeBien(codeComplementTypeBien);
    });

    btnLauchSearchArticleGenerique.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        getArticleNonAffecter(codeComplementTypeBien, codeArticleAffecter);

    });

    btnAssocierArticleComplementBien.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        var objetArticle = '';

        objetArticle = dataComplementAffecterBien();

        if (objetArticle != '') {
            codeArticleAffecter = objetArticle.codeUpdateAB;
            codeTypeComplement = objetArticle.codeTypeComplement;
        }

        if (codeTypeComplement == '' || codeTypeComplement == 'null') {
            alertify.alert('Veuillez d\'abord sélectionner un complément.');
            return;
        }

        getArticleNonAffecter(codeTypeComplement, codeArticleAffecter);

        modalSelectedArticleBudgetaire.modal('show');
    });

    inputValueResearchArticleGenerique.on('keypress', function (e) {
        if (e.keyCode == 13) {
            getArticleNonAffecter(codeComplementTypeBien, codeArticleAffecter);
        }
    });

    btnValiderAffectationArticle.click(function (e) {

        var objetArticle = '';
        objetArticle = dataArticleNonAffecter();

        if (objetArticle === empty) {
            alertify.alert('Veuillez d\'abord sélectionner un article');
            return;
        }

        alertify.confirm('Etes-vous sûre de vouloir affecter <b>' + objetArticle.intituleArticle + '</b> ?', function () {
            affecterArticleComplement(objetArticle.codeArticle, codeTypeComplement, codeAbComplement);

        });
    });

    btnAffecterTypeBien.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (codeComplementTypeBien == '' || codeComplementTypeBien == 'null') {
            alertify.alert('Veuillez sélectionner un type bien.');
            return;
        }

        getComplementBienByType();

        modalComplementTypeBien.modal('show');
    });

    btnLauchSearchArticleComplementTypeBien.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

//        if (inputValueResearchComplementTypeBien.val() == '' || inputValueResearchComplementTypeBien.val() == 'null') {
//            alertify.alert('Veuillez fournir un critère de recherche.');
//            return;
//        }        
        getComplementBienByType();
    });

    btnValiderAffectationTypeBien.click(function (e) {

        e.preventDefault();

        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }


        listComplementBien = preparerAffectationComplementBien();

        if (listComplementBien.length > 0) {
            affecterComplementBien(JSON.stringify(listComplementBien));
        } else {
            alertify.alert('Veuillez sélectionner au moins un complément.');
            return;
        }



    });

    getTypeBien();
    printComplementTypeBien('');

});

function getTypeBien() {

    $.ajax({
        type: 'POST',
        url: 'assujetissement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'operation': 'loadTypeBien'
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                $.unblockUI();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else {
                    var dataTypeBien = $.parseJSON(JSON.stringify(response));

                    var dataTypeBiens = '<option value ="0">-- Sélectionner --</option>';
                    for (var i = 0; i < dataTypeBien.length; i++) {
                        dataTypeBiens += '<option value =' + dataTypeBien[i].codeTypeBien + '>' + dataTypeBien[i].libelleTypeBien + '</option>';
                    }
                    cmbComplementTypeBiens.html(dataTypeBiens);
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getComplementTypeBien(codeComplement) {

    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getTypeComplementBien',
            'codeComplement': codeComplement
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            }

            setTimeout(function () {

                complementTypeBienList = $.parseJSON(JSON.stringify(response));

                if (complementTypeBienList.length > 0) {
                    printComplementTypeBien(complementTypeBienList);
                } else {
                    printComplementTypeBien('');
                }
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function getArticleNonAffecter(codeComplemntBien, codeArticle) {

    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        crossDomain: false,
        data: {
            'valueSearch': inputValueResearchArticleGenerique.val(),
            'codeComplement': codeComplemntBien,
            'codeArticle': codeArticle,
            'operation': 'getArticleNonAffecterComplement'
        },
        beforeSend: function () {
            modalSelectedArticleBudgetaire.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherches en cours ...</h5>'});
        },
        success: function (response)
        {
            setTimeout(function () {
                modalSelectedArticleBudgetaire.unblock();
                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '0') {
                    btnValiderAffectationArticle.hide();
                    printArticleGeneriqueTable('');
                    return;
                } else {
                    articleObjetList = JSON.parse(JSON.stringify(response));
                    if (articleObjetList.length > 0) {
                        btnValiderAffectationArticle.show();
                        printArticleGeneriqueTable(articleObjetList);
                    } else {
                        btnValiderAffectationArticle.hide();
                    }
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function () {
            modalSelectedArticleBudgetaire.unblock();
            showResponseError();
        }

    });
}

function affecterComplementForme(codeComplementTypeBien) {


    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterComplementTypeBien',
            'complementObjetList': codeComplementTypeBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du dépôt déclaration en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                    getComplementTypeBien(codeComplementTypeBien);
                    modalSelectedArticleBudgetaire.modal('hide');
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'affectation du complément.');
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printComplementTypeBien(result) {

    var header = '<thead style="background-color:#0085c7;color:white"><tr>';
    header += '<th style="text-align:center;width:50%" scope="col"> TYPE COMPLEMENT </th>';
    header += '<th style="text-align:center;width:40%" scope="col"> ARTICLE BUDGETAIRE </th>';
    header += '<th style="width:5%;text-align:center"scope="col"></th>';
    header += '<th style="width:5%;text-align:center"scope="col"></th>';
    header += '<th hidden="true" scope="col">Code</th>';
    header += '</tr></thead>';

    var body = '<tbody>';

    for (var i = 0; i < result.length; i++) {

        body += '<td style="text-align:left">' + result[i].libelleTypeComplement + '</td>';
        body += '<td style="text-align:left">' + result[i].libelle + '</td>';
        body += '<td style="text-align:center"><center><input onclick="checkComplement(\'' + result[i].codeTypeComplement + '\')" type="checkbox" id="checkBoxSelectComplement' + result[i].codeTypeComplement + '" name="checkBoxSelectComplement' + result[i].codeTypeComplement + '"></center></td>';
        body += '<td style="vertical-align:middle;">'
                + '<a onclick="removeComplementBien(\'' + result[i].codeTypeComplement + '\')" class="btn btn-danger" title="Désactiver ce complément"><i class="fa fa-trash-o"></i></a></td>'
        body += '<td hidden="true">' + result[i].codeTypeComplement + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';

    var tableContent = header + body;

    tableComplementTypeBien.html(tableContent);
    var dttableComplementTypeBien = tableComplementTypeBien.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function printArticleGeneriqueTable(data) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:left;width:60%">ARTICLE BUDGETAIRE</th>';
    tableContent += '<th style="text-align:left;width:40%">SERCICE D\'ASSIETTE</th>';
    tableContent += '<th style="width:5%;text-align:center"scope="col"></th>';
    tableContent += '<th hidden="true" scope="col">Code article</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;width:60%;text-align:left">' + data[i].intituleArticle.toUpperCase() + '</td>';
        tableContent += '<td style="vertical-align:middle;width:40%;text-align:left">' + data[i].intituleService.toUpperCase() + '</td>';
        tableContent += '<td style="text-align:center"><center><input onclick="checkArticleNonAffecter(\'' + data[i].codeArticle + '\')" type="checkbox" id="checkBoxSelectArticleNonAffecter' + data[i].codeArticle + '" name="checkBoxSelectArticleNonAffecter' + data[i].codeArticle + '"></center></td>';
        tableContent += '<td hidden="true">' + data[i].codeArticle + '</td>';
        tableContent += '</tr>';
    }
    tableContent += '</tbody>';
    tableResultArticleGenerique.html(tableContent);

    var myDt = tableResultArticleGenerique.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 15,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });

//    $('#tableResultArticleGenerique tbody').on('click', 'tr', function () {
//        if (checkSession()) {
//            showSessionExpiredMessage();
//            return;
//        }
//        selectArticleGeneriqueData = new Object();
//        selectArticleGeneriqueData.intituleArticle = myDt.row(this).data()[0].toUpperCase();
//        selectArticleGeneriqueData.intituleService = myDt.row(this).data()[1];
//        selectArticleGeneriqueData.codeArticle = myDt.row(this).data()[2];
//        selectArticleGeneriqueData.codeService = myDt.row(this).data()[3];
//
//    });
}

function getComplementBienByType() {

    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'getComplementNonAffected',
            'intituleComplement': inputValueResearchComplementTypeBien.val(),
            'codeComplementBien': codeComplementTypeBien
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Chargement en cours...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            if (response == '-1') {
                showResponseError();
                return;
            } else if (response == '0') {
                printSelectedComplementTypeBien('');
            }

            listTypeComplementTypeBien = $.parseJSON(JSON.stringify(response));
            if (listTypeComplementTypeBien.length > 0) {
                printSelectedComplementTypeBien(listTypeComplementTypeBien);
            } else {
                printSelectedComplementTypeBien('');
            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function printSelectedComplementTypeBien(result) {

    var tableContent = '';
    tableContent += '<thead style="background-color:#0085c7;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="text-align:center;width:45%"scope="col">INTITULE</th>';
    tableContent += '<th style="text-align:center;width:35%"scope="col">INTITULE CATEGORIE</th>';
    tableContent += '<th style="width:5%;text-align:center"scope="col"></th>';
    tableContent += '<th hidden="true" scope="col"> Code article </th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';



    for (var i = 0; i < result.length; i++) {

        var firstLineCp = '';

        if (result[i].intitule.length > 70) {
            firstLineCp = result[i].intitule.substring(0, 70).toUpperCase() + ' ...';
        } else {
            firstLineCp = result[i].intitule.toUpperCase();
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;title="' + result[i].intitule.toUpperCase() + '"><span style="font-weight:bold;"></span>' + firstLineCp.toUpperCase() + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].intituleCategorie + '</td>';
        tableContent += '<td style="text-align:center"><center><input onclick="checkComplementBien(\'' + result[i].code + '\')" type="checkbox" id="checkBoxSelectComplementBien' + result[i].code + '" name="checkBoxSelectComplementBien' + result[i].code + '"></center></td>';
        tableContent += '<td hidden="true">' + result[i].code + '</td>';
        tableContent += '</tr>';

    }
    tableContent += '</tbody>';
    tableResultSelectedComplementBien.html(tableContent);

    var myDataTable = tableResultSelectedComplementBien.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        tracking: false,
        ordering: false,
        searching: true,
        pageLength: 10,
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
}

function preparerAffectationComplementBien() {
    
     complementObjetListBien = [];

    for (var i = 0; i < listTypeComplementTypeBien.length; i++) {

        var selectComplementBien = '#checkBoxSelectComplementBien' + listTypeComplementTypeBien[i].code;

        var select = $(selectComplementBien);

        if (select.is(':checked')) {

            var complementObjetBien = new Object();

            complementObjetBien.code = listTypeComplementTypeBien[i].code;
            complementObjetBien.codeArticle = listTypeComplementTypeBien[i].codeAB;
            complementObjetBien.codeComplementTypeBien = codeComplementTypeBien;

            complementObjetListBien.push(complementObjetBien);

        }

    }

    return complementObjetListBien;
}

function affecterComplementBien(complementObjetListBien) {


    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterComplementBien',
            'complementObjetListBien': complementObjetListBien
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement du dépôt déclaration en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                    complementObjetListBien = [];
                    listComplementBien = '';
                    getComplementTypeBien(codeComplementTypeBien);
                    modalComplementTypeBien.modal('hide');
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'affectation du complément.');
                    complementObjetListBien = [];
                    listComplementBien = '';
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function removeComplementBien(code) {

    alertify.confirm('Etes-vous sûrs de vouloir désactiver ?', function () {
        desactiverComplementBien(code);
    });

}

function desactiverComplementBien(code) {


    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'desactiverComplementBien',
            'codeTypeComplementBien': code
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation du complément en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                    getComplementTypeBien(codeComplementTypeBien);
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de désactivation du complément bien.');
//                     complementObjetList = [];
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}

function dataComplementAffecterBien() {
    
    var complementObjet = '';

    for (var i = 0; i < complementTypeBienList.length; i++) {

        var selectedComplenet = '#checkBoxSelectComplement' + complementTypeBienList[i].codeTypeComplement;
        var select = $(selectedComplenet);
        if (select.is(':checked')) {
            complementObjet = new Object();
            complementObjet.codeTypeComplement = complementTypeBienList[i].codeTypeComplement;
            complementObjet.codeUpdateAB = complementTypeBienList[i].codeAB;
            complementObjet.libelleTypeComplement = complementTypeBienList[i].libelleTypeComplement;
            complementObjet.libelleAB = complementTypeBienList[i].libelle;
            complementObjet.codeAbComplement = complementTypeBienList[i].codeAbComplement;
            codeAbComplement = complementTypeBienList[i].codeAbComplement;
            break;
        } 
    }
    return complementObjet;
}

function checkComplement(codeComplement) {

    for (var i = 0; i < complementTypeBienList.length; i++) {
        if (codeComplement != complementTypeBienList[i].codeTypeComplement) {
            var control = document.getElementById("checkBoxSelectComplement" + complementTypeBienList[i].codeTypeComplement);
            control.checked = false;
        }
    }
}

function dataArticleNonAffecter() {

    for (var i = 0; i < articleObjetList.length; i++) {

        var selectedArticleNonAffecter = '#checkBoxSelectArticleNonAffecter' + articleObjetList[i].codeArticle;
        var select = $(selectedArticleNonAffecter);
        if (select.is(':checked')) {
            var articleNonAffecterObjet = new Object();
            articleNonAffecterObjet.codeArticle = articleObjetList[i].codeArticle;
            articleNonAffecterObjet.intituleArticle = articleObjetList[i].intituleArticle;
            break;
        } else {
            articleNonAffecterObjet = '';
        }
    }
    return articleNonAffecterObjet;
}

function checkArticleNonAffecter(codeArticle) {

    for (var i = 0; i < articleObjetList.length; i++) {
        if (codeArticle != articleObjetList[i].codeArticle) {
            var controlArticle = document.getElementById("checkBoxSelectArticleNonAffecter" + articleObjetList[i].codeArticle);
            controlArticle.checked = false;
        }
    }
}

function affecterArticleComplement(codeArticle, codeTypeComplement, codeAbComplement) {


    $.ajax({
        type: 'POST',
        url: 'complement_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'affecterArticleComplement',
            'codeAbCompl': codeAbComplement,
            'codeArticle': codeArticle,
            'codeComplementArticle': codeTypeComplement
        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Désactivation du complément en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                if (response == '1') {
                    alertify.alert('L\'opération effectuée avec succès.');
                    getComplementTypeBien(codeComplementTypeBien);
                    modalSelectedArticleBudgetaire.modal('hide');
                } else if (response == '0') {
                    alertify.alert('Une erreur inattendue s\'est produite lors de l\'affectation de l\'article.');
//                     complementObjetList = [];
                } else {
                    showResponseError();
                }
            }
            , 1);
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });
}


function preparerAssocierComplement() {

    var objetComplenet = dataComplementAffecterBien();

    var message;
    if (objetComplenet.codeUpdateAB == '') {
        message = 'Voulez-vous associer le complément ' + objetComplenet.libelleTypeComplement + ' avec l\'article ' + objetComplenet.libelleAB;
    } else {
        message = 'Voulez-vous dissocier le complément ' + objetComplenet.libelleTypeComplement + ' avec l\'article ' + objetComplenet.libelleAB;
    }

    alertify.confirm(message + ' : ?', function () {


    });
}