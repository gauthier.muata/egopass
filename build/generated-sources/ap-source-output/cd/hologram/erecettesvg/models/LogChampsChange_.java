package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Log;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(LogChampsChange.class)
public class LogChampsChange_ { 

    public static volatile SingularAttribute<LogChampsChange, String> champs;
    public static volatile SingularAttribute<LogChampsChange, Integer> visible;
    public static volatile SingularAttribute<LogChampsChange, String> before;
    public static volatile SingularAttribute<LogChampsChange, Integer> id;
    public static volatile SingularAttribute<LogChampsChange, String> after;
    public static volatile SingularAttribute<LogChampsChange, Log> fkTLog;

}