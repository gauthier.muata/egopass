package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ExtraitDeRole;
import cd.hologram.erecettesvg.models.Role;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(DetailsRole.class)
public class DetailsRole_ { 

    public static volatile SingularAttribute<DetailsRole, Role> fkRole;
    public static volatile SingularAttribute<DetailsRole, Integer> ordonnance;
    public static volatile SingularAttribute<DetailsRole, String> fkTypeDocument;
    public static volatile SingularAttribute<DetailsRole, Integer> id;
    public static volatile SingularAttribute<DetailsRole, String> documentRef;
    public static volatile SingularAttribute<DetailsRole, ExtraitDeRole> fkExtraitRole;

}