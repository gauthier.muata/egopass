package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.EntiteAdministrative;
import cd.hologram.erecettesvg.models.Site;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Adresse.class)
public class Adresse_ { 

    public static volatile ListAttribute<Adresse, AdressePersonne> adressePersonneList;
    public static volatile SingularAttribute<Adresse, EntiteAdministrative> ville;
    public static volatile SingularAttribute<Adresse, String> chaine;
    public static volatile SingularAttribute<Adresse, String> numero;
    public static volatile SingularAttribute<Adresse, EntiteAdministrative> province;
    public static volatile SingularAttribute<Adresse, EntiteAdministrative> commune;
    public static volatile SingularAttribute<Adresse, EntiteAdministrative> district;
    public static volatile SingularAttribute<Adresse, EntiteAdministrative> quartier;
    public static volatile SingularAttribute<Adresse, EntiteAdministrative> avenue;
    public static volatile SingularAttribute<Adresse, String> id;
    public static volatile SingularAttribute<Adresse, Boolean> etat;
    public static volatile ListAttribute<Adresse, Site> siteList;

}