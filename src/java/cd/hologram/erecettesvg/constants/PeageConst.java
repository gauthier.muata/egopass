/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.constants;

/**
 *
 * @author WILLY
 */
public class PeageConst {

    public class ParamName {

        public final static String OPERATION = "operation";
        public final static String TYPE_SEARCH = "typeSearch";
        public final static String VALUE_SEARCH = "valueSearch";
        public final static String TYPE_PREVISION = "typePrevision";

        public final static String VALEUR = "valeur";
        public final static String CODE_BIEN = "codebien";
        public final static String INTITULE_BIEN = "intituleBien";
        public final static String CHAINE_ADRESSE = "chaineAdresse";
        public final static String DEVISE = "devise";
        public final static String DESCRIPTION_BIEN = "descriptionBien";

        public final static String CODE_DETAIL_ASSUJETTISSEMENT = "codeDetailAssujettisement";
        public final static String DEATIL_PREVISION_CREDIT_LIST = "detailPrevisionCreditList";

        public final static String NOMBRE_TOURE = "nbrToure";
        public final static String MONTANT_PERCU = "montantPercu";
        public final static String CODE_DEVISE = "codeDevise";

        public final static String USER_ID = "userId";
        public final static String EST_EXEMPTE = "estExempte";
        public final static String CODE_PERSONNE_EXEMPTE = "codePresonneExempte";

    }

    public class Operation {

        public final static String EXEMPTE_ASSUJETTI = "exempteAssujettis";
        public final static String DISABLE_PERSONNE_EXEMPTE = "disabledPersonneExempte";
        public final static String LOAD_PEAGES = "loadPeages";
    }

}
