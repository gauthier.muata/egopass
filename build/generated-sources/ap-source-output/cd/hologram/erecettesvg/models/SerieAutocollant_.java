package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(SerieAutocollant.class)
public class SerieAutocollant_ { 

    public static volatile SingularAttribute<SerieAutocollant, String> agentMaj;
    public static volatile SingularAttribute<SerieAutocollant, Integer> idSerieMere;
    public static volatile SingularAttribute<SerieAutocollant, Integer> borneInferieur;
    public static volatile SingularAttribute<SerieAutocollant, Integer> qteRestante;
    public static volatile SingularAttribute<SerieAutocollant, Date> dateDerniereUtilisation;
    public static volatile SingularAttribute<SerieAutocollant, String> type;
    public static volatile SingularAttribute<SerieAutocollant, Integer> borneSuperieur;
    public static volatile SingularAttribute<SerieAutocollant, Integer> etat;
    public static volatile SingularAttribute<SerieAutocollant, Integer> qteUtilisee;
    public static volatile SingularAttribute<SerieAutocollant, String> site;
    public static volatile SingularAttribute<SerieAutocollant, String> suffixe;
    public static volatile SingularAttribute<SerieAutocollant, String> province;
    public static volatile SingularAttribute<SerieAutocollant, Integer> derniereCle;
    public static volatile SingularAttribute<SerieAutocollant, Date> dateAffectation;
    public static volatile SingularAttribute<SerieAutocollant, Integer> id;
    public static volatile SingularAttribute<SerieAutocollant, String> agentCreat;
    public static volatile SingularAttribute<SerieAutocollant, Integer> qteInitiale;
    public static volatile SingularAttribute<SerieAutocollant, Integer> qteManquant;
    public static volatile SingularAttribute<SerieAutocollant, Date> dateReception;

}