<%-- 
    Document   : paiement
    Created on : 14 juin 2018, 08:26:40
    Author     : bonheur.muntasomo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edition des taux de pénalité</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/main.css">
        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/datepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/lib/css/datepicker3.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>
    </head>
    <body>
        <div class="wrapper">
            <%@include file="assets/include/menu.html" %>
            <div id="content" style="width: 100%">
                <%@include file="assets/include/header.html" %>
                <div class="row">
                    <div class="col-lg-12">

                        <form class="form-inline" role="form">
                            Pénalité <span style="color:red"> *</span>: 
                            <select  class="form-control" id="selectPenalite" style="width: 530px">
                            </select>
                            Nature de la pénalité <span style="color:red"> *</span> : 

                            <select  class="form-control" id="selectNature" style="width: 350px">
                            </select>

                            <button 
                                class="btn btn-success" 
                                type="submit"id="btnSaveTauxPenalite">
                                <i class="fa fa-save"></i>  
                                Enregistrer
                            </button>

                            <button 
                                class="btn btn-default" 
                                type="submit"id="btnInitFields">
                                <i class="fa fa-close"></i> 
                                Initialiser
                            </button>

                        </form>
                    </div>
                </div><hr/>

                <div class="row">
                    <div class="col-lg-12">
                        <form class="form-inline" role="form">
                            Type personne <span style="color:red"> *</span>: 
                            <select  class="form-control" id="selectTypePersonne" style="width: 300px">
                            </select>
                            Taux <span style="color:red"> *</span>:

                            <input 
                                type="number" 
                                class="form-control" 
                                min="1"
                                style="width: 100px" 
                                id="inputTaux">
                            <span id="spnLblTypeTaux" style="font-weight: bold"></span>
                            Nbre Jour de retard minimal :

                            <input 
                                type="number" 
                                min="1"
                                class="form-control" 
                                style="width: 150px" 
                                id="inputNbreJourMin">

                            Nbre Jour de retard maximal :

                            <input 
                                type="number" 
                                min="1"
                                class="form-control" 
                                style="width: 150px" 
                                id="inputNbreJourMax">

                        </form>
                    </div>
                </div><hr/>

                <div class="row">
                    <div class="col-lg-3">

                        <div style="margin-top: 10px">

                            <input  type="checkbox" id="checkBoxTypeTaux" style="margin-top: -90px"/>
                            <label style="font-weight: bold;font-size: 13.5px">Le taux est en pourcentage</label><br/>
                            <label style="font-size: 13px;font-weight: normal;font-style: italic;color: blue">Lors de l'application de ce taux, le système déduira en % la valeur du taux sur le montant dû </label>
                        </div>
                    </div>

                    <div class="col-lg-3">

                        <div style="margin-top: 10px">

                            <input  type="checkbox" id="checkBoxActivateTauxPenalite" style="margin-top: -90px" checked="true"/>
                            <label style="font-weight: bold;font-size: 13.5px">Ce taux est activ&eacute;</label><br/>
                            <label style="font-size: 13px;font-weight: normal;font-style: italic;color: blue">Au cas contraire ce taux sera non applicable dans le système</label>
                        </div>
                    </div>

                    <div class="col-lg-3">

                        <div style="margin-top: 10px"> 

                            <input  type="checkbox" id="checkBoxRecidive" style="margin-top: -90px"/>
                            <label style="font-weight: bold;font-size: 13.5px">Ce taux est lié au cas de recidive</label><br/>
                            <label style="font-size: 13px;font-weight: normal;font-style: italic;color: blue">Ce taux ne s'appliquera que le sur les assujettis recidiviste</label>
                        </div>
                    </div>

                </div><hr/>

                <div class="journal" >
                    <table id="tableTauxPenalite" class="table table-bordered">
                    </table>
                </div>
                <div>
                    <label style="font-weight: normal">Champs obligatoire <span style="color:red">(*)</span></label>
                </div>
            </div>


        </div>
        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/gestionPenalite/editionTauxPenalite.js"></script>
    </body>
</html>