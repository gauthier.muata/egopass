package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Archive.class)
public class Archive_ { 

    public static volatile SingularAttribute<Archive, String> refDocument;
    public static volatile SingularAttribute<Archive, String> documentString;
    public static volatile SingularAttribute<Archive, String> application;
    public static volatile SingularAttribute<Archive, Boolean> estSynchronise;
    public static volatile SingularAttribute<Archive, byte[]> document;
    public static volatile SingularAttribute<Archive, Date> dateProduction;
    public static volatile SingularAttribute<Archive, String> dossier;
    public static volatile SingularAttribute<Archive, String> typeDocument;
    public static volatile SingularAttribute<Archive, String> id;
    public static volatile SingularAttribute<Archive, String> typeFormat;

}