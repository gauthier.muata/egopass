package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.FormeJuridique;
import cd.hologram.erecettesvg.models.Penalite;
import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(PalierTauxPenalite.class)
public class PalierTauxPenalite_ { 

    public static volatile SingularAttribute<PalierTauxPenalite, Penalite> penalite;
    public static volatile SingularAttribute<PalierTauxPenalite, FormeJuridique> formeJuridique;
    public static volatile SingularAttribute<PalierTauxPenalite, BigDecimal> valeur;
    public static volatile SingularAttribute<PalierTauxPenalite, String> nature;
    public static volatile SingularAttribute<PalierTauxPenalite, String> natureArticleBudgetaire;
    public static volatile SingularAttribute<PalierTauxPenalite, String> typeValeur;
    public static volatile SingularAttribute<PalierTauxPenalite, Boolean> estPourcentage;
    public static volatile SingularAttribute<PalierTauxPenalite, Integer> id;
    public static volatile SingularAttribute<PalierTauxPenalite, Boolean> recidive;
    public static volatile SingularAttribute<PalierTauxPenalite, BigDecimal> borneInferieure;
    public static volatile SingularAttribute<PalierTauxPenalite, BigDecimal> borneSuperieure;
    public static volatile SingularAttribute<PalierTauxPenalite, Boolean> etat;

}