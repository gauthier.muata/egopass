<%-- 
    Document   : gestionDroits
    Created on : 29 janv. 2020, 13:34:39
    Author     : joelkhang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<% Date current_date = new Date();%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Gestion des Groupes</title>

        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/lib/css/font-awesome.css"> 
        <link rel="stylesheet" type="text/css" href="assets/css/login.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main.css?ver=<%= current_date.toString() %>">

        <link href="assets/lib/css/datatables.min.css"/>
        <link href="assets/lib/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
        <link href="assets/lib/css/select.bootstrap.min.css" rel="stylesheet"/>

        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>
                
                <div class="container-fluid">
                    
                    <div class="row">
                        
                        <div class="col-lg-4">
                            <div class="panel panel-default">
                                <div class="panel-heading flex-align-center"> 
                                    <h4 class="panel-title">Les groupes</h4> 
                                    <button type="button" class="btn btn-success btn-add-group">
                                        <i class="fa fa-plus-circle"></i> Ajouter un groupe 
                                    </button>
                                </div>
                                <div class="panel-body">
                                    <div class="groupes">
                                        <table id="tableGroupe" class="table">
                                            <thead>
                                                <tr class="hidden">
                                                    <th></th>
                                                    <th>
                                                        Nom du groupe
                                                    </th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="panel panel-default droits-membres">
                                <div class="panel-heading flex-align-center"> 
                                    <ul class="nav nav-pills nav-justifieds">
                                        <li class="nav-item active">
                                            <a class="nav-link" data-toggle="tab" href="#tab_droits" role="tab" aria-selected="false">
						Droits
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#tab_membres" role="tab" aria-selected="false">
						Membres
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_droits">
                                            <div class="no-droits-list hidden">
                                                <button type="button" class="btn btn-success btn-add-droit mb-20">
                                                    <i class="fa fa-plus-circle"></i> Ajouter des droits dans ce groupe 
                                                </button>
                                                <div class="alert alert-danger alert-dismissible" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                     Aucun droit n'est affecté à ce groupe
                                                </div>
                                            </div>
                                            <div class="droits-list hidden">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <button type="button" class="btn btn-success btn-add-droit mb-20">
                                                            <i class="fa fa-plus-circle"></i> Ajouter des droits dans ce groupe 
                                                        </button>
                                                        <button type="button" class="btn btn-danger btn-remove-droit hidden mb-20">
                                                            <i class="fa fa-trash"></i> Supprimer ces droits de ce groupe 
                                                        </button>
                                                    </div>
                                                </div>
                                                
                                                <table id="tableDroits" class="table">
                                                    <thead>
                                                        <tr class="hidden">
                                                            <th></th>
                                                            <th>
                                                                Nom du groupe
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="tab_membres">
                                            <div class="no-membres-list hidden">
                                                <button type="button" class="btn btn-success btn-add-membre mb-20">
                                                    <i class="fa fa-plus-circle"></i> Ajouter des membres dans ce groupe 
                                                </button>
                                                <div class="alert alert-danger alert-dismissible" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                     Aucun membre n'appartient à ce groupe
                                                </div>
                                            </div>
                                            <div class="membres-list hidden">
                                                <div class="row">
                                                    <button type="button" class="btn btn-success btn-add-membre mb-20">
                                                        <i class="fa fa-plus-circle"></i> Ajouter des membres dans ce groupe 
                                                    </button>
                                                    <button type="button" class="btn btn-success btn-remove-membre hidden mb-20">
                                                        <i class="fa fa-plus-circle"></i> Supprimer ces membres de ce groupe 
                                                    </button>
                                                </div>
                                                
                                                <table id="tableMembres" class="table">
                                                    <thead>
                                                        <tr class="hidden">
                                                            <th></th>
                                                            <th>
                                                                Nom du groupe
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            
            <div class="modal fade" id="ajout-group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form class="form formulaire-edition-groupe">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel"> Ajouter un groupe </h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="intitule" class="control-label">Intitulé</label>
                                    <input type="text" class="form-control" name="intitule" id="intitule" placeholder="Taper l'intitulé">
                                </div>
                                <div class="form-group">
                                    <label for="description" class="control-label">Description</label>
                                    <textarea class="form-control" id="description" name="description" placeholder="Ecrivez une description" cols="30" rows="4"></textarea>
                                </div>
                                <input type="hidden" class="form-control" name="idgroupe" id="idgroupe">
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default btn-dismiss" data-dismiss="modal">Fermer</button>
                              <button type="submit" class="btn btn-success">Enregistrer</button>
                            </div>
                       </form>
                    </div>
                </div>
            </div>
                
            <div class="modal fade" id="ajout-droit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-header"></div>
                    <div class="modal-content">
                        <div class="panel panel-default mb-0">
                            <div class="panel-heading">
                                <h3>Listes des Droits</h3>
                            </div>
                            <div class="panel-body padding-20mm">
                                <table id="tableAutresDroits" class="table">
                                    <thead>
                                        <tr class="">
                                            <th></th>
                                            <th>
                                                Intitulé du groupe
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-dismiss" data-dismiss="modal">Fermer</button>
                        <button type="submit" class="btn btn-success btn-add-more-droit">Enregistrer</button>
                    </div>
                    </div>
                    
                </div>
            </div>
                
        </div>
                
        
        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/datatables.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/dataTables.select.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script> 
        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/gestionGroupes.js?ver=<%= current_date.toString() %>"></script>
        
    </body>
</html>
