/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author moussa.toure
 */
@Entity
@Table(name = "T_COMMANDEMENT")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commandement.findAll", query = "SELECT c FROM Commandement c"),
    @NamedQuery(name = "Commandement.findById", query = "SELECT c FROM Commandement c WHERE c.id = :id"),
    @NamedQuery(name = "Commandement.findByNumeroDocument", query = "SELECT c FROM Commandement c WHERE c.numeroDocument = :numeroDocument"),
    @NamedQuery(name = "Commandement.findByFaitGenerateur", query = "SELECT c FROM Commandement c WHERE c.faitGenerateur = :faitGenerateur"),
    @NamedQuery(name = "Commandement.findByDateEmissionCmdt", query = "SELECT c FROM Commandement c WHERE c.dateEmissionCmdt = :dateEmissionCmdt"),
    @NamedQuery(name = "Commandement.findByDateExigibilite", query = "SELECT c FROM Commandement c WHERE c.dateExigibilite = :dateExigibilite"),
    @NamedQuery(name = "Commandement.findByMontantPoursuivi", query = "SELECT c FROM Commandement c WHERE c.montantPoursuivi = :montantPoursuivi"),
    @NamedQuery(name = "Commandement.findByMontantFraisPoursuite", query = "SELECT c FROM Commandement c WHERE c.montantFraisPoursuite = :montantFraisPoursuite"),
    @NamedQuery(name = "Commandement.findByEtatImpression", query = "SELECT c FROM Commandement c WHERE c.etatImpression = :etatImpression"),
    @NamedQuery(name = "Commandement.findByDateCreat", query = "SELECT c FROM Commandement c WHERE c.dateCreat = :dateCreat"),
    @NamedQuery(name = "Commandement.findByEtat", query = "SELECT c FROM Commandement c WHERE c.etat = :etat"),
    @NamedQuery(name = "Commandement.findByDateEcheance", query = "SELECT c FROM Commandement c WHERE c.dateEcheance = :dateEcheance"),
    @NamedQuery(name = "Commandement.findByDateReception", query = "SELECT c FROM Commandement c WHERE c.dateReception = :dateReception")})
public class Commandement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "ID")
    private String id;
    @Size(max = 100)
    @Column(name = "NUMERO_DOCUMENT")
    private String numeroDocument;
    @Size(max = 50)
    @Column(name = "FAIT_GENERATEUR")
    private String faitGenerateur;
    @Column(name = "DATE_EMISSION_CMDT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEmissionCmdt;
    @Column(name = "DATE_EXIGIBILITE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateExigibilite;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "MONTANT_POURSUIVI")
    private BigDecimal montantPoursuivi;
    @Column(name = "MONTANT_FRAIS_POURSUITE")
    private BigDecimal montantFraisPoursuite;
    @Column(name = "ETAT_IMPRESSION")
    private Boolean etatImpression;
    
    @Column(name = "ETAT")
    private int etat;
    
    @Column(name = "DATE_CREAT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreat;
    @Column(name = "DATE_ECHEANCE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEcheance;
    @Column(name = "DATE_RECEPTION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReception;
    @JoinColumn(name = "ID_CONTRAINTE", referencedColumnName = "ID")
    @ManyToOne
    private Contrainte idContrainte;

    public Commandement() {
    }

    public Commandement(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumeroDocument() {
        return numeroDocument;
    }

    public void setNumeroDocument(String numeroDocument) {
        this.numeroDocument = numeroDocument;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }
    
    
    

    public String getFaitGenerateur() {
        return faitGenerateur;
    }

    public void setFaitGenerateur(String faitGenerateur) {
        this.faitGenerateur = faitGenerateur;
    }

    public Date getDateEmissionCmdt() {
        return dateEmissionCmdt;
    }

    public void setDateEmissionCmdt(Date dateEmissionCmdt) {
        this.dateEmissionCmdt = dateEmissionCmdt;
    }

    public Date getDateExigibilite() {
        return dateExigibilite;
    }

    public void setDateExigibilite(Date dateExigibilite) {
        this.dateExigibilite = dateExigibilite;
    }

    public BigDecimal getMontantPoursuivi() {
        return montantPoursuivi;
    }

    public void setMontantPoursuivi(BigDecimal montantPoursuivi) {
        this.montantPoursuivi = montantPoursuivi;
    }

    public BigDecimal getMontantFraisPoursuite() {
        return montantFraisPoursuite;
    }

    public void setMontantFraisPoursuite(BigDecimal montantFraisPoursuite) {
        this.montantFraisPoursuite = montantFraisPoursuite;
    }

    public Boolean getEtatImpression() {
        return etatImpression;
    }

    public void setEtatImpression(Boolean etatImpression) {
        this.etatImpression = etatImpression;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Date getDateEcheance() {
        return dateEcheance;
    }

    public void setDateEcheance(Date dateEcheance) {
        this.dateEcheance = dateEcheance;
    }

    public Date getDateReception() {
        return dateReception;
    }

    public void setDateReception(Date dateReception) {
        this.dateReception = dateReception;
    }

    public Contrainte getIdContrainte() {
        return idContrainte;
    }

    public void setIdContrainte(Contrainte idContrainte) {
        this.idContrainte = idContrainte;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commandement)) {
            return false;
        }
        Commandement other = (Commandement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cd.hologram.erecettesvg.api.Commandement[ id=" + id + " ]";
    }
    
}
