/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author KASHALA
 */
@Entity
@Cacheable(false)
@Table(name = "T_AEROPORT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aeroport.findAll", query = "SELECT c FROM Aeroport c"),
    @NamedQuery(name = "Aeroport.findByIdAeroport", query = "SELECT c FROM Aeroport c WHERE c.idAeroport = :idAeroport"),
    @NamedQuery(name = "Aeroport.findByIntitule", query = "SELECT c FROM Aeroport c WHERE c.intitule = :intitule"),
    @NamedQuery(name = "Aeroport.findByCodeAeroport", query = "SELECT c FROM Aeroport c WHERE c.codeAeroport = :codeAeroport"),
    @NamedQuery(name = "Aeroport.findByEtat", query = "SELECT c FROM Aeroport c WHERE c.etat = :etat")})
public class Aeroport implements Serializable{
     private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdAeroport")
    private Integer idAeroport;
    @Size(max = 250)
    @Column(name = "Intitule")
    private String intitule;
    @Size(max = 50)
    @Column(name = "CodeAeroport")
    private String codeAeroport;
    @Column(name = "ETAT")
    private Integer etat;
    @JoinColumn(name = "Fk_entite", referencedColumnName = "CODE")
    @ManyToOne
    private EntiteAdministrative fkEntite;

    public Aeroport() {
    }

    public Aeroport(Integer idAeroport) {
        this.idAeroport = idAeroport;
    }

    public Integer getIdAeroport() {
        return idAeroport;
    }

    public void setIdAeroport(Integer idAeroport) {
        this.idAeroport = idAeroport;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getCodeAeroport() {
        return codeAeroport;
    }

    public void setCodeAeroport(String codeAeroport) {
        this.codeAeroport = codeAeroport;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public EntiteAdministrative getFkEntite() {
        return fkEntite;
    }

    public void setFkEntite(EntiteAdministrative fkEntite) {
        this.fkEntite = fkEntite;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aeroport other = (Aeroport) obj;
        return true;
    }

    @Override
    public String toString() {
        return "Aeroport{" + "idAeroport=" + idAeroport + '}';
    }
    
}
