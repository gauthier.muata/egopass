/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author emmanuel.tsasa
 */
@Entity
@Table(name = "T_CONDUCTEUR_VEHICULE")
@Cacheable(false)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConducteurVehicule.findAll", query = "SELECT c FROM ConducteurVehicule c"),
    @NamedQuery(name = "ConducteurVehicule.findById", query = "SELECT c FROM ConducteurVehicule c WHERE c.id = :id"),
    @NamedQuery(name = "ConducteurVehicule.findByDateMaj", query = "SELECT c FROM ConducteurVehicule c WHERE c.dateMaj = :dateMaj"),
    @NamedQuery(name = "ConducteurVehicule.findByAgentMaj", query = "SELECT c FROM ConducteurVehicule c WHERE c.agentMaj = :agentMaj"),
    @NamedQuery(name = "ConducteurVehicule.findByAgentCreat", query = "SELECT c FROM ConducteurVehicule c WHERE c.agentCreat = :agentCreat"),
    @NamedQuery(name = "ConducteurVehicule.findByDateCreation", query = "SELECT c FROM ConducteurVehicule c WHERE c.dateCreation = :dateCreation"),
    @NamedQuery(name = "ConducteurVehicule.findByEtat", query = "SELECT c FROM ConducteurVehicule c WHERE c.etat = :etat")})
public class ConducteurVehicule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ID")
    private String id;
    @Size(max = 10)
    @Column(name = "DATE_CREATION")
    private String dateCreation;
    @Column(name = "ETAT")
    private Integer etat;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATE_MAJ")
    private Date dateMaj;

    @Column(name = "AGENT_CREAT")
    private int agentCreat;

    @Column(name = "AGENT_MAJ")
    private int agentMaj;

    @JoinColumn(name = "BIEN", referencedColumnName = "ID")
    @ManyToOne
    private Bien bien;
    @JoinColumn(name = "PROPRIETAIRE", referencedColumnName = "CODE")
    @ManyToOne
    private Personne proprietaire;
    @JoinColumn(name = "CONDUCTEUR", referencedColumnName = "CODE")
    @ManyToOne
    private Personne conducteur;
    @OneToMany(mappedBy = "fkConducteurVehicule")
    private List<Carte> carteList;

    public ConducteurVehicule() {
    }

    public ConducteurVehicule(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public int getAgentCreat() {
        return agentCreat;
    }

    public void setAgentCreat(int agentCreat) {
        this.agentCreat = agentCreat;
    }

    public int getAgentMaj() {
        return agentMaj;
    }

    public void setAgentMaj(int agentMaj) {
        this.agentMaj = agentMaj;
    }
    
    

    public Bien getBien() {
        return bien;
    }

    public void setBien(Bien bien) {
        this.bien = bien;
    }

    public Personne getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire) {
        this.proprietaire = proprietaire;
    }

    public Personne getConducteur() {
        return conducteur;
    }

    public void setConducteur(Personne conducteur) {
        this.conducteur = conducteur;
    }

    @XmlTransient
    public List<Carte> getCarteList() {
        return carteList;
    }

    public void setCarteList(List<Carte> carteList) {
        this.carteList = carteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConducteurVehicule)) {
            return false;
        }
        ConducteurVehicule other = (ConducteurVehicule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.ConducteurVehicule[ id=" + id + " ]";
    }

}
