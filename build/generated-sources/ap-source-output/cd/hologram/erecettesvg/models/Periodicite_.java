package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.ArticleBudgetaire;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Periodicite.class)
public class Periodicite_ { 

    public static volatile SingularAttribute<Periodicite, Integer> nbrJour;
    public static volatile SingularAttribute<Periodicite, String> code;
    public static volatile ListAttribute<Periodicite, ArticleBudgetaire> articleBudgetaireList;
    public static volatile SingularAttribute<Periodicite, Integer> periodeRecidive;
    public static volatile SingularAttribute<Periodicite, String> intitule;
    public static volatile SingularAttribute<Periodicite, Boolean> etat;

}