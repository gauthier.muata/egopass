/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author PC
 */
@Entity
@Cacheable(false)
@Table(name = "T_SITE_PEAGE_USER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SitePeageUser.findAll", query = "SELECT l FROM SitePeageUser l"),
    @NamedQuery(name = "SitePeageUser.findById", query = "SELECT l FROM SitePeageUser l WHERE l.id = :id"),
    @NamedQuery(name = "SitePeageUser.findByFkAgent", query = "SELECT l FROM SitePeageUser l WHERE l.fkAgent = :fkAgent"),
    @NamedQuery(name = "SitePeageUser.findByFkSitePeage", query = "SELECT l FROM SitePeageUser l WHERE l.fkSitePeage = :fkSitePeage"),
    @NamedQuery(name = "SitePeageUser.findByEtat", query = "SELECT l FROM SitePeageUser l WHERE l.etat = :etat")})

public class SitePeageUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull

    @Column(name = "ID")
    private Integer id;
    
    @Column(name = "FK_AGENT")
    private Integer fkAgent;
    
    @Column(name = "FK_SITE_PEAGE")
    private String fkSitePeage;
    
    @Column(name = "ETAT")
    private Integer etat;
    
    public SitePeageUser() {
    }

    public SitePeageUser(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkAgent() {
        return fkAgent;
    }

    public void setFkAgent(Integer fkAgent) {
        this.fkAgent = fkAgent;
    }

    public String getFkSitePeage() {
        return fkSitePeage;
    }

    public void setFkSitePeage(String fkSitePeage) {
        this.fkSitePeage = fkSitePeage;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SitePeageUser)) {
            return false;
        }
        SitePeageUser other = (SitePeageUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.nyongo.models.SitePeageUser[ id=" + id + " ]";
    }
    

}
