package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(ProrogationEcheanceLegaleDeclaration.class)
public class ProrogationEcheanceLegaleDeclaration_ { 

    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, Integer> agentMaj;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, String> dateDeclarationProrogee;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, String> dateLimitePaiementProrogee;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, Date> dateCreate;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, Integer> etat;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, Date> dateMaj;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, Integer> agentCreate;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, String> fkArticleBudgetaire;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, String> dateDeclarationLegale;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, String> exercice;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, String> dateLimitePaiement;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, Integer> id;
    public static volatile SingularAttribute<ProrogationEcheanceLegaleDeclaration, String> motif;

}