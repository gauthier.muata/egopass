package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.NotePerception;
import cd.hologram.erecettesvg.models.Personne;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(AcquitLiberatoire.class)
public class AcquitLiberatoire_ { 

    public static volatile SingularAttribute<AcquitLiberatoire, String> agentMaj;
    public static volatile SingularAttribute<AcquitLiberatoire, String> code;
    public static volatile SingularAttribute<AcquitLiberatoire, String> dateCreat;
    public static volatile SingularAttribute<AcquitLiberatoire, NotePerception> notePerception;
    public static volatile SingularAttribute<AcquitLiberatoire, String> observation;
    public static volatile SingularAttribute<AcquitLiberatoire, Boolean> etat;
    public static volatile SingularAttribute<AcquitLiberatoire, String> dateMaj;
    public static volatile SingularAttribute<AcquitLiberatoire, Personne> receptionniste;
    public static volatile SingularAttribute<AcquitLiberatoire, String> numeroTimbre;
    public static volatile SingularAttribute<AcquitLiberatoire, String> site;
    public static volatile SingularAttribute<AcquitLiberatoire, Boolean> etatImpression;
    public static volatile SingularAttribute<AcquitLiberatoire, String> numeroPapier;
    public static volatile SingularAttribute<AcquitLiberatoire, String> agentCreat;
    public static volatile SingularAttribute<AcquitLiberatoire, Boolean> utilise;

}