package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Acquisition;
import cd.hologram.erecettesvg.models.AdressePersonne;
import cd.hologram.erecettesvg.models.Assujeti;
import cd.hologram.erecettesvg.models.ComplementBien;
import cd.hologram.erecettesvg.models.TypeBien;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Bien.class)
public class Bien_ { 

    public static volatile SingularAttribute<Bien, String> fkTarif;
    public static volatile ListAttribute<Bien, Assujeti> assujetiList;
    public static volatile SingularAttribute<Bien, String> fkQuartier;
    public static volatile SingularAttribute<Bien, String> description;
    public static volatile SingularAttribute<Bien, String> intitule;
    public static volatile SingularAttribute<Bien, Boolean> etat;
    public static volatile SingularAttribute<Bien, TypeBien> typeBien;
    public static volatile ListAttribute<Bien, ComplementBien> complementBienList;
    public static volatile SingularAttribute<Bien, Integer> fkUsageBien;
    public static volatile ListAttribute<Bien, Acquisition> acquisitionList;
    public static volatile SingularAttribute<Bien, AdressePersonne> fkAdressePersonne;
    public static volatile SingularAttribute<Bien, String> id;
    public static volatile SingularAttribute<Bien, String> fkCommune;

}