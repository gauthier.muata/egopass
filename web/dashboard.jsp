<%-- 
    Document   : newjsp
    Created on : 13 juin 2018, 13:13:41
    Author     : moussa.toure
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>Tableau de bord</title>
        <link rel="stylesheet" href="assets/lib/css/bootstrap.min.css">  
        <link rel="stylesheet" href="assets/lib/alertify/alertify.min.css"> 
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/lib/fonts/fontawesome/css/fontawesome.min.css"/>

    </head>

    <body>

        <div class="wrapper">

            <%@include file="assets/include/menu.html" %>

            <div id="content" style="width: 100%">

                <%@include file="assets/include/header.html" %>

                <div class="row sameheight-container" style="display: none">
                    <div class="col col-12 col-sm-12 col-md-6 col-xl-5 stats-col">
                        <div class="card sameheight-item stats" data-exclude="xs" style="height: 530px;">
                            <div class="card-block">

                                <div class="title-block">
                                    <div class="card-header card-header-sm bordered">
                                        <div class="header-block">
                                            <h4 class="title">Fonction Ponctuel </h4>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <select style="width: 100%" class="form-control" id="cmbAssietteService">
                                        </select>
                                    </div>

                                    <div class="col-lg-6">
                                        <select style="width: 100%" class="form-control" id="cmdTypeTBB">
                                        </select>
                                    </div>
                                </div>
                                <div class="card-header card-header-sm bordered">

                                </div>
                                <div class="row row-sm stats-container">
                                    <div class="col-12 col-sm-6 stat-col">
                                        <div class="stat-icon">
                                            <i class="fa fa-rocket"></i>
                                        </div>
                                        <div class="stat">
                                            <div class="value">Taxation </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 stat-col">


                                        <div class="stat">
                                            <div class="value"> CDF </div>
                                            <div class="name">  USD </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6  stat-col">
                                        <div class="stat-icon">
                                            <i class="fa fa-line-chart"></i>
                                        </div>
                                        <div class="stat">
                                            <div class="value">Ordonancement </div>

                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6  stat-col">

                                        <div class="stat">
                                            <div class="value"> CDF </div>
                                            <div class="name"> USD </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6  stat-col">
                                        <div class="stat-icon">
                                            <i class="fa fa-list-alt"></i>
                                        </div>
                                        <div class="stat">
                                            <div class="value">Paiement </div>

                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 stat-col">

                                        <div class="stat">
                                            <div class="value"> CDF </div>
                                            <div class="name"> USD </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6  stat-col">
                                        <div class="stat-icon">
                                            <i class="fa fa-list-alt"></i>
                                        </div>
                                        <div class="stat">
                                            <div class="value">Reste à recouvrer </div>

                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 stat-col">
                                        
                                        <div class="stat">
                                            <div class="value"> CDF </div>
                                        </div>
                                        <div class="stat">
                                            <div class="value"> USD </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col col-12 col-sm-12 col-md-6 col-xl-5 stats-col">
                        <div class="card sameheight-item stats" data-exclude="xs" style="height: 530px;">
                            <div class="card-block">

                                <div class="title-block">
                                    <div class="card-header card-header-sm bordered">
                                        <div class="header-block">
                                            <h4 class="title">Impot </h4>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <select style="width: 100%" class="form-control" id="">
                                        </select>
                                    </div>

                                   
                                </div>
                                <div class="card-header card-header-sm bordered">

                                </div>
                                <div class="row row-sm stats-container">
                                    <div class="col-12 col-sm-6 stat-col">
                                        <div class="stat-icon">
                                            <i class="fa fa-rocket"></i>
                                        </div>
                                        <div class="stat">
                                            <div class="value">Periode </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 stat-col">


                                        <div class="stat">
                                            <div class="value"> CDF </div>
                                            <div class="name">  USD </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6  stat-col">
                                        <div class="stat-icon">
                                            <i class="fa fa-line-chart"></i>
                                        </div>
                                        <div class="stat">
                                            <div class="value">Déclaration </div>

                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6  stat-col">

                                        <div class="stat">
                                            <div class="value"> CDF </div>
                                            <div class="name"> USD </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6  stat-col">
                                        <div class="stat-icon">
                                            <i class="fa fa-list-alt"></i>
                                        </div>
                                        <div class="stat">
                                            <div class="value">Paiement </div>

                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 stat-col">

                                        <div class="stat">
                                            <div class="value"> CDF </div>
                                            <div class="name"> USD </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6  stat-col">
                                        <div class="stat-icon">
                                            <i class="fa fa-list-alt"></i>
                                        </div>
                                        <div class="stat">
                                            <div class="value">Reste à recouvrer </div>

                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 100%;"></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6 stat-col">
                                        
                                        <div class="stat">
                                            <div class="value"> CDF </div>
                                        </div>
                                        <div class="stat">
                                            <div class="value"> USD </div>
                                        </div>
                                        <div class="progress stat-progress">
                                            <div class="progress-bar" style="width: 0%;"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <%@include file="assets/include/modal/header.html" %>

        <script type="text/javascript" src="assets/lib/js/jquery.min.js"></script>      
        <script type="text/javascript" src="assets/lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/lib/alertify/alertify.min.js"></script>

        <script type="text/javascript" src="assets/lib/js/jquery.blockUI.js"></script>

        <script type="text/javascript" src="assets/js/utils.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
        <script type="text/javascript" src="assets/js/dashboard.js"></script>
    </body>
</html>
