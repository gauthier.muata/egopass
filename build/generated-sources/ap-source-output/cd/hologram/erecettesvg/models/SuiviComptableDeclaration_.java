package cd.hologram.erecettesvg.models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(SuiviComptableDeclaration.class)
public class SuiviComptableDeclaration_ { 

    public static volatile SingularAttribute<SuiviComptableDeclaration, Integer> code;
    public static volatile SingularAttribute<SuiviComptableDeclaration, Date> dateCreat;
    public static volatile SingularAttribute<SuiviComptableDeclaration, String> nc;
    public static volatile SingularAttribute<SuiviComptableDeclaration, String> libelleOperation;
    public static volatile SingularAttribute<SuiviComptableDeclaration, String> observation;
    public static volatile SingularAttribute<SuiviComptableDeclaration, BigDecimal> solde;
    public static volatile SingularAttribute<SuiviComptableDeclaration, String> documentReference;
    public static volatile SingularAttribute<SuiviComptableDeclaration, BigDecimal> debit;
    public static volatile SingularAttribute<SuiviComptableDeclaration, BigDecimal> credit;
    public static volatile SingularAttribute<SuiviComptableDeclaration, String> devise;

}