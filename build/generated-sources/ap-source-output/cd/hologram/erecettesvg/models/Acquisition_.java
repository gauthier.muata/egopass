package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Bien;
import cd.hologram.erecettesvg.models.Personne;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Acquisition.class)
public class Acquisition_ { 

    public static volatile SingularAttribute<Acquisition, String> referenceContrat;
    public static volatile SingularAttribute<Acquisition, Boolean> proprietaire;
    public static volatile SingularAttribute<Acquisition, String> numActeNotarie;
    public static volatile SingularAttribute<Acquisition, String> observation;
    public static volatile SingularAttribute<Acquisition, Personne> personne;
    public static volatile SingularAttribute<Acquisition, String> dateActeNotarie;
    public static volatile SingularAttribute<Acquisition, String> id;
    public static volatile SingularAttribute<Acquisition, String> dateAcquisition;
    public static volatile SingularAttribute<Acquisition, Short> etat;
    public static volatile SingularAttribute<Acquisition, Bien> bien;

}