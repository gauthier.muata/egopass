/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.hologram.erecettesvg.servlets;

import cd.hologram.erecettesvg.business.GestionPenaliteBackendBusiness;
import cd.hologram.erecettesvg.constants.GeneralConst;
import cd.hologram.erecettesvg.models.NatureArticleBudgetaire;
import cd.hologram.erecettesvg.models.PalierTauxPenalite;
import cd.hologram.erecettesvg.models.Penalite;
import cd.hologram.erecettesvg.models.TypePenalite;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author gauthier.muata
 */
@WebServlet(name = "GestionPenaliteBackend", urlPatterns = {"/gestionPenaliteBackendServlet"})
public class GestionPenaliteBackend extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        String result = GeneralConst.EMPTY_STRING,
                operation = request.getParameter(GeneralConst.OPERATION);

        switch (operation) {
            case "loadPenalites":
                result = loadPenalites(request);
                break;
            case "savePenalite":
                result = savePenalite(request);
                break;
            case "loadTauxPenalites":
                result = loadTauxPenalites(request);
                break;
            case "loadNatureAb":
                result = loaNatureABs(request);
                break;
            case "saveTauxPenalite":
                result = saveTauxPenalite(request);
                break;
            case "deleteTauxPenalite":
                result = deleteTauxPenalite(request);
                break;
            case "loadTypePenalite":
                result = loadTypePenalites(request);
                break;
            case "deleteTypePenalite":
                result = deleteTypePenalite(request);
                break;
            case "saveTypePenalite":
                result = saveTypePenalite(request);
                break;

        }

        out.print(result);
    }

    public String savePenalite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result;

        try {

            String code = request.getParameter("code");
            String inputPenalite = request.getParameter("inputPenalite");
            String selectTypePenalite = request.getParameter("selectTypePenalite");
            String valueChkVisibility = request.getParameter("valueChkVisibility");
            String valueChkActivate = request.getParameter("valueChkActivate");
            String valueChkPalier = request.getParameter("valueChkPalier");
            String valueChkApplyMonth = request.getParameter("valueChkApplyMonth");
            String userId = request.getParameter("userId");

            boolean valueChkVisibility_ = valueChkVisibility.equals("true") ? true : false;
            int valueChkActivate_ = valueChkActivate.equals("true") ? 1 : 0;
            boolean valueChkPalier_ = valueChkPalier.equals("true") ? true : false;
            boolean valueChkApplyMonth_ = valueChkApplyMonth.equals("true") ? true : false;

            if (code.isEmpty()) {

                result = GestionPenaliteBackendBusiness.savePenalite(
                        inputPenalite, selectTypePenalite,
                        valueChkPalier_, valueChkVisibility_, valueChkActivate_, userId, valueChkApplyMonth_);
            } else {

                result = GestionPenaliteBackendBusiness.updatePenalite(
                        code, inputPenalite, selectTypePenalite, valueChkPalier_,
                        valueChkVisibility_, valueChkActivate_, userId, valueChkApplyMonth_);
            }

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteTauxPenalite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String id = request.getParameter("id");

            if (GestionPenaliteBackendBusiness.deleteTauxPenalite(Integer.valueOf(id))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {

                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String deleteTypePenalite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String code = request.getParameter("code");
            String userId = request.getParameter("userId");

            if (GestionPenaliteBackendBusiness.deleteTypePenalite(code, Integer.valueOf(userId))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {

                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveTypePenalite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        try {

            String code = request.getParameter("code");
            String intitule = request.getParameter("intitule");
            String userId = request.getParameter("userId");

            if (GestionPenaliteBackendBusiness.saveTypePenalite(code, intitule, Integer.valueOf(userId))) {

                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;

            } else {

                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;

            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String saveTauxPenalite(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;
        boolean result;
        String typeTaux = GeneralConst.EMPTY_STRING;

        try {

            String id = request.getParameter("id");
            String selectPenalite = request.getParameter("selectPenalite");
            String selectNature = request.getParameter("selectNature");
            //typeTaux = request.getParameter("typeTaux");

            String selectTypePersonne = request.getParameter("selectTypePersonne");
            String inputTaux = request.getParameter("inputTaux");
            String inputNbreJourMin = request.getParameter("inputNbreJourMin");
            String inputNbreJourMax = request.getParameter("inputNbreJourMax");

            String valueCheckBoxTypeTaux = request.getParameter("valueCheckBoxTypeTaux");
            String valueCheckBoxActivateTauxPenalite = request.getParameter("valueCheckBoxActivateTauxPenalite");
            String valueCheckBoxRecidive = request.getParameter("valueCheckBoxRecidive");

            boolean valueCheckBoxTypeTaux_ = valueCheckBoxTypeTaux.equals("true") ? true : false;
            boolean valueCheckBoxActivateTauxPenalite_ = valueCheckBoxActivateTauxPenalite.equals("true") ? true : false;
            boolean valueCheckBoxRecidive_ = valueCheckBoxRecidive.equals("true") ? true : false;

            BigDecimal taux = BigDecimal.valueOf(Double.valueOf(inputTaux));

            BigDecimal bornMin = new BigDecimal("0");
            BigDecimal bornMax = new BigDecimal("0");

            if (!inputNbreJourMin.isEmpty()) {
                bornMin = BigDecimal.valueOf(Double.valueOf(inputNbreJourMin));
            }

            if (!inputNbreJourMax.isEmpty()) {
                bornMax = BigDecimal.valueOf(Double.valueOf(inputNbreJourMax));
            }

            typeTaux = (valueCheckBoxTypeTaux_) ? "%" : "F";

            if (id.isEmpty()) {

                result = GestionPenaliteBackendBusiness.saveTauxPenalite(
                        selectPenalite, selectNature, selectTypePersonne, taux, bornMin, bornMax, typeTaux,
                        valueCheckBoxTypeTaux_, valueCheckBoxRecidive_, valueCheckBoxActivateTauxPenalite_);
            } else {

                result = GestionPenaliteBackendBusiness.updateTauxPenalite(
                        Integer.valueOf(id), selectPenalite, selectNature, selectTypePersonne, taux, bornMin,
                        bornMax, typeTaux, valueCheckBoxTypeTaux_, valueCheckBoxRecidive_,
                        valueCheckBoxActivateTauxPenalite_);
            }

            if (result) {
                dataReturn = GeneralConst.ResultCode.SUCCES_OPERATION;
            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadPenalites(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        List<JsonObject> jsonPenaliteList = new ArrayList<>();
        List<Penalite> listPenalite;
        try {

            String valueSearch = request.getParameter("valueSearch");

            if (valueSearch.isEmpty()) {
                listPenalite = GestionPenaliteBackendBusiness.getListPenalites();
            } else {
                listPenalite = GestionPenaliteBackendBusiness.getListPenalitesV2(valueSearch);
            }

            if (!listPenalite.isEmpty()) {

                for (Penalite penalite : listPenalite) {

                    JsonObject jsonPenalite = new JsonObject();

                    jsonPenalite.addProperty("penaliteCode", penalite.getCode());
                    jsonPenalite.addProperty("penaliteName", penalite.getIntitule());
                    jsonPenalite.addProperty("penaliteTypeName", penalite.getTypePenalite().getIntitule());
                    jsonPenalite.addProperty("penaliteTypeCode", penalite.getTypePenalite().getCode());
                    jsonPenalite.addProperty("palier", penalite.getPalier() != null ? penalite.getPalier() : false);
                    jsonPenalite.addProperty("visibility", penalite.getVisibilteUtilisateur() != null ? penalite.getVisibilteUtilisateur() : false);
                    jsonPenalite.addProperty("applyMonth", penalite.getAppliquerMoisRetard() != null ? penalite.getAppliquerMoisRetard() : false);
                    jsonPenalite.addProperty("state", penalite.getEtat() != null ? penalite.getEtat() : 0);

                    jsonPenaliteList.add(jsonPenalite);

                }

                dataReturn = jsonPenaliteList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loaNatureABs(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        List<JsonObject> jsonNatureList = new ArrayList<>();
        List<NatureArticleBudgetaire> listNatureArticleBudgetaire;
        try {

            listNatureArticleBudgetaire = GestionPenaliteBackendBusiness.getListNatureArticleBudgetaires();

            if (!listNatureArticleBudgetaire.isEmpty()) {

                for (NatureArticleBudgetaire nature : listNatureArticleBudgetaire) {

                    JsonObject jsonNature = new JsonObject();

                    jsonNature.addProperty("natureABCode", nature.getCode());
                    jsonNature.addProperty("natureABName", nature.getIntitule());

                    jsonNatureList.add(jsonNature);

                }

                dataReturn = jsonNatureList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadTypePenalites(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        List<JsonObject> jsonTypePenaliteList = new ArrayList<>();
        List<TypePenalite> listTypePenalite;

        try {

            listTypePenalite = GestionPenaliteBackendBusiness.getListTypePenalites();

            if (!listTypePenalite.isEmpty()) {

                for (TypePenalite typePenalite : listTypePenalite) {

                    JsonObject jsonTypePenalite = new JsonObject();

                    jsonTypePenalite.addProperty("typePenaliteCode", typePenalite.getCode());
                    jsonTypePenalite.addProperty("typePenaliteName", typePenalite.getIntitule());

                    if (!typePenalite.getPenaliteList().isEmpty()) {

                        List<JsonObject> penaliteJsonList = new ArrayList<>();

                        jsonTypePenalite.addProperty("penalisteExist", GeneralConst.Number.ONE);

                        for (Penalite penalite : typePenalite.getPenaliteList()) {

                            JsonObject penaliteJson = new JsonObject();

                            penaliteJson.addProperty("typePenaliteCode", typePenalite.getCode());
                            penaliteJson.addProperty("penaliteCode", penalite.getCode());
                            penaliteJson.addProperty("penaliteName", penalite.getIntitule());

                            penaliteJsonList.add(penaliteJson);
                        }

                        jsonTypePenalite.addProperty("penalisteList", penaliteJsonList.toString());

                    } else {
                        jsonTypePenalite.addProperty("penalisteExist", GeneralConst.Number.ZERO);
                        jsonTypePenalite.addProperty("penalisteList", GeneralConst.EMPTY_STRING);
                    }

                    jsonTypePenaliteList.add(jsonTypePenalite);

                }

                dataReturn = jsonTypePenaliteList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    public String loadTauxPenalites(HttpServletRequest request) {

        String dataReturn = GeneralConst.EMPTY_STRING;

        List<JsonObject> jsonPalierTauxPenaliteList = new ArrayList<>();
        List<PalierTauxPenalite> listPalierTauxPenalite;

        try {

            //String valueSearch = request.getParameter("valueSearch");
            String penaliteCode = request.getParameter("penaliteCode");

            /*if (valueSearch.isEmpty()) {
             listPalierTauxPenalite = GestionPenaliteBackendBusiness.getListPalierTauxPenalites(penaliteCode);
             } else {
             listPalierTauxPenalite = GestionPenaliteBackendBusiness.getListPalierTauxPenalitesV2(valueSearch, penaliteCode);
             }*/
            listPalierTauxPenalite = GestionPenaliteBackendBusiness.getListPalierTauxPenalites(penaliteCode);

            if (!listPalierTauxPenalite.isEmpty()) {

                for (PalierTauxPenalite palierTauxPenalite : listPalierTauxPenalite) {

                    JsonObject jsonPalierTauxPenalite = new JsonObject();
                    NatureArticleBudgetaire nab = new NatureArticleBudgetaire();

                    jsonPalierTauxPenalite.addProperty("id", palierTauxPenalite.getId());
                    jsonPalierTauxPenalite.addProperty("penaliteCode", palierTauxPenalite.getPenalite().getCode());
                    jsonPalierTauxPenalite.addProperty("penaliteName", palierTauxPenalite.getPenalite().getIntitule());
                    jsonPalierTauxPenalite.addProperty("bornInf", palierTauxPenalite.getBorneInferieure().floatValue());
                    jsonPalierTauxPenalite.addProperty("bornSup", palierTauxPenalite.getBorneSuperieure().floatValue());
                    jsonPalierTauxPenalite.addProperty("typeTaux", palierTauxPenalite.getTypeValeur());
                    jsonPalierTauxPenalite.addProperty("taux", palierTauxPenalite.getValeur());
                    jsonPalierTauxPenalite.addProperty("isPercent", palierTauxPenalite.getEstPourcentage());
                    jsonPalierTauxPenalite.addProperty("isRecidive", palierTauxPenalite.getRecidive());
                    jsonPalierTauxPenalite.addProperty("state", palierTauxPenalite.getEtat());

                    nab = GestionPenaliteBackendBusiness.getNatureArticleBudgetaireByCode(
                            palierTauxPenalite.getNature().trim());

                    if (nab != null) {

                        jsonPalierTauxPenalite.addProperty("natureCode", nab.getCode());
                        jsonPalierTauxPenalite.addProperty("natureName", nab.getIntitule());

                    } else {

                        jsonPalierTauxPenalite.addProperty("natureCode", GeneralConst.EMPTY_STRING);
                        jsonPalierTauxPenalite.addProperty("natureName", GeneralConst.EMPTY_STRING);

                    }

                    jsonPalierTauxPenalite.addProperty("typePersonneCode", palierTauxPenalite.getFormeJuridique().getCode());

                    if (palierTauxPenalite.getFormeJuridique().getCode().equals(GeneralConst.ALL)) {
                        jsonPalierTauxPenalite.addProperty("typePersonneName", "Tous type de personne");
                    } else {
                        jsonPalierTauxPenalite.addProperty("typePersonneName", palierTauxPenalite.getFormeJuridique().getIntitule());
                    }

                    jsonPalierTauxPenaliteList.add(jsonPalierTauxPenalite);

                }

                dataReturn = jsonPalierTauxPenaliteList.toString();

            } else {
                dataReturn = GeneralConst.ResultCode.FAILED_OPERATION;
            }

        } catch (Exception e) {
            dataReturn = GeneralConst.ResultCode.EXCEPTION_OPERATION;
        }

        return dataReturn;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
