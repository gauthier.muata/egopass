package cd.hologram.erecettesvg.models;

import cd.hologram.erecettesvg.models.Amr;
import cd.hologram.erecettesvg.models.CompteBancaire;
import cd.hologram.erecettesvg.models.Personne;
import cd.hologram.erecettesvg.models.Site;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(Journal.class)
public class Journal_ { 

    public static volatile SingularAttribute<Journal, String> agentMaj;
    public static volatile SingularAttribute<Journal, String> numeroReleveBancaire;
    public static volatile SingularAttribute<Journal, CompteBancaire> compteBancaire;
    public static volatile SingularAttribute<Journal, String> code;
    public static volatile SingularAttribute<Journal, Amr> amr;
    public static volatile SingularAttribute<Journal, BigDecimal> montant;
    public static volatile SingularAttribute<Journal, Short> etat;
    public static volatile SingularAttribute<Journal, Date> dateMaj;
    public static volatile SingularAttribute<Journal, String> dateBordereau;
    public static volatile SingularAttribute<Journal, String> numeroAttestationPaiement;
    public static volatile SingularAttribute<Journal, Short> etatImpression;
    public static volatile SingularAttribute<Journal, String> typeDocument;
    public static volatile SingularAttribute<Journal, String> observationBanque;
    public static volatile SingularAttribute<Journal, BigDecimal> montantConvert;
    public static volatile SingularAttribute<Journal, Date> dateCreat;
    public static volatile SingularAttribute<Journal, BigDecimal> taux;
    public static volatile SingularAttribute<Journal, String> observation;
    public static volatile SingularAttribute<Journal, Personne> personne;
    public static volatile SingularAttribute<Journal, String> documentApure;
    public static volatile SingularAttribute<Journal, String> modePaiement;
    public static volatile SingularAttribute<Journal, String> bordereau;
    public static volatile SingularAttribute<Journal, String> dateApurement;
    public static volatile SingularAttribute<Journal, String> devise;
    public static volatile SingularAttribute<Journal, Site> site;
    public static volatile SingularAttribute<Journal, BigDecimal> montantApurre;
    public static volatile SingularAttribute<Journal, BigDecimal> montantPercu;
    public static volatile SingularAttribute<Journal, String> agentApurement;
    public static volatile SingularAttribute<Journal, String> agentCreat;
    public static volatile SingularAttribute<Journal, String> dateReleveBancaire;

}