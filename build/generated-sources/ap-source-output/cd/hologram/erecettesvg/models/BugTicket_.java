package cd.hologram.erecettesvg.models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2021-08-05T08:46:16")
@StaticMetamodel(BugTicket.class)
public class BugTicket_ { 

    public static volatile SingularAttribute<BugTicket, String> site;
    public static volatile SingularAttribute<BugTicket, String> code;
    public static volatile SingularAttribute<BugTicket, Date> dateCreat;
    public static volatile SingularAttribute<BugTicket, String> titre;
    public static volatile SingularAttribute<BugTicket, String> service;
    public static volatile SingularAttribute<BugTicket, String> ip;
    public static volatile SingularAttribute<BugTicket, String> macAdresse;
    public static volatile SingularAttribute<BugTicket, String> contenu1;
    public static volatile SingularAttribute<BugTicket, String> agentCreat;
    public static volatile SingularAttribute<BugTicket, String> contenu;
    public static volatile SingularAttribute<BugTicket, Integer> etat;

}