/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var btnCallModalSearchResponsable;
var lblLegalForm,
        lblNifResponsible,
        lblNameResponsible,
        lblAddress;

var lblLegalForm2,
        lblNifResponsible2,
        lblNameResponsible2,
        lblAddress2;

var isConducteur = 0;

var responsibleModal;
var responsibleObject = null;
var conducteurObject = null;
var codeFormeJuridique, codeResponsible, adresseId, codeConducteur,
        codeFormeJuridiqueConducteur, adresseIdConducteur;
var btnCallModalSearchConducteur;

var inputAutocollant, inputCPI, inputNumAutorisation, inputDistrict,
        inputDateCreation, inputDateExpiration, btnAssociateBien, btnSearchPhoto,
        textPieceJointe;
var btnSearchCPI, btnSaveCarte;
var contentBody, contentSearchCPI, ResearchCPI;

var modalBienPersonne, modalverifyCPI;
var lblIntituleBien, lblDescriptionBien, lblAdresserBien;
var codeBien = '';
var codeCPI = '';

var lbl1P, lbl3P, lbl4P, lbl1C, lbl3C, lbl4C;

var autocollantExists;
var autorisationExists;
var valueCheckIsChaine;
var nbreTaxe;

$(function () {

    codeBien = '';

    valueCheckIsBolean = true;
    mainNavigationLabel.text('IDENTIFICATION CONDUCTEUR MOTO');
    secondNavigationLabel.text('Edition d\'une carte');

    lblLegalForm = $('#lblLegalForm');
    lblNameResponsible = $('#lblNameResponsible');
    lblAddress = $('#lblAddress');
    lblNifResponsible = $('#lblNifResponsible');

    lblLegalForm2 = $('#lblLegalForm2');
    lblNameResponsible2 = $('#lblNameResponsible2');
    lblAddress2 = $('#lblAddress2');
    lblNifResponsible2 = $('#lblNifResponsible2');
    lblIntituleBien = $('#lblIntituleBien');
    lblDescriptionBien = $('#lblDescriptionBien');
    lblAdresserBien = $('#lblAdresserBien');

    responsibleModal = $('#responsibleModal');
    btnSearchPhoto = $('#btnSearchPhoto');
    textPieceJointe = $('#textPieceJointe');
    btnSearchCPI = $('#btnSearchCPI');
    contentBody = $('#contentBody');
    contentSearchCPI = $('#contentSearchCPI');
    ResearchCPI = $('#ResearchCPI');
    btnSaveCarte = $('#btnSaveCarte');

    lbl1P = $('#lbl1P');
    lbl3P = $('#lbl3P');
    lbl4P = $('#lbl4P');
    lbl1C = $('#lbl1C');
    lbl3C = $('#lbl3C');
    lbl4C = $('#lbl4C');

    inputAutocollant = $('#inputAutocollant');
    inputCPI = $('#inputCPI');
    inputNumAutorisation = $('#inputNumAutorisation');
    inputDistrict = $('#inputDistrict');
    inputDateCreation = $('#inputDateCreation');
    inputDateExpiration = $('#inputDateExpiration');
    btnAssociateBien = $('#btnAssociateBien');
    modalBienPersonne = $('#modalBienPersonne');
    modalverifyCPI = $('#modalverifyCPI');

    btnCallModalSearchResponsable = $('#btnCallModalSearchResponsable');
    btnCallModalSearchConducteur = $('#btnCallModalSearchConducteur');

    inputAutocollant.focusout(function () {

        switch (nbreTaxe) {
            case 2 :
            case 3 :
                if (inputAutocollant.val() !== empty) {
                    checkValidateAutocollant(inputAutocollant.val(), '1');
                }
                break;
        }

    });

    inputNumAutorisation.focusout(function () {

        switch (nbreTaxe) {
            case 2 :
            case 3 :
                if (inputNumAutorisation.val() !== empty) {
                    checkValidateAutorisationTransport(inputNumAutorisation.val(), '2');
                }
                break;
        }

    });

    btnCallModalSearchResponsable.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        isConducteur = 0;
        assujettiModal.modal('show');
    });

    btnCallModalSearchConducteur.on('click', function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        if (responsibleObject == null) {
            alertify.alert('Veuillez d\'abord sélectionner le propriétaire de la moto');
            return;
        }
        isConducteur = 1;
        assujettiModal.modal('show');
    });

    btnAssociateBien.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (responsibleObject == null && conducteurObject == null) {

            alertify.alert('Veuillez d\'abord rechercher et sélectionner le propriétaire et le conducteur');
            return;
        }

        //loadBiensPersonne2(responsibleObject.codeResponsible);
        loadBiensPersonne3(responsibleObject.codeResponsible);
    });

    btnSearchPhoto.click(function (e) {
        e.preventDefault();

        textPieceJointe.trigger('change');
        var self = document.getElementById('textPieceJointe');

        if (self.files && self.files[0]) {

            getFile(self);

        }
//        getFile();
    });

    btnSearchCPI.click(function (e) {
        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }
        isConducteur = 0;

        if (ResearchCPI.val() === empty) {
            alertify.alert('Veuillez d\'abord saisir le numéro du reçu');
            return;
        } else {
            searchCPI();
        }
    });

    btnSaveCarte.click(function (e) {

        e.preventDefault();
        if (checkSession()) {
            showSessionExpiredMessage();
            return;
        }

        if (conducteurObject == null) {

            alertify.alert('Veuillez d\'abord sélectionner le conducteur de la moto');
            return;

        } else {

            if (photoConducteurExist == false) {
                alertify.alert('Aucune photo n\'est prise pour le conducteur de la moto');
                return;
            }
        }

        switch (nbreTaxe) {
            case 2:
            case 3:

                if (codeBien === empty) {
                    alertify.alert('Veuillez d\'abord sélectionner la moto concernée');
                    return;
                }

                if (inputAutocollant.val() === empty) {
                    alertify.alert('Veuillez d\'abord saisir le numéro de l\'autocollant de la moto');
                    return;
                }

                if (inputNumAutorisation.val() === empty) {
                    alertify.alert('Veuillez d\'abord saisir le numéro d\'autorisation de transport');
                    return;
                }

                if (autocollantExists == false) {

                    alertify.alert('Cet autocollant : ' + inputAutocollant.val() + ' est déjà utilisé pour une demande antérieure. Veuillez saisir un autre autollant.');
                    return;
                }

                if (autorisationExists == false) {
                    alertify.alert('Cette autorisation de transport  : ' + inputNumAutorisation.val() + ' est déjà utilisée pour une demande antérieure. Veuillez saisir une autre autorisation.');
                    return;
                }
                break;
        }


        saveCarte();
    });


    // Initialise CropSelect
    $('#crop-select').CropSelectJs();


    // Handle file select change
    $('#file-input').on('change', function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#crop-select').CropSelectJs('setImageSrc', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
        $('#crop-select').CropSelectJs('selectCentredSquare');

        $('#modalCrop').modal('show');

    });


});

function getSelectedAssujetiData() {

    if (isConducteur === 0) {

        lbl1P.attr('style', 'display:inline');
        lbl3P.attr('style', 'display:inline');
        lbl4P.attr('style', 'display:inline');

        lblNameResponsible.html(selectAssujettiData.nomComplet);
        lblLegalForm.html(selectAssujettiData.categorie);
        lblAddress.html(selectAssujettiData.adresse);
        codeResponsible = selectAssujettiData.code;
        lblNifResponsible.html(selectAssujettiData.nif);
        codeFormeJuridique = selectAssujettiData.codeForme;
        adresseId = selectAssujettiData.codeAdresse;

        responsibleObject = new Object();

        responsibleObject.codeResponsible = codeResponsible;
        responsibleObject.codeFormeJuridique = codeFormeJuridique;
        responsibleObject.adresseId = adresseId;

    } else if (isConducteur === 1) {

        lbl1C.attr('style', 'display:inline');
        lbl3C.attr('style', 'display:inline');
        lbl4C.attr('style', 'display:inline');

        lblNameResponsible2.html(selectAssujettiData.nomComplet);
        lblLegalForm2.html(selectAssujettiData.categorie);
        lblAddress2.html(selectAssujettiData.adresse);
        codeConducteur = selectAssujettiData.code;
        lblNifResponsible2.html(selectAssujettiData.nif);
        codeFormeJuridiqueConducteur = selectAssujettiData.codeForme;
        adresseIdConducteur = selectAssujettiData.codeAdresse;

        conducteurObject = new Object();

        conducteurObject.codeResponsible = codeConducteur;
        conducteurObject.codeFormeJuridique = codeFormeJuridique;
        conducteurObject.adresseId = adresseId;
    }

}

function selectPersonneBien() {

    if (selectAssujettiBien.contratExist === '1') {
        var br = '</br></br>';

        if (controlAccess('SET_CONTRACT_MOTO')) {
            alertify.alert('Cette moto est actuellement sous contrat d\'exploitation avec un autre conducteur.' + br + 'Si toutefois vous souhaier l\'utiliser, veuillez suspendre le contrat en cours en cliquant sur le bouton [Casser].');
        } else {
            alertify.alert('Cette moto est actuellement sous contrat d\'exploitation avec un autre conducteur.');
        }

        return;
    } else {
        codeBien = selectAssujettiBien.id;
        lblIntituleBien.html(selectAssujettiBien.libelle);
        lblDescriptionBien.html(selectAssujettiBien.description);
        lblAdresserBien.html(selectAssujettiBien.adresse);
    }


}

function getFile(input) {
    console.log(input);

    if (input.files && input.files[0]) {
        console.log('In...');
        var reader = new FileReader();

        reader.onload = function (e) {

            var name = $("#textPieceJointe")[0].files[0].name;
            var base64 = e.target.result;
            console.log(base64);
            console.log(input);

//            if (!ifExistImage(name)) {
//                nextSteep(name, base64);
//            }
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function saveCarte() {

    alertify.confirm('Etes-vous sûr de vouloir enregistrer cette demande de carte ?', function () {
        $.ajax({
            type: 'POST',
            url: 'carteConducteur_servlet',
            dataType: 'JSON',
            crossDomain: false,
            data: {
                'numAutocollant': inputAutocollant.val(),
                'numAutorisation': inputNumAutorisation.val(),
                'district': userData.districtUser,
                'commune': userData.communeUser,
                'numCPI': codeCPI,
                'proprietaire': responsibleObject.codeResponsible,
                'conducteur': conducteurObject.codeResponsible,
                'bien': codeBien,
                'photo': $('#avatar').attr('src'),
                'operation': 'saveCarte'
            },
            beforeSend: function () {
                responsibleModal.block({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Enregistrement en cours ...</h5>'});
            },
            success: function (response)
            {

                setTimeout(function () {
                    $.unblockUI();

                    if (response == '1') {

                        if (nbreTaxe == 2) {

                            alertify.alert('L\'enregistrement de ces informations s\'est effectué avec succès');
                            window.location = 'editer-carte';

                        } else {
                            if (controlAccess('VIEW_CARTE')) {

                                alertify.confirm('L\'enregistrement de la carte s\'est effectué avec succès.<br/><br/>Voulez-vous accéder au registre pour lancer l\'impression de la carte ?',
                                        function () {
                                            window.location = 'registre-carte?recu=' + btoa(codeCPI);

                                        }, function () {
                                    window.location = 'editer-carte';

                                });
                            } else {
                                alertify.alert('L\'enregistrement de la carte s\'est effectué avec succès');
                                window.location = 'editer-carte';
                            }
                        }



                    } else if (response == '0') {
                        alertify.alert('Une erreur inattendue s\'est produite lors de l\'enregistrement.');

                    }
                }
                , 1);
            },
            complete: function () {
            },
            error: function () {
                $.unblockUI();
                showResponseError();
            }
        });
    });
}

function searchCPI() {

    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'JSON',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'searchCPI',
            'codeCPI': ResearchCPI.val()

        },
        beforeSend: function () {

            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Recherche en cours...</h5>'});

        },
        success: function (response)
        {

            if (response == '-1') {
                $.unblockUI();
                showResponseError();
                return;
            } else if (response == '0') {

                $.unblockUI();
                alertify.alert('Désoler, le reçu fournis n\'existe pas dans le système. Veuillez saisir un reçu valide.')
                return;

            } else if (response == '2') {

                $.unblockUI();
                alertify.alert('Ce reçu est déjà utilisé pour une demande antérieure');
                return;
            }

            setTimeout(function () {

                $.unblockUI();

                var cpiList = JSON.parse(JSON.stringify(response));

                codeCPI = cpiList.codeCPI;
                nbreTaxe = cpiList.nbreTaxe;

                lbl1P.attr('style', 'display:inline');
                lbl3P.attr('style', 'display:inline');
                lbl4P.attr('style', 'display:inline');

                inputDistrict.attr('readonly', true);

                lblNameResponsible.html(cpiList.nomComplet);
                lblAddress.html(cpiList.chaine);
                lblLegalForm.html(cpiList.categorie);
                codeResponsible = cpiList.codePersonne;
                codeFormeJuridique = cpiList.categorie
                codeFormeJuridique = cpiList.categorie
                inputDistrict.val(userData.districtUser);


                responsibleObject = new Object();

                responsibleObject.codeResponsible = codeResponsible;
                responsibleObject.codeFormeJuridique = codeFormeJuridique;

                contentBody.removeAttr('style')
                contentSearchCPI.attr('style', 'display : none');

                $.unblockUI();
            }
            , 1);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });
}

function checkValidateAutocollant(value, sw) {

    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'checkValidate',
            'value': value,
            'sw': sw
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification autocollant en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();

            if (response == '1') {

                autocollantExists = false;
                alertify.alert('Cet autocollant : ' + value + ' est déjà utilisé pour une demande antérieure. Veuillez saisir un autre autollant.');

            } else if (response == '-1') {
                autocollantExists = false;
                showResponseError();
            } else {
                autocollantExists = true;
            }
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }

    });

}

function checkValidateAutorisationTransport(value, sw) {

    $.ajax({
        type: 'POST',
        url: 'carteConducteur_servlet',
        dataType: 'text',
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        crossDomain: true,
        data: {
            'operation': 'checkValidate',
            'value': value,
            'sw': sw
        },
        beforeSend: function () {
            $.blockUI({message: '<img src="assets/images/loading.gif" alt="" /> <h5>Vérification autorisation en cours ...</h5>'});
        },
        success: function (response)
        {
            $.unblockUI();
            valueCheckIsChaine = sw;

            if (response == '1') {
                autorisationExists = false;
                alertify.alert('Cette autorisation de transport  : ' + value + ' est déjà utilisée pour une demande antérieure. Veuillez saisir une autre autorisation.');

            } else if (response == '-1') {
                autorisationExists = false;
                showResponseError();
            } else {
                autorisationExists = true;
            }
        },
        complete: function () {
        },
        error: function (xhr, status, error) {
            $.unblockUI();
            showResponseError();
        }
    });


}
